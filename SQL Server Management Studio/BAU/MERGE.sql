/****** Script for SelectTopNRows command from SSMS  ******/
SELECT TOP (1000) [ServerID]
      ,[DBMailEnabled]
      ,[DBMailTested]
      ,[DBMailTestResult]
  FROM [BAU].[dbo].[DBMail]

  MERGE [DBMail] AS TGT
  USING (
  SELECT 123 [ServerID]
	 ,0 [DBMailEnabled]
  ) AS SRC
  ON TGT.[ServerID] = SRC.[ServerID]
  WHEN NOT MATCHED THEN
  INSERT (ServerID,[DBMailEnabled])
  VALUES (SRC.ServerID, SRC.[DBMailEnabled])
  WHEN MATCHED THEN
  UPDATE 
  SET TGT.[DBMailEnabled] = SRC.[DBMailEnabled]
  ;
   
