--USE [MagPE]
--GO
--checkpoint
--DBCC SHRINKFILE (N'MagPE_log' , 5120)
--GO
--USE [MagED]
--GO
--checkpoint
--DBCC SHRINKFILE (N'MagED_log' , 5120)
--GO

--USE [MagicLogging]
--GO
--checkpoint
--DBCC SHRINKFILE (N'MAGFILES_Log', 5120)
--GO



exec sp_msforeachdb '

use [?];

if (db_name() in (''maghh'',''MAGUP''))
begin
	DECLARE IndexMaintenance CURSOR FOR
		SELECT	''ALTER INDEX ['' + ix.name + ''] ON ['' + DB_NAME(ix_phy.database_id) + ''].[''+ OBJECT_SCHEMA_NAME(ix.object_id, ix_phy.database_id) +''].'' + OBJECT_NAME(ix.object_id) + '' '' +
				CASE
					WHEN avg_fragmentation_in_percent BETWEEN 5 AND 30 THEN ''REORGANIZE''
					ELSE ''REBUILD''
				END + '';''
			FROM	sys.dm_db_index_physical_stats(DB_ID(''?''), NULL, NULL, NULL, NULL) ix_phy
			INNER JOIN sys.indexes ix
					ON ix_phy.object_id = ix.object_id
				AND ix_phy.index_id = ix.index_id
		WHERE	avg_fragmentation_in_percent > 30
		AND	ix.index_id <> 0
		ORDER BY database_id;
	DECLARE @SQL NVARCHAR(MAX);	
	OPEN IndexMaintenance;
	FETCH NEXT FROM IndexMaintenance INTO @SQL;
	WHILE @@FETCH_STATUS = 0
	BEGIN
		print(@SQL);
		FETCH NEXT FROM IndexMaintenance INTO @SQL;
	END
	CLOSE IndexMaintenance;
	DEALLOCATE IndexMaintenance;
end
'

	
--use MAGHH;

--exec sp_updatestats;

--use MAGUP;

--exec sp_updatestats;