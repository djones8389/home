CREATE PROC [usp_OSPatching]
	@ServerList nvarchar(MAX)
AS 

DECLARE @Dynamic nvarchar(MAX)='';

SELECT @Dynamic +=CHAR(13) + '

SELECT COMBINED.*
FROM (
SELECT
			h.[hostname],
			s.[serverID],
			s.[instanceName],
			--s.[hostID],
			--s.[clusterID],
			--s.[status],
			--s.[tcpPort],
			--s.[postConfigBy],
			--s.[postConfigPeerReviewedBy],
			--s.[serverNetworkProtocols],
			--s.[type],
			--s.[edition],
			--s.[version],
			--s.[servicePack],
			--s.[startupParameters],
			--s.[systemDbDevice],
			--s.[errorLogLocation],
			--s.[collation],
			--s.[minMemory],
			--s.[maxMemory],
			--s.[AWEEnabled],
			--s.[maxUserConnections],
			--s.[systemCreateDate],
			--s.[systemUpdateDate],
			--s.[suppliedUserDetails],
			--s.[serverState],
			--extras.BusinessUnit,
			--extras.ThirdPartyBackups,
			--extras.SQLMonitoring,
			--extras.CommissionedDate,
			--extras.DecommissionedDate,
			d.[Application],
			--d.[BusinessOwner],
			--d.[TechnicalOwner],
			--d.[CoreDbMaintenanceWindow],
			--d.[TechnicalExpert],
			--d.[VSR],
			--d.[Notes],
			extras.DNSAlias
		FROM 
			[dbo].[Servers] s
			INNER JOIN [dbo].[Hosts] h ON s.hostid = h.[hostID]
			LEFT JOIN [dbo].[Extras_Instance] extras ON s.serverID = extras.ServerID
			LEFT JOIN [dbo].[Databases] d ON s.serverID = d.serverID AND d.databaseName = ''model''
	
		UNION ALL 
	
		SELECT 
			c.[sqlclustername] as [hostname], 
			s.[serverID],
			s.[instanceName],
			--s.[hostID],
			--s.[clusterID],
			--s.[status],
			--s.[tcpPort],
			--s.[postConfigBy],
			--s.[postConfigPeerReviewedBy],
			--s.[serverNetworkProtocols],
			--s.[type],
			--s.[edition],
			--s.[version],
			--s.[servicePack],
			--s.[startupParameters],
			--s.[systemDbDevice],
			--s.[errorLogLocation],
			--s.[collation],
			--s.[minMemory],
			--s.[maxMemory],
			--s.[AWEEnabled],
			--s.[maxUserConnections],
			--s.[systemCreateDate],
			--s.[systemUpdateDate],
			--s.[suppliedUserDetails],
			--s.[serverState],
			--extras.BusinessUnit,
			--extras.ThirdPartyBackups,
			--extras.SQLMonitoring,
			--extras.CommissionedDate,
			--extras.DecommissionedDate,
			d.[Application],
			--d.[BusinessOwner],
			--d.[TechnicalOwner],
			--d.[CoreDbMaintenanceWindow],
			--d.[TechnicalExpert],
			--d.[VSR],
			--d.[Notes],
			extras.DNSAlias
		FROM 
			[dbo].[Servers] s 
			INNER JOIN [dbo].[Clusters] c ON s.clusterid = c.clusterid
			LEFT JOIN [dbo].[Extras_Instance] extras ON s.serverID = extras.ServerID
			LEFT JOIN [dbo].[Databases] d ON s.serverID = d.serverID AND d.databaseName = ''model''
) COMBINED
	WHERE
		Combined.[hostname] in ('+@ServerList+')
	ORDER BY
		hostname;
'

print(@ServerList);