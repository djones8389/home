--\\pr0303c844ed-00\SQL_Backups\LS

--BACKUP DATABASE [IDIT] TO  DISK = N'T:\Backup\IDIT_2019-03-14.bak' 
--WITH  COPY_ONLY, FORMAT,  NAME = N'IDIT-Full Database Backup', COMPRESSION
--	,  MAXTRANSFERSIZE = 4194304,  STATS = 1;

--RESTORE DATABASE IDIT FROM 
--	disk = '\\pr0303c844ed-00\t$\backup\IDIT_2019-03-14.bak'
--WITH MOVE 'HSCX_DEV' TO 'E:\SQL_Databases\IDIT.mdf',    
--MOVE 'HSCX_DEV_log' TO 'E:\SQL_Logs\IDIT_log.ldf', 
--REPLACE, RECOVERY, stats = 1;
--GO 



-- BACKUP DATABASE [BlackBox] TO  DISK = N'E:\SQL_Backups\BlackBox_2019-03-15.bak' WITH  COPY_ONLY
--	, FORMAT,  NAME = N'BlackBox-Full Database Backup', COMPRESSION,  STATS = 10;

--RESTORE DATABASE BlackBox FROM 
--	disk = '\\pr0303c844ed-00\e$\SQL_Backups\BlackBox_2019-03-15.bak'
--WITH MOVE 'BlackBox' TO 'E:\SQL_Databases\BlackBox.mdf',    
--MOVE 'BlackBox_Log' TO 'E:\SQL_Logs\BlackBox.ldf', 
--REPLACE, RECOVERY, stats = 1;
--GO 