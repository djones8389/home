--set transaction isolation level read uncommitted;

select * 
into T_META_DATA_tmp
from T_META_DATA

select * 
into T_META_DATA_EXT_HISTORY_tmp
from T_META_DATA_EXT_HISTORY

select * 
into T_META_DATA_EXTENSION_tmp
from T_META_DATA_EXTENSION

select * 
into T_META_DATA_HISTORY_tmp
from T_META_DATA_HISTORY

select * 
into T_META_EXT_GROUP_VALIDATION_tmp
from T_META_EXT_GROUP_VALIDATION

select * 
into T_PAGE_META_DATA_tmp
from T_PAGE_META_DATA



/*

(12176 rows affected)

(264 rows affected)

(20088 rows affected)

(180 rows affected)

(8028 rows affected)

(60459 rows affected)

*/