--select ID
--	,case 
--		when len(ACCOUNT_NR) = 2 then '000000' + ACCOUNT_NR
--		when len(ACCOUNT_NR) = 3 then '00000' + ACCOUNT_NR
--		when len(ACCOUNT_NR) = 4 then '0000' + ACCOUNT_NR
--		when len(ACCOUNT_NR) = 5 then '000' + ACCOUNT_NR
--		when len(ACCOUNT_NR) = 6 then '00' + ACCOUNT_NR
--		when len(ACCOUNT_NR) = 7 then '0' + ACCOUNT_NR
--		end
--from [IDIT_Obfuscated].[dbo].[CN_CONTACT_BANK_ACCOUNT] 
--where len(ACCOUNT_NR) < 8
--order by 1
 

--Would it be possible to obfuscate these as a random number between �00000001� and �99999999�?
--sql-lsupp-0-00.azure.hiscox.com

use [IDIT_Obfuscated];

-- step 1
	
	update [dbo].[CN_CONTACT_BANK_ACCOUNT] 
	set	ACCOUNT_NR = (SELECT ABS(CONVERT(BIGINT,CONVERT(BINARY(8), NEWID()))) % 99999999);

-- step 2

	update A
	set ACCOUNT_NR = b.[NewAccountRef]
	from [dbo].[CN_CONTACT_BANK_ACCOUNT] A
	inner join ( 
	select ID
		,case 
			when len(ACCOUNT_NR) = 2 then '000000' + ACCOUNT_NR
			when len(ACCOUNT_NR) = 3 then '00000' + ACCOUNT_NR
			when len(ACCOUNT_NR) = 4 then '0000' + ACCOUNT_NR
			when len(ACCOUNT_NR) = 5 then '000' + ACCOUNT_NR
			when len(ACCOUNT_NR) = 6 then '00' + ACCOUNT_NR
			when len(ACCOUNT_NR) = 7 then '0' + ACCOUNT_NR
			end as [NewAccountRef]
	from [IDIT_Obfuscated].[dbo].[CN_CONTACT_BANK_ACCOUNT] 
	where len(ACCOUNT_NR) < 8
	) B
   on A.id = b.ID;



update [dbo].[CN_CONTACT_BANK_ACCOUNT] 
set    ACCOUNT_NR = (SELECT RIGHT('00000000' + CAST(ABS(CONVERT(BIGINT,CONVERT(BINARY(8), NEWID()))) % 99999999 AS VARCHAR(8)), 8));