/***	read the current SQL Server default backup location	***/  
DECLARE @BackupDirectory NVARCHAR(100)   
EXEC master..xp_instance_regread @rootkey = 'HKEY_LOCAL_MACHINE',  
    @key = 'Software\Microsoft\MSSQLServer\MSSQLServer',  
    @value_name = 'BackupDirectory', @BackupDirectory = @BackupDirectory OUTPUT ;  
 
--SELECT @BackupDirectory AS [SQL Server default backup Value]
SELECT @BackupDirectory = '\\hiscox.com\backup\ReleaseDump\HXD70203'


SELECT 'BACKUP DATABASE [' + name + '] TO  DISK = N'''+@BackupDirectory+'\' + name ++'.bak'' WITH  COPY_ONLY, FORMAT,  NAME = N''' + name + '-Full Database Backup'', COMPRESSION,  STATS = 10;' 
FROM sys.databases
WHERE database_id > 4
	--and state_desc = 'ONLINE'
order by 1;

BACKUP DATABASE [CATMan] TO  DISK = N'\\hiscox.com\backup\ReleaseDump\HXD70203\CATMan.bak' WITH  COPY_ONLY, FORMAT,  NAME = N'CATMan-Full Database Backup', COMPRESSION,  STATS = 10;
BACKUP DATABASE [CATMan_ELTData] TO  DISK = N'\\hiscox.com\backup\ReleaseDump\HXD70203\CATMan_ELTData.bak' WITH  COPY_ONLY, FORMAT,  NAME = N'CATMan_ELTData-Full Database Backup', COMPRESSION,  STATS = 10;
BACKUP DATABASE [CATMan_SELTData_001] TO  DISK = N'\\hiscox.com\backup\ReleaseDump\HXD70203\CATMan_SELTData_001.bak' WITH  COPY_ONLY, FORMAT,  NAME = N'CATMan_SELTData_001-Full Database Backup', COMPRESSION,  STATS = 10;
BACKUP DATABASE [CATMan_TEST] TO  DISK = N'\\hiscox.com\backup\ReleaseDump\HXD70203\CATMan_TEST.bak' WITH  COPY_ONLY, FORMAT,  NAME = N'CATMan_TEST-Full Database Backup', COMPRESSION,  STATS = 10;
BACKUP DATABASE [CATManAdmin] TO  DISK = N'\\hiscox.com\backup\ReleaseDump\HXD70203\CATManAdmin.bak' WITH  COPY_ONLY, FORMAT,  NAME = N'CATManAdmin-Full Database Backup', COMPRESSION,  STATS = 10;
BACKUP DATABASE [CDR_Local] TO  DISK = N'\\hiscox.com\backup\ReleaseDump\HXD70203\CDR_Local.bak' WITH  COPY_ONLY, FORMAT,  NAME = N'CDR_Local-Full Database Backup', COMPRESSION,  STATS = 10;
BACKUP DATABASE [HIS_SQL_GLOBAL] TO  DISK = N'\\hiscox.com\backup\ReleaseDump\HXD70203\HIS_SQL_GLOBAL.bak' WITH  COPY_ONLY, FORMAT,  NAME = N'HIS_SQL_GLOBAL-Full Database Backup', COMPRESSION,  STATS = 10;
BACKUP DATABASE [HIS_SQL_REPORTS] TO  DISK = N'\\hiscox.com\backup\ReleaseDump\HXD70203\HIS_SQL_REPORTS.bak' WITH  COPY_ONLY, FORMAT,  NAME = N'HIS_SQL_REPORTS-Full Database Backup', COMPRESSION,  STATS = 10;
BACKUP DATABASE [Hiscox_Athena] TO  DISK = N'\\hiscox.com\backup\ReleaseDump\HXD70203\Hiscox_Athena.bak' WITH  COPY_ONLY, FORMAT,  NAME = N'Hiscox_Athena-Full Database Backup', COMPRESSION,  STATS = 10;
BACKUP DATABASE [Hiscox_Athena_Exposure] TO  DISK = N'\\hiscox.com\backup\ReleaseDump\HXD70203\Hiscox_Athena_Exposure.bak' WITH  COPY_ONLY, FORMAT,  NAME = N'Hiscox_Athena_Exposure-Full Database Backup', COMPRESSION,  STATS = 10;
BACKUP DATABASE [Hiscox_Athena_Reports] TO  DISK = N'\\hiscox.com\backup\ReleaseDump\HXD70203\Hiscox_Athena_Reports.bak' WITH  COPY_ONLY, FORMAT,  NAME = N'Hiscox_Athena_Reports-Full Database Backup', COMPRESSION,  STATS = 10;
BACKUP DATABASE [Hiscox_Athena_TEST] TO  DISK = N'\\hiscox.com\backup\ReleaseDump\HXD70203\Hiscox_Athena_TEST.bak' WITH  COPY_ONLY, FORMAT,  NAME = N'Hiscox_Athena_TEST-Full Database Backup', COMPRESSION,  STATS = 10;
BACKUP DATABASE [Hiscox_Discovery] TO  DISK = N'\\hiscox.com\backup\ReleaseDump\HXD70203\Hiscox_Discovery.bak' WITH  COPY_ONLY, FORMAT,  NAME = N'Hiscox_Discovery-Full Database Backup', COMPRESSION,  STATS = 10;
BACKUP DATABASE [Hiscox_Download] TO  DISK = N'\\hiscox.com\backup\ReleaseDump\HXD70203\Hiscox_Download.bak' WITH  COPY_ONLY, FORMAT,  NAME = N'Hiscox_Download-Full Database Backup', COMPRESSION,  STATS = 10;
BACKUP DATABASE [Hiscox_Irene_PML_Zones] TO  DISK = N'\\hiscox.com\backup\ReleaseDump\HXD70203\Hiscox_Irene_PML_Zones.bak' WITH  COPY_ONLY, FORMAT,  NAME = N'Hiscox_Irene_PML_Zones-Full Database Backup', COMPRESSION,  STATS = 10;
BACKUP DATABASE [Hiscox_Quote] TO  DISK = N'\\hiscox.com\backup\ReleaseDump\HXD70203\Hiscox_Quote.bak' WITH  COPY_ONLY, FORMAT,  NAME = N'Hiscox_Quote-Full Database Backup', COMPRESSION,  STATS = 10;
BACKUP DATABASE [Hiscox_Terrorism] TO  DISK = N'\\hiscox.com\backup\ReleaseDump\HXD70203\Hiscox_Terrorism.bak' WITH  COPY_ONLY, FORMAT,  NAME = N'Hiscox_Terrorism-Full Database Backup', COMPRESSION,  STATS = 10;
BACKUP DATABASE [RMS_GEOGRAPHY] TO  DISK = N'\\hiscox.com\backup\ReleaseDump\HXD70203\RMS_GEOGRAPHY.bak' WITH  COPY_ONLY, FORMAT,  NAME = N'RMS_GEOGRAPHY-Full Database Backup', COMPRESSION,  STATS = 10;
BACKUP DATABASE [RMS_SYSTEMDATA] TO  DISK = N'\\hiscox.com\backup\ReleaseDump\HXD70203\RMS_SYSTEMDATA.bak' WITH  COPY_ONLY, FORMAT,  NAME = N'RMS_SYSTEMDATA-Full Database Backup', COMPRESSION,  STATS = 10;
BACKUP DATABASE [RMS_USERCONFIG] TO  DISK = N'\\hiscox.com\backup\ReleaseDump\HXD70203\RMS_USERCONFIG.bak' WITH  COPY_ONLY, FORMAT,  NAME = N'RMS_USERCONFIG-Full Database Backup', COMPRESSION,  STATS = 10;
BACKUP DATABASE [SSISDB] TO  DISK = N'\\hiscox.com\backup\ReleaseDump\HXD70203\SSISDB.bak' WITH  COPY_ONLY, FORMAT,  NAME = N'SSISDB-Full Database Backup', COMPRESSION,  STATS = 10;