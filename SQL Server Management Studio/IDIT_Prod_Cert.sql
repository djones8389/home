SELECT db.name, db.is_encrypted, dm.encryption_state, dm.percent_complete, dm.key_algorithm, dm.key_length
FROM sys.databases db    
LEFT OUTER JOIN sys.dm_database_encryption_keys dm        
ON db.database_id = dm.database_id;
go

SELECT d.is_master_key_encrypted_by_server, name
FROM sys.databases AS d

USE master;  
GO 

select * from sys.dm_database_encryption_keys
GO
 

---- drop MASTER KEY
--CREATE MASTER KEY ENCRYPTION BY PASSWORD = 'YYYthdh758934759875kfljfsdj';  
--go 

--drop certificate IDIT_Prod_Cert
--4. Restore the master certificate
CREATE CERTIFICATE IDIT_Prod_Cert   
FROM FILE = 'T:\Keys\IDIT_Prod_Cert'  
WITH PRIVATE KEY   
(  
    FILE = 'T:\Keys\IDIT_PrivateKeyFile',  
    DECRYPTION BY PASSWORD = 'J(*%kkjhdsfjk65768763434'  
);  
GO 

