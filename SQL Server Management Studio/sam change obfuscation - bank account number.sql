--SELECT RAND(100), RAND(), RAND()  

SELECT FLOOR(RAND()*(99999999-0+1))+0

--from �00000001� to �99999999�

--4987477

EXEC ObfuscationControl..sp_audit_log 'Start','obfuscate CN_CONTACT_BANK_ACCOUNT'

UPDATE [IDIT_Obfuscated].[dbo].[CN_CONTACT_BANK_ACCOUNT] 
SET BANK_NAME = 'Test Bank UK'
	,BRANCH_NAME = NULL
	,ACCOUNT_NR = (SELECT FLOOR(RAND()*(99999999-0+1))+0)
	,IBAN_REFERENCE = 'GB15MIDL60180116221648'
	,SWIFT_CODE = NULL
	,BIC_CODE = 'MIDLGB22123'
	,BBAN = NULL
	,VERIFICATION_STATUS_REMARKS = NULL

EXEC ObfuscationControl..sp_audit_log 'Finish','obfuscate CN_CONTACT_BANK_ACCOUNT',@@rowcount

--6 seconds
--1 second

--36 seconds
--17 seconds

select ACCOUNT_NR
from [IDIT_Obfuscated].[dbo].[CN_CONTACT_BANK_ACCOUNT] 

declare bankacc cursor for
select top 10 id, ACCOUNT_NR
from [IDIT_Obfuscated].[dbo].[CN_CONTACT_BANK_ACCOUNT] 

declare @id int, @account nvarchar(max);

open bankacc

fetch next from bankacc into @id, @account

while @@FETCH_STATUS = 0

begin
	update [IDIT_Obfuscated].[dbo].[CN_CONTACT_BANK_ACCOUNT] 
	set	ACCOUNT_NR = (SELECT FLOOR(RAND()*(99999999-0+1))+0)
	where id = @id

	fetch next from bankacc into @id, @account

end
close bankacc
deallocate bankacc



select top 10 * from [IDIT_Obfuscated].[dbo].[CN_CONTACT_BANK_ACCOUNT]  where id between 20 and 29

select len(ACCOUNT_NR) from [IDIT_Obfuscated].[dbo].[CN_CONTACT_BANK_ACCOUNT] group by ACCOUNT_NR  order by 1

select * from [IDIT_Obfuscated].[dbo].[CN_CONTACT_BANK_ACCOUNT]  where  len(ACCOUNT_NR) = 2

update [IDIT_Obfuscated].[dbo].[CN_CONTACT_BANK_ACCOUNT] 
set	ACCOUNT_NR = convert(nvarchar(100),(SELECT FLOOR(RAND()*(99999999-1+1)+1)))
where id between 20 and 29

 
SELECT FLOOR(RAND()*(99999999-1+1)+1)

SELECT ABS(CONVERT(BIGINT,CONVERT(BINARY(8), NEWID()))) % 99999999



update [dbo].[CN_CONTACT_BANK_ACCOUNT] 
set    ACCOUNT_NR = (SELECT RIGHT('00000000' + CAST(ABS(CONVERT(BIGINT,CONVERT(BINARY(8), NEWID()))) % 99999999 AS VARCHAR(8)), 8));