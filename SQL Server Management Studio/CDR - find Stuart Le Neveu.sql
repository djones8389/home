use SQL_Inventory;

SELECT A.*
FROM (
select distinct 
	 h.hostName
	 , s.instanceName
	 , d.databaseName
	from [SQL_Inventory].[dbo].[Servers] s --ON s.[serverID] = faj.[serverID]
	LEFT OUTER JOIN [SQL_Inventory].[dbo].[Clusters] c ON c.[clusterID] = s.[clusterID]
	LEFT OUTER JOIN [SQL_Inventory].[dbo].[ClusterNodes] cn ON cn.[clusterID] = s.[clusterID]
	LEFT OUTER JOIN [SQL_Inventory].[dbo].[Hosts] ch ON ch.[hostID] = cn.[nodeID]
	LEFT OUTER JOIN [SQL_Inventory].[dbo].[Hosts] h ON h.[hostID] = s.[hostID]
	inner join Databases D on D.serverID = s.serverID
	where technicalExpert like '%Stuart Le Neveu%'
		or technicalOwner like '%Stuart Le Neveu%'
		or businessOwner  like '%Stuart Le Neveu%'
) A		
where databaseName not in ('tempdb','msdb','model','master')
	and hostName is not null
	order by 1,2,3;



	select distinct 
	 h.hostName
	from [SQL_Inventory].[dbo].[Servers] s --ON s.[serverID] = faj.[serverID]
	LEFT OUTER JOIN [SQL_Inventory].[dbo].[Clusters] c ON c.[clusterID] = s.[clusterID]
	LEFT OUTER JOIN [SQL_Inventory].[dbo].[ClusterNodes] cn ON cn.[clusterID] = s.[clusterID]
	LEFT OUTER JOIN [SQL_Inventory].[dbo].[Hosts] ch ON ch.[hostID] = cn.[nodeID]
	LEFT OUTER JOIN [SQL_Inventory].[dbo].[Hosts] h ON h.[hostID] = s.[hostID]
	inner join Databases D on D.serverID = s.serverID
	where  h.hostname like '%azure%'

	SELECT DISTINCT hostname
	from Hosts
	where hostname like '%azure%'