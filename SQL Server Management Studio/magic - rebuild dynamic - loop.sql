if OBJECT_ID('tempdb..##indexes')  is not null drop table ##indexes;

create table ##indexes (
	dbname sysname
		, tablename sysname
		, indexname sysname
);

exec sp_MSforeachdb '

use [?];

if exists (
select 1
from INFORMATION_SCHEMA.COLUMNS a
inner join sys.indexes b
on a.TABLE_NAME	 = OBJECT_NAME(b.object_id)
where TABLE_NAME like ''POLOPTN%''
	and type_desc = ''CLUSTERED''
)

begin
	insert ##indexes
	select db_name()
		, a.TABLE_NAME
		, b.name
	from INFORMATION_SCHEMA.TABLES a
	inner join sys.indexes b
	on a.TABLE_NAME	 = OBJECT_NAME(b.object_id)
	where TABLE_NAME like ''POLOPTN%''
		and type_desc = ''CLUSTERED''

	declare @dynamic nvarchar(max) = '''';

	select @dynamic+=CHAR(13) + 
		''use ''+QUOTENAME(db_name()) +'' ALTER INDEX '' +  quotename(B.name)  +  '' ON '' + quotename(a.TABLE_NAME) + '' rebuild;''
	from INFORMATION_SCHEMA.TABLES a
	inner join sys.indexes b
	on a.TABLE_NAME	 = OBJECT_NAME(b.object_id)
	where TABLE_NAME like ''POLOPTN%''
		and type_desc = ''CLUSTERED''
	print(@dynamic);

end

'

use [MAGXXXXX] ALTER INDEX [cIdx_Poloptn] ON [POLOPTN] rebuild;use [MAGXXXXX] ALTER INDEX [PK__POLOPTN_CHNG__55FFB06A] ON [POLOPTN_CHNG] rebuild;
use [MAGWW] ALTER INDEX [cIdx_Poloptn] ON [POLOPTN] rebuild;use [MAGWW] ALTER INDEX [PK__POLOPTN_CHNG__56D3D912] ON [POLOPTN_CHNG] rebuild;
use [MAGUT] ALTER INDEX [cIdx_Poloptn] ON [PolOptn] rebuild;use [MAGUT] ALTER INDEX [PK__POLOPTN_CHNG__2E26C93A] ON [POLOPTN_CHNG] rebuild;
use [MAGUR] ALTER INDEX [cIdx_Poloptn] ON [POLOPTN] rebuild;use [MAGUR] ALTER INDEX [PK__POLOPTN_CHNG__7889D298] ON [POLOPTN_CHNG] rebuild;
use [MAGUP] ALTER INDEX [cIdx_Poloptn] ON [PolOptn] rebuild;use [MAGUP] ALTER INDEX [PK__POLOPTN_CHNG__6482D9EB] ON [POLOPTN_CHNG] rebuild;
use [MAGUM] ALTER INDEX [PK__POLOPTN_CHNG__127EAEC5] ON [POLOPTN_CHNG] rebuild;use [MAGUM] ALTER INDEX [cIdx_Poloptn] ON [PolOptn] rebuild;
use [MAGTM] ALTER INDEX [PK__POLOPTN_CHNG__1B13F4C6] ON [POLOPTN_CHNG] rebuild;use [MAGTM] ALTER INDEX [cIdx_Poloptn] ON [PolOptn] rebuild;
use [MAGTF] ALTER INDEX [cIdx_Poloptn] ON [PolOptn] rebuild;use [MAGTF] ALTER INDEX [PK__POLOPTN_CHNG__575DE8F7] ON [POLOPTN_CHNG] rebuild;
use [MAGSC] ALTER INDEX [cIdx_Poloptn] ON [PolOptn] rebuild;use [MAGSC] ALTER INDEX [PK__POLOPTN_CHNG__5FF32EF8] ON [POLOPTN_CHNG] rebuild;
use [MAGMG] ALTER INDEX [cIdx_Poloptn] ON [PolOptn] rebuild;use [MAGMG] ALTER INDEX [PK__POLOPTN_CHNG__2A563856] ON [POLOPTN_CHNG] rebuild;
use [MAGLL] ALTER INDEX [PK__POLOPTN_CHNG__1372D2FE] ON [POLOPTN_CHNG] rebuild;use [MAGLL] ALTER INDEX [cIdx_Poloptn] ON [POLOPTN] rebuild;
use [MAGEV] ALTER INDEX [cIdx_Poloptn] ON [PolOptn] rebuild;use [MAGEV] ALTER INDEX [PK__POLOPTN_CHNG__4321E620] ON [POLOPTN_CHNG] rebuild;
use [MAGEU] ALTER INDEX [cIdx_Poloptn] ON [PolOptn] rebuild;use [MAGEU] ALTER INDEX [PK__POLOPTN_CHNG__75E27017] ON [POLOPTN_CHNG] rebuild;
use [MAGES] ALTER INDEX [cIdx_Poloptn] ON [PolOptn] rebuild;use [MAGES] ALTER INDEX [PK__POLOPTN_CHNG__2F4FF79D] ON [POLOPTN_CHNG] rebuild;
use [MAGEP] ALTER INDEX [PK__POLOPTN_CHNG__024846FC] ON [POLOPTN_CHNG] rebuild;use [MAGEP] ALTER INDEX [cIdx_Poloptn] ON [POLOPTN] rebuild;
use [MAGPI] ALTER INDEX [cIdx_Poloptn] ON [POLOPTN] rebuild;use [MAGPI] ALTER INDEX [PK__POLOPTN_CHNG__55FFB06A] ON [POLOPTN_CHNG] rebuild;
use [MAGHH] ALTER INDEX [PK__POLOPTN_CHNG__2C09769E] ON [POLOPTN_CHNG] rebuild;use [MAGHH] ALTER INDEX [cIdx_Poloptn] ON [PolOptn] rebuild;
use [MAGMP] ALTER INDEX [cIdx_Poloptn] ON [POLOPTN] rebuild;use [MAGMP] ALTER INDEX [PK__POLOPTN_CHNG__55FFB06A] ON [POLOPTN_CHNG] rebuild;


select * from ##indexes