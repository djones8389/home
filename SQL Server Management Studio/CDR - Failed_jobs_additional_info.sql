use SQL_Inventory

select *  
from SQL_Inventory..Failed_jobs_additional_info
where hostName = 'hxe70201.hiscoxbm.local'



insert Failed_jobs_additional_info 
values('hxe70201.hiscoxbm.local'
	,'MSSQLSERVER'
	,'Refresh Environment after Prod Deployment'
	, getdate()
	, 'DaveJ'
	, 'Nick Webber working on this'
	)

update Failed_jobs_additional_info
set comment = 'Working now, after restarting SQL Agent service on server'
	, recorded_date = getdate()
where job = 'Server Maintanence: Recycle Error Logs'
	and hostName = 'hxd70203.hiscoxbm.local'
	
update Failed_jobs_additional_info
set comment =  'Emailed:  Alastair Gilhooley, Ben Walker and Andy Isgate'
where hostName = 'aphxp06s-uk01.hiscox.com'
