use IDIT

select distinct
	object_name(st.object_id) [tablename]
	, row_count
	, b.type
into ##IDIT
from sys.dm_db_partition_stats st 
inner join sys.tables b
on b.name = object_name(st.object_id)
where row_count <> 0	
	and type = 'U'
order by 1;

select * 
from ##IDIT
order by 1

use IDIT_1

select distinct
	object_name(st.object_id) [tablename]
	, row_count
	, b.type
into ##IDIT_1
from sys.dm_db_partition_stats st 
inner join sys.tables b
on b.name = object_name(st.object_id)
where row_count <> 0	
	and type = 'U'
order by 1;

select * 
from ##IDIT_1
order by 1

select * 
from ##IDIT A
inner join ##IDIT_1 B
on A.[tablename] = b.[tablename]
where a.row_count <> b.row_count
order by 1;