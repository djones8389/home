DECLARE @SessionName SysName  = 'system_health';

DECLARE @Target_File NVarChar(1000)
	, @Target_Dir NVarChar(1000)
	, @Target_File_WildCard NVarChar(1000)

SELECT  @Target_File = CAST(t.target_data as XML).value('EventFileTarget[1]/File[1]/@name', 'NVARCHAR(256)')
FROM sys.dm_xe_session_targets t
	INNER JOIN sys.dm_xe_sessions s ON s.address = t.event_session_address
WHERE s.name = @SessionName
	AND t.target_name = 'event_file';
	
SELECT @Target_Dir = LEFT(@Target_File, Len(@Target_File) - CHARINDEX('\', REVERSE(@Target_File))) ;
SELECT @Target_File_WildCard = @Target_Dir + '\' + @SessionName + '_*.xel';


SELECT CONVERT(xml, event_data).query('/event/data/value/child::*')
	, CONVERT(xml, event_data).value('(event[@name="xml_deadlock_report"]/@timestamp)[1]','datetime') as Execution_Time 
FROM sys.fn_xe_file_target_read_file(@Target_File_WildCard, null, null, null)
WHERE object_name like 'xml_deadlock_report'	
	--and cast(CONVERT(xml, event_data).query('/event/data/value/child::*') as nvarchar(MAX))  like '%OPENBOX.dbo.p_ClaimAmend_1%'
	--and cast(CONVERT(xml, event_data).query('/event/data/value/child::*') as nvarchar(MAX))  like '%processa5c6cf8%'
