USE msdb;
GO

EXEC dbo.sp_add_job
    @job_name = N'Pre-Migration - Backup ItemBank' ;
GO

DECLARE @CreateIBBackupDevice nvarchar(MAX)= '';

SELECT @CreateIBBackupDevice = '

DECLARE @BackupDefault TABLE (Data nvarchar(max))
INSERT @BackupDefault
VALUES (''E:\Backup\Pre-Migration'')

--DECLARE @BackupDefault TABLE (Value nvarchar(30), Data nvarchar(max))
--INSERT @BackupDefault
--EXEC  master.dbo.xp_instance_regread 
--N''HKEY_LOCAL_MACHINE'', N''Software\Microsoft\MSSQLServer\MSSQLServer'',N''BackupDirectory''


DECLARE @BackupToDefaultLoc nvarchar(MAX) = '''';

SELECT @BackupToDefaultLoc += CHAR(13) + ''

IF NOT EXISTS (SELECT 1 FROM sys.backup_devices where Name = N''''ItemBank'''')

EXEC master.dbo.sp_addumpdevice  @devtype = N''''disk'''', @logicalname = N''''ItemBank'''', @physicalname ='''''' + Data + ''\'' + ''Pre-Migration-ItemBank.bak'''''' 
FROM @BackupDefault

EXEC(@BackupToDefaultLoc)


'

EXEC sp_add_jobstep
    @job_name = N'Pre-Migration - Backup ItemBank',
    @step_name = N'Create Backup Device',
    @subsystem = N'TSQL',
    @database_name = N'msdb',
    @on_success_action = 3,
    @on_fail_action = 2,
    @command = @CreateIBBackupDevice, 
    @retry_attempts = 0,
    @retry_interval = 0;
GO


DECLARE @BackupItemBank nvarchar(MAX) = '';

SELECT @BackupItemBank = 'DECLARE @BackupItemBank nvarchar(MAX) = '''';

SELECT @BackupItemBank +=CHAR(13) +  ''BACKUP DATABASE [''+ Name +''] TO [''+ ''ItemBank'' +''] WITH COPY_ONLY, COMPRESSION, NOFORMAT, NOINIT, SKIP, NOREWIND, NOUNLOAD; ''
from sys.databases
where name like ''%ItemBank''
EXEC(@BackupItemBank)

'

EXEC sp_add_jobstep
    @job_name = N'Pre-Migration - Backup ItemBank',
    @step_name = N'Backup ItemBank to Device',
    @subsystem = N'TSQL',
    @database_name = N'msdb',
    @on_success_action = 1,
    @on_fail_action = 2,
    @command = @BackupItemBank, 
    @retry_attempts = 0,
    @retry_interval = 0;
GO

EXEC dbo.sp_add_jobserver
    @job_name = N'Pre-Migration - Backup ItemBank';
GO