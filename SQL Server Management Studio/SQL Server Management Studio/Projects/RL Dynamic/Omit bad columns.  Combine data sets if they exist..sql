----USE [PRV_AAT_ItemBank]

--FROM AssessmentTable AT
--INNER JOIN AssessmentGroupTable AGT ON AGT.id = AT.AssessmentGroupID
--INNER JOIN AssessmentStatusLookupTable ASLT on ASLT.id = AT.AssessmentStatus



use master

exec sp_MSforeachdb '

use [?];

SET QUOTED_IDENTIFIER ON;

BEGIN

if(''?'' like ''%ItemBank%'')

if (select count(*) from INFORMATION_SCHEMA.TABLES where TABLE_NAME in (''AssessmentTable'',''AssessmentGroupTable'',''AssessmentStatusLookupTable'')) = 3

SELECT ''?''
	,at.id
	,ASLT.Name
	, gb.GradeBoundary [Grade Boundaries]
	, ISNULL(AssessmentRules.value(''data(PaperRules/Section/@TotalQuestions)[1]'',''tinyint''),''0'') [Total Questions]
	,IsPrintable
	,AllowCloseWithoutSubmit
	,ISNULL(AT.UseAsTemplate,''0'') [Use As Template]
	,PreCaching [AutoSync]
	,CASE ActionScriptMode
		WHEN ''0''
			THEN ''Mixed''
		WHEN ''1''
			THEN ''AS2''
		WHEN ''2''
			THEN ''AS3''
		ELSE ''N/A''
		END AS ''ActionScriptMode''
	,CASE DurationMode
		WHEN ''0''
			THEN ''Untimed''
		WHEN ''1''
			THEN ''Timed''
		WHEN ''2''
			THEN ''Timed Sections''
		ELSE ''N/A''
		END AS ''Timing''
	,AssessmentDuration  [Duration]
	,SRBonus [Reasonable Adjustments (mins):   standard]
	,SRBonusMaximum  [Reasonable Adjustments (mins):   maximum]	
	,TestFeedBackType.value(''data(/TestFeedbackType/PassFail)[1]'', ''bit'') [Pass/Fail]
	,TestFeedBackType.value(''data(/TestFeedbackType/PercentageMark)[1]'', ''bit'') [Percentage Mark]
	,isnull(TestFeedBackType.value(''data(/TestFeedbackType/AllowFeedbackDuringExam)[1]'', ''bit''),''0'') [Show FeedBack Button During Assessment]
	,TestFeedBackType.value(''data(/TestFeedbackType/AllowSummaryFeedback)[1]'', ''bit'') [Summary Feedback]
	,CASE WHEN TestFeedBackType.value(''data(/TestFeedbackType/SummaryFeedbackType)[1]'', ''bit'') = ''0''
			THEN ''No Review''
		ELSE ''Review Answers''
		END AS ''Summary FeedBack Type''
	
FROM AssessmentTable AT
INNER JOIN AssessmentGroupTable AGT ON AGT.id = AT.AssessmentGroupID
INNER JOIN AssessmentStatusLookupTable ASLT on ASLT.id = AT.AssessmentStatus
INNER JOIN
		(
select ID,
		(
		select 
			a.b.value(''@description'',''nvarchar(20)'') + '' ''
			+ a.b.value(''@modifier'',''char(2)'')  + '' ''
			+ a.b.value(''@value'',''nvarchar(20)'') + '' ''					
		from AssessmentTable L
		CROSS APPLY GradeBoundaries.nodes(''GradeBoundaries/grade'') a(b)
		where l.id = m.ID
			FOR XML PATH(''''), type
				) as GradeBoundary
From AssessmentTable M
group by ID
	) GB
on GB.ID = AT.ID
WHERE ExternalReference NOT LIKE ''%BTL%''
	AND AT.AssessmentName  NOT LIKE ''%BTL%'';
	
END
'





/*
, at.id
	, ASLT.Name
	, gb.GradeBoundary [Grade Boundaries]
	,ISNULL(AssessmentRules.value(''data(PaperRules/Section/@TotalQuestions)[1]'',''int''),''0'') [Total Questions]
	,IsPrintable
	,AllowCloseWithoutSubmit
	,ISNULL(AT.UseAsTemplate,''0'') [Use As Template]	
	,CASE DurationMode
		WHEN ''0''
			THEN ''Untimed''
		WHEN ''1''
			THEN ''Timed''
		WHEN ''2''
			THEN ''Timed Sections''
		ELSE ''N/A''
		END AS ''Timing''
	,AssessmentDuration  [Duration]
	
FROM AssessmentTable AT
INNER JOIN AssessmentGroupTable AGT ON AGT.id = AT.AssessmentGroupID
INNER JOIN AssessmentStatusLookupTable ASLT on ASLT.id = AT.AssessmentStatus
INNER JOIN
		(
select ID,
		(
		select 
			a.b.value(''@description'',''nvarchar(20)'') + '' ''
			+ a.b.value(''@modifier'',''char(2)'')  + '' ''
			+ a.b.value(''@value'',''nvarchar(20)'') + '' ''					
		from AssessmentTable L
		CROSS APPLY GradeBoundaries.nodes(''GradeBoundaries/grade'') a(b)
		where l.id = m.ID
			FOR XML PATH(''''), type
				) as GradeBoundary
From AssessmentTable M
group by ID
	) GB
on GB.ID = AT.ID
WHERE ExternalReference NOT LIKE ''%BTL%''
	AND AT.AssessmentName  NOT LIKE ''%BTL%'';

*/







/*	

	,ISNULL([ShowCandidateDetailsPage], 1) [Requires User Details Confirmation]
	,ISNULL(CandidateDetailsTime, 0) 'Set Duration (min): '''
	,ISNULL(NdaTime, 0) [Set Duration (min):]
	,CASE WHEN [NoOfResitsAllowed] IS NULL THEN 0 ELSE 1 END AS  [Restrict Number of Resits]
	,ISNULL(NoOfResitsAllowed, '0') [Number of Resits]
	,CASE convert(nvarchar(10),ISNULL([CandidateDetailsTime],'0')) when convert(nvarchar(10),'0') then convert(nvarchar(10),'N/A') else convert(nvarchar(10),[CandidateDetailsTime]) end as  [Candidate Details Screen Duration]
	,CASE convert(nvarchar(10),ISNULL([NdaTime],'0')) when convert(nvarchar(10),'0') then convert(nvarchar(10),'N/A') else convert(nvarchar(10),[NdaTime]) END AS 'NDA Screen Duration'''
	,CASE convert(nvarchar(10),ISNULL((AssessmentDuration + [CandidateDetailsTime] + [NdaTime]),'0')) when convert(nvarchar(10),'0') then convert(nvarchar(10),'N/A') else convert(nvarchar(10),(AssessmentDuration + [CandidateDetailsTime] + [NdaTime])) END AS [NDA Screen Duration]
	,[SuppressResultsScreenMark] AS 'Suppress mark from Results screen'
	,[SuppressResultsScreenPercent] AS 'Suppress percent from Results screen'
	,[SuppressResultsScreenResult] AS 'Suppress results from Results screen'
	,[DisableReportingCandidateReport] AS 'Disable Candidate Report'
	,[DisableReportingSummaryReport] AS 'Disable Summary Report'
	,[DisableReportingCandidateBreakdownReport] AS 'Disable Candidate Breakdown Report'
	,[DisableReportingExamBreakdownReport] AS 'Disable Exam Breakdown Report'
	,[DisableReportingResultSlip] AS 'Disable ResultSlip'	
	*/