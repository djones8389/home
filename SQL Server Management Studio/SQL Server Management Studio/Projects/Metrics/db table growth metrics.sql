SELECT --OLD.*
	top 10 
	(new.data_kb + new.index_size) - (old.data_kb + old.index_size)
	,*
FROM (
SELECT 
	[Instance]
      ,[database_name]
      ,[table_name]
      ,[rows]
      --,[reserved_kb]
      ,cast(replace([data_kb], 'KB','') as float) [data_kb]
      ,cast(replace([index_size], 'KB','') as float) [index_size]
      --,[unused_kb]
      ,cast([StartDate] as nvarchar(16)) [StartDate]
      --,[EndDate]
  FROM [PSCollector].[dbo].[DBGrowthMetrics]
where Instance = '430088-BC-SQL\SQL1'--'553092-PRDSQL'
	--and table_name in ('ExamSessionTable','WAREHOUSE_ExamSessionTable')
	and database_name = 'BRITISHCOUNCIL_SecureAssess'
	and cast([StartDate] as nvarchar(16)) = '2017-04-11 07:54'
) OLD

INNER JOIN (

SELECT 
	[Instance]
      ,[database_name]
      ,[table_name]
      ,[rows]
      --,[reserved_kb]
      ,cast(replace([data_kb], 'KB','') as float) [data_kb]
      ,cast(replace([index_size], 'KB','') as float) [index_size]
      --,[unused_kb]
      ,cast([StartDate] as nvarchar(16)) [StartDate]
      --,[EndDate]
  FROM [PSCollector].[dbo].[DBGrowthMetrics]
where Instance = '430088-BC-SQL\SQL1'--'553092-PRDSQL'
	--and table_name in ('ExamSessionTable','WAREHOUSE_ExamSessionTable')
	and database_name = 'BRITISHCOUNCIL_SecureAssess'
	and cast([StartDate] as nvarchar(16)) = '2017-04-24 07:15'
) New

on old.database_name = new.database_name
	and old.table_name = new.table_name

ORDER BY (new.data_kb + new.index_size) - (old.data_kb + old.index_size) DESC;

--order by [Instance]
--      ,[database_name]
--      ,[table_name]
--	  ,[StartDate]
