SET NOCOUNT ON

DECLARE @@DatabaseRoleMemberShip TABLE (
	 Databasename NVARCHAR(100)
	, Username NVARCHAR(100)
	, Rolename NVARCHAR(100)
)

DECLARE @RolesAndPerms NVARCHAR(MAX) = 
'
USE [?];

select ''?'' 
		,u.name 
		,r.name 
from sys.database_role_members RM 
	inner join sys.database_principals U on U.principal_id = RM.member_principal_id
	inner join sys.database_principals R on R.principal_id = RM.role_principal_id
where u.type<>''R''
	and u.name != ''dbo''
	and u.name  like ''%itembank%'' or u.name  like ''%secureassess%'' or u.name  like ''%content%''
	--and u.name like ''UAT%''
	and r.name IN (''db_owner'')
	and ''?'' not IN (''msdb'', ''master'', ''tempdb'')
'

INSERT @@DatabaseRoleMemberShip
EXEC sp_MSforeachdb  @RolesAndPerms


SELECT *
	, 'use ' + quotename(DatabaseName) + '; GRANT SELECT, EXECUTE ON SCHEMA::dbo to ' + QUOTENAME(username) + ';'
	, 'use ' + quotename(DatabaseName) + '; EXEC sp_droprolemember ''db_owner'',' + ''''  + Username + '''' + ';'
	, 'use ' + quotename(DatabaseName) + '; EXEC sp_addrolemember ''db_owner'',' + ''''  + Username + '''' + ';'
FROM @@DatabaseRoleMemberShip order by 1;




/*

--Assign schema permissions

SELECT 'use [' + DatabaseName + ']; GRANT SELECT, EXECUTE ON SCHEMA::dbo to ' + Username + ';'
FROM @@DatabaseRoleMemberShip
order by Databasename ASC;

--Drop DBOwner from it

SELECT 'use [' + DatabaseName + ']; EXEC sp_droprolemember ''db_owner'',' + '''' + username + '''' + ';'
FROM @@DatabaseRoleMemberShip
order by Databasename ASC;

--Readd DBOwner

SELECT 'use [' + DatabaseName + ']; EXEC sp_addrolemember ''db_owner'',' + '''' + username + '''' + ';'
FROM @@DatabaseRoleMemberShip
order by Databasename ASC;

*/