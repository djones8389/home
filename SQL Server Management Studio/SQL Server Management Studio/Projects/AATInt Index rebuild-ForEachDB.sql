use master;

exec sp_MSforeachdb '

USE [?];

IF (DB_ID() > 4)

BEGIN

	DECLARE @DiskSpace TABLE  (
		   Drive CHAR(1) 
		   ,[Space-KB] INT
		   );
       
	INSERT @DiskSpace
	EXEC master..xp_fixeddrives;


	DECLARE @AvailableSpaceOnDisk int;
	DECLARE @SQLBuilder nvarchar(MAX)= '''';

	SELECT @AvailableSpaceOnDisk = [Space-KB] 
	FROM @DiskSpace
	WHERE Drive in (
		select substring(physical_name, 0, charindex('':'', physical_name))
		from sys.database_files
		where type_desc = ''ROWS''
	);

	
SELECT @SQLBuilder += CHAR(13) + RebuildStatement
FROM (
	SELECT
		CASE WHEN cast(LTRIM (STR ((CASE WHEN usedpages > pages THEN (usedpages - pages) ELSE 0 END) * 8, 15, 0)) as int)	
			< ((@AvailableSpaceOnDisk - 5120)*1024) /*5 GB buffer*/
				THEN ''ALTER INDEX '' + QUOTENAME ([IndexName]) + '' ON '' + QUOTENAME ([SchemaName]) + ''.'' + QUOTENAME ([TableName]) + '' REBUILD; ''  
				ELSE ''''
				 end as ''RebuildStatement''
	FROM (

		SELECT distinct
			OBJECT_NAME(s.object_id) [TableName]
			, SCHEMA_NAME(t.schema_id) [SchemaName]
			, i.name [IndexName]
			, (used_page_count) [usedpages]
			,  (
				CASE
					WHEN (s.index_id < 2) THEN (in_row_data_page_count + lob_used_page_count + row_overflow_used_page_count)
					ELSE lob_used_page_count + row_overflow_used_page_count
				END
			) [pages]	
		FROM sys.dm_db_partition_stats AS s
		
			INNER JOIN sys.indexes AS i
				ON s.[object_id] = i.[object_id]
					AND s.[index_id] = i.[index_id]
		
			INNER JOIN sys.tables t
			on t.object_id = s.object_id

			INNER JOIN sys.dm_db_index_physical_stats (DB_ID(), NULL, NULL, NULL, NULL) as b
			on b.object_id = s.object_id
				and b.index_id = i.index_id

			where i.index_id > 0
				and t.type = ''u''
				AND B.avg_fragmentation_in_percent > 30
	) A

) F

PRINT(@SQLBuilder);

END
		
'

















