
/*Re-export to TPM*/

DECLARE @Keycodes TABLE (
	Keycode nvarchar(10)
)


INSERT @Keycodes
SELECT
	Keycode
FROM dbo.Warehouse_ExamSessionTable
WHERE ID IN (1530093);

select SPCE.Score, SPC.PackageScore
FROM BritishCouncil_TestPackage.dbo.ScheduledPackageCandidates SPC
INNER JOIN BritishCouncil_TestPackage.dbo.ScheduledPackageCandidateExams SPCe
on SPCE.Candidate_ScheduledPackageCandidateId = SPC.ScheduledPackageCandidateId
where ScheduledExamRef in (Select Keycode from @Keycodes);

UPDATE BritishCouncil_TestPackage.dbo.ScheduledPackageCandidateExams
set IsCompleted = 0, IsVoided = 0, Score = '0.00'
where ScheduledExamRef in (Select Keycode from @Keycodes)

UPDATE BritishCouncil_TestPackage.dbo.ScheduledPackageCandidates
set IsCompleted = 0, IsVoided = 0
FROM BritishCouncil_TestPackage.dbo.ScheduledPackageCandidates SPC
INNER JOIN BritishCouncil_TestPackage.dbo.ScheduledPackageCandidateExams SPCe
on SPCE.Candidate_ScheduledPackageCandidateId = SPC.ScheduledPackageCandidateId
where ScheduledExamRef in (Select Keycode from @Keycodes);

UPDATE BRITISHCOUNCIL_SecureAssess.dbo.WAREHOUSE_ExamSessionTable
set warehouseTime = getDATE(), WarehouseExamState = 1
where Keycode in (Select Keycode from @Keycodes);

UPDATE BRITISHCOUNCIL_SecureAssess.dbo.WAREHOUSE_ExamSessionTable_Shreded
set warehouseTime = getDATE(), WarehouseExamState = 1
where Keycode in (Select Keycode from @Keycodes);
