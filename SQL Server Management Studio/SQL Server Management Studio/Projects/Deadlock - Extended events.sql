--Check deadlocks are being captured

--SELECT s.name
--	, se.event_name 
--FROM sys.dm_xe_sessions s 
--INNER JOIN sys.dm_xe_session_events se ON (s.address = se.event_session_address) and (event_name = 'xml_deadlock_report') 
--WHERE name = 'system_health'

--View deadlocks 
SELECT  A.*
	, DeadlockXML.value('data(deadlock/victim-list/victimProcess/@id)[1]','nvarchar(100)') victimProcess
FROM (
SELECT  CONVERT(xml, event_data).query('/event/data/value/child::*') DeadlockXML
	, CONVERT(xml, event_data).value('(event[@name="xml_deadlock_report"]/@timestamp)[1]','datetime') as Execution_Time 
FROM sys.fn_xe_file_target_read_file('C:\Program Files\Microsoft SQL Server\MSSQL13.MSSQLSERVER\MSSQL\Log\system_health*.xel', null, null, null)
WHERE object_name like 'xml_deadlock_report'

) A
