declare    
	@filterXml				NVARCHAR(MAX),          
	@originatorId			INT ,          
	@qualificationIdList	NVARCHAR(MAX) ,         
	@closeDefinition		NVARCHAR(MAX) ,         
	@qualLevels				NVARCHAR(MAX),
	@gradeList				NVARCHAR(MAX) 
  		     
set @filterXml=N'<request>
    <filter pageSize="50" pageIndex="0" filterType="AND">
        <i name="surname">%</i>
        <i name="forename">%</i>
        <i name="centreName">%</i>
        <i name="examName">%</i>
        <i name="durationStartRange">0</i>
        <i name="durationEndRange"/>
        <i name="candidateRef">%</i>
        <i name="uln">%</i>
        <i name="keycode">%JK7MD75S%</i>
        <i name="completedDateStartRange"/>
        <i name="completedDateEndRange"/>
        <i name="pass">1</i>
        <i name="fail">1</i>
        <i name="void">1</i>
        <i name="close">1</i>
        <i name="extraTime">1</i>
        <i name="standardTime">1</i>
        <i name="sorting">completedDate DESC, qualification ASC, examName ASC, candidateSurname ASC, candidateForename ASC</i>
        <i name="reMarkStatus">0,1,2</i>
        <i name="centreCode">%</i>
        <i name="assignedMarker"/>
        <i name="assignedModerator"/>
        <i name="warehouseExamState">1,2,3,4,5,6,99</i>
        <i name="examVersionRef">%</i>
    </filter>
</request>'
set @originatorId=1
set @qualificationIdList=N''
set @qualLevels=N'103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121'
set @closeDefinition=N'0 marks'
set @gradeList=N''

BEGIN                  
	SET NOCOUNT ON;          
          
	-- If the warehouse is empty a strange error occurs when the secure assess code executes this stored procedure.          
	-- This code is in place to protect from this and return an empty page - DN         
	If NOT EXISTS(select top(1)ID from WAREHOUSE_ScheduledExamsTable)          
		BEGIN          
			SELECT '0' as '@errorCode',          
			(          
				SELECT '1' as '@pageIndex', '50' as '@pageSize', '0' as '@totalCount', '0' as '@totalRecords', @closeDefinition as '@closeDef'          
				FOR XML PATH('return') ,TYPE          
			)          
			FOR XML PATH('result')          
			RETURN          
		END         
          
	BEGIN TRY          
		SET DATEFORMAT dmy;   
		       
		-- Paging vars                 
		DECLARE	@PageSize						INT,
				@PageNumber						INT,          
				@TotalResultCount				INT,           
				-- Select vars          
				@filterType						VARCHAR(3),         
				@sorting						VARCHAR(MAX),          
				@whereclause					VARCHAR(MAX),                 
				-- Variables for Dynamic Sql
				@surname						NVARCHAR(MAX),          
				@centreName						NVARCHAR(MAX),          
				@examName						NVARCHAR(MAX),          
				@completedDateStartRange		NVARCHAR(MAX),          
				@completedDateEndRange			NVARCHAR(MAX),
				@pass							BIT,
				@fail							BIT,
				@close							BIT,          
				@void							BIT,          
				@standardTime					BIT,          
				@extraTime						BIT,
				@reMarkStates					NVARCHAR(10),
				@centreCode						NVARCHAR(MAX),          
				@candidateRef					NVARCHAR(300),        
				@uln							NVARCHAR(300),
				@keycode						NVARCHAR(300),      
				@warehouseExamStates			NVARCHAR(100),
				@examVersionRef					NVARCHAR(MAX),
				@forename						NVARCHAR(MAX),
				@durationStartRange				NVARCHAR(MAX),
				@durationEndRange				NVARCHAR(MAX),
				@durationClause					NVARCHAR(MAX),
				@assignedMarkerUser				NVARCHAR(50),
				@assignedModeratorUser			NVARCHAR(50),
				@myQualificationClause			NVARCHAR(MAX),          
				@mySelectQualLevelsStatemement	NVARCHAR(MAX),
				@myNumQualLevels				INT,
				@myGradeClause					NVARCHAR(MAX),
				@myQualLevelsClause				NVARCHAR(MAX),          
				@myUlnClause					NVARCHAR(MAX),       
				@myCandidateRefClause			NVARCHAR(MAX),        
				@myKeyCodeClause				NVARCHAR(MAX),   
				@myCompletedDateStartClause		NVARCHAR(MAX),
				@myCompletedDateEndClause		NVARCHAR(MAX),
				@calculatePagingRequirements	NVARCHAR(MAX),
				@rowCount						INT,
				@startCounter					INT,
				@pageCount						DECIMAL(20, 5);
				
		CREATE TABLE #tempWAREHOUSEExamDetailsSearchTable (rowNumber[INT], id[INT]);
		-- Open the filter XML passed into the sp          
		DECLARE @hdoc INT;          
		EXEC sp_xml_preparedocument @hdoc OUTPUT, @filterXml;        
           
		SELECT  @PageNumber					= pageIndex,
				@PageSize					= pageSize,
				@surname					= [surname],
				@centreName					= [centreName],
				@examName					= [examName],
				@completedDateStartRange	= [completedDateStartRange],
				@completedDateEndRange		= [completedDateEndRange],
				@pass						= [pass],
				@fail						= [fail],
				@close						= [close],
				@void						= [void],
				@standardTime				= [standardTime],
				@extraTime					= [extraTime],
				@sorting					= [sorting],
				@reMarkStates				= [reMarkStates],
				@centreCode					= [centreCode],
				@candidateRef				= [candidateRef],
				@uln						= [uln],
				@keycode					= [keycode],
				@warehouseExamStates		= [warehouseExamState],
				@examVersionRef				= [examVersionRef],
				@forename					= [forename],
				@durationStartRange			= [durationStartRange],
				@durationEndRange			= [durationEndRange],
				@assignedMarkerUser			= [assignedMarker],
				@assignedModeratorUser		= [assignedModerator]
		FROM          
		OPENXML(@hdoc, '/request/filter',1)
			WITH	
			(          
				pageSize					INT,
				pageIndex					INT,
				[surname]					NVARCHAR(MAX)	'./i[@name="surname"]',
				[centreName]				NVARCHAR(MAX)	'./i[@name="centreName"]',
				[examName]					NVARCHAR(MAX)	'./i[@name="examName"]',
				[completedDateStartRange]	NVARCHAR(MAX)	'./i[@name="completedDateStartRange"]',
				[completedDateEndRange]		NVARCHAR(MAX)	'./i[@name="completedDateEndRange"]',
				[pass]						BIT				'./i[@name="pass"]',
				[fail]						BIT				'./i[@name="fail"]',
				[close]						BIT				'./i[@name="close"]',
				[void]						BIT				'./i[@name="void"]',
				[standardTime]				BIT				'./i[@name="standardTime"]',
				[extraTime]					BIT				'./i[@name="extraTime"]',
				[sorting]					VARCHAR(MAX)	'./i[@name="sorting"]',
				[reMarkStates]				NVARCHAR(10)	'./i[@name="reMarkStatus"]',
				[centreCode]				NVARCHAR(MAX)   './i[@name="centreCode"]',
				[candidateRef]				NVARCHAR(300)   './i[@name="candidateRef"]',
				[uln]						NVARCHAR(300)   './i[@name="uln"]',
				[keycode]					NVARCHAR(300)   './i[@name="keycode"]',
				[warehouseExamState]		NVARCHAR(100)	'./i[@name="warehouseExamState"]',
				[examVersionRef]			NVARCHAR(MAX)	'./i[@name="examVersionRef"]',
				[forename]					NVARCHAR(MAX)	'./i[@name="forename"]',
				[durationStartRange]		NVARCHAR(MAX)	'./i[@name="durationStartRange"]',
				[durationEndRange]			NVARCHAR(MAX)	'./i[@name="durationEndRange"]',
				[assignedMarker]			varchar(max)	'./i[@name="assignedMarker"]',
				[assignedModerator]			varchar(max)	'./i[@name="assignedModerator"]'
			);      
                     
	-- Clean the filter doc from memory          
	EXEC sp_xml_removedocument @hdoc;          
    
	DECLARE @myAssignedMarkerWhereClause nvarchar(max) =''
	IF(LEN(@assignedMarkerUser) > 0)
		BEGIN
			SET @myAssignedMarkerWhereClause = ' AND WAREHOUSE_Users_Marker.Username LIKE ''' + @assignedMarkerUser + ''''
		END
	
	DECLARE @myAssignedModeratorWhereClause nvarchar(max) =''
	IF(LEN(@assignedModeratorUser) > 0)
		BEGIN
			SET @myAssignedModeratorWhereClause = ' AND WAREHOUSE_Users_Moderator.Username LIKE ''' + @assignedModeratorUser + ''''
		END
			
	SET @myQualificationClause = '';
	          
	IF(LEN(@qualificationIdList) > 0)          
		SET @myQualificationClause = ' AND (WAREHOUSE_ExamSessionTable_Shreded.qualificationID IN (' + @qualificationIdList + ')) ';          
            
	SET @mySelectQualLevelsStatemement = 'SET @myNumQualLevels = (SELECT COUNT(DISTINCT level) FROM QualificationLevelsTable  WHERE Level in (' + @qualLevels + '))';          
	SET @myGradeClause = '';
	IF LEN(@gradeList) > 0
		SET @myGradeClause = @myGradeClause + ' AND (originalGrade IN (' + @gradeList + '))';  
			         
	SET @void = 1; 
  
	SET @durationClause = '';
	IF @durationEndRange <> ''
		SET @durationClause = 'AND WAREHOUSE_ExamSessionTable_Shreded.scheduledDurationValue >=' + @durationStartRange + ' AND WAREHOUSE_ExamSessionTable_Shreded.scheduledDurationValue <= '+ @durationEndRange; 
		
	EXEC sp_executesql @mySelectQualLevelsStatemement, N'@myNumQualLevels INT OUTPUT', @myNumQualLevels OUTPUT;          
           
	SET @myQualLevelsClause = '';          
	IF(@myNumQualLevels < (SELECT COUNT(level) FROM QualificationLevelsTable))          
		SET @myQualLevelsClause = ' AND WAREHOUSE_ExamSessionTable_Shreded.qualificationLevel IN (' + @qualLevels + ') ';          
    
    -- PREPARE
	SET @sorting = REPLACE(@sorting, 'candidateReference', 'candidateRef'); -- change candidateReference to candidateRef so both filter names can be passed to the same effect
    
	--REPLACE SORTING WITH DB COLS          
	SET @sorting = REPLACE(@sorting, 'qualification', 'WAREHOUSE_ExamSessionTable_Shreded.qualificationName');
	SET @sorting = REPLACE(@sorting, 'examName', 'WAREHOUSE_ExamSessionTable_Shreded.examName'); 
	SET @sorting = REPLACE(@sorting, 'candidateSurname', '[Candidate].Surname');
	SET @sorting = REPLACE(@sorting, 'candidateForename', '[Candidate].Forename');
	SET @sorting = REPLACE(@sorting, 'appeal', 'WAREHOUSE_ExamSessionTable.appeal');
	SET @sorting = REPLACE(@sorting, 'remarkStatus', 'WAREHOUSE_ExamSessionTable.remarkStatus');
	SET @sorting = REPLACE(@sorting, 'ExportedToIntegration', 'WAREHOUSE_ExamSessionTable.ExportedToIntegration');
	SET @sorting = REPLACE(@sorting, 'completedDate', 'SubmittedDate');
	SET @sorting = REPLACE(@sorting, 'centreName', 'WAREHOUSE_CentreTable.centreName');
	SET @sorting = REPLACE(@sorting, 'candidateRef', '[Candidate].candidateRef');
	SET @sorting = REPLACE(@sorting, 'uln', '[Candidate].uln');
	SET @sorting = REPLACE(@sorting, 'keycode', 'WAREHOUSE_ExamSessionTable.keycode');
	SET @sorting = REPLACE(@sorting, 'resultData', 'examResult');
	SET @sorting = REPLACE(@sorting, 'duration', 'scheduledDurationValue');
	SET @sorting = REPLACE(@sorting, 'warehouseExamState', 'WAREHOUSE_ExamSessionTable_Shreded.warehouseExamState');
	SET @sorting = REPLACE(@sorting, 'assignedModerator', 'WAREHOUSE_Users_Moderator.Username');
	SET @sorting = REPLACE(@sorting, 'assignedMarker', 'WAREHOUSE_Users_Marker.Username');
	SET @sorting = REPLACE(@sorting, 'language', 'WAREHOUSE_ExamSessionTable_Shreded.language');
	
	IF CHARINDEX('originalGrade', @sorting) > 0
		AND (
			CHARINDEX('Voided', @gradeList) > 0
			OR @gradeList = ''
			) -- Check if sorting on grade, If void is included need to amend the sorting.
	BEGIN
		SET @sorting = REPLACE(@sorting, 'originalGrade', 'OriginalGradewithVoidReason');			
	END	
	
	SET @myUlnClause = '';        
	IF @uln <> '%'        
		SET @myUlnClause = 'AND  ([Candidate].ULN LIKE ''' + @uln + ''')';
          
	SET @myCandidateRefClause = ''        
	IF @candidateRef <> '%'
		SET @myCandidateRefClause = 'AND [Candidate].CandidateRef LIKE ''' + @candidateRef + '''';        
 
	SET @myKeyCodeClause = ''        
	IF @keycode <> '%'        
		SET @myKeyCodeClause = 'AND WAREHOUSE_ExamSessionTable.KeyCode LIKE ''' + @keycode + '''';
        
    SET @WHERECLAUSE =  ' WHERE    
			(([Candidate].Surname LIKE ''' + @surname + ''')           
				AND (WAREHOUSE_CentreTable.centreName LIKE '''+ @centreName + ''')           
				AND (WAREHOUSE_ExamSessionTable_Shreded.examName LIKE '''+ @examName + ''')         
				AND (WAREHOUSE_CentreTable.CentreCode Like ''' + @centreCode + ''')) ';     
				   
	IF LEN(@myCandidateRefClause) > 0        
		SET @whereclause = @whereclause + @myCandidateRefClause;
	IF LEN(@myUlnClause) > 0        
		SET @whereclause = @whereclause + @myUlnClause;
	IF LEN(@myKeyCodeClause) > 0        
		SET @whereclause = @whereclause + @myKeyCodeClause;
	IF @examVersionRef <> '%'
		SET @whereclause = @whereclause + ' AND WAREHOUSE_ExamSessionTable_Shreded.examVersionRef LIKE '''+@examVersionRef + '''';
    IF @forename <> '%'      
		SET @whereclause = @whereclause + ' AND [Candidate].Forename LIKE ''' + @forename + '''';
	IF LEN(@durationClause) > 0
		SET @whereclause = @whereclause + @durationClause;
	IF LEN(@myAssignedMarkerWhereClause) > 0
		SET @whereclause = @whereclause + @myAssignedMarkerWhereClause
	IF LEN(@myAssignedModeratorWhereClause) > 0
		SET @whereclause = @whereclause + @myAssignedModeratorWhereClause
          
    SET @myCompletedDateStartClause = '';
    IF (@completedDateStartRange != '')
		SET @myCompletedDateStartClause = 'AND WAREHOUSE_ExamSessionTable_Shreded.submittedDate >= Convert(datetime, ''' + @completedDateStartRange +''')';
		
	SET @myCompletedDateEndClause = '';
    IF (@completedDateEndRange != '')
		SET @myCompletedDateEndClause = 'AND WAREHOUSE_ExamSessionTable_Shreded.submittedDate <= Convert(datetime, ''' + @completedDateEndRange +''')';
           
    SET @whereclause = --@whereclause + 'AND (WAREHOUSE_ExamSessionTable_Shreded.originatorId = ''' + CONVERT(NVARCHAR(MAX),@originatorId) + ''')'          
        -- + @myQualificationClause + @myQualLevelsClause + @myGradeClause+  @myCompletedDateStartClause + @myCompletedDateEndClause 
		 
		 --+
         '	AND WAREHOUSE_ExamSessionTable.reMarkStatus IN (' + @reMarkStates + ')          
			AND WAREHOUSE_ExamSessionTable_Shreded.WarehouseExamState IN (' + @warehouseExamStates + ')                    
			AND (
					(
						(' + CONVERT(NCHAR(1),@pass) + ' =  1) 
						AND	(
								WAREHOUSE_ExamSessionTable_Shreded.previousExamState = 12 
								OR (
										WAREHOUSE_ExamSessionTable_Shreded.previousExamState = 9 
										AND (
												WAREHOUSE_ExamSessionTable.ExportToSecureMarker = 1
												OR WAREHOUSE_ExamSessionTable_Shreded.WarehouseExamState = 6
											)
									)
								OR WAREHOUSE_ExamSessionTable_Shreded.previousExamState = 17 
							) 
						AND CAST(WAREHOUSE_ExamSessionTable_Shreded.passValue AS varchar(1000))  = 1 
					)  
					OR	(
							(' + CONVERT(NCHAR(1),@fail) + ' =  1) 
							AND (
									WAREHOUSE_ExamSessionTable_Shreded.previousExamState = 12 
									OR  (
											WAREHOUSE_ExamSessionTable_Shreded.previousExamState = 9 
											AND (
													WAREHOUSE_ExamSessionTable.ExportToSecureMarker = 1
													OR WAREHOUSE_ExamSessionTable_Shreded.WarehouseExamState = 6
												)
										)
									OR WAREHOUSE_ExamSessionTable_Shreded.previousExamState = 17 
								) 
							AND CAST(WAREHOUSE_ExamSessionTable_Shreded.passValue AS varchar(1000)) = 0 
							AND CAST(WAREHOUSE_ExamSessionTable_Shreded.closeValue AS varchar(1000)) != 1 
						) 
					OR	(
							(' + CONVERT(NCHAR(1),@close) + ' =  1) 
							AND (
										WAREHOUSE_ExamSessionTable_Shreded.previousExamState = 12 
									OR  (
											WAREHOUSE_ExamSessionTable_Shreded.previousExamState = 9 
											AND (
												WAREHOUSE_ExamSessionTable.ExportToSecureMarker = 1
												OR WAREHOUSE_ExamSessionTable_Shreded.WarehouseExamState = 6
											)
										)
									OR WAREHOUSE_ExamSessionTable_Shreded.previousExamState = 17 
								) 
							AND CAST(WAREHOUSE_ExamSessionTable_Shreded.passValue AS varchar(1000)) = 0  
							AND CAST(WAREHOUSE_ExamSessionTable_Shreded.closeValue AS varchar(1000)) = 1 
						) -- CLOSE Tick Box          
					OR  (
							(' + CONVERT(NCHAR(1),@void) + ' =  1) 
							AND
							( 
								WAREHOUSE_ExamSessionTable_Shreded.previousExamState = 10 
								OR WAREHOUSE_ExamSessionTable_Shreded.previousExamState = 17 
							)
						) -- VOID Tick Box                              
				)          
				AND (          
						(((' + CONVERT(NVARCHAR(MAX),@standardTime) + ' =  1)  
						AND (WAREHOUSE_ExamSessionTable_Shreded.actualDuration = WAREHOUSE_ExamSessionTable_Shreded.scheduledDurationValue))
					)          
				OR  (
						((' + CONVERT(NVARCHAR(MAX),@extraTime) + ' =  1)     
						AND (WAREHOUSE_ExamSessionTable_Shreded.actualDuration != WAREHOUSE_ExamSessionTable_Shreded.scheduledDurationValue))
					)          
			  )';                
	
	IF CAST(@PageNumber AS VARCHAR(MAX)) > 0
		SET @startCounter = ((@PageSize * @PageNumber) + 1);
	ELSE
		SET @startCounter = 0;
		
	SET @calculatePagingRequirements = N'
	INSERT INTO #tempWAREHOUSEExamDetailsSearchTable
		SELECT ROW_NUMBER() OVER (ORDER BY ' + @sorting + '), WAREHOUSE_ExamSessionTable_Shreded.examsessionid
		FROM
			WAREHOUSE_ExamSessionTable_Shreded
		LEFT OUTER JOIN
			WAREHOUSE_ExamSessionTable on WAREHOUSE_ExamSessionTable.id = WAREHOUSE_ExamSessionTable_Shreded.examsessionid
		INNER JOIN WAREHOUSE_ScheduledExamsTable
			ON WAREHOUSE_ExamSessionTable.WAREHOUSEScheduledExamID = WAREHOUSE_ScheduledExamsTable.ID
		INNER JOIN WAREHOUSE_CentreTable
			ON WAREHOUSE_ScheduledExamsTable.WAREHOUSECentreID = WAREHOUSE_CentreTable.ID
		INNER JOIN WAREHOUSE_UserTable AS [Candidate]
			ON [Candidate].Id = WAREHOUSE_ExamSessionTable.WAREHOUSEUserId
		LEFT OUTER JOIN
			WAREHOUSE_UserTable AS WAREHOUSE_Users_Moderator ON WAREHOUSE_ExamSessionTable.AssignedModeratorUserID = WAREHOUSE_Users_Moderator.ID 
		LEFT OUTER JOIN
			WAREHOUSE_UserTable AS WAREHOUSE_Users_Marker ON WAREHOUSE_ExamSessionTable.AssignedMarkerUserID = WAREHOUSE_Users_Marker.ID
		LEFT JOIN dbo.voidJustificationLookupTable VoidTable ON VoidTable.ID =  voidJustificationLookupTableId
		' + @whereclause
	+ 'SET @TotalResultCount = @@ROWCOUNT;';

	EXEC sp_executesql @calculatePagingRequirements,  N'@TotalResultCount INT OUTPUT', @TotalResultCount OUTPUT;

	-- Work out the actual pages based on a float as int will already be rounded therefore not showing the last page in some cases
	SET @pageCount = CAST(@TotalResultCount AS [float]) / CAST(@PageSize AS [float]);

	

	SELECT '0' as '@errorCode',          
	(          
		SELECT @PageNumber as '@pageIndex', @PageSize as '@pageSize', CEILING(@pageCount) as '@totalCount', @TotalResultCount as '@totalRecords', @closeDefinition as '@closeDef',          
		(     
			SELECT	[WAREHOUSE_ExamSessionTable_Shreded].[examSessionId]							AS 'examSessionId',
					[WAREHOUSE_ExamSessionTable_Shreded].[examName]									AS 'examName',
					[WAREHOUSE_ExamSessionTable_Shreded].[examRef]									AS 'examRef',
					[WAREHOUSE_ExamSessionTable_Shreded].[examVersionName]							AS 'examVersionName',
					[WAREHOUSE_ExamSessionTable_Shreded].[examVersionRef]							AS 'examVersionRef',
					REPLACE([WAREHOUSE_ExamSessionTable_Shreded].[foreName], '&apos;', '''')		AS 'candidateForename',
					REPLACE([WAREHOUSE_ExamSessionTable_Shreded].[surName], '&apos;', '''')			AS 'candidateSurname',
					[WAREHOUSE_ExamSessionTable_Shreded].[centreName]								AS 'centreName',
					[WAREHOUSE_ExamSessionTable_Shreded].[centreId]									AS 'centreId',
					[WAREHOUSE_ExamSessionTable_Shreded].[qualificationName]						AS 'qualification',
					[WAREHOUSE_ExamSessionTable_Shreded].[qualificationLevel]						AS 'qualificationLevel',
					CONVERT(VARCHAR(20),[WAREHOUSE_ExamSessionTable_Shreded].[submittedDate],113)	AS 'completedDate',
					[WAREHOUSE_ExamSessionTable_Shreded].[resultData]								AS 'resultData',
					[WAREHOUSE_ExamSessionTable_Shreded].[scheduledDurationValue]					AS 'scheduledDuration',
					[WAREHOUSE_UserTable].[CandidateRef]											AS 'candidateRef',
					ISNULL([WAREHOUSE_UserTable].[uln],'')											AS 'uln',
					[WAREHOUSE_ExamSessionTable].[keycode]											AS 'keycode',
					[WAREHOUSE_ExamSessionTable_Shreded].[OriginatorId]								AS 'originatorId',
					[WAREHOUSE_ExamSessionTable_Shreded].[PreviousExamState]						AS 'previousExamState',
					[WAREHOUSE_ExamSessionTable_Shreded].[ExamStateInformation]						AS 'information',
					[WAREHOUSE_ExamSessionTable_Shreded].[examResult]								AS 'examResult',
					ISNULL([WAREHOUSE_ExamSessionTable].[appeal], 0)								AS 'appeal',
					[WAREHOUSE_ExamSessionTable].[reMarkStatus]										AS 'reMarkStatus',
					[WAREHOUSE_CentreTable].[centreCode]											AS 'centreCode',
					[WAREHOUSE_ExamSessionTable].[AllowPackageDelivery]								AS 'AllowPackageDelivery',
					ISNULL([WAREHOUSE_ExamSessionTable].[ExportedToIntegration], 0)					AS 'ExportedToIntegration',
					[WAREHOUSE_ExamSessionTable_Shreded].[WarehouseExamState]						AS 'WarehouseExamState',
					LTRIM(RTRIM([WAREHOUSE_ExamSessionTable_Shreded].[language]))					AS 'language',
					[WAREHOUSE_ExamSessionTable_Shreded].[originalGrade],
					[WAREHOUSE_ExamSessionTable_Shreded].[adjustedGrade],
					[WAREHOUSE_ExamSessionTable_Shreded].[userPercentage],
					[WAREHOUSE_ExamSessionTable_Shreded].[userMark],
					[WAREHOUSE_ExamSessionTable].[scaleScore],
					[WAREHOUSE_ExamSessionTable].[EnableOverrideMarking],
					isnull([WAREHOUSE_ExamSessionTable_Shreded].[TargetedForVoid],'')				AS 'TargetedForVoid',
					[WAREHOUSE_ExamSessionTable_Shreded].IsExternal,
					[WAREHOUSE_ExamSessionTable].IsProjectBased										AS 'projectBased',
					ISNULL([WAREHOUSE_Users_Marker].[Username],'')									AS 'assignedMarker',
					ISNULL([WAREHOUSE_Users_Moderator].[Username],'')								AS 'assignedModerator'
			FROM #tempWAREHOUSEExamDetailsSearchTable temp
				INNER JOIN WAREHOUSE_ExamSessionTable_Shreded
					ON temp.rowNumber BETWEEN (@startCounter) and ((CAST(@PageNumber AS VARCHAR(MAX)) + 1) * CAST(@PageSize AS VARCHAR(MAX)))
					AND temp.id = WAREHOUSE_ExamSessionTable_Shreded.examsessionid
				INNER JOIN WAREHOUSE_ExamSessionTable
					ON WAREHOUSE_ExamSessionTable.id = WAREHOUSE_ExamSessionTable_Shreded.examsessionid
				INNER JOIN WAREHOUSE_UserTable
					ON WAREHOUSE_ExamSessionTable.WAREHOUSEUserID = WAREHOUSE_UserTable.ID
				INNER JOIN WAREHOUSE_ScheduledExamsTable
					ON WAREHOUSE_ExamSessionTable.WAREHOUSEScheduledExamID = WAREHOUSE_ScheduledExamsTable.ID
				INNER JOIN WAREHOUSE_CentreTable
					ON WAREHOUSE_ScheduledExamsTable.WAREHOUSECentreID = WAREHOUSE_CentreTable.ID
				LEFT OUTER JOIN WAREHOUSE_UserTable AS WAREHOUSE_Users_Moderator
					ON WAREHOUSE_ExamSessionTable.AssignedModeratorUserID = WAREHOUSE_Users_Moderator.ID 
				LEFT OUTER JOIN WAREHOUSE_UserTable AS WAREHOUSE_Users_Marker
					ON WAREHOUSE_ExamSessionTable.AssignedMarkerUserID = WAREHOUSE_Users_Marker.ID
			ORDER BY rowNumber ASC
			FOR XML PATH('exam'),TYPE
		)FOR XML PATH('return') ,TYPE          
	)FOR XML PATH('result');   
	
	DROP TABLE #tempWAREHOUSEExamDetailsSearchTable;

	END TRY          
	BEGIN CATCH          
		EXEC sa_SHARED_GetErrorDetails_sp          
	END CATCH;           
END

