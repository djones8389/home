use master

/*

PPD_AAT_ContentProducer
PPD_AAT_ItemBank
PPD_AAT_SecureAssess
PPD_AAT_SecureMarker

PRV_Evolve_CPProjectAdmin
PRV_Evolve_ItemBank
PRV_Evolve_OpenAssessAdmin
PRV_Evolve_SecureAssess

PRV_SQA_CPProjectAdmin
PRV_SQA_ItemBank
PRV_SQA_SecureAssess
STG_SQA_CPProjectAdmin
STG_SQA_ItemBank
STG_SQA_SecureAssess
STG_SQA_OpenAssess

STG_WJEC_ContentProducer
STG_WJEC_ItemBank
STG_WJEC_OpenAssess
STG_WJEC_SecureAssess

*/

SELECT
    db.name AS DBName
	--, type_desc AS FileType
	--, Physical_Name AS Location
	,mf.size/128 as Size_in_MB
FROM   sys.master_files mf
INNER JOIN sys.databases db 
ON db.database_id = mf.database_id

where db.name in ('PPD_AAT_ContentProducer','PPD_AAT_ItemBank','PPD_AAT_SecureAssess','PPD_AAT_SecureMarker','PRV_Evolve_CPProjectAdmin','PRV_Evolve_ItemBank','PRV_Evolve_OpenAssessAdmin','PRV_Evolve_SecureAssess','PRV_SQA_CPProjectAdmin','PRV_SQA_ItemBank','PRV_SQA_SecureAssess','STG_SQA_CPProjectAdmin','STG_SQA_ItemBank','STG_SQA_SecureAssess','STG_SQA_OpenAssess','STG_WJEC_ContentProducer','STG_WJEC_ItemBank','STG_WJEC_OpenAssess','STG_WJEC_SecureAssess')
	and type_desc = 'ROWS'
ORDER BY DBName
