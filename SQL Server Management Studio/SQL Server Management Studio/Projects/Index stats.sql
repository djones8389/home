SELECT 
	B.name AS TableName
	, C.name AS IndexName
	, C.fill_factor AS IndexFillFactor
	, D.rows AS RowsCount
	, A.avg_fragmentation_in_percent
	, A.page_count
	, 'ALTER INDEX ' + C.name +  ' ON ' + 'dbo' + '.' + B.name + ' REBUILD;'
	, (E.used_page_count * 8) IndexInKB
	, (E.used_page_count * 8)/1024 IndexInMB
FROM sys.dm_db_index_physical_stats(DB_ID(),NULL,NULL,NULL,NULL) A
INNER JOIN sys.objects B
ON A.object_id = B.object_id
INNER JOIN sys.indexes C
ON B.object_id = C.object_id AND A.index_id = C.index_id
INNER JOIN sys.partitions D
ON B.object_id = D.object_id AND A.index_id = D.index_id
INNER JOIN sys.dm_db_partition_stats E
on E.object_id = b.object_id
	and e.index_id = c.index_id
WHERE C.index_id > 0
	--and A.avg_fragmentation_in_percent > 30
ORDER BY E.used_page_count desc;




--BACKUP DATABASE [Demo_SDWH_Perf2] TO DISK = N'D:\Backup\Demo_SDWH_Perf2.2016.05.09.bak' 
--WITH NAME = N'Demo_SDWH_Perf2- Database Backup', COPY_ONLY, NOFORMAT, NOINIT, SKIP, COMPRESSION, NOREWIND, NOUNLOAD, STATS = 10;