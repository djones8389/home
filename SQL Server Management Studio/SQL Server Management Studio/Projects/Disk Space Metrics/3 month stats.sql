IF OBJECT_ID('tempdb..#AATStats') IS NOT NULL DROP TABLE #AATStats;


CREATE TABLE #AATStats (
	server_name nvarchar(100)
	,database_name nvarchar(100)
	,schema_name nvarchar(100)
	,table_name nvarchar(100)
	,row_count nvarchar(100)
	,reserved_kb int
	,data_kb int
	,index_kb int
	,unused_kb int
	,collection_date datetime
) 

BULK INSERT #AATStats
from 'C:\Data.csv'
with (fieldterminator = ',', rowterminator = '\n')
go


select 
	CONVERT(VARCHAR(50), [Date], 103) [Collection Date]
	,SUM(totalkb)/1024/1024 GB
from (
SELECT
	collection_date as [Date] --SUBSTRING(convert(nvarchar(100),collection_date), 0, 7) 
	,SUM(data_kb) + SUM(index_kb) [Totalkb]
	, collection_date
FROM #AATStats 
where  database_name = 'AAT_SecureAssess'
 group by collection_date
	
	) A
	group by Date
	order by Date


DECLARE @Date1 datetime = '11 January 2016'
DECLARE @Date2 datetime = '04 April 2016'

SELECT DATEDIFF(DAY,@Date1, @Date2)

--38gb in 84 days
--~13 gb per month