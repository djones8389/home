SET NOCOUNT ON;

IF EXISTS (
SELECT 1 from sys.configurations where Name = 'xp_cmdshell' and value_in_use = 0
)
	PRINT 'You need to enable CMDShell, or copy files manually!!'

IF (SELECT CONVERT(SYSNAME, SERVERPROPERTY('instancedefaultdatapath'))) IS NULL
	PRINT 'Default Data Path isn''t defined, set this in the variable manually'
	
IF (SELECT CONVERT(SYSNAME, SERVERPROPERTY('instancedefaultlogpath'))) IS NULL
	PRINT 'Default Log Path isn''t defined, set this in the variable manually'

DECLARE @SQL NVARCHAR(MAX) = ''
	  , @OnlineOffline NVARCHAR(MAX) = '';

DECLARE @DefaultDataLoc NVARCHAR(1000) = CONVERT(SYSNAME, SERVERPROPERTY('instancedefaultdatapath'))
	  ,  @DefaultLogLoc NVARCHAR(1000) = CONVERT(SYSNAME, SERVERPROPERTY('instancedefaultlogpath'));

SELECT @DefaultDataLoc = 'E:\Data\'
SELECT @DefaultLogLoc = 'E:\Log\'

DECLARE @Tables TABLE(databaseName nvarchar(100), [LogicalName] nvarchar(250), [CurrentLocation] nvarchar(1000), [NewLocation] nvarchar(1000))
INSERT @Tables
SELECT D.[Name] [DatabaseName]
	  , F.Name  [LogicalName]
	  , F.physical_name [CurrentLocation]
	  ,  case f.type_desc when 'ROWS' then   @DefaultDataLoc + SUBSTRING(F.physical_name,LEN(REVERSE(SUBSTRING(REVERSE(F.physical_name), CHARINDEX('\',REVERSE(F.physical_name)), LEN(F.physical_name))))+1,LEN(F.physical_name))
					 when 'LOG' then   @DefaultLogLoc + SUBSTRING(F.physical_name,LEN(REVERSE(SUBSTRING(REVERSE(F.physical_name), CHARINDEX('\',REVERSE(F.physical_name)), LEN(F.physical_name))))+1,LEN(F.physical_name))
	END as [NewLocation]
FROM sys.master_files as F
inner join (
	SELECT database_id, [Name]
	FROM sys.databases
	) as D
on D.database_id = F.database_id 
where d.database_id > 4
	--and (
	--	F.physical_name not like @DefaultDataLoc
	--	or
	--	F.physical_name not like @DefaultLogLoc
	--	)

SELECT CurrentLocation
	, COALESCE(b.command,b.command2)
FROM (
	select CurrentLocation
		, CASE R WHEN 1 then 'ALTER DATABASE ' + QUOTENAME(databaseName) + ' SET OFFLINE ' + command  end as  [Command]
		, CASE R when 2 then command + 'ALTER DATABASE ' + QUOTENAME(databaseName) + ' SET ONLINE;' end as  [Command2]
	FROM (

	SELECT CurrentLocation
		,databaseName
		, 'ALTER DATABASE ' + QUOTENAME (databaseName) + ' MODIFY FILE (NAME = ''' + LogicalName + ''',' + 'Filename = ''' + NewLocation + ''');'
			+ ' EXECUTE xp_cmdshell ''' + 'move /Y ' + CurrentLocation + '  ' +  NewLocation + '''' [Command]
			, ROW_NUMBER() OVER (PARTITION BY databaseName ORDER BY databaseName) R
	FROM @Tables

	) A
) B
where  COALESCE(b.command,b.command2) is not null
	
