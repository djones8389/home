use master
SET NOCOUNT ON;

IF OBJECT_ID('tempdb..##Global') IS NOT NULL DROP TABLE ##Global;
IF OBJECT_ID('tempdb..##SpecialItems') IS NOT NULL DROP TABLE ##SpecialItems;

CREATE TABLE ##Global (

	Servername sysname
    , DatabaseName sysname
	, ProjectID int
	, ProjectName nvarchar(200)
	, ItemID nvarchar(20)
	, setting nvarchar(200)	
	, SpecialItemCount int
	, TotalCount bigint
	, [% of Items] nvarchar(20)
);

CREATE TABLE ##SpecialItems (

	ItemID nvarchar(20)
	, Setting nvarchar(200)
)


INSERT ##SpecialItems
exec sp_msforeachdb '	
use [?];
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;	
if(''?'' like ''%CPProjectAdmin%'' or ''?'' like ''%ContentProducer%'')	
SELECT [Outer].*
FROM (
	SELECT Scenes.ItemID
		, ''Scenes'' [Setting]
	FROM (
	select substring(id,0,charindex(''s'',id)) ItemID
		, max(substring(id,charindex(''s'',id)+1,10)) Scene
	from SceneTable
	where substring(id,charindex(''s'',id)+1,10) <> 1
	group by substring(id,0,charindex(''s'',id))
	) Scenes

	UNION ALL

	SELECT DISTINCT substring(id,0,charindex(''s'',id)) ItemID
		, ''Scene Conditions'' [Setting]
	FROM ItemSceneConditionTable

	UNION ALL

	SELECT DISTINCT substring(id,0,charindex(''s'',id)) ItemID 
		, ''Scene Timer'' [Setting]
	from SceneTable 
	where sct <> 0   

	UNION ALL

	select DISTINCT substring(id,0,charindex(''s'',id)) ItemID 
			, ''Shared Answer Sets'' [Setting] 
	from [ComponentTable]
	where shA IS NOT NULL
		and shA <> ''''

	UNION ALL
				
	select  DISTINCT substring(id,0,charindex(''s'',id)) ItemID
			, ''FollowOnMarking'' [Setting] 
	from [ItemGapfillTable]
	where [for] <> ''''	

	UNION ALL

	SELECT DISTINCT substring(id,0,charindex(''s'',id)) ItemID
		, ''RightToLeft'' [Setting]
	FROM (
		select id from [ItemVideoTable] where rtl = 1 union
		select id from [ItemTextSelectorTable] where rtl = 1 union
		select id from [ItemTextBoxTable] where rtl = 1 union
		select id from [ItemReorderingTable] where rtl = 1 union
		select id from [ItemPicklistTable] where rtl = 1 union
		select id from [ItemMultipleChoiceTable] where rtl = 1 union
		select id from [ItemImageMapTable] where rtl = 1 union
		select id from [ItemHotSpotTable] where rtl = 1 union
		select id from [ItemGraphicTable] where rtl = 1 union
		select id from [ItemGapfillTable] where rtl = 1 union
		select id from [ItemDragAndDropTable] where rtl = 1 union
		select id from [ItemDocumentTable] where rtl = 1 union
		select id from [ItemAudioRecorderTable] where rtl = 1 
	) RightToLeft
) [Outer]	
'

DECLARE @DYNAMIC NVARCHAR(MAX) = ''

select @DYNAMIC +=CHAR(13) +
'use ' +quotename(name)+ ';
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
BEGIN
	if OBJECT_ID(''tempdb..[#'+name+']'') is not null drop table [#'+name+'];

	CREATE TABLE [#'+name+'] (
		ItemID NVARCHAR(20)
	);

	INSERT [#'+name+']
	select 
		cast(Projectlisttable.id as varchar(5))+ ''P'' + project.page.value(''@ID'',''nvarchar(12)'') as PageID
	from '+quotename(name)+'..Projectlisttable
	cross apply ProjectStructureXml.nodes(''Pro//Pag'') project(page)
	INNER JOIN '+quotename(name)+'..PageTable 
	on PageTable.ParentID = Projectlisttable.id AND PageTable.ID = cast(Projectlisttable.ID as nvarchar(10)) + ''P'' + cast(project.page.value(''@ID'',''nvarchar(12)'') as nvarchar(10))

	where project.page.value(''@sta'',''int'') Between PublishSettingsXML.value(''(/PublishConfig/StatusLevelFrom)[1]'',''int'') and PublishSettingsXML.value(''(/PublishConfig/StatusLevelTo)[1]'',''int'')

	EXCEPT

	select 
		cast(Projectlisttable.id as varchar(5))+ ''P'' + project.page.value(''@ID'',''nvarchar(12)'') as PageID
	from '+quotename(name)+'..Projectlisttable
	cross apply ProjectStructureXml.nodes(''Pro/Rec//Pag'') project(page)
	INNER JOIN '+quotename(name)+'..PageTable 
	on PageTable.ParentID = Projectlisttable.id AND PageTable.ID = cast(Projectlisttable.ID as nvarchar(10)) + ''P'' + cast(project.page.value(''@ID'',''nvarchar(12)'') as nvarchar(10))

	where project.page.value(''@sta'',''int'') Between PublishSettingsXML.value(''(/PublishConfig/StatusLevelFrom)[1]'',''int'') and PublishSettingsXML.value(''(/PublishConfig/StatusLevelTo)[1]'',''int'')

	DECLARE @SpecialItemCount int  = (

		SELECT distinct COUNT(D.ItemID)
		FROM ##SpecialItems D
		INNER JOIN [#'+name+'] E
		ON E.ItemID = D.ItemID
	)

	SELECT A.*
		, cast(cast((cast([SpecialItemCount] as float)/cast(TotalCount as float))*100 as decimal(8,2)) as varchar(10)) + '' %'' [% of Items]
	FROM (
	SELECT DISTINCT @@ServerName ServerName
		,db_name() DBName
		, PLT.ID
		, PLT.Name
		, [Outer].ItemID
		, [Outer].[Setting]
		, @SpecialItemCount SpecialItemCount
		, (SELECT COUNT(1) FROM [#'+name+']) TotalCount
	FROM (	
			SELECT *
			FROM ##SpecialItems
		) [Outer]
		INNER JOIN [#'+name+'] on [#'+name+'].ItemID  = [Outer].ItemID 
		INNER JOIN ProjectListTable plt on plt.id = SUBSTRING([Outer].ItemID, 0, CHARINDEX(''P'',[Outer].ItemID))
	) A
END
'
from sys.databases
where name like '%CPProjectAdmin%' 	or name like '%ContentProducer%' and name != 'SHA_QEYADAH_ContentProducer'

INSERT ##Global
exec(@DYNAMIC);

SELECT
	Servername 
    , DatabaseName 
	, ProjectID 
	, ProjectName
	, ItemID 
	, setting
	, SpecialItemCount
	, TotalCount 
	, [% of Items] 
FROM ##Global