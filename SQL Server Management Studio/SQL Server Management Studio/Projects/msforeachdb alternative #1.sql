IF OBJECT_ID('tempdb..#DATALENGTH') IS NOT NULL DROP TABLE #DATALENGTH;

CREATE TABLE #DATALENGTH (
	client nvarchar(1000)
	, [DataLength-Bytes] bigint
);


DECLARE foreachdb cursor for
select name
from sys.databases
where state_desc = 'ONLINE'
	and name like '%[_]ItemBank';

declare @tsql nvarchar(MAX)

OPEN foreachdb

FETCH NEXT FROM foreachdb INTO @tsql

WHILE @@FETCH_STATUS = 0

BEGIN

select @tsql = 
'use ' + quotename(@tsql)+ '

INSERT #DATALENGTH	
SELECT DB_NAME() [Client] 
		, MAX(DATALENGTH(ItemFile)) [DataLength-Bytes]
FROM [dbo].[ItemTable] (READUNCOMMITTED);

'
exec(@tsql)

FETCH NEXT FROM foreachdb INTO @tsql

END
CLOSE foreachdb
DEALLOCATE foreachdb

--04:18
select *
from #DATALENGTH


