sp_whoisactive

select 'kill ' + convert(varchar(5),spid)
	,db_name(dbid)
from sys.sysprocesses s
where dbid > 4
	--and  db_name(dbid) = 'SaxionDefect_ItemBank'
order by 2; 

SELECT 'ALTER DATABASE ' + QUOTENAME(Name) + ' SET ONLINE;  DROP DATABASE ' + QUOTENAME(Name) + ';'
from sys.databases
where database_id > 4
	and state_desc = 'Offline';

select 'ALTER LOGIN ' + QUOTENAME(NAME) + ' DISABLE;'
from sys.syslogins
order by Name
	
--ALTER DATABASE [PPD_AAT_ContentProducer] SET ONLINE;   DROP DATABASE [PPD_AAT_ContentProducer];
--ALTER DATABASE [PPD_AAT_ItemBank] SET ONLINE;   DROP DATABASE [PPD_AAT_ItemBank];
--ALTER DATABASE [PPD_AAT_SecureAssess] SET ONLINE;   DROP DATABASE [PPD_AAT_SecureAssess];
--ALTER DATABASE [PPD_AAT_SecureMarker] SET ONLINE;   DROP DATABASE [PPD_AAT_SecureMarker];
--ALTER DATABASE [PPD_AAT_SurpassDataWarehouse] SET ONLINE;   DROP DATABASE [PPD_AAT_SurpassDataWarehouse];
--ALTER LOGIN [PPD_AAT_ContentProducer_Login] DISABLE;
--ALTER LOGIN [PPD_AAT_ItemBank_Login] DISABLE;
--ALTER LOGIN [PPD_AAT_SecureAssess_Login] DISABLE;
--ALTER LOGIN [PPD_AAT_SecureMarker_Login] DISABLE;



--ALTER DATABASE [PPD_AQA_ContentProducer] SET ONLINE;   DROP DATABASE [PPD_AQA_ContentProducer];
--ALTER DATABASE [PPD_AQA_ItemBank] SET ONLINE;   DROP DATABASE [PPD_AQA_ItemBank];
--ALTER DATABASE [PPD_AQA_SecureAssess] SET ONLINE;   DROP DATABASE [PPD_AQA_SecureAssess];
--ALTER LOGIN [PPD_AQA_ContentProducer_Login] DISABLE;
--ALTER LOGIN [PPD_AQA_ItemBank_Login] DISABLE;
--ALTER LOGIN [PPD_AQA_SecureAssess_Login] DISABLE;


--ALTER DATABASE [PPD_NCFE_CPProjectAdmin] SET ONLINE;   DROP DATABASE [PPD_NCFE_CPProjectAdmin];
--ALTER DATABASE [PPD_NCFE_Itembank] SET ONLINE;   DROP DATABASE [PPD_NCFE_Itembank];
--ALTER DATABASE [PPD_NCFE_SecureAssess] SET ONLINE;   DROP DATABASE [PPD_NCFE_SecureAssess];
--ALTER LOGIN [PPD_NCFE_CPProjectAdmin_Login] DISABLE;
--ALTER LOGIN [PPD_NCFE_Itembank_Login] DISABLE;
--ALTER LOGIN [PPD_NCFE_SecureAssess_Login] DISABLE;


--ALTER DATABASE [PPD_OCR_ContentProducer] SET ONLINE;   DROP DATABASE [PPD_OCR_ContentProducer];
--ALTER DATABASE [PPD_OCR_ItemBank] SET ONLINE;   DROP DATABASE [PPD_OCR_ItemBank];
--ALTER DATABASE [PPD_OCR_SecureAssess] SET ONLINE;   DROP DATABASE [PPD_OCR_SecureAssess];
--ALTER DATABASE [PPD_OcrDataManagement] SET ONLINE;   DROP DATABASE [PPD_OcrDataManagement];
--ALTER LOGIN [PPD_OCR_ContentProducer_Login] DISABLE;
--ALTER LOGIN [PPD_OCR_ItemBank_Login] DISABLE;
--ALTER LOGIN [PPD_OCR_SecureAssess_Login] DISABLE;
--ALTER LOGIN [PPD_OcrDataManagement_Login] DISABLE;


--ALTER DATABASE [PRV_OCR_ASPState] SET ONLINE;   DROP DATABASE [PRV_OCR_ASPState];
--ALTER DATABASE [PRV_OCR_ContentProducer] SET ONLINE;   DROP DATABASE [PRV_OCR_ContentProducer];
--ALTER DATABASE [PRV_OCR_DataManagement] SET ONLINE;   DROP DATABASE [PRV_OCR_DataManagement];
--ALTER DATABASE [PRV_OCR_ItemBank] SET ONLINE;   DROP DATABASE [PRV_OCR_ItemBank];
--ALTER DATABASE [PRV_OCR_OpenAssess] SET ONLINE;   DROP DATABASE [PRV_OCR_OpenAssess];
--ALTER DATABASE [PRV_OCR_SecureAssess] SET ONLINE;   DROP DATABASE [PRV_OCR_SecureAssess];
--ALTER DATABASE [PRV_OCR_SurpassDataWarehouse] SET ONLINE;   DROP DATABASE [PRV_OCR_SurpassDataWarehouse];
--ALTER LOGIN [PRV_OCR_ASPState_Login] DISABLE;
--ALTER LOGIN [PRV_OCR_ContentProducer_Login] DISABLE;
--ALTER LOGIN [PRV_OCR_ItemBank_Login] DISABLE;
--ALTER LOGIN [PRV_OCR_OpenAssess_Login] DISABLE;
--ALTER LOGIN [PRV_OCR_SecureAssess_Login] DISABLE;


--ALTER DATABASE [PRV_SkillsFirst_ContentProducer] SET ONLINE;   DROP DATABASE [PRV_SkillsFirst_ContentProducer];
--ALTER DATABASE [PRV_SkillsFirst_ItemBank] SET ONLINE;   DROP DATABASE [PRV_SkillsFirst_ItemBank];
--ALTER DATABASE [PRV_SkillsFirst_SecureAssess] SET ONLINE;   DROP DATABASE [PRV_SkillsFirst_SecureAssess];
--ALTER DATABASE [PRV_SkillsFirst_SurpassDataWarehouse] SET ONLINE;   DROP DATABASE [PRV_SkillsFirst_SurpassDataWarehouse];
--ALTER LOGIN [PRV_SkillsFirst_ContentProducer_Login] DISABLE;
--ALTER LOGIN [PRV_SkillsFirst_ItemBank_Login] DISABLE;
--ALTER LOGIN [PRV_SkillsFirst_SecureAssess_Login] DISABLE;


--ALTER DATABASE [PRV_NCFE_CPProjectAdmin] SET ONLINE;   DROP DATABASE [PRV_NCFE_CPProjectAdmin];
--ALTER DATABASE [PRV_NCFE_Itembank] SET ONLINE;   DROP DATABASE [PRV_NCFE_Itembank];
--ALTER DATABASE [PRV_NCFE_SecureAssess] SET ONLINE;   DROP DATABASE [PRV_NCFE_SecureAssess];
--ALTER LOGIN [PRV_NCFE_CPProjectAdmin_Login] DISABLE;
--ALTER LOGIN [PRV_NCFE_Itembank_Login] DISABLE;
--ALTER LOGIN [PRV_NCFE_SecureAssess_Login] DISABLE;


--ALTER DATABASE [PRV_Standard_CPProjectAdmin] SET ONLINE;   DROP DATABASE [PRV_Standard_CPProjectAdmin];
--ALTER DATABASE [PRV_Standard_Itembank] SET ONLINE;   DROP DATABASE [PRV_Standard_Itembank];
--ALTER DATABASE [PRV_Standard_OpenAssessAdmin] SET ONLINE;   DROP DATABASE [PRV_Standard_OpenAssessAdmin];
--ALTER DATABASE [PRV_Standard_SecureAssess] SET ONLINE;   DROP DATABASE [PRV_Standard_SecureAssess];
--ALTER DATABASE [PRV_Standard_SecureMarker] SET ONLINE;   DROP DATABASE [PRV_Standard_SecureMarker];
--ALTER LOGIN [PRV_Standard_CPProjectAdmin_Login] DISABLE;
--ALTER LOGIN [PRV_Standard_Itembank_Login] DISABLE;
--ALTER LOGIN [PRV_Standard_OpenAssessAdmin_login] DISABLE;
--ALTER LOGIN [PRV_Standard_SecureAssess_Login] DISABLE;
--ALTER LOGIN [PRV_Standard_SecureMarker_Login] DISABLE;









--ALTER LOGIN [PRV_NCFE_CPProjectAdmin_Login] DISABLE;
--ALTER LOGIN [PRV_NCFE_Itembank_Login] DISABLE;
--ALTER LOGIN [PRV_NCFE_SecureAssess_Login] DISABLE;




