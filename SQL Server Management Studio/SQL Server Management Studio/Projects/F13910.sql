SELECT CA_ETL.CPID
	, CA_ETL.CPVersion
FROM (
SELECT 
	CAST(S.ProjectId AS nvarchar(20)) + 'P' + CAST(COALESCE(I.[ContentProducerItemId], I.VersionOriginalId, I.Id) AS nvarchar(20)) CPID
	,I.MajorVersion CPVersion
FROM [Surpass_ContentAuthor].[dbo].[SourceMaterials] SM
	INNER JOIN [Surpass_ContentAuthor].[dbo].[Items] I ON I.[Id] = SM.[ItemId]
	INNER JOIN [Surpass_ContentAuthor].[dbo].[Subjects] S ON S.[Id] = I.[SubjectId]
WHERE SM.[Type]=0
	AND I.[OriginalItemId] IS NULL
	AND I.[UpdateDate] >=  '2018-03-20 00:00:01'   --I�ve set this to just before the ETL started failing as a catch-all.  Configure as necessary
) CA_ETL
LEFT JOIN [Surpass_SurpassDataWarehouse].[dbo].[DimQuestions] DQ
on DQ.[CPID] = CA_ETL.[CPID]
	and DQ.[CPVersion] = CA_ETL.[CPVersion]
where DQ.[CPID] IS NULL;


--inserts to:  [dbo].[DimItemsSourceMaterialsCrossRef]

[dbo].[DimItemsSourceMaterialsCrossRef]

--I�ve set this to just before the ETL started failing as a catch-all.  Configure as necessary


UPDATE Items
set UpdateDate = GETDATE()
FROM Items I
INNER JOIN Subjects S
on S.Id = I.SubjectId
where CAST(S.ProjectId AS nvarchar(20)) + 'P' + CAST(COALESCE(I.[ContentProducerItemId], I.VersionOriginalId, I.Id) AS nvarchar(20)) in ('','','') --Pass item(s) into here

--select top 10 CAST(S.ProjectId AS nvarchar(20)) + 'P' + CAST(COALESCE(I.[ContentProducerItemId], I.VersionOriginalId, I.Id) AS nvarchar(20))
--FROM Items I
--INNER JOIN Subjects S
--on S.Id = I.SubjectId