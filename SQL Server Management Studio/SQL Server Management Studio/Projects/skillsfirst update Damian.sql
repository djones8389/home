update warehouse_examsessiontable
set resultData = '<exam passMark="60" passType="1" originalPassMark="60" originalPassType="1" totalMark="60" userMark="" userPercentage="" passValue="1" originalPassValue="1" totalTimeSpent="" closeBoundaryType="0" closeBoundaryValue="0" grade="" originalGrade="">
  <gradeBoundaryData>
    <gradeBoundaries passLevelType="1" adjustedGradeBoundaries="0">
      <grade modifier="lt" value="60" description="Fail" />
      <grade modifier="gt" value="60" description="Pass" />
    </gradeBoundaries>
  </gradeBoundaryData>
  <section id="1" passMark="0" passType="1" name="Section 1" totalMark="30" userMark="" userPercentage="" passValue="1" totalTimeSpent="" itemsToMark="0">
    <item id="951P5646" name="Copy of Scenario 1" totalMark="30" userMark="0" actualUserMark="" markingType="1" markerUserMark="" viewingTime="" userAttempted="1" version="21">
      <mark mark="0" learningOutcome="1" displayName="A-Write clearly and coherently, including an appropriate level of detail" maxMark="6" />
      <mark mark="0" learningOutcome="2" displayName="B-Present information in a logical sequence" maxMark="6" />
      <mark mark="0" learningOutcome="3" displayName="C-Use language, format and structure suitable for purpose and audience" maxMark="6" />
      <mark mark="0" learningOutcome="4" displayName="D-Use correct grammar including correct and consistent use of tense" maxMark="4" />
      <mark mark="0" learningOutcome="5" displayName="E-Ensure written work includes generally accurate spelling and that meaning is clear" maxMark="4" />
      <mark mark="0" learningOutcome="6" displayName="F-Ensure written work includes generally accurate punctuation and that meaning is clear" maxMark="4" />
    </item>
  </section>
  <section id="2" passMark="0" passType="1" name="Section 2" totalMark="30" userMark="" userPercentage="" passValue="1" totalTimeSpent="" itemsToMark="0">
    <item id="951P5649" name="Copy of Scenario 2" totalMark="30" userMark="0" actualUserMark="" markingType="1" markerUserMark="" viewingTime="" userAttempted="1" version="24">
      <mark mark="0" learningOutcome="1" displayName="A-Write clearly and coherently, including an appropriate level of detail" maxMark="6" />
      <mark mark="0" learningOutcome="2" displayName="B-Present information in a logical sequence" maxMark="6" />
      <mark mark="0" learningOutcome="3" displayName="C-Use language, format and structure suitable for purpose and audience" maxMark="6" />
      <mark mark="0" learningOutcome="4" displayName="D-Use correct grammar including correct and consistent use of tense" maxMark="4" />
      <mark mark="0" learningOutcome="5" displayName="E-Ensure written work includes generally accurate spelling and that meaning is clear" maxMark="4" />
      <mark mark="0" learningOutcome="6" displayName="F-Ensure written work includes generally accurate punctuation and that meaning is clear" maxMark="4" />
    </item>
  </section>
</exam>'
where keycode = 'C7QM64B4'

update WAREHOUSE_ExamSessionTable_Shreded
set resultData = '<exam passMark="60" passType="1" originalPassMark="60" originalPassType="1" totalMark="60" userMark="" userPercentage="" passValue="1" originalPassValue="1" totalTimeSpent="" closeBoundaryType="0" closeBoundaryValue="0" grade="" originalGrade="">
  <gradeBoundaryData>
    <gradeBoundaries passLevelType="1" adjustedGradeBoundaries="0">
      <grade modifier="lt" value="60" description="Fail" />
      <grade modifier="gt" value="60" description="Pass" />
    </gradeBoundaries>
  </gradeBoundaryData>
  <section id="1" passMark="0" passType="1" name="Section 1" totalMark="30" userMark="" userPercentage="" passValue="1" totalTimeSpent="" itemsToMark="0">
    <item id="951P5646" name="Copy of Scenario 1" totalMark="30" userMark="0" actualUserMark="" markingType="1" markerUserMark="" viewingTime="" userAttempted="1" version="21">
      <mark mark="0" learningOutcome="1" displayName="A-Write clearly and coherently, including an appropriate level of detail" maxMark="6" />
      <mark mark="0" learningOutcome="2" displayName="B-Present information in a logical sequence" maxMark="6" />
      <mark mark="0" learningOutcome="3" displayName="C-Use language, format and structure suitable for purpose and audience" maxMark="6" />
      <mark mark="0" learningOutcome="4" displayName="D-Use correct grammar including correct and consistent use of tense" maxMark="4" />
      <mark mark="0" learningOutcome="5" displayName="E-Ensure written work includes generally accurate spelling and that meaning is clear" maxMark="4" />
      <mark mark="0" learningOutcome="6" displayName="F-Ensure written work includes generally accurate punctuation and that meaning is clear" maxMark="4" />
    </item>
  </section>
  <section id="2" passMark="0" passType="1" name="Section 2" totalMark="30" userMark="" userPercentage="" passValue="1" totalTimeSpent="" itemsToMark="0">
    <item id="951P5649" name="Copy of Scenario 2" totalMark="30" userMark="0" actualUserMark="" markingType="1" markerUserMark="" viewingTime="" userAttempted="1" version="24">
      <mark mark="0" learningOutcome="1" displayName="A-Write clearly and coherently, including an appropriate level of detail" maxMark="6" />
      <mark mark="0" learningOutcome="2" displayName="B-Present information in a logical sequence" maxMark="6" />
      <mark mark="0" learningOutcome="3" displayName="C-Use language, format and structure suitable for purpose and audience" maxMark="6" />
      <mark mark="0" learningOutcome="4" displayName="D-Use correct grammar including correct and consistent use of tense" maxMark="4" />
      <mark mark="0" learningOutcome="5" displayName="E-Ensure written work includes generally accurate spelling and that meaning is clear" maxMark="4" />
      <mark mark="0" learningOutcome="6" displayName="F-Ensure written work includes generally accurate punctuation and that meaning is clear" maxMark="4" />
    </item>
  </section>
</exam>'
where keycode = 'C7QM64B4'