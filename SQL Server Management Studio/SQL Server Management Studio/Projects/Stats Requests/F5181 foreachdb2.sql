IF OBJECT_ID('tempdb..#ResultsHolder') IS NOT NULL DROP TABLE #ResultsHolder;

CREATE TABLE #ResultsHolder (

	[ClientName] nvarchar(200)
	, [Month] tinyint
	, [Online Exams] int
	, [Offline Exams] int
)

INSERT #ResultsHolder
exec sp_MSforeachdb '

USE [?];

SET QUOTED_IDENTIFIER ON;

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

if (''?'' like ''%SecureAssess%'')

SELECT DB_NAME() ClientName 
	 , MONTH(Exams.warehouseTime) [Month]
	 , count([Online].ID) [Online Exams]
	 , count([Offline].ID) [Offline Exams]

FROM (

select ID
	, warehouseTime
	from WAREHOUSE_ExamSessionTable 
	where examstatechangeauditxml.exist(''exam/stateChange/newStateID[text() = (6)]'') = 1	
		AND examstatechangeauditxml.exist(''exam/stateChange/newStateID[text() = (9)]'') = 1
) Exams
LEFT JOIN (
	select 
		 ID
		 , warehouseTime
	from WAREHOUSE_ExamSessionTable 
	where examstatechangeauditxml.exist(''exam/stateChange/newStateID[text() = (8)]'') = 0
)  [Online]
	
On Exams.ID = [Online].ID

LEFT JOIN (
	select 
		 ID
		 , warehouseTime
from WAREHOUSE_ExamSessionTable 
where examstatechangeauditxml.exist(''exam/stateChange/newStateID[text() = (8)]'') = 1
)  [Offline]

On Exams.ID = [Offline].ID

GROUP BY MONTH(Exams.warehouseTime)
ORDER BY MONTH(Exams.warehouseTime)

'

SELECT SUBSTRING(CLientName, 0, CHARINDEX(''_'',CLientName))
	 , [Month]
	 ,  [Online Exams]
	 , ISNULL([Offline Exams],0) 
FROM #ResultsHolder NOLOCK
order by 1;