USE [PRV_AAT_ItemBank]

--ALTER VIEW [dbo].[DJ_Testing]
--AS
SELECT 
	ASLT.Name
	, gb.GradeBoundary [Grade Boundaries]
	, ISNULL(AssessmentRules.value('data(PaperRules/Section/@TotalQuestions)[1]','tinyint'),'0') [Total Questions]
	,IsPrintable
	,AllowCloseWithoutSubmit
	,ISNULL(AT.UseAsTemplate,'0') [Use As Template]
	,PreCaching [AutoSync]
	,CASE ActionScriptMode
		WHEN '0'
			THEN 'Mixed'
		WHEN '1'
			THEN 'AS2'
		WHEN '2'
			THEN 'AS3'
		ELSE 'N/A'
		END AS 'ActionScriptMode'
	,CASE DurationMode
		WHEN '0'
			THEN 'Untimed'
		WHEN '1'
			THEN 'Timed'
		WHEN '2'
			THEN 'Timed Sections'
		ELSE 'N/A'
		END AS 'Timing'
	--, at.GradeBoundaries.value('data(GradeBoundaries/grade[1]/@description)[1]','nvarchar(10)') GB1
	,AssessmentDuration  [Duration]
	,CASE convert(nvarchar(10),ISNULL([CandidateDetailsTime],'0')) when convert(nvarchar(10),'0') then convert(nvarchar(10),'N/A') else convert(nvarchar(10),[CandidateDetailsTime]) end as  [Candidate Details Screen Duration]
	,CASE convert(nvarchar(10),ISNULL([NdaTime],'0')) when convert(nvarchar(10),'0') then convert(nvarchar(10),'N/A') else convert(nvarchar(10),[NdaTime]) END AS 'NDA Screen Duration'''
	,CASE convert(nvarchar(10),ISNULL((AssessmentDuration + [CandidateDetailsTime] + [NdaTime]),'0')) when convert(nvarchar(10),'0') then convert(nvarchar(10),'N/A') else convert(nvarchar(10),(AssessmentDuration + [CandidateDetailsTime] + [NdaTime])) END AS [NDA Screen Duration]
	,SRBonus [Reasonable Adjustments (mins):   standard]
	,SRBonusMaximum  [Reasonable Adjustments (mins):   maximum]	
	,TestFeedBackType.value('data(/TestFeedbackType/PassFail)[1]', 'bit') [Pass/Fail]
	,TestFeedBackType.value('data(/TestFeedbackType/PercentageMark)[1]', 'bit') [Percentage Mark]
	,isnull(TestFeedBackType.value('data(/TestFeedbackType/AllowFeedbackDuringExam)[1]', 'bit'),'0') [Show FeedBack Button During Assessment]
	,TestFeedBackType.value('data(/TestFeedbackType/AllowSummaryFeedback)[1]', 'bit') [Summary Feedback]
	,CASE 
		WHEN TestFeedBackType.value('data(/TestFeedbackType/SummaryFeedbackType)[1]', 'bit') = '0'
			THEN 'No Review'
		ELSE 'Review Answers'
		END AS 'Summary FeedBack Type'
	,CASE 
		WHEN TestFeedBackType.value('data(/TestFeedbackType/AllowSummaryFeedback)[1]', 'nvarchar(10)') = '0'
			THEN 'Disabled'
		ELSE ISNULL(TestFeedBackType.value('data(/TestFeedbackType/PrintableSummary)[1]', 'nvarchar(10)'), '0')
		END AS [Printable Summary]
	,CASE 
		WHEN TestFeedBackType.value('data(/TestFeedbackType/AllowSummaryFeedback)[1]', 'nvarchar(10)') = '0'
			THEN 'Disabled'
		ELSE ISNULL(TestFeedBackType.value('data(/TestFeedbackType/CandidateDetails)[1]', 'nvarchar(10)'), '0')
		END AS [Show Candidate Details]
	,CASE 
		WHEN TestFeedBackType.value('data(/TestFeedbackType/AllowSummaryFeedback)[1]', 'nvarchar(10)') = '0'
			THEN 'Disabled'
		ELSE ISNULL(TestFeedBackType.value('data(/TestFeedbackType/FeedbackByReference)[1]', 'nvarchar(10)'), '0')
		END AS [Display Feedback by Reference]
	,CASE 
		WHEN TestFeedBackType.value('data(/TestFeedbackType/AllowSummaryFeedback)[1]', 'nvarchar(10)') = '0'
			THEN 'Disabled'
		ELSE ISNULL(TestFeedBackType.value('data(/TestFeedbackType/AllowFeedbackDuringSummary)[1]', 'nvarchar(10)'), '0')
		END AS [Show View Feedback Button]	
	,CASE AutomaticVerification
		WHEN '1'
			THEN '0'
		WHEN '0'
			THEN '1'
		END AS [Requires Moderation To Release Results] --0 is ticked, 1 is unticked	BUG
	,AutoProgressItems [Auto Progress Items status when used in live Assessment]
	,isnull(UseSecureMarker,'0') [Requires SecureMarker]
	,CertifiedAccessible [Certified Accessible]
	,isnull(AGT.UseAsTemplate,'0') [Allow use as Template]
	,ExamType
	,CASE 
		WHEN ExamType = '0'
			THEN 'Computed Based Test'
		ELSE 'NotCBT'
		END AS [Exam Type]
	,CanExtendTime [Allow Time Extension While In Progress]
	,attemptAutoSubmit [Attempt Auto Submit]
	,ISNULL([AwaitingUploadPeriod], 0) AS 'Results Upload Grace Period'
	,CASE AGT.ExamDeliverySystemOption
		WHEN '0'
			THEN 'SecureAssess'
		WHEN '1'
			THEN 'OpenAssess'
		WHEN '2'
			THEN 'Both'
		END AS 'Delivery Options'' '
	,RequiresSecureClient [Requires SecureClient]
	,CASE convert(NVARCHAR(20), RequiresSecureClient)
		WHEN '0'
			THEN 'Option Disabled'
		ELSE convert(NVARCHAR(10), SecureClientOperationMode)
		END AS [SecureClientOperationMode]
	,agt.RequiresInvigilation [Requires Invigilation]
	,ISNULL([CertifiedForTabletDelivery], '0') [Certified For Tablet Delivery]
	,CASE WHEN [NoOfResitsAllowed] IS NULL THEN 0 ELSE 1 END AS  [Restrict Number of Resits]
	,ISNULL(NoOfResitsAllowed, '0') [Number of Resits]
	,agt.RequiresVersionAvailability [Use Assessment Version availability dates]
	,case ExamCostCode when '-1' then '1' when '0' then '1' when ISNULL(ExamCostCode,'1') then '1' else ExamCostCode END AS [Cost Code]
	,CASE ScoreBoundaries.value('data(ScoreBoundaries[showScoreBoundaryColumn])[1]', 'bit')
		WHEN '0'
			THEN 'Show ''Percentage '' Column'
		ELSE 'Show ''Result'' Column'
		END AS [Candidate Score Boundaries]
	,[RandomiseExams] [Randomise Assessment Versions]
	,[AllowVersionRecycling] [Allow Assessment Version Recycling]
	,CASE [ExamDeliveryOption]
		WHEN '0'
			THEN 'Deliver same assessment to all candidates'
		WHEN '1'
			THEN 'Deliver different assessments to all candidates'
		WHEN '2'
			THEN 'Either'
		END AS [Delivery Options]
	,CASE convert(NVARCHAR(10), ExamDeliverySystemOption)
		WHEN '0'
			THEN 'Online'
		WHEN '1'
			THEN 'Offline'
		ELSE 'N/A'
		END AS [Assessment Distribution]
	, at.ExternalReference
	,ISNULL([ShowCandidateDetailsPage], 1) [Requires User Details Confirmation]
	,ISNULL(CandidateDetailsTime, 0) 'Set Duration (min): '''
	,[RequiresConfirmationCheck] [Requires NDA confirmation]
	,ISNULL(NdaTime, 0) [Set Duration (min):]
	,ISNULL([ConfirmationText], '') AS ConfirmationText
	,[MarkingProgressVisible] [Show Marking Progress Bar]
	,CASE [MarkingProgressMode]
		WHEN '0'
			THEN 'Item Based'
		WHEN '1'
			THEN 'Marks based'
		END AS [Mode:]
	,CASE ExamDeliverySystemStyleType
		WHEN 'delivery'
			THEN 'Left Hand Navigation'
		WHEN 'delivery_surpass'
			THEN 'Surpass Standard Left Hand Navigation'
		END AS [Assessment Style]
	,[DefaultNavigationLanguage] [Default Navigation Language]
	,DefaultNavigationLanguageAllowOverride [Allow Language Override]
	,ISNULL(ShowPageRequiresScrollingAlert, '0') [Show 'Page Requires Scrolling' Alert]
	,[EasyFacilityValue]
	,[MaxEasyFacilityValue]
	,[MinHardFacilityValue]
	,[HardFacilityValue]
	,[SuppressResultsScreenMark] AS 'Suppress mark from Results screen'
	,[SuppressResultsScreenPercent] AS 'Suppress percent from Results screen'
	,[SuppressResultsScreenResult] AS 'Suppress results from Results screen'
	,[DisableReportingCandidateReport] AS 'Disable Candidate Report'
	,[DisableReportingSummaryReport] AS 'Disable Summary Report'
	,[DisableReportingCandidateBreakdownReport] AS 'Disable Candidate Breakdown Report'
	,[DisableReportingExamBreakdownReport] AS 'Disable Exam Breakdown Report'
	,[DisableReportingResultSlip] AS 'Disable ResultSlip'	
	,[MinResitTimeInDays] [Minimum Resit Time]
	,[GenerateExamStatistics] [Generate Assessment Statistics]
	,AllowPackagingDelivery [Allow Packaging of Candidate Responses]
	,AutoViewExam [Automatically Show To Centre]
	,automaticCreatePin [Auto Create PIN]
	,[StrictControlOnDDA] [Enable strict control for Reasonable Adjustments]
	,MarkingAutoVoidPeriod [Marking Auto Void Period]
	,[IsUserAssociationsEnabled]
	,[CanMarkerBeAssigned]
	,[IsMarkerAssignmentRequired]
	,[CanModeratorBeAssigned]
	,[IsModeratorAssignmentRequired]
	,[strictControlsRequired]	
FROM AssessmentTable AT
INNER JOIN AssessmentGroupTable AGT ON AGT.id = AT.AssessmentGroupID
INNER JOIN AssessmentStatusLookupTable ASLT on ASLT.id = AT.AssessmentStatus
INNER JOIN
		(
select ID,
		(
		select 
			a.b.value('@description','nvarchar(20)') + ' '
			+ a.b.value('@modifier','char(2)')  + ' '
			+ a.b.value('@value','nvarchar(20)') + ' '					
		from AssessmentTable L
		CROSS APPLY GradeBoundaries.nodes('GradeBoundaries/grade') a(b)
		where l.id = m.ID
			FOR XML PATH(''), type
				) as GradeBoundary
From AssessmentTable M
group by ID

	) GB
on GB.ID = AT.ID
WHERE ExternalReference NOT LIKE '%BTL%'
	AND AT.AssessmentName  NOT LIKE '%BTL%'
	--= 'AAT Conference 2010 Exemplar'
	--OR ExternalReference = 'CAPQ4/PRACTICE'
GO


--select * from [DJ_Testing]


