if OBJECT_ID('tempdb..##DiskSpace') IS NOT NULL DROP TABLE ##DiskSpace;

CREATE TABLE ##DiskSpace (
       Drive CHAR(1) 
       ,[Space-KB] INT
       );
       
INSERT ##DiskSpace
EXEC master..xp_fixeddrives;

DECLARE @AvailableSpaceOnDisk int;

SELECT @AvailableSpaceOnDisk = [Space-KB] 
FROM ##DiskSpace
WHERE Drive in (
	select substring(physical_name, 0, charindex(':', physical_name))
	from sys.database_files
	where type_desc = 'ROWS'
);



--SELECT (@AvailableSpaceOnDisk * 1024) [KB Avail]
--	, ((@AvailableSpaceOnDisk - 5120)*1024) [KB with 5gb buffer]

DECLARE @SQLBuilder nvarchar(MAX)= ''

SELECT @SQLBuilder += CHAR(13) + RebuildStatement
FROM (

SELECT 		
	 CASE WHEN cast(LTRIM (STR ((CASE WHEN usedpages > pages THEN (usedpages - pages) ELSE 0 END) * 8, 15, 0)) as int)	
		< ((@AvailableSpaceOnDisk - 5120)*1024) /*5 GB buffer*/
			THEN 'ALTER INDEX ' + QUOTENAME ([IndexName]) + ' ON ' + QUOTENAME ([SchemaName]) + '.' + QUOTENAME ([TableName]) + ' REBUILD; '  
			ELSE ''
			 end as 'RebuildStatement'
FROM (

	SELECT distinct
		OBJECT_NAME(s.object_id) [TableName]
		, SCHEMA_NAME(t.schema_id) [SchemaName]
		, i.name [IndexName]
		, (used_page_count) [usedpages]
		,  (
			CASE
				WHEN (s.index_id < 2) THEN (in_row_data_page_count + lob_used_page_count + row_overflow_used_page_count)
				ELSE lob_used_page_count + row_overflow_used_page_count
			END
		) [pages]	
	FROM sys.dm_db_partition_stats AS s
		
		INNER JOIN sys.indexes AS i
			ON s.[object_id] = i.[object_id]
				AND s.[index_id] = i.[index_id]
		
		INNER JOIN sys.tables t
		on t.object_id = s.object_id

	    INNER JOIN sys.dm_db_index_physical_stats (DB_ID(), NULL, NULL, NULL, NULL) as b
		on b.object_id = s.object_id
			and b.index_id = i.index_id

		where i.index_id > 0
			and t.type = 'u'
			AND B.avg_fragmentation_in_percent > 30
) A

)F
	PRINT(@SQLBuilder)
	--where TableName = 'WAREHOUSE_ExamSessionItemResponseTable'








--Declare @Rebuild NVARCHAR(MAX) = '';


--       SELECT @Rebuild +=CHAR(13) + 
       
--       'ALTER INDEX ' + C.NAME + ' ON ' + 'dbo' + '.' + B.NAME + ' REBUILD;'
--              FROM sys.dm_db_index_physical_stats(DB_ID(), NULL, NULL, NULL, NULL) A
--              INNER JOIN sys.objects B ON A.object_id = B.object_id
--              INNER JOIN sys.indexes C ON B.object_id = C.object_id
--                     AND A.index_id = C.index_id
--              INNER JOIN sys.partitions D ON B.object_id = D.object_id
--                     AND A.index_id = D.index_id
--              INNER JOIN sys.dm_db_partition_stats E ON E.object_id = b.object_id
--                     AND e.index_id = c.index_id
--              WHERE C.index_id > 0
--                     AND A.avg_fragmentation_in_percent > 30

--       PRINT(@Rebuild);



--select 
--		'exec sp_spaceused @objname = '''+s.name+'.'+t.name + ''' '
--	from sys.tables T
--	inner join sys.schemas S
--	on s.schema_id = t.schema_id
--	where type = 'u'
--		and t.name not in ('_MigrationDatabaseMetrics','_MigrationScripts','_MigrationTableMetrics' )
--	order by t.name;

--exec sp_spaceused @objname = 'dbo.WAREHOUSE_ScheduledExamsTable' 
--exec sp_spaceused @objname = 'dbo.WAREHOUSE_UserTable' 
--exec sp_spaceused @objname = 'dbo.WelcomeMessageTable' 





--SELECT 'ALTER INDEX ' + C.NAME + ' ON ' + 'dbo' + '.' + B.NAME + ' REBUILD;'		
--	  , a.*
--	  , b.name
--              FROM sys.dm_db_index_physical_stats(DB_ID(), NULL, NULL, NULL, NULL) A
--              INNER JOIN sys.objects B ON A.object_id = B.object_id
--              INNER JOIN sys.indexes C ON B.object_id = C.object_id
--                     AND A.index_id = C.index_id
--              INNER JOIN sys.partitions D ON B.object_id = D.object_id
--                     AND A.index_id = D.index_id
--              INNER JOIN sys.dm_db_partition_stats E ON E.object_id = b.object_id
--                     AND e.index_id = c.index_id
--              WHERE C.index_id > 0
--					and B.NAME = 'WAREHOUSE_ScheduledExamsTable'
              


--SELECT i.[name] AS IndexName
--	,(s.[used_page_count]) * 8 AS IndexSizeKB
--	, o.name
--FROM sys.dm_db_partition_stats AS s
--INNER JOIN sys.indexes AS i ON s.[object_id] = i.[object_id]
--	AND s.[index_id] = i.[index_id]
--INNER JOIN sys.objects O
--on O.object_id = i.object_id
--where o.type = 'u'
--	and i.name is not null
--	and o.NAME = 'WAREHOUSE_ScheduledExamsTable'



--exec sp_spaceused @objname = 'dbo.WAREHOUSE_ScheduledExamsTable' 

















