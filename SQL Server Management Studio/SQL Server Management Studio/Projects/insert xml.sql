DECLARE @Node1 xml = '<test>'
DECLARE @markerComment xml = '<entry>
		<comment/>
		<userId>96999</userId>
		<userForename>David</userForename>
		<userSurname>Green</userSurname>
		<assignedMark>6</assignedMark>
		<annotations>
			<comments/>
		</annotations>
		<version>0</version>
		<dateCreated>05/08/2016</dateCreated>
	</entry>'

declare @table table (

	myXML xml
)
INSERT @table
select @markerComment 

UPDATE @table
SET myXML.modify('insert sql:variable("@Node1") as first into ((entry)[1])')

