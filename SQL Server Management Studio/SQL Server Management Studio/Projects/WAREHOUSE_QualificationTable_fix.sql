use master

DECLARE @dynamic nvarchar(MAX)='';

select @dynamic +=CHAR(13)+ '
use '+QUOTENAME([NAME])+'

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

if not exists (select 1 from sys.tables where name= ''WAREHOUSE_QualificationTable_DE28081'')

SELECT *
INTO [WAREHOUSE_QualificationTable_DE28081]
FROM [dbo].[WAREHOUSE_QualificationTable] WITH (NOLOCK)
where QualificationLevel = 1


IF EXISTS (select 1 from sys.tables where name= ''WAREHOUSE_QualificationTable_DE28081'')

UPDATE [WAREHOUSE_QualificationTable]
SET QualificationLevel = QualificationID
where QualificationLevel = 1


'
from sys.databases
where state_desc = 'ONLINE'
	and name like '%[_]SecureAssess';

exec(@dynamic);



