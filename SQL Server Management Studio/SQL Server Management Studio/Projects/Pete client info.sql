select COUNT([Display Resolution])
, [Display Resolution]
from (
SELECT 
	E.clientInformation.value('(/clientInformation/systemConfiguration[1]/environment/video/resolution)[1]', 'nvarchar(max)') AS [Display Resolution]
FROM dbo.WAREHOUSE_ExamSessionTable E WITH (NOLOCK)
where clientInformation is not null
	and warehouseTime > '01 Jan 2016'
) A
group by [Display Resolution]
ORDER BY 1