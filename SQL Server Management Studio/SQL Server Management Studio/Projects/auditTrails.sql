SELECT
       AT.Id ExternalId
       ,S.ProjectId ProjectKey
       ,ISNULL(I.[ContentProducerItemId],I.Id) ItemKey
       ,ISNULL(AT.Version,0) CPVersion
       ,AT.UserId UserKey
       ,ServerDateTime ActionDate
       ,AT.[Status] NewStatus
       ,CommentText Comment
       ,1 NotHandled
       ,EventType
FROM Items I
    INNER JOIN AuditTrails AT
    ON I.Id = AT.ItemId
    INNER JOIN Subjects S
    ON S.Id = I.SubjectId

WHERE   I.CreateDate <= CONVERT(datetime, '', 121)
       AND I.OriginalItemId IS NULL
       AND AT.ServerDateTime <= CONVERT(datetime, '', 121)
       AND AT.ServerDateTime >= CONVERT(datetime, '', 121)

UNION

---- Audit history for duplicated items

SELECT 

       AT.Id ExternalId
       ,S.ProjectId ProjectKey
       ,ISNULL(I.[ContentProducerItemId],I.Id) ItemKey
       ,ISNULL(AT.Version,0) CPVersion
       ,AT.UserId UserKey
       ,ServerDateTime ActionDate
       ,AT.[Status] NewStatus
       ,CommentText Comment
       ,1 NotHandled
       ,EventType
FROM Items I
    INNER JOIN AuditTrails AT
    ON I.Id = AT.ItemId
    INNER JOIN Subjects S
    ON S.Id = I.SubjectId

WHERE   I.CreateDate <= CONVERT(datetime, '', 121)
       AND I.CreateDate >= CONVERT(datetime, '', 121)
       AND I.OriginalItemId IS NULL
       AND AT.ServerDateTime <= CONVERT(datetime, '', 121) 

