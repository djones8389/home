SET NOCOUNT ON;
BEGIN TRY

if exists (
select 1
from sys.objects o
inner join sys.tables t
on t.object_id = o.object_id
inner join sys.columns c 
on c.object_id = t.object_id
where o.name = 'WAREHOUSE_ExamSessionDocumentTable'
	and c.name = 'ID'
		and is_identity = 1
)

	INSERT INTO [dbo].[WAREHOUSE_ExamSessionDocumentTable] ([warehouseExamSessionID], [itemId], [documentName], [Document], [uploadDate])
	SELECT	
		@warehouseExamSessionID
		,@itemId
		,@fileName
		,@document
		,GETDATE()
		
else
	
	INSERT INTO [dbo].[WAREHOUSE_ExamSessionDocumentTable] ([ID], [warehouseExamSessionID], [itemId], [documentName], [Document], [uploadDate])
	SELECT	
		CASE
			WHEN MIN(ID) <= 0
			THEN MIN(ID) - 1
			ELSE -1
		END
		,@warehouseExamSessionID
		,@itemId
		,@fileName
		,@document
		,GETDATE()
	FROM [dbo].[WAREHOUSE_ExamSessionDocumentTable]

--Return success errorCode string
DECLARE @errorReturnString 	nvarchar(max)
SET @errorReturnString = '<result errorCode="0"><return>true</return></result>'
SELECT @errorReturnString

END TRY
BEGIN CATCH
	SELECT '1' as '@errorCode', --Attribute to our root (btl standard 'result') node
	(
		SELECT ERROR_MESSAGE() AS "*"
		FOR XML PATH('') ,TYPE
	)
	FOR XML PATH('return'), ROOT('result')--Our outermost/root node
END CATCH




