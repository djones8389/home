ALTER DATABASE [Prometric_SecureAssess] SET SINGLE_USER WITH ROLLBACK IMMEDIATE
USE [Prometric_SecureAssess]
BACKUP DATABASE [Prometric_SecureAssess] TO  DISK = N'\\524778-sql\DATASHARE\Surpass\DeploymentSettings\SecureAssess\DatabaseBackups\Prometric\Prometric_SecureAssess_2017-06-05_143114.bak' WITH NOFORMAT, NOINIT, NOSKIP, REWIND, NOUNLOAD,  STATS = 10
USE [Prometric_SecureAssess]
ALTER DATABASE [Prometric_SecureAssess] SET MULTI_USER
RESTORE FILELISTONLY FROM  DISK = N'\\524778-sql\DATASHARE\Surpass\DeploymentSettings\SecureAssess\DatabaseBackups\Prometric\Prometric_SecureAssess_2017-06-05_143114.bak' WITH  NOUNLOAD
ALTER DATABASE [Prometric_SecureAssess] SET SINGLE_USER WITH ROLLBACK IMMEDIATE
ALTER DATABASE [Prometric_SecureAssess] MODIFY NAME = [Prometric_SecureAssess_deleted_2017-06-05_143119]
ALTER DATABASE [Prometric_SecureAssess_deleted_2017-06-05_143119] SET MULTI_USER
ALTER DATABASE [Prometric_SecureAssess_deleted_2017-06-05_143119] SET  OFFLINE
RESTORE DATABASE [Prometric_SecureAssess_2017-06-05_143119] FROM  DISK = N'\\524778-sql\DATASHARE\Surpass\DeploymentSettings\SecureAssess\DatabaseBackups\Prometric\Prometric_SecureAssess_2017-06-05_143114.bak' WITH  MOVE N'Data' TO N'S:\DATA\Prometric_SecureAssess_2017-06-05_143119.mdf',  MOVE N'Log' TO N'L:\LOGS\Prometric_SecureAssess_2017-06-05_143119_log.ldf',  NOUNLOAD,  STATS = 10
ALTER DATABASE [Prometric_SecureAssess_2017-06-05_143119] SET SINGLE_USER WITH ROLLBACK IMMEDIATE
ALTER DATABASE [Prometric_SecureAssess_2017-06-05_143119] MODIFY NAME = [Prometric_SecureAssess]
ALTER DATABASE [Prometric_SecureAssess] SET MULTI_USER
USE [Prometric_SecureAssess]
USE [Prometric_SecureAssess_2017-06-05_143119]
USE [Prometric_SecureAssess_deleted_2017-06-05_143119]
USE [Prometric_SecureAssess]