IF OBJECT_ID (N'dbo.xmlParser', N'FN') IS NOT NULL  
    DROP FUNCTION dbo.xmlParser;  
GO  

CREATE FUNCTION [xmlParser](@ID INT)
returns XML 
AS
BEGIN
	
DECLARE @XML XML
SELECT @XML = structureXML
FROM [dbo].[xmlTable]
where ID = @ID


DECLARE @AsciiCheck TABLE (
       
       [Character] char(1)
       , [ASCII Val] tinyint
)


declare @mystring nvarchar(MAX) = CAST(@XML as nvarchar(MAX))
declare @min int = 1
declare @max int = (select LEN(@mystring)+1)

while (@min < @max)

BEGIN

       INSERT @AsciiCheck
       select substring(@mystring, @min,1), ASCII((SELECT substring(@mystring, @min,1)))

       select @min = @min + 1

END


DELETE @AsciiCheck
where [ASCII Val] < 32;


Declare @newString NVARCHAR(max) =  (

select CAST 
       (
              (
                     select CHAR([ASCII Val])
                     from @AsciiCheck
                     for xml path(''), type
              )  as nvarchar(MAX)
       )
)
DECLARE @NEWXML XML = (select cast(replace(replace(@newString, '&lt;','<'), '&gt;','>') as xml))

RETURN (@NEWXML)
END