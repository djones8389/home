SET NOCOUNT ON;

IF OBJECT_ID('tempdb..#mytest') IS NOT NULL DROP TABLE #mytest;

SELECT A.path
	, case when (CurrentFile - [max-files]) > 0 then (CurrentFile - [max-files])+1 else '' end as 'MinFile'
	, a.CurrentFile
into #mytest
FROM (
	select a.path	
		,  case 
			ISNUMERIC(reverse(substring(reverse(path), 5,charindex('_',reverse(path))-5))) 
				when 1 then reverse(substring(reverse(path), 5,charindex('_',reverse(path))-5)) 
				else '' 
				end as 'CurrentFile'
		, [max-files]
	from (

		select path
			, [max-files]
		FROM [master].[dbo].[sys.traces]
		where id > 1 and path LIKE '%/_%' ESCAPE '/'

	) a
) a


DECLARE @StringBuilder table (

	Path nvarchar(200)
)

INSERT @StringBuilder
select path
from #mytest;

DELETE
from #mytest
where MinFile = 0
	and ISNUMERIC(currentFile) = 0;


INSERT @StringBuilder
select REPLACE(dj, CurrentFile,MinFile)
FROM (

SELECT 
	case when path like '%_1.trc%' then SUBSTRING(path, 0, CHARINDEX('_1.trc',path)) + '.trc'
		else path
		end as 'dj'
		, MinFile
		, CurrentFile
FROM #mytest

) A

--UPDATE @StringBuilder
--SET Path = REPLACE(PATH, 'O:\','C:\New folder\')

DECLARE @Dynamic nvarchar(MAX)='';

select @Dynamic += CHAR(13) + 'select ObjectName, Duration
from ::fn_trace_gettable('''+Path+''',default)'
from @StringBuilder


PRINT(@Dynamic);


--select *
--from ::fn_trace_gettable('',default)