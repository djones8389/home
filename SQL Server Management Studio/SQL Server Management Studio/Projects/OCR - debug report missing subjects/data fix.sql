--BACKUP DATABASE [Ocr_SurpassDataWarehouse] TO DISK = N'T:\Backup\Ocr_SurpassDataWarehouse.2017.10.26.bak' 
--	WITH NAME = N'Ocr_SurpassDataWarehouse- Database Backup', COPY_ONLY, NOFORMAT, NOINIT, SKIP, COMPRESSION, NOREWIND, NOUNLOAD, STATS = 10;
	
SELECT CPID
	, Deleted
FROM OCR_SurpassDataWarehouse..DimQuestions
WHERE CPID NOT IN (

	SELECT distinct SDW.CPID
	FROM OCR_SurpassDataWarehouse..DimQuestions SDW
	LEFT JOIN (
		SELECT distinct CPID
		FROM OCR_SurpassDataWarehouse..DJ_CPIDs
	) CP
	on SDW.CPID = CP.CPID
	where CP.CPID IS NULL
) 
and Deleted = 1


--SELECT CPID, Deleted
--INTO OCR_SurpassDataWarehouse..[DJ_Items_26102017]
--FROM OCR_SurpassDataWarehouse..DimQuestions


--select count(1)
--from OCR_SurpassDataWarehouse..DimQuestions
--UPDATE OCR_SurpassDataWarehouse..DimQuestions
--set Deleted = 0
--WHERE CPID NOT IN (

--	SELECT distinct SDW.CPID
--	FROM OCR_SurpassDataWarehouse..DimQuestions SDW
--	LEFT JOIN (
--		SELECT distinct CPID
--		FROM OCR_SurpassDataWarehouse..DJ_CPIDs
--	) CP
--	on SDW.CPID = CP.CPID
--	where CP.CPID IS NULL
--) 
--and Deleted = 1









/*
use SAXION_ContentAuthor

SELECT 
	CAST(S.ProjectId AS nvarchar(20)) + 'P' + CAST(COALESCE(I.[ContentProducerItemId], I.VersionOriginalId, I.Id) AS nvarchar(20))  CPID
	--,I.Id ExternalId
	--,S.ProjectId ProjectKey
	--,COALESCE(I.[ContentProducerItemId], I.VersionOriginalId, I.Id) ItemKey
	--,[MajorVersion] CPVersion
	--,I.ParentId [ItemParentKey]
	--,CAST(I.Name AS nvarchar(100)) QuestionName
	--,I.CreatedById CreatedByUserKey
	--,I.CreateDate CreationDate
	--,I.ContentUpdateDate LastModifiedDate
	--,I.Mark TotalMark
	--,I.ContentType
	--,[Text] QuestionText --strip out html tags to obtain QuestionStem
	--,I.[Type] CAQuestionTypeKey
	--,I.[WorkflowStatusId]
	--,[MajorVersion] LatestVersion --current version is the latest
	--,MarkingType MarkingTypeKey
	--,I.QuestionComponentId ComponentId
	----,0 Priority
	--,0 Priority
	--,IsDeleted Deleted
	--,2 Source
	--,I.SeedPValue
	--,I.SeedUsageCount
	--,I.ImportDate
	--,I.UpdateDate
	--,QI.MarkType
	--,MRI.AwardAllCorrect
	--,I.ParentUpdateDate
	--,I.Position
	--,I.Version MinorVersion
	--,I.Latest
FROM 
	Items I
	JOIN Subjects S
		ON I.SubjectId = S.Id
	LEFT JOIN QuestionItems QI ON I.Id = QI.Id
	LEFT JOIN MultipleResponseItems MRI ON I.Id = MRI.Id
WHERE 
	I.OriginalItemId IS NULL
	*/