DECLARE @TableStats TABLE (
	TableName varchar(50)
	, rows int
	, reserved varchar(20)
	, dataKB varchar(20)
	, indexKB varchar(20)
	, unusedKB varchar(20)
)

insert @TableStats
exec sp_spaceused 'ExamSessionCandidateInteractionLogsTable'
insert @TableStats
exec sp_spaceused 'Warehouse_ExamSessionCandidateInteractionLogsTable'

select TableName
	, [rows] [RowCount]
	, replace(dataKB, 'kb','') DataKB
	, replace(dataKB, 'kb','')/1024 DataMB
	, replace(indexKB, 'kb','') IndexKB
	, replace(indexKB, 'kb','')/1024 IndexMB
from @TableStats


