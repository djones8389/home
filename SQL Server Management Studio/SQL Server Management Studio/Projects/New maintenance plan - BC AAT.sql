use master


--step 1
DBCC CHECKDB('21BrokenExams');

--step 2
DECLARE @rename VARCHAR(500);
SET @rename = 'ren "D:\Backup\SecureAssess.bak" "' + CONVERT(VARCHAR(10), GETDATE(), 102) + '.bak"';
EXECUTE xp_cmdshell @rename;
--PRINT(@rename);

--step 3
DECLARE @move VARCHAR(500);
SET @move = 'MOVE /Y "D:\Backup\' + CONVERT(VARCHAR(10), GETDATE(), 102) + '.bak"' + ' "C:\Backup\' + CONVERT(VARCHAR(10), GETDATE(), 102) + '.bak"';
EXECUTE xp_cmdshell @move
--PRINT(@move);

--step 4

--Backup DB

