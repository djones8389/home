USE [DI_BC_SecureMarker]
GO
/****** Object:  StoredProcedure [dbo].[sm_STATEMANAGEMENT_UpdateExamSessionStates_sp]    Script Date: 23/07/2015 10:02:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================
-- Author:		Dave Dixon
-- Create date: 26/05/2011
-- Description:	sproc to update the states of the Candidate Exam Versions (sessions)
-- =================================================================================
ALTER PROCEDURE [dbo].[sm_STATEMANAGEMENT_UpdateExamSessionStates_sp]

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	DECLARE @cevs TABLE(
            cevId INT
           ,ExamSessionState INT
           ,PercentageMarkingComplete INT
           ,Mark DECIMAL(18 ,10)
        )
    DECLARE @date DATETIME = GETDATE()
	
	-- calculate if any 'Result Pending' exam sessions have had items remarked
	-- check to see if they've had any items marked more recently than when the result was exported
	INSERT INTO @cevs(
		cevId		
	)	
    SELECT DISTINCT CEV.ID
    FROM dbo.CandidateExamVersions CEV
		INNER JOIN dbo.CandidateGroupResponses CGR ON CEV.ID = CGR.CandidateExamVersionID
		INNER JOIN dbo.AssignedGroupMarks AGM ON AGM.UniqueGroupResponseId = CGR.UniqueGroupResponseID
	WHERE CEV.ExamSessionState = 4
		AND	AGM.isConfirmedMark = 1
		AND	AGM.[timestamp] > CEV.lastExportDate
    
    BEGIN TRY
    
		BEGIN TRAN
			UPDATE dbo.CandidateExamVersions
				SET ExamSessionState = 5
			WHERE ID IN (SELECT cevId FROM @cevs)
	
			INSERT INTO CandidateExamVersionStatuses
			(
				-- ID -- this column value is auto-generated
				[Timestamp],
				StateID,
				CandidateExamVersionID
			)
			SELECT @date, 5, cevId FROM @cevs		COMMIT TRAN

		-- normal state progression	
		SET @date = GETDATE()
	
		BEGIN TRAN
				
			INSERT INTO CandidateExamVersionStatuses
			(
				-- ID -- this column value is auto-generated
				[Timestamp],
				StateID,
				CandidateExamVersionID
			)
			SELECT @date, cevs.ExamSessionState, cevs.cevId
				FROM CandidateExamVersionsForStateUpdate_view AS cevs
					INNER JOIN CandidateExamVersions cev ON cevs.cevId = cev.ID
				WHERE cevs.ExamSessionState <> cev.ExamSessionState
		
			UPDATE CEV
			SET
				ExamSessionState = cevs.ExamSessionState, 		
				PercentageMarkingComplete = cevs.PercentageMarkingComplete,
				[Mark] = cevs.[Mark]
			FROM CandidateExamVersions AS CEV
			INNER JOIN CandidateExamVersionsForStateUpdate_view AS cevs ON cevs.cevId = CEV.ID
		
			SELECT @@ROWCOUNT
		
		COMMIT TRAN
		
	END TRY
        BEGIN CATCH
            IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
            DECLARE @myErrorNum INT = ERROR_NUMBER()
            DECLARE @myMessage NVARCHAR(MAX) = ERROR_MESSAGE()
            DECLARE @myFullMessage NVARCHAR(MAX) = CONVERT(NVARCHAR(10), @myErrorNum) + ':' + @myMessage
            RAISERROR (@myFullMessage, 16, 1)
        END CATCH
        
    -- Clean up tempDb
	DELETE FROM @cevs
END
