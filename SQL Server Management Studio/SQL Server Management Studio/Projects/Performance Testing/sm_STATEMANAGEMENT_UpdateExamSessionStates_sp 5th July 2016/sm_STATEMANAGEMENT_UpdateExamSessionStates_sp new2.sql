--ALTER PROCEDURE [dbo].[sm_STATEMANAGEMENT_UpdateExamSessionStates_sp]
-- Add the parameters for the stored procedure here
DECLARE
       @examSessionState INT = 2
--AS
BEGIN
       -- SET NOCOUNT ON added to prevent extra result sets from
       -- interfering with SELECT statements.
       SET NOCOUNT ON;
       
    
	    DECLARE @cevs TABLE(
			cevId INT
			,ExamSessionState INT
			,PercentageMarkingComplete INT
			,Mark DECIMAL(18 ,10)
	)

    BEGIN TRY
    
              IF @examSessionState = 5
              BEGIN

						
						--DECLARE @date DATETIME
					   -- calculate if any 'Result Pending' exam sessions have had items remarked
					   -- check to see if they've had any items marked more recently than when the result was exported
					   INSERT INTO @cevs(
							  cevId         
					   )      
					   SELECT DISTINCT CEV.ID
					   FROM dbo.CandidateExamVersions CEV WITH(NOLOCK)
							  INNER JOIN dbo.CandidateGroupResponses CGR WITH(NOLOCK) ON CEV.ID = CGR.CandidateExamVersionID
							  INNER JOIN dbo.AssignedGroupMarks AGM WITH(NOLOCK) ON AGM.UniqueGroupResponseId = CGR.UniqueGroupResponseID
					   WHERE CEV.ExamSessionState = 4
							  AND    AGM.isConfirmedMark = 1
							  AND    AGM.[timestamp] > CEV.lastExportDate

					   UPDATE dbo.CandidateExamVersions
							  SET ExamSessionState = 5,
							  [LastManagedDate] = GETDATE()
					   WHERE ID IN (SELECT cevId FROM @cevs)
   
					   --SET @date = GETDATE()
						
						BEGIN TRAN 
                        
							INSERT INTO CandidateExamVersionStatuses
                           (
                                  [Timestamp],
                                  StateID,
                                  CandidateExamVersionID
                           )
                           SELECT GETDATE(), 5, cevId FROM @cevs

						COMMIT TRAN 
						   
					   DELETE @cevs
                     
				END

				ELSE 
				
				BEGIN

					DECLARE @cevUpdate TABLE(cevId INT, ExamSessionState INT)
					INSERT INTO @cevUpdate(cevId, ExamSessionState)
					SELECT TOP 500 CV.ID
						, CV.ExamSessionState 
					FROM dbo.CandidateExamVersions CV WITH (INDEX (IX_StateManagement))
					WHERE CV.ExamSessionState = @examSessionState 
					ORDER BY CV.LastManagedDate
						
					
					INSERT INTO @cevs(
							cevId
							,ExamSessionState
							,PercentageMarkingComplete
							,Mark      
					 )
					SELECT Y.ID
						, CASE 
							WHEN (Y.PercentageMarkingComplete = 0 AND Y.ExamSessionState NOT IN (4)) THEN 1
							WHEN (Y.PercentageMarkingComplete = 0 AND Y.ExamSessionState = 4) THEN 5
							WHEN (Y.PercentageMarkingComplete > 0 AND Y.PercentageMarkingComplete < 100 AND Y.ExamSessionState NOT IN (4,5)) THEN 2
							WHEN (Y.PercentageMarkingComplete > 0 AND Y.PercentageMarkingComplete < 100 AND Y.ExamSessionState = 4) THEN 5
							WHEN (Y.PercentageMarkingComplete = 100 AND Y.ExamSessionState != 4) THEN 3
							WHEN (Y.PercentageMarkingComplete = 100 AND Y.ExamSessionState = 4) THEN 4
							WHEN (Y.PercentageMarkingComplete < 100 AND Y.ExamSessionState = 5) THEN 2
					  END AS 'LiveState'
					,ROUND(Y.PercentageMarkingComplete,0) as PercentageMarkingComplete
					,CurrentMark
					FROM (
					SELECT
						CEV.cevId AS 'ID'
						,ExamSessionState	AS 'ExamSessionState'				
						,COUNT(NULLIF(UGR.confirmedMark, NULL))*100/COUNT(UGR.ID) AS 'PercentageMarkingComplete'
						,ISNULL(SUM(ISNULL(m.[Mark], UGRL.confirmedMark)), 0) as 'CurrentMark'
					FROM @cevUpdate CEV
						INNER JOIN dbo.CandidateGroupResponses CGR WITH(NOLOCK) ON CGR.CandidateExamVersionID = CEV.cevId
						INNER JOIN dbo.UniqueGroupResponses UGR WITH(NOLOCK) on UGR.id = CGR.UniqueGroupResponseID							
						INNER JOIN dbo.ActiveGroups_view ACT WITH(NOLOCK) ON  ACT.GroupDefinitionID = UGR.GroupDefinitionID
						INNER JOIN dbo.UniqueGroupResponseLinks UGRL WITH(NOLOCK) ON  UGRL.UniqueGroupResponseId = UGR.ID
						INNER JOIN [dbo].[CandidateExamVersionGroups_view] CEVG WITH(NOLOCK) 
							ON CEVG.CandidateExamVersionID = CEV.cevId 
							AND CEVG.GroupDefinitionID = UGR.GroupDefinitionID
						LEFT JOIN dbo.Moderations M WITH(NOLOCK) ON  M.UniqueResponseId = UGRL.UniqueResponseId AND M.CandidateExamVersionID = CEV.cevId AND m.IsActual = 1				
					GROUP BY CEV.cevId, CEV.ExamSessionState
					) Y
					INNER JOIN CandidateExamVersions CEV
					on CEV.ID = Y.ID
	
					WHERE 
						   (
								  CEV.ExamSessionState != Y.ExamSessionState 
								  OR ISNULL(CEV.PercentageMarkingComplete,0) != ROUND(Y.PercentageMarkingComplete,0)
								  OR  (Y.ExamSessionState IN (3, 4) AND ROUND(Y.PercentageMarkingComplete, 0) = 100 AND Y.CurrentMark != CEV.Mark)
						   )
					AND  Y.ExamSessionState IS NOT NULL    

				END
			


				BEGIN TRAN --Moved to only include DML
				 
                     INSERT INTO CandidateExamVersionStatuses
                     (
                           -- ID -- this column value is auto-generated
                           [Timestamp],
                           StateID,
                           CandidateExamVersionID
                     )
                     SELECT GETDATE(), ExamSessionState, cevId
                     FROM @cevs 
                     --INNER JOIN CandidateExamVersions cev WITH(NOLOCK) ON cevs.cevId = cev.ID	 /*This  already gets validated within INSERT INTO @cevs*/
                     --WHERE cevs.ExamSessionState <> cev.ExamSessionState;              

					
                     UPDATE CandidateExamVersions
                     SET [LastManagedDate] = GETDATE()
                     FROM CandidateExamVersions 
                     INNER JOIN @cevUpdate AS cevs ON cevs.cevId = CandidateExamVersions.ID;

                     UPDATE CandidateExamVersions
                     SET
                           ExamSessionState = cevs.ExamSessionState,              
                           PercentageMarkingComplete = cevs.PercentageMarkingComplete,
                           [Mark] = cevs.[Mark]
						   --,[LastManagedDate] = GETDATE()		/*Wouldn't this mean it gets updated twice??*/
                     FROM CandidateExamVersions 
                     INNER JOIN @cevs AS cevs ON cevs.cevId = CandidateExamVersions.ID;
              
                     SELECT @@ROWCOUNT
              
              COMMIT TRAN
              
       END TRY
    BEGIN CATCH
        IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
        DECLARE @myErrorNum INT = ERROR_NUMBER()
        DECLARE @myMessage NVARCHAR(MAX) = ERROR_MESSAGE()
        DECLARE @myFullMessage NVARCHAR(MAX) = CONVERT(NVARCHAR(10), @myErrorNum) + ':' + @myMessage
        RAISERROR (@myFullMessage, 16, 1)
    END CATCH
END

