--ALTER PROCEDURE [dbo].[sm_STATEMANAGEMENT_UpdateExamSessionStates_sp]
-- Add the parameters for the stored procedure here
DECLARE
       @examSessionState INT = 2
--AS
BEGIN
       -- SET NOCOUNT ON added to prevent extra result sets from
       -- interfering with SELECT statements.
       SET NOCOUNT ON;
       
    
	    DECLARE @cevs TABLE(
			cevId INT
			,ExamSessionState INT
			,PercentageMarkingComplete INT
			,Mark DECIMAL(18 ,10)
	)

    BEGIN TRY
    
              IF @examSessionState = 5
              BEGIN

						
						--DECLARE @date DATETIME
					   -- calculate if any 'Result Pending' exam sessions have had items remarked
					   -- check to see if they've had any items marked more recently than when the result was exported
					   INSERT INTO @cevs(
							  cevId         
					   )      
					   SELECT DISTINCT CEV.ID
					   FROM dbo.CandidateExamVersions CEV WITH(NOLOCK)
							  INNER JOIN dbo.CandidateGroupResponses CGR WITH(NOLOCK) ON CEV.ID = CGR.CandidateExamVersionID
							  INNER JOIN dbo.AssignedGroupMarks AGM WITH(NOLOCK) ON AGM.UniqueGroupResponseId = CGR.UniqueGroupResponseID
					   WHERE CEV.ExamSessionState = 4
							  AND    AGM.isConfirmedMark = 1
							  AND    AGM.[timestamp] > CEV.lastExportDate

					   UPDATE dbo.CandidateExamVersions
							  SET ExamSessionState = 5,
							  [LastManagedDate] = GETDATE()
					   WHERE ID IN (SELECT cevId FROM @cevs)
   
					   --SET @date = GETDATE()
						
						BEGIN TRAN 
                        
							INSERT INTO CandidateExamVersionStatuses
                           (
                                  [Timestamp],
                                  StateID,
                                  CandidateExamVersionID
                           )
                           SELECT GETDATE(), 5, cevId FROM @cevs

						COMMIT TRAN 
						   
					   DELETE @cevs
                     
				END

				--ELSE 
				
				--BEGIN
					 DECLARE @cevUpdate TABLE(cevId INT, ExamSessionState INT, PercentageMarkingComplete float, currentMark decimal(10,3))
					 INSERT INTO @cevUpdate(cevId, ExamSessionState,PercentageMarkingComplete,currentMark)
					 select ID
						 , ExamSessionState
						 , PercentageMarkingComplete
						 , CurrentMark
					 FROM (
					 SELECT TOP 500
							CEV.ID AS 'ID'
						   , CASE 
								  WHEN (PercentageMarkingComplete = 0 AND ExamSessionState NOT IN (4)) THEN 1
								  WHEN (PercentageMarkingComplete = 0 AND ExamSessionState = 4) THEN 5
								  WHEN (PercentageMarkingComplete > 0 AND PercentageMarkingComplete < 100 AND ExamSessionState NOT IN (4,5)) THEN 2
								  WHEN (PercentageMarkingComplete > 0 AND PercentageMarkingComplete < 100 AND ExamSessionState = 4) THEN 5
								  WHEN (PercentageMarkingComplete = 100 AND ExamSessionState != 4) THEN 3
								  WHEN (PercentageMarkingComplete = 100 AND ExamSessionState = 4) THEN 4
								  WHEN (PercentageMarkingComplete < 100 AND ExamSessionState = 5) THEN 2
							  END AS ExamSessionState    
						   , LastManagedDate
						   ,COUNT(NULLIF(CGR.confirmedMark, NULL))*100/COUNT(CGR.Id) AS 'PercentageMarkingComplete'
						   ,ISNULL(SUM(ISNULL(m.[Mark], UGRL.confirmedMark)), 0) as 'CurrentMark'
					   
					 FROM CandidateExamVersions CEV  WITH (INDEX (IX_StateManagement))
				     INNER JOIN (
							SELECT DISTINCT
								UniqueGroupResponses.ID
								,UniqueGroupResponses.confirmedMark
								,CandidateGroupResponses.CandidateExamVersionID
								, CandidateGroupResponses.UniqueGroupResponseID
								, UniqueGroupResponses.GroupDefinitionID
							FROM dbo.CandidateGroupResponses 
							INNER JOIN dbo.UniqueGroupResponses 
							ON UniqueGroupResponses.ID = CandidateGroupResponses.UniqueGroupResponseID
							) CGR 
							ON CGR.CandidateExamVersionID = CEV.ID					                                           
						INNER JOIN dbo.ActiveGroups_view ACT WITH(NOLOCK) ON  ACT.GroupDefinitionID = CGR.GroupDefinitionID
					    INNER JOIN dbo.UniqueGroupResponseLinks UGRL WITH(NOLOCK) ON  UGRL.UniqueGroupResponseId = CGR.ID
						LEFT JOIN dbo.Moderations M WITH(NOLOCK) ON  M.UniqueResponseId = UGRL.UniqueResponseId AND M.CandidateExamVersionID = CEV.ID AND m.IsActual = 1					   
						where ExamSessionState = @examSessionState  
						GROUP BY CEV.ID, CEV.ExamSessionState, CEV.LastManagedDate, PercentageMarkingComplete
						ORDER BY CEV.LastManagedDate ASC
						) ScriptMarks_CTE


		
					INSERT INTO @cevs(
							cevId
							,ExamSessionState
							,PercentageMarkingComplete
							,Mark      
					 )
					SELECT CEV.ID
						, Y.ExamSessionState
						, ROUND(Y.PercentageMarkingComplete,0) as PercentageMarkingComplete
						, CurrentMark
					FROM @cevUpdate Y
					INNER JOIN CandidateExamVersions CEV
					on CEV.ID = Y.cevId
					WHERE 
						   (
								  CEV.ExamSessionState != Y.ExamSessionState 
								  OR ISNULL(CEV.PercentageMarkingComplete,0) != ROUND(Y.PercentageMarkingComplete,0)
								  OR  (Y.ExamSessionState IN (3, 4) AND ROUND(Y.PercentageMarkingComplete, 0) = 100 AND Y.CurrentMark != CEV.Mark)
						   )
					AND  Y.ExamSessionState IS NOT NULL    

				--END
			


				BEGIN TRAN --Moved to only include DML
				 
                     INSERT INTO CandidateExamVersionStatuses
                     (
                           -- ID -- this column value is auto-generated
                           [Timestamp],
                           StateID,
                           CandidateExamVersionID
                     )
                     SELECT GETDATE(), ExamSessionState, cevId
                     FROM @cevs 
                     --INNER JOIN CandidateExamVersions cev WITH(NOLOCK) ON cevs.cevId = cev.ID	 /*This  already gets validated within INSERT INTO @cevs*/
                     --WHERE cevs.ExamSessionState <> cev.ExamSessionState;              

					
                     UPDATE CandidateExamVersions
                     SET [LastManagedDate] = GETDATE()
                     FROM CandidateExamVersions 
                     INNER JOIN @cevUpdate AS cevs ON cevs.cevId = CandidateExamVersions.ID;

                     UPDATE CandidateExamVersions
                     SET
                           ExamSessionState = cevs.ExamSessionState,              
                           PercentageMarkingComplete = cevs.PercentageMarkingComplete,
                           [Mark] = cevs.[Mark]
						   --,[LastManagedDate] = GETDATE()		/*Wouldn't this mean it gets updated twice??*/
                     FROM CandidateExamVersions 
                     INNER JOIN @cevs AS cevs ON cevs.cevId = CandidateExamVersions.ID;
              
                     SELECT @@ROWCOUNT
              
              COMMIT TRAN
              
       END TRY
    BEGIN CATCH
        IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
        DECLARE @myErrorNum INT = ERROR_NUMBER()
        DECLARE @myMessage NVARCHAR(MAX) = ERROR_MESSAGE()
        DECLARE @myFullMessage NVARCHAR(MAX) = CONVERT(NVARCHAR(10), @myErrorNum) + ':' + @myMessage
        RAISERROR (@myFullMessage, 16, 1)
    END CATCH
END

