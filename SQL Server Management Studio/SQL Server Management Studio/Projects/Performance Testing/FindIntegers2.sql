select 'Select MAX (' + C.name + ') AS ' + C.name + ' from ' + O.name
FROM sys.all_objects as O	
	inner join sys.tables as T on T.object_id = O.object_id	
	inner join sys.columns c ON c.object_id = t.object_id
where o.schema_id = 1
	and o.Type = 'u'
	and user_type_id = '56' --system_type_id
	and t.name = 'ExamSessionTable'
	


select C.name
FROM sys.all_objects as O	
	inner join sys.tables as T on T.object_id = O.object_id	
	inner join sys.columns c ON c.object_id = t.object_id
where o.schema_id = 1
	and o.Type = 'u'
	and user_type_id = '56' --system_type_id
	and t.name = 'ExamSessionTable'
	

Select MAX (ID) AS ID
	  ,  MAX (ScheduledExamID) AS ScheduledExamID 
	  ,  MAX (UserID) AS UserID

from ExamSessionTable


DECLARE TEST CURSOR FOR

select C.name, T.name
FROM sys.all_objects as O	
	inner join sys.tables as T on T.object_id = O.object_id	
	inner join sys.columns c ON c.object_id = t.object_id
where o.schema_id = 1
	and o.Type = 'u'
	and user_type_id = '56' --system_type_id
	and t.name = 'ExamSessionTable'

DECLARE @Column nvarchar(100)

OPEN TEST

FETCH NEXT FROM TEST INTO @Column

WHILE @@FETCH_STATUS = 0

BEGIN

SELECT @Column

FETCH NEXT FROM TEST INTO @Column

END

CLOSE TEST
DEALLOCATE TEST







/*

Select MAX (ID) AS ID from WAREHOUSE_ExamSessionItemResponseTable
Select MAX (WAREHOUSEExamSessionID) AS WAREHOUSEExamSessionID from WAREHOUSE_ExamSessionItemResponseTable
Select MAX (ItemVersion) AS ItemVersion from WAREHOUSE_ExamSessionItemResponseTable
Select MAX (ItemMark) AS ItemMark from WAREHOUSE_ExamSessionItemResponseTable
Select MAX (examSessionId) AS examSessionId from WAREHOUSE_ExamSessionTable_ShrededItems
Select MAX (candidateId) AS candidateId from WAREHOUSE_ExamSessionTable_ShrededItems
Select MAX (ItemVersion) AS ItemVersion from WAREHOUSE_ExamSessionTable_ShrededItems
Select MAX (selectedCount) AS selectedCount from WAREHOUSE_ExamSessionTable_ShrededItems
Select MAX (correctAnswerCount) AS correctAnswerCount from WAREHOUSE_ExamSessionTable_ShrededItems
Select MAX (markedMetadataCount) AS markedMetadataCount from WAREHOUSE_ExamSessionTable_ShrededItems
Select MAX (ItemType) AS ItemType from WAREHOUSE_ExamSessionTable_ShrededItems
Select MAX (ExamSessionID) AS ExamSessionID from WAREHOUSE_ExamSessionTable_ShreddedItems_Mark
Select MAX (LearningOutcome) AS LearningOutcome from WAREHOUSE_ExamSessionTable_ShreddedItems_Mark
Select MAX (Id) AS Id from PerformanceTest_INT
Select MAX (ID) AS ID from WAREHOUSE_ExamSessionTable
Select MAX (ExamSessionID) AS ExamSessionID from WAREHOUSE_ExamSessionTable
Select MAX (WAREHOUSEScheduledExamID) AS WAREHOUSEScheduledExamID from WAREHOUSE_ExamSessionTable
Select MAX (WAREHOUSEUserID) AS WAREHOUSEUserID from WAREHOUSE_ExamSessionTable
Select MAX (PreviousExamState) AS PreviousExamState from WAREHOUSE_ExamSessionTable
Select MAX (reMarkStatus) AS reMarkStatus from WAREHOUSE_ExamSessionTable
Select MAX (reMarkUser) AS reMarkUser from WAREHOUSE_ExamSessionTable
Select MAX (WarehouseExamState) AS WarehouseExamState from WAREHOUSE_ExamSessionTable
Select MAX (copiedFromExamSessionID) AS copiedFromExamSessionID from WAREHOUSE_ExamSessionTable
Select MAX (LocalScanNumPages) AS LocalScanNumPages from WAREHOUSE_ExamSessionTable
Select MAX (AssignedMarkerUserID) AS AssignedMarkerUserID from WAREHOUSE_ExamSessionTable
Select MAX (AssignedModeratorUserID) AS AssignedModeratorUserID from WAREHOUSE_ExamSessionTable
Select MAX (examSessionId) AS examSessionId from WAREHOUSE_ExamSessionTable_Shreded
Select MAX (examVersionId) AS examVersionId from WAREHOUSE_ExamSessionTable_Shreded
Select MAX (qualificationid) AS qualificationid from WAREHOUSE_ExamSessionTable_Shreded
Select MAX (originatorId) AS originatorId from WAREHOUSE_ExamSessionTable_Shreded
Select MAX (scheduledDurationValue) AS scheduledDurationValue from WAREHOUSE_ExamSessionTable_Shreded
Select MAX (previousExamState) AS previousExamState from WAREHOUSE_ExamSessionTable_Shreded
Select MAX (examType) AS examType from WAREHOUSE_ExamSessionTable_Shreded
Select MAX (actualDuration) AS actualDuration from WAREHOUSE_ExamSessionTable_Shreded
Select MAX (reMarkStatus) AS reMarkStatus from WAREHOUSE_ExamSessionTable_Shreded
Select MAX (qualificationLevel) AS qualificationLevel from WAREHOUSE_ExamSessionTable_Shreded
Select MAX (centreId) AS centreId from WAREHOUSE_ExamSessionTable_Shreded
Select MAX (WarehouseExamState) AS WarehouseExamState from WAREHOUSE_ExamSessionTable_Shreded
Select MAX (passType) AS passType from WAREHOUSE_ExamSessionTable_Shreded
Select MAX (voidJustificationLookupTableId) AS voidJustificationLookupTableId from WAREHOUSE_ExamSessionTable_Shreded
Select MAX (totalTimeSpent) AS totalTimeSpent from WAREHOUSE_ExamSessionTable_Shreded
Select MAX (defaultDuration) AS defaultDuration from WAREHOUSE_ExamSessionTable_Shreded
Select MAX (WAREHOUSEScheduledExamID) AS WAREHOUSEScheduledExamID from WAREHOUSE_ExamSessionTable_Shreded
Select MAX (WAREHOUSEUserID) AS WAREHOUSEUserID from WAREHOUSE_ExamSessionTable_Shreded
Select MAX (examId) AS examId from WAREHOUSE_ExamSessionTable_Shreded
Select MAX (userId) AS userId from WAREHOUSE_ExamSessionTable_Shreded
Select MAX (LocalScanNumPages) AS LocalScanNumPages from WAREHOUSE_ExamSessionTable_Shreded
Select MAX (AssignedMarkerUserID) AS AssignedMarkerUserID from WAREHOUSE_ExamSessionTable_Shreded
Select MAX (AssignedModeratorUserID) AS AssignedModeratorUserID from WAREHOUSE_ExamSessionTable_Shreded

*/