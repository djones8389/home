if OBJECT_ID('tempdb..#TEMPINSERTTABLE') IS NOT NULL DROP TABLE #TEMPINSERTTABLE;

DECLARE @myExamSessionId2 int = 1406250

CREATE TABLE #TEMPINSERTTABLE (
	[ExamSessionId] INT
	,[StructureXml] XML
	,[resultData] XML
	)

INSERT INTO #TEMPINSERTTABLE
SELECT [examSessionId]
	,[structureXml]
	,[resultData]
FROM [sa_CandidateResultRelease_View]
WHERE [examSessionId] IN (
		@myExamSessionId2
		)

WHILE (
		SELECT COUNT([ExamSessionId])
		FROM #TEMPINSERTTABLE
		) > 0
BEGIN
	--DECLARE @myExamSessionId2 INT
	DECLARE @myStructureXml XML
	DECLARE @myResultData XML

	SELECT TOP 1 @myExamSessionId2 = [ExamSessionId]
		,@myStructureXml = [StructureXml]
		,@myResultdata = [resultData]
	FROM #TEMPINSERTTABLE

	BEGIN TRANSACTION

	--INSERT INTO [ExamSessionTable_ShrededItems]
	SELECT @myExamSessionId2
		,ParamValues.Item.query('data(@id)').value('.', 'nvarchar(max)')
		,ParamValues.Item.query('data(@name)').value('.', 'nvarchar(max)')
		,CONVERT(DECIMAL(6, 3), ParamValues.Item.query('data(@userMark)').value('.', 'nvarchar(max)'))
		,ParamValues.Item.query('data(@markerUserMark)').value('.', 'nvarchar(max)')
		,CONVERT(DECIMAL(6, 3), @myResultData.query('data(/exam/@userPercentage)').value('.', 'nvarchar(max)'))
		,(
			SELECT [ItemVersion]
			FROM [ExamSessionItemResponseTable]
			WHERE [ExamSessionItemResponseTable].ExamSessionId = @myExamSessionId2
				AND [ItemID] = ParamValues.Item.query('data(@id)').value('.', 'nvarchar(max)')
			)
		,-- item version
		(
			SELECT [ItemResponseData]
			FROM [ExamSessionItemResponseTable]
			WHERE [ExamSessionItemResponseTable].ExamSessionId = @myExamSessionId2
				AND [ItemID] = ParamValues.Item.query('data(@id)').value('.', 'nvarchar(max)')
			)
		,-- response xml
		ISNULL((
				SELECT ' ' + CONVERT(NVARCHAR(MAX), [ItemResponseData].query('data(//c[@typ=''10'']/i[@sl=''1'']/@ac)')) + ' '
				FROM [ExamSessionItemResponseTable]
				WHERE [ExamSessionItemResponseTable].ExamSessionId = @myExamSessionId2
					AND [ItemID] = ParamValues.Item.query('data(@id)').value('.', 'nvarchar(max)')
				), ' UA ')
		,-- options chosen
		ISNULL((
				SELECT CONVERT(NVARCHAR(MAX), [ItemResponseData].query('count(//i[@sl=''1''])'))
				FROM [ExamSessionItemResponseTable]
				WHERE [ExamSessionItemResponseTable].ExamSessionId = @myExamSessionId2
					AND [ItemID] = ParamValues.Item.query('data(@id)').value('.', 'nvarchar(max)')
				), 0)
		,-- selected count
		ISNULL((
				SELECT CONVERT(NVARCHAR(MAX), [ItemResponseData].query('count(//i[@ca=''1''])'))
				FROM [ExamSessionItemResponseTable]
				WHERE [ExamSessionItemResponseTable].ExamSessionId = @myExamSessionId2
					AND [ItemID] = ParamValues.Item.query('data(@id)').value('.', 'nvarchar(max)')
				), 1)
		,-- correct answer count
		ParamValues.Item.query('data(@totalMark)').value('.', 'nvarchar(max)')
	FROM @myStructureXml.nodes('/assessmentDetails/assessment/section/item') AS ParamValues(Item)

	DELETE TOP (1)
	FROM #TEMPINSERTTABLE

	COMMIT TRANSACTION
END

DROP TABLE #TEMPINSERTTABLE