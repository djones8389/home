use SecureAssess_Populated

--delete from examsessionitemresponsetable where examsessionid = 4 and itemid = '4043P4057';

--select * from examsessionitemresponsetable where examsessionid = 4 and itemid = '4043P4057';

DECLARE
	 @examInstanceId   int = 4,  
	 @itemId     nvarchar(max) = '4043P4057',  
	 @version    int = '9',  
	 @markerComment xml = '<entry>
		<comment/>
		<userId>96999</userId>
		<userForename>David</userForename>
		<userSurname>Green</userSurname>
		<assignedMark>6</assignedMark>
		<annotations>
			<comments/>
		</annotations>
		<version>0</version>
		<dateCreated>05/08/2016</dateCreated>
	</entry>'


BEGIN  
    
	DECLARE @errorReturnString  nvarchar(max)  
	DECLARE @errorNum  nvarchar(100)  
	DECLARE @errorMess  nvarchar(max)  
  
	SET NOCOUNT ON;  

	 BEGIN TRY  

	   DECLARE @tempStructureXML AS XML =
	   (
		 SELECT CAST(StructureXml AS XML) 
		 FROM ExamSessionTable 
		 WHERE ID = @examInstanceId   
	   );
	   
	   DECLARE @myHumanMark tinyint  = 
	   (
		 select @markerComment.value('data(entry/assignedMark)[1]','tinyint')
	   );

	   SET @tempStructureXml.modify('replace value of (/assessmentDetails[1]/assessment[1]/section/item[@id = sql:variable("@itemId")]/@markerUserMark)[1] with sql:variable("@myHumanMark")');
	   SET @tempStructureXml.modify('replace value of (/assessmentDetails[1]/assessment[1]/section/item[@id = sql:variable("@itemId")]/@markingState)[1] with ("1")');   

	  BEGIN TRANSACTION  

		   UPDATE ExamSessionTable 
		   SET StructureXml = @tempStructureXML 
		   WHERE ID = @examInstanceId;   
 
			 IF EXISTS(SELECT 1 FROM ExamSessionItemResponseTable WHERE ExamSessionID = @examInstanceId	and ItemID = @itemId and ItemVersion = @version)                    
					DECLARE @existing XML = 
					(  
						select [MarkerResponseData] 
						from examsessionitemresponsetable
						where examsessionid = @examInstanceId 
							and itemid = @itemId 
					)
					
				IF @existing IS NOT NULL 
					SET @existing.modify('insert sql:variable("@markerComment") as last into (/entries)[1]')

				UPDATE [ExamSessionItemResponseTable]
				set MarkerResponseData = @existing 
				WHERE ExamSessionID = @examInstanceId	
					and ItemID = @itemId 
					and ItemVersion = @version;
	
			IF NOT EXISTS(SELECT 1 	FROM ExamSessionItemResponseTable WHERE ExamSessionID = @examInstanceId	and ItemID = @itemId and ItemVersion = @version)                    
				INSERT INTO [dbo].[ExamSessionItemResponseTable]
					([ExamSessionID]
					,[ItemID]
					,[ItemVersion]
					,[ItemResponseData]
					,[MarkerResponseData]
					,[ItemMark]
					,[MarkingIgnored])
				VALUES
					(@examInstanceId
					,@itemId
					,@version
					,''
					,'<entries>'+convert(nvarchar(max),@markerComment)+'</entries>'
					,NULL
					,0
					)
			
		    
			 /*execute the following stored procedure after completing the above transaction.*/
			EXECUTE dbo.sa_CANDIDATEEXAMSTATEMANAGEMENTSERVICE_UpdateCumulativeMarkingProgress_sp @examInstanceId

			--SET @errorReturnString = '<result errorCode="0"><return>True</return></result>'  
			--SELECT @errorReturnString  

	   COMMIT TRANSACTION;  

	  END TRY  

	  BEGIN CATCH  
	   SET @errorNum = (SELECT ERROR_NUMBER() AS ErrorNumber)  
	   SET @errorMess = (SELECT ERROR_MESSAGE() AS ErrorMessage)  
	   SET @errorReturnString = '<result errorCode="2"><return>SQL Error, Number: ' + @errorNum + ' MESSAGE: ' + @errorMess + '</return></result>'  
	   SELECT @errorReturnString  
	   ROLLBACK TRANSACTION;  
	 END CATCH  
END

GO


