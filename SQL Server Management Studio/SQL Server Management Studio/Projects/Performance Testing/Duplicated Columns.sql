select C.Name, TY.Name
from sys.columns C
inner join sys.tables T
on T.object_id = C.object_id
inner join sys.types ty
on ty.system_type_id = C.system_type_id
where t.name = 'WAREHOUSE_ExamSessionTable'

INTERSECT
select C.Name, TY.Name
from sys.columns C
inner join sys.tables T
on T.object_id = C.object_id
inner join sys.types ty
on ty.system_type_id = C.system_type_id
where t.name = 'WAREHOUSE_ExamSessionTable_Shreded'

ORDER BY 1