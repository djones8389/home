SELECT B.*
FROM (
SELECT A.[OBJECT_NAME]
	, sum(a.execution_count) execution_count
	, sum(cpu) cpu
	, sum(elapsed) elapsed
	, sum(LOGICAL_READS) LOGICAL_READS
	, sum(logical_writes) logical_writes
	, sum(PHYSICAL_READS) PHYSICAL_READS
FROM (
SELECT [OBJECT_NAME]
      ,[execution_count]
      ,[CPU] / execution_count [CPU]
      ,[ELAPSED] / execution_count [ELAPSED]
      ,[LOGICAL_READS] / execution_count [LOGICAL_READS]
      ,[LOGICAL_WRITES] / execution_count [LOGICAL_WRITES]
      ,[PHYSICAL_READS] / execution_count [PHYSICAL_READS]
  FROM [PSCollector].[dbo].[SprocMetrics]
  where SCHEMA_NAME != 'ETL'
	and execution_count > 100  
  ) A
 group by [OBJECT_NAME]
) B
--where OBJECT_NAME = 'sa_REPORTINGSERVICE_GetExamInstancesForReporting_sp'
order by (elapsed/execution_count) desc

