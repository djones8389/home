USE [DI_BC_ItemBank]
GO
/****** Object:  StoredProcedure [dbo].[ib3_SetProjectStructure_SP]    Script Date: 07/21/2015 15:55:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[DJ_ib3_SetProjectStructure_SP]

	@ProjectID int,
	@ProjectMetaDataXml xml,
	@ProjectStructureXml xml   
AS

	SET NOCOUNT ON;
   
    --Add all relevent Assessment ID's into a table variable to use later in the update statement, to prevent an XPath update statement--
    
    DECLARE @AssessmentsToUpdate TABLE (ID INT)
	INSERT @AssessmentsToUpdate		
	SELECT 
		DISTINCT ID
	FROM dbo.AssessmentTable
	CROSS APPLY AssessmentRules.nodes('//ProjectIDList/ID') a(b)
	WHERE a.b.value('(.)[1]','int') =  @ProjectID
    
  
    --Store all items and their totalMarks    
        
    DECLARE @XMLholder TABLE (ProjectStructureXML XML)    
		INSERT @XMLholder 
			SELECT @ProjectStructureXml
    
    DECLARE @ItemTotalMark TABLE (ItemID NVARCHAR(20), TotalMark tinyint)
		INSERT @ItemTotalMark
			SELECT 
				my.item.value('@ID[1]','nvarchar(20)') AS ItemID
			  , my.item.value('@TotalMark[1]','tinyint') AS TotalMark
			FROM @XMLholder
			CROSS APPLY ProjectStructureXML.nodes('/P//I') my(item)
		
	--Validation
	IF (SELECT COUNT(ProjectID)FROM ProjectTable WHERE ProjectID=@ProjectID) < 1
		BEGIN		
			SELECT 0
			RETURN
		END
	ELSE	
	
		BEGIN TRY
			BEGIN TRANSACTION
			
			   BEGIN
		
				UPDATE [dbo].[ItemTable] 
				SET [TotalMark]=B.TotalMark 
				FROM [dbo].[ItemTable] A
				INNER JOIN @ItemTotalMark B
				ON A.[ItemRef] = B.ItemID                                
				WHERE  A.[ProjectID]=@ProjectID			--Only do the one project
						AND A.[ItemRef] = B.ItemID		--Extra step to ensure they match
						AND A.[TotalMark]=-99			--Presumably to omit certains ones?
						AND A.TotalMark != B.TotalMark;	--Only update the ones that need it
	           END 

				DELETE @ItemTotalMark                     
				
				BEGIN
				
				-- Update project structure
				UPDATE [dbo].[ProjectTable]
				SET [ProjectMetaData] = @ProjectMetaDataXml
				  ,[ProjectStructure] = @ProjectStructureXml
				WHERE [ProjectID] = @ProjectID;
				
				END
				-- Due to updated project structure, existing assessment rules
				-- may now be invalid so
				-- update requiresValidation field for each assessment that has rules
				-- based on this project structure
				BEGIN
				 
				UPDATE [dbo].[AssessmentTable]
				SET [RequiresValidation]=1, [VersionNumber] = [VersionNumber]+1
				FROM [dbo].[AssessmentTable] A
				INNER JOIN @AssessmentsToUpdate B
				On A.ID = B.ID;
				
				END		
							
			COMMIT TRANSACTION		
		END TRY
BEGIN CATCH
    SELECT 
        ERROR_NUMBER() AS ErrorNumber
        ,ERROR_SEVERITY() AS ErrorSeverity
        ,ERROR_STATE() AS ErrorState
        ,ERROR_PROCEDURE() AS ErrorProcedure
        ,ERROR_LINE() AS ErrorLine
        ,ERROR_MESSAGE() AS ErrorMessage;

    IF @@TRANCOUNT > 0
        ROLLBACK TRANSACTION;
END CATCH;

IF @@TRANCOUNT > 0
    COMMIT TRANSACTIOn
GO
