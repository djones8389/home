USE [Develop-MI-IB]
GO


--DECLARE	@return_value int

--EXEC	@return_value = [dbo].[ib3_GetAssessmentsForQualification_SP]
--		@qualificationID = 1
--		,@StatusListXml='<StatusList><Status>2</Status><Status>3</Status></StatusList>'
--SELECT	'Return Value' = @return_value

--GO



declare @qualificationID tinyint = 6;
declare @StatusListxml nvarchar(max) = '<StatusList><Status>2</Status><Status>3</Status></StatusList>';

if OBJECT_ID('tempdb..#TEMP_STATUS_TABLE') is not null drop table #TEMP_STATUS_TABLE;

CREATE TABLE #TEMP_STATUS_TABLE
	(	
		[Status]	int
    )

	DECLARE @statusList varchar(max)
	
	
	

		DECLARE @hdoc	int
		EXEC sp_xml_preparedocument @hdoc OUTPUT, @StatusListXml

		-- Insert the XML into the table
		INSERT INTO #TEMP_STATUS_TABLE
		SELECT *
		FROM OPENXML(@hdoc, 'StatusList/Status',2)
		WITH ([Status]	int	'.')

		-- create a csv of the passed in status values
		if len(@statusList) = 0 or @statusList is null
		select @statusList = COALESCE(@statusList + ',', '') + 
		   CAST([Status] AS varchar(max))
		FROM #TEMP_STATUS_TABLE
		
		--remove leading and trailing delimiters
		SET @statusList = REPLACE(LTRIM(REPLACE(@statusList, ',', ' ')), ' ', ',')

		select @statusList

		BEGIN
			SELECT 
					AssessmentGroupTable.ID												AS "Assessment/AssessmentID",
					AssessmentGroupTable.Name											AS "Assessment/AssessmentName",
					AssessmentGroupTable.Ref											AS "Assessment/ExternalReference",
					--@myParentQualificationStatus										AS "Assessment/Status",
					CONVERT(varchar(11), [AssessmentGroupTable].[ValidFromDate], 106)	AS "Assessment/ValidFromDate",
					CONVERT(varchar(11), [AssessmentGroupTable].[ExpiryDate], 106)		AS "Assessment/ExpiryDate",
					CONVERT(varchar(8), [AssessmentGroupTable].[StartTime], 114)		AS "Assessment/StartTime", 
					CONVERT(varchar(8), [AssessmentGroupTable].[EndTime], 114)			AS "Assessment/EndTime",
					[AssessmentGroupTable].[AllowOffline]								AS "Assessment/Offline",
					AssessmentGroupTable.MinResitTimeInDays								AS "Assessment/resitTimeInDays",
					AssessmentGroupTable.AllowVersionRecycling							AS "Assessment/allowRecycling",
					AssessmentGroupTable.RandomiseExams									AS "Assessment/randomiseExams",
					AssessmentGroupTable.ExamDeliveryOption								AS "Assessment/ExamDeliveryOption",
					dbo.GetAssessmentVersionsForSuppliedStatusList(AssessmentGroupTable.ID, @statusList)		
																						AS "Assessment/versions/*",
					AssessmentGroupTable.ExamDeliverySystemOption						AS "Assessment/DeliverySystem"
			FROM [dbo].AssessmentGroupTable
			WHERE [QualificationID] = @qualificationID
			AND [IsValid]=1 AND (SELECT COUNT(AssessmentTable.ID) FROM AssessmentTable
								WHERE AssessmentTable.AssessmentGroupID = AssessmentGroupTable.ID) > 0
			for xml path(''), root('AssessmentList')
		END




/*
select distinct o.name
from sys.sysobjects o
inner join sys.syscomments c
on c.id = o.id
where c.text like '%GetAssessmentVersionsForSuppliedStatusList%'
*/

