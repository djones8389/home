DECLARE	@examId INT = 8;

BEGIN
    SET NOCOUNT ON;
    
	SELECT ID
		, NAME
		, IsManualMarkingType
		, NumResponses
		, NumMarkedResponses
		, NumAwaitingReview
		, NumHumanMarkedResponses
		, NumParkedResponses
	FROM (

    SELECT GD.ID          AS 'ID'
          ,GD.Name        AS 'NAME'
		  ,CASE  
			  WHEN (COUNT(I.ID) > 0) 
				THEN 1 
				ELSE 0 
		   END AS 'IsManualMarkingType'
          ,COUNT(UGR.ID)  AS 'NumResponses'
          ,SUM(CASE WHEN UGR.ConfirmedMark IS NULL THEN 0 ELSE 1 END)	 AS 'NumMarkedResponses'
          ,SUM(CASE WHEN UGR.CI=1 AND UGR.CI_Review=0 THEN 1 ELSE 0 END) AS 'NumAwaitingReview'
          ,SUM(
               CASE 
                    WHEN UGR.ConfirmedMark IS NOT NULL
               AND UGR.CI=0
               AND AGM.ID IS NULL THEN 1
                   ELSE 0
                   END
           )              AS 'NumHumanMarkedResponses'        
          ,SUM(CASE WHEN UGR.IsEscalated = 1 THEN 1 ELSE 0 END)             AS 'NumParkedResponses'
    FROM   dbo.GroupDefinitions GD
           LEFT JOIN dbo.UniqueGroupResponses UGR ON  UGR.GroupDefinitionID = GD.ID   
           LEFT JOIN dbo.AssignedGroupMarks AGM ON  AGM.UniqueGroupResponseId = UGR.ID
                    AND AGM.MarkingMethodId = 12
                    AND AGM.IsConfirmedMark = 1
                    AND AGM.GroupDefinitionID = GD.ID
		   INNER JOIN GroupDefinitionItems GDI ON GD.ID = GDI.GroupID
		   INNER JOIN (
				select ID
				FROM Items I
				WHERE  I.MarkingType = 1
			) I 
			ON  GDI.ItemID = I.ID			

    WHERE  GD.ExamID = @examId
	
    GROUP BY
           GD.ID
          ,GD.Name

	) A
	INNER JOIN (
		SELECT distinct GDSCR.GroupDefinitionID
                    FROM   dbo.GroupDefinitionStructureCrossRef GDSCR
                        INNER JOIN dbo.ExamVersionStructures EVS ON  EVS.ID = GDSCR.ExamVersionStructureID
                    WHERE  GDSCR.Status = 0 -- 0 = Active
                        AND EVS.StatusID = 0 -- 0 = Released
	) GDS
	on GDS.GroupDefinitionID = A.ID
END
