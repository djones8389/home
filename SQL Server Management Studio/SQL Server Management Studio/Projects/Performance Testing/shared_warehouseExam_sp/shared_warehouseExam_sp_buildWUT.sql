DECLARE  @examInstanceId  int = 42486,--906939,      
		 @OriginatorID  int = 1,      
		 @returnString  nvarchar(max),
		 @mySAScheduledExamId int
		, @mySACentreId int
		, @mySACreatedById int
		, @mySAUserId int
		, @myKeyCode nvarchar(12)      
		, @AvailableForCentreReview bit -- Centre Direct     
		, @ResultDataFull xml
		, @TodaysDate nvarchar(50)
		, @mySACentreVersion int      
		, @mySACreatedByIdVersion int
		, @mySAUserIdVersion int   
		, @myWarehouseExamState INT
		, @ValidationCount INT
		, @completionDate smalldatetime 
		, @qualificationid int
		, @qualificationName nvarchar(100)
		
  SELECT       
	  @mySAScheduledExamId = ExamSessionTable.ScheduledExamID,      
	  @mySACentreId = ScheduledExamsTable.CentreID,      
	  @mySACreatedById = ScheduledExamsTable.CreatedBy,      
	  @mySAUserId = ExamSessionTable.UserID,      
	  @myKeyCode = ExamSessionTable.KeyCode,    
	  @AvailableForCentreReview = scheduledForCentreDirect,
	  @mySACentreVersion = CentreTable.version,
	  @mySACreatedByIdVersion = UserTable.version,
	  @mySAUserIdVersion = UserTable.version,
	  @ValidationCount = ValidationCount,
	  @ResultDataFull = resultDataFull,
	  @qualificationid = qualificationid,
	  @qualificationName = qualificationName
  FROM ExamSessionTable    
    
  INNER JOIN ScheduledExamsTable 
  ON ScheduledExamsTable.ID = ExamSessionTable.ScheduledExamID      
  Inner join CentreTable
  ON CentreTable.ID = ScheduledExamsTable.CentreID
  INNER JOIN UserTable
  on UserTable.ID = ExamSessionTable.UserID
  INNER Join IB3QualificationLookup
  on IB3QualificationLookup.ID = ScheduledExamsTable.qualificationID
  WHERE ExamSessionTable.ID = @examInstanceId  
  

DECLARE @WarehouseUserTableInsert TABLE
	(
		SACreatedByUser int
		, SACandidateID int
		, SAMarkerUser int
		, InsertNeeded bit
	)

  
--SA CreatedBy (Scheduler of exam)
  
	DECLARE @MaxWHScheduledUserVersion int = ( 
	SELECT MAX(version)
	FROM WAREHOUSE_UserTable    
	WHERE UserId = @mySACreatedById      
	AND OriginatorID = @OriginatorID   
	)

	IF  ISNULL(@MaxWHScheduledUserVersion, 0) <= @mySACreatedByIdVersion
	AND
	NOT EXISTS ( SELECT 1 
		  FROM WAREHOUSE_UserTable      
		  WHERE UserId = @mySACreatedById      
		  AND OriginatorID = @OriginatorID
		  AND WAREHOUSE_UserTable.version = @MaxWHScheduledUserVersion
	)
	      
	INSERT @WarehouseUserTableInsert(SACreatedByUser, InsertNeeded)
	VALUES (@mySACreatedById,'1')

--SA Candidate who sat the exam

	DECLARE @MaxWHCandidateUserVersion int = ( 
	SELECT MAX(version)
	FROM WAREHOUSE_UserTable    
	WHERE UserId = @mySAUserId      
	AND OriginatorID = @OriginatorID   
	)

	IF  ISNULL(@MaxWHCandidateUserVersion, 0) <= @mySAUserIdVersion
	AND
	NOT EXISTS ( SELECT 1 
		  FROM WAREHOUSE_UserTable      
		  WHERE UserId = @mySAUserId      
		  AND OriginatorID = @OriginatorID
		  AND WAREHOUSE_UserTable.version = @MaxWHCandidateUserVersion
	)
	    
	INSERT @WarehouseUserTableInsert(SACandidateID, InsertNeeded)
	VALUES (@mySAUserId,'1')
      

--SA Marker User Data


	DECLARE @markerUser TABLE (markerID int, Version int)	
	INSERT @markerUser(markerID, Version)
	SELECT distinct a.b.value('(.)[1]','int')
		  , (select version from UserTable where ID = a.b.value('(.)[1]','int'))
	FROM ExamSessionItemResponseTable (NOLOCK)
	inner join ExamSessionTable
	on ExamSessionTable.ID = ExamSessionItemResponseTable.ExamSessionID
	CROSS APPLY [MarkerResponseData].nodes('/entries/entry/userId') a(b)
	WHERE ExamSessionID = @examInstanceId
	  

	IF NOT EXISTS (
		
		select 1
		FROM WAREHOUSE_UserTable
		
		INNER JOIN @markerUser A
		on A.markerID = WAREHOUSE_UserTable.UserId
		
		where A.Version >= WAREHOUSE_UserTable.version

	)

	INSERT @WarehouseUserTableInsert(SAMarkerUser, InsertNeeded)
	select markerID, '1' from @markerUser

	SELECT * FROM @WarehouseUserTableInsert

  BEGIN  
    
	;with CTE (IDsToInsert) AS (

	SELECT SACreatedByUser FROM @WarehouseUserTableInsert where InsertNeeded = 1
	UNION
	SELECT SACandidateID FROM @WarehouseUserTableInsert where InsertNeeded = 1
	UNION
	SELECT SAMarkerUser FROM @WarehouseUserTableInsert where InsertNeeded = 1

	)
	
     --insert a new user record      
		 INSERT [dbo].[WAREHOUSE_UserTable]      
			 ([UserId]      
			 ,[CandidateRef]      
			 ,[Forename]      
			 ,[Surname]      
			 ,[Middlename]      
			 ,[DOB]      
			 ,[Gender]      
			 ,[SpecialRequirements]      
			 ,[AddressLine1]      
			 ,[AddressLine2]      
			 ,[Town]      
			 ,[County]      
			 ,[Country]      
			 ,[PostCode]      
			 ,[Telephone]      
			 ,[Email]      
			 ,[EthnicOrigin]      
			 ,[AccountCreationDate]      
			 ,[AccountExpiryDate]      
			 ,[ExtraInfo]      
			 ,[version]      
			 ,[OriginatorID]    
			 ,[ULN]
			 ,[Username])      
		 SELECT      
		   [UserTable].[ID]      
		   ,[UserTable].[CandidateRef]      
		   ,[UserTable].[Forename]      
		   ,[UserTable].[Surname]      
		   ,[UserTable].[Middlename]      
		   ,[UserTable].[DOB]      
		   ,[UserTable].[Gender]      
		   ,[UserTable].[SpecialRequirements]      
		   ,[UserTable].[AddressLine1]      
		   ,[UserTable].[AddressLine2]      
		   ,[UserTable].[Town]      
		   ,[CountyLookupTable].[County]      
		   ,[CountryLookupTable].[Country]      
		   ,[UserTable].[PostCode]      
		   ,[UserTable].[Telephone]      
		   ,[UserTable].[Email]      
		   ,[EnthnicOriginLookupTable].[Name]      
		   ,[UserTable].[AccountCreationDate]      
		   ,[UserTable].[AccountExpiryDate]      
		   ,[UserTable].[ExtraInfo]      
		   ,[UserTable].[version]      
		   ,@OriginatorID     
		   ,[UserTable].[ULN] 
		   ,[UserTable].[Username]   
		 FROM [dbo].[UserTable],[dbo].[CountyLookupTable]
			,[dbo].[CountryLookupTable]
			,[dbo].[EnthnicOriginLookupTable]
		       
		 WHERE [UserTable].[ID] IN (SELECT IDsToInsert 
									from CTE 
									where IDsToInsert IS NOT NULL 
									and IDsToInsert != '-1'
									)
		 AND [dbo].[UserTable].[County] = [dbo].[CountyLookupTable].[ID]      
		 AND [dbo].[UserTable].[Country] = [dbo].[CountryLookupTable].[ID]      
		 AND [dbo].[EnthnicOriginLookupTable].[ID] = [UserTable].[EthnicOriginID]
		 
		       
     
    END      