USE [PPD_AAT_SecureAssess]
GO
/****** Object:  StoredProcedure [dbo].[sa_ASPIREINTEGRATION_GetDetailedResult_sp]    Script Date: 01/05/2016 14:47:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--ALTER PROCEDURE [dbo].[sa_ASPIREINTEGRATION_GetDetailedResult_sp]
--	@examSessionId INT
--AS
BEGIN
    --Returns the detailed test result information that includes item's sections data
    
    SET NOCOUNT ON;
    SET LANGUAGE british;
    
    SELECT Shreded.examName           AS 'ExamName'
          ,Shreded.examRef            AS 'ExamReference'
          ,Shreded.examVersionName    AS 'ExamVersionName'
          ,Shreded.examVersionRef     AS 'ExamVersionReference'
          ,Shreded.centreCode         AS 'CentreReference'	 
          ,Shreded.centreId	          AS 'CentreId'	 
          ,Shreded.qualificationRef   AS 'SubjectReference'
          ,Shreded.KeyCode            AS 'Keycode'
          ,Shreded.grade              AS 'Grade'
          --,WEST.ExamStateChangeAuditXml.value('(/exam/stateChange[newStateID=6]/changeDate)[1]' ,'datetime') AS 'StartedDate'
          ,Shreded.started			  AS 'StartedDate'
          ,Shreded.submittedDate      AS 'SubmittedDate'
          ,Shreded.foreName           AS 'CandidateForename'
          ,Users.Middlename           AS 'CandidateMiddleName'
          ,Shreded.surName            AS 'CandidateSurname'
          ,Shreded.gender             AS 'CandidateGender'
          ,Shreded.candidateRef       AS 'CandidateReference' 
          ,Users.UserId		          AS 'CandidateId'
          ,Shreded.resultData.value('(/exam/@passMark)[1]' ,'decimal(6,3)') AS 'PassMark'
          ,Shreded.resultData.value('(/exam/@totalMark)[1]' ,'decimal(6,3)') AS 'AvailableMark'
          ,Shreded.userMark           AS 'UserMark'
          ,Shreded.qualificationName  AS 'QualificationName'
          ,Shreded.warehouseTime      AS 'WarehouseTime'
          --,CONVERT(
          --     NVARCHAR(MAX)
          --    ,WEST.StructureXml.query('data(//automaticVerification)')
          -- )                          AS 'ResultReleased'
           ,Shreded.automaticVerification  AS 'ResultReleased'
          --,WEST.ExamStateChangeAuditXml.value(
          --     '(/exam/stateChange[newStateID=12]/information/stateChangeInformation/type)[1]'
          --    ,'nvarchar(max)'
          -- )                          AS 'ResultSampled'
          ,CASE WHEN Shreded.ResultSampled = '1' 
				THEN 'release' 
				ELSE '0' 
			END AS 'ResultSampled'
          --,CAST(
          --     (
          --         SELECT WEST.resultDataFull.query('//section') FOR XML 
          --                PATH('sectionData')
          --     ) AS XML
          -- )                          AS 'ItemData'
           ,itemDataFull  AS 'ItemData' /*Not quite correct*/
    FROM   dbo.WAREHOUSE_ExamSessionTable_Shreded Shreded
           INNER JOIN dbo.WAREHOUSE_ExamSessionTable WEST ON  Shreded.examSessionId = WEST.ID
           INNER JOIN dbo.WAREHOUSE_UserTable Users ON  Users.ID = WEST.WAREHOUSEUserID
    WHERE  Shreded.examSessionId = 1559085
           AND Shreded.WarehouseExamState = 1
END
