--USE master

--DROP TABLE PerformanceTest_INT;
--DROP TABLE PerformanceTest_TinyINT;

--TRUNCATE TABLE PerformanceTest_INT;
--TRUNCATE TABLE PerformanceTest_TinyINT;
--USE TempDBForTom

--CREATE TABLE PerformanceTest_INT 
--(
--		Id INT NOT NULL, 
--		InsertDate DATETIME NOT NULL, 
--		ABunchOfLetters NVARCHAR(100)
--)

--CREATE TABLE PerformanceTest_TinyINT
--(
--		Id TINYINT NOT NULL, 
--		InsertDate DATETIME NOT NULL, 
--		ABunchOfLetters NVARCHAR(100)
--)

--GO
--SET NOCOUNT ON
--GO


DECLARE @GenerateDate nvarchar(MAX) =  	'
		DECLARE @i INT = 1
		WHILE @i <= 250
		BEGIN
			INSERT PerformanceTest_INT (ID, InsertDate, ABunchOfLetters)
			SELECT @i, GETDATE(), REPLICATE(N''ABCD'', 25)
			SET @i+=1
		END
		
	
		DECLARE @j INT = 1
		WHILE @j <= 250
		BEGIN
			INSERT PerformanceTest_TinyINT (ID, InsertDate, ABunchOfLetters)
			SELECT @j, GETDATE(), REPLICATE(N''ABCD'', 25)
			SET @j+=1
		END
		'

DECLARE @NumberOfIterations smallint = 1;
WHILE @NumberOfIterations <= 400
BEGIN

EXEC(@GenerateDate)


SET @NumberOfIterations +=1
END
GO



SELECT * FROM  PerformanceTest_INT --where ID= 201
SELECT * FROM  PerformanceTest_TinyINT-- where ID= 201