USE [PRV_AAT_SecureMarker]

set statistics io on

--ALTER PROCEDURE [dbo].[sm_QUOTAMANAGEMENTSERVICE_getExaminersForExam_sp]
-- Add the parameters for the stored procedure here
DECLARE
	 @examId INT = 8
	,@getAllExaminers bit = 1
--AS
BEGIN
   
    SET NOCOUNT ON;
    

     SELECT U.ID															 AS 'ID'
           ,U.Forename														 AS 'Forename'
           ,U.Surname														 AS 'Surname'
           ,dbo.sm_getQuotaForUserForExam_fn(@examId,U.ID)					 AS 'AssignedQuota'
           ,ISNULL(QMI.NumberMarked ,0)										 AS 'NumberMarked'
           ,CAST(
                CASE 
                    WHEN ISNULL(QMI.MarkerSuspended ,0)=0 THEN 0
                    ELSE 1
                END
                AS BIT
            )																 AS 'MarkerSuspended'
           ,CAST(
                CASE 
                    WHEN ISNULL(QMI.MarkerCloseToBeingSuspended ,0)=0 THEN 0
                    ELSE 1
                END
                AS BIT
            )																 AS 'MarkerCloseToBeingSuspended'
           ,CAST(CASE WHEN ISNULL(QMI.Parked ,0)=0 THEN 0 ELSE 1 END AS BIT) AS 'Parked'
           ,ISNULL(QMI.NumberSuspended ,0)									 AS 'NumberSuspended'
           ,CAST(
                CASE 
                    WHEN ISNULL(HRM.HasRealMarkedResponses ,0)=0 THEN 0
                    ELSE 1
                END
                AS BIT
            )																 AS 'HasRealMarkedResponses'
    FROM   Users U

		  LEFT JOIN  (		
				
				SELECT  QM.UserId							AS 'UserId',
						ISNULL(SUM(QM.NumberMarked), 0)		AS 'NumberMarked',
						(CASE
							WHEN 
								ISNULL(SUM(CAST(QM.MarkingSuspended AS INT)), 0) > 0
							THEN 1
							ELSE 0
						END)								AS 'MarkerSuspended',
						(CASE
							WHEN 
								ISNULL(SUM(CAST(QM.Within1CIofSuspension AS INT)), 0) > 0
							THEN 1
							ELSE 0
						END)								AS 'MarkerCloseToBeingSuspended',
						(CASE
							WHEN 
								ISNULL(SUM(QM.NumItemsParked), 0) > 0
							THEN 1
							ELSE 0
						END)								AS 'Parked',
						ISNULL(SUM(QM.NumberSuspended), 0)	AS 'NumberSuspended'
		
				FROM    QuotaManagement QM
						INNER JOIN GroupDefinitions GD ON QM.GroupId = GD.ID
				WHERE   GD.ExamID = @examId  
						AND
						EXISTS 
						(
							SELECT	* 
							FROM	GroupDefinitionStructureCrossRef GDSCR
									INNER JOIN ExamVersionStructures EVS ON EVS.ID = GDSCR.ExamVersionStructureID			
							WHERE	GDSCR.Status = 0 
									AND GDSCR.GroupDefinitionID = GD.ID 
									AND EVS.StatusID = 0
						)  
				GROUP BY QM.UserId    
		  
		  ) QMI ON  QMI.UserId = U.ID


		   LEFT JOIN (
		   
			    SELECT U.ID  AS 'UserId'
				  ,CAST(
					   CASE 
							WHEN SUM(
									 CASE 
										  WHEN AGM.IsConfirmedMark=1
											   AND UGR.CI=0
										  THEN 1 ELSE 0 
									 END
								 )>0 THEN 1
							ELSE 0
					   END AS BIT
				   )     AS 'hasRealMarkedResponses'
			FROM   Users U
				   INNER JOIN dbo.AssignedGroupMarks AGM ON  U.ID = AGM.UserId
				   INNER JOIN dbo.UniqueGroupResponses UGR ON  AGM.UniqueGroupResponseId = UGR.ID
				   INNER JOIN dbo.GroupDefinitions GD ON  GD.ID = UGR.GroupDefinitionID
			WHERE  GD.ExamID = @examId
				   AND EXISTS (
						   SELECT *
						   FROM   dbo.GroupDefinitionStructureCrossRef GDSCR
								  INNER JOIN dbo.ExamVersionStructures EVS ON  EVS.ID = GDSCR.ExamVersionStructureID
						   WHERE  GDSCR.Status = 0
								  AND GDSCR.GroupDefinitionID = GD.ID
								  AND EVS.StatusID = 0
               )
				GROUP BY
					   U.ID	   
		   )  HRM ON HRM.UserId = U.ID
    WHERE   U.ID IN (SELECT UserID
                    FROM   dbo.AssignedUserRoles AR
                           INNER JOIN dbo.RolePermissions RP ON  RP.RoleID = AR.RoleID
                    WHERE  RP.PermissionID IN (12 ,13 ,14)
                           AND (AR.ExamID=@examId OR AR.ExamID=0))
           AND U.Retired = 0
           AND (U.InternalUser=0 OR U.Username='superuser')
	       AND (ISNULL(QMI.NumberMarked ,0) > 0 AND @getAllExaminers = 0 OR @getAllExaminers = 1)
		   
END






		  

           --LEFT JOIN sm_getQuotaManagementInfoForUser_fn(@examId) QMI ON  QMI.UserId = U.ID
           --LEFT JOIN sm_hasUserRealMarkedResponsesForExam_fn(@examId) HRM ON HRM.UserId = U.ID