USE master

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

;With CTE as (
SELECT  S.Title [Subject]
		,IT.ProjectID
       , i.id [ItemID]
       , i.Name [ItemName]
       , I.Version [ContentAuthor Version]
       , IT.Version [TestCreation Version]
	   
FROM [Cache_ContentAuthor].[dbo].[Subjects] S
INNER JOIN [Cache_ContentAuthor].[dbo].[Items]  I
ON I.SubjectId = S.Id
INNER JOIN (
       SELECT ProjectID
              , ItemRef
              , MAX(Version) [Version]
       FROM Cache_ItemBank.[dbo].[ItemTable] 
       group by ProjectID
              , ItemRef
       ) IT
on IT.ProjectID = S.ProjectId
       and IT.ItemRef = I.id
where I.[Status] = 3               /*  Live Items in ContentAuthor  */                       
      and I.Version > IT.Version
)


SELECT SUBSTRING(C.ItemID, 0, CHARINDEX('P',C.ItemID)) [ProjectID]
		, SUBSTRING(C.ItemID, CHARINDEX('P',C.ItemID)+1, LEN(C.ItemID) - CHARINDEX('P',C.ItemID)) [ItemID]
		, C.ItemID [FullItemID]
	FROM (
		SELECT REPLACE(REPLACE(cast(a.b.query('.') as NVARCHAR(MAX)), '<XPath>@ID="',''), '"</XPath>','') ItemID
		FROM [Cache_ItemBank]..AssessmentTable AT
		
		CROSS APPLY AssessmentRules.nodes('PaperRules//XPath') a(b)

		where IsValid = 1   --Valid exams
			and AssessmentStatus  = 2  --Live exams

	) C
INNER JOIN CTE
on  CTE.[ProjectID] = [ProjectID]	
	and CTE.[ItemID] = SUBSTRING(C.ItemID, CHARINDEX('P',C.ItemID)+1, LEN(C.ItemID) - CHARINDEX('P',C.ItemID)) 

where ISNUMERIC(SUBSTRING(C.ItemID, CHARINDEX('P',C.ItemID)+1, LEN(C.ItemID) - CHARINDEX('P',C.ItemID))) = 1;
	