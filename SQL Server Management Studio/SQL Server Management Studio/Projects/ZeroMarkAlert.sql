DECLARE  @subject VARCHAR(30) = 'test',
	     @query NVARCHAR(MAX);

CREATE TABLE ##Temp ( 
	examCount tinyint 
);

SET @query = 

'SET QUOTED_IDENTIFIER ON;

DECLARE @@Counter tinyint;

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

SELECT A.ID
	 , A.KeyCode
	 , a.correctAnswerCount
     , a.warehouseTime
FROM
( 
SELECT
	WAREHOUSE_ExamSessionTable.ID
	,WAREHOUSE_ExamSessionTable.KeyCode
	--,resultDataFull
	,WAREHOUSE_ExamSessionTable.PreviousExamState
	,WAREHOUSE_ExamSessionTable.ExportToSecureMarker
	,WAREHOUSE_ExamSessionTable.warehouseTime
	,sum(resultDataFull.value(''count(assessmentDetails/assessment/section/item/p[@um != "0"])'', ''int'')) AS [correctAnswerCount]
FROM WAREHOUSE_ExamSessionTable with (NOLOCK)
WHERE ID IN (
		SELECT DISTINCT examSessionId
		FROM [dbo].[WAREHOUSE_ExamSessionTable_ShrededItems] AS WESTSI
		WHERE ExamSessionID IN (
				SELECT DISTINCT WEST.ID
				FROM WAREHOUSE_ExamSessionTable_ShrededItems WESTSI
				INNER JOIN WAREHOUSE_ExamSessionTable AS WEST ON WESTSI.examSessionId = WEST.ID
				INNER JOIN WAREHOUSE_UserTable AS WUT ON WUT.ID = WEST.WAREHOUSEUserID
				INNER JOIN WAREHOUSE_ExamSessionTable_Shreded AS WES ON WES.examSessionId = WEST.ID
				WHERE WESTSI.userMark = 0
					AND examPercentage = 0
					AND userAttempted = 0
					AND correctAnswerCount > 0
					AND WEST.warehouseTime > ''2015-09-01''
					AND WEST.PreviousExamState <> 10
					AND WES.examVersionRef NOT LIKE ''%Practice''
				)
		)	
      
	AND ID NOT IN (635389, 1909417, 1969914)

group by 	
		WAREHOUSE_ExamSessionTable.ID
		,WAREHOUSE_ExamSessionTable.KeyCode
		,WAREHOUSE_ExamSessionTable.PreviousExamState
		,WAREHOUSE_ExamSessionTable.ExportToSecureMarker
		,WAREHOUSE_ExamSessionTable.warehouseTime
) A

where a.correctAnswerCount > 0
group by A.ID
	, A.KeyCode
	, a.correctAnswerCount
    , a.warehouseTime
    
    
INSERT ##Temp
SELECT @@ROWCOUNT
       
 ';

EXECUTE sp_executesql @query;

SELECT @subject = CONVERT(nvarchar(3), (SELECT examCount from ##Temp)) + ' Exams are Zero-Marked';

EXECUTE	 msdb.dbo.sp_send_dbmail
		 @profile_name = N'Local SMTP Virtual Server',
		 @recipients = N'dave.jones@btl.com',
		 @subject = @subject,
		 @body = N'See attached for zero marked exams.',
		 @execute_query_database = N'PPD_AAT_SecureAssess',
		 @query = @query,
		 @attach_query_result_as_file = 1,
		 @query_attachment_filename = N'AAT Zero Mark Exams.csv',
		 @query_result_separator = N',',
		 @query_result_no_padding = 1;
		 
DROP TABLE ##Temp;