USE master  

if exists (
select 1
from sys.credentials
where name = 'https://btlr12prdhub.blob.core.windows.net/backupcontainer'
)
DROP CREDENTIAL [https://btlr12prdhub.blob.core.windows.net/backupcontainer]



--CREATE CREDENTIAL [https://btlr12prdhub.blob.core.windows.net/backupcontainer] -- this name must match the container path, start with https and must not contain a trailing forward slash.  
--   WITH IDENTITY='SHARED ACCESS SIGNATURE' -- this is a mandatory string and do not change it.   
--   , SECRET = 'SharedAccessSignature=sv=2017-04-17&ss=bfqt&srt=sco&sp=rwdl&st=2018-04-16T13%3A40%3A00Z&se=2018-04-17T13%3A40%3A00Z&sig=DMcTQ7BQ5zhC66GY7jAHb9s5uTYiwWRRzOfklz%2F6pwk%3D;BlobEndpoint=https://btlr12prdhub.blob.core.windows.net/;FileEndpoint=https://btlr12prdhub.file.core.windows.net/;QueueEndpoint=https://btlr12prdhub.queue.core.windows.net/;TableEndpoint=https://btlr12prdhub.table.core.windows.net/;' --�- this is the shared access signature token   
--GO  

CREATE CREDENTIAL [https://btlr12prdhub.blob.core.windows.net/backupcontainer] -- this name must match the container path, start with https and must not contain a trailing forward slash.  
   WITH IDENTITY='SHARED ACCESS SIGNATURE' -- this is a mandatory string and do not change it.   
   , SECRET = 'sv=2017-04-17&ss=bfqt&srt=sco&sp=rwdl&st=2018-04-16T13%3A40%3A00Z&se=2018-04-17T13%3A40%3A00Z&sig=DMcTQ7BQ5zhC66GY7jAHb9s5uTYiwWRRzOfklz%2F6pwk%3D' --�- this is the shared access signature token   
GO  


RESTORE HEADERONLY
FROM URL = N'https://btlr12prdhub.blob.core.windows.net/backupcontainer/Windesheim_SurpassManagement.2018.04.16.bak'

RESTORE filelistonly
FROM URL = N'https://btlr12prdhub.blob.core.windows.net/backupcontainer/Templateempty_ItemBank_2c0c79b8d008486a8796e3440f3ed3b2_20180415025559%2B01.bak'

sp_whoisactive

