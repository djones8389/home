use demo_surpassmanagement

select c.name	[centrename]
	, s.name [subject]
	, sc.*
from UserCentreSubjectRoles u
inner join centres c
on c.id = u.centreid
left join subjects s
on s.id = subjectid
left join [SubjectCentres] sc
on sc.centreid = c.id
	and sc.subjectid = s.id
where c.name in ('andywcentre', 'btltest1')
 

 select s.name [subject]
	, s.id
	, a.*
	, c.name [centre]
	, uc.*
from [dbo].[SubjectCentres] a
inner join subjects s
on s.id = a.subjectid
inner join centres c
on c.id = a.centreid
left join UserCentreSubjectRoles uc
on uc.subjectid = s.id
	and uc.centreid = c.id
where s.name = 'Arcadia subject'
 --or s.name = 'BTL Test'
	order by s.name

	--BTL QLTS Centre

SELECT c.name	
	, r.name [role]
	, a.*
FROM [SharedSubjectRoles] a
inner join centres c
on c.id = a.centreid
inner join subjects s
on s.id = a.subjectid
inner join roles r
on r.id = roleid
where s.name = 'Arcadia subject'
order by 1