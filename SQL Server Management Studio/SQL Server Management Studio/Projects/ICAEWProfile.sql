SELECT TextData
	, StartTime
	, EndTime
	, Reads
	, Writes
	, cpu
	, ObjectName 
	, RowCounts
	, databasename
	, SourceDatabaseID
	, SessionLoginName
	, HostName
FROM fn_trace_gettable('T:\ICAEWTrace\Savetrace_1.trc',default)  --Savetrace_1
where DatabaseName not like 'icaewmock%'
	and (StartTime > '2017-10-25 14:18:00' and EndTime < '2017-10-25 14:20:00')
		or
		(StartTime > '2017-10-25 14:48:00' and EndTime < '2017-10-25 14:50:00')
 --and DatabaseName <> 'ASPState'
--order by DATEDIFF(MILLISECOND, 	StartTime	, EndTime) desc;


SELECT databasename
	, count(databasename)
FROM fn_trace_gettable('T:\ICAEWTrace\Savetrace_1.trc',default)
group by databasename
order by databasename