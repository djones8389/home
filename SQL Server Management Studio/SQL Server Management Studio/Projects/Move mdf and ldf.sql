use master;

DECLARE @NewLogLocation nvarchar(20) = 'D:\Log'
DECLARE @NewDataLocation nvarchar(20) = 'D:\Data'

DECLARE @Tables TABLE(DBName nvarchar(100), MyFileName nvarchar(100), FileType nvarchar(10), OriginalLocation nvarchar(250))
INSERT @Tables  
SELECT D.Name, F.Name, type_desc, F.physical_name
FROM sys.master_files as F
inner join sys.databases as D
on D.database_id = F.database_id 
where D.name in ('SecureAssess')

	and D.Database_ID > 4



SELECT DBName, MyFileName, FileType, OriginalLocation
, 'ALTER DATABASE ' + QUOTENAME(DBName) + '; SET OFFLINE; ' +
  'ALTER DATABASE ' + QUOTENAME(DBName) + ' MODIFY FILE (NAME =''' 
	+
		CASE WHEN FileType = 'ROWS' 
		THEN MyFileName 
		ELSE MyFileName 
		END AS MyFileName
	
FROM @Tables  
/*
	
DECLARE @Tables TABLE(DBName nvarchar(100), MyFileName nvarchar(100), FileType nvarchar(10), OriginalLocation nvarchar(250))
INSERT @Tables  
SELECT D.Name, F.Name, type_desc, F.physical_name
FROM sys.master_files as F
inner join sys.databases as D
on D.database_id = F.database_id 
where D.name in ('SecureAssess')
	and D.Database_ID > 4
	
	
	
	
	



SELECT *, 'SELECT CASE WHEN FileType =' + '''Log''' + ' AND OriginalLocation like ''%.ldf''  THEN MyFileName
	 WHEN FileType =''ROWS'' AND OriginalLocation like ''%.mdf''  THEN MyFileName
		 END AS MyFileName 
		 '
		 
*/