use master

IF OBJECT_ID('tempdb..#ResultHolder') IS NOT NULL DROP TABLE #ResultHolder;

DECLARE @LogSize TABLE (
	DBName sysname
	, logSizeMB real
	, [LogSpace%] real
	,[Status] bit
)

INSERT @LogSize
exec ('dbcc sqlperf(logspace)')

CREATE TABLE #ResultHolder
(
	DBName sysname NULL
	, [Data File Size - KB] float NULL
	, [Log File Size - KB] float NULL
	, [Used_pages - KB] float NULL
	, [Total_pages - KB] float NULL
	, logSizeMB real NULL
	, [LogSpace%] real NULL
)

INSERT #ResultHolder (DBName, [Data File Size - KB], [Log File Size - KB],[Used_pages - KB], [Total_pages - KB])
exec sp_msforeachdb '

use [?];

if(db_ID() > 4)

BEGIN

	select Data.DBName
		, Data.[Data File Size - KB]
		, [Log].[Log File Size - KB]
		, [Used_pages - KB] 
		, [Total_pages - KB]
	FROM (
	select db_name() [DBName]
		, cast ((size * 8) as float) [Data File Size - KB]
	from sys.master_files
	where database_id = db_ID()
		and type_desc = ''ROWS''
	) Data
	INNER JOIN (

	select db_name() [DBName]
		, cast ((size * 8) as float) [Log File Size - KB]
	from sys.master_files
	where database_id = db_ID()
		and type_desc = ''Log''

	) [Log]
	on [Data].DBName = [Log].DBName

	INNER JOIN (

	SELECT db_name() [DBName]
		,CAST(((SUM(used_pages) * 8192) / 1048576.0) AS FLOAT) [Used_pages - KB] 
		,CAST(((SUM(total_pages) * 8192) / 1048576.0) AS FLOAT) [Total_pages - KB] 
	FROM sys.allocation_units
) [DataPages]
	ON [DataPages].DBName = [Data].DBName


END

'

SELECT A.DBName
	, a.[Data File Size - KB]
	, a.[Log File Size - KB]
	, a.[Total_pages - KB]*1024 [Total_pages - KB]
	, a.[Used_pages - KB]*1024 [Used_pages - KB]
	, ((b.[LogSpace%] / 100) * b.logSizeMB)*1024 [LogSpaceUsed - KB]
FROM #ResultHolder A
INNER JOIN @LogSize B
on A.DBName = B.DBName
	order by 1 ASC;


