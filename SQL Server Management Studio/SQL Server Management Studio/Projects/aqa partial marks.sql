SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED


SELECT distinct 
	  a.examSessionId
	  , KeyCode
	  , a.userMark * a.TotalMark [itemMark]
	  , qualificationName
  	  , examName
	  , qualificationRef
	  , warehousetime
FROM WAREHOUSE_ExamSessionTable_ShrededItems a 
inner join WAREHOUSE_ExamSessionTable_Shreded b 
on a.examSessionId = b.examSessionId

WHERE warehousetime > '2017-01-01 00:00:00.001'
		AND (a.userMark * a.TotalMark % 1 != 0 OR ISNUMERIC(qualificationRef) = 0)
		order by warehousetime