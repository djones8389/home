USE [PRV_BritishCouncil_SecureAssess]
GO

/****** Object:  Table [dbo].[WAREHOUSE_ExamSessionAvailableItemsTable]    Script Date: 25/04/2017 09:09:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[WAREHOUSE_ExamSessionAvailableItemsTable_new] (
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ExamSessionID] [int] NOT NULL,
	[ItemID] [nvarchar](50) NOT NULL,
	[ItemVersion] [int] NOT NULL,
	[ItemXML] [xml] NOT NULL
 ) ON [THIRD] TEXTIMAGE_ON [THIRD]
GO

SET IDENTITY_INSERT [WAREHOUSE_ExamSessionAvailableItemsTable_new] ON
GO

INSERT [WAREHOUSE_ExamSessionAvailableItemsTable_new] (ID, ExamSessionID, ItemID, ItemVersion, ItemXML)
SELECT ID, ExamSessionID, ItemID, ItemVersion, ItemXML
FROM [WAREHOUSE_ExamSessionAvailableItemsTable]

SET IDENTITY_INSERT [WAREHOUSE_ExamSessionAvailableItemsTable_new] OFF
GO

DROP TABLE [dbo].[WAREHOUSE_ExamSessionAvailableItemsTable];
GO

PRINT 'DROP TABLE [dbo].[WAREHOUSE_ExamSessionAvailableItemsTable]'

EXEC sp_rename 'WAREHOUSE_ExamSessionAvailableItemsTable_new','WAREHOUSE_ExamSessionAvailableItemsTable';
GO

/*Constraints*/

ALTER TABLE [dbo].[WAREHOUSE_ExamSessionAvailableItemsTable]
	ADD CONSTRAINT [PK_WAREHOUSE_ExamSessionAvailableItemsTable] PRIMARY KEY CLUSTERED (ID)
GO

PRINT 'ADD CONSTRAINT [PK_WAREHOUSE_ExamSessionAvailableItemsTable] PRIMARY KEY CLUSTERED (ID)'

ALTER TABLE [dbo].[WAREHOUSE_ExamSessionAvailableItemsTable]  WITH CHECK 
ADD  CONSTRAINT [FK_WAREHOUSE_ExamSessionAvailableItemsTable_WAREHOUSE_ExamSessionTable] FOREIGN KEY([ExamSessionID])
REFERENCES [dbo].[WAREHOUSE_ExamSessionTable] ([ID])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[WAREHOUSE_ExamSessionAvailableItemsTable] CHECK CONSTRAINT [FK_WAREHOUSE_ExamSessionAvailableItemsTable_WAREHOUSE_ExamSessionTable]
GO


/*Indexes*/

CREATE NONCLUSTERED INDEX [FK_ExamSessionID] ON [dbo].[WAREHOUSE_ExamSessionAvailableItemsTable]
(
	[ExamSessionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [THIRD]
GO

