set nocount on;

if OBJECT_ID('tempdb..##RawData') is not null drop table ##RawData;
if OBJECT_ID('tempdb..##Formatted') is not null drop table ##Formatted;

SELECT [ItemID]
	  , Itemversion
	  ,cast(item.node.query('data(.)[1]') as nvarchar(200)) [Q/A]
	  ,ROW_NUMBER() OVER (PARTITION BY ItemID, itemversion ORDER BY ItemID) R
INTO ##RawData
FROM [PRV_SkillsFirst_SecureAssess].[dbo].[CPAuditTable]
CROSS APPLY itemxml.nodes('P/S/C/I/TEXTFORMAT/P/FONT') item(node)
where itemid in ('334P345', '334P466')

create table ##Formatted (

	Itemid varchar(12)
	, Itemversion smallint
	, Question varchar(200)
	, Answer varchar(200)
)
INSERT ##Formatted(Itemid,Itemversion,Question)
select ItemID
	, ItemVersion
	, [Q/A]
from ##RawData
where R = 1;

select *
from ##Formatted

DECLARE @ACols nvarchar(max) = (
		SELECT SUBSTRING(
			(
			SELECT DISTINCT N', '+QUOTENAME([Q/A])
			FROM ##RawData 
			where R > 1
			FOR XML PATH('')
			), 2, 10000)
);


DECLARE @tSql nvarchar(max) = N'
SELECT *
FROM (
		SELECT [ItemID],
			Itemversion,
			[Question]
		FROM (
			SELECT [ItemID],
					Itemversion,
					[Q and A]
			FROM ##Formatted
		) AS [S]
		PIVOT (
			MAX([Q and A])
			FOR [Q and A] IN ('+@ACols+N')
		) AS [PT1]
) AS [Q] 
'

PRINT(@tSql);





select * from ##Formatted


SELECT *
FROM (
		SELECT Itemid, Itemversion,[Complete the title of the novel by J.M.Barrie, 'The Admirable…', what?]
		FROM (
			SELECT [ItemID],
					Itemversion,
					[Question],
					[Answer]
			FROM ##Formatted
		) AS [S]
		PIVOT (
			MAX([Answer])
			FOR [Question] IN ([Complete the title of the novel by J.M.Barrie, 'The Admirable…', what?], [Who was the first person to die in a powered aircraft accident?])
		) AS [PT1]
) AS [Q] 






/*








SELECT *
FROM (
		SELECT [ItemID],
			Itemversion, [Complete the title of the novel by J.M.Barrie, 'The Admirable…', what?], [Who was the first person to die in a powered aircraft accident?], [Crichton], [Douglas Bader], [Lieutenant Thomas Selfridge], [Lord Curzon], [Mr Jones], [Orville Wright], [Servant], [Years]
		FROM (
			SELECT [ItemID],
					Itemversion,
					[Q and A]
			FROM ##Data
		) AS [S]
		PIVOT (
			MAX([Q and A])
			FOR [Q and A] IN ( [Complete the title of the novel by J.M.Barrie, 'The Admirable…', what?], [Who was the first person to die in a powered aircraft accident?], [Crichton], [Douglas Bader], [Lieutenant Thomas Selfridge], [Lord Curzon], [Mr Jones], [Orville Wright], [Servant], [Years])
		) AS [PT1]
	) AS [CA]


*/







--CREATE TABLE ##RawData (

--	itemid varchar(12)
--	,itemversion smallint
--	,question varchar(200)
--	,answer varchar(200)
--)

--INSERT ##Data(itemid,itemversion,q/a,question)
