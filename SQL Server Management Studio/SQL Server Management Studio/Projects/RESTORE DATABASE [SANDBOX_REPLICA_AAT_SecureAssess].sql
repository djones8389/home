select 'kill ' + convert(varchar(5),spid)
	,db_name(dbid)
from sys.sysprocesses s
where dbid > 4
	--and  db_name(dbid) = 'SaxionDefect_ItemBank'
order by 2;

/*
DECLARE @DBName nvarchar(100) = 'SANDBOX_Saxion_SurpassDataWarehouse';
--DECLARE @Login nvarchar(100) = 'Davej';

DECLARE @DynamicSpidKiller nvarchar(MAX) = '';

select @DynamicSpidKiller +=CHAR(13) + 
	 'KILL '  + convert(nvarchar(10),spid)
from sys.databases D
INNER JOIN sys.sysprocesses S
on D.database_id = s.dbid
where Name = @DBName
	--or loginame = @Login
PRINT (@DynamicSpidKiller);
*/


use master
RESTORE DATABASE [SANDBOX_REPLICA_AAT_SecureAssess]
FROM DISK = 'T:\FTP\AAT\AAT.SA.2017.01.17.bak'
	,DISK = 'T:\FTP\AAT\AAT.SA.2017.01.17_1.bak'
	,DISK = 'T:\FTP\AAT\AAT.SA.2017.01.17_2.bak'
WITH FILE = 1
	,MEDIAPASSWORD = 's_!85WrenukEtrut+4f5a#-a&ac*aKUs'
	,NOUNLOAD
	,NOREWIND
	,REPLACE
	,STATS = 5
	,MOVE 'Data' TO N'E:\DATA\SANDBOX_REPLICA_AAT_SecureAssess.mdf'
	,MOVE 'Log' TO N'T:\SANDBOX_REPLICA_AAT_SecureAssess.ldf';
GO

ALTER DATABASE [SANDBOX_REPLICA_AAT_SecureAssess] SET RECOVERY SIMPLE;
GO
use [SANDBOX_REPLICA_AAT_SecureAssess] dbcc shrinkfile('log', 1024);
GO
ALTER DATABASE [SANDBOX_REPLICA_AAT_SecureAssess] MODIFY FILE (NAME = 'Log',Filename = 'L:\Logs\SANDBOX_REPLICA_AAT_SecureAssess.ldf',FILEGROWTH = 314572800KB ); 
GO
ALTER DATABASE [SANDBOX_REPLICA_AAT_SecureAssess] SET offline; 
--Move file manually..

--ALTER DATABASE [SANDBOX_REPLICA_AAT_SecureAssess] SET ONLINE;
--GOUSE [master]
