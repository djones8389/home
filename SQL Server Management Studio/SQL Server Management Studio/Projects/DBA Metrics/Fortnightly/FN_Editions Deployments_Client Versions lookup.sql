/*
	, case cast(d.deploymenttype as varchar(2))
		when cast(2 as varchar(2)) then ''Update''
		when cast(13 as varchar(2)) then ''Clone''
		when cast(9 as varchar(2)) then ''Apply Features''
		else cast(DeploymentType as varchar(2)) 
		end as DeploymentType
*/

SELECT TOP (1000) [ID]
      ,[Instance]
      ,[ClientName]
      ,[ApplicationName]
      ,[DeploymentType]
      ,[CreatedDate]
      ,[Error]
      ,[Version]
      ,[IntVersion]
      ,[StartDate]
      ,[EndDate]
  FROM [PSCollector].[dbo].[FN_Editions Deployments_Client Versions]