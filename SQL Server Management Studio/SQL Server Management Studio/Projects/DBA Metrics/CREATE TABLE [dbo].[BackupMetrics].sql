USE [PSCollector]
GO


ALTER TABLE [dbo].[Backup Metrics] SET (SYSTEM_VERSIONING = OFF)
GO
DROP TABLE [BackupMetricsHistory]
GO
DROP TABLE [Backup Metrics]
GO

CREATE TABLE [dbo].[Backup Metrics](
	[ID] uniqueidentifier PRIMARY KEY CLUSTERED,
	[Instance] [nvarchar](500) NULL,
	[DBName] [nvarchar](500) NULL,
	[Pages] [nvarchar](500) NULL,
	[compressed_backup_size] [nvarchar](500) NULL,
	[Duration] [bigint] NULL,
	[backup_start_date] [datetime] NULL,
	[backup_finish_date] [datetime] NULL,
	[Backup Type] [nvarchar](500) NULL,
	[is_compressed] [bit] NULL,
	StartDate [datetime2](2) GENERATED ALWAYS AS ROW START NOT NULL,
	EndDate [datetime2](2) GENERATED ALWAYS AS ROW END NOT NULL,
	PERIOD FOR SYSTEM_TIME (StartDate,EndDate)  
	)
WITH (
	SYSTEM_VERSIONING = ON ( HISTORY_TABLE = [dbo].[BackupMetricsHistory] )
)
GO
