USE [PSCollector]
GO


ALTER TABLE SQLErrorLog SET (SYSTEM_VERSIONING = OFF)
GO
DROP TABLE [SQLErrorLogHistory] 
GO
DROP TABLE [dbo].SQLErrorLog;
go

CREATE TABLE [dbo].[SQLErrorLog](
	[ID] uniqueIdentifier PRIMARY KEY CLUSTERED ,
	[Instance] [nvarchar](500),
	[LogDate] [datetime] NULL,
	[ProcessInfo] [nvarchar](max) NULL,
	[Text] [nvarchar](max) NULL,
	StartDate [datetime2](2) GENERATED ALWAYS AS ROW START NOT NULL,
	EndDate [datetime2](2) GENERATED ALWAYS AS ROW END NOT NULL,
	 PERIOD FOR SYSTEM_TIME (StartDate,EndDate)  
	)
WITH (
	SYSTEM_VERSIONING = ON ( HISTORY_TABLE = [dbo].[SQLErrorLogHistory] )
)

GO


