use [SecureAssess_Populated] 
--DBCC SHRINKFILE (N'SecureAssess' , 2421)

	select 	 (
					SELECT CONVERT(DECIMAL(18, 2), SUM(CAST(df.size AS FLOAT)) * 8 / 1024.0)
					FROM sys.database_files AS df
					WHERE df.type IN (0,2,4)
					) AS [DbSize]
				, CONVERT(DECIMAL(18, 2), SUM(a.total_pages) * 8 / 1024.0)  AS [SpaceUsed]
				,(
					SELECT CONVERT(DECIMAL(18, 2), SUM(CAST(df.size AS FLOAT)) * 8 / 1024.0)
					FROM sys.database_files AS df
					WHERE df.type IN (1,3)
					) AS [LogSize]
	FROM sys.partitions p
	INNER JOIN sys.allocation_units a ON p.partition_id = a.container_id;

/*
DbSize	SpaceUsed	LogSize
2421.50	2418.15		102.25
*/

/*Per Index*/

SELECT  
	[TableName]
	,[index_size-kb] = cast(LTRIM (STR ((CASE WHEN usedpages > pages THEN (usedpages - pages) ELSE 0 END) * 8, 15, 0)) as int)
	, name [index name]
	, 'ALTER INDEX ' + QUOTENAME(name) + ' ON ' + QUOTENAME(TABLENAME) + ' REBUILD'
FROM (

	SELECT 
		OBJECT_NAME(s.object_id) [TableName]
		, (used_page_count) [usedpages]
		,  (
			CASE
				WHEN (s.index_id < 2) THEN (in_row_data_page_count + lob_used_page_count + row_overflow_used_page_count)
				ELSE lob_used_page_count + row_overflow_used_page_count
			END
		) [pages]
		, i.name
	FROM sys.dm_db_partition_stats AS s
		INNER JOIN sys.indexes AS i
			ON s.[object_id] = i.[object_id]
			AND s.[index_id] = i.[index_id]
		INNER JOIN sys.tables t
		on t.object_id = s.object_id
		
		where OBJECT_NAME(s.object_id)  = 'WAREHOUSE_ExamSessionItemResponseTable'
) A
	order by [index_size-kb] DESC

--166232 KB  (162.3359375 MB)
ALTER INDEX [idx_WAREHOUSE_ExamSessionItemResponseTable_WAREHOUSEExamSessionID] ON [WAREHOUSE_ExamSessionItemResponseTable] REBUILD

/*
DbSizemb	SpaceUsed	LogSize
2597.50	2418.34	102.25
*/

--176mb