USE AAT_SecureAssess

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

IF OBJECT_ID('tempdb..#Counter') IS NOT null DROP TABLE #Counter;
IF OBJECT_ID('tempdb..#Report') IS NOT null DROP TABLE #Report;

CREATE TABLE #Counter (
	examsessionID INT
	, [Extension] NVARCHAR(100)
	, [Count] tinyint
);

CREATE TABLE #Report (
	examsessionID INT
	, [Student name] NVARCHAR(200)
	, [Student membership no.]  NVARCHAR(200)
	, [Centre name]  NVARCHAR(200)
	, [Centre reference]  NVARCHAR(200)
	, [Date of assessment] DATETIME
	, [Date of result] DATETIME
	, [FileName] NVARCHAR(200)
	, [Extension] NVARCHAR(100)
	, [Count] tinyint
);


DECLARE @QualName NVARCHAR(200) = 'Advanced Diploma Synoptic Assessment (AQ2016)';


INSERT #Counter
SELECT 
	est.id
	, REVERSE(SUBSTRING(REVERSE(documentName), 0, CHARINDEX('.',REVERSE(documentName)))) [Extension]
	, COUNT(REVERSE(SUBSTRING(REVERSE(documentName), 0, CHARINDEX('.',REVERSE(documentName))))) [Count]
FROM dbo.ScheduledExamsTable scet
INNER JOIN dbo.ExamSessionTable est ON est.ScheduledExamID = scet.ID
INNER JOIN dbo.ExamSessionDocumentTable ESDT ON ESDT.examSessionId = est.ID
INNER JOIN dbo.IB3QualificationLookup ib ON ib.ID = scet.qualificationId
WHERE qualificationName = @QualName
	GROUP BY est.id
		, REVERSE(SUBSTRING(REVERSE(documentName), 0, CHARINDEX('.',REVERSE(documentName))))
UNION
SELECT 
	WEST.examSessionId
	, REVERSE(SUBSTRING(REVERSE(documentName), 0, CHARINDEX('.',REVERSE(documentName)))) [Extension]
	, COUNT(REVERSE(SUBSTRING(REVERSE(documentName), 0, CHARINDEX('.',REVERSE(documentName))))) [Count]
FROM dbo.WAREHOUSE_ExamSessionTable_Shreded WESTS
INNER JOIN dbo.WAREHOUSE_ExamSessionDocumentTable WESDT ON WESDT.warehouseExamSessionID = WESTS.examSessionId
INNER JOIN dbo.WAREHOUSE_ExamSessionTable west ON west.id = wests.examSessionId
WHERE qualificationName = @QualName
	GROUP BY WEST.examSessionId
		, REVERSE(SUBSTRING(REVERSE(documentName), 0, CHARINDEX('.',REVERSE(documentName))))

INSERT #Report
SELECT est.id [ExamSessionID]
	 , UT.Forename + ' ' + UT.Surname [Student name]
	 , UT.CandidateRef [Student membership no.]
	 , CT.CentreName [Centre name]
	 , CT.CentreCode [Centre reference]
	 , State6.StateChangeDate [Date of assessment]
	 , State9.StateChangeDate [Date of result]
	, REPLACE(documentName, REVERSE(SUBSTRING(REVERSE(documentName), 1, CHARINDEX('.',REVERSE(documentName)))), '') [FileName]
	, REVERSE(SUBSTRING(REVERSE(documentName), 0, CHARINDEX('.',REVERSE(documentName)))) [Extension]
	, c.[Count]
  FROM ExamSessionTable as EST 
  Inner Join ScheduledExamsTable as SCET on SCET.ID = EST.ScheduledExamID   
  Inner Join UserTable as UT on UT.ID = EST.UserID
  Inner Join CentreTable as CT on CT.ID = SCET.CentreID
  INNER JOIN dbo.ExamSessionDocumentTable esdt ON esdt.examSessionId = EST.ID
  INNER JOIN #Counter c
  ON c.examSessionId = est.ID
		AND c.Extension = [Extension]
  LEFT JOIN (
	SELECT ExamSessionID
		, MIN(StateChangeDate) StateChangeDate
	FROM dbo.ExamStateChangeAuditTable
	WHERE NewState = 6
	GROUP BY ExamSessionID
	) State6 
	ON State6.ExamSessionID = EST.ID
  LEFT JOIN (
	SELECT ExamSessionID
		, MIN(StateChangeDate) StateChangeDate
	FROM dbo.ExamStateChangeAuditTable
	WHERE NewState = 9
	GROUP BY ExamSessionID
	) State9 
	ON State9.ExamSessionID = EST.ID

UNION

SELECT west.ExamSessionID [ExamSessionID]
	, WESTS.foreName  + ' ' + WESTS.surName [Student name]
	, WESTS.candidateRef [Student membership no.]
	, WESTS.centreName [Centre name]
	, WESTS.centreCode [Centre reference]
	, WESTS.started [Date of assessment]
	, WESTS.warehouseTime [Date of result]
	, REPLACE(documentName, REVERSE(SUBSTRING(REVERSE(documentName), 1, CHARINDEX('.',REVERSE(documentName)))), '') [FileName]
	, REVERSE(SUBSTRING(REVERSE(documentName), 0, CHARINDEX('.',REVERSE(documentName)))) [Extension]
	, c.[Count]
FROM dbo.WAREHOUSE_ExamSessionTable_Shreded WESTS
INNER JOIN dbo.WAREHOUSE_ExamSessionDocumentTable WESDT
ON WESDT.warehouseExamSessionID = WESTS.examSessionId
INNER JOIN dbo.WAREHOUSE_ExamSessionTable west ON west.id = wests.examSessionId
INNER JOIN #Counter c
ON c.examSessionId = west.examSessionId
	AND c.Extension = [Extension]
WHERE qualificationName = @QualName
	--AND WESTS.candidateRef = '20158556'


--SELECT * FROM #Counter WHERE examsessionID = 2624102
SELECT DISTINCT * FROM #Report ORDER BY examsessionID;