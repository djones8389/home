SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

SELECT b.SurpassExamRef [Component Name]
	, b.ScheduledExamRef [Keycode]
	, Code [CEFR]
	, round([Score], 0)
	,DefaultScaleValue [Component Max Scale Score]
FROM (
	SELECT a.*
		, ROW_NUMBER () OVER (partition by ScheduledExamRef, SurpassExamRef order by ScoreDiff) R
	FROM (
		select ScheduledExamRef
			, SurpassExamRef
			, spce.Score
			, eg.*
			, (Score-DefaultMinScore) [ScoreDiff]
			, DefaultScaleValue
		from ScheduledPackageCandidateExams as SPCE

			inner join ScheduledPackageCandidates as SPC
			on SPC.ScheduledPackageCandidateId =  SPCE.Candidate_ScheduledPackageCandidateId
			inner join ScheduledPackages as SP
			 on SP.ScheduledPackageId =  SPC.ScheduledPackage_ScheduledPackageId
			inner join [dbo].[PackageExams] pe
			on pe.[PackageExamId] = SPCE.[PackageExamId]
			inner join [dbo].[Packages] pack
			on pack.[PackageId] = pe.[PackageId]
			inner join PackageExamScoreBoundaries pes
			on pes.PackageExamId = pack.PackageId
				and pes.ExamGradeId = ExamTypeId
			 inner join ExamGradeDescriptions egd
			 on pes.examgradeid = egd.[ExamTypeId]
			 inner join [dbo].[ExamGrades] EG
			 on EG.Id = egd.ExamGradeId
		
		--where ScheduledExamRef In  ('NY9XYM01','5B48C501','YRDJLN01','ENCUVW01')
		where ScheduledExamRef In  ('C8JQDK01')
	) a
	where [ScoreDiff] > 0 
) b
where R = 1

select *
from sys.tables t
inner join sys.columns c
on c.object_id = t.object_id
where c.name = 'Defaultscalevalue'

			 /*no on egd.[ExamGradeId] = pes.[ExamGradeId]*/