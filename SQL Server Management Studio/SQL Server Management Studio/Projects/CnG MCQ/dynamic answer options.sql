declare @n table (
	n float primary key
)

INSERT @n
SELECT  DISTINCT N
FROM  ##Answers

DECLARE @OptionList nvarchar(150) = (

	SELECT 
		distinct
		substring((
			SELECT ', ' + QUOTENAME(cast([N] as varchar(10)))
			FROM @n 
			order by [N]
			FOR XML PATH('')
		), 2,1000)
	FROM @n 
)


select @OptionList
