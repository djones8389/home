USE master;

IF OBJECT_ID('tempdb..#Users') IS NOT NULL DROP TABLE #Users;

CREATE TABLE #Users (
	dbname sysname
	, username sysname
	, [sid] VARBINARY(max)
);

DECLARE @Command NVARCHAR(MAX) =  '

use [?];
if db_id() >  4
BEGIN
	SELECT ''?''
		,[Name]
		, sid
	FROM sysusers 
	WHERE issqlrole = 0
		AND uid > 4
		AND [Name] NOT IN (''LON\3168686-Admins'',''Powershell'')
END
'

INSERT #Users
EXEC sys.sp_MSforeachdb  @command1 =  @Command

SELECT sid
	, name
	, 'DROP LOGIN ' + QUOTENAME(name) + ';'
	, denylogin
	, hasaccess
	, *
FROM sys.syslogins
WHERE sid NOT IN (
	SELECT sid
	FROM #Users 
)
ORDER BY name;


/*

DROP LOGIN [AQAEditions_CP];
DROP LOGIN [cpuser_demo_editions];
DROP LOGIN [cpuser_demoEditions];
DROP LOGIN [cpuser_saxion];
DROP LOGIN [cpuser_saxion_editions];

DROP LOGIN [Editions_Shared_ContentProducer_Login];

DROP LOGIN [LON\977496-tomg];
DROP LOGIN [LON\977496-davidn];
DROP LOGIN [LON\977496-brendand];

DROP LOGIN [PerformanceTest1_SecureAssessUser];
DROP LOGIN [ReportingUser_Saxion];
DROP LOGIN [SANDBOX_EBS_SecureAssess_Login];
DROP LOGIN [Saxion_CPPreProd];
*/