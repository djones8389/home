use master;

IF OBJECT_ID('tempdb..#Durations') IS NOT NULL DROP TABLE #Durations;

CREATE TABLE #Durations (
	Client nvarchar(100)
	,Max_Live_Reason smallint
	,Max_Warehouse_Reason smallint
)

INSERT #Durations
exec sp_MSforeachdb '

use [?];

if (''?'' in (select 
				name from sys.databases 
				where name like ''%[_]SecureAssess'' and state_desc = ''ONLINE''
				)
				)

BEGIN

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

;with Live as (
SELECT MAX(LEN(REASON)) MaxLiveReason
FROM [dbo].[ExamSessionDurationAuditTable]
)
, WH as (

SELECT MAX(LEN(REASON)) MaxWHReason
FROM [dbo].[Warehouse_ExamSessionDurationAuditTable]

)


select db_NAME()
, ISNULL(MaxLiveReason, 0) MaxLiveReason
, ISNULL(MaxWHReason,0) MaxWHReason
from Live, WH
	

END

'

SELECT * FROM #Durations;