use Ocr_SurpassDataWarehouse

declare @ES table (ExamSessionKey int)

insert @ES
values(215572)

SELECT DQ.CPID	
	, QuestionTypeKey	
	, FQR.DerivedResponse
	, ISNULL(CASE QuestionTypeKey
				WHEN 11 THEN REPLACE(dbo.fn_CLR_StripHTML(FQR.DerivedResponse),N'#!#',' ')
				WHEN 521 THEN REPLACE(dbo.fn_CLR_StripHTML(FQR.DerivedResponse),N'#!#',' ')
				ELSE FQR.ShortDerivedResponse
				END, '') [item!10!response]	-- this isn't doing the job.  data is in DerivedResponse, but not in ShortDerivedResponse
	, FQR.*
FROM @ES ES
JOIN FactQuestionResponses FQR ON FQR.ExamSessionKey = ES.ExamSessionKey
JOIN DimQuestions DQ ON DQ.CPID = FQR.CPID AND DQ.CPVersion = FQR.CPVersion
--where dq.CPID in ('4208P5359','4208P5366','4208P5383','4208P5384','4208P5386','4208P5387','4208P5388')
where dq.cpid in ('4208P5383','4208P5386','4208P5388')

--11 = fine
--Gap Fill, Gap Fill = fail


--select  fqr.CPID
-- , DerivedResponse
--from FactQuestionResponses FQR
--JOIN DimQuestions DQ ON DQ.CPID = FQR.CPID AND DQ.CPVersion = FQR.CPVersion
--where questiontypekey = 521