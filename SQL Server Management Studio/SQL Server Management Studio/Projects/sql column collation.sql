select a.[Alter Column] + ' COLLATE ' + (SELECT convert(sysname, serverproperty(N'collation'))) 
              + CASE WHEN is_nullable = 1 
                     THEN ' NULL' else ' NOT NULL;'              
                      END AS [Statement]
from (
select t.name [TableName]
       , c.name [ColumnName]
       , collation_name
       , is_nullable
       , max_length
       , 'ALTER TABLE ' + SCHEMA_NAME(T.schema_id) + '.' + QUOTENAME(T.NAME) +
              '  ALTER COLUMN ' + QUOTENAME(C.NAME) + ' ' + type_name(C.system_type_id) 
              + 
                     CASE
                       WHEN type_name(C.system_type_id) = 'varchar'  and max_length > -1  THEN + '(' + cast(max_length as varchar(100)) + ')' 
                       WHEN type_name(C.system_type_id) = 'varchar'  and max_length = -1  THEN + '(MAX)'                                  
                     ELSE
                     CASE
                       WHEN type_name(C.system_type_id) = 'nvarchar' and max_length > -1 THEN '(' +  cast(max_length/2 as varchar(100)) + ')' 
                       WHEN type_name(C.system_type_id) = 'nvarchar' and max_length = -1 THEN '(MAX)' 
                           ELSE + '(' + cast(max_length as varchar(100)) + ')'
                           END

                        end as [Alter Column]
from sys.columns c
inner join sys.tables t
on t.object_id = c.object_id

where collation_name != (SELECT convert(sysname, serverproperty(N'collation')))
       and type_name(C.system_type_id) <> 'ntext'
)a


order by 1;


