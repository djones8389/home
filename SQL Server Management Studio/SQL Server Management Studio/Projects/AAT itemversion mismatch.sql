USE PPD_AAT_SecureMarker

;with SM AS (
select KeyCode, ItemID, ItemVersion, DATALENGTH(ItemResponseData) DL, cast(ItemResponseData as varbinary(MAX)) VB, ItemResponseData
from PPD_AAT_SecureAssess..WAREHOUSE_ExamSessionTable west
inner join PPD_AAT_SecureAssess..WAREHOUSE_ExamSessionItemResponseTable wesirt
on west.id = wesirt.warehouseexamsessionid
where WarehouseExamState != 1
	and Keycode = 'RRPLWJA6'
), SA AS (
select Keycode, I.ExternalItemID, I.Version, DATALENGTH(ur.responseData) DL, cast(ur.responseData as varbinary(MAX)) VB, ur.responseData
from CandidateExamVersions cev
inner join CandidateResponses cr
on cr.CandidateExamVersionID = cev.ID
inner join UniqueResponses ur
on ur.id = cr.UniqueResponseID
inner join items i
on i.id = ur.itemId
where ExamSessionState != 7
	and Keycode = 'RRPLWJA6'
)

select *
from SM 
inner join SA
on SA.Keycode = SM.KeyCode
 and sm.ItemID = Sa.ExternalItemID
 where sm.ItemVersion <> sa.Version
	and sm.vb <> sa.vb