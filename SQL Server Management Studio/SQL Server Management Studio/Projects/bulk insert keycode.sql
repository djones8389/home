if OBJECT_ID('tempdb..#djkeycode') is not null drop table #djkeycode;
if OBJECT_ID('tempdb..#cfkeycode') is not null drop table #cfkeycode;

create table #djkeycode (	
	 keycode varchar(10)
);

create table #cfkeycode (	
	 keycode varchar(10)
);


bulk insert #djkeycode
from 'C:\Users\DaveJ\Desktop\09022017\djkeycodes.csv'
with (fieldterminator=',',rowterminator='\n')

bulk insert #cfkeycode
from 'C:\Users\DaveJ\Desktop\09022017\cfkeycodes.csv'
with (fieldterminator=',',rowterminator='\n')


declare @matching table (keycode varchar(10))
insert @matching
select * from #djkeycode
intersect
select * from #cfkeycode

delete from #djkeycode where keycode in  (select keycode from @matching)
delete from #cfkeycode where keycode in  (select keycode from @matching)


select * from #djkeycode order by Keycode;
select * from #cfkeycode order by Keycode;	

	--where keycode = 'R8MXJKA6'  --where id =2264345
