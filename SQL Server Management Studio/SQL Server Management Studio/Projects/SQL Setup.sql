/*TempDB*/

DECLARE @Cores tinyint = (SELECT cpu_count FROM [sys].[dm_os_sys_info]);
DECLARE @TempDBSizeMB smallint = 1000;
DECLARE @TempDBPath TABLE (physical_name nvarchar(MAX)); 
INSERT @TempDBPath
SELECT
	 SUBSTRING(physical_name, 0, (LEN(physical_name) - LEN(SUBSTRING(REVERSE(physical_name), 0, CHARINDEX('\',REVERSE(physical_name))))))
from sys.master_files
where DB_NAME(database_id) = 'tempdb'
	and type_desc = 'Rows'
	
DECLARE @CurrentNoOfTempDBs tinyint = (SELECT COUNT(*) FROM @TempDBPath)

DECLARE @Dynamic nvarchar(MAX)= '';

WHILE(@CurrentNoOfTempDBs<@Cores)
BEGIN
	
	SELECT @Dynamic += CHAR(13) +
		'ALTER DATABASE tempdb ADD FILE (NAME = tempdev'+cast(@CurrentNoOfTempDBs as varchar(2))+', FILENAME = '''+(Select top 1 physical_name from @TempDBPath)+'\tempdev'+cast(@CurrentNoOfTempDBs as varchar(2))+'.ndf'', SIZE ='+cast(@TempDBSizeMB as varchar(5))+'MB);'
	
	SET @CurrentNoOfTempDBs = @CurrentNoOfTempDBs + 1
	
END

PRINT(@Dynamic)


/*Data & Log Filegrowth*/

	DECLARE @AutoGrow TABLE (
		Name nvarChar(100)
		, fileID tinyint
		, filename nvarchar(1000)
		, filegroup nvarchar(100)
		, size nvarchar(100)
		, maxSize nvarchar(100)
		, growth  nvarchar(20)
		, usage nvarchar(30)
	)

	INSERT @AutoGrow
	EXEC sp_MSforeachdb '
	use [?];

	IF (db_ID() > 4)
	BEGIN
		EXEC sp_helpfile  
	END
	';
	
	DECLARE @NewMDFFileSize nvarchar(10) = '128MB';
	DECLARE @NewLDFFileSize nvarchar(10) = '64MB';

	SELECT 
		case f.type_desc
			when 'ROWS' then 'ALTER DATABASE [' + D.Name + '] MODIFY FILE ( NAME =''' + F.Name + ''', FILEGROWTH =' + @NewMDFFileSize + ' )'
			else 'ALTER DATABASE [' + D.Name + '] MODIFY FILE ( NAME =''' + F.Name + ''', FILEGROWTH =' + @NewLDFFileSize + ' )'
			END AS [AlterFile]
	FROM @AutoGrow  A
	INNER JOIN sys.master_files as F
	on F.physical_name COLLATE SQL_Latin1_General_CP1_CI_AS = A.filename		
	INNER JOIN sys.databases as D
	on D.database_id  = F.database_id
		
/*Simple Recovery*/

	select 'ALTER DATABASE ' + QUOTENAME(name) + ' SET RECOVERY SIMPLE WITH NO_WAIT;'
	from sys.databases
	where recovery_model_desc = 'FULL'

/*Max memory*/

	exec sp_configure 'show advanced options',1
	reconfigure

	select name, value, value_in_use 
	from sys.configurations where name = 'max server memory (MB)';
	
	--exec sp_configure 'max server memory (MB)', 22528  
	--reconfigure
