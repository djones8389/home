DECLARE @FindChar VARCHAR(1) = '_'

select 
	case when type in ('PC','P') then 'DROP PROCEDURE ' + QUOTENAME(NAME) +';'
		when type = 'F' then 'ALTER TABLE ' +quotename(object_name(parent_object_id))+ ' DROP CONSTRAINT ' + + QUOTENAME(NAME) + ';'
		 when type = 'U' then 'DROP TABLE ' + QUOTENAME(NAME) + ';'
		 when type in ('FN','FS','IF','TF') then 'DROP FUNCTION ' + QUOTENAME(NAME) + ';'
		 when type = 'v' then 'DROP VIEW ' + quotename(substring(name,charindex('TT_',Name), charindex('_',Name))) + ';'
		 when type = 'TT' then'DROP TYPE ' + quotename(SUBSTRING(name, 4, LEN(name) - CHARINDEX(@FindChar,REVERSE(name))-3))  + ';'
	else name
	end as 'DROP STATEMENT'
	,[type]
from sys.objects 
where create_date > '2010-04-03'
order by  sys.objects.[type]


/*
select *--distinct type
from sys.objects 
where name = 'itemResponseXmlXsd'

select *
from sys.objects 
where [NAme] = 'sa_ASSESSMENTSERVICE_SyncStructureXmlWithLiveTables_sp'

[type] = 'FN'
	and type_desc like '%function%'

	*/
