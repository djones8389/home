use msdb

DECLARE @To NVARCHAR(100),
		@body NVARCHAR(MAX), 
		@profile_name SYSNAME,
		@query nvarchar(MAX)

/* Get email address */
	SELECT @To = email_address FROM msdb.dbo.sysoperators WHERE name = 'Dave Jones';

	SET @body = ERROR_MESSAGE();

	SELECT TOP(1) @profile_name = name FROM msdb.dbo.sysmail_profile


SELECT @query = '

if OBJECT_ID(''tempdb..#IndexResults'') IS NOT NULL DROP TABLE #IndexResults;

CREATE TABLE #IndexResults (
	[DB_Name] sysname
	, TableName nvarchar(200)
	, IndexName nvarchar(200)
	, Avg_fragmentation_in_percent decimal(15,13)
	, page_count bigint
	, statement nvarchar(50)
)

INSERT #IndexResults
EXECUTE sp_MSforeachdb N''
		SET QUOTED_IDENTIFIER ON;
		USE [?];
		IF DB_ID() > 4
		BEGIN
			SELECT	db_NAME() [DB]
					,OBJECT_NAME(ix.object_id)
					,QUOTENAME(ix.name) [Index]
					, avg_fragmentation_in_percent 
					, ix_phy.page_count
					,	CASE
						WHEN (avg_fragmentation_in_percent BETWEEN 5 AND 30) and page_count > 100 THEN ''''REORGANIZE''''
						WHEN (avg_fragmentation_in_percent > 30) THEN ''''REBUILD''''
						END [Statement]
				FROM	 sys.dm_db_index_physical_stats(DB_ID(''''?''''), NULL, NULL, NULL, NULL) ix_phy
				INNER JOIN sys.indexes ix
						ON ix_phy.object_id = ix.object_id
					AND ix_phy.index_id = ix.index_id
				WHERE ix.index_id <> 0
					and (
							(avg_fragmentation_in_percent BETWEEN 5 AND 30) and page_count > 100 
						or 
							(avg_fragmentation_in_percent > 30) and page_count > 10
						)
			
		END'';


SELECT *
FROM #IndexResults
ORDER BY 1,2,3,4,5,6'



EXECUTE	 msdb.dbo.sp_send_dbmail
		@profile_name = @profile_name,
		@recipients = @To,
		@subject = N'Index - NEW',
		@body = @body,
		@execute_query_database = N'master',
		@query = @query,
		@attach_query_result_as_file = 1,
		@query_attachment_filename = N'Index NEW.csv',
		@query_result_separator = N',',
		@query_result_no_padding = 1,
		@body_format = 'HTML';