SET NOCOUNT ON;

DECLARE @Kb float = 1024.0;
DECLARE @PageSize float;
DECLARE @SQL varchar(2000);
DECLARE @DynamicSQL nvarchar(MAX) = '';

SELECT @PageSize=v.low/@Kb FROM master..spt_values v WHERE v.number=1 AND v.type='E';

IF OBJECT_ID('tempdb.dbo.#FileSize') IS NOT NULL DROP TABLE #FileSize;
IF OBJECT_ID('tempdb.dbo.#FileStats') IS NOT NULL  DROP TABLE #FileStats;

CREATE TABLE #FileSize (
      DatabaseName sysname,
      FileName sysname,
      FileSize int,
      FileGroupName sysname,
      LogicalName sysname
);

CREATE TABLE #FileStats (
      FileID int,
      FileGroup int,
      TotalExtents int,
      UsedExtents int,
      LogicalName sysname,
      FileName nchar(520)
);

DECLARE @DatabaseName sysname

DECLARE cur_Databases CURSOR FAST_FORWARD FOR
SELECT DatabaseName = [name] 
FROM sys.databases 
where state_desc = 'ONLINE' 
ORDER BY DatabaseName
	
	OPEN cur_Databases
	FETCH NEXT FROM cur_Databases INTO @DatabaseName
	WHILE @@FETCH_STATUS = 0
	  BEGIN
		  SET @SQL = '
							USE [' + @DatabaseName + '];
							DBCC showfilestats;
							INSERT #FileSize (DatabaseName, FileName, FileSize, FileGroupName, LogicalName)
							SELECT ''' +@DatabaseName + ''', filename, size, ISNULL(FILEGROUP_NAME(groupid),''LOG''), [name]
							FROM dbo.sysfiles sf;
							'

		  INSERT #FileStats EXECUTE (@SQL)
		  FETCH NEXT FROM cur_Databases INTO @DatabaseName
	  END

	CLOSE cur_Databases;
	DEALLOCATE cur_Databases;

SELECT @DynamicSQL += CHAR(13) +
   'USE [' + fsi.DatabaseName + '];  DBCC SHRINKFILE (''' + RTRIM(fsi.LogicalName) + + '''' + ',' + CONVERT(Nvarchar(MAX), CONVERT(DECIMAL(7,0), ROUND(CAST((fs.UsedExtents*@PageSize*8.0/@Kb) as decimal(15))*1.05, 0))) + ');'
FROM #FileSize fsi
LEFT JOIN #FileStats fs
      ON fs.FileName = fsi.FileName
where RTRIM(fsi.FileName) like '%.mdf%'
	AND CONVERT(Nvarchar(MAX), CONVERT(DECIMAL(7,0), ROUND(CAST((fs.UsedExtents*@PageSize*8.0/@Kb) as decimal(15))*1.05, 0))) <  CAST(fsi.FileSize*@PageSize/@Kb as decimal(15,2))
	and DatabaseName not in ('tempdb','model','msdb', 'master')
	AND CAST((fsi.FileSize - UsedExtents*8.0)*@PageSize/@Kb as decimal(15,2)) > 500  --Only do >500MB to make it worthwhile

IF OBJECT_ID('tempdb.dbo.#FileSize') IS NOT NULL DROP TABLE #FileSize;
IF OBJECT_ID('tempdb.dbo.#FileStats') IS NOT NULL  DROP TABLE #FileStats;

EXEC(@DynamicSQL);