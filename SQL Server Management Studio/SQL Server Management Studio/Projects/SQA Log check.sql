/*
use master

DECLARE @Statement nvarchar(MAX) = 'DBCC SQLPERF(LogSpace)'
INSERT #LogGrowth([DatabaseName], [logSpace], [logSpaceUsed], [status])
exec sp_executesql @Statement;

UPDATE #LogGrowth
set timeStamp = getDATE()
where timeStamp IS NULL;

select *
from #LogGrowth
where DatabaseName = 'PRV_SQA_SecureAssess'


DatabaseName			logSpace	logSpaceUsed	status	timeStamp
PRV_SQA_SecureAssess	125.9922	29.93117		0		2016-08-26 08:56:46.717


USE PRV_SQA_SecureAssess

SELECT
----B.name AS TableName
----, C.name AS IndexName
----, C.fill_factor AS IndexFillFactor
----, D.rows AS RowsCount
----, A.avg_fragmentation_in_percent
----, A.page_count
distinct 'ALTER INDEX ' + C.name +  ' ON ' + 'dbo' + '.' + B.name + ' REORGANIZE WITH ( LOB_COMPACTION = ON );'
FROM sys.dm_db_index_physical_stats(DB_ID(),NULL,NULL,NULL,NULL) A
INNER JOIN sys.objects B
ON A.object_id = B.object_id
INNER JOIN sys.indexes C
ON B.object_id = C.object_id AND A.index_id = C.index_id
INNER JOIN sys.partitions D
ON B.object_id = D.object_id AND A.index_id = D.index_id
WHERE C.index_id > 0
	--and A.avg_fragmentation_in_percent > 30
*/

DECLARE @Statement nvarchar(MAX) = 'DBCC SQLPERF(LogSpace)'
INSERT #LogGrowth([DatabaseName], [logSpace], [logSpaceUsed], [status])
exec sp_executesql @Statement;

UPDATE #LogGrowth
set timeStamp = getDATE()
where timeStamp IS NULL;

DBCC CHECKDB(N'PRV_SQA_SecureAssess')  WITH NO_INFOMSGS  

ALTER INDEX PK_EnthnicOriginLookupTable ON dbo.EnthnicOriginLookupTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX PK_InteractionAudit ON dbo.InteractionAudit REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX IX_WarehouseTimeExamState ON dbo.WAREHOUSE_ExamSessionTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX PK_WAREHOUSE_ExamSessionTable_ShrededItems ON dbo.WAREHOUSE_ExamSessionTable_ShrededItems REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX PK_UserExamHistoryCompletionDateTrackerTable ON dbo.UserExamHistoryCompletionDateTrackerTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX IX_WAREHOUSE_ExamSessionTable_WarehouseExamState ON dbo.WAREHOUSE_ExamSessionTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX IX_ScheduledExamsTable_CentreID_CI ON dbo.ScheduledExamsTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX IX_WAREHOUSE_ExamSessionTable_submissionExported ON dbo.WAREHOUSE_ExamSessionTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX IX_ExamSessionID ON dbo.WAREHOUSE_ExamSessionTable_ShrededItems REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX PK_ExamSessionTable_ShrededItems ON dbo.ExamSessionTable_ShrededItems REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX IX_warehouseExamSessionID_ItemID ON dbo.WAREHOUSE_ExamSessionDocumentTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX examSessionId_NCI ON dbo.WAREHOUSE_ExamSessionItemResponseTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX IX_ExamVersion ON dbo.WAREHOUSE_ExamSessionTable_Shreded REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX IX_ExamIDExamVersionID ON dbo.WAREHOUSE_ScheduledExamsTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX PK_ExamSessionCandidateInteractionLogsTable ON dbo.ExamSessionCandidateInteractionLogsTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX PK_Warehouse_ExamStateAuditTable ON dbo.WAREHOUSE_ExamStateAuditTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX PK_CumulativeMarkingTable ON dbo.CumulativeMarkingTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX IX_ExamSessionTable_90 ON dbo.ExamSessionTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX IX_UserTable_SeededCandId ON dbo.UserTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX PK_CohortUsersTable ON dbo.CohortUsersTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX idx_WAREHOUSE_ExamSessionTable_Shreded_ExamName ON dbo.WAREHOUSE_ExamSessionTable_Shreded REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX PK_WAREHOUSE_AssessmentSectionsTable ON dbo.WAREHOUSE_ExamSectionsTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX IX_IB3QualificationLookup ON dbo.IB3QualificationLookup REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX IX_CentreTable_Retired_NCI ON dbo.CentreTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX IX_WAREHOUSE_ExamSessionTable_CompDate_ExamState ON dbo.WAREHOUSE_ExamSessionTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX PK_ExtensionTypeTable ON dbo.DocumentExtensionTypeLookupTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX PK_AssessmentSectionTypesTable ON dbo.ExamSectionTypeLookupTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX idx_NC_WAREHOUSE_ExamSessionItemResponseTable_WAREHOUSEExamSessionID_ItemID_ItemVersion ON dbo.WAREHOUSE_ExamSessionItemResponseTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX IX_ExamSessionTable_UserID_NCI ON dbo.ExamSessionTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX PK_WAREHOUSE_ExamSessionTable_1 ON dbo.WAREHOUSE_ExamSessionTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX PK_MarkSchemeDocumentTable ON dbo.MarkSchemeDocumentTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX PK_UserTable_CI ON dbo.UserTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX PK_LiveItemSessionTable ON dbo.LiveItemSessionTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX PK_CandidateExternalCache ON dbo.CandidateExternalCache REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX IX_UserQualificationsTable_QualID ON dbo.UserQualificationsTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX idx_WAREHOUSE_ExamSessionTable_Shreded_Grade ON dbo.WAREHOUSE_ExamSessionTable_Shreded REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX PK_RolePermissionsTable ON dbo.RolePermissionsTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX PK_ExamSessionAudiTrail ON dbo.ExamSessionAuditTrail REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX PK_AnnotationsTable ON dbo.AnnotationsTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX PK_WAREHOUSE_CandidateReviewAuditTable ON dbo.WAREHOUSE_CandidateReviewAuditTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX PK_UserExamHistoryVersionTrackerTable_1 ON dbo.UserExamHistoryVersionTrackerTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX IX_WAREHOUSE_ExamSessionTable_ShrededItems ON dbo.WAREHOUSE_ExamSessionTable_ShrededItems REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX PK_ExamSessionDeliveryMechanismLookupTable ON dbo.ExamSessionDeliveryMechanismLookupTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX IX_UserTable_Username_NCI ON dbo.UserTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX missing_index_32_31_UserTable ON dbo.UserTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX PK_VoidJustificationLookupTable ON dbo.VoidJustificationLookupTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX IX_UserTable_Retired_Seeded_ID_NCI ON dbo.UserTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX PK_UserExamHistoryResitTrackerTable ON dbo.UserExamHistoryResitTrackerTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX PK_LocalUpdatesTable ON dbo.LocalUpdatesTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX idx_NC_ExamSessionTable_examState_attemptAutoSubmitForAwaitingUpload_CI ON dbo.ExamSessionTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX IX_ExamStateChangeAuditTable_ExamSessionID_CI ON dbo.ExamStateChangeAuditTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX PK_WAREHOUSE_DataLookup ON dbo.WAREHOUSE_DataLookupTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX PK_AssessmentSectionsTable ON dbo.ExamSectionsTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX PK_WAREHOUSE_ExamSessionAvailableItemsTable ON dbo.WAREHOUSE_ExamSessionAvailableItemsTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX UQ__ExamSessionItemR__6521F869 ON dbo.ExamSessionItemResponseTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX PK__sysdiagrams__599B3724 ON dbo.sysdiagrams REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX idx_WAREHOUSE_UserTable_UserId_version_OriginatorID ON dbo.WAREHOUSE_UserTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX PK_CohortTable ON dbo.CohortTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX PK_RolesTable ON dbo.RolesTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX warehousetime ON dbo.WAREHOUSE_ExamSessionTable_Shreded REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX PK_CountyLookupTable ON dbo.CountyLookupTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX PK_ExamSessionTagLookupTable ON dbo.ExamSessionTagLookupTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX IX_WAREHOUSE_ExamSessionTable_PreviousExamState ON dbo.WAREHOUSE_ExamSessionTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX IX_LiveItemSessionTable ON dbo.LiveItemSessionTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX UQ__WAREHOUS__42CEAA1F1FD8A9E3 ON dbo.WAREHOUSE_CandidateReviewTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX IX_UserTable_AccountExpiryDate_NCI ON dbo.UserTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX PK__ExamSessionItemR__642DD430 ON dbo.ExamSessionItemResponseTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX missing_index_28_27_UserTable ON dbo.UserTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX PK_ExamSessionDocumentInfoTable ON dbo.ExamSessionDocumentInfoTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX PK_QualificationStatusLookup ON dbo.QualificationStatusLookup REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX PK_UsersCentreReferenceLookup ON dbo.UsersCentreReferenceLookup REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX UK_principal_name ON dbo.sysdiagrams REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX FK_ExamSessionID ON dbo.WAREHOUSE_ExamSessionAvailableItemsTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX IX_WAREHOUSE_ExamSessionDurationAuditTable_Warehoused_ExamSessionID ON dbo.WAREHOUSE_ExamSessionDurationAuditTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX IX_UserTable_Surname_NCI ON dbo.UserTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX IX_WAREHOUSE_ScheduledExamsTable_ExamID_OrigID ON dbo.WAREHOUSE_ScheduledExamsTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX PK_LocalCentreReportsTable ON dbo.LocalCentreReportsTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX Idx_WAREHOUSE_ScheduledExamsTable_QualificationLevel ON dbo.WAREHOUSE_ScheduledExamsTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX idx_NC_ExamStateChangeAuditTable_NewState ON dbo.ExamStateChangeAuditTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX UQ__ExamSessionDocumentID ON dbo.ExamSessionDocumentInfoTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX IX_Invigilated_ExamID_INC_ID_CentreID_ScheduleStartDT_ScheduleEndDT_ActiveST_ActiveET_QualicationID_ExamName ON dbo.ScheduledExamsTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX PK_WAREHOUSE_CentreChangeAuditTable ON dbo.WAREHOUSE_CentreChangeAuditTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX IX_ExamSessionTable_examState ON dbo.ExamSessionTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX IX_WAREHOUSE_ScheduledExamsTable_QualId_OrigID ON dbo.WAREHOUSE_ScheduledExamsTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX idx_WAREHOUSE_ExamSessionTable_Shreded_QualificationName ON dbo.WAREHOUSE_ExamSessionTable_Shreded REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX PK_CPAuditTable ON dbo.CPAuditTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX PK_WAREHOUSE_ExamSessionDocumentTable ON dbo.WAREHOUSE_ExamSessionDocumentTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX PK_StyleProfileTable ON dbo.StyleProfileTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX IX_Type ON dbo.GenericLookup REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX idx_NC_ExamSessionAuditTrail_examInstanceID ON dbo.ExamSessionAuditTrail REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX PK_ExamSessionTable ON dbo.ExamSessionTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX IX_UserTable_OutCode_NCI ON dbo.UserTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX IX_UserTable_County_NCI ON dbo.UserTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX PK_LocalScanExamStateLookupTable ON dbo.LocalScanExamStateLookupTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX PK_LiveSectionSessionTable ON dbo.LiveSectionSessionTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX PK_WAREHOUSEExamSessionItemResponseTable_1 ON dbo.WAREHOUSE_ExamSessionItemResponseTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX IX_WAREHOUSE_ExamSessionTable_BTLOffice_CompDate_ExamState ON dbo.WAREHOUSE_ExamSessionTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX PK_CentreTable ON dbo.CentreTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX PK_PermissionsTable ON dbo.PermissionsTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX idx_WAREHOUSE_ExamSessionTable_Shreded_ScheduledDurationValue ON dbo.WAREHOUSE_ExamSessionTable_Shreded REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX PK_ConfigurationTable ON dbo.ConfigurationTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX IX_LiveSectionSessionTable_1 ON dbo.LiveSectionSessionTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX IX_WAREHOUSE_ExamSessionItemResponseTable_R11_3PT_script3 ON dbo.WAREHOUSE_ExamSessionItemResponseTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX IX_WAREHOUSE_ScheduledExamsTable_ID_WAREHOUSECentreID_NCI ON dbo.WAREHOUSE_ScheduledExamsTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX PK_WAREHOUSE_CandidateReviewTable ON dbo.WAREHOUSE_CandidateReviewTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX IX_UserTable_CandidateRef_NCI ON dbo.UserTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX pk_CandidateDemographicsTable ON dbo.CandidateDemographicsTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX IX_GroupStateLookupTable ON dbo.GroupStateLookupTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX PK_TokenStore ON dbo.TokenStoreTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX IX_CentreTable_CentreName_NCI ON dbo.CentreTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX idx_WAREHOUSE_ExamSessionTable_Shreded_CentreName ON dbo.WAREHOUSE_ExamSessionTable_Shreded REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX IX_UserID_VersionId_INC_ExamID ON dbo.UserExamHistoryVersionTrackerTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX IX_ExamSessionTagReferenceTable_ExamSessionId ON dbo.ExamSessionTagReferenceTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX IX_CentreTable_InstallCode ON dbo.CentreTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX PK_UserQualificationsTable_1 ON dbo.UserQualificationsTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX _dta_index_ExamSessionTable_6_477244755__K2_K7_1 ON dbo.ExamSessionTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX IX_WAREHOUSE_CandidateReviewBookingTable_PIN_NCI ON dbo.WAREHOUSE_CandidateReviewBookingTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX PK_ExamSessionDurationAuditTable ON dbo.ExamSessionDurationAuditTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX IX_StateManagementException ON dbo.StateManagementException REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX PK_WAREHOUSE_CentreTable ON dbo.WAREHOUSE_CentreTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX qualificationID ON dbo.WAREHOUSE_ExamSessionTable_Shreded REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX PK_LocalCentreUpdateTable ON dbo.LocalCentreUpdateTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX UQ__ExamSessionTable__67FE6514 ON dbo.ExamSessionTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX PK_StateManagementException ON dbo.StateManagementException REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX PK_ReMarkExamSessionItemTable ON dbo.ReMarkExamSessionItemTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX IX_WAREHOUSE_ExamSessionTable_WarehouseScheduledExamID_NCI ON dbo.WAREHOUSE_ExamSessionTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX idx_WAREHOUSE_ExamSessionTable_Shreded_ForeName ON dbo.WAREHOUSE_ExamSessionTable_Shreded REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX idx_WAREHOUSE_ExamSessionTable_Shreded_CandidateRef ON dbo.WAREHOUSE_ExamSessionTable_Shreded REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX PK_IB3QualificationLookup ON dbo.IB3QualificationLookup REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX IX_WAREHOUSE_ExamSessionTable_Shreded_R11_3PT_script1 ON dbo.WAREHOUSE_ExamSessionTable_Shreded REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX PK_WAREHOUSE_QualificationTable_DataLookup ON dbo.WAREHOUSE_QualificationTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX PK_RelationshipTypeLookupTable ON dbo.RelationTypeLookupTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX PK_GenericLookup ON dbo.GenericLookup REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX IX_CohortTable_Centre_CI ON dbo.CohortTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX idx_WAREHOUSE_ExamSessionTable_Shreded_ExamResult ON dbo.WAREHOUSE_ExamSessionTable_Shreded REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX idx_WAREHOUSE_ExamSessionItemResponseTable_WAREHOUSEExamSessionID ON dbo.WAREHOUSE_ExamSessionItemResponseTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX _dta_index_WAREHOUSE_ExamSessionTable_6_317244185__K2_K4 ON dbo.WAREHOUSE_ExamSessionTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX PK_FilterMappingTable ON dbo.FilterMappingTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX PK_ReMarkExamSessionTable ON dbo.ReMarkExamSessionTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX PK_ScheduledExamsTable ON dbo.ScheduledExamsTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX PK_ExamSessionDocumentTable ON dbo.ExamSessionDocumentTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX PK_UsedKeyCodeTable ON dbo.UsedKeyCodeTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX PK_LocalScanRemovedExamTable ON dbo.LocalScanRemovedExamTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX PK_CentreQualificationsTable ON dbo.CentreQualificationsTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX idx_NC_ScheduledExamsTable_invigilated_ExamID_CI ON dbo.ScheduledExamsTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX PK_ExamSessionCandidateInteractionEventsLookupTable ON dbo.ExamSessionCandidateInteractionEventsLookupTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX IX_WAREHOUSE_ExamSessionTable_Shreded_R11_3PT_script2 ON dbo.WAREHOUSE_ExamSessionTable_Shreded REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX PK_ExamStateChangeAuditTable ON dbo.ExamStateChangeAuditTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX PK_WAREHOUSEExamSessionItemMark ON dbo.WAREHOUSE_ExamSessionTable_ShreddedItems_Mark REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX missing_index_2_1_ExamSessionTable ON dbo.ExamSessionTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX _dta_index_UserTable_6_125243501__K21_K1_3_4_16_20_25 ON dbo.UserTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX PK_LocalCentreLastUploadDatesTable ON dbo.LocalCentreLastUploadDatesTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX submittedDate ON dbo.WAREHOUSE_ExamSessionTable_Shreded REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX _dta_index_ExamSessionTable_c_6_477244755__K1 ON dbo.ExamSessionTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX idx_WAREHOUSE_ExamSessionTable_Shreded_WarehouseExamState_submittedDate_grade ON dbo.WAREHOUSE_ExamSessionTable_Shreded REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX PK_CandiateReviewBookingTable ON dbo.WAREHOUSE_CandidateReviewBookingTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX IX_WAREHOUSE_ScheduledExamsTable_WarehouseCentreID_NCI ON dbo.WAREHOUSE_ScheduledExamsTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX PK_WAREHOUSE_DataLookupType ON dbo.WAREHOUSE_DataLookupType REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX PK_WAREHOUSE_ExamSessionDurationAuditTable ON dbo.WAREHOUSE_ExamSessionDurationAuditTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX IX_ExamSessionDocumentTable ON dbo.ExamSessionDocumentTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX _dta_index_ExamSessionTable_6_477244755__K6_K2_K3 ON dbo.ExamSessionTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX PK_AssignedUserRolesTable ON dbo.AssignedUserRolesTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX IX_WAREHOUSE_ExamSessionTable ON dbo.WAREHOUSE_ExamSessionTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX PK_WAREHOUSE_UserTable_CI ON dbo.WAREHOUSE_UserTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX IX_WAREHOUSE_CentreTable_CentreID_NCI ON dbo.WAREHOUSE_CentreTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX IX_WAREHOUSE_ExamSessionTable_Shreded_Keycode ON dbo.WAREHOUSE_ExamSessionTable_Shreded REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX PK_ConfigurationDataTypeLookupTable ON dbo.ConfigurationDataTypeLookupTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX PK_WAREHOUSE_ScheduledExamsTable ON dbo.WAREHOUSE_ScheduledExamsTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX PK_CountryLookupTable ON dbo.CountryLookupTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX PK_ExamStateLookupTable ON dbo.ExamStateLookupTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX idx_WAREHOUSE_ExamSessionTable_Shreded_QualificationLevel_centreId_NCI ON dbo.WAREHOUSE_ExamSessionTable_Shreded REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX IX_ExamSessionDurationAuditTable_ExamSessionID ON dbo.ExamSessionDurationAuditTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX PK_WAREHOUSE_ExamSessionResultHistoryTable ON dbo.WAREHOUSE_ExamSessionResultHistoryTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX PK_ExamSessionTable_Shredded ON dbo.ExamSessionTable_Shredded REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX IX_CohortUsersTable_UserID_NCI ON dbo.CohortUsersTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX PK_QualificationLevelsTable ON dbo.QualificationLevelsTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX PK_DeletedTable ON dbo.DeletedTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX PK_UserSessionRelationshipTable ON dbo.UserSessionRelationTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX PK_WAREHOUSE_CandidateReviewAuditDetailsTable ON dbo.WAREHOUSE_CandidateReviewAuditDetailsTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX IX_StyleProfileTable ON dbo.StyleProfileTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX IX_CentreTable_OutCode_NCI ON dbo.CentreTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX PK_WAREHOUSE_CandidateInteractionLogsTable ON dbo.WAREHOUSE_ExamSessionCandidateInteractionLogsTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX IX_WAREHOUSE_ExamSessionTable_keycode_ID ON dbo.WAREHOUSE_ExamSessionTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX PK_DeletedTypeTable ON dbo.DeletedTypeTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX PK_FailedResultUploadTable ON dbo.FailedResultUploadTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX PK_WAREHOUSE_ExamStateLookupTable ON dbo.WAREHOUSE_ExamStateLookupTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX missing_index_12_11_ExamSessionTable ON dbo.ExamSessionTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX PK_WAREHOUSE_ExamSessionDocumentInfoTable ON dbo.WAREHOUSE_ExamSessionDocumentInfoTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX PK_CentreChangeAuditTable ON dbo.CentreChangeAuditTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX PK_WAREHOUSE_ExamSessionTable_Shreded ON dbo.WAREHOUSE_ExamSessionTable_Shreded REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX IX_UserTable_Town_NCI ON dbo.UserTable REORGANIZE WITH ( LOB_COMPACTION = ON );
ALTER INDEX PK_GrantableRolesTable ON dbo.GrantableRolesTable REORGANIZE WITH ( LOB_COMPACTION = ON );

EXECUTE sp_updatestats

DECLARE @Statement1 nvarchar(MAX) = 'DBCC SQLPERF(LogSpace)'
INSERT #LogGrowth([DatabaseName], [logSpace], [logSpaceUsed], [status])
exec sp_executesql @Statement1;

UPDATE #LogGrowth
set timeStamp = getDATE()
where timeStamp IS NULL;

select *
from #LogGrowth
where DatabaseName = 'PRV_SQA_SecureAssess'






