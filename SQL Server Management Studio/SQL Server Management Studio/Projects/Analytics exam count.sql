select count(KEYCODE) [examCount], DAY(warehousetime) [Day],MONTH(warehousetime) [Month],YEAR(warehousetime)[Year]
from FactExamSessions
where YEAR(warehousetime) in(2015, 2016)
	group by DAY(warehousetime),MONTH(warehousetime),YEAR(warehousetime)
	order by YEAR(warehousetime), MONTH(warehousetime),  DAY(warehousetime)