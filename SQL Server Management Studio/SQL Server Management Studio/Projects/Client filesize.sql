use master

select a.DBName
	, sum(Size_in_MB) Size_in_MB
from (
SELECT
    substring(db.name, 0, CHARINDEX('_',db.name)) AS DBName
	, type_desc AS FileType
    , mf.size/128 as Size_in_MB
FROM
    sys.master_files mf
INNER JOIN 
    sys.databases db ON db.database_id = mf.database_id
	where db.database_id > 4
	and substring(db.name, 0, CHARINDEX('_',db.name)) <> ''
) a
group by a.DBName
order by 1;