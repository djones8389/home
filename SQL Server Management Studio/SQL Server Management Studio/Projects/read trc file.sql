select 
	LEFT(cast(TextData as nvarchar(max)),50) [QuerySummary]	
	, LEN(cast(TextData as nvarchar(max))) as [LengthOfQuery]
	, cast(Duration as float)/100000 [Duration in Seconds]
	, Reads
	, Writes
	
	--cast(SUM(Duration) as float)/100000
from ::fn_trace_gettable('C:\Users\davej\Desktop\Professional_CA3.trc',1)
where cast(TextData as nvarchar(max)) != 'exec sp_reset_connection' 
	order by 3 desc;
