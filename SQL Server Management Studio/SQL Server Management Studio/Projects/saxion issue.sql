SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;


--select distinct so.name
--from sys.sysobjects so
--inner join sys.syscomments sc
--on sc.id = so.id
--where sc.text like '%AssignedItemMarks%'

--SELECT c.name AS ColName, t.name AS TableName
--FROM sys.columns c
--    JOIN sys.tables t ON c.object_id = t.object_id
--WHERE c.name like  '%GroupMarkId%'
/*
		--Insert an entry in the dbo.AssignedItemMarks.					
		INSERT INTO dbo.AssignedItemMarks
				( GroupMarkId,
				  UniqueResponseId,
				  AnnotationData,
				  AssignedMark,
				  MarkingDeviation,
				  MarkedMetadataResponse
				)
		SELECT @groupMarkId, UR.ID, UR.Annotations, UR.Mark, UR.MarkingDeviation, UR.MarkedMetadata
		FROM @uniqueResponses UR
		*/

--INSERT INTO dbo.AssignedItemMarks(GroupMarkId, UniqueResponseId, AssignedMark)
	--GroupMarkId --autoid
	--, ur.id
	--, 1 --ur.confirmedMark

USE Saxion_SecureAssess

select wesirt.ItemID
from WAREHOUSE_ExamSessionItemResponseTable wesirt
Inner join WAREHOUSE_ExamSessionTable west
on wesirt.WAREHOUSEExamSessionID = west.ID
where keycode = 'XBH3GL6P'


USE Saxion_SecureMarker
SELECT 
 cev.*
FROM CandidateExamVersions as CEV 

INNER JOIN CandidateResponses as CR
ON CR.CandidateExamVersionID = CEV.ID

INNER JOIN UniqueResponses as UR
ON UR.ID = CR.UniqueResponseID 

inner join UniqueGroupResponseLinks as UGRL
on UGRL.UniqueGroupResponseID = UR.id

inner join UniqueGroupResponses as UGR
on UGR.ID = UGRL.UniqueGroupResponseID

inner join AssignedGroupMarks as AGM
on AGM.UniqueGroupResponseId = UGR.ID

left join UniqueResponseDocuments as URD
ON URD.UniqueResponseID = UR.id 
	
Inner Join Items as I
on I.ID = ur.itemId

LEFT JOIN dbo.Moderations M ON M.UniqueResponseId = UR.id AND M.CandidateExamVersionId = cev.ID AND M.IsActual = 1

LEFT JOIN dbo.AssignedItemMarks AIM ON  AIM.UniqueResponseId = CR.UniqueResponseId AND AIM.GroupMarkId = AGM.ID           

where ExternalItemID = '5715P346350'
	and Keycode = 'XBH3GL6P'
	and ExamSessionState <> 7
	