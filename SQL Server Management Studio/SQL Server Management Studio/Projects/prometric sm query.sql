USE [AAT_SecureMarker]
GO

SELECT 
	  AM.id                          AS AssignedMarkID
	  ,CEV.ExternalSessionID
	  ,CEV.Keycode
      ,UGR.ID                         AS UniqueGroupResponseID
      --,ISNULL(U.ID ,-1)               AS UserID
      --,ISNULL(U.Surname ,AM.Surname)  AS Surname      
      ,GD.TotalMark                   AS TotalMark
      ,AM.assignedMark                AS AssignedMark
      ,AM.timestamp                   AS TIMESTAMP
      --,GD.Name                        AS GroupName
      --,ISNULL(U.Surname+', '+U.Forename ,AM.fullName) AS FullName
      --,GD.ExamID                      AS ExamID
      --,UGR.GroupDefinitionID          AS GroupID
      --,dbo.sm_checkIsUniqueGroupResponseInConfirmedScript(UGR.ID) AS IsInConfirmedScript
      --,UGR.CI						  AS CI
	  --, cr.*
FROM   [dbo].[AssignedGroupMarks] AM
       LEFT OUTER JOIN [dbo].[Users] U ON  AM.userId = U.ID
       INNER JOIN [dbo].[UniqueGroupResponses] UGR ON  AM.uniqueGroupResponseId = UGR.ID
       INNER JOIN UniqueGroupResponseLinks as UGRL on UGRL.UniqueGroupResponseID = UGR.ID
	   INNER JOIN UniqueResponses as UR ON UR.ID =  UGRL.UniqueResponseID 
	   INNER JOIN CandidateResponses CR on CR.UniqueResponseID = UR.ID
	   INNER JOIN CandidateExamVersions CEV ON CEV.ID = CR.CandidateExamVersionID
      INNER JOIN [dbo].[GroupDefinitions] GD ON  UGR.GroupDefinitionID = GD.ID
WHERE   AM.ID IN 
       (
			SELECT TOP 1 AGM.ID
			FROM AssignedGroupMarks AGM
			WHERE AGM.UniqueGroupResponseId = UGR.ID
			AND AGM.IsConfirmedMark = 1
			AND AGM.MarkingMethodId IN (4, 7, 15, 17)
			ORDER BY Timestamp DESC
	   )
	   AND UGR.ID IN (927746,927697,927686,926759,926755,926752,926750,918134,907530)
	   and ExamID = 170
	   and GD.ID = 5618
	   --and U.ID = 170
	   --AND CI <> 1
       AND EXISTS ( SELECT TOP 1 *
                         FROM   dbo.GroupDefinitionStructureCrossRef AS CR
                                INNER JOIN dbo.ExamVersionStructures AS EVS ON EVS.ID = CR.ExamVersionStructureID
                         WHERE  ( CR.Status = 0 )
                                AND ( CR.GroupDefinitionID = GD.ID )
                                AND ( EVS.StatusID = 0 ) )

								order by 2 desc;


Select *
from CandidateExamVersions
where ID IN ( 153352,153353)