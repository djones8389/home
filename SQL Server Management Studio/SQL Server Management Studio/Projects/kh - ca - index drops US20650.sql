USE PSCollector

SELECT  
	[Instance]
	,[database]
	,[table]
	,[index]
	,[user_updates]
	,[last_user_update]
	,[user_seeks]
	,[user_scans]
	,[user_lookups]
	,([user_seeks] + [user_scans] + [user_lookups]) [Combination]
	,[last_user_seek]
	,[last_user_scan]
	,[last_user_lookup]
	,cast(StartDate as date) as [collectionDate]
FROM IndexMetrics
WHERE [database] LIKE '%ContentAuthor'
	AND StartDate > '2018-04-09'
	--AND [index] in ('IX_UserTable_Surname_NCI','IX_WAREHOUSE_ExamSessionDurationAuditTable_Warehoused_ExamSessionID','examSessionId_NCI','IX_WAREHOUSE_ExamSessionTable_PreviousExamState','IX_WAREHOUSE_ExamSessionTable_submissionExported','idx_WAREHOUSE_ExamSessionTable_Shreded_ForeName','idx_WAREHOUSE_ExamSessionTable_Shreded_Grade','qualificationID','submittedDate','IX_WAREHOUSE_ExamSessionTable_Shreded_R11_3PT_script1','IX_ExamIDExamVersionID')
	--and ([user_seeks] + [user_scans] + [user_lookups]) > 0
	--and [database] = 'WJEC_SecureAssess'
--	and [Instance]in ('','','')
--order by 1,2,3,4

	--and [table] in ('Subjects','Items','AttributeChanges')
	and [index] in ('IX_FileId','IX_UserId','IX_ItemId','IX_ItemId','IX_CreatedById','IX_LastEditedById ','IX_LockedById','IX_OwnerId','IX_FileId','IX_MediaItems_SourceType','IX_ThumbnailId','IX_NotificationType','IX_SubjectId','IX_SubjectTagTypes_TagTypeKey','IX_dbo.TestCoverInfos_TestFormId_CreateDate','IX_KeyCode','IX_FacultyId','IX_UserId')
	order by ([user_seeks] + [user_scans] + [user_lookups]) desc