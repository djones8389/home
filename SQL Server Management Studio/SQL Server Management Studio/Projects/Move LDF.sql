use master;

DECLARE @NewLogLocation nvarchar(20) = 'D:\Log'

DECLARE @AlterLocation nvarchar(MAX) = '';
DECLARE @moveLog nvarchar(MAX) = '';
DECLARE @setOnline nvarchar(MAX) = '';


DECLARE @Tables TABLE(DBName nvarchar(100), LogName nvarchar(250), LogFileName nvarchar(250),FileName nvarchar(250))
INSERT @Tables
SELECT D.Name
	  , F.Name
	  , F.physical_name
	  , SUBSTRING(F.physical_name,LEN(REVERSE(SUBSTRING(REVERSE(F.physical_name), CHARINDEX('\',REVERSE(F.physical_name)), LEN(F.physical_name))))+1,LEN(F.physical_name))
FROM sys.master_files as F
inner join sys.databases as D
on D.database_id = F.database_id 
where D.name in ('SPDurationMetrics')
	and type_desc = 'LOG'

SELECT @AlterLocation += char(13) + 
	'ALTER DATABASE [' + DBName + '] SET OFFLINE;

ALTER DATABASE [' + DBName + '] MODIFY FILE (NAME = ' + LogName  + ', Filename=''' + @NewLogLocation + '\'+ FileName + ''');'
FROM @Tables 


SELECT @setOnline += char(13) +  
	'ALTER DATABASE [' + DBName + '] SET ONLINE;'
FROM @Tables 


SELECT @moveLog += char(13) +  
	 'EXECUTE xp_cmdshell ''move /Y '+  LogFileName + ' '+ @NewLogLocation + '\' + FileName +''''
FROM @Tables 

PRINT(@AlterLocation);
PRINT(@moveLog);
PRINT(@setOnline);
