--select SubjectTagValues.Id,* from Items 

--inner join SubjectTagValueItems on SubjectTagValueItems.Item_Id = Items.Id
--inner join SubjectTagValues on SubjectTagValues.id = SubjectTagValueItems.SubjectTagValue_Id
--inner join SubjectTagTypes on SubjectTagTypes.Id = SubjectTagValues.SubjectTagTypeId
--inner join Subjects on subjects.Id = Items.SubjectId

--where Items.Id  = 4392


SELECT stvi.Item_Id, [SubjectTagTypeId], [Text]      
  FROM  [dbo].[SubjectTagValues] sv
  inner join [dbo].[SubjectTagValueItems] stvi on stvi.SubjectTagValue_Id = sv.Id   
  
  --where deleted = 1

  group by stvi.Item_Id, [SubjectTagTypeId], [Text]
  HAVING  count(Id)   > 1







select a.*
FROM (

select 
   ROW_NUMBER() OVER(PARTITION BY 
	 SubjectTagValues.Text
	, SubjectTagValues.Deleted
	, SubjectTagTypeId
	, TagTypeKey
	, SubjectTagTypes.Deleted  
	, Subjects.ID
	, Items.Id
		ORDER BY SubjectTagValue_Id
	, SubjectTagValues.Text
	, SubjectTagValues.Deleted
	, SubjectTagTypeId
	, TagTypeKey
	, SubjectTagTypes.Deleted	
	, Subjects.ID
	, Items.Id
	) R

	,SubjectTagValue_Id
	, SubjectTagValues.Text
	, SubjectTagValues.Deleted [SubjectTagValuesDeleted]
	, SubjectTagTypeId
	, TagTypeKey
	, SubjectTagTypes.Deleted	
	, Subjects.ID [SubjectID]
	, Items.Id [ItemID]
  
from Items 

inner join SubjectTagValueItems on SubjectTagValueItems.Item_Id = Items.Id
inner join SubjectTagValues on SubjectTagValues.id = SubjectTagValueItems.SubjectTagValue_Id
inner join SubjectTagTypes on SubjectTagTypes.Id = SubjectTagValues.SubjectTagTypeId
inner join Subjects on subjects.Id = Items.SubjectId

--where Items.Name = 'BBANERE01'

) A
where r > 1


