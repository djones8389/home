SELECT  
	[TableName]
	,[index_size-kb] = cast(LTRIM (STR ((CASE WHEN usedpages > pages THEN (usedpages - pages) ELSE 0 END) * 8, 15, 0)) as int)
	, name [index name]	
FROM (

	SELECT 
		OBJECT_NAME(s.object_id) [TableName]
		, (used_page_count) [usedpages]
		,  (
			CASE
				WHEN (s.index_id < 2) THEN (in_row_data_page_count + lob_used_page_count + row_overflow_used_page_count)
				ELSE lob_used_page_count + row_overflow_used_page_count
			END
		) [pages]
		, i.name
	FROM sys.dm_db_partition_stats AS s
		INNER JOIN sys.indexes AS i
			ON s.[object_id] = i.[object_id]
			AND s.[index_id] = i.[index_id]
		INNER JOIN sys.tables t
		on t.object_id = s.object_id
		
		--where OBJECT_NAME(s.object_id)  = 'EmailAddress'
) A
--where TableName = 'WAREHOUSE_ExamSessionItemResponseTable'
--where name = '_dta_index_ExamSessionTable_6_477244755__K2_K7_1'
	order by [index_size-kb] DESC


	--exec sp_spaceused 'WAREHOUSE_ExamSessionItemResponseTable'   
	--index_size 350360 KB


ALTER INDEX [idx_NC_WAREHOUSE_ExamSessionItemResponseTable_WAREHOUSEExamSessionID_ItemID_ItemVersion] ON [WAREHOUSE_ExamSessionItemResponseTable] REBUILD;


--SELECT  
--	[TableName]
--	,index_size = LTRIM (STR ((CASE WHEN usedpages > pages THEN (usedpages - pages) ELSE 0 END) * 8, 15, 0) )	
--into #index
--FROM (

--	SELECT 
--		SUM (used_page_count) [usedpages]
--		, SUM (
--			CASE
--				WHEN (s.index_id < 2) THEN (in_row_data_page_count + lob_used_page_count + row_overflow_used_page_count)
--				ELSE lob_used_page_count + row_overflow_used_page_count
--			END
--		) [pages]
--		,OBJECT_NAME(s.object_id) [TableName]
--	FROM sys.dm_db_partition_stats AS s
--		INNER JOIN sys.indexes AS i
--			ON s.[object_id] = i.[object_id]
--			AND s.[index_id] = i.[index_id]
--		INNER JOIN sys.tables t
--		on t.object_id = s.object_id
		
--		--where OBJECT_NAME(s.object_id)  = 'EmailAddress'

--		group by OBJECT_NAME(s.object_id)

--		) A

		--select *
		--from #index
		--order by cast(index_size as int) desc