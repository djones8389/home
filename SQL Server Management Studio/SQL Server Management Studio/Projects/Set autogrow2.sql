--Set all MDF's to autogrow by 100MB--

DECLARE @AutoGrow TABLE
(
	Name nvarChar(100)
	, fileID tinyint
	, filename nvarchar(1000)
	, filegroup nvarchar(100)
	, size nvarchar(100)
	, maxSize nvarchar(100)
	, growth  nvarchar(20)
	, usage nvarchar(30)

)
INSERT @AutoGrow
exec sp_MSforeachdb '

use [?];

IF (
''?'' not in (''msdb'',''model'',''master'',''tempdb'')
)

EXEC sp_helpfile  '


DECLARE @NewFileSize nvarchar(10) = '25MB'

SELECT 
'ALTER DATABASE [' + D.Name + ']
	MODIFY FILE ( NAME =''' + F.Name + ''', FILEGROWTH =' + @NewFileSize + ' )'
	,A.growth
	, d.name
FROM @AutoGrow  A
    INNER JOIN sys.master_files as F
    on F.physical_name COLLATE SQL_Latin1_General_CP1_CI_AS = A.filename
		
    INNER JOIN sys.databases as D
	on D.database_id  = F.database_id 
	
	where A.filename like '%.mdf%'
		and A.growth NOT IN ('102400 KB')
		and f.database_id > 4
		and type_desc = 'ROWS'
		and d.name like 'ppd_ocr%'
--and (d.name not like '%content%' and d.name not like '%cppro%'
--	and d.name not like '%secureassess%' and d.name not like '%itembank%'
--	and d.name not like '%securemarker%' and d.name not like '%surpassdata%'
--	and d.name not like '%testpack%'
--	)

order by d.name		

--Others, 25MB

--CP 50mb
--IB 50mb
--OA 50MB
--TPM 50mb

--SA 100mb
--SM 100mb
--SDW 100mb
