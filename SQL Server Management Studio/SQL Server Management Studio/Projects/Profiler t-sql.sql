--declare @p1 int
--set @p1=2
--exec sp_trace_create @p1 output,1,NULL,NULL,NULL
--select @p1
--go



declare @p1 int = 2
	, @maxfilesize bigint = 100
	, @stopDate datetime = (select dateadd(DAY, 3,getdate()))
	, @tracefile nvarchar(256) = 'C:\Sproc2'

exec sp_trace_create @traceid = @p1 output
	,@options=2
	,@tracefile=@tracefile
	,@maxfilesize = @maxfilesize
	,@stoptime= @stopDate
	,@filecount= 50

go


exec sp_trace_setevent 2,12,1,1
go
exec sp_trace_setevent 2,12,11,1
go
exec sp_trace_setevent 2,12,12,1
go
exec sp_trace_setevent 2,12,13,1
go
exec sp_trace_setevent 2,12,14,1
go
exec sp_trace_setevent 2,12,15,1
go
exec sp_trace_setevent 2,12,16,1
go
exec sp_trace_setevent 2,12,17,1
go
exec sp_trace_setevent 2,12,18,1
go
exec sp_trace_setevent 2,12,35,1
go
exec sp_trace_setfilter 2,10,0,7,N'SQL Server Profiler - 6b66a5aa-94ad-4652-a02d-10a3647d37c5'
go
exec sp_trace_setstatus 2,1
go
