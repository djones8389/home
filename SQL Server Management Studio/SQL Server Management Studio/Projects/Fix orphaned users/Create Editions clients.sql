DECLARE @ClientCreator table (
	Name varchar(100)
)

insert @ClientCreator
select 'ItemBank' union
select 'SecureAssess' union
select 'ContentAuthor' union
select 'SurpassManagement' --union
--select 'SurpassDataWarehouse' union


declare @mysql nvarchar(MAX)=''

select @mysql += CHAR(13) + '

create database [Bpec_'+[name]+']

create login [Bpec_'+[name]+'_User] with password = ''123456789''

use [Bpec_'+[name]+']  create user [Bpec_'+[name]+'_User] for login  [Bpec_'+[name]+'_User]
exec sp_addrolemember ''db_owner'',''Bpec_'+[name]+'_User''

'
from @ClientCreator

print(@mysql)


create login [Bpec_ETL_User] with password = '123456789'


use [Bpec_SecureAssess] Create user [Bpec_ETL_User] for login [Bpec_ETL_User]
exec sp_addrolemember 'db_datareader','Bpec_ETL_User'

use [Bpec_ItemBank] Create user [Bpec_ETL_User] for login [Bpec_ETL_User]
exec sp_addrolemember 'db_datawriter','Bpec_ETL_User'

use [Bpec_ContentAuthor] Create user [Bpec_ETL_User] for login [Bpec_ETL_User]
exec sp_addrolemember 'db_owner','Bpec_ETL_User'
