USE [Prometric_SurpassLocal]

--Grab Comms Key

DECLARE @CommsKey nvarchar(20) = (
	SELECT [CommunicationKey] 
	FROM [Prometric_SurpassLocal].[dbo].[LocalSettingsTable]
);

--Choose where you want files to be saved

DECLARE @SaveLocation nvarchar(30) = 'D:\PrometricItems\'

--Produce commands for CMD

	SELECT
		 'LocalDecryptor.exe --commskey='+@CommsKey+' --encryptedtext=' + [ItemResponseData].value('data(/p)[1]','nvarchar(MAX)')
			 + ' > '+ @SaveLocation + cast([ExamSessionID] as nvarchar(10))+'_'+Keycode+'_'+cast([ItemID] as nvarchar(12)) 
				+ '_V' + cast([ItemVersion]as nvarchar(5))+'.xml' [CmdLine]
	FROM [Prometric_SurpassLocal].[dbo].[ExamSessionItemResponseTable] ESIRT
	INNER JOIN ExamSessionTable EST ON EST.ID = ESIRT.ExamSessionID 
  
--Launch CMD,  cd to decryptor location:   cd D:\Local Data Decryptor
--This will save all the decrypted itemXMLs into your @SaveLocation


--Add a new column

	ALTER TABLE [Prometric_SurpassLocal].[dbo].[ExamSessionItemResponseTable]
		ADD [Decrypted] XML;


--Update the [Decrypted] for each item, using powershell


