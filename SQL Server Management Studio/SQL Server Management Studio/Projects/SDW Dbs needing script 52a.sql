if object_ID('tempdb..#temp') IS NOT NULL DROP TABLE #temp;

create table #temp (
	dbname nvarchar(1000)
)

INSERT #temp
exec sp_MSforeachdb '

use [?];

if(''?'' like ''%SurpassDataWarehouse%'')

begin

if exists (select 1 from INFORMATION_SCHEMA.TABLES where TABLE_NAME = ''_MigrationScripts'')
	
select distinct ''?'' 
from dbo._MigrationScripts
where  exists (select 1 from _MigrationScripts where FileName = ''52_Update_SurpassDataWarehouse_Schema_73_74_StagingTables.sql'')
	and not exists (select 1 from _MigrationScripts where FileName = ''52a_Update_SurpassDataWarehouse_Schema_74_74_stgDimOptions_hotfix.sql'')
	
end

'

select * from #temp

/*
, ID
	, filename
	, checksum
	, startDateTime
	, WindowsuserName
	
	
select ''?'' 
from dbo._MigrationScripts
where  exists (select 1 from _MigrationScripts where FileName = '52_Update_SurpassDataWarehouse_Schema_73_74_StagingTables.sql')
	and not exists (select 1 from _MigrationScripts where FileName = '52a_Update_SurpassDataWarehouse_Schema_74_74_stgDimOptions_hotfix.sql')
	
	*/
