/****** Script for SelectTopNRows command from SSMS  ******/
--SELECT ss.[Id]
--      ,[CentreId]
--	  , c.name [centre]
--      ,[SubjectId]
--	  , s.name [subject]
--      ,[Assignable]
--      ,[RoleId]
select ss.*
  FROM [Demo_SurpassManagement].[dbo].[SharedSubjectRoles] ss
  inner join subjects s
  on s.id = ss.subjectid
  inner join centres c
  on c.id = ss.centreid

where s.name in ('laura subject', 'Wardak Test Subject')


select id 'centre'
from centres
where name = 'btltest1'


select id 'subject'
from subjects s
where s.name = 'Wardak Test Subject'


SELECT ss.[Id]
      ,[CentreId]
      ,[SubjectId]
      ,[Assignable]
      ,[RoleId]
	  , c.name
	  , s.id
  FROM [Demo_SurpassManagement].[dbo].[SharedSubjectRoles] ss
  inner join subjects s
  on s.id = ss.subjectid
  inner join centres cl
  on c.id = ss.centreid

where s.name = 'Wardak Test Subject'


insert [SharedSubjectRoles] (CentreId, SubjectId, Assignable, RoleId)
values(295,542,0,5)

