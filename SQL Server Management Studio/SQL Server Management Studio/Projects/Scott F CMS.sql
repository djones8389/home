USE master

exec sp_MSforeachdb '
	
	use [?]

	Declare @StartDate DATE = ''01 Sep 2012''
	Declare @EndDate DATE = ''30 Dec 2012''
		
	if(db_ID() = 1)

	BEGIN
		select @StartDate
	END

'




/*
ID	ExamSessionID	NewState	StateChangeDate	StateInformation
2270226	225108	9	2012-10-06 12:23:14.063	NULL
2270227	225108	9	2012-10-06 12:23:21.720	
*/

--SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

--	SELECT COUNT(State9.ExamSessionID)
--	FROM  (
--	select ExamSessionID
--	from ExamStateChangeAuditTable
--		where ExamStateChangeAuditTable.NewState = 9
--			and StateChangeDate between @StartDate and @EndDate
--			group by ExamSessionID
--	) State9
--	where exists (	
--	select 1
--	from ExamStateChangeAuditTable
--	where ExamStateChangeAuditTable.ExamSessionID = State9.ExamSessionID
--		and ExamStateChangeAuditTable.NewState = 6
--	)
