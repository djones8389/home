declare @bigSQL nvarchar(MAX) = '';



SELECT @bigSQL +=CHAR(13) +
--B.name AS TableName
--, C.name AS IndexName
--, C.fill_factor AS IndexFillFactor
--, D.rows AS RowsCount
--, A.avg_fragmentation_in_percent
--, A.page_count
--, 
'ALTER INDEX ' + C.name +  ' ON ' + 'dbo' + '.' + B.name + ' REORGANIZE;'
FROM sys.dm_db_index_physical_stats(DB_ID(),NULL,NULL,NULL,NULL) A
INNER JOIN sys.objects B
ON A.object_id = B.object_id
INNER JOIN sys.indexes C
ON B.object_id = C.object_id AND A.index_id = C.index_id
INNER JOIN sys.partitions D
ON B.object_id = D.object_id AND A.index_id = D.index_id
WHERE C.index_id > 0
	and A.avg_fragmentation_in_percent > 30

PRINT(@bigSQL)

/*

declare @dynamic nvarchar(MAX);
declare @Maintenance TABLE (mySQL nvarchar(MAX))

set @dynamic = '

use [?];

if(''?'' in (select name from sys.databases 
	where state_desc = ''ONLINE''
		and database_id > 4) )

SELECT
''USE ['' + ''?'' + ''] ALTER INDEX '' + C.name +  '' ON '' + ''dbo'' + ''.'' + B.name + '' REORGANIZE;''
FROM sys.dm_db_index_physical_stats(DB_ID(),NULL,NULL,NULL,NULL) A
INNER JOIN sys.objects B
ON A.object_id = B.object_id
INNER JOIN sys.indexes C
ON B.object_id = C.object_id AND A.index_id = C.index_id
INNER JOIN sys.partitions D
ON B.object_id = D.object_id AND A.index_id = D.index_id
WHERE C.index_id > 0
	and A.avg_fragmentation_in_percent > 30


'
--INSERT @Maintenance 
exec sp_msforeachdb @dynamic


EXECUTE(@dynamic)




DECLARE myMaintenance CURSOR FOR
select name from sys.databases 
	where state_desc = 'ONLINE'
		and database_id > 4

DECLARE @DBName nvarchar(200);

OPEN myMaintenance;

FETCH NEXT FROM myMaintenance INTO @DBName;

WHILE @@FETCH_STATUS = 0

BEGIN

PRINT()


FETCH NEXT FROM myMaintenance INTO @DBName;

END
CLOSE myMaintenance;
DEALLOCATE myMaintenance;


*/





/*

'

use [?];

if(''?'' in (select name from sys.databases 
	where state_desc = ''ONLINE''
		and database_id > 4) )

SELECT 
''USE ['' + ''?'' + ''] ALTER INDEX '' + C.name +  '' ON '' + ''dbo'' + ''.'' + B.name + '' REORGANIZE;''
FROM sys.dm_db_index_physical_stats(DB_ID(),NULL,NULL,NULL,NULL) A
INNER JOIN sys.objects B
ON A.object_id = B.object_id
INNER JOIN sys.indexes C
ON B.object_id = C.object_id AND A.index_id = C.index_id
INNER JOIN sys.partitions D
ON B.object_id = D.object_id AND A.index_id = D.index_id
WHERE C.index_id > 0
	and A.avg_fragmentation_in_percent > 30

'
*/