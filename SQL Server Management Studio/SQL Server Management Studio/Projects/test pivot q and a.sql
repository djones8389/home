USE SurpassDataWarehouse_Empty


if OBJECT_ID('tempdb..#questions') is not null drop table #questions;
if OBJECT_ID('tempdb..#answers') is not null drop table #answers;

create table #questions (

	itemid varchar(10)
	, question varchar(100)
	, answers varchar(100)
);

create table #answers (

	itemid varchar(10)
	, answers varchar(100)
);

INSERT #questions(itemid,question)
values('123p456', 'what colour is the sky?')
 , ('555p666', 'which animal is this?')

INSERT #answers(itemid,answers)
values('123p456', 'blue')
,('123p456', 'red')
,('555p666', 'monkey')
,('555p666', 'koala')

update #questions
set answers = a.NameValues
from #questions q
inner join (
	SELECT 
	  itemid,
	  STUFF((
		SELECT ', ' + [answers]  
		FROM #answers 
		WHERE (itemid = Results.itemid) 
		FOR XML PATH(''),TYPE).value('(./text())[1]','VARCHAR(MAX)')
	  ,1,2,'') AS NameValues
	FROM #answers Results
	GROUP BY itemid
) a
on a.itemid = q.itemid







CREATE TABLE #YourTable ([ID] INT, [Name] CHAR(1), [Value] INT)

INSERT INTO #YourTable ([ID],[Name],[Value]) VALUES (1,'A',4)
INSERT INTO #YourTable ([ID],[Name],[Value]) VALUES (1,'B',8)
INSERT INTO #YourTable ([ID],[Name],[Value]) VALUES (2,'C',9)

SELECT 
  [ID],
  STUFF((
    SELECT ', ' + [Name] + ':' + CAST([Value] AS VARCHAR(MAX)) 
    FROM #YourTable 
    WHERE (ID = Results.ID) 
    FOR XML PATH(''),TYPE).value('(./text())[1]','VARCHAR(MAX)')
  ,1,2,'') AS NameValues
FROM #YourTable Results
GROUP BY ID

DROP TABLE #YourTable



































