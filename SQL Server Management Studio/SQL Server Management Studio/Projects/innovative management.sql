

select KeyCode 
from WAREHOUSE_ExamSessionTable west WITH (NOLOCK)
--left JOIN WAREHOUSE_ExamSessionItemResponseTable WESIRT WITH (NOLOCK)
--ON WESIRT.WAREHOUSEExamSessionID = WEST.ID
where west.KeyCode in ('7FKGBYA6','GP9XLWA6','J4K4PTA6','7J7GDPA6','QCKM9BA6','4N4DL7A6','YTTM7HA6','6MR3NMA6','73YPBYA6','BWLNP7A6','PRGGN6A6','FNPXKYA6','C3FMH3A6','DQP4MYA6')

select examsessionid
, qualificationName
, examName
, examVersionName
, examVersionRef
, resultData
from WAREHOUSE_ExamSessionTable_Shreded
where centreName = 'innovative management & professional training'
	and qualificationName = 'bookkeeping controls (aq2016)'
	and previousExamState != 10

select ItemResponseData, KeyCode
from WAREHOUSE_ExamSessionItemResponseTable r
inner join WAREHOUSE_ExamSessionTable s
on s.ID = r.WAREHOUSEExamSessionID
where ItemID in ('974P1252','974P1125','974P1180','974P1169','974P1132','974P1064')
	 and s.id in (
		select examsessionid
		from WAREHOUSE_ExamSessionTable_Shreded
		where centreName = 'innovative management & professional training'
			and qualificationName = 'bookkeeping controls (aq2016)'
			and previousExamState != 10
)

order by KeyCode

	select west.examsessionid, west.KeyCode,west.resultData, resultDataFull
		from WAREHOUSE_ExamSessionTable_Shreded wests
		inner join WAREHOUSE_ExamSessionTable west
		on west.id = wests.examSessionId
		inner join WAREHOUSE_ExamSessionItemResponseTable wesirt
		on west.ID = wesirt.WAREHOUSEExamSessionID
		where west.id in (
			select examsessionid
			from WAREHOUSE_ExamSessionTable_Shreded
			where centreName = 'innovative management & professional training'
				and qualificationName = 'bookkeeping controls (aq2016)'
			and previousExamState != 10
			)
			and ItemID in ('974P1252','974P1125','974P1180','974P1169','974P1132','974P1064')
			order by KeyCode



select ItemResponseData, KeyCode
from WAREHOUSE_ExamSessionItemResponseTable r
inner join WAREHOUSE_ExamSessionTable s
on s.ID = r.WAREHOUSEExamSessionID
where s.id in (
			select examsessionid
			from WAREHOUSE_ExamSessionTable_Shreded
			where centreName = 'innovative management & professional training'
				and qualificationName = 'bookkeeping controls (aq2016)'
			and previousExamState != 10
			)
for xml path('Response')
