--1   Kill SPIDs

DECLARE @KillSpids nvarchar(MAX) = ''

SELECT @KillSpids +=CHAR(13) + 'kill ' + convert(varchar(5),spid)
from sys.sysprocesses s
where dbid = DB_ID()
	and spid != @@SPID
	
EXEC(@KillSpids);


--2  Backup

DECLARE @Location nvarchar(100) = 'T:\Backup'
DECLARE @BackupDB nvarchar(MAX) = ''

SELECT @BackupDB += N'BACKUP DATABASE ['+name+N'] TO DISK = N''' +  @Location + '\' + NAME + '.bak'' WITH NAME = N'''+name+N'- Database Backup'', COPY_ONLY, NOFORMAT, NOINIT, SKIP, COMPRESSION, NOREWIND, NOUNLOAD, STATS = 10, MEDIAPASSWORD = ''bKyrfe3k7rDO4KDLwFeBP6sM0eqXQiu3'';'  
FROM sys.databases 
where database_id = db_id()

EXEC(@BackupDB)


-- 3  RestoreVerifyOnly

DECLARE @Location nvarchar(100) = 'T:\Backup'
DECLARE @RestoreVerifyOnly nvarchar(MAX) = ''

SELECT @RestoreVerifyOnly += N'RESTORE VERIFYONLY FROM DISK = N''' +  @Location + '\' + NAME + '.bak'' WITH MEDIAPASSWORD = ''bKyrfe3k7rDO4KDLwFeBP6sM0eqXQiu3'';'  
FROM sys.databases 
where database_id = db_id()

EXEC(@RestoreVerifyOnly)


-- 4 Growth metrics

if OBJECT_ID('tempdb..##DBName') IS NOT NULL DROP TABLE ##DBName;

CREATE TABLE ##DBName (DBName sysname) 
INSERT ##DBName
SELECT DB_NAME();

if not exists (select 1 from sys.databases where name = 'OCRGrowthStats')

	CREATE DATABASE [OCRGrowthStats];
	GO

USE [OCRGrowthStats];

if not exists (select 1 from sys.tables where name = 'TableStats')
	CREATE TABLE [OCRGrowthStats].[dbo].[TableStats] (
		 database_name nvarchar(100) null
		, table_name nvarchar(100)
		, rows int
		, reserved_kb nvarchar(20)
		, data_kb nvarchar(20)
		, index_size nvarchar(20)
		, unused_kb nvarchar(20)
		, CollectionDate datetime null
	);
	GO


declare @dynamicScript nvarchar(MAX) = ''

SELECT @dynamicScript+=char(13) +
'
use '+quotename(DBName)+';

	declare @dynamic nvarchar(MAX) = '''';

	select @dynamic +=CHAR(13) +
		''exec sp_spaceused @objname = ''''''+s.name+''.''+t.name + '''''' ''
	from sys.tables T
	inner join sys.schemas S
	on s.schema_id = t.schema_id
	where type = ''u''
	order by t.name;

	INSERT [OCRGrowthStats].[dbo].[TableStats] (table_name, [rows], reserved_kb, data_kb, index_size, unused_kb)
	EXEC(@dynamic);

	UPDATE [OCRGrowthStats].[dbo].[TableStats]
	SET  database_name = DB_Name()
		, CollectionDate = GetDATE()
	where database_name is null
	'
from ##DBName
EXEC(@dynamicScript)
