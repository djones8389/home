if OBJECT_ID('tempdb..##DBName') IS NOT NULL DROP TABLE ##DBName;

CREATE TABLE ##DBName (DBName sysname) 
INSERT ##DBName
SELECT DB_NAME();

if not exists (select 1 from sys.databases where name = 'OCRGrowthStats')

	CREATE DATABASE [OCRGrowthStats];
	GO

USE [OCRGrowthStats];

if not exists (select 1 from sys.tables where name = 'TableStats')
	CREATE TABLE [OCRGrowthStats].[dbo].[TableStats] (
		 database_name nvarchar(100) null
		, table_name nvarchar(100)
		, rows int
		, reserved_kb nvarchar(20)
		, data_kb nvarchar(20)
		, index_size nvarchar(20)
		, unused_kb nvarchar(20)
		, CollectionDate datetime null
	);
	GO


declare @dynamicScript nvarchar(MAX) = ''

SELECT @dynamicScript+=char(13) +
'
use '+quotename(DBName)+';

	declare @dynamic nvarchar(MAX) = '''';

	select @dynamic +=CHAR(13) +
		''exec sp_spaceused @objname = ''''''+s.name+''.''+t.name + '''''' ''
	from sys.tables T
	inner join sys.schemas S
	on s.schema_id = t.schema_id
	where type = ''u''
	order by t.name;

	INSERT [OCRGrowthStats].[dbo].[TableStats] (table_name, [rows], reserved_kb, data_kb, index_size, unused_kb)
	EXEC(@dynamic);

	UPDATE [OCRGrowthStats].[dbo].[TableStats]
	SET  database_name = DB_Name()
		, CollectionDate = GetDATE()
	where database_name is null
	'
from ##DBName
PRINT(@dynamicScript)




/*
	declare @dynamic nvarchar(MAX) = '';

	select @dynamic +=CHAR(13) +
		'exec sp_spaceused @objname = '''+s.name+'.'+t.name + ''' '
	from sys.tables T
	inner join sys.schemas S
	on s.schema_id = t.schema_id
	where type = 'u'
	order by t.name;

	INSERT [OCRGrowthStats].[dbo].[TableStats] (table_name, [rows], reserved_kb, data_kb, index_size, unused_kb)
	EXEC(@dynamic);

	UPDATE [OCRGrowthStats].[dbo].[TableStats]
	SET  database_name = DB_Name()
		, CollectionDate = GetDATE()
	where database_name is null
*/