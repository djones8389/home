use master 

exec sp_msforeachdb '

use [?];

if (DB_name() in (
	select name
	from sys.databases
	where user_access_desc = ''MULTI_USER''
		and state_desc = ''ONLINE''
		and database_id > 4
		)
	)

BEGIN

	IF EXISTS (
		SELECT 1
		FROM sys.database_principals
		WHERE NAME = ''Live Services''
	)


		DROP USER [Live Services]
	
END
'