USE [MASTER];
GO

IF NOT EXISTS (
	SELECT 1 
	FROM syslogins 
	WHERE [NAME] = 'PRE-PROD-SQL-1\Live Services'
	)

CREATE LOGIN [PRE-PROD-SQL-1\Live Services] FROM WINDOWS WITH DEFAULT_DATABASE=[master], DEFAULT_LANGUAGE=[us_english]
GO

GRANT CONNECT ANY DATABASE TO [pre-prod-sql-1\Live Services];
GRANT VIEW ANY DATABASE TO [pre-prod-sql-1\Live Services];
GRANT VIEW ANY DEFINITION TO [pre-prod-sql-1\Live Services];
GRANT VIEW SERVER STATE TO [pre-prod-sql-1\Live Services];
GRANT ALTER TRACE TO [pre-prod-sql-1\Live Services];


GRANT EXECUTE ON OBJECT::sp_whoisactive TO [pre-prod-sql-1\Live Services];  


USE [MSDB];

exec sp_addrolemember 'SQLAgentOperatorRole','pre-prod-sql-1\Live Services'
exec sp_addrolemember 'SQLAgentReaderRole','pre-prod-sql-1\Live Services'
exec sp_addrolemember 'SQLAgentUserRole','pre-prod-sql-1\Live Services'




IF EXISTS (
	select name
	from sysjobs
	where name = 'Access'
)

exec sp_delete_job  @job_name = 'Access'

/****** Object:  Job [Access]    Script Date: 24/04/2018 15:09:05 ******/
BEGIN TRANSACTION
DECLARE @ReturnCode INT
SELECT @ReturnCode = 0
/****** Object:  JobCategory [[Uncategorized (Local)]]    Script Date: 24/04/2018 15:09:05 ******/
IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'[Uncategorized (Local)]' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'[Uncategorized (Local)]'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=N'Access', 
		@enabled=1, 
		@notify_level_eventlog=0, 
		@notify_level_email=0, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description=N'No description available.', 
		@category_name=N'[Uncategorized (Local)]', 
		@owner_login_name=N'sa', @job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Access]    Script Date: 24/04/2018 15:09:05 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Access', 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'exec sp_msforeachdb ''

use [?];

if (DB_name() in (
	select name
	from sys.databases
	where user_access_desc = ''''MULTI_USER''''
		and state_desc = ''''ONLINE''''
		and database_id > 4)
	)

BEGIN

	IF NOT EXISTS (
		SELECT 1
		FROM sys.database_principals
		WHERE NAME = ''''pre-prod-sql-1\Live Services''''
	)
	BEGIN
		CREATE USER [pre-prod-sql-1\Live Services] FOR LOGIN [pre-prod-sql-1\Live Services]
		exec sp_addrolemember ''''db_datareader'''',''''pre-prod-sql-1\Live Services''''
		exec sp_addrolemember ''''db_datawriter'''',''''pre-prod-sql-1\Live Services''''

		GRANT VIEW DEFINITION ON SCHEMA::[dbo] TO [pre-prod-sql-1\Live Services]
		DENY DELETE TO [pre-prod-sql-1\Live Services]
		DENY INSERT TO [pre-prod-sql-1\Live Services]
	END

	IF NOT EXISTS (
		SELECT 1
		FROM sys.database_principals
		WHERE NAME = ''''PRE-PROD-SQL-1\Deployments''''
	)
	BEGIN
		CREATE USER [pre-prod-sql-1\Deployments] FOR LOGIN [pre-prod-sql-1\Deployments]
		exec sp_addrolemember ''''db_datareader'''',''''pre-prod-sql-1\Deployments''''
		exec sp_addrolemember ''''db_datawriter'''',''''pre-prod-sql-1\Deployments''''

		GRANT VIEW DEFINITION ON SCHEMA::[dbo] TO [pre-prod-sql-1\Deployments]
		DENY DELETE TO [pre-prod-sql-1\Deployments]
		DENY INSERT TO [pre-prod-sql-1\Deployments]
	END

END

''', 
		@database_name=N'master', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'Every 5 minutes', 
		@enabled=1, 
		@freq_type=4, 
		@freq_interval=1, 
		@freq_subday_type=4, 
		@freq_subday_interval=5, 
		@freq_relative_interval=0, 
		@freq_recurrence_factor=0, 
		@active_start_date=20180405, 
		@active_end_date=99991231, 
		@active_start_time=80000, 
		@active_end_time=210000, 
		@schedule_uid=N'20cf7a99-0dc0-46a0-9fdd-5161ac964b69'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:

GO


