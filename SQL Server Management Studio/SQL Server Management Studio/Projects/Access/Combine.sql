USE [MASTER];
GO

--select @@ServerName

IF NOT EXISTS (
	SELECT 1 
	FROM syslogins 
	WHERE [NAME] = 'welsh-sql-1\Live Services'
	)

CREATE LOGIN [welsh-sql-1\Live Services] 
FROM WINDOWS WITH DEFAULT_DATABASE=[master], DEFAULT_LANGUAGE=[us_english]
GO

IF NOT EXISTS (
	SELECT 1 
	FROM syslogins 
	WHERE [NAME] = 'welsh-sql-1\Deployments'
	)

CREATE LOGIN [welsh-sql-1\Deployments] 
FROM WINDOWS WITH DEFAULT_DATABASE=[master]
GO
ALTER SERVER ROLE [dbcreator] ADD MEMBER [welsh-sql-1\Deployments]
ALTER SERVER ROLE [processadmin] ADD MEMBER [welsh-sql-1\Deployments]
GO


GRANT CONNECT ANY DATABASE TO [welsh-sql-1\Live Services];
GRANT VIEW ANY DATABASE TO [welsh-sql-1\Live Services];
GRANT VIEW ANY DEFINITION TO [welsh-sql-1\Live Services];
GRANT VIEW SERVER STATE TO [welsh-sql-1\Live Services];
GRANT ALTER TRACE TO [welsh-sql-1\Live Services];

GRANT CONNECT ANY DATABASE TO [welsh-sql-1\Deployments];
GRANT VIEW ANY DATABASE TO [welsh-sql-1\Deployments];
GRANT VIEW ANY DEFINITION TO [welsh-sql-1\Deployments];
GRANT VIEW SERVER STATE TO [welsh-sql-1\Deployments];
GRANT ALTER TRACE TO [welsh-sql-1\Deployments];
GRANT ALTER ANY LOGIN TO [welsh-sql-1\Deployments];
GRANT ALTER ANY ROLE TO [welsh-sql-1\Deployments];

GRANT EXECUTE ON OBJECT::sp_whoisactive TO [welsh-sql-1\Live Services];  
GRANT EXECUTE ON OBJECT::sp_whoisactive TO [welsh-sql-1\Deployments];  

USE [MSDB];

exec sp_addrolemember 'SQLAgentOperatorRole','welsh-sql-1\Live Services'
exec sp_addrolemember 'SQLAgentReaderRole','welsh-sql-1\Live Services'
exec sp_addrolemember 'SQLAgentUserRole','welsh-sql-1\Live Services'

exec sp_addrolemember 'SQLAgentOperatorRole','welsh-sql-1\Deployments'
exec sp_addrolemember 'SQLAgentReaderRole','welsh-sql-1\Deployments'
exec sp_addrolemember 'SQLAgentUserRole','welsh-sql-1\Deployments'


IF EXISTS (
	select name
	from sysjobs
	where name = 'Access'
)

exec sp_delete_job @job_name = 'Access'

/****** Object:  Job [Access]    Script Date: 24/04/2018 15:09:05 ******/
BEGIN TRANSACTION
DECLARE @ReturnCode INT
SELECT @ReturnCode = 0
/****** Object:  JobCategory [[Uncategorized (Local)]]    Script Date: 24/04/2018 15:09:05 ******/
IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'[Uncategorized (Local)]' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'[Uncategorized (Local)]'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=N'Access', 
		@enabled=1, 
		@notify_level_eventlog=0, 
		@notify_level_email=0, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description=N'No description available.', 
		@category_name=N'[Uncategorized (Local)]', 
		@owner_login_name=N'sa', @job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Access]    Script Date: 24/04/2018 15:09:05 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Access', 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'DECLARE @DYNAMIC NVARCHAR(MAX) = ''''

select @DYNAMIC +=CHAR(13) + 
	''USE ''+quotename(name)+'' 
BEGIN

	IF NOT EXISTS (
		SELECT 1
		FROM sys.database_principals
		WHERE NAME = ''''welsh-sql-1\Live Services''''
	)
	BEGIN
		CREATE USER [welsh-sql-1\Live Services] FOR LOGIN [welsh-sql-1\Live Services]
	END
		exec sp_addrolemember ''''db_datareader'''',''''welsh-sql-1\Live Services''''
		exec sp_addrolemember ''''db_datawriter'''',''''welsh-sql-1\Live Services''''

		GRANT VIEW DEFINITION ON SCHEMA::[dbo] TO [welsh-sql-1\Live Services]
		DENY DELETE TO [welsh-sql-1\Live Services]
		DENY INSERT TO [welsh-sql-1\Live Services]
	
	IF NOT EXISTS (
		SELECT 1
		FROM sys.database_principals
		WHERE NAME = ''''welsh-sql-1\Deployments''''
	)
	BEGIN
		CREATE USER [welsh-sql-1\Deployments] FOR LOGIN [welsh-sql-1\Deployments]
	END
		exec sp_addrolemember ''''db_owner'''',''''welsh-sql-1\Deployments''''

		GRANT VIEW DEFINITION ON SCHEMA::[dbo] TO [welsh-sql-1\Deployments]
END''
from sys.databases
where user_access_desc = ''MULTI_USER''
	and state_desc = ''ONLINE''
	and database_id > 4
	and create_date > DATEADD(MINUTE, -5, getDATE())

EXEC(@DYNAMIC);', 
		@database_name=N'master', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'Every 5 minutes', 
		@enabled=1, 
		@freq_type=4, 
		@freq_interval=1, 
		@freq_subday_type=4, 
		@freq_subday_interval=5, 
		@freq_relative_interval=0, 
		@freq_recurrence_factor=0, 
		@active_start_date=20180405, 
		@active_end_date=99991231, 
		@active_start_time=70000, 
		@active_end_time=210000, 
		@schedule_uid=N'20cf7a99-0dc0-46a0-9fdd-5161ac964b69'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:

GO

exec sp_start_job @job_name = 'Access'