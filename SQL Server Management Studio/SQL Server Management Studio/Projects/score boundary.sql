use master
--drop table #resultHolder

create table #resultHolder (

	dbName sysname
	, qualificationName nvarchar(250)
	, assessmentName nvarchar(250)
	, examversionref nvarchar(250)
	, [Status] nvarchar(100)
)

INSERT #resultHolder
exec sp_MSforeachdb '

	use [?]

	if(''?'' like ''%ItemBank%'')

	BEGIN

	SET QUOTED_IDENTIFIER ON;

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	select  DB_NAME() [Client]
			,qt.QualificationName
			, at.AssessmentName
			, at.externalreference [examversionref]
			,aslt.Name [Status]
		from QualificationTable QT
		inner join AssessmentGroupTable agt
		on agt.QualificationID = qt.ID
		inner join AssessmentTable at
		on at.AssessmentGroupID = agt.id
		inner join AssessmentStatusLookupTable aslt
		on aslt.ID = at.AssessmentStatus
		where assessmentrules.exist(''PaperRules/Section[@PassLevelType="0" and @Fixed="0"]'') = 1;

	END


'

select *
from #resultHolder;



--select 
--	count(ID)
--from WAREHOUSE_ExamSessionTable (NOLOCK)
--where clientinformation is not null
--	and clientinformation.value('data(clientInformation/systemConfiguration/secureClient/Application)[1]','nvarchar(100)') IS NOT NULL
--	and clientinformation.value('data(clientInformation/systemConfiguration/secureClient/Application)[1]','nvarchar(100)') != 'not available'
