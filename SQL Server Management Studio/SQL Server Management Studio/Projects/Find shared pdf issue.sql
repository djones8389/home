SELECT distinct m.name, i.Id
  FROM [Demo_ContentAuthor].[dbo].[MediaItems] M
  left JOIN [Demo_ContentAuthor].[dbo].[SourceMaterials] SM
  on SM.MediaItemId = M.Id
  LEFT JOIN [Demo_ContentAuthor].[dbo].[Items] I
  on I.Id = Sm.ItemId
	 and I.SubjectId = M.SubjectId
  LEFT JOIN [Demo_ContentAuthor].[dbo].[Tools] T
  on T.MediaItemId = M.Id
where m.SubjectId = 125
	and (M.Type = 5 or m.RequireTool = 1)
	

	/*
		
	type = 0  requireTool = 0 = NOT shared
	type = 5 requireTool = 1 SHARED
		
	




SELECT  S.Title [Subject]
       , i.id [ItemID]
       , i.Name [ItemName]
       , I.Version [ContentAuthor Version]
       , IT.Version [TestCreation Version]
          , 'https://'+substring(DB_NAME(),0,charindex('_',DB_NAME()))+'.btlsurpass.com/#ItemAuthoring/Subject/'+cast(S.id as varchar(10))+'/Item/Edit/'+cast(i.id  as varchar(10))+ ''
FROM [Demo_ContentAuthor].[dbo].[Subjects] S
INNER JOIN [Demo_ContentAuthor].[dbo].[Items]  I
ON I.SubjectId = S.Id
INNER JOIN (
       SELECT ProjectID
              , ItemRef
              , MAX(Version) [Version]
       FROM Demo_ItemBank.[dbo].[ItemTable] 
       group by ProjectID
              , ItemRef
       ) IT
on IT.ProjectID = S.ProjectId
       and IT.ItemRef = I.id
where I.ID IN ('18201',
 '18205',
 '18209',
 '18211',
 '18436'
)

*/