DECLARE @DDM TABLE (
	DB sysname
	, table_name sysname	
	, table_schema sysname
	, column_name sysname
);

INSERT @DDM
EXEC sp_MSforeachdb '

use [?];

if(''?'' like ''%Performance2%'')

begin
	
	SELECT ''?''
		, A.table_name
		, A.table_schema
		, A.column_name 
	FROM INFORMATION_SCHEMA.COLUMNS A
	INNER JOIN INFORMATION_SCHEMA.TABLES B
	ON A.TABLE_NAME = B.TABLE_NAME
	WHERE COLUMN_name in (''FirstName'',''Forename'',''LastName'',''Surname'', ''email'',''documentName'')
		and B.TABLE_TYPE = ''BASE TABLE''
	order by 1,2,3,4
end

'
SELECT	
	'use '+QUOTENAME(DB)+'; ALTER TABLE '+QUOTENAME(table_schema) + '.' + QUOTENAME(table_name) + ' ALTER COLUMN '+QUOTENAME(column_name) + ' ADD MASKED WITH (FUNCTION = ''partial(2,"XXX",0)'');  '
	, *
FROM @DDM;

/*

use [Performance2_ItemBank]; ALTER TABLE [dbo].[PrintedAssessmentTable] ALTER COLUMN [DocumentName] ADD MASKED WITH (FUNCTION = 'partial(2,"XXX",0)');  
use [Performance2_ItemBank]; ALTER TABLE [dbo].[UserTable] ALTER COLUMN [Email] ADD MASKED WITH (FUNCTION = 'partial(2,"XXX",0)');  
use [Performance2_ItemBank]; ALTER TABLE [dbo].[UserTable] ALTER COLUMN [FirstName] ADD MASKED WITH (FUNCTION = 'partial(2,"XXX",0)');  
use [Performance2_ItemBank]; ALTER TABLE [dbo].[UserTable] ALTER COLUMN [LastName] ADD MASKED WITH (FUNCTION = 'partial(2,"XXX",0)');  
use [Performance2_SecureAssess]; ALTER TABLE [dbo].[ExamSessionDocumentTable] ALTER COLUMN [documentName] ADD MASKED WITH (FUNCTION = 'partial(2,"XXX",0)');  
use [Performance2_SecureAssess]; ALTER TABLE [dbo].[ExamSessionTable_Shredded] ALTER COLUMN [foreName] ADD MASKED WITH (FUNCTION = 'partial(2,"XXX",0)');  
use [Performance2_SecureAssess]; ALTER TABLE [dbo].[ExamSessionTable_Shredded] ALTER COLUMN [surName] ADD MASKED WITH (FUNCTION = 'partial(2,"XXX",0)');  
use [Performance2_SecureAssess]; ALTER TABLE [dbo].[UserTable] ALTER COLUMN [Email] ADD MASKED WITH (FUNCTION = 'partial(2,"XXX",0)');  
use [Performance2_SecureAssess]; ALTER TABLE [dbo].[UserTable] ALTER COLUMN [Forename] ADD MASKED WITH (FUNCTION = 'partial(2,"XXX",0)');  
use [Performance2_SecureAssess]; ALTER TABLE [dbo].[UserTable] ALTER COLUMN [Surname] ADD MASKED WITH (FUNCTION = 'partial(2,"XXX",0)');  
use [Performance2_SecureAssess]; ALTER TABLE [dbo].[WAREHOUSE_ExamSessionDocumentTable] ALTER COLUMN [documentName] ADD MASKED WITH (FUNCTION = 'partial(2,"XXX",0)');  
use [Performance2_SecureAssess]; ALTER TABLE [dbo].[WAREHOUSE_ExamSessionTable_Shreded] ALTER COLUMN [foreName] ADD MASKED WITH (FUNCTION = 'partial(2,"XXX",0)');  
use [Performance2_SecureAssess]; ALTER TABLE [dbo].[WAREHOUSE_ExamSessionTable_Shreded] ALTER COLUMN [surName] ADD MASKED WITH (FUNCTION = 'partial(2,"XXX",0)');  
use [Performance2_SecureAssess]; ALTER TABLE [dbo].[WAREHOUSE_UserTable] ALTER COLUMN [Email] ADD MASKED WITH (FUNCTION = 'partial(2,"XXX",0)');  
use [Performance2_SecureAssess]; ALTER TABLE [dbo].[WAREHOUSE_UserTable] ALTER COLUMN [Forename] ADD MASKED WITH (FUNCTION = 'partial(2,"XXX",0)');  
use [Performance2_SecureAssess]; ALTER TABLE [dbo].[WAREHOUSE_UserTable] ALTER COLUMN [Surname] ADD MASKED WITH (FUNCTION = 'partial(2,"XXX",0)');  
use [Performance2_ContentAuthor]; ALTER TABLE [dbo].[Users] ALTER COLUMN [Email] ADD MASKED WITH (FUNCTION = 'partial(2,"XXX",0)');  
use [Performance2_ContentAuthor]; ALTER TABLE [dbo].[Users] ALTER COLUMN [FirstName] ADD MASKED WITH (FUNCTION = 'partial(2,"XXX",0)');  
use [Performance2_ContentAuthor]; ALTER TABLE [dbo].[Users] ALTER COLUMN [LastName] ADD MASKED WITH (FUNCTION = 'partial(2,"XXX",0)');  
use [Performance2_SurpassManagement]; ALTER TABLE [dbo].[Users] ALTER COLUMN [Email] ADD MASKED WITH (FUNCTION = 'partial(2,"XXX",0)');  
use [Performance2_SurpassManagement]; ALTER TABLE [dbo].[Users] ALTER COLUMN [FirstName] ADD MASKED WITH (FUNCTION = 'partial(2,"XXX",0)');  
use [Performance2_SurpassManagement]; ALTER TABLE [dbo].[Users] ALTER COLUMN [LastName] ADD MASKED WITH (FUNCTION = 'partial(2,"XXX",0)');  
use [Performance2_SurpassDataWarehouse]; ALTER TABLE [dbo].[DimCandidate] ALTER COLUMN [Email] ADD MASKED WITH (FUNCTION = 'partial(2,"XXX",0)');  
use [Performance2_SurpassDataWarehouse]; ALTER TABLE [dbo].[DimCandidate] ALTER COLUMN [Forename] ADD MASKED WITH (FUNCTION = 'partial(2,"XXX",0)');  
use [Performance2_SurpassDataWarehouse]; ALTER TABLE [dbo].[DimCandidate] ALTER COLUMN [Surname] ADD MASKED WITH (FUNCTION = 'partial(2,"XXX",0)');  
use [Performance2_SurpassDataWarehouse]; ALTER TABLE [dbo].[DimContentUsers] ALTER COLUMN [Email] ADD MASKED WITH (FUNCTION = 'partial(2,"XXX",0)');  
use [Performance2_SurpassDataWarehouse]; ALTER TABLE [dbo].[DimContentUsers] ALTER COLUMN [FirstName] ADD MASKED WITH (FUNCTION = 'partial(2,"XXX",0)');  
use [Performance2_SurpassDataWarehouse]; ALTER TABLE [dbo].[DimContentUsers] ALTER COLUMN [LastName] ADD MASKED WITH (FUNCTION = 'partial(2,"XXX",0)');  
use [Performance2_SurpassDataWarehouse]; ALTER TABLE [ETL].[stg_DimContentUsers] ALTER COLUMN [Email] ADD MASKED WITH (FUNCTION = 'partial(2,"XXX",0)');  
use [Performance2_SurpassDataWarehouse]; ALTER TABLE [ETL].[stg_DimContentUsers] ALTER COLUMN [FirstName] ADD MASKED WITH (FUNCTION = 'partial(2,"XXX",0)');  
use [Performance2_SurpassDataWarehouse]; ALTER TABLE [ETL].[stg_DimContentUsers] ALTER COLUMN [LastName] ADD MASKED WITH (FUNCTION = 'partial(2,"XXX",0)');  
use [Performance2_AnalyticsManagement]; ALTER TABLE [dbo].[Users] ALTER COLUMN [Email] ADD MASKED WITH (FUNCTION = 'partial(2,"XXX",0)');  
use [Performance2_AnalyticsManagement]; ALTER TABLE [dbo].[Users] ALTER COLUMN [Forename] ADD MASKED WITH (FUNCTION = 'partial(2,"XXX",0)');  
use [Performance2_AnalyticsManagement]; ALTER TABLE [dbo].[Users] ALTER COLUMN [Surname] ADD MASKED WITH (FUNCTION = 'partial(2,"XXX",0)');  

*/