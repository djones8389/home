DECLARE @DDM TABLE (
	DB sysname
	, table_name sysname	
	, table_schema sysname
	, column_name sysname
);

INSERT @DDM
EXEC sp_MSforeachdb '

use [?];

if(''?'' like ''SANDBOX_Demo%'')

begin
	
	SELECT ''?''
		, A.table_name
		, A.table_schema
		, A.column_name 
	FROM INFORMATION_SCHEMA.COLUMNS A
	INNER JOIN INFORMATION_SCHEMA.TABLES B
	ON A.TABLE_NAME = B.TABLE_NAME
	WHERE COLUMN_name in (''FirstName'',''Forename'',''LastName'',''Surname'', ''email'',''documentName'')
		and B.TABLE_TYPE = ''BASE TABLE''
	order by 1,2,3,4
end

'
SELECT	
	'use '+QUOTENAME(DB)+'; ALTER TABLE '+QUOTENAME(table_schema) + '.' + QUOTENAME(table_name) + ' ALTER COLUMN '+QUOTENAME(column_name) + ' ADD MASKED WITH (FUNCTION = ''partial(2,"XXX",0)'');  '
	, 'use '+QUOTENAME(DB)+'; UPDATE '+QUOTENAME(table_schema) + '.' + QUOTENAME(table_name) + ' SET '+QUOTENAME(column_name) + ' = (SELECT HASHBYTES (''MD5'','+column_name+')) '
	, *
FROM @DDM;



/*
use [SANDBOX_Demo_AnalyticsManagement]; ALTER TABLE [dbo].[Users] ALTER COLUMN [Email] ADD MASKED WITH (FUNCTION = 'partial(2,"XXX",0)');  
use [SANDBOX_Demo_AnalyticsManagement]; ALTER TABLE [dbo].[Users] ALTER COLUMN [Forename] ADD MASKED WITH (FUNCTION = 'partial(2,"XXX",0)');  
use [SANDBOX_Demo_AnalyticsManagement]; ALTER TABLE [dbo].[Users] ALTER COLUMN [Surname] ADD MASKED WITH (FUNCTION = 'partial(2,"XXX",0)');  

use [SANDBOX_Demo_AnalyticsManagement]; UPDATE [dbo].[Users] SET [Email] = (SELECT HASHBYTES ('MD5',Email)) 
use [SANDBOX_Demo_AnalyticsManagement]; UPDATE [dbo].[Users] SET [Forename] = (SELECT HASHBYTES ('MD5',Forename)) 
use [SANDBOX_Demo_AnalyticsManagement]; UPDATE [dbo].[Users] SET [Surname] = (SELECT HASHBYTES ('MD5',Surname)) 


--3 seconds

use [SANDBOX_Demo_AnalyticsManagement]; UPDATE [dbo].[Users] SET [Email] = (SELECT HASHBYTES ('MD5',Email)) 
use [SANDBOX_Demo_AnalyticsManagement]; UPDATE [dbo].[Users] SET [Forename] = (SELECT HASHBYTES ('MD5',Forename)) 
use [SANDBOX_Demo_AnalyticsManagement]; UPDATE [dbo].[Users] SET [Surname] = (SELECT HASHBYTES ('MD5',Surname)) 
use [SANDBOX_Demo_ContentAuthor]; UPDATE [dbo].[Users] SET [Email] = (SELECT HASHBYTES ('MD5',Email)) 
use [SANDBOX_Demo_ContentAuthor]; UPDATE [dbo].[Users] SET [FirstName] = (SELECT HASHBYTES ('MD5',FirstName)) 
use [SANDBOX_Demo_ContentAuthor]; UPDATE [dbo].[Users] SET [LastName] = (SELECT HASHBYTES ('MD5',LastName)) 
use [SANDBOX_Demo_ItemBank]; UPDATE [dbo].[PrintedAssessmentTable] SET [DocumentName] = (SELECT HASHBYTES ('MD5',DocumentName)) 
use [SANDBOX_Demo_ItemBank]; UPDATE [dbo].[UserTable] SET [Email] = (SELECT HASHBYTES ('MD5',Email)) 
use [SANDBOX_Demo_ItemBank]; UPDATE [dbo].[UserTable] SET [FirstName] = (SELECT HASHBYTES ('MD5',FirstName)) 
use [SANDBOX_Demo_ItemBank]; UPDATE [dbo].[UserTable] SET [LastName] = (SELECT HASHBYTES ('MD5',LastName)) 
use [SANDBOX_Demo_SecureAssess]; UPDATE [dbo].[ExamSessionDocumentTable] SET [documentName] = (SELECT HASHBYTES ('MD5',documentName)) 
use [SANDBOX_Demo_SecureAssess]; UPDATE [dbo].[ExamSessionTable_Shredded] SET [foreName] = (SELECT HASHBYTES ('MD5',foreName)) 
use [SANDBOX_Demo_SecureAssess]; UPDATE [dbo].[ExamSessionTable_Shredded] SET [surName] = (SELECT HASHBYTES ('MD5',surName)) 
use [SANDBOX_Demo_SecureAssess]; UPDATE [dbo].[UserTable] SET [Email] = (SELECT HASHBYTES ('MD5',Email)) 
use [SANDBOX_Demo_SecureAssess]; UPDATE [dbo].[UserTable] SET [Forename] = (SELECT HASHBYTES ('MD5',Forename)) 
use [SANDBOX_Demo_SecureAssess]; UPDATE [dbo].[UserTable] SET [Surname] = (SELECT HASHBYTES ('MD5',Surname)) 
use [SANDBOX_Demo_SecureAssess]; UPDATE [dbo].[WAREHOUSE_ExamSessionDocumentTable] SET [documentName] = (SELECT HASHBYTES ('MD5',documentName)) 
use [SANDBOX_Demo_SecureAssess]; UPDATE [dbo].[WAREHOUSE_ExamSessionTable_Shreded] SET [foreName] = (SELECT HASHBYTES ('MD5',foreName)) 
use [SANDBOX_Demo_SecureAssess]; UPDATE [dbo].[WAREHOUSE_ExamSessionTable_Shreded] SET [surName] = (SELECT HASHBYTES ('MD5',surName)) 
use [SANDBOX_Demo_SecureAssess]; UPDATE [dbo].[WAREHOUSE_UserTable] SET [Email] = (SELECT HASHBYTES ('MD5',Email)) 
use [SANDBOX_Demo_SecureAssess]; UPDATE [dbo].[WAREHOUSE_UserTable] SET [Forename] = (SELECT HASHBYTES ('MD5',Forename)) 
use [SANDBOX_Demo_SecureAssess]; UPDATE [dbo].[WAREHOUSE_UserTable] SET [Surname] = (SELECT HASHBYTES ('MD5',Surname)) 
use [SANDBOX_Demo_SecureMarker]; UPDATE [dbo].[AssignedGroupMarks] SET [Surname] = (SELECT HASHBYTES ('MD5',Surname)) 
use [SANDBOX_Demo_SecureMarker]; UPDATE [dbo].[Users] SET [Email] = (SELECT HASHBYTES ('MD5',Email)) 
use [SANDBOX_Demo_SecureMarker]; UPDATE [dbo].[Users] SET [Forename] = (SELECT HASHBYTES ('MD5',Forename)) 
use [SANDBOX_Demo_SecureMarker]; UPDATE [dbo].[Users] SET [Surname] = (SELECT HASHBYTES ('MD5',Surname)) 
use [SANDBOX_Demo_SurpassDataWarehouse]; UPDATE [dbo].[DimCandidate] SET [Email] = (SELECT HASHBYTES ('MD5',Email)) 
use [SANDBOX_Demo_SurpassDataWarehouse]; UPDATE [dbo].[DimCandidate] SET [Forename] = (SELECT HASHBYTES ('MD5',Forename)) 
use [SANDBOX_Demo_SurpassDataWarehouse]; UPDATE [dbo].[DimCandidate] SET [Surname] = (SELECT HASHBYTES ('MD5',Surname)) 
use [SANDBOX_Demo_SurpassDataWarehouse]; UPDATE [dbo].[DimContentUsers] SET [Email] = (SELECT HASHBYTES ('MD5',Email)) 
use [SANDBOX_Demo_SurpassDataWarehouse]; UPDATE [dbo].[DimContentUsers] SET [FirstName] = (SELECT HASHBYTES ('MD5',FirstName)) 
use [SANDBOX_Demo_SurpassDataWarehouse]; UPDATE [dbo].[DimContentUsers] SET [LastName] = (SELECT HASHBYTES ('MD5',LastName)) 
use [SANDBOX_Demo_SurpassDataWarehouse]; UPDATE [ETL].[stg_DimContentUsers] SET [Email] = (SELECT HASHBYTES ('MD5',Email)) 
use [SANDBOX_Demo_SurpassDataWarehouse]; UPDATE [ETL].[stg_DimContentUsers] SET [FirstName] = (SELECT HASHBYTES ('MD5',FirstName)) 
use [SANDBOX_Demo_SurpassDataWarehouse]; UPDATE [ETL].[stg_DimContentUsers] SET [LastName] = (SELECT HASHBYTES ('MD5',LastName)) 
use [SANDBOX_Demo_SurpassManagement]; UPDATE [dbo].[Users] SET [Email] = (SELECT HASHBYTES ('MD5',Email)) 
use [SANDBOX_Demo_SurpassManagement]; UPDATE [dbo].[Users] SET [FirstName] = (SELECT HASHBYTES ('MD5',FirstName)) 
use [SANDBOX_Demo_SurpassManagement]; UPDATE [dbo].[Users] SET [LastName] = (SELECT HASHBYTES ('MD5',LastName)) 

*/