if OBJECT_ID('tempdb..#MyTest') IS NOT NULL DROP TABLE #MyTest;

--CURSOR--

SELECT ID
		,  AssessmentID
		,  Assessment
into #MyTest
FROM AssessmentAuditTable NOLOCK

--select * FROM #MyTest

DECLARE AssessmentAudit CURSOR FOR
SELECT	
		ID
		,  AssessmentID
		,  Assessment
FROm #MyTest

DECLARE @ID int, @AssessmentID INT,  @Assessment XML

OPEN AssessmentAudit 

FETCH NEXT FROM AssessmentAudit INTO @ID, @AssessmentID,  @Assessment

while @@FETCH_STATUS = 0

BEGIN

	UPDATE #MyTest
	set Assessment.modify('replace value of (Assessment/ID/text())[1] with sql:variable("@AssessmentID")')
	where ID = @ID;

	FETCH NEXT FROM AssessmentAudit INTO @ID, @AssessmentID,  @Assessment

END


CLOSE AssessmentAudit 
DEALLOCATE AssessmentAudit 

--select * FROM #MyTest

--select top 1 *
--from #MyTest