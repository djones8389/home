IF OBJECT_ID('tempdb..##DATALENGTH') IS NOT NULL DROP TABLE ##DATALENGTH;

CREATE TABLE ##DATALENGTH (
	client nvarchar(1000)
	, SP sysname
);

DECLARE @dynamic nvarchar(MAX)='';

select @dynamic +=CHAR(13)+ '
use '+QUOTENAME([NAME])+'

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

INSERT ##DATALENGTH
	select db_name()
		,  so.name
	from syscomments sc 
	inner join sysobjects so
	on so.id = sc.id
	where sc.text like ''%collate%''
'
from sys.databases
where state_desc = 'ONLINE'
	

exec(@dynamic);



select *
from ##DATALENGTH
order by 1






