--Declare a DB here:
	use Windesheim_AnalyticsManagement



DECLARE @ClientName nvarchar(20) = (select SUBSTRING(db_name(),0,charindex('_',db_name())))

declare @applications table (
	[application] nvarchar(20)
)
insert @applications
values('AnalyticsManagement'),('ContentAuthor'),('ItemBank'),('SecureAssess'),('SurpassDataWarehouse'),('SurpassManagement') --('SecureMarker'),


select 	' IF EXISTS (select 1 from sys.databases where name = '''+@ClientName+'_'+[application]+''') ' 
		 + 'use ' + QUOTENAME(@ClientName+'_'+[application]) + '
			  BEGIN

				exec sp_addrolemember ''db_datareader'','''+@ClientName+'_ETLUser'+'''

			  END
		'		
from @applications
where application not in ('AnalyticsManagement','SecureMarker')  --('ContentAuthor','ItemBank','SecureAssess','SurpassDataWarehouse','SecureMarker')

--GRANT EXECUTE ON SCHEMA :: dbo TO '+@ClientName+'_ETLUser;  

union

select ' IF EXISTS (select 1 from sys.databases where name = '''+@ClientName+'_'+[application]+''') ' 
	 + 'use ' + QUOTENAME(@ClientName+'_'+[application]) + '
			  BEGIN
					exec sp_addrolemember ''db_datareader'','''+@ClientName+'_'+[application]+'User'+'''
					GRANT EXECUTE ON SCHEMA :: dbo TO '+quotename(@ClientName+'_'+[application]+'User')+'
			  END
			  '		
from @applications

where application =  'AnalyticsManagement'

union

select ' IF EXISTS (select 1 from sys.databases where name = '''+@ClientName+'_'+[application]+''') ' 
	 + 'use ' + QUOTENAME(@ClientName+'_'+[application]) + '
			  BEGIN
					exec sp_addrolemember ''db_datareader'','''+@ClientName+'_'+[application]+'User'+'''
					GRANT EXECUTE ON SCHEMA :: dbo TO '+quotename(@ClientName+'_'+[application]+'User')+'				
			  END
			  '		
from @applications

where application in ('ContentAuthor','ItemBank')

union

select ' IF EXISTS (select 1 from sys.databases where name = '''+@ClientName+'_'+[application]+''') ' 
	 + 'use ' + QUOTENAME(@ClientName+'_'+[application]) + '
			  BEGIN
							
				GRANT EXECUTE ON SCHEMA :: dbo TO '+quotename(@ClientName+'_'+[application]+'User')+'
				GRANT SELECT ON SCHEMA :: dbo TO '+quotename(@ClientName+'_'+[application]+'User')+'
				GRANT VIEW DEFINITION ON XML SCHEMA COLLECTION::[dbo].[itemResponseXmlXsd] TO '+quotename(@ClientName+'_'+[application]+'User')+'
				GRANT VIEW DEFINITION ON XML SCHEMA COLLECTION::[dbo].[structureXmlXsd] TO '+quotename(@ClientName+'_'+[application]+'User')+'

			  END
			  '		
from @applications
where application =  'SecureAssess'

union

select ' IF EXISTS (select 1 from sys.databases where name = '''+@ClientName+'_'+[application]+''') ' 
	 + 'use ' + QUOTENAME(@ClientName+'_'+[application]) + '
			  BEGIN

				GRANT EXECUTE ON SCHEMA :: ETL TO '+@ClientName+'_ETLUser;  
				GRANT ALTER ON SCHEMA :: ETL TO '+@ClientName+'_ETLUser;  
				GRANT ALTER ON SCHEMA :: dbo TO '+@ClientName+'_ETLUser;  
				exec sp_addrolemember ''db_datawriter'','''+@ClientName+'_ETLUser'+'''

			  END
			  '		
from @applications								--may need datawriter?
where application =  'SurpassDataWarehouse'

union

select ' IF EXISTS (select 1 from sys.databases where name = '''+@ClientName+'_'+[application]+''') ' 
	 + 'use ' + QUOTENAME(@ClientName+'_'+[application]) + '
			  BEGIN
					GRANT SELECT ON SCHEMA :: dbo TO '+quotename(@ClientName+'_'+[application]+'User')+'
					exec sp_addrolemember ''db_datawriter'','''+@ClientName+'_'+[application]+'User'+'''
			  END
			  '		
from @applications

where application =  'SurpassManagement'




 