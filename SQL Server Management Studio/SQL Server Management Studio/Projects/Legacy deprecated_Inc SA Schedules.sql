IF OBJECT_ID('tempdb..##Deprecated') IS NOT NULL DROP TABLE ##Deprecated;

CREATE TABLE ##Deprecated (

	client sysname
	, examSessionID int
	, Keycode nvarchar(10)
	, examState tinyint
	, examStateLookup  nvarchar(200)
	, forename nvarchar(200)
	, surname nvarchar(200)
	, candidateRef nvarchar(200)
	, centreName nvarchar(200)
	, qualificationName nvarchar(200)
	, examName nvarchar(200)
	, SecureAssessStyleProfileID smallint
	, ItemBankStyleProfileID smallint
	, ItemBankStyleProfileVersion smallint
);


declare @dynamic nvarchar(max)='';

SELECT @dynamic += CHAR(13) + '

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

SELECT '''+name+'''
	,EST.ID
	,EST.KeyCode
	,EST.examState
	,ESLT.StateName
	,UT.Forename
	,UT.Surname
	,UT.CandidateRef
	,CT.CentreName
	,IB3.QualificationName
	,SCET.ExamName
	,est.SecureAssessStyleProfileID
	,est.ItemBankStyleProfileID
	,est.ItemBankStyleProfileVersion

FROM ['+name+'_SecureAssess].dbo.ExamSessionTable AS EST

	INNER JOIN ['+name+'_SecureAssess].dbo.ScheduledExamsTable as SCET	on SCET.ID = EST.ScheduledExamID   
	INNER JOIN ['+name+'_SecureAssess].dbo.UserTable as UT on UT.ID = EST.UserID
	INNER JOIN ['+name+'_SecureAssess].dbo.CentreTable as CT on CT.ID = SCET.CentreID
	INNER JOIN ['+name+'_SecureAssess].dbo.IB3QualificationLookup IB3 on IB3.ID = qualificationID
	INNER JOIN ['+name+'_SecureAssess].dbo.ExamStateLookupTable ESLT on ESLT.ID = examState
	INNER JOIN (
		select 
			db_NAME() [Client]
			,QT.[QualificationName]
			, AGT.name [Assessment]	
		from ['+name+'_ItemBank].dbo.AssessmentGroupTable AGT 
		INNER JOIN ['+name+'_ItemBank].dbo.QualificationTable QT
		on qt.ID = agt.QualificationID
		INNER JOIN ['+name+'_ItemBank].dbo.[QualificationStatusLookupTable] QL
		on QL.ID = QT.QualificationStatus
		INNER JOIN ['+name+'_ItemBank].dbo.AssessmentStatusLookupTable AST
		on AST.ID = AGT.[Status]
		where StyleProfileVersion is null
	) IB
ON IB.QualificationName = IB3.QualificationName
	and IB.[Assessment] = scet.examName
where examState not in (15,16,13)
'
FROM  (
	select distinct B.name
	FROM (
		select SUBSTRING(name, 0, charindex('_',name)) [Name]
			, ROW_NUMBER() OVER(PARTITION BY SUBSTRING(name, 0, charindex('_',name)) ORDER BY SUBSTRING(name, 0, charindex('_',name)) ) R
		from sys.databases
		where state_desc = 'ONLINE'
			and [Name] like '%/_%' ESCAPE '/'
			and name not like 'test%'
		) B
	where R > 3
) a

INSERT ##Deprecated
EXEC(@dynamic)


SELECT *
FROM ##Deprecated
order by 1;