use master

CREATE DATABASE [2.10.0.15_SecureMarker];
GO

USE [2.10.0.15_SecureMarker]
GO

/****** Object:  SqlAssembly [SqlClassLibrary]    Script Date: 30/08/2017 08:26:37 ******/
CREATE ASSEMBLY [SqlClassLibrary]
FROM 0x4D5A90000300000004000000FFFF0000B800000000000000400000000000000000000000000000000000000000000000000000000000000000000000800000000E1FBA0E00B409CD21B8014CCD21546869732070726F6772616D2063616E6E6F742062652072756E20696E20444F53206D6F64652E0D0D0A2400000000000000504500004C010300888E81550000000000000000E00002210B010B00001000000006000000000000FE2E0000002000000040000000000010002000000002000004000000000000000400000000000000008000000002000000000000030040850000100000100000000010000010000000000000100000000000000000000000A82E00005300000000400000C002000000000000000000000000000000000000006000000C000000702D00001C0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000200000080000000000000000000000082000004800000000000000000000002E74657874000000040F0000002000000010000000020000000000000000000000000000200000602E72737263000000C0020000004000000004000000120000000000000000000000000000400000402E72656C6F6300000C0000000060000000020000001600000000000000000000000000004000004200000000000000000000000000000000E02E0000000000004800000002000500242400004C0900000100000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000133001000B00000001000011027B040000040A2B00062A2202037D040000042AAE0002730B00000A7D01000004027E0C00000A7D03000004021728020000060002730D00000A7D020000042A13300300C200000001000011000F03280E00000A2C17027B020000040F01280F00000A6F1000000A16FE012B0117000A062D0538950000000F02281100000A16FE010F02280F00000A6F1200000A16FE025F16FE010A062D3900020F02280F00000A7D03000004027B010000046F1300000A16FE0216FE010A062D1500027B010000040F02280F00000A6F1400000A260000027B010000040F01280F00000A6F1400000A26027B020000040F01280F00000A6F1500000A000F01281100000A0A062D0A000216280200000600002A0000133003005500000002000011000F017B010000046F1600000A0A027B010000046F1300000A16FE020F017B010000046F1300000A16FE025F16FE010B072D1400027B01000004027B030000046F1400000A2600027B01000004066F1400000A262A00000013300100160000000300001100027B010000046F1600000A731700000A0A2B00062A00001330020039000000010000110002036F1800000A7D0300000402036F1800000A731900000A7D01000004027B010000046F1300000A16FE010A062D0802162802000006002A860003027B030000046F1A00000A0003027B010000046F1600000A6F1A00000A002A00133002001300000001000011000203280A00000615FE0116FE010A2B00062A00133004001A0100000400001100022C090314FE0116FE012B011600130611062D06731B00000A7A036F1200000A16FE0116FE01130611062D0816130538E2000000036F1200000A17FE0116FE01130611062D460003166F1C00000A0A160B2B1F02076F1D00000A06FE0116FE01130611062D0807130538A80000000717580B07026F1300000AFE0116FE01130611062DCF151305388A000000160C160D03280B00000613042B660003096F1C00000A020809586F1D00000AFE0116FE01130611062D2E0009036F1200000A1759FE0116FE01130611062D1208036F1200000A2E03082B01150013052B390917580D002B1B0008095811040994590C11040994153003162B0411040994000D0000080958026F1300000AFE04130611062D891513052B0011052A000013300400710000000500001100026F1200000A8D170000010A180B160C0616159E0617169E2B44020717596F1C00000A02086F1C00000AFE0116FE01130411042D0E06072517580B081758250C9E2B1B0816FE0216FE01130411042D060608940C2B0806072517580B169E07068E69FE04130411042DB0060D2B00092A00000042534A4201000100000000000C00000076322E302E35303732370000000005006C0000008C030000237E0000F80300002404000023537472696E6773000000001C080000080000002355530024080000100000002347554944000000340800001801000023426C6F6200000000000000020000015717A20B0900000000FA253300160000010000001700000003000000040000000B0000000C000000010000001D0000000B000000050000000100000001000000020000000200000001000000010000000300000000000A0001000000000006005C0055000A008D00720006009E0055000600B100A5000600E700CC000A0031011C010A003B011C0106006B0161010600AE01610106007B0268022B008F0200000600C402A4020600E402A4020E000E03A4020600210355000A00370372000A005803720006007E035F03060094035F0306009F03A4020600BA0355000600FB03550006001B0455000000000001000000000001000100092110001E0000000500010001008101100025003C000D00050009000100BF000A000100EE000E000100F60015000100F3014600502000000000860801011800010067200000000081080C011C00010070200000000086001701210002009C200000000086004601250002006C2100000000860051012F000500D021000000008600570135000600F42100000000E10178013A000600392200000000E101BB01400007005C2200000000960012024D0008007C220000000096001B0254000A00A42300000000910023025B000C00000001002C02000001002C02000002003202000003003C02000001004702000001004D02000001004F02000001005102000002005A02000001005102000002005A02000001006102020009001100A9013A001100ED01400051009E02610061009E02670069009E02210071009E02210079009E02210081009E026C0091009E028D00A1009E02210021009E022100A900C10315000C009E0221003900C70318003100C703A2000C001202A600310001011800A900D103AC002100D103AC002100DC03B0000C00E303B6001900E703A20031009E02C1004100F003A20021009E02C1004900ED01C100B1009E022100A9001104CB0021001104CB002000530093002E002B00F7002E002300EE002E001B00E5002E00330093004000530093004300430072006300330093008100530093002001330093004001330093009800BC00C600D000DB000200010000000B02490002000100030001000200030002000E0003000200100005009C000480000000000000000000000000000000003C00000002000000000000000000000001004C00000000000200000000000000000000000100660000000000030005000000000000000000010002030000000000000000003C4D6F64756C653E0053716C436C6173734C6962726172792E646C6C00436F6E63617400537472696E674275696C646572536561726368696E670053716C436C6173734C696272617279006D73636F726C69620053797374656D0056616C7565547970650053797374656D2E44617461004D6963726F736F66742E53716C5365727665722E536572766572004942696E61727953657269616C697A65004F626A6563740053797374656D2E5465787400537472696E674275696C646572005F616363756D756C61746F720053797374656D2E436F6C6C656374696F6E732E47656E65726963004C6973746031005F76616C756573005F64656C696D69746572006765745F49734E756C6C007365745F49734E756C6C00496E69740053797374656D2E446174612E53716C54797065730053716C537472696E670053716C426F6F6C65616E00416363756D756C617465004D65726765005465726D696E6174650053797374656D2E494F0042696E617279526561646572004D6963726F736F66742E53716C5365727665722E5365727665722E4942696E61727953657269616C697A652E5265616400526561640042696E617279577269746572004D6963726F736F66742E53716C5365727665722E5365727665722E4942696E61727953657269616C697A652E5772697465005772697465003C49734E756C6C3E6B5F5F4261636B696E674669656C640049734E756C6C00436F6E7461696E7300496E6465784F66004B4D505461626C650076616C75650064656C696D69746572006F6E6C79556E697175650067726F75700072007700686179737461636B006E6565646C6500736F756768740053797374656D2E446961676E6F73746963730044656275676761626C6541747472696275746500446562756767696E674D6F646573002E63746F720053797374656D2E52756E74696D652E436F6D70696C6572536572766963657300436F6D70696C6174696F6E52656C61786174696F6E734174747269627574650052756E74696D65436F6D7061746962696C6974794174747269627574650053797374656D2E436F726500457874656E73696F6E4174747269627574650053657269616C697A61626C654174747269627574650053716C55736572446566696E656441676772656761746541747472696275746500466F726D61740053797374656D2E52756E74696D652E496E7465726F705365727669636573005374727563744C61796F7574417474726962757465004C61796F75744B696E6400436F6D70696C657247656E65726174656441747472696275746500537472696E6700456D707479006765745F56616C7565006765745F4C656E67746800417070656E640041646400546F537472696E670052656164537472696E6700417267756D656E744E756C6C457863657074696F6E006765745F436861727300496E743332000000000003200000000000E25A8201C811E44F8DA379ACB83850150008B77A5C561934E089030612110606151215010E02060E032000020420010102032000010920030111191119111D0520010111080420001119052001011221052001011225020602032800020600020212110E0600020812110E0500011D080E05200101112D04200101080520010111451A010002000000010054080B4D61784279746553697A65FFFFFFFF05200101114D04010000000307010205151215010E0320000E0520010213000320000805200112110E0520010113000407020E02042001010E040701111904200103080A0707030808081D0808020907051D0808081D08020801000701000000000801000800000000001E01000100540216577261704E6F6E457863657074696F6E5468726F777301000000000000888E815500000000020000001C0100008C2D00008C0F000052534453213576B56CB9734995FE485CACEF172601000000633A5C55736572735C4D6178696D2E4572616368696E5C446F63756D656E74735C56697375616C2053747564696F20323031335C50726F6A656374735C4461746162617365315C4461746162617365315C6F626A5C44656275675C53716C436C6173734C6962726172792E706462000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000D02E00000000000000000000EE2E0000002000000000000000000000000000000000000000000000E02E000000000000000000000000000000005F436F72446C6C4D61696E006D73636F7265652E646C6C0000000000FF250020001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000100100000001800008000000000000000000000000000000100010000003000008000000000000000000000000000000100000000004800000058400000640200000000000000000000640234000000560053005F00560045005200530049004F004E005F0049004E0046004F0000000000BD04EFFE00000100000000000000000000000000000000003F000000000000000400000002000000000000000000000000000000440000000100560061007200460069006C00650049006E0066006F00000000002400040000005400720061006E0073006C006100740069006F006E00000000000000B004C4010000010053007400720069006E006700460069006C00650049006E0066006F000000A001000001003000300030003000300034006200300000002C0002000100460069006C0065004400650073006300720069007000740069006F006E000000000020000000300008000100460069006C006500560065007200730069006F006E000000000030002E0030002E0030002E003000000048001400010049006E007400650072006E0061006C004E0061006D0065000000530071006C0043006C006100730073004C006900620072006100720079002E0064006C006C0000002800020001004C006500670061006C0043006F0070007900720069006700680074000000200000005000140001004F0072006900670069006E0061006C00460069006C0065006E0061006D0065000000530071006C0043006C006100730073004C006900620072006100720079002E0064006C006C000000340008000100500072006F006400750063007400560065007200730069006F006E00000030002E0030002E0030002E003000000038000800010041007300730065006D0062006C0079002000560065007200730069006F006E00000030002E0030002E0030002E0030000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000002000000C000000003F00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
WITH PERMISSION_SET = SAFE
GO
/****** Object:  UserDefinedTableType [dbo].[CandidateMarksType]    Script Date: 30/08/2017 08:26:37 ******/
CREATE TYPE [dbo].[CandidateMarksType] AS TABLE(
	[UniqueResponseId] [int] NULL,
	[Mark] [decimal](18, 10) NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[UniqueResponsesType1]    Script Date: 30/08/2017 08:26:37 ******/
CREATE TYPE [dbo].[UniqueResponsesType1] AS TABLE(
	[ID] [bigint] NULL,
	[Mark] [decimal](18, 10) NULL,
	[Annotations] [xml] NULL,
	[MarkedMetadata] [xml] NULL,
	[MarkingDeviation] [decimal](18, 10) NULL
)
GO
/****** Object:  UserDefinedAggregate [dbo].[ag_CLR_Concat]    Script Date: 30/08/2017 08:26:37 ******/
CREATE AGGREGATE [dbo].[ag_CLR_Concat]
(@string [nvarchar](max), @delimiter [nvarchar](100), @onlyUniq [bit])
RETURNS[nvarchar](max)
EXTERNAL NAME [SqlClassLibrary].[Concat]
GO
/****** Object:  UserDefinedFunction [dbo].[ParamsToList]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[ParamsToList] (@Parameters varchar(max))
returns @result TABLE (Value varchar(max))
AS  
begin
     DECLARE @TempList table
          (
          Value varchar(max)
          )

     DECLARE @Value varchar(max), @Pos int

     SET @Parameters = LTRIM(RTRIM(@Parameters))+ ','
     SET @Pos = CHARINDEX(',', @Parameters, 1)

     IF REPLACE(@Parameters, ',', '') <> ''
     BEGIN
          WHILE @Pos > 0
          BEGIN
               SET @Value = LTRIM(RTRIM(LEFT(@Parameters, @Pos - 1)))
               IF @Value <> ''
               BEGIN
                    INSERT INTO @TempList (Value) VALUES (@Value) --Use Appropriate conversion
               END
               SET @Parameters = RIGHT(@Parameters, LEN(@Parameters) - @Pos)
               SET @Pos = CHARINDEX(',', @Parameters, 1)

          END
     END    
     INSERT @result
     SELECT value
        FROM @TempList
     RETURN
END
GO
/****** Object:  UserDefinedFunction [dbo].[ParamsToThreeColumnTable]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[ParamsToThreeColumnTable] (@Parameters varchar(max))
returns @result TABLE (Value1 varchar(max), Value2 varchar(max), Value3 varchar(max))
AS  
begin
     DECLARE @TempList table
          (
          Value1 varchar(max),
          Value2 varchar(max),
          Value3 varchar(max)
          )

     DECLARE @Value1 varchar(max)
     DECLARE @Value2 varchar(max)
     DECLARE @Value3 varchar(max)
     
     DECLARE @Pos int

     SET @Parameters = LTRIM(RTRIM(@Parameters))+ ','
     SET @Pos = CHARINDEX(',', @Parameters, 1)

     IF REPLACE(@Parameters, ',', '') <> ''
     BEGIN
          WHILE @Pos > 0
          BEGIN
               SET @Value1 = LTRIM(RTRIM(LEFT(@Parameters, @Pos - 1)))
               SET @Parameters = RIGHT(@Parameters, LEN(@Parameters) - @Pos)
               SET @Pos = CHARINDEX(',', @Parameters, 1)
               SET @Value2 = LTRIM(RTRIM(LEFT(@Parameters, @Pos - 1)))
               SET @Parameters = RIGHT(@Parameters, LEN(@Parameters) - @Pos)
               SET @Pos = CHARINDEX(',', @Parameters, 1)
               SET @Value3 = LTRIM(RTRIM(LEFT(@Parameters, @Pos - 1)))
               SET @Parameters = RIGHT(@Parameters, LEN(@Parameters) - @Pos)
               SET @Pos = CHARINDEX(',', @Parameters, 1)
               IF (@Value1 <> '' AND @Value2 <> '' AND @Value3 <> '')
               BEGIN
                    INSERT INTO @TempList (Value1, Value2, Value3) VALUES (@Value1, @Value2, @Value3) --Use Appropriate conversion
               END  
               
          END
     END    
     INSERT @result
     SELECT Value1, Value2, Value3
        FROM @TempList
     RETURN
END
GO
/****** Object:  UserDefinedFunction [dbo].[sm_checkGroupAndItemsMarkedInTolerance_fn]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Anton Burdyugov
-- Create date: 28/06/2013
-- Description:	Check all items in some group marked in tolerance
-- =============================================
CREATE FUNCTION [dbo].[sm_checkGroupAndItemsMarkedInTolerance_fn]
(
	-- Add the parameters for the function here
	@assignedGroupMarkId     INT
   ,@uniqueGroupResponse     INT
   ,@groupDeviation          DECIMAL(18,10)
   ,@groupTolerance          DECIMAL(18,10)
   ,@isAutoGroup			 BIT = NULL 
)
RETURNS BIT
AS
BEGIN
    -- Declare the return variable here
    DECLARE @myCheck BIT=0
    
    -- Add the T-SQL statements to compute the return value here
    
    
    IF	(@isAutoGroup IS NULL)
    BEGIN
		SET @isAutoGroup = (SELECT GD.IsAutomaticGroup FROM dbo.GroupDefinitions GD 
			INNER JOIN dbo.UniqueGroupResponses UGR ON UGR.ID = @uniqueGroupResponse AND UGR.GroupDefinitionID = GD.ID)
    END
    
    IF (@groupDeviation > @groupTolerance) -- group marked out of tolerance
    BEGIN
        SET @myCheck = 1
    END
    ELSE 
    IF (
			-- if autogroup check only group tolerance
			@isAutoGroup = 0 AND
           -- group marked within tolerance, so we need the same check on item level
           EXISTS
           (
               SELECT *
               FROM   dbo.AssignedItemMarks AIM
                      INNER JOIN dbo.UniqueResponses UR
                           ON  UR.ID = AIM.UniqueResponseId
                      INNER JOIN dbo.Items I
                           ON  I.ID = UR.itemId
                      INNER JOIN dbo.UniqueGroupResponseLinks UGRL
                           ON  UGRL.UniqueResponseId = UR.id
                      INNER JOIN dbo.UniqueGroupResponses UGR
                           ON  UGR.Id = UGRL.UniqueGroupResponseID
                      INNER JOIN dbo.GroupDefinitionItems GDI
                           ON  GDI.itemId = UR.itemId
                               AND GDI.GroupID = UGR.GroupDefinitionID
                               AND GDI.ViewOnly = 0
               WHERE  AIM.MarkingDeviation>I.CIMarkingTolerance -- the main check
                      AND AIM.GroupMarkId = @assignedGroupMarkId
                      AND UGRL.UniqueGroupResponseID = @uniqueGroupResponse
           )
       )
    BEGIN
        SET @myCheck = 1
    END
    
    -- Return the result of the function
    RETURN @myCheck
END


GO
/****** Object:  UserDefinedFunction [dbo].[sm_checkGroupCIsExceeded_fn]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Alexey Kharitonov
-- Create date: 29/05/2014
-- Description:	Check number of CIs for group is exceeded.
-- =============================================
CREATE FUNCTION [dbo].[sm_checkGroupCIsExceeded_fn]
(
	-- Add the parameters for the function here
	@groupID INT
)
RETURNS BIT
AS
BEGIN
	-- Return the result of the function
	RETURN(SELECT CONVERT(BIT,
					CASE WHEN 
							(SELECT COUNT(ID)
								FROM UniqueGroupResponses
								WHERE   CI=1
									AND GroupDefinitionID=GD.ID)>=GD.NoOfCIsRequired 
						THEN 1 
					ELSE 
						0 
					END) 
		   FROM GroupDefinitions GD
		   WHERE ID=@groupID)
END
GO
/****** Object:  UserDefinedFunction [dbo].[sm_checkIsUniqueGroupResponseInConfirmedScript]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Vitaly Bevzik
-- Create date: 12/30/2013
-- Description:	Checks Is Unique Group Response In ConfirmedS cript
-- =============================================
CREATE FUNCTION [dbo].[sm_checkIsUniqueGroupResponseInConfirmedScript]
(
	-- Add the parameters for the function here
	@uniqueGroupResponseId BIGINT
)
RETURNS BIT
AS
BEGIN
	-- Declare the return variable here
	DECLARE @result BIT = 0

	-- Add the T-SQL statements to compute the return value here
	IF EXISTS (
	SELECT * FROM dbo.CandidateGroupResponses CGR
		INNER JOIN dbo.CandidateExamVersions CEV on CEV.ID = CGR.CandidateExamVersionID
		WHERE CGR.UniqueGroupResponseID = @uniqueGroupResponseId
		AND CEV.ExamSessionState in (6,7))
	BEGIN
		SET @result = 1
	END

	-- Return the result of the function
	RETURN @result

END
GO
/****** Object:  UserDefinedFunction [dbo].[sm_checkUser1CiawayFromSuspend]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Vitaly Bevzik
-- Create date: 22/8/2013
-- Description:	Checks Examiner 1 CI away from being suspended
-- =============================================
CREATE FUNCTION [dbo].[sm_checkUser1CiawayFromSuspend]
(
	-- Add the parameters for the function here
	@userId INT,
	@groupId INT
)
RETURNS BIT
AS
BEGIN
	-- Declare the return variable here
	DECLARE @isUser1CiawayFromSuspend BIT

			-- calculate the last AGM.ID a CI was marked within tolerance by this user for this item
        DECLARE @lastCiAgmId BIGINT
				
        SELECT TOP 1
                @lastCiAgmId = AGM.ID
        FROM    [dbo].[AssignedGroupMarks] AGM
                INNER JOIN [dbo].[UniqueGroupResponses] UGR ON AGM.UniqueGroupResponseId = UGR.[id]
                INNER JOIN [dbo].GroupDefinitions GD ON UGR.GroupDefinitionID = GD.[ID]
        WHERE   AGM.userId = @userId
                AND UGR.GroupDefinitionID = @groupId
                AND AGM.markingMethodId IN ( 2, 3 )
                AND AGM.disregarded = 0
                AND dbo.sm_checkGroupAndItemsMarkedInTolerance_fn(AGM.id,
                                                              UGR.id,
                                                              AGM.MarkingDeviation,
                                                              GD.CIMarkingTolerance, GD.IsAutomaticGroup) = 0
        ORDER BY [timestamp] DESC					
		
		-- Get examiner CIs marked out of tolerance since last CG
		DECLARE @userCIsOutOfTol INT = (				
        SELECT  COUNT(*)
        FROM    [dbo].[AssignedGroupMarks] AGM
                INNER JOIN [dbo].[UniqueGroupResponses] UGR ON AGM.UniqueGroupResponseId = UGR.[id]
                INNER JOIN [dbo].GroupDefinitions GD ON UGR.GroupDefinitionID = GD.[ID]
        WHERE   AGM.userId = @userId
                AND AGM.markingMethodId IN ( 2, 3 )
                AND UGR.GroupDefinitionID = @groupId
                AND AGM.ID > ISNULL(@lastCiAgmId, 0)
                AND AGM.disregarded = 0
                AND dbo.sm_checkGroupAndItemsMarkedInTolerance_fn(AGM.id,
                                                              UGR.id,
                                                              AGM.MarkingDeviation,
                                                              GD.CIMarkingTolerance, GD.IsAutomaticGroup) = 1)
           
		-- get count of how many CIs examiner has marked out of tolerance within the rolling review period
		DECLARE @numberofItemsWithinRollingReview INT = (SELECT NumberOfCIsInRollingReview FROM GroupDefinitions WHERE ID = @groupId)
		DECLARE @maxConsecutiveCIsThatCanBeMarkedOutOfTolerance INT = (SELECT MaxConsecutiveCIsThatCanBeMarkedOutOfTolerance FROM GroupDefinitions WHERE ID = @groupId)
		DECLARE @maxCIsMarkedOutOfToleranceInRollingReview INT = (SELECT MaximumCIsMarkedOutOfToleranceInRollingReview FROM GroupDefinitions WHERE ID = @groupId)
		 
		DECLARE @numOfCIsMarkedOutOfTolinRollingReview INT = (                                                              
		SELECT  COUNT(x.[markingDeviation])
        FROM    ( SELECT TOP ( @numberofItemsWithinRollingReview )
                            agm.ID AS [AgmId] ,
                            UGR.ID AS [UGRId] ,
                            [markingDeviation] ,
                            [CIMarkingTolerance],
                            GD.IsAutomaticGroup
                  FROM      [dbo].[AssignedGroupMarks] AGM
                            INNER JOIN [dbo].[UniqueGroupResponses] UGR ON AGM.UniqueGroupResponseId = UGR.[id]
                            INNER JOIN [dbo].GroupDefinitions GD ON UGR.GroupDefinitionID = GD.[ID]
                  WHERE     AGM.userId = @userId
                            AND AGM.markingMethodId IN ( 2, 3 )
                            AND UGR.GroupDefinitionID = @groupId
                            AND AGM.disregarded = 0
                  ORDER BY Timestamp DESC
                ) x
        WHERE   dbo.sm_checkGroupAndItemsMarkedInTolerance_fn([AgmId], [UGRId],
                                                              [markingDeviation],
                                                              [CIMarkingTolerance], IsAutomaticGroup) = 1 )
    
    IF(@maxConsecutiveCIsThatCanBeMarkedOutOfTolerance <= @userCIsOutOfTol OR  @maxCIsMarkedOutOfToleranceInRollingReview <= @numOfCIsMarkedOutOfTolinRollingReview)  
    SET @isUser1CiawayFromSuspend = 1
    ELSE
    SET @isUser1CiawayFromSuspend = 0                                                     
                                                        
	-- Return the result of the function
	RETURN @isUser1CiawayFromSuspend

END
GO
/****** Object:  UserDefinedFunction [dbo].[sm_getExamPermissionsForScriptReview_fn]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE FUNCTION [dbo].[sm_getExamPermissionsForScriptReview_fn]
(
	@userId INT
)
RETURNS @returnTable TABLE 
    (
		PermExamID INT NOT NULL,
		IsAllowReview BIT,
		IsAllowModerate BIT,
		IsAllowConfirm BIT,
		IsAllowVip BIT,
		IsAllowFlag BIT
    )
AS

BEGIN
    IF (
           EXISTS
           (
               SELECT r.ID
               FROM   dbo.AssignedUserRoles aur
                      INNER JOIN dbo.Roles r ON  r.ID = aur.RoleID
                      INNER JOIN dbo.RolePermissions rp ON  aur.RoleID = rp.RoleID
                      INNER JOIN dbo.Permissions p ON  rp.PermissionID = p.ID
               WHERE  aur.ExamID = 0
                      AND aur.UserID = @userID
                      AND p.GlobalSupport = 1
                      AND rp.PermissionID = 27 -- Script Review                 
           )
       )
    BEGIN
        INSERT @returnTable
        SELECT e.ID         AS 'PermExamID'
			   ,null
			   ,null
			   ,null
			   ,null
			   ,null
        FROM   dbo.Exams e
        WHERE  e.ID<>0
    END
    ELSE
    BEGIN
        INSERT @returnTable
        SELECT DISTINCT
               aur.ExamID AS 'PermExamID'
               ,null
			   ,null
			   ,null
			   ,null
			   ,null
        FROM   dbo.AssignedUserRoles aur
               INNER JOIN dbo.RolePermissions rp ON  aur.RoleID = rp.RoleID
        WHERE  aur.ExamID<>0
               AND aur.UserID = @userID
               AND rp.PermissionID = 27 -- Script Review
    END
    
    UPDATE RT
    SET IsAllowReview = (
						SELECT(CASE WHEN ((SELECT COUNT(*)
						FROM dbo.RolePermissions RP
						INNER JOIN dbo.AssignedUserRoles AUR ON Rp.RoleID = AUR.RoleID
						WHERE AUR.UserID = @userId AND (RT.PermExamID = AUR.ExamID OR AUR.ExamID = 0) AND RP.PermissionID = 35) -- Script Review View Items
						> 0) THEN 1  ELSE 0 END)), 
        IsAllowModerate = (SELECT(CASE WHEN ((SELECT COUNT(*)
						FROM dbo.RolePermissions RP
						INNER JOIN dbo.AssignedUserRoles AUR ON Rp.RoleID = AUR.RoleID
						WHERE AUR.UserID = @userId AND (RT.PermExamID = AUR.ExamID OR AUR.ExamID = 0) AND RP.PermissionID = 40) -- Script Review Moderate Script
						> 0) THEN 1  ELSE 0 END)), 
		IsAllowFlag = (SELECT(CASE WHEN ((SELECT COUNT(*)
						FROM dbo.RolePermissions RP
						INNER JOIN dbo.AssignedUserRoles AUR ON Rp.RoleID = AUR.RoleID
						WHERE AUR.UserID = @userId AND (RT.PermExamID = AUR.ExamID OR AUR.ExamID = 0) AND RP.PermissionID = 41) -- Script Review Set Flag
						> 0) THEN 1  ELSE 0 END)),
        IsAllowVip = (SELECT(CASE WHEN ((SELECT COUNT(*)
						FROM dbo.RolePermissions RP
						INNER JOIN dbo.AssignedUserRoles AUR ON Rp.RoleID = AUR.RoleID
						WHERE AUR.UserID = @userId AND (RT.PermExamID = AUR.ExamID OR AUR.ExamID = 0) AND RP.PermissionID = 30) -- Script Review Set VIP         
						> 0) THEN 1  ELSE 0 END)),
        IsAllowConfirm = (SELECT(CASE WHEN ((SELECT COUNT(*)
						FROM dbo.RolePermissions RP
						INNER JOIN dbo.AssignedUserRoles AUR ON Rp.RoleID = AUR.RoleID
						WHERE AUR.UserID = @userId AND (RT.PermExamID = AUR.ExamID OR AUR.ExamID = 0) AND RP.PermissionID = 29) -- Script Review Confirm         
						> 0) THEN 1  ELSE 0 END))
		FROM @returnTable RT
		
    RETURN
END
GO
/****** Object:  UserDefinedFunction [dbo].[sm_getIsNewItem_fn]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Anton Burdyugov
-- Create date: 29/04/2013
-- Description:	Determines whether or not an item is new.
-- =============================================
CREATE FUNCTION [dbo].[sm_getIsNewItem_fn]
(
	-- Add the parameters for the function here	
	@itemId INT
)
RETURNS INT
AS
BEGIN
	-- Declare the return variable here
	DECLARE @isNew BIT = 1 -- true

	-- Add the T-SQL statements to compute the return value here	
	IF
	(
		EXISTS
		(
			Select TOP 1 *
				from GroupDefinitionItems 
				inner join GroupDefinitions 
				on GroupDefinitions.Id = GroupDefinitionItems.GroupID
				where 
					GroupDefinitionItems.ItemID = @itemId 
					and 
					GroupDefinitions.IsAutomaticGroup = 0 -- false
		)
	)
		BEGIN
			SET @isNew = 0 -- false
		END	

	-- Return the result of the function
	RETURN @isNew

END
GO
/****** Object:  UserDefinedFunction [dbo].[sm_getMarkingExaminerReport_fn]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[sm_getMarkingExaminerReport_fn]
(
	-- Add the parameters for the function here
	@userId						INT
	,@startDate					DATETIME
	,@endDate					DATETIME
)
RETURNS @returnTable TABLE
        (
            ID INT
           ,NAME NVARCHAR(MAX)
           ,Qualification NVARCHAR(100)
           ,Exam NVARCHAR(200)
           ,ExamVersion NVARCHAR(MAX)
           ,Total INT
           ,ControlItems INT
           ,UniqueResponses INT
           ,FirstName NVARCHAR(50)
           ,LastName NVARCHAR(50)
           ,UserId INT
        )
AS
BEGIN
	
	DECLARE @examIds TABLE (ExamId INT)
	INSERT INTO @examIds
		SELECT ExamId FROM dbo.AssignedUserRoles AUR
			INNER JOIN dbo.RolePermissions RP ON AUR.RoleID = RP.RoleID
			WHERE RP.PermissionID = 53 AND AUR.UserID = @userId
			
	DECLARE @isGlobal BIT = 0
	if (EXISTS(SELECT ExamId FROM @examIds WHERE ExamId = 0))
		SET @isGlobal = 1
	
    ;WITH cteMRV AS (
        SELECT MRV.ID						AS 'Id'
              ,MRV.QualificationId			AS 'QualificationId'
              ,MRV.ExamId					AS 'ExamId'
			  ,STUFF(
                   (SELECT
                        ', ' + EV2.Name
                        FROM   dbo.MarkingReport_view  MRV2
						JOIN dbo.CandidateGroupResponses CGR2
							ON (CGR2.UniqueGroupResponseID = MRV2.UniqueGroupResponseId
								AND TIMESTAMP BETWEEN @startDate AND @endDate 
								AND MRV2.EscalatedWithoutMark = 0			
								AND MRV2.ID = MRV.ID
								AND MRV2.QualificationId = MRV.QualificationId
								AND MRV2.ExamId = MRV.ExamId)
						JOIN dbo.CandidateExamVersions CEV2 
							ON (CEV2.ID = CGR2.CandidateExamVersionID)
						JOIN dbo.ExamVersions EV2 
							ON (EV2.ID = CEV2.ExamVersionID)
                        GROUP BY EV2.Name
                        ORDER BY EV2.Name
                        FOR XML PATH(''), TYPE
                   ).value('.','nvarchar(max)')
                   ,1,2, ''
              )								AS 'ExamVersion'
              ,COUNT(MRV.GroupDefinitionID) AS 'Total'
              ,SUM(MRV.ControlItem)			AS 'ControlItems'
              ,SUM(MRV.UniqueResponse)		AS 'UniqueResponses'
              ,MRV.Forename					AS 'FirstName'
              ,MRV.Surname					AS 'LastName'
              ,MRV.UserId					AS 'UserId'
        FROM   dbo.MarkingReport_view  MRV
		WHERE  TIMESTAMP BETWEEN @startDate AND @endDate
			AND (@isGlobal = 1 OR MRV.ExamId IN (SELECT ExamId FROM @examIds))
			AND MRV.EscalatedWithoutMark = 0
        GROUP BY
               MRV.ID
              ,MRV.QualificationId
              ,MRV.ExamId
              ,MRV.UserId
              ,MRV.Forename
              ,MRV.Surname
	     
    )
    
    INSERT INTO @returnTable
      (
        ID
       ,NAME
       ,Qualification
       ,Exam
       ,ExamVersion
       ,Total
       ,ControlItems
       ,UniqueResponses
       ,FirstName
       ,LastName
       ,UserId
      )
    SELECT MRV.ID
          ,i.ExternalItemName
          ,q.Name
          ,e.Name
          ,MRV.ExamVersion as Name
          ,MRV.Total
          ,MRV.ControlItems
          ,MRV.UniqueResponses
          ,MRV.FirstName
          ,MRV.LastName
          ,MRV.UserId
    FROM   cteMRV MRV
           INNER JOIN Items i ON  i.ID = MRV.ID
           INNER JOIN dbo.Qualifications q ON  q.ID = MRV.QualificationId
           INNER JOIN dbo.Exams e ON  e.ID = MRV.ExamId    
	OPTION(RECOMPILE)
    RETURN
END
GO
/****** Object:  UserDefinedFunction [dbo].[sm_getMarkingReportItems_fn]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO






CREATE FUNCTION [dbo].[sm_getMarkingReportItems_fn]
(
	-- Add the parameters for the function here
	@userId        INT
   ,@startDate     DATETIME
   ,@endDate       DATETIME
)
RETURNS @returnTable TABLE
        (
            ID INT
           ,NAME NVARCHAR(MAX)
           ,Qualification NVARCHAR(100)
           ,Exam NVARCHAR(200)
           ,ExamVersion NVARCHAR(MAX)
           ,Total INT
           ,ControlItems INT
           ,UniqueResponses INT
        )
AS
BEGIN

     WITH cteMRV AS (
        SELECT MRV.ID						AS 'Id'
              ,MRV.QualificationId			AS 'QualificationId'
              ,MRV.ExamId					AS 'ExamId'
			  ,STUFF(
                   (SELECT
                        ', ' + EV2.Name
                        FROM   dbo.MarkingReport_view  MRV2
						JOIN dbo.CandidateGroupResponses CGR2
							ON (CGR2.UniqueGroupResponseID = MRV2.UniqueGroupResponseId
								AND TIMESTAMP BETWEEN @startDate AND @endDate 
								AND MRV2.EscalatedWithoutMark = 0	
								AND MRV2.ID = MRV.ID
								AND MRV2.QualificationId = MRV.QualificationId
								AND MRV2.ExamId = MRV.ExamId)
						JOIN dbo.CandidateExamVersions CEV2 
							ON (CEV2.ID = CGR2.CandidateExamVersionID)
						JOIN dbo.ExamVersions EV2 
							ON (EV2.ID = CEV2.ExamVersionID)
                        GROUP BY EV2.Name
                        ORDER BY EV2.Name
                        FOR XML PATH(''), TYPE
                   ).value('.','nvarchar(max)')
                   ,1,2, ''
              )					            AS 'ExamVersion'
              ,COUNT(MRV.GroupDefinitionID) AS 'Total'
              ,SUM(MRV.ControlItem)			AS 'ControlItems'
              ,SUM(MRV.UniqueResponse)		AS 'UniqueResponses'
        FROM   dbo.MarkingReport_view  MRV
		WHERE  UserId = @userId AND TIMESTAMP BETWEEN @startDate AND @endDate
		AND MRV.EscalatedWithoutMark = 0
        GROUP BY
               ID
              ,QualificationId
              ,ExamId
    )

    
    INSERT INTO @returnTable
      (
        ID
       ,NAME
       ,Qualification
       ,Exam
       ,ExamVersion
       ,Total
       ,ControlItems
       ,UniqueResponses
      )
    SELECT MRV.ID
          ,i.ExternalItemName
          ,q.Name
          ,e.Name
          ,MRV.ExamVersion as Name
          ,MRV.Total
          ,MRV.ControlItems
          ,MRV.UniqueResponses
    FROM   cteMRV MRV
           INNER JOIN Items i ON  i.ID = MRV.ID
           INNER JOIN dbo.Qualifications q ON  q.ID = MRV.QualificationId
           INNER JOIN dbo.Exams e ON  e.ID = MRV.ExamId    
    RETURN
END



GO
/****** Object:  UserDefinedFunction [dbo].[sm_getQuotaForUserForExam_fn]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[sm_getQuotaForUserForExam_fn]
    (
      -- Add the parameters for the function here
      @examId INT,
      @userId INT
    )
RETURNS INT
AS 
    BEGIN
	-- Declare the return variable here
        DECLARE @myQuota INT

	-- Add the T-SQL statements to compute the return value here

        IF ( EXISTS ( SELECT    UserId
                      FROM      dbo.QuotaManagement QM
                                INNER JOIN dbo.GroupDefinitions GD ON QM.GroupId = GD.ID
                      WHERE     GD.ExamID = @examId
                                AND QM.UserId = @userId
                                AND QM.AssignedQuota = -1
                                AND EXISTS ( SELECT *
                                             FROM   GroupDefinitionStructureCrossRef CR
                                                    INNER JOIN ExamVersionStructures EVS ON EVS.ID = CR.ExamVersionStructureID
                                             WHERE  CR.Status = 0
                                                    AND CR.GroupDefinitionID = GD.ID
                                                    AND EVS.StatusID = 0 ) ) ) 
            BEGIN
                SET @myQuota = -1
            END
        ELSE 
            BEGIN
                SELECT  @myQuota = ISNULL(SUM(QM.AssignedQuota), 0)
                FROM    dbo.QuotaManagement QM
                        INNER JOIN dbo.GroupDefinitions GD ON QM.GroupId = GD.ID                       
                WHERE   GD.ExamID = @examId
                        AND QM.UserId = @userId
                        AND EXISTS ( SELECT *
                                             FROM   GroupDefinitionStructureCrossRef CR
                                                    INNER JOIN ExamVersionStructures EVS ON EVS.ID = CR.ExamVersionStructureID
                                             WHERE  CR.Status = 0
                                                    AND CR.GroupDefinitionID = GD.ID
                                                    AND EVS.StatusID = 0 )
            END

	-- Return the result of the function
        RETURN @myQuota

    END
GO
/****** Object:  UserDefinedFunction [dbo].[sm_getUserQuotaForGroup_fn]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Vitaly Bevzik
-- Create date: 19/09/2013
-- Description:	Gets a user's assigned quota for an item.
-- =============================================
CREATE FUNCTION [dbo].[sm_getUserQuotaForGroup_fn]
(
	-- Add the parameters for the function here	
	@groupID INT ,
    @userID INT
)
RETURNS INT
AS
BEGIN
	-- Declare the return variable here
	DECLARE @quota int = 0

	 IF ( EXISTS ( SELECT    Q.AssignedQuota
                      FROM      dbo.QuotaManagement Q
                      WHERE     Q.UserId = @userID
                                AND Q.GroupId = @groupID ) ) 
            BEGIN
                set @quota = (SELECT  ISNULL(Q.AssignedQuota, 0)
                FROM    dbo.QuotaManagement Q
                WHERE   Q.UserId = @userID
                        AND Q.GroupId = @groupID)
            END       

	-- Return the result of the function
	RETURN @quota

END

GO
/****** Object:  Table [dbo].[AffectedCandidateExamVersions]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AffectedCandidateExamVersions](
	[CandidateExamVersionId] [int] NOT NULL,
	[UserId] [int] NOT NULL,
	[UniqueGroupResponseId] [bigint] NOT NULL,
	[GroupDefinitionId] [int] NOT NULL,
 CONSTRAINT [PK_AffectedCandidateExamVersions] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[CandidateExamVersionId] ASC,
	[UniqueGroupResponseId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AssignedGroupMarks]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AssignedGroupMarks](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NULL,
	[UniqueGroupResponseId] [bigint] NOT NULL,
	[Timestamp] [datetime] NOT NULL,
	[AssignedMark] [decimal](18, 10) NOT NULL,
	[IsConfirmedMark] [bit] NOT NULL,
	[UserName] [nvarchar](50) NULL,
	[FullName] [nvarchar](101) NULL,
	[Surname] [nvarchar](50) NULL,
	[Disregarded] [bit] NOT NULL,
	[MarkingDeviation] [decimal](18, 10) NULL,
	[MarkingMethodId] [int] NULL,
	[IsEscalated] [bit] NOT NULL,
	[GroupDefinitionID] [int] NULL,
	[IsActualMarking] [bit] NULL,
	[ParkedAnnotation] [nvarchar](max) NULL,
	[IsReportableCI] [bit] NOT NULL,
	[EscalatedWithoutMark] [bit] NOT NULL,
 CONSTRAINT [PK_AssignedGroupMarks] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AssignedGroupMarksParkedReasonCrossRef]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AssignedGroupMarksParkedReasonCrossRef](
	[AssignedGroupMarkId] [bigint] NOT NULL,
	[ParkedReasonId] [int] NOT NULL,
 CONSTRAINT [PK_AssignedGroupMarksParkedReasonCrossRef] PRIMARY KEY CLUSTERED 
(
	[AssignedGroupMarkId] ASC,
	[ParkedReasonId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AssignedItemMarks]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AssignedItemMarks](
	[GroupMarkId] [bigint] NOT NULL,
	[UniqueResponseId] [bigint] NOT NULL,
	[AnnotationData] [xml] NULL,
	[AssignedMark] [decimal](18, 10) NOT NULL,
	[MarkingDeviation] [decimal](18, 10) NULL,
	[MarkedMetadataResponse] [xml] NULL,
 CONSTRAINT [PK_AssignedItemMarks] PRIMARY KEY CLUSTERED 
(
	[GroupMarkId] ASC,
	[UniqueResponseId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AssignedUserRoles]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AssignedUserRoles](
	[UserID] [int] NOT NULL,
	[RoleID] [int] NOT NULL,
	[ExamID] [int] NOT NULL,
 CONSTRAINT [PK_AssignedUserRoles] PRIMARY KEY CLUSTERED 
(
	[UserID] ASC,
	[RoleID] ASC,
	[ExamID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CandidateExamVersions]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CandidateExamVersions](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ExternalSessionID] [int] NOT NULL,
	[ExamVersionID] [int] NOT NULL,
	[DateSubmitted] [datetime] NOT NULL,
	[Keycode] [nvarchar](12) NULL,
	[ExamSessionState] [int] NULL,
	[VIP] [bit] NOT NULL,
	[lastExportDate] [datetime] NULL,
	[PercentageMarkingComplete] [int] NULL,
	[CandidateForename] [nvarchar](50) NULL,
	[CandidateSurname] [nvarchar](50) NULL,
	[CandidateRef] [nvarchar](300) NULL,
	[CentreName] [nvarchar](100) NULL,
	[CentreCode] [nvarchar](50) NULL,
	[CountryName] [nvarchar](50) NULL,
	[CompletionDate] [datetime] NULL,
	[ScriptType] [smallint] NOT NULL,
	[ExamVersionStructureID] [int] NULL,
	[Mark] [decimal](18, 10) NOT NULL,
	[TotalMark] [decimal](18, 10) NOT NULL,
	[IsFlagged] [bit] NOT NULL,
	[IsModerated] [bit] NOT NULL,
	[LastManagedDate] [datetime] NULL,
 CONSTRAINT [PK_CandidateExamVersions_1] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CandidateExamVersionStateLookup]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CandidateExamVersionStateLookup](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[State] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_CandidateExamVersionStateLookup] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CandidateExamVersionStatuses]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CandidateExamVersionStatuses](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Timestamp] [datetime] NOT NULL,
	[StateID] [int] NOT NULL,
	[CandidateExamVersionID] [int] NOT NULL,
 CONSTRAINT [PK_CandidateExamVersionStatuses] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CandidateGroupResponses]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CandidateGroupResponses](
	[CandidateExamVersionID] [int] NOT NULL,
	[UniqueGroupResponseID] [bigint] NOT NULL,
 CONSTRAINT [PK_CandidateGriupResponses] PRIMARY KEY CLUSTERED 
(
	[CandidateExamVersionID] ASC,
	[UniqueGroupResponseID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CandidateResponses]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CandidateResponses](
	[CandidateExamVersionID] [int] NOT NULL,
	[UniqueResponseID] [bigint] NOT NULL,
 CONSTRAINT [PK_CandidateResponses] PRIMARY KEY CLUSTERED 
(
	[CandidateExamVersionID] ASC,
	[UniqueResponseID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CompetenceStatusLookup]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CompetenceStatusLookup](
	[ID] [int] NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_QualifiedStatusLookup] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Exams]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Exams](
	[ID] [int] IDENTITY(0,1) NOT NULL,
	[Name] [varchar](200) NOT NULL,
	[QualificationID] [int] NOT NULL,
	[Reference] [nvarchar](100) NULL,
	[ExternalID] [int] NOT NULL,
	[AnnotationSchema] [xml] NULL,
	[AutoCIReview] [bit] NOT NULL,
	[DefaultAllowAnnotationsInMarking] [int] NOT NULL,
	[DefaultNoOfCIsRequired] [int] NOT NULL,
	[DefaultTotalItemsInCompetencyRun] [int] NOT NULL,
	[DefaultCorrectItemsWithinToleranceWithinCompetenceRunDefault] [int] NOT NULL,
	[DefaultMaximumCumulativeErrorWithinCompetenceRun] [decimal](18, 10) NOT NULL,
	[DefaultProbabilityOfGettingCI] [decimal](18, 0) NOT NULL,
	[DefaultMaximumResponseBeforePresentingCI] [int] NOT NULL,
	[DeafultMaxConsecutiveCIsThatCanBeMarkedOutOfTolerance] [int] NOT NULL,
	[DefaultNumberOfCIsInRollingReview] [int] NOT NULL,
	[DefaultMaximumCIsMarkedOutOfToleranceInRollingReview] [int] NOT NULL,
	[DefaultMaximumCumulativeErrorWithinCompetenceRunItemLevel] [decimal](18, 0) NOT NULL,
	[DeafultMaxConsecutiveCIsThatCanBeMarkedOutOfToleranceItemLevel] [int] NOT NULL,
	[DefaultMaximumCIsMarkedOutOfToleranceInRollingReviewItemLevel] [int] NOT NULL,
	[IsMarkOneItemPerScript] [bit] NULL,
	[IsFeedbackEnabled] [bit] NULL,
	[IsDecimalMarksEnabled] [bit] NOT NULL,
 CONSTRAINT [PK_Exams_1] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ExamSectionItems]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ExamSectionItems](
	[ExamSectionID] [int] NOT NULL,
	[ItemID] [int] NOT NULL,
	[ExamStructureId] [int] NOT NULL,
 CONSTRAINT [PK_ExamSectionItems] PRIMARY KEY CLUSTERED 
(
	[ExamSectionID] ASC,
	[ItemID] ASC,
	[ExamStructureId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ExamSections]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ExamSections](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ExamVersionID] [int] NOT NULL,
	[ExternalID] [int] NOT NULL,
	[Name] [nvarchar](255) NOT NULL,
	[IsDynamic] [bit] NOT NULL,
 CONSTRAINT [PK_ExamSections] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ExamVersionItems]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ExamVersionItems](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CandidateExamVersionID] [int] NOT NULL,
	[ItemID] [int] NOT NULL,
 CONSTRAINT [PK_ExamVersionItems] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ExamVersionItemsRef]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ExamVersionItemsRef](
	[ItemID] [int] NOT NULL,
	[ExamVersionID] [int] NOT NULL,
 CONSTRAINT [PK_ExamVersionItemsRef] PRIMARY KEY CLUSTERED 
(
	[ItemID] ASC,
	[ExamVersionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ExamVersions]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ExamVersions](
	[ID] [int] IDENTITY(0,1) NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[ExternalID] [int] NOT NULL,
	[ExamID] [int] NOT NULL,
	[ExternalExamReference] [nvarchar](100) NULL,
 CONSTRAINT [PK_ExamVersions] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ExamVersionStructureItems]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ExamVersionStructureItems](
	[ExamStructureID] [int] NOT NULL,
	[ItemID] [int] NOT NULL,
 CONSTRAINT [PK_ExamVersionStructureItems] PRIMARY KEY CLUSTERED 
(
	[ExamStructureID] ASC,
	[ItemID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ExamVersionStructures]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ExamVersionStructures](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[EarliestDate] [datetime] NOT NULL,
	[LatestDate] [datetime] NOT NULL,
	[ExamVersionID] [int] NOT NULL,
	[StatusID] [int] NOT NULL,
 CONSTRAINT [PK_ExamVersionStructures] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ExamVersionStructuresStatuses]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ExamVersionStructuresStatuses](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](255) NOT NULL,
 CONSTRAINT [PK_ExamVersionStructuresStatuses] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[GrantableRoles]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GrantableRoles](
	[RoleID] [int] NOT NULL,
	[GrantableRoleID] [int] NOT NULL,
 CONSTRAINT [PK_AssignableRoles] PRIMARY KEY CLUSTERED 
(
	[RoleID] ASC,
	[GrantableRoleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[GroupDefinitionItems]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GroupDefinitionItems](
	[GroupID] [int] NOT NULL,
	[ItemID] [int] NOT NULL,
	[ViewOnly] [bit] NOT NULL,
	[GroupDefinitionItemOrder] [int] NULL,
 CONSTRAINT [PK_GroupDefinitionItems] PRIMARY KEY CLUSTERED 
(
	[GroupID] ASC,
	[ItemID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[GroupDefinitions]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GroupDefinitions](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NULL,
	[IsAutomaticGroup] [bit] NOT NULL,
	[NoOfCIsRequired] [int] NOT NULL,
	[CIMarkingTolerance] [decimal](18, 10) NOT NULL,
	[TotalMark] [decimal](18, 10) NOT NULL,
	[TotalItemsInCompetencyRun] [int] NOT NULL,
	[CorrectItemsWithinToleranceWithinCompetenceRun] [int] NOT NULL,
	[MaximumCumulativeErrorWithinCompetenceRun] [decimal](18, 10) NOT NULL,
	[ProbabilityOfGettingCI] [decimal](18, 15) NOT NULL,
	[MaximumResponseBeforePresentingCI] [int] NOT NULL,
	[MaxConsecutiveCIsThatCanBeMarkedOutOfTolerance] [int] NOT NULL,
	[NumberOfCIsInRollingReview] [int] NOT NULL,
	[MaximumCIsMarkedOutOfToleranceInRollingReview] [int] NOT NULL,
	[IsReleased] [bit] NULL,
	[IsFeedbackOnDailyCompetencyCheck] [bit] NOT NULL,
	[IsFeedbackOnOngoingCompetencyCheck] [bit] NOT NULL,
	[ExamID] [int] NULL,
 CONSTRAINT [PK_GroupDefinitions] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[GroupDefinitionStatuses]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GroupDefinitionStatuses](
	[ID] [int] NOT NULL,
	[Name] [nvarchar](255) NULL,
 CONSTRAINT [PK_GroupDefinitionStatuses] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[GroupDefinitionStructureCrossRef]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GroupDefinitionStructureCrossRef](
	[GroupDefinitionID] [int] NOT NULL,
	[ExamVersionStructureID] [int] NOT NULL,
	[Status] [int] NOT NULL,
 CONSTRAINT [PK_GroupDefinitionStructureCrossRef] PRIMARY KEY CLUSTERED 
(
	[GroupDefinitionID] ASC,
	[ExamVersionStructureID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[GroupDefinitionStructureCrossRefStatuses]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GroupDefinitionStructureCrossRefStatuses](
	[ID] [int] NOT NULL,
	[Name] [nvarchar](255) NULL,
 CONSTRAINT [PK_GroupDefinitionStructureCrossRefStatuses] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Items]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Items](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[AllowAnnotations] [int] NOT NULL,
	[Version] [int] NOT NULL,
	[ExternalItemID] [nchar](20) NULL,
	[MarkingType] [int] NOT NULL,
	[CIMarkingTolerance] [decimal](18, 10) NOT NULL,
	[TotalMark] [decimal](18, 10) NOT NULL,
	[MaximumCumulativeErrorWithinCompetenceRun] [decimal](18, 10) NOT NULL,
	[ExternalContent] [varbinary](max) NOT NULL,
	[MaxConsecutiveCIsThatCanBeMarkedOutOfTolerance] [int] NOT NULL,
	[MaximumCIsMarkedOutOfToleranceInRollingReview] [int] NOT NULL,
	[LayoutName] [nvarchar](50) NULL,
	[ExternalItemName] [nvarchar](max) NULL,
	[ExamGroupID] [int] NULL,
	[ExamGroupName] [nvarchar](255) NULL,
	[ExamID] [int] NULL,
 CONSTRAINT [PK_Items] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [AK_Items_ExternalItemID_Version_ExamID] UNIQUE NONCLUSTERED 
(
	[ExternalItemID] ASC,
	[Version] ASC,
	[ExamID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ItemTools]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ItemTools](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ToolItemId] [int] NOT NULL,
	[ParentItemId] [int] NOT NULL,
	[ToolName] [nvarchar](1024) NOT NULL,
 CONSTRAINT [PK_dbo.ItemTools] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UC_ItemTools_ParentItemId_ToolItemId] UNIQUE NONCLUSTERED 
(
	[ParentItemId] ASC,
	[ToolItemId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MarkingMethodLookup]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MarkingMethodLookup](
	[id] [int] NOT NULL,
	[markingMethod] [nvarchar](100) NOT NULL,
 CONSTRAINT [PK_MarkingMethodLookup] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MarkSchemes]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MarkSchemes](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[markSchemeBlOb] [varbinary](max) NULL,
	[version] [int] NOT NULL,
	[ExternalItemID] [nchar](20) NOT NULL,
	[IsMarkingCriteria] [bit] NOT NULL,
	[externalUrl] [nvarchar](max) NULL,
	[Extension] [nvarchar](20) NULL,
 CONSTRAINT [PK_MarkSchemeTable] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MethodSecurity]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MethodSecurity](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Class] [nvarchar](200) NOT NULL,
	[Method] [nvarchar](50) NOT NULL,
	[Permission] [int] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Moderations]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Moderations](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UniqueResponseId] [bigint] NOT NULL,
	[CandidateExamVersionId] [int] NOT NULL,
	[UserId] [int] NOT NULL,
	[Timestamp] [datetime] NOT NULL,
	[Mark] [decimal](18, 10) NOT NULL,
	[Comment] [nvarchar](max) NOT NULL,
	[AnnotationData] [xml] NULL,
	[MarkedMetadataResponse] [xml] NULL,
	[IsActual] [bit] NOT NULL,
 CONSTRAINT [PK_Moderations] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ParkedReasonsLookup]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ParkedReasonsLookup](
	[ID] [int] NOT NULL,
	[Name] [nvarchar](200) NOT NULL,
	[IsCommentRequired] [bit] NOT NULL,
 CONSTRAINT [PK_ParkedReasonsLookup] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Permissions]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Permissions](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nchar](30) NOT NULL,
	[GlobalSupport] [bit] NOT NULL,
	[AssignableToExams] [bit] NOT NULL,
	[ParentId] [int] NULL,
	[Training] [bit] NULL,
	[InternalPermission] [bit] NOT NULL,
	[Order] [int] NULL,
 CONSTRAINT [PK_Permissions] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Qualifications]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Qualifications](
	[ID] [int] IDENTITY(0,1) NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[Reference] [nvarchar](100) NULL,
	[ExternalID] [int] NOT NULL,
 CONSTRAINT [PK_Qualifications_1] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[QuotaManagement]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[QuotaManagement](
	[UserId] [int] NOT NULL,
	[AssignedQuota] [int] NOT NULL,
	[MarkingSuspended] [bit] NOT NULL,
	[SuspendedReason] [int] NULL,
	[NumberMarked] [int] NOT NULL,
	[Within1CIofSuspension] [bit] NOT NULL,
	[NumItemsParked] [int] NOT NULL,
	[PreviousAssignedMarkId] [bigint] NULL,
	[GroupId] [int] NOT NULL,
	[NumberSuspended] [int] NOT NULL,
	[ExamID] [int] NOT NULL,
 CONSTRAINT [PK_QuotaManagement] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[GroupId] ASC,
	[ExamID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[RolePermissions]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RolePermissions](
	[RoleID] [int] NOT NULL,
	[PermissionID] [int] NOT NULL,
 CONSTRAINT [PK_RolePermissions] PRIMARY KEY CLUSTERED 
(
	[RoleID] ASC,
	[PermissionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Roles]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Roles](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[AssignableToGlobal] [bit] NOT NULL,
	[AssignedByIntegration] [bit] NOT NULL,
	[TrainingRole] [bit] NOT NULL,
	[InternalRole] [bit] NOT NULL,
	[AssignableToExams] [bit] NOT NULL,
 CONSTRAINT [PK_Roles] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SuspendedReasonLookUp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SuspendedReasonLookUp](
	[ID] [int] NOT NULL,
	[Type] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_SuspendedReasonLookUp] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SuspendedReasons]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SuspendedReasons](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ReasonID] [int] NOT NULL,
	[Reason] [nvarchar](max) NOT NULL,
	[UserID] [int] NOT NULL,
	[Date] [datetime] NOT NULL,
 CONSTRAINT [PK_SuspendedReasons] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TEMP_ItemsGroupsMigration]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TEMP_ItemsGroupsMigration](
	[ItemId] [int] NOT NULL,
	[FinalItemId] [int] NOT NULL,
	[GroupId] [int] NOT NULL,
	[FinalGroupId] [int] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TokenStore]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TokenStore](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[userId] [int] NOT NULL,
	[token] [nchar](30) NOT NULL,
	[lastInteractionDateTime] [datetime] NOT NULL,
	[serialisedUser] [image] NOT NULL,
 CONSTRAINT [PK_TokenStore] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UniqueGroupResponseLinks]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UniqueGroupResponseLinks](
	[UniqueGroupResponseID] [bigint] NOT NULL,
	[UniqueResponseId] [bigint] NOT NULL,
	[confirmedMark] [decimal](9, 5) NULL,
	[markedMetadataResponse] [xml] NULL,
 CONSTRAINT [PK_UniqueGroupResponseLinks] PRIMARY KEY CLUSTERED 
(
	[UniqueGroupResponseID] ASC,
	[UniqueResponseId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UniqueGroupResponseParkedReasonCrossRef]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UniqueGroupResponseParkedReasonCrossRef](
	[UniqueGroupResponseId] [bigint] NOT NULL,
	[ParkedReasonId] [int] NOT NULL,
 CONSTRAINT [PK_UniqueGroupResponseParkedReasonCrossRef] PRIMARY KEY CLUSTERED 
(
	[UniqueGroupResponseId] ASC,
	[ParkedReasonId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UniqueGroupResponses]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UniqueGroupResponses](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[GroupDefinitionID] [int] NOT NULL,
	[CheckSum] [nvarchar](50) NOT NULL,
	[InsertionDate] [datetime] NULL,
	[CI_Review] [bit] NOT NULL,
	[CI] [bit] NOT NULL,
	[VIP] [bit] NULL,
	[ConfirmedMark] [decimal](9, 5) NULL,
	[ParkedAnnotation] [nvarchar](max) NULL,
	[ParkedDate] [datetime] NULL,
	[TokenId] [int] NULL,
	[ParkedUserID] [int] NULL,
	[ScriptType] [smallint] NOT NULL,
	[Feedback] [nvarchar](max) NULL,
	[IsEscalated] [bit] NOT NULL,
	[OriginalUniqueGroupResponseId] [bigint] NULL,
 CONSTRAINT [PK_UniqueGroupResponses] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UniqueResponseDocuments]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UniqueResponseDocuments](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](100) NULL,
	[Blob] [varbinary](max) NOT NULL,
	[UniqueResponseID] [bigint] NOT NULL,
	[BlobType] [nvarchar](20) NOT NULL,
	[ExternalDocID] [int] NULL,
	[UploadedDate] [datetime] NOT NULL,
	[FileName] [nvarchar](200) NOT NULL,
 CONSTRAINT [PK_UniqueResponseDocuments] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UniqueResponses]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UniqueResponses](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[itemId] [int] NOT NULL,
	[responseData] [xml] NULL,
	[checksum] [nchar](10) NOT NULL,
	[confirmedMark] [decimal](9, 5) NULL,
	[insertionDate] [datetime] NOT NULL,
	[markSchemeId] [int] NULL,
	[latestMarkSchemeIdAtTimeOfImport] [int] NOT NULL,
	[markedMetadataResponse] [xml] NULL,
 CONSTRAINT [PK_UniqueResponseTable] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UploadedFilesDetails]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UploadedFilesDetails](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UserType] [int] NOT NULL,
	[UploadedByUserID] [int] NOT NULL,
	[ReasonTypeID] [int] NOT NULL,
	[ReasonText] [nvarchar](2000) NULL,
	[UniqueResponseDocumentID] [int] NOT NULL,
 CONSTRAINT [PK_UploadedFilesDetails] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UploadReasons]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UploadReasons](
	[ID] [int] IDENTITY(0,1) NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
 CONSTRAINT [PK_UploadReasons] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserCompetenceStatus]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserCompetenceStatus](
	[UserID] [int] NOT NULL,
	[DateCertified] [datetime] NOT NULL,
	[CompetenceStatus] [int] NOT NULL,
	[ItemCountSinceLastCISubmitted] [int] NOT NULL,
	[GroupDefinitionID] [int] NOT NULL,
 CONSTRAINT [PK_UserQualifiedStatus_1] PRIMARY KEY CLUSTERED 
(
	[UserID] ASC,
	[GroupDefinitionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserInteractionAudit]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserInteractionAudit](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UserID] [int] NOT NULL,
	[CallingClass] [nvarchar](300) NOT NULL,
	[CallingMethod] [nvarchar](50) NOT NULL,
	[Arguments] [xml] NULL,
	[Date] [datetime] NOT NULL,
	[Success] [bit] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserQualifiedStatusAudit]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserQualifiedStatusAudit](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UserID] [int] NOT NULL,
	[ItemID] [int] NOT NULL,
	[Date] [datetime] NOT NULL,
	[ItemCountSinceLastCISubmitted] [int] NOT NULL,
 CONSTRAINT [PK_UserQualifiedStatusAudit] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserRecentExams]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserRecentExams](
	[ExamId] [int] NOT NULL,
	[UserId] [int] NOT NULL,
	[LastUsageDate] [datetime] NOT NULL,
 CONSTRAINT [PK_UserRecentExams] PRIMARY KEY CLUSTERED 
(
	[ExamId] ASC,
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Users]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Users](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Username] [nvarchar](50) NOT NULL,
	[Password] [nvarchar](100) NOT NULL,
	[Forename] [nvarchar](50) NOT NULL,
	[Surname] [nvarchar](50) NOT NULL,
	[Retired] [bit] NOT NULL,
	[Email] [nvarchar](100) NULL,
	[RequiresPasswordReset] [bit] NOT NULL,
	[Phone] [nvarchar](20) NULL,
	[ExternalRef] [nvarchar](50) NULL,
	[ExpiryDate] [smalldatetime] NULL,
	[InternalUser] [bit] NOT NULL,
	[PasswordAttempts] [int] NULL,
	[RetiredUsername] [nvarchar](50) NULL,
	[LastLoginDate] [datetime] NULL,
 CONSTRAINT [PK_UserTable] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[WelcomeMessageTable]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WelcomeMessageTable](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[welcomeText] [nvarchar](max) NOT NULL,
	[modifiedBy] [int] NOT NULL,
	[modifiedDate] [datetime] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  View [dbo].[UserExamPermissions_view]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[UserExamPermissions_view]
WITH SCHEMABINDING
AS
	SELECT E.ID AS ExamID, RP.PermissionID, AUR.UserID
			FROM	dbo.Exams E
			JOIN	dbo.AssignedUserRoles AUR ON AUR.ExamID = E.ID OR AUR.ExamID = 0
			JOIN	dbo.RolePermissions RP ON  (AUR.RoleID = RP.RoleID AND RP.PermissionID IN (12 ,13 ,14, 42))

GO
/****** Object:  View [dbo].[UserExamUnrestrictedExaminer_view]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[UserExamUnrestrictedExaminer_view]
WITH SCHEMABINDING
AS
	SELECT UserId, ExamId, 0 AS MarkingRestriction 
			FROM dbo.UserExamPermissions_view 
			WHERE PermissionID = 42
			GROUP BY UserID, ExamId


GO
/****** Object:  UserDefinedFunction [dbo].[getExamsWithRestrictions_fn]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE FUNCTION [dbo].[getExamsWithRestrictions_fn]
(	
	@userId INT,
	@defaultMarkingRestriction BIT
)
RETURNS TABLE
WITH SCHEMABINDING
AS
RETURN 
(
	SELECT E.ID AS ExamId
              , ISNULL(UE.MarkingRestriction, ISNULL(E.IsMarkOneItemPerScript ,@defaultMarkingRestriction)) AS IsMarkingRestrictionEnableForExam
              , E.Name as Name
              , E.QualificationID as QualificationID
			FROM dbo.Exams E
			LEFT JOIN dbo.UserExamUnrestrictedExaminer_view AS UE ON UE.ExamId = E.ID AND UserId = @userId
)





GO
/****** Object:  View [dbo].[NotMarkedResponses_view]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[NotMarkedResponses_view]
WITH SCHEMABINDING
AS
SELECT ID as UniqueGroupResponseId
	, InsertionDate
	, TokenId
	, GroupDefinitionID
	, VIP
	FROM dbo.UniqueGroupResponses AS UGR
	WHERE UGR.ParkedUserID IS NULL
			AND UGR.CI = 0
			AND UGR.confirmedMark IS NULL


GO
SET ARITHABORT ON
SET CONCAT_NULL_YIELDS_NULL ON
SET QUOTED_IDENTIFIER ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
SET NUMERIC_ROUNDABORT OFF
GO
/****** Object:  Index [PK_NotMarkedResponses_view]    Script Date: 30/08/2017 08:26:38 ******/
CREATE UNIQUE CLUSTERED INDEX [PK_NotMarkedResponses_view] ON [dbo].[NotMarkedResponses_view]
(
	[UniqueGroupResponseId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  UserDefinedFunction [dbo].[getAvailableUniqueGroupResponses_fn]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE FUNCTION [dbo].[getAvailableUniqueGroupResponses_fn]
(	
	@tokenStoreID INT
)
RETURNS TABLE
WITH SCHEMABINDING
AS
RETURN 
(
	SELECT UniqueGroupResponseId
		, InsertionDate
		, TokenId
		, GroupDefinitionID
		, VIP
		FROM dbo.NotMarkedResponses_view
		WHERE TokenId IS NULL
				OR TokenId = @tokenStoreID
)

GO
/****** Object:  View [dbo].[ActiveGroups_view]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[ActiveGroups_view]
WITH SCHEMABINDING
AS
	SELECT GDSCR.GroupDefinitionID, COUNT_BIG(*) as RecordsCount
		FROM   dbo.GroupDefinitionStructureCrossRef GDSCR
			   INNER JOIN dbo.ExamVersionStructures EVS
					ON  EVS.ID = GDSCR.ExamVersionStructureID
		WHERE  GDSCR.Status = 0 -- 0 = Active
			   AND EVS.StatusID = 0 -- 0 = Released
		GROUP BY GDSCR.GroupDefinitionID
GO
SET ARITHABORT ON
SET CONCAT_NULL_YIELDS_NULL ON
SET QUOTED_IDENTIFIER ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
SET NUMERIC_ROUNDABORT OFF
GO
/****** Object:  Index [IX_ActiveGroups_GroupDefinitionID]    Script Date: 30/08/2017 08:26:38 ******/
CREATE UNIQUE CLUSTERED INDEX [IX_ActiveGroups_GroupDefinitionID] ON [dbo].[ActiveGroups_view]
(
	[GroupDefinitionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  View [dbo].[UserExamExaminer_view]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW [dbo].[UserExamExaminer_view]
WITH SCHEMABINDING
AS
	SELECT UserId, ExamId, 0 AS MarkingRestriction 
			FROM dbo.UserExamPermissions_view 
			WHERE PermissionID IN (12 ,13 ,14)
			GROUP BY UserID, ExamId



GO
/****** Object:  UserDefinedFunction [dbo].[getUserExamsWithMarkingPermissions_fn]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE FUNCTION [dbo].[getUserExamsWithMarkingPermissions_fn]
(	
	@userId INT
)
RETURNS @result TABLE(ExamId INT)
WITH SCHEMABINDING
AS
BEGIN 
	DECLARE @userExamPermissions TABLE (ExamId INT)
	INSERT INTO @userExamPermissions
		SELECT ExamId
			FROM dbo.UserExamExaminer_view
			WHERE UserId = @userId
			GROUP BY ExamId

	IF EXISTS (SELECT ExamId FROM @userExamPermissions WHERE ExamId = 0)
		BEGIN
			INSERT INTO @result VALUES(0)
		END
	ELSE
		BEGIN
			INSERT INTO @result
				SELECT ExamId FROM @userExamPermissions
		END
	RETURN
END

GO
/****** Object:  UserDefinedFunction [dbo].[getGroupDefinitionsJoined_fn]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[getGroupDefinitionsJoined_fn]
(	
	@userId INT,
	@tokenStoreId INT
)
RETURNS TABLE
WITH SCHEMABINDING
AS
RETURN 
(
	SELECT GD.ID as ID
		, GD.Name as Name
		, GD.ExamID as ExamId
		, E.IsMarkOneItemPerScript as IsMarkOneItemPerScript
		, GD.IsReleased as IsReleased
	FROM dbo.GroupDefinitions GD
		JOIN dbo.Exams E ON GD.ExamID = E.ID
		JOIN dbo.getUserExamsWithMarkingPermissions_fn(@userId) ER ON (ER.ExamId = 0 OR ER.ExamId = GD.ExamID)
		WHERE GD.IsReleased = 0 OR GD.IsReleased = 1 -- we have to get groups with any state but Not Released
			AND GD.ID IN (SELECT GroupDefinitionID FROM dbo.ActiveGroups_view)
)
GO
/****** Object:  UserDefinedFunction [dbo].[getGroupSourceTable_fn]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[getGroupSourceTable_fn]
(	
	@userId INT,
	@tokenStoreID INT,
	@unlimitedOnly BIT
)
RETURNS TABLE
WITH SCHEMABINDING
AS
RETURN 
(
	SELECT GD.ID
            , GD.Name AS GroupName
            , GD.ExamID
            , UGR.InsertionDate
            , UGR.UniqueGroupResponseId
            , GD.IsMarkOneItemPerScript 
            , CAST(ISNULL(UGR.VIP ,0) AS FLOAT) AS VIP
            , CASE (QM.MarkingSuspended) WHEN 1 THEN 1 ELSE 0 END AS Suspended
            , GD.IsReleased AS 'IsReleased'
	FROM   dbo.QuotaManagement QM
            JOIN dbo.getGroupDefinitionsJoined_fn(@userId, @tokenStoreID) GD ON GD.ID = QM.GroupId
            LEFT JOIN dbo.getAvailableUniqueGroupResponses_fn(@tokenStoreID) AS UGR ON UGR.GroupDefinitionID = QM.GroupId
	WHERE  QM.UserId = @userId
			AND (@unlimitedOnly = 1 AND QM.AssignedQuota = -1 OR @unlimitedOnly = 0)
)






GO
/****** Object:  View [dbo].[ManualMarkedGroups_view]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[ManualMarkedGroups_view]
WITH SCHEMABINDING
AS
SELECT GD.ID
FROM   dbo.Items I
       INNER JOIN dbo.GroupDefinitionItems GDI ON I.ID = GDI.ItemID
       INNER JOIN dbo.GroupDefinitions GD ON GDI.GroupID = GD.ID
WHERE  (
			I.MarkingType = 1
			AND 
			GD.IsAutomaticGroup = 1
	   )
	   OR GD.IsAutomaticGroup = 0
GROUP BY GD.ID


GO
/****** Object:  UserDefinedFunction [dbo].[getGroupAggregatedSourceForMarking_fn]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[getGroupAggregatedSourceForMarking_fn]
(	
	@userId INT,
	@tokenStoreID INT
)
RETURNS TABLE
WITH SCHEMABINDING
AS
RETURN 
(
	SELECT 
	T.ID AS GroupId
	, GroupName
	, ExamID
	, MIN(InsertionDate) AS EarliestDate
	, IsReleased
	, COUNT(UniqueGroupResponseId) AS NumResponses
	, ISNULL( MAX(CAST(Suspended as INT)), 0) AS Suspended
	, CASE WHEN COUNT(UniqueGroupResponseId) = 0 THEN 0 ELSE SUM(VIP)/CAST(COUNT(UniqueGroupResponseId) AS FLOAT) END AS VIP
	FROM dbo.getGroupSourceTable_fn(@userId, @tokenStoreID, 0) T
	INNER JOIN dbo.ManualMarkedGroups_view MMG
	ON T.ID = MMG.ID 
	GROUP BY ExamID
			, T.ID
			, T.GroupName
			, T.IsReleased
			, T.IsMarkOneItemPerScript
)
GO
/****** Object:  UserDefinedFunction [dbo].[getAffectedScripts_fn]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[getAffectedScripts_fn]
(	
	@userId INT
)
RETURNS TABLE
WITH SCHEMABINDING
AS
RETURN 
(
	SELECT ACEV.CandidateExamVersionId AS AffectedScript
			FROM   dbo.CandidateExamVersions CEV
				 INNER JOIN dbo.AffectedCandidateExamVersions ACEV
					  ON  ACEV.CandidateExamVersionId = CEV.ID
			WHERE  ACEV.UserId = @userId
				 AND ACEV.GroupDefinitionID IN (SELECT GroupDefinitionID FROM dbo.ActiveGroups_view)
			GROUP BY ACEV.CandidateExamVersionId
)

GO
/****** Object:  UserDefinedFunction [dbo].[getAffectedUniqueResponses_fn]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE FUNCTION [dbo].[getAffectedUniqueResponses_fn]
(	
	@userId INT,
	@tokenStoreID INT
)
RETURNS TABLE
WITH SCHEMABINDING
AS
RETURN 
(
	SELECT COUNT(CGR1.UniqueGroupResponseID) as ResponsesCount
				, GD.ID AS GroupId
			FROM dbo.GroupDefinitions GD
			JOIN dbo.UniqueGroupResponses UGR 
				ON  (UGR.GroupDefinitionID = GD.ID
					AND UGR.IsEscalated = 0 
					AND UGR.CI = 0
					AND UGR.confirmedMark IS NULL		
					AND (UGR.[tokenid] IS NULL OR UGR.[tokenid] = @tokenStoreID))
			JOIN dbo.CandidateGroupResponses CGR1 ON CGR1.UniqueGroupResponseID = UGR.ID
		WHERE CGR1.CandidateExamVersionID IN (SELECT AffectedScript FROM dbo.getAffectedScripts_fn(@userId))
		GROUP BY GD.ID
)


GO
/****** Object:  UserDefinedFunction [dbo].[getGroupsWithAffectedResponsesForMarking_fn]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- User Defined Function

CREATE FUNCTION [dbo].[getGroupsWithAffectedResponsesForMarking_fn]
(	
	@userId INT,
	@tokenStoreID INT
)
RETURNS TABLE
WITH SCHEMABINDING
AS
RETURN 
(
	SELECT GAS.GroupId
				, GroupName
				, ExamID
				, EarliestDate
				, NumResponses
				, IsReleased
				, VIP
				, Suspended
				, ResponsesCount as AffectedUniqueResponses
				, NumResponses - ISNULL(ResponsesCount, 0) as ResponsesDif
			FROM dbo.getGroupAggregatedSourceForMarking_fn(@userId, @tokenStoreID) AS GAS
				LEFT JOIN dbo.getAffectedUniqueResponses_fn(@userId, @tokenStoreID) AS AUR ON AUR.GroupId = GAS.GroupId
)

GO
/****** Object:  UserDefinedFunction [dbo].[sm_MARKINGSERVICE_GetAllGroupsForUser_fn]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[sm_MARKINGSERVICE_GetAllGroupsForUser_fn]
(
	@userId INT,
	@tokenStoreID INT,
	@defaultMarkingRestriction BIT
)
RETURNS TABLE 
RETURN
(
	SELECT Q.Name AS Qualification,
		   Q.ID AS QualificationId,
		   EWR.Name AS Exam,
		   EWR.ExamID,
		   T.GroupId AS ID,
		   T.GroupName AS ItemName,
		   QM.NumberMarked AS Marked,
		   (
			   CASE 
					WHEN (QM.[assignedquota] = -1) THEN -1
					WHEN ((QM.[assignedquota] - QM.NumberMarked) > 0) THEN (QM.[assignedquota] - QM.NumberMarked)
					ELSE 0
			   END
		   ) AS RemainingQuota,
		   T.NumResponses AS UnmarkedUGRsCount,
		   (
			   CASE EWR.IsMarkingRestrictionEnableForExam
					WHEN 1 THEN (T.ResponsesDif)
					ELSE T.NumResponses
			   END
		   ) AS UGRsAvailableToMarkCount,
		   CAST(
			   CASE WHEN (
				   (CASE EWR.IsMarkingRestrictionEnableForExam
						WHEN 1 THEN (T.ResponsesDif)
						ELSE T.NumResponses
				   END) > 0 
				   AND T.Suspended = 0
				   AND (CASE 
							WHEN (QM.[assignedquota] = -1) THEN -1
							WHEN ((QM.[assignedquota] - QM.NumberMarked) > 0) THEN (QM.[assignedquota] - QM.NumberMarked)
							ELSE 0
					   END) != 0
				   ) THEN 1 ELSE 0 END AS BIT
		   ) AS HasToMark,
		   CONVERT(BIT, T.Suspended) AS IsSuspended,
		   T.IsReleased AS IsReleased,
		   T.VIP AS VIP,
		   T.EarliestDate AS EarliestDate
	       
	FROM [dbo].[getGroupsWithAffectedResponsesForMarking_fn](@userId, @tokenStoreID) AS T
		INNER JOIN dbo.getExamsWithRestrictions_fn(@userId, @defaultMarkingRestriction) AS EWR ON  EWR.ExamID = T.ExamId
		INNER JOIN dbo.Qualifications AS Q ON  EWR.QualificationID = Q.ID
		INNER JOIN QuotaManagement AS QM ON (T.GroupId = QM.GroupId AND QM.UserId = @userId)
	-- if the user didn't mark the group and the user doesn't have quota for the group => we shouldn't show the group
	-- otherwice if the user has quota for the group or marked the group already => we have to show such group
	WHERE (QM.NumberMarked != 0 OR QM.AssignedQuota != 0)
)

GO
/****** Object:  View [dbo].[GroupCentreCandidate_view]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




CREATE VIEW [dbo].[GroupCentreCandidate_view]
WITH SCHEMABINDING
AS
	SELECT CGR.UniqueGroupResponseID AS ID
	, CGR.CandidateExamVersionID
	, CEV.CandidateForename
	, CEV.CandidateSurname
	, CEV.CandidateRef
	, CEV.CentreName
	, CEV.CentreCode
	, CEV.ID as ScriptId
	FROM dbo.CandidateExamVersions CEV
	INNER JOIN dbo.CandidateGroupResponses CGR ON CGR.CandidateExamVersionID = CEV.ID



GO
SET ARITHABORT ON
SET CONCAT_NULL_YIELDS_NULL ON
SET QUOTED_IDENTIFIER ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
SET NUMERIC_ROUNDABORT OFF
GO
/****** Object:  Index [PK_GroupCentreCandidate_view]    Script Date: 30/08/2017 08:26:38 ******/
CREATE UNIQUE CLUSTERED INDEX [PK_GroupCentreCandidate_view] ON [dbo].[GroupCentreCandidate_view]
(
	[ID] ASC,
	[CandidateExamVersionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  View [dbo].[CandidateCentreResponse_view]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




CREATE VIEW [dbo].[CandidateCentreResponse_view]
WITH SCHEMABINDING
AS
	SELECT ID,
			(CASE 
				WHEN COUNT(CandidateExamVersionID) = 1 
					THEN 
						MAX(CandidateForename)+ ' ' + MAX(CandidateSurname)
					ELSE
						''
			END) AS CandidateName,
			(CASE
				WHEN COUNT(CandidateExamVersionID) = 1 
					THEN 
						MAX(CandidateRef)
					ELSE
						''
			END) AS CandidateRef,
			(CASE 
				WHEN COUNT(CandidateExamVersionID) = 1 
					THEN 
						MAX(CentreName)
					ELSE
						''
			END) AS CentreName,
			(CASE 
				WHEN COUNT(CandidateExamVersionID) = 1 
					THEN 
						MAX(CentreCode)
					ELSE
						''
			END) AS CentreCode,
			(CASE 
				WHEN COUNT(CandidateExamVersionID) = 1 
					THEN 
						MAX(ScriptId)
					ELSE
						-1
			END) AS ScriptId
	FROM dbo.GroupCentreCandidate_view
	GROUP BY Id

GO
/****** Object:  View [dbo].[EscalatedUniqueGroupResponses_view]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





CREATE VIEW [dbo].[EscalatedUniqueGroupResponses_view]
WITH SCHEMABINDING
AS
	SELECT [ID]
	,[GroupDefinitionID]
	,[CheckSum]
	,[InsertionDate]
	,[CI_Review]
	,[CI]
	,[VIP]
	,[ConfirmedMark]
	,[ParkedAnnotation]
	,[ParkedDate]
	,[TokenId]
	,[ParkedUserID]
	,[ScriptType]
	,[Feedback]
	,[IsEscalated]
	,[OriginalUniqueGroupResponseId]
    FROM dbo.UniqueGroupResponses
	WHERE IsEscalated = 1
GO
SET ARITHABORT ON
SET CONCAT_NULL_YIELDS_NULL ON
SET QUOTED_IDENTIFIER ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
SET NUMERIC_ROUNDABORT OFF
GO
/****** Object:  Index [PK_EscalatedUniqueGroupResponses_view]    Script Date: 30/08/2017 08:26:38 ******/
CREATE UNIQUE CLUSTERED INDEX [PK_EscalatedUniqueGroupResponses_view] ON [dbo].[EscalatedUniqueGroupResponses_view]
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  View [dbo].[EscalatedUniqueGroupResponsesForCandidateCentre_view]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[EscalatedUniqueGroupResponsesForCandidateCentre_view]
WITH SCHEMABINDING
AS
	SELECT UGR.ParkedUserID AS ExaminerId
		, UGR.ParkedDate AS DateEscalated
		, UGR.ID AS ID
		, UGR.ParkedUserID as ParkedUserId
		, UGR.GroupDefinitionId
		, UGR.ParkedDate
		, T.CandidateName AS CandidateName
		, T.CandidateRef AS CandidateRef
		, T.CentreName AS CentreName
		, T.CentreCode AS CentreCode
		, T.ScriptId AS ScriptId
	FROM dbo.EscalatedUniqueGroupResponses_view UGR
		INNER JOIN dbo.CandidateCentreResponse_view T ON T.ID = UGR.ID

GO
/****** Object:  View [dbo].[GroupIdExamVersionName_view]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW [dbo].[GroupIdExamVersionName_view]
WITH SCHEMABINDING
AS
	SELECT GDI.GroupID
			, EV.Name
			, EV.ExternalExamReference
	FROM dbo.GroupDefinitionItems GDI
		JOIN dbo.Items I 
			ON (I.ID = GDI.ItemID)
		JOIN dbo.ExamVersionItemsRef EVIR
			ON EVIR.ItemID = I.ID
		JOIN dbo.ExamVersions EV
			ON (EV.ID = EVIR.ExamVersionID)


GO
/****** Object:  View [dbo].[ExamVersionNames_view]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[ExamVersionNames_view]
WITH SCHEMABINDING
AS
	SELECT groupSource.ExamID as ExamId
				, groupSource.Id as GroupId
				, GEV.Name as VersionName
				, GEV.ExternalExamReference
            FROM dbo.GroupDefinitions groupSource
			JOIN dbo.GroupIdExamVersionName_view GEV
				ON GEV.GroupId = groupSource.Id


GO
/****** Object:  View [dbo].[ExamVersionNamesConcatenated_view]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO






CREATE VIEW [dbo].[ExamVersionNamesConcatenated_view]
WITH SCHEMABINDING
AS
	SELECT GD.ID AS GroupId,
		   GD.ExamID AS ExamId,
		   STUFF(
			(SELECT ', ' + VN.VersionName
				FROM dbo.ExamVersionNames_view VN
				WHERE VN.GroupId = GD.ID AND ExamID = GD.ExamID
				GROUP BY VN.VersionName
				ORDER BY VN.VersionName
				FOR XML PATH(''), TYPE
			).value('.','nvarchar(max)')
			,1,2, ''
          ) as VersionNames,
		   STUFF(
			(SELECT ', ' + VN.ExternalExamReference
				FROM dbo.ExamVersionNames_view VN
				WHERE VN.GroupId = GD.ID AND ExamID = GD.ExamID
				GROUP BY VN.ExternalExamReference
				ORDER BY VN.ExternalExamReference
				FOR XML PATH(''), TYPE
			).value('.','nvarchar(max)')
			,1,2, '') as ExternalExamReferences
	FROM dbo.GroupDefinitions GD

GO
/****** Object:  UserDefinedFunction [dbo].[getCentreNameAndCodeForUniqueGroupResponse_fn]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE FUNCTION [dbo].[getCentreNameAndCodeForUniqueGroupResponse_fn]
(	
	@uniqueGroupResponseId INT
)
RETURNS TABLE
WITH SCHEMABINDING
AS
RETURN 
(
	SELECT CentreName
		, CentreCode
		, Id as UniqueGroupResponseId
		FROM [dbo].[GroupCentreCandidate_view]
		WHERE Id = @uniqueGroupResponseId
)


GO
/****** Object:  UserDefinedFunction [dbo].[sm_getExamPermissionsForEscalations_fn]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[sm_getExamPermissionsForEscalations_fn]
(
	@userId INT
)
RETURNS TABLE 
WITH SCHEMABINDING
RETURN
    
SELECT aur.ExamID AS 'ID'
FROM   dbo.AssignedUserRoles aur
       INNER JOIN dbo.RolePermissions rp ON  aur.RoleID = rp.RoleID
       INNER JOIN dbo.Permissions p ON  rp.PermissionID = p.ID
WHERE  aur.ExamID<>0
       AND aur.UserID = @userId
       AND rp.PermissionID = 45 -- Escalations (tab)
		       
		UNION

SELECT e.ID       AS 'ID'
FROM   dbo.Exams     e
WHERE  e.ID<>0
       AND EXISTS
           (
               SELECT aur.RoleID
               FROM   dbo.AssignedUserRoles aur
                      INNER JOIN dbo.RolePermissions rp ON  aur.RoleID = rp.RoleID
                      INNER JOIN dbo.Permissions p ON  rp.PermissionID = p.ID
               WHERE  aur.ExamID = 0
                      AND aur.UserID = @userId
                      AND p.GlobalSupport = 1
                      AND rp.PermissionID = 45 -- Escalations (tab)
           )
    
GO
/****** Object:  UserDefinedFunction [dbo].[getGroupDefinitionsForEscalations_fn]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE FUNCTION [dbo].[getGroupDefinitionsForEscalations_fn]
(	
	@userId INT
)
RETURNS TABLE
WITH SCHEMABINDING
AS
RETURN 
(
	SELECT [ID]
	,[Name]
	,[ExamID]
	FROM dbo.GroupDefinitions GD
	WHERE EXISTS 
		(
			SELECT GDS.Status 
			FROM dbo.GroupDefinitionStructureCrossRef GDS 
			WHERE GDS.GroupDefinitionID = GD.ID 
				AND (GDS.Status = 0 OR GDS.Status = 2)
		) 
	AND EXISTS (SELECT ID FROM dbo.sm_getExamPermissionsForEscalations_fn(@userId) WHERE ID = GD.ExamID)
)


GO
/****** Object:  View [dbo].[QualificationExams_view]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[QualificationExams_view]
WITH SCHEMABINDING
AS
	SELECT Q.Name AS Qualification
		, Q.ID AS QualificationId
		, E.Name AS Exam
		, E.ID AS ExamId
		FROM dbo.Exams E
			INNER JOIN dbo.Qualifications Q ON Q.ID = E.QualificationID

GO
/****** Object:  UserDefinedFunction [dbo].[sm_EscalatedUGRs_fn]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE FUNCTION [dbo].[sm_EscalatedUGRs_fn]
(
	@userId INT
)
RETURNS TABLE
RETURN
(
	SELECT
		QE.Qualification, 
		QE.QualificationId, 
		QE.Exam, 
		QE.ExamId, 
		EVNC.VersionNames AS 'ExamVersion', 
		0 AS 'ExamVersionId', 
		GD.Name AS 'ItemName', 
		GD.ID AS 'ItemId', 
		(
			SELECT U.Surname + ' ' + U.Forename FROM dbo.Users U WHERE U.ID = EUGR.ParkedUserID
		) AS 'Examiner',
		EUGR.ParkedUserID AS 'ExaminerId',
		EUGR.ParkedDate AS 'DateEscalated',
		EUGR.ID AS 'ID',
		(
			SELECT RTRIM(UGRPRCR.ParkedReasonId) + ', '
			FROM dbo.UniqueGroupResponseParkedReasonCrossRef UGRPRCR
			WHERE UGRPRCR.UniqueGroupResponseId = EUGR.ID
			FOR XML PATH('')
		) AS 'ReasonIds',	
		(
			SELECT RTRIM(PRL.Name) + ', '
			FROM dbo.ParkedReasonsLookup PRL
			INNER JOIN dbo.UniqueGroupResponseParkedReasonCrossRef UGRPRCR ON UGRPRCR.ParkedReasonId = PRL.ID
			WHERE UGRPRCR.UniqueGroupResponseId = EUGR.ID
			FOR XML PATH('')
		) AS 'Issues',
		( 
			SELECT DISTINCT RTRIM(CentreName) + ', '
			FROM dbo.getCentreNameAndCodeForUniqueGroupResponse_fn(EUGR.ID)
			FOR XML PATH('')
				
		) AS 'Centres',
        (
            SELECT DISTINCT RTRIM(CentreCode) + ', '
            FROM dbo.getCentreNameAndCodeForUniqueGroupResponse_fn(EUGR.ID)
            FOR XML PATH('')
		) AS 'CentresCodes',
		(
			SELECT COUNT(CGR.CandidateExamVersionID)
			FROM dbo.CandidateGroupResponses CGR
			WHERE CGR.UniqueGroupResponseID = EUGR.ID
		) AS 'CountOfScripts',
		-- returns empty string when UGR contains more then one Script, for Filters work correctly
		EUGR.CandidateName AS 'CandidateName',
		EUGR.CandidateRef AS 'CandidateRef',
		EUGR.CentreName AS 'CentreName',
		EUGR.CentreCode AS 'CentreCode',
		EUGR.ScriptId AS 'ScriptId',
		(
			CASE WHEN 0 < 
				(
					SELECT COUNT(*)
					FROM dbo.RolePermissions RP
					INNER JOIN dbo.AssignedUserRoles AUR ON Rp.RoleID = AUR.RoleID
					WHERE AUR.UserID = @userId 
						AND (AUR.ExamID = GD.ExamID OR AUR.ExamID = 0) 
						AND RP.PermissionID = 46
				)
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END
		) AS 'IsAllowViewWholeScript'
		 
	FROM dbo.getGroupDefinitionsForEscalations_fn(@userId) GD
		INNER JOIN dbo.EscalatedUniqueGroupResponsesForCandidateCentre_view EUGR ON GD.ID = EUGR.GroupDefinitionID
		INNER JOIN dbo.QualificationExams_view QE ON QE.ExamId = GD.ExamId
		INNER JOIN dbo.ExamVersionNamesConcatenated_view EVNC ON EVNC.ExamId = GD.ExamID AND EVNC.GroupId = GD.ID
	WHERE EXISTS 
		(
			SELECT GDS.GroupDefinitionID
			FROM dbo.GroupDefinitionStructureCrossRef GDS 
			WHERE GDS.GroupDefinitionID = GD.ID 
			AND (GDS.Status = 0 OR GDS.Status = 2)
		) 
	AND EXISTS (SELECT ID FROM dbo.sm_getExamPermissionsForEscalations_fn(@userId) WHERE ID = GD.ExamID)

)
GO
/****** Object:  UserDefinedFunction [dbo].[getGroupAggregatedSourceForHome_fn]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[getGroupAggregatedSourceForHome_fn]
(	
	@userId INT,
	@tokenStoreID INT
)
RETURNS TABLE
WITH SCHEMABINDING
AS
RETURN 
(
	SELECT ID as GroupId
				, GroupName
				, ExamID
				, MIN(InsertionDate) AS EarliestDate
				, COUNT(UniqueGroupResponseId) AS NumResponses
				, SUM(VIP)/CAST(COUNT(UniqueGroupResponseId) AS FLOAT) as VIP
				, ISNULL(MAX(Suspended),0) AS Suspended
			FROM dbo.getGroupSourceTable_fn(@userId, @tokenStoreID, 1)
			WHERE IsReleased = 1 -- for Home we have to get only groups with Released state
			GROUP BY ExamID
					,ID
					,GroupName
					,IsMarkOneItemPerScript
			HAVING COUNT(UniqueGroupResponseId) > 0
)



GO
/****** Object:  UserDefinedFunction [dbo].[getGroupsWithAffectedResponsesForHome_fn]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- User Defined Function

CREATE FUNCTION [dbo].[getGroupsWithAffectedResponsesForHome_fn]
(	
	@userId INT,
	@tokenStoreID INT
)
RETURNS TABLE
WITH SCHEMABINDING
AS
RETURN 
(
	SELECT GAS.GroupId
				, GroupName
				, ExamID
				, EarliestDate
				, NumResponses
				, VIP
				, Suspended
				, ResponsesCount as AffectedUniqueResponses
				, NumResponses - ISNULL(ResponsesCount, 0) as ResponsesDif
			FROM dbo.getGroupAggregatedSourceForHome_fn(@userId, @tokenStoreID) AS GAS
				LEFT JOIN dbo.getAffectedUniqueResponses_fn(@userId, @tokenStoreID) AS AUR ON AUR.GroupId = GAS.GroupId
)







GO
/****** Object:  UserDefinedFunction [dbo].[sm_getQuotaManagementInfoForUser_fn]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[sm_getQuotaManagementInfoForUser_fn]
(
	@examId INT
)
RETURNS TABLE
AS
    
RETURN
(

	SELECT  QM.UserId							AS 'UserId',
			ISNULL(SUM(QM.NumberMarked), 0)		AS 'NumberMarked',
			(CASE
				WHEN 
					ISNULL(SUM(CAST(QM.MarkingSuspended AS INT)), 0) > 0
				THEN 1
				ELSE 0
			END)								AS 'MarkerSuspended',
			(CASE
				WHEN 
					ISNULL(SUM(CAST(QM.Within1CIofSuspension AS INT)), 0) > 0
				THEN 1
				ELSE 0
			END)								AS 'MarkerCloseToBeingSuspended',
			(CASE
				WHEN 
					ISNULL(SUM(QM.NumItemsParked), 0) > 0
				THEN 1
				ELSE 0
			END)								AS 'Parked',
			ISNULL(SUM(QM.NumberSuspended), 0)	AS 'NumberSuspended'
		
	FROM    QuotaManagement QM
			INNER JOIN GroupDefinitions GD ON QM.GroupId = GD.ID
	WHERE   GD.ExamID = @examId  
			AND
			EXISTS 
			(
				SELECT	* 
				FROM	GroupDefinitionStructureCrossRef GDSCR
						INNER JOIN ExamVersionStructures EVS ON EVS.ID = GDSCR.ExamVersionStructureID			
				WHERE	GDSCR.Status = 0 
						AND GDSCR.GroupDefinitionID = GD.ID 
						AND EVS.StatusID = 0
			)  
	GROUP BY QM.UserId    
)
GO
/****** Object:  UserDefinedFunction [dbo].[sm_hasUserRealMarkedResponsesForExam_fn]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE FUNCTION [dbo].[sm_hasUserRealMarkedResponsesForExam_fn]
(
	@examId INT
)
RETURNS TABLE
AS
 
RETURN
(
    SELECT U.ID  AS 'UserId'
          ,CAST(
               CASE 
                    WHEN SUM(
                             CASE 
                                  WHEN AGM.IsConfirmedMark=1
									   AND UGR.CI=0
								  THEN 1 ELSE 0 
                             END
                         )>0 THEN 1
                    ELSE 0
               END AS BIT
           )     AS 'hasRealMarkedResponses'
    FROM   Users U
           INNER JOIN dbo.AssignedGroupMarks AGM ON  U.ID = AGM.UserId
           INNER JOIN dbo.UniqueGroupResponses UGR ON  AGM.UniqueGroupResponseId = UGR.ID
           INNER JOIN dbo.GroupDefinitions GD ON  GD.ID = UGR.GroupDefinitionID
    WHERE  GD.ExamID = @examId
           AND EXISTS (
                   SELECT *
                   FROM   dbo.GroupDefinitionStructureCrossRef GDSCR
                          INNER JOIN dbo.ExamVersionStructures EVS ON  EVS.ID = GDSCR.ExamVersionStructureID
                   WHERE  GDSCR.Status = 0
                          AND GDSCR.GroupDefinitionID = GD.ID
                          AND EVS.StatusID = 0
               )
    GROUP BY
           U.ID
)


GO
/****** Object:  UserDefinedFunction [dbo].[sm_MANAGEEXAMINERS_GetExaminers_fn]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[sm_MANAGEEXAMINERS_GetExaminers_fn]
(
	@examId INT
)
RETURNS TABLE 
RETURN
(
	SELECT ID,
       Forename,
       Surname,
       (
           SELECT (RTRIM(NAME))
           FROM   Roles
           WHERE  Id IN (SELECT RoleId
                         FROM   AssignedUserRoles
                         WHERE  UserId = Users.ID
                                AND AssignedUserRoles.ExamID = @examId) FOR XML PATH('')
       )  AS RolesNames,
       (
           SELECT (RTRIM(RoleId) + ',')
           FROM   AssignedUserRoles
           WHERE  UserId = ID
                  AND AssignedUserRoles.ExamID = @examId FOR XML PATH('')
       )  AS Roles,
       (
           CASE 
                WHEN EXISTS(
                         SELECT RoleID
                         FROM   AssignedUserRoles
                                INNER JOIN Roles
                                     ON  AssignedUserRoles.RoleId = Roles.ID
                         WHERE  AssignedUserRoles.UserID = Users.ID
                                AND Roles.TrainingRole = 1
                                AND AssignedUserRoles.ExamID = @examId
                     ) THEN 1
                ELSE 0
           END
       )  AS Training,
       Retired,
       InternalUser
FROM   Users
)

GO
/****** Object:  View [dbo].[CandidateExamVersionGroups_view]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[CandidateExamVersionGroups_view]
WITH SCHEMABINDING
AS
SELECT  UGR2.GroupDefinitionID, CGR2.CandidateExamVersionID, COUNT_BIG(*) AS CTBIG
FROM dbo.CandidateGroupResponses CGR2
 INNER JOIN dbo.UniqueGroupResponses UGR2 ON UGR2.ID = CGR2.UniqueGroupResponseID
GROUP BY UGR2.GroupDefinitionID, CGR2.CandidateExamVersionID
GO
SET ARITHABORT ON
SET CONCAT_NULL_YIELDS_NULL ON
SET QUOTED_IDENTIFIER ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
SET NUMERIC_ROUNDABORT OFF
GO
/****** Object:  Index [CEVG_IDX]    Script Date: 30/08/2017 08:26:38 ******/
CREATE UNIQUE CLUSTERED INDEX [CEVG_IDX] ON [dbo].[CandidateExamVersionGroups_view]
(
	[GroupDefinitionID] ASC,
	[CandidateExamVersionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  View [dbo].[ControlGroups_view]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





CREATE VIEW [dbo].[ControlGroups_view]
AS
	WITH cteCreator AS
	(
		SELECT
			 AGM.UniqueGroupResponseId
			,AGM.Timestamp AS CreationDate
			,U.id AS CreatorID
			,CASE 
				WHEN U.ID IS NOT NULL THEN U.Surname + ', ' + U.Forename
				ELSE AGM.FullName
			 END AS CreatorFullName 
		FROM dbo.AssignedGroupMarks AGM
		LEFT JOIN dbo.Users U ON U.ID = AGM.UserId 
		WHERE AGM.MarkingMethodId = 5 and AGM.IsConfirmedMark = 1
	),
	cteReviewer AS
	(
		SELECT
			 AGM.UniqueGroupResponseId
			,AGM.Timestamp AS ReviewDate
			,U.id AS ReviewerID
			,CASE 
				WHEN U.ID IS NOT NULL THEN U.Surname + ', ' + U.Forename
				ELSE AGM.FullName
			 END AS ReviewerFullName
		FROM dbo.AssignedGroupMarks AGM
		LEFT JOIN dbo.Users U ON U.ID = AGM.UserId 
		WHERE AGM.ID = (SELECT MAX(ID) FROM dbo.AssignedGroupMarks AGM1 
						WHERE AGM1.UniqueGroupResponseId = AGM.UniqueGroupResponseId
						AND AGM1.MarkingMethodId = 6)
	)
	,ctePresented as
	(
		SELECT
			 UniqueGroupResponseId 
			,COUNT(*) as CNT
		FROM dbo.AssignedGroupMarks AGM
		WHERE AGM.MarkingMethodId IN (2, 3)
		GROUP BY UniqueGroupResponseId 
	)
	,cteFailed as
	(
		SELECT
			 UniqueGroupResponseId 
			,COUNT(*) as CNT
		FROM dbo.AssignedGroupMarks AGM
			JOIN dbo.UniqueGroupResponses UGR ON UGR.ID = AGM.UniqueGroupResponseId
			JOIN dbo.GroupDefinitions GD on GD.ID = UGR.GroupDefinitionID
		WHERE 
			AGM.MarkingMethodId IN (2, 3)
			AND (AGM.IsEscalated = 1 OR 
			dbo.sm_checkGroupAndItemsMarkedInTolerance_fn(
				AGM.ID, 
				AGM.UniqueGroupResponseId,
				AGM.MarkingDeviation, 
				GD.CIMarkingTolerance, GD.IsAutomaticGroup) = 1
			)
		GROUP BY UniqueGroupResponseId 
	)
	,cteNotExact as
	(
		SELECT
			 UniqueGroupResponseId 
			,COUNT(*) as CNT
		FROM dbo.AssignedGroupMarks AGM
			JOIN dbo.UniqueGroupResponses UGR ON UGR.ID = AGM.UniqueGroupResponseId
			JOIN dbo.GroupDefinitions GD on GD.ID = UGR.GroupDefinitionID
		WHERE 
			AGM.MarkingMethodId IN (2, 3)
			AND AGM.IsEscalated = 0
			AND (AGM.MarkingDeviation > 0 AND 
			dbo.sm_checkGroupAndItemsMarkedInTolerance_fn(
				AGM.ID, 
				AGM.UniqueGroupResponseId,
				AGM.MarkingDeviation, 
				GD.CIMarkingTolerance, GD.IsAutomaticGroup) = 0)
		GROUP BY UniqueGroupResponseId 
	)
	,cteMeanMark as
	(
		SELECT
			 UniqueGroupResponseId 
			,CASE
				WHEN COUNT(*) <> 0 THEN SUM(AGM.AssignedMark) / COUNT(*)
				ELSE 0
			 END as MeanMark
		FROM dbo.AssignedGroupMarks AGM
		WHERE 
			AGM.MarkingMethodId IN (2, 3)
			AND AGM.IsEscalated = 0
		GROUP BY UniqueGroupResponseId 
	)
	,cteIsInConfirmedScript AS
	(
		SELECT 
			   CGR.UniqueGroupResponseID
			  ,1 AS IsInConfirmedScript
		FROM   dbo.CandidateGroupResponses CGR
			   INNER JOIN dbo.CandidateExamVersions CEV ON  CEV.ID = CGR.CandidateExamVersionID
		WHERE  CEV.ExamSessionState IN (6 ,7)
	)

SELECT DISTINCT
	 UGR.ID AS UniqueGroupResponseId
	,GD.ID AS GroupDefinitionID
	,UGR.ConfirmedMark
	,GD.TotalMark
    ,CAST(ISNULL(CONFIRMEDSCRIPTS.IsInConfirmedScript ,0) AS BIT) AS IsInConfirmedScript
	,CAST((
		CASE
			WHEN UGR.Feedback IS NULL OR UGR.Feedback = '' THEN 0
            ELSE 1
        END
	) AS BIT) AS 'Feedback'

	-- Statistics
	,ISNULL(PRESENTED.CNT, 0) AS NumPresented
	,CASE
			WHEN ISNULL(PRESENTED.CNT, 0) = 0 THEN 0
            ELSE CAST(ROUND(ISNULL(FAILED.CNT, 0)*100/CAST(PRESENTED.CNT AS FLOAT),0) AS INT) 
     END AS 'FailedPercent'     
	,CASE
			WHEN ISNULL(PRESENTED.CNT, 0) = 0 THEN 0
            ELSE CAST(ROUND(ISNULL(NOTEXACT.CNT, 0)*100/CAST(PRESENTED.CNT AS FLOAT),0) AS INT)
     END AS 'PassedNotExactPercent'	
	,ISNULL(MEANMARK.MeanMark, 0) AS MeanMark

	-- Creator
	,CRE.CreationDate
	,CRE.CreatorID
	,CRE.CreatorFullName

	-- Reviewer
	,REV.ReviewDate
	,REV.ReviewerID
	,REV.ReviewerFullName
FROM
	dbo.UniqueGroupResponses AS UGR 
	INNER JOIN dbo.GroupDefinitions AS GD ON UGR.GroupDefinitionID = GD.ID 
	LEFT JOIN cteCreator CRE ON CRE.UniqueGroupResponseId = UGR.ID
	LEFT JOIN cteReviewer REV ON REV.UniqueGroupResponseId = UGR.ID
	-- Statistics
	LEFT JOIN ctePresented PRESENTED ON  UGR.ID = PRESENTED.UniqueGroupResponseId 
	LEFT JOIN cteFailed FAILED ON  UGR.ID = FAILED.UniqueGroupResponseId 
	LEFT JOIN cteNotExact NOTEXACT ON  UGR.ID = NOTEXACT.UniqueGroupResponseId 
	LEFT JOIN cteMeanMark MEANMARK ON  UGR.ID = MEANMARK.UniqueGroupResponseId
    LEFT JOIN cteIsInConfirmedScript CONFIRMEDSCRIPTS ON  UGR.ID = CONFIRMEDSCRIPTS.UniqueGroupResponseId	 	
WHERE     
	UGR.CI = 1 AND UGR.CI_Review = 1



GO
/****** Object:  View [dbo].[EscalatedGroups_view]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Anton Burdyugov
-- Create date: 2013-06-26
-- Description:	Data about escalated(parked) groups.

-- Author:		Anton Burdyugov
-- Create date: 2013-07-03
-- Description:	Data from deleted user
-- =============================================
CREATE VIEW [dbo].[EscalatedGroups_view]
AS

SELECT GD.ExamID                     AS ExamID
      ,UGR.Id                        AS ID
      ,GD.Name                       AS NAME
      ,ISNULL(U.Surname+', '+U.Forename ,'Unknown') AS Examiner
      ,UGR.ParkedDate                AS ParkedDate
      ,UGR.ParkedAnnotation          AS ParkedAnnotation
      ,ISNULL(U.Surname ,'Unknown')  AS Surname
      ,ISNULL(U.Id ,-1)              AS UserID
      ,(
            SELECT RTRIM(UGRPRCS.ParkedReasonId) + ',' 
            FROM UniqueGroupResponseParkedReasonCrossRef UGRPRCS 
            WHERE UGRPRCS.UniqueGroupResponseId = UGR.Id 
            FOR XML PATH('')
        ) AS ParkedReasonIDs
      ,(
            SELECT RTRIM(PRL.Name) + ', ' 
            FROM UniqueGroupResponseParkedReasonCrossRef UGRPRCS 
				INNER JOIN ParkedReasonsLookup PRL ON UGRPRCS.ParkedReasonId = PRL.ID
            WHERE UGRPRCS.UniqueGroupResponseId = UGR.Id 
            ORDER BY PRL.Id
            FOR XML PATH('')
        ) AS ParkedReasons
       ,
		(
			SELECT DISTINCT RTRIM(CEV.CountryName) + ', '
			FROM CandidateExamVersions CEV
			INNER JOIN CandidateGroupResponses CGR
			ON CGR.CandidateExamVersionID = CEV.ID
			WHERE CGR.UniqueGroupResponseID = UGR.ID
			FOR XML PATH('')
		)												AS 'Countries'
       ,(
			SELECT DISTINCT RTRIM(CEV.CentreName) + ', '
			FROM CandidateExamVersions CEV
			INNER JOIN CandidateGroupResponses CGR
			ON CGR.CandidateExamVersionID = CEV.ID
			WHERE CGR.UniqueGroupResponseID = UGR.ID
			FOR XML PATH('')
		)												AS 'Centres'
        
FROM   UniqueGroupResponses UGR
       INNER JOIN GroupDefinitions GD
            ON  GD.ID = UGR.GroupDefinitionID
       LEFT JOIN Users U
            ON  U.ID = UGR.ParkedUserID
WHERE  UGR.IsEscalated = 1
       AND (U.InternalUser=0 OR U.Username = 'superuser' OR U.ID IS NULL)
       AND EXISTS (
               SELECT *
               FROM   GroupDefinitionStructureCrossRef GDSCR
                      INNER JOIN ExamVersionStructures EVS
                           ON  EVS.ID = GDSCR.ExamVersionStructureID
               WHERE  GDSCR.Status = 0
                      AND GDSCR.GroupDefinitionID = GD.ID
                      AND EVS.StatusID = 0
           )





GO
/****** Object:  View [dbo].[GroupMarkedResponses_view]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Anton Burdyugov
-- Create date: 20/05/2013
-- Description:	Data about group marked responses.

-- Modifier:	Vitaly Bevzik
-- Modify date: 24/06/2013
-- Description:	Add responses wich were disagree from Review Ci
-- =============================================
CREATE VIEW [dbo].[GroupMarkedResponses_view]
AS
SELECT AM.id                          AS AssignedMarkID
      ,UGR.ID                         AS UniqueGroupResponseID
      ,ISNULL(U.ID ,-1)               AS UserID
      ,ISNULL(U.Surname ,AM.Surname)  AS Surname      
      ,GD.TotalMark                   AS TotalMark
      ,AM.assignedMark                AS AssignedMark
      ,AM.timestamp                   AS TIMESTAMP
      ,GD.Name                        AS GroupName
      ,ISNULL(U.Surname+', '+U.Forename ,AM.fullName) AS FullName
      ,GD.ExamID                      AS ExamID
      ,UGR.GroupDefinitionID          AS GroupID
      ,dbo.sm_checkIsUniqueGroupResponseInConfirmedScript(UGR.ID) AS IsInConfirmedScript
      ,UGR.CI						  AS CI
FROM   [dbo].[AssignedGroupMarks] AM
       LEFT OUTER JOIN [dbo].[Users] U ON  AM.userId = U.ID
       INNER JOIN [dbo].[UniqueGroupResponses] UGR ON  AM.uniqueGroupResponseId = UGR.ID
       INNER JOIN [dbo].[GroupDefinitions] GD ON  UGR.GroupDefinitionID = GD.ID
WHERE  U.ID <> 0
       AND AM.ID IN 
       (
			SELECT TOP 1 AGM.ID
			FROM AssignedGroupMarks AGM
			WHERE AGM.UniqueGroupResponseId = UGR.ID
			AND AGM.IsConfirmedMark = 1
			AND AGM.MarkingMethodId IN (4, 7, 15, 17)
			ORDER BY Timestamp DESC
	   )
       AND EXISTS ( SELECT TOP 1 *
                         FROM   dbo.GroupDefinitionStructureCrossRef AS CR
                                INNER JOIN dbo.ExamVersionStructures AS EVS ON EVS.ID = CR.ExamVersionStructureID
                         WHERE  ( CR.Status = 0 )
                                AND ( CR.GroupDefinitionID = GD.ID )
                                AND ( EVS.StatusID = 0 ) )









GO
/****** Object:  View [dbo].[MarkedCIGroups_view]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



/* =============================================
 Author:		Vitaly Bevzik
 Create date: 2013-05-30
 Description:	Data about group marked.
 =============================================*/
CREATE VIEW [dbo].[MarkedCIGroups_view]
AS

WITH cteIsTolerant AS
(
    SELECT AGM.ID
          ,AGM.UniqueGroupResponseId
          ,1 AS 'NotInTolerance'
    FROM   dbo.AssignedGroupMarks AGM
           INNER JOIN dbo.GroupDefinitions GD ON  GD.ID = AGM.GroupDefinitionID
    WHERE  AGM.MarkingDeviation>GD.CIMarkingTolerance
           OR (
                  AGM.MarkingDeviation<=GD.CIMarkingTolerance
                  AND GD.IsAutomaticGroup=0
                  AND EXISTS(
                          SELECT *
                          FROM   dbo.AssignedItemMarks AIM
                                 INNER JOIN dbo.UniqueResponses UR ON  UR.id = aim.UniqueResponseId
                                 INNER JOIN dbo.GroupDefinitionItems GDI ON  GDI.ItemID = UR.itemId
                                 INNER JOIN dbo.Items I ON  I.ID = GDI.itemId
                          WHERE  AIM.GroupMarkId = AGM.ID
                                 AND GDI.GroupID = GD.ID
                                 AND GDI.ViewOnly = 0
                                 AND AIM.MarkingDeviation>I.CIMarkingTolerance
                      )
              )
)
	
	
SELECT UGR.ID                           AS 'UniqueGroupResponseID'
      ,GD.ID                            AS 'GroupID'
      ,GD.Name                          AS 'GroupName'
      ,GD.TotalMark                     AS 'TotalMark'
      ,GD.CIMarkingTolerance            AS 'CIMarkingTolerance'
      ,GD.ExamID                        AS 'ExamID'
      ,ISNULL(U.Surname ,AGM.Surname)   AS 'Surname'
      ,ISNULL(U.Surname+', '+U.Forename ,AGM.FullName) AS 'Fullname'
      ,ISNULL(U.ID ,-1)                 AS 'UserId'
      ,AGM.ID                           AS 'AssignedMarkID'
      ,AGM.Timestamp                    AS 'Timestamp'
      ,AGM.AssignedMark                 AS 'AssignedMark'
      ,AGM.MarkingDeviation             AS 'MarkingDeviation'
      ,(
           CASE 
                WHEN UGR.ConfirmedMark IS NULL THEN (
                         SELECT TOP 1 AM.AssignedMark
                         FROM   dbo.AssignedGroupMarks AM
                         WHERE  AM.UniqueGroupResponseId=UGR.ID 
                                AND AM.MarkingMethodId=6
                         ORDER BY
                                TIMESTAMP DESC
                     )
                ELSE UGR.ConfirmedMark
           END
       )                                AS 'ConfirmedMark'
      ,AGM.MarkingMethodId              AS 'MarkingMethod'
      ,CAST(ISNULL(IT.NotInTolerance ,0) AS BIT) AS 'IsOutOfTolerance'
      ,AGM.IsEscalated                  AS 'IsEscalated'
FROM   dbo.AssignedGroupMarks           AGM
       LEFT OUTER JOIN dbo.Users U ON  AGM.UserId = U.ID
       INNER JOIN dbo.UniqueGroupResponses UGR ON  AGM.UniqueGroupResponseId = UGR.ID
       INNER JOIN dbo.GroupDefinitions  GD ON  GD.ID = UGR.GroupDefinitionID
       LEFT JOIN cteIsTolerant IT ON  IT.ID = AGM.ID
                AND IT.UniqueGroupResponseId = AGM.UniqueGroupResponseId
WHERE  (AGM.MarkingMethodId=2 OR AGM.MarkingMethodId=3)
       AND UGR.CI_Review = 1
       AND EXISTS (
               SELECT TOP 1 *
               FROM   dbo.GroupDefinitionStructureCrossRef CR
                      INNER JOIN dbo.ExamVersionStructures EVS ON  EVS.ID = CR.ExamVersionStructureID
               WHERE  CR.Status=0
                      AND CR.GroupDefinitionID=GD.ID
                      AND EVS.StatusID=0
           )



GO
/****** Object:  View [dbo].[MarkingReport_view]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO











CREATE VIEW  [dbo].[MarkingReport_view] 
	AS
SELECT I.ID                      AS ID
	  ,I.ExternalItemID          AS ExternalItemId
	  ,I.ExternalItemName        AS Name
	  ,Q.Reference               AS QualificationRef
	  ,Q.Name                    AS Qualification
	  ,Q.ID						 AS QualificationId
	  ,E.Reference               AS ExamRef
	  ,E.Name                    AS Exam
	  ,E.ID	                     AS ExamId
	  ,AGM.UserId				 AS UserId
	  ,U.Username                AS Username
	  ,U.Forename                AS Forename
	  ,U.Surname                 AS Surname
	  ,AGM.GroupDefinitionID     AS GroupDefinitionId
	  ,AGM.UniqueGroupResponseId AS UniqueGroupResponseId
	  ,CASE WHEN (AGM.MarkingMethodId = 11 AND AGM.EscalatedWithoutMark = 1) THEN 1 ELSE 0 END  AS EscalatedWithoutMark
	  ,(CASE WHEN AGM.MarkingMethodId IN (2 ,3) THEN 1 ELSE 0 END) AS ControlItem
	  ,(CASE WHEN AGM.MarkingMethodId NOT IN (2 ,3) THEN 1 ELSE 0 END) AS UniqueResponse
	  ,AGM.Timestamp             AS Timestamp
FROM   dbo.GroupDefinitionItems GDI
	   JOIN dbo.Items I ON  I.ID = GDI.ItemID
	   JOIN dbo.AssignedGroupMarks AGM ON  AGM.GroupDefinitionID = GDI.GroupID
	   JOIN dbo.Exams E ON  E.ID = I.ExamID
	   JOIN dbo.Qualifications Q ON  Q.ID = E.QualificationID
	   LEFT JOIN dbo.Users U ON  U.id = AGM.UserId
WHERE  I.ID>0 AND GDI.ViewOnly = 0
		AND ((AGM.MarkingMethodId = 4 AND AGM.IsActualMarking = 1)
			OR (AGM.MarkingMethodId IN (2, 3, 10, 11))
			OR (AGM.MarkingMethodId = 5 AND AGM.IsReportableCI = 1))
	   AND EXISTS (
			   SELECT TOP 1 *
			   FROM   dbo.GroupDefinitionStructureCrossRef CR
					  INNER JOIN dbo.ExamVersionStructures EVS
						   ON  CR.ExamVersionStructureID = EVS.ID
			   WHERE  CR.GroupDefinitionID = GDI.GroupID
					  AND CR.Status IN (0 ,2)
					  AND EVS.StatusID = 0
		   )







GO
/****** Object:  View [dbo].[QuotaGroupCandidateResponses_view]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[QuotaGroupCandidateResponses_view]
WITH SCHEMABINDING
AS
	SELECT DISTINCT UGR.TokenId
			, GD.ID AS GroupId
			, CGR1.UniqueGroupResponseID
			, CGR1.CandidateExamVersionID
	FROM dbo.QuotaManagement QM
			JOIN dbo.GroupDefinitions GD ON  GD.ID = QM.GroupId
			JOIN dbo.UniqueGroupResponses UGR ON UGR.GroupDefinitionID = GD.ID
			JOIN dbo.CandidateGroupResponses CGR1 ON CGR1.UniqueGroupResponseID = UGR.ID
	WHERE UGR.IsEscalated = 0
		AND UGR.CI = 0
		AND UGR.confirmedMark IS NULL
GO
/****** Object:  View [dbo].[ScriptReview_view]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[ScriptReview_view]
AS
	SELECT CEV.ID                 AS 'ID'
          ,q.Name                 AS 'Qualification'
          ,q.ID                   AS 'QualificationID'
          ,e.Name                 AS 'Exam'
          ,e.ID                   AS 'ExamID'
          ,ev.Name                AS 'ExamVersion'
          ,ev.ID                  AS 'ExamVersionID'
          ,CEV.Keycode            AS 'Keycode'
          ,(
               CASE 
                    WHEN CEV.ExamSessionState IN (3 ,4 ,6 ,7) THEN CEV.Mark
                    ELSE - 1
               END
           )                      AS 'Mark'
          ,CEV.TotalMark          AS 'TotalMarks'
          ,ISNULL(
               CASE 
                    WHEN CEV.ExamSessionState IN (3 ,4 ,6 ,7) THEN CEV.Mark
                        *100/NULLIF(CEV.TotalMark ,0)
                    ELSE -1
               END
              ,0
           )                      AS 'Percentage'
          ,CEV.ExamSessionState   AS 'ExamSessionState'
          ,ISNULL(CEV.PercentageMarkingComplete ,0) AS 'PercentageMarked'
          ,CEV.CompletionDate	  AS 'Submitted'
          ,CEV.DateSubmitted      AS 'Imported'
          ,CEV.IsFlagged		  AS 'IsFlagged'
          ,CEV.VIP                AS 'VIP'
          ,CEV.CandidateSurname   AS 'CandidateSurname'
          ,CEV.CandidateForename  AS 'CandidateForename'
          ,CEV.CandidateForename + ' ' + CEV.CandidateSurname  AS 'CandidateFullName'          
          ,CEV.CandidateRef       AS 'CandidateRef'
          ,CEV.CentreName         AS 'CentreName'
          ,CEV.CentreCode         AS 'CentreCode'
          ,CEV.CountryName		  AS 'Country'
          ,CEV.IsModerated		  AS 'IsModerated'
      FROM   dbo.CandidateExamVersions CEV
           INNER JOIN dbo.ExamVersions ev
                ON  ev.ID = CEV.ExamVersionID
           INNER JOIN dbo.Exams e
                ON  e.ID = ev.ExamID
           INNER JOIN dbo.Qualifications q
                ON  q.ID = e.QualificationID


GO
/****** Object:  View [dbo].[ScriptToUpdate_view]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW [dbo].[ScriptToUpdate_view]
WITH SCHEMABINDING
AS
	SELECT CEV.ID, CEV.ExamSessionState, CEV.LastManagedDate
    FROM   dbo.CandidateExamVersions CEV
    WHERE CEV.ExamSessionState IN (1,2,3,4,5) 
GO
/****** Object:  Index [AssignedGroupMarks_MarkingReport]    Script Date: 30/08/2017 08:26:38 ******/
CREATE NONCLUSTERED INDEX [AssignedGroupMarks_MarkingReport] ON [dbo].[AssignedGroupMarks]
(
	[Timestamp] ASC
)
INCLUDE ( 	[UniqueGroupResponseId],
	[MarkingMethodId],
	[GroupDefinitionID],
	[IsActualMarking],
	[IsReportableCI],
	[EscalatedWithoutMark]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [AssignedGroupMarks_Suspending]    Script Date: 30/08/2017 08:26:38 ******/
CREATE NONCLUSTERED INDEX [AssignedGroupMarks_Suspending] ON [dbo].[AssignedGroupMarks]
(
	[UniqueGroupResponseId] ASC,
	[ID] ASC,
	[MarkingMethodId] ASC,
	[Timestamp] ASC,
	[UserId] ASC,
	[Disregarded] ASC
)
INCLUDE ( 	[MarkingDeviation]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [AssignedGroupMarks_UserId]    Script Date: 30/08/2017 08:26:38 ******/
CREATE NONCLUSTERED INDEX [AssignedGroupMarks_UserId] ON [dbo].[AssignedGroupMarks]
(
	[UserId] ASC
)
INCLUDE ( 	[GroupDefinitionID],
	[IsConfirmedMark],
	[MarkingMethodId],
	[UniqueGroupResponseId]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_AssignedGroupMarks_GroupDefinitionID]    Script Date: 30/08/2017 08:26:38 ******/
CREATE NONCLUSTERED INDEX [IX_AssignedGroupMarks_GroupDefinitionID] ON [dbo].[AssignedGroupMarks]
(
	[GroupDefinitionID] ASC
)
INCLUDE ( 	[MarkingMethodId],
	[Timestamp],
	[UserId]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_AssignedGroupMarks_TimeStamp]    Script Date: 30/08/2017 08:26:38 ******/
CREATE NONCLUSTERED INDEX [IX_AssignedGroupMarks_TimeStamp] ON [dbo].[AssignedGroupMarks]
(
	[Timestamp] DESC
)
INCLUDE ( 	[AssignedMark],
	[EscalatedWithoutMark],
	[FullName],
	[UniqueGroupResponseId],
	[UserId]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_AssignedGroupMarks_UniqueGroupResponseID]    Script Date: 30/08/2017 08:26:38 ******/
CREATE NONCLUSTERED INDEX [IX_AssignedGroupMarks_UniqueGroupResponseID] ON [dbo].[AssignedGroupMarks]
(
	[UniqueGroupResponseId] ASC
)
INCLUDE ( 	[AssignedMark],
	[EscalatedWithoutMark],
	[IsConfirmedMark],
	[MarkingDeviation],
	[MarkingMethodId],
	[Timestamp]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_CandidateExamVersions_ExamSessionState]    Script Date: 30/08/2017 08:26:38 ******/
CREATE NONCLUSTERED INDEX [IX_CandidateExamVersions_ExamSessionState] ON [dbo].[CandidateExamVersions]
(
	[ExamSessionState] ASC
)
INCLUDE ( 	[ExternalSessionID],
	[DateSubmitted],
	[VIP]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_CandidateExamVersions_ExamVersionID]    Script Date: 30/08/2017 08:26:38 ******/
CREATE NONCLUSTERED INDEX [IX_CandidateExamVersions_ExamVersionID] ON [dbo].[CandidateExamVersions]
(
	[ExamVersionID] ASC
)
INCLUDE ( 	[CandidateForename],
	[CandidateRef],
	[CandidateSurname],
	[CentreCode],
	[CentreName],
	[CompletionDate],
	[CountryName],
	[DateSubmitted],
	[ExamSessionState],
	[ID],
	[IsFlagged],
	[IsModerated],
	[Keycode],
	[Mark],
	[PercentageMarkingComplete],
	[TotalMark],
	[VIP]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_StateManagement]    Script Date: 30/08/2017 08:26:38 ******/
CREATE NONCLUSTERED INDEX [IX_StateManagement] ON [dbo].[CandidateExamVersions]
(
	[ExamSessionState] ASC
)
INCLUDE ( 	[ID],
	[LastManagedDate]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_CandidateGroupResponses_UniqueGroupResponseID]    Script Date: 30/08/2017 08:26:38 ******/
CREATE NONCLUSTERED INDEX [IX_CandidateGroupResponses_UniqueGroupResponseID] ON [dbo].[CandidateGroupResponses]
(
	[UniqueGroupResponseID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_CandidateResponses_UniqueResponseId]    Script Date: 30/08/2017 08:26:38 ******/
CREATE NONCLUSTERED INDEX [IX_CandidateResponses_UniqueResponseId] ON [dbo].[CandidateResponses]
(
	[UniqueResponseID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_ExamSections_ExamVersionID]    Script Date: 30/08/2017 08:26:38 ******/
CREATE NONCLUSTERED INDEX [IX_ExamSections_ExamVersionID] ON [dbo].[ExamSections]
(
	[ExamVersionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_ExamVersionItems_CandidateExamVersionID]    Script Date: 30/08/2017 08:26:38 ******/
CREATE NONCLUSTERED INDEX [IX_ExamVersionItems_CandidateExamVersionID] ON [dbo].[ExamVersionItems]
(
	[CandidateExamVersionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_ExamVersions_ExamID]    Script Date: 30/08/2017 08:26:38 ******/
CREATE NONCLUSTERED INDEX [IX_ExamVersions_ExamID] ON [dbo].[ExamVersions]
(
	[ExamID] ASC
)
INCLUDE ( 	[ID],
	[Name]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_GroupDefenitions_ExamId]    Script Date: 30/08/2017 08:26:38 ******/
CREATE NONCLUSTERED INDEX [IX_GroupDefenitions_ExamId] ON [dbo].[GroupDefinitions]
(
	[ExamID] ASC
)
INCLUDE ( 	[Name]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_ExamVersionStructureID_Status]    Script Date: 30/08/2017 08:26:38 ******/
CREATE NONCLUSTERED INDEX [IX_ExamVersionStructureID_Status] ON [dbo].[GroupDefinitionStructureCrossRef]
(
	[ExamVersionStructureID] ASC,
	[Status] ASC
)
INCLUDE ( 	[GroupDefinitionID]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_UniqueGroupResponses_CI_ConfirmedMark_IsEscalated_TokenId]    Script Date: 30/08/2017 08:26:38 ******/
CREATE NONCLUSTERED INDEX [IX_UniqueGroupResponses_CI_ConfirmedMark_IsEscalated_TokenId] ON [dbo].[UniqueGroupResponses]
(
	[CI] ASC,
	[ConfirmedMark] ASC,
	[IsEscalated] ASC,
	[TokenId] ASC
)
INCLUDE ( 	[GroupDefinitionID],
	[ID]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_UniqueGroupResponses_CI_ConfirmedMark_ParkedUserId_TokenId]    Script Date: 30/08/2017 08:26:38 ******/
CREATE NONCLUSTERED INDEX [IX_UniqueGroupResponses_CI_ConfirmedMark_ParkedUserId_TokenId] ON [dbo].[UniqueGroupResponses]
(
	[GroupDefinitionID] ASC,
	[CI] ASC,
	[ConfirmedMark] ASC,
	[ParkedUserID] ASC,
	[TokenId] ASC
)
INCLUDE ( 	[ID],
	[InsertionDate],
	[VIP]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_UniqueGroupResponses_GroupDefinitionID]    Script Date: 30/08/2017 08:26:38 ******/
CREATE NONCLUSTERED INDEX [IX_UniqueGroupResponses_GroupDefinitionID] ON [dbo].[UniqueGroupResponses]
(
	[GroupDefinitionID] ASC
)
INCLUDE ( 	[CheckSum],
	[CI],
	[CI_Review],
	[ConfirmedMark],
	[ID],
	[IsEscalated],
	[ParkedDate],
	[ParkedUserID]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_UniqueGroupResponses_IsEscalated]    Script Date: 30/08/2017 08:26:38 ******/
CREATE NONCLUSTERED INDEX [IX_UniqueGroupResponses_IsEscalated] ON [dbo].[UniqueGroupResponses]
(
	[IsEscalated] ASC
)
INCLUDE ( 	[GroupDefinitionID],
	[ID],
	[ParkedDate],
	[ParkedUserID]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_UniqueGroupResponses_TokenId]    Script Date: 30/08/2017 08:26:38 ******/
CREATE NONCLUSTERED INDEX [IX_UniqueGroupResponses_TokenId] ON [dbo].[UniqueGroupResponses]
(
	[TokenId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [UniqueGroupResponses_GroupDefenition_IDX]    Script Date: 30/08/2017 08:26:38 ******/
CREATE NONCLUSTERED INDEX [UniqueGroupResponses_GroupDefenition_IDX] ON [dbo].[UniqueGroupResponses]
(
	[GroupDefinitionID] ASC,
	[CI] ASC,
	[IsEscalated] ASC,
	[TokenId] ASC
)
INCLUDE ( 	[ConfirmedMark]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_UniqueResponses_ItemID]    Script Date: 30/08/2017 08:26:38 ******/
CREATE NONCLUSTERED INDEX [IX_UniqueResponses_ItemID] ON [dbo].[UniqueResponses]
(
	[itemId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_UploadedFilesDetails_UserType_UploadedByUserID_ReasonTypeID]    Script Date: 30/08/2017 08:26:38 ******/
CREATE NONCLUSTERED INDEX [IX_UploadedFilesDetails_UserType_UploadedByUserID_ReasonTypeID] ON [dbo].[UploadedFilesDetails]
(
	[UserType] ASC,
	[UploadedByUserID] ASC,
	[ReasonTypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AssignedGroupMarks] ADD  CONSTRAINT [DF__AssignedG__IsCon__1B7E091A]  DEFAULT ((0)) FOR [IsConfirmedMark]
GO
ALTER TABLE [dbo].[AssignedGroupMarks] ADD  CONSTRAINT [DF__AssignedG__Disre__1C722D53]  DEFAULT ((0)) FOR [Disregarded]
GO
ALTER TABLE [dbo].[AssignedGroupMarks] ADD  CONSTRAINT [DF__AssignedG__IsEsc__49954745]  DEFAULT ((0)) FOR [IsEscalated]
GO
ALTER TABLE [dbo].[AssignedGroupMarks] ADD  CONSTRAINT [DF_AssignedGroupMarks_IsReportableCI]  DEFAULT ((0)) FOR [IsReportableCI]
GO
ALTER TABLE [dbo].[AssignedGroupMarks] ADD  CONSTRAINT [DF_AssignedGroupMarks_EscalatedWithoutMark]  DEFAULT ((0)) FOR [EscalatedWithoutMark]
GO
ALTER TABLE [dbo].[CandidateExamVersions] ADD  CONSTRAINT [DF_CandidateExamVersions_ExamSessionState]  DEFAULT ((1)) FOR [ExamSessionState]
GO
ALTER TABLE [dbo].[CandidateExamVersions] ADD  CONSTRAINT [DF_CandidateExamVersions_VIP]  DEFAULT ((0)) FOR [VIP]
GO
ALTER TABLE [dbo].[CandidateExamVersions] ADD  CONSTRAINT [DF__Candidate__Scrip__5F492382]  DEFAULT ((0)) FOR [ScriptType]
GO
ALTER TABLE [dbo].[CandidateExamVersions] ADD  CONSTRAINT [DF__CandidateE__Mark__41E93885]  DEFAULT ((0)) FOR [Mark]
GO
ALTER TABLE [dbo].[CandidateExamVersions] ADD  CONSTRAINT [DF__Candidate__Total__42DD5CBE]  DEFAULT ((0)) FOR [TotalMark]
GO
ALTER TABLE [dbo].[CandidateExamVersions] ADD  CONSTRAINT [DF_CandidateExamVersions_IsFlagged]  DEFAULT ((0)) FOR [IsFlagged]
GO
ALTER TABLE [dbo].[CandidateExamVersions] ADD  CONSTRAINT [DF_CandidateExamVersions_IsModerated]  DEFAULT ((0)) FOR [IsModerated]
GO
ALTER TABLE [dbo].[Exams] ADD  CONSTRAINT [DF_Exams_ExternalID]  DEFAULT ((0)) FOR [ExternalID]
GO
ALTER TABLE [dbo].[Exams] ADD  CONSTRAINT [DF_Exams_AutoCIReview]  DEFAULT ((0)) FOR [AutoCIReview]
GO
ALTER TABLE [dbo].[Exams] ADD  CONSTRAINT [DF_Exams_DefaultAllowAnnotationsInMarking]  DEFAULT ((0)) FOR [DefaultAllowAnnotationsInMarking]
GO
ALTER TABLE [dbo].[Exams] ADD  CONSTRAINT [DF_Exams_DefaultNoOfCIsRequired]  DEFAULT ((0)) FOR [DefaultNoOfCIsRequired]
GO
ALTER TABLE [dbo].[Exams] ADD  CONSTRAINT [DF_Exams_DefaultTotalItemsInCompetencyRun]  DEFAULT ((0)) FOR [DefaultTotalItemsInCompetencyRun]
GO
ALTER TABLE [dbo].[Exams] ADD  CONSTRAINT [DF_Exams_DefaultCorrectItemsWithinToleranceWithinCompetenceRunDefault]  DEFAULT ((0)) FOR [DefaultCorrectItemsWithinToleranceWithinCompetenceRunDefault]
GO
ALTER TABLE [dbo].[Exams] ADD  CONSTRAINT [DF_Exams_DefaultMaximumCumulativeErrorWithinCompetenceRun]  DEFAULT ((0)) FOR [DefaultMaximumCumulativeErrorWithinCompetenceRun]
GO
ALTER TABLE [dbo].[Exams] ADD  CONSTRAINT [DF_Exams_DefaultProbabilityOfGettingCI]  DEFAULT ((0)) FOR [DefaultProbabilityOfGettingCI]
GO
ALTER TABLE [dbo].[Exams] ADD  CONSTRAINT [DF_Exams_DefaultMaximumResponseBeforePresentingCI]  DEFAULT ((0)) FOR [DefaultMaximumResponseBeforePresentingCI]
GO
ALTER TABLE [dbo].[Exams] ADD  CONSTRAINT [DF_Exams_DeafultMaxConsecutiveCIsThatCanBeMarkedOutOfTolerance]  DEFAULT ((1)) FOR [DeafultMaxConsecutiveCIsThatCanBeMarkedOutOfTolerance]
GO
ALTER TABLE [dbo].[Exams] ADD  CONSTRAINT [DF_Exams_DefaultNumberOfCIsInRollingReview]  DEFAULT ((1)) FOR [DefaultNumberOfCIsInRollingReview]
GO
ALTER TABLE [dbo].[Exams] ADD  CONSTRAINT [DF_Exams_DefaultMaximumCIsMarkedOutOfToleranceInRollingReview]  DEFAULT ((1)) FOR [DefaultMaximumCIsMarkedOutOfToleranceInRollingReview]
GO
ALTER TABLE [dbo].[Exams] ADD  CONSTRAINT [DF_Exams_DefaultMaximumCumulativeErrorWithinCompetenceRunItemLevel]  DEFAULT ((0)) FOR [DefaultMaximumCumulativeErrorWithinCompetenceRunItemLevel]
GO
ALTER TABLE [dbo].[Exams] ADD  CONSTRAINT [DF_Exams_DeafultMaxConsecutiveCIsThatCanBeMarkedOutOfToleranceItemLevel]  DEFAULT ((0)) FOR [DeafultMaxConsecutiveCIsThatCanBeMarkedOutOfToleranceItemLevel]
GO
ALTER TABLE [dbo].[Exams] ADD  CONSTRAINT [DF_Exams_DefaultMaximumCIsMarkedOutOfToleranceInRollingReviewItemLevel]  DEFAULT ((0)) FOR [DefaultMaximumCIsMarkedOutOfToleranceInRollingReviewItemLevel]
GO
ALTER TABLE [dbo].[Exams] ADD  CONSTRAINT [DF_Exams_IsDecimalMarksEnabled]  DEFAULT ((0)) FOR [IsDecimalMarksEnabled]
GO
ALTER TABLE [dbo].[GroupDefinitions] ADD  CONSTRAINT [DF_GroupDefinitions_IsAutomaticGroup]  DEFAULT ((0)) FOR [IsAutomaticGroup]
GO
ALTER TABLE [dbo].[GroupDefinitions] ADD  CONSTRAINT [DF_GroupDefinitions_IsFeedbackOnDailyCompetencyCheck]  DEFAULT ((1)) FOR [IsFeedbackOnDailyCompetencyCheck]
GO
ALTER TABLE [dbo].[GroupDefinitions] ADD  CONSTRAINT [DF_GroupDefinitions_IsFeedbackOnOngoingCompetencyCheck]  DEFAULT ((1)) FOR [IsFeedbackOnOngoingCompetencyCheck]
GO
ALTER TABLE [dbo].[Items] ADD  CONSTRAINT [DF_Items_Marking Method]  DEFAULT ((0)) FOR [AllowAnnotations]
GO
ALTER TABLE [dbo].[Items] ADD  CONSTRAINT [DF_Items_MarkingType]  DEFAULT ((0)) FOR [MarkingType]
GO
ALTER TABLE [dbo].[Items] ADD  CONSTRAINT [DF_Items_CIMarkingTolerance]  DEFAULT ((0)) FOR [CIMarkingTolerance]
GO
ALTER TABLE [dbo].[Items] ADD  CONSTRAINT [DF_Items_TotalMark]  DEFAULT ((1)) FOR [TotalMark]
GO
ALTER TABLE [dbo].[Items] ADD  CONSTRAINT [DF_Items_MaximumCumulativeErrorWitihnQualificationRun]  DEFAULT ((0)) FOR [MaximumCumulativeErrorWithinCompetenceRun]
GO
ALTER TABLE [dbo].[Items] ADD  CONSTRAINT [DF_Items_MaxConsecutiveCIsThatCanBeMarkedOutOfTolerance]  DEFAULT ((1)) FOR [MaxConsecutiveCIsThatCanBeMarkedOutOfTolerance]
GO
ALTER TABLE [dbo].[Items] ADD  CONSTRAINT [DF_Items_MaximumCIsMarkedOutOfToleranceInRollingReview]  DEFAULT ((1)) FOR [MaximumCIsMarkedOutOfToleranceInRollingReview]
GO
ALTER TABLE [dbo].[MarkSchemes] ADD  CONSTRAINT [DF_MarkSchemes_IsMarkingCriteria]  DEFAULT ((1)) FOR [IsMarkingCriteria]
GO
ALTER TABLE [dbo].[Moderations] ADD  CONSTRAINT [DF_Moderations_IsActual]  DEFAULT ((0)) FOR [IsActual]
GO
ALTER TABLE [dbo].[ParkedReasonsLookup] ADD  CONSTRAINT [DF_ParkedReasonsLookup_IsCommentRequired]  DEFAULT ((0)) FOR [IsCommentRequired]
GO
ALTER TABLE [dbo].[Permissions] ADD  CONSTRAINT [DF_Permissions_GlobalSupport]  DEFAULT ((0)) FOR [GlobalSupport]
GO
ALTER TABLE [dbo].[Permissions] ADD  CONSTRAINT [DF__Permissio__Inter__61316BF4]  DEFAULT ((0)) FOR [InternalPermission]
GO
ALTER TABLE [dbo].[Qualifications] ADD  CONSTRAINT [DF_Qualifications_ExternalID]  DEFAULT ((0)) FOR [ExternalID]
GO
ALTER TABLE [dbo].[QuotaManagement] ADD  CONSTRAINT [DF_QuotaManagement_MarkingSuspended]  DEFAULT ((0)) FOR [MarkingSuspended]
GO
ALTER TABLE [dbo].[QuotaManagement] ADD  CONSTRAINT [DF_QuotaManagement_NumberMarked]  DEFAULT ((0)) FOR [NumberMarked]
GO
ALTER TABLE [dbo].[QuotaManagement] ADD  CONSTRAINT [DF_QuotaManagement_Within1CIofSuspension]  DEFAULT ((0)) FOR [Within1CIofSuspension]
GO
ALTER TABLE [dbo].[QuotaManagement] ADD  CONSTRAINT [DF_QuotaManagement_NumItemsParked]  DEFAULT ((0)) FOR [NumItemsParked]
GO
ALTER TABLE [dbo].[QuotaManagement] ADD  CONSTRAINT [DF_QuotaManagement_GroupId]  DEFAULT ((0)) FOR [GroupId]
GO
ALTER TABLE [dbo].[QuotaManagement] ADD  CONSTRAINT [DF__QuotaMana__Numbe__531EB17F]  DEFAULT ((0)) FOR [NumberSuspended]
GO
ALTER TABLE [dbo].[Roles] ADD  CONSTRAINT [DF_Roles_AssignableToGlobal]  DEFAULT ((0)) FOR [AssignableToGlobal]
GO
ALTER TABLE [dbo].[Roles] ADD  CONSTRAINT [DF_Roles_AssignedByIntegration]  DEFAULT ((0)) FOR [AssignedByIntegration]
GO
ALTER TABLE [dbo].[Roles] ADD  CONSTRAINT [DF_Roles_TrainingRole]  DEFAULT ((0)) FOR [TrainingRole]
GO
ALTER TABLE [dbo].[Roles] ADD  CONSTRAINT [DF__Roles__InternalR__603D47BB]  DEFAULT ((0)) FOR [InternalRole]
GO
ALTER TABLE [dbo].[Roles] ADD  CONSTRAINT [DF_Roles_AssignableToExams]  DEFAULT ((1)) FOR [AssignableToExams]
GO
ALTER TABLE [dbo].[UniqueGroupResponses] ADD  CONSTRAINT [DF_UniqueGroupResponses_DefaultScriptType]  DEFAULT ((0)) FOR [ScriptType]
GO
ALTER TABLE [dbo].[UniqueGroupResponses] ADD  CONSTRAINT [DF_UniqueGroupResponses_IsEscalated]  DEFAULT ((0)) FOR [IsEscalated]
GO
ALTER TABLE [dbo].[UniqueResponseDocuments] ADD  CONSTRAINT [DF_UniqueResponseDocuments_ExternalDocID]  DEFAULT ((-1)) FOR [ExternalDocID]
GO
ALTER TABLE [dbo].[UniqueResponseDocuments] ADD  CONSTRAINT [DF_UniqueResponseDocuments_UploadedDate]  DEFAULT ('2011/01/01 00:00:00') FOR [UploadedDate]
GO
ALTER TABLE [dbo].[UniqueResponseDocuments] ADD  CONSTRAINT [DF_UniqueResponseDocuments_FileName]  DEFAULT (N'') FOR [FileName]
GO
ALTER TABLE [dbo].[UserCompetenceStatus] ADD  CONSTRAINT [DF_UserQualifiedStatus_Qualified]  DEFAULT ((0)) FOR [CompetenceStatus]
GO
ALTER TABLE [dbo].[UserCompetenceStatus] ADD  CONSTRAINT [DF_UserCompetenceStatus_GroupDefinitionID]  DEFAULT ((0)) FOR [GroupDefinitionID]
GO
ALTER TABLE [dbo].[UserInteractionAudit] ADD  CONSTRAINT [DF_UserInteractionAudit_Success]  DEFAULT ((0)) FOR [Success]
GO
ALTER TABLE [dbo].[Users] ADD  CONSTRAINT [DF_Users_Retired]  DEFAULT ((0)) FOR [Retired]
GO
ALTER TABLE [dbo].[Users] ADD  CONSTRAINT [DF_Users_RequiresPassword]  DEFAULT ((1)) FOR [RequiresPasswordReset]
GO
ALTER TABLE [dbo].[Users] ADD  CONSTRAINT [DF_Users_InternalUser]  DEFAULT ((0)) FOR [InternalUser]
GO
ALTER TABLE [dbo].[Users] ADD  CONSTRAINT [DF_Users_PasswordAttempts]  DEFAULT ((0)) FOR [PasswordAttempts]
GO
ALTER TABLE [dbo].[AffectedCandidateExamVersions]  WITH CHECK ADD  CONSTRAINT [FK_AffectedCandidateExamVersions_CandidateExamVersions] FOREIGN KEY([CandidateExamVersionId])
REFERENCES [dbo].[CandidateExamVersions] ([ID])
GO
ALTER TABLE [dbo].[AffectedCandidateExamVersions] CHECK CONSTRAINT [FK_AffectedCandidateExamVersions_CandidateExamVersions]
GO
ALTER TABLE [dbo].[AffectedCandidateExamVersions]  WITH CHECK ADD  CONSTRAINT [FK_AffectedCandidateExamVersions_GroupDefinitions] FOREIGN KEY([GroupDefinitionId])
REFERENCES [dbo].[GroupDefinitions] ([ID])
GO
ALTER TABLE [dbo].[AffectedCandidateExamVersions] CHECK CONSTRAINT [FK_AffectedCandidateExamVersions_GroupDefinitions]
GO
ALTER TABLE [dbo].[AffectedCandidateExamVersions]  WITH CHECK ADD  CONSTRAINT [FK_AffectedCandidateExamVersions_UniqueGroupResponses] FOREIGN KEY([UniqueGroupResponseId])
REFERENCES [dbo].[UniqueGroupResponses] ([ID])
GO
ALTER TABLE [dbo].[AffectedCandidateExamVersions] CHECK CONSTRAINT [FK_AffectedCandidateExamVersions_UniqueGroupResponses]
GO
ALTER TABLE [dbo].[AffectedCandidateExamVersions]  WITH CHECK ADD  CONSTRAINT [FK_AffectedCandidateExamVersions_Users] FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([ID])
GO
ALTER TABLE [dbo].[AffectedCandidateExamVersions] CHECK CONSTRAINT [FK_AffectedCandidateExamVersions_Users]
GO
ALTER TABLE [dbo].[AssignedGroupMarks]  WITH CHECK ADD  CONSTRAINT [FK_AssignedGroupMarks_GroupDefinitions] FOREIGN KEY([GroupDefinitionID])
REFERENCES [dbo].[GroupDefinitions] ([ID])
GO
ALTER TABLE [dbo].[AssignedGroupMarks] CHECK CONSTRAINT [FK_AssignedGroupMarks_GroupDefinitions]
GO
ALTER TABLE [dbo].[AssignedGroupMarks]  WITH CHECK ADD  CONSTRAINT [FK_AssignedGroupMarks_MarkingMethodLookup] FOREIGN KEY([MarkingMethodId])
REFERENCES [dbo].[MarkingMethodLookup] ([id])
GO
ALTER TABLE [dbo].[AssignedGroupMarks] CHECK CONSTRAINT [FK_AssignedGroupMarks_MarkingMethodLookup]
GO
ALTER TABLE [dbo].[AssignedGroupMarks]  WITH CHECK ADD  CONSTRAINT [FK_AssignedGroupMarks_UniqueGroupResponses] FOREIGN KEY([UniqueGroupResponseId])
REFERENCES [dbo].[UniqueGroupResponses] ([ID])
GO
ALTER TABLE [dbo].[AssignedGroupMarks] CHECK CONSTRAINT [FK_AssignedGroupMarks_UniqueGroupResponses]
GO
ALTER TABLE [dbo].[AssignedGroupMarks]  WITH CHECK ADD  CONSTRAINT [FK_AssignedGroupMarks_Users] FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([ID])
GO
ALTER TABLE [dbo].[AssignedGroupMarks] CHECK CONSTRAINT [FK_AssignedGroupMarks_Users]
GO
ALTER TABLE [dbo].[AssignedGroupMarksParkedReasonCrossRef]  WITH CHECK ADD  CONSTRAINT [FK_AssignedGroupMarksParkedReasonCrossRef_ParkedReasonsLookup] FOREIGN KEY([ParkedReasonId])
REFERENCES [dbo].[ParkedReasonsLookup] ([ID])
GO
ALTER TABLE [dbo].[AssignedGroupMarksParkedReasonCrossRef] CHECK CONSTRAINT [FK_AssignedGroupMarksParkedReasonCrossRef_ParkedReasonsLookup]
GO
ALTER TABLE [dbo].[AssignedItemMarks]  WITH CHECK ADD  CONSTRAINT [FK_AssignedItemMarks_UniqueResponses] FOREIGN KEY([UniqueResponseId])
REFERENCES [dbo].[UniqueResponses] ([id])
GO
ALTER TABLE [dbo].[AssignedItemMarks] CHECK CONSTRAINT [FK_AssignedItemMarks_UniqueResponses]
GO
ALTER TABLE [dbo].[AssignedUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_AssignedUserRoles_Exams] FOREIGN KEY([ExamID])
REFERENCES [dbo].[Exams] ([ID])
GO
ALTER TABLE [dbo].[AssignedUserRoles] CHECK CONSTRAINT [FK_AssignedUserRoles_Exams]
GO
ALTER TABLE [dbo].[AssignedUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_AssignedUserRoles_Roles] FOREIGN KEY([RoleID])
REFERENCES [dbo].[Roles] ([ID])
GO
ALTER TABLE [dbo].[AssignedUserRoles] CHECK CONSTRAINT [FK_AssignedUserRoles_Roles]
GO
ALTER TABLE [dbo].[AssignedUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_AssignedUserRoles_Users] FOREIGN KEY([UserID])
REFERENCES [dbo].[Users] ([ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AssignedUserRoles] CHECK CONSTRAINT [FK_AssignedUserRoles_Users]
GO
ALTER TABLE [dbo].[CandidateExamVersions]  WITH CHECK ADD  CONSTRAINT [FK_CandidateExamVersions_CandidateExamVersionStateLookup] FOREIGN KEY([ExamSessionState])
REFERENCES [dbo].[CandidateExamVersionStateLookup] ([ID])
GO
ALTER TABLE [dbo].[CandidateExamVersions] CHECK CONSTRAINT [FK_CandidateExamVersions_CandidateExamVersionStateLookup]
GO
ALTER TABLE [dbo].[CandidateExamVersions]  WITH CHECK ADD  CONSTRAINT [FK_CandidateExamVersions_ExamVersions] FOREIGN KEY([ExamVersionID])
REFERENCES [dbo].[ExamVersions] ([ID])
GO
ALTER TABLE [dbo].[CandidateExamVersions] CHECK CONSTRAINT [FK_CandidateExamVersions_ExamVersions]
GO
ALTER TABLE [dbo].[CandidateExamVersions]  WITH CHECK ADD  CONSTRAINT [FK_CandidateExamVersions_ExamVersionStructures] FOREIGN KEY([ExamVersionStructureID])
REFERENCES [dbo].[ExamVersionStructures] ([ID])
GO
ALTER TABLE [dbo].[CandidateExamVersions] CHECK CONSTRAINT [FK_CandidateExamVersions_ExamVersionStructures]
GO
ALTER TABLE [dbo].[CandidateExamVersionStatuses]  WITH CHECK ADD  CONSTRAINT [FK_CandidateExamVersionStatuses_CandidateExamVersions] FOREIGN KEY([CandidateExamVersionID])
REFERENCES [dbo].[CandidateExamVersions] ([ID])
GO
ALTER TABLE [dbo].[CandidateExamVersionStatuses] CHECK CONSTRAINT [FK_CandidateExamVersionStatuses_CandidateExamVersions]
GO
ALTER TABLE [dbo].[CandidateExamVersionStatuses]  WITH CHECK ADD  CONSTRAINT [FK_CandidateExamVersionStatuses_CandidateExamVersionStateLookup] FOREIGN KEY([StateID])
REFERENCES [dbo].[CandidateExamVersionStateLookup] ([ID])
GO
ALTER TABLE [dbo].[CandidateExamVersionStatuses] CHECK CONSTRAINT [FK_CandidateExamVersionStatuses_CandidateExamVersionStateLookup]
GO
ALTER TABLE [dbo].[CandidateGroupResponses]  WITH CHECK ADD  CONSTRAINT [FK_CandidateGroupResponses_CandidateExamVersions] FOREIGN KEY([CandidateExamVersionID])
REFERENCES [dbo].[CandidateExamVersions] ([ID])
GO
ALTER TABLE [dbo].[CandidateGroupResponses] CHECK CONSTRAINT [FK_CandidateGroupResponses_CandidateExamVersions]
GO
ALTER TABLE [dbo].[CandidateGroupResponses]  WITH CHECK ADD  CONSTRAINT [FK_CandidateGroupResponses_UniqueGroupResponses] FOREIGN KEY([UniqueGroupResponseID])
REFERENCES [dbo].[UniqueGroupResponses] ([ID])
GO
ALTER TABLE [dbo].[CandidateGroupResponses] CHECK CONSTRAINT [FK_CandidateGroupResponses_UniqueGroupResponses]
GO
ALTER TABLE [dbo].[CandidateResponses]  WITH CHECK ADD  CONSTRAINT [FK_CandidateResponses_CandidateExamVersions] FOREIGN KEY([CandidateExamVersionID])
REFERENCES [dbo].[CandidateExamVersions] ([ID])
GO
ALTER TABLE [dbo].[CandidateResponses] CHECK CONSTRAINT [FK_CandidateResponses_CandidateExamVersions]
GO
ALTER TABLE [dbo].[CandidateResponses]  WITH CHECK ADD  CONSTRAINT [FK_CandidateResponses_UniqueResponseTable] FOREIGN KEY([UniqueResponseID])
REFERENCES [dbo].[UniqueResponses] ([id])
GO
ALTER TABLE [dbo].[CandidateResponses] CHECK CONSTRAINT [FK_CandidateResponses_UniqueResponseTable]
GO
ALTER TABLE [dbo].[Exams]  WITH CHECK ADD  CONSTRAINT [FK_Exams_Qualifications1] FOREIGN KEY([QualificationID])
REFERENCES [dbo].[Qualifications] ([ID])
GO
ALTER TABLE [dbo].[Exams] CHECK CONSTRAINT [FK_Exams_Qualifications1]
GO
ALTER TABLE [dbo].[ExamSectionItems]  WITH CHECK ADD  CONSTRAINT [FK_ExamSectionItems_ExamSections] FOREIGN KEY([ExamSectionID])
REFERENCES [dbo].[ExamSections] ([ID])
GO
ALTER TABLE [dbo].[ExamSectionItems] CHECK CONSTRAINT [FK_ExamSectionItems_ExamSections]
GO
ALTER TABLE [dbo].[ExamSectionItems]  WITH CHECK ADD  CONSTRAINT [FK_ExamSectionItems_ExamVersionStructures] FOREIGN KEY([ExamStructureId])
REFERENCES [dbo].[ExamVersionStructures] ([ID])
GO
ALTER TABLE [dbo].[ExamSectionItems] CHECK CONSTRAINT [FK_ExamSectionItems_ExamVersionStructures]
GO
ALTER TABLE [dbo].[ExamSectionItems]  WITH CHECK ADD  CONSTRAINT [FK_ExamSectionItems_Items] FOREIGN KEY([ItemID])
REFERENCES [dbo].[Items] ([ID])
GO
ALTER TABLE [dbo].[ExamSectionItems] CHECK CONSTRAINT [FK_ExamSectionItems_Items]
GO
ALTER TABLE [dbo].[ExamSections]  WITH CHECK ADD  CONSTRAINT [FK_ExamSections_ExamVersions] FOREIGN KEY([ExamVersionID])
REFERENCES [dbo].[ExamVersions] ([ID])
GO
ALTER TABLE [dbo].[ExamSections] CHECK CONSTRAINT [FK_ExamSections_ExamVersions]
GO
ALTER TABLE [dbo].[ExamVersionItems]  WITH CHECK ADD  CONSTRAINT [FK_ExamVersionItems_CandidateExamVersions] FOREIGN KEY([CandidateExamVersionID])
REFERENCES [dbo].[CandidateExamVersions] ([ID])
GO
ALTER TABLE [dbo].[ExamVersionItems] CHECK CONSTRAINT [FK_ExamVersionItems_CandidateExamVersions]
GO
ALTER TABLE [dbo].[ExamVersionItems]  WITH CHECK ADD  CONSTRAINT [FK_ExamVersionItems_Items] FOREIGN KEY([ItemID])
REFERENCES [dbo].[Items] ([ID])
GO
ALTER TABLE [dbo].[ExamVersionItems] CHECK CONSTRAINT [FK_ExamVersionItems_Items]
GO
ALTER TABLE [dbo].[ExamVersionItemsRef]  WITH CHECK ADD  CONSTRAINT [FK_ExamVersionItemsRef_ExamVersions] FOREIGN KEY([ExamVersionID])
REFERENCES [dbo].[ExamVersions] ([ID])
GO
ALTER TABLE [dbo].[ExamVersionItemsRef] CHECK CONSTRAINT [FK_ExamVersionItemsRef_ExamVersions]
GO
ALTER TABLE [dbo].[ExamVersionItemsRef]  WITH CHECK ADD  CONSTRAINT [FK_ExamVersionItemsRef_Items] FOREIGN KEY([ItemID])
REFERENCES [dbo].[Items] ([ID])
GO
ALTER TABLE [dbo].[ExamVersionItemsRef] CHECK CONSTRAINT [FK_ExamVersionItemsRef_Items]
GO
ALTER TABLE [dbo].[ExamVersions]  WITH CHECK ADD  CONSTRAINT [FK_ExamVersions_Exams] FOREIGN KEY([ExamID])
REFERENCES [dbo].[Exams] ([ID])
GO
ALTER TABLE [dbo].[ExamVersions] CHECK CONSTRAINT [FK_ExamVersions_Exams]
GO
ALTER TABLE [dbo].[ExamVersionStructureItems]  WITH CHECK ADD  CONSTRAINT [FK_ExamVersionStructureItems_ExamVersionStructures] FOREIGN KEY([ExamStructureID])
REFERENCES [dbo].[ExamVersionStructures] ([ID])
GO
ALTER TABLE [dbo].[ExamVersionStructureItems] CHECK CONSTRAINT [FK_ExamVersionStructureItems_ExamVersionStructures]
GO
ALTER TABLE [dbo].[ExamVersionStructureItems]  WITH CHECK ADD  CONSTRAINT [FK_ExamVersionStructureItems_Items] FOREIGN KEY([ItemID])
REFERENCES [dbo].[Items] ([ID])
GO
ALTER TABLE [dbo].[ExamVersionStructureItems] CHECK CONSTRAINT [FK_ExamVersionStructureItems_Items]
GO
ALTER TABLE [dbo].[ExamVersionStructures]  WITH CHECK ADD  CONSTRAINT [FK_ExamVersionStructures_ExamVersions] FOREIGN KEY([ExamVersionID])
REFERENCES [dbo].[ExamVersions] ([ID])
GO
ALTER TABLE [dbo].[ExamVersionStructures] CHECK CONSTRAINT [FK_ExamVersionStructures_ExamVersions]
GO
ALTER TABLE [dbo].[ExamVersionStructures]  WITH CHECK ADD  CONSTRAINT [FK_ExamVersionStructures_ExamVersionStructuresStatuses] FOREIGN KEY([StatusID])
REFERENCES [dbo].[ExamVersionStructuresStatuses] ([ID])
GO
ALTER TABLE [dbo].[ExamVersionStructures] CHECK CONSTRAINT [FK_ExamVersionStructures_ExamVersionStructuresStatuses]
GO
ALTER TABLE [dbo].[GrantableRoles]  WITH CHECK ADD  CONSTRAINT [FK_AssignableRoles_Roles] FOREIGN KEY([RoleID])
REFERENCES [dbo].[Roles] ([ID])
GO
ALTER TABLE [dbo].[GrantableRoles] CHECK CONSTRAINT [FK_AssignableRoles_Roles]
GO
ALTER TABLE [dbo].[GrantableRoles]  WITH CHECK ADD  CONSTRAINT [FK_AssignableRoles_Roles1] FOREIGN KEY([GrantableRoleID])
REFERENCES [dbo].[Roles] ([ID])
GO
ALTER TABLE [dbo].[GrantableRoles] CHECK CONSTRAINT [FK_AssignableRoles_Roles1]
GO
ALTER TABLE [dbo].[GroupDefinitionItems]  WITH CHECK ADD  CONSTRAINT [FK_GroupDefinitionItems_GroupDefinitions] FOREIGN KEY([GroupID])
REFERENCES [dbo].[GroupDefinitions] ([ID])
GO
ALTER TABLE [dbo].[GroupDefinitionItems] CHECK CONSTRAINT [FK_GroupDefinitionItems_GroupDefinitions]
GO
ALTER TABLE [dbo].[GroupDefinitionItems]  WITH CHECK ADD  CONSTRAINT [FK_GroupDefinitionItems_Items] FOREIGN KEY([ItemID])
REFERENCES [dbo].[Items] ([ID])
GO
ALTER TABLE [dbo].[GroupDefinitionItems] CHECK CONSTRAINT [FK_GroupDefinitionItems_Items]
GO
ALTER TABLE [dbo].[GroupDefinitions]  WITH CHECK ADD  CONSTRAINT [FK_GroupDefinitions_Exams] FOREIGN KEY([ExamID])
REFERENCES [dbo].[Exams] ([ID])
GO
ALTER TABLE [dbo].[GroupDefinitions] CHECK CONSTRAINT [FK_GroupDefinitions_Exams]
GO
ALTER TABLE [dbo].[GroupDefinitionStructureCrossRef]  WITH CHECK ADD  CONSTRAINT [FK_GroupDefinitionStructureCrossRef_ExamVersionStructures] FOREIGN KEY([ExamVersionStructureID])
REFERENCES [dbo].[ExamVersionStructures] ([ID])
GO
ALTER TABLE [dbo].[GroupDefinitionStructureCrossRef] CHECK CONSTRAINT [FK_GroupDefinitionStructureCrossRef_ExamVersionStructures]
GO
ALTER TABLE [dbo].[GroupDefinitionStructureCrossRef]  WITH CHECK ADD  CONSTRAINT [FK_GroupDefinitionStructureCrossRef_GroupDefinitions] FOREIGN KEY([GroupDefinitionID])
REFERENCES [dbo].[GroupDefinitions] ([ID])
GO
ALTER TABLE [dbo].[GroupDefinitionStructureCrossRef] CHECK CONSTRAINT [FK_GroupDefinitionStructureCrossRef_GroupDefinitions]
GO
ALTER TABLE [dbo].[GroupDefinitionStructureCrossRef]  WITH CHECK ADD  CONSTRAINT [FK_GroupDefinitionStructureCrossRef_GroupDefinitionStructureCrossRefStatuses] FOREIGN KEY([Status])
REFERENCES [dbo].[GroupDefinitionStructureCrossRefStatuses] ([ID])
GO
ALTER TABLE [dbo].[GroupDefinitionStructureCrossRef] CHECK CONSTRAINT [FK_GroupDefinitionStructureCrossRef_GroupDefinitionStructureCrossRefStatuses]
GO
ALTER TABLE [dbo].[Items]  WITH CHECK ADD  CONSTRAINT [FK_Items_Exams] FOREIGN KEY([ExamID])
REFERENCES [dbo].[Exams] ([ID])
GO
ALTER TABLE [dbo].[Items] CHECK CONSTRAINT [FK_Items_Exams]
GO
ALTER TABLE [dbo].[ItemTools]  WITH CHECK ADD  CONSTRAINT [FK_dbo.ItemTools_dbo.Items_ParentItemId] FOREIGN KEY([ParentItemId])
REFERENCES [dbo].[Items] ([ID])
GO
ALTER TABLE [dbo].[ItemTools] CHECK CONSTRAINT [FK_dbo.ItemTools_dbo.Items_ParentItemId]
GO
ALTER TABLE [dbo].[ItemTools]  WITH CHECK ADD  CONSTRAINT [FK_dbo.ItemTools_dbo.Items_ToolItemId] FOREIGN KEY([ToolItemId])
REFERENCES [dbo].[Items] ([ID])
GO
ALTER TABLE [dbo].[ItemTools] CHECK CONSTRAINT [FK_dbo.ItemTools_dbo.Items_ToolItemId]
GO
ALTER TABLE [dbo].[Moderations]  WITH CHECK ADD  CONSTRAINT [FK_Moderations_CandidateExamVersions] FOREIGN KEY([CandidateExamVersionId])
REFERENCES [dbo].[CandidateExamVersions] ([ID])
GO
ALTER TABLE [dbo].[Moderations] CHECK CONSTRAINT [FK_Moderations_CandidateExamVersions]
GO
ALTER TABLE [dbo].[Moderations]  WITH CHECK ADD  CONSTRAINT [FK_Moderations_Moderations] FOREIGN KEY([UniqueResponseId])
REFERENCES [dbo].[UniqueResponses] ([id])
GO
ALTER TABLE [dbo].[Moderations] CHECK CONSTRAINT [FK_Moderations_Moderations]
GO
ALTER TABLE [dbo].[Moderations]  WITH CHECK ADD  CONSTRAINT [FK_Moderations_Users] FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([ID])
GO
ALTER TABLE [dbo].[Moderations] CHECK CONSTRAINT [FK_Moderations_Users]
GO
ALTER TABLE [dbo].[QuotaManagement]  WITH CHECK ADD  CONSTRAINT [FK_QuotaManagement_Exams] FOREIGN KEY([ExamID])
REFERENCES [dbo].[Exams] ([ID])
GO
ALTER TABLE [dbo].[QuotaManagement] CHECK CONSTRAINT [FK_QuotaManagement_Exams]
GO
ALTER TABLE [dbo].[QuotaManagement]  WITH CHECK ADD  CONSTRAINT [FK_QuotaManagement_GroupDefinitions] FOREIGN KEY([GroupId])
REFERENCES [dbo].[GroupDefinitions] ([ID])
GO
ALTER TABLE [dbo].[QuotaManagement] CHECK CONSTRAINT [FK_QuotaManagement_GroupDefinitions]
GO
ALTER TABLE [dbo].[QuotaManagement]  WITH CHECK ADD  CONSTRAINT [FK_QuotaManagement_SuspendedReasons] FOREIGN KEY([SuspendedReason])
REFERENCES [dbo].[SuspendedReasons] ([ID])
GO
ALTER TABLE [dbo].[QuotaManagement] CHECK CONSTRAINT [FK_QuotaManagement_SuspendedReasons]
GO
ALTER TABLE [dbo].[QuotaManagement]  WITH CHECK ADD  CONSTRAINT [FK_QuotaManagement_Users] FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[QuotaManagement] CHECK CONSTRAINT [FK_QuotaManagement_Users]
GO
ALTER TABLE [dbo].[RolePermissions]  WITH CHECK ADD  CONSTRAINT [FK_RolePermissions_Permissions] FOREIGN KEY([PermissionID])
REFERENCES [dbo].[Permissions] ([ID])
GO
ALTER TABLE [dbo].[RolePermissions] CHECK CONSTRAINT [FK_RolePermissions_Permissions]
GO
ALTER TABLE [dbo].[RolePermissions]  WITH CHECK ADD  CONSTRAINT [FK_RolePermissions_Roles] FOREIGN KEY([RoleID])
REFERENCES [dbo].[Roles] ([ID])
GO
ALTER TABLE [dbo].[RolePermissions] CHECK CONSTRAINT [FK_RolePermissions_Roles]
GO
ALTER TABLE [dbo].[SuspendedReasons]  WITH CHECK ADD  CONSTRAINT [FK_SuspendedReasons_SuspendedReasonLookUp] FOREIGN KEY([ReasonID])
REFERENCES [dbo].[SuspendedReasonLookUp] ([ID])
GO
ALTER TABLE [dbo].[SuspendedReasons] CHECK CONSTRAINT [FK_SuspendedReasons_SuspendedReasonLookUp]
GO
ALTER TABLE [dbo].[SuspendedReasons]  WITH CHECK ADD  CONSTRAINT [FK_SuspendedReasons_Users] FOREIGN KEY([UserID])
REFERENCES [dbo].[Users] ([ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[SuspendedReasons] CHECK CONSTRAINT [FK_SuspendedReasons_Users]
GO
ALTER TABLE [dbo].[UniqueGroupResponseLinks]  WITH CHECK ADD  CONSTRAINT [FK_UniqueGroupResponseLinks_UniqueGroupResponses] FOREIGN KEY([UniqueGroupResponseID])
REFERENCES [dbo].[UniqueGroupResponses] ([ID])
GO
ALTER TABLE [dbo].[UniqueGroupResponseLinks] CHECK CONSTRAINT [FK_UniqueGroupResponseLinks_UniqueGroupResponses]
GO
ALTER TABLE [dbo].[UniqueGroupResponseLinks]  WITH CHECK ADD  CONSTRAINT [FK_UniqueGroupResponseLinks_UniqueResponses] FOREIGN KEY([UniqueResponseId])
REFERENCES [dbo].[UniqueResponses] ([id])
GO
ALTER TABLE [dbo].[UniqueGroupResponseLinks] CHECK CONSTRAINT [FK_UniqueGroupResponseLinks_UniqueResponses]
GO
ALTER TABLE [dbo].[UniqueGroupResponseParkedReasonCrossRef]  WITH CHECK ADD  CONSTRAINT [FK_UniqueGroupResponseParkedReasonCrossRef_ParkedReasonsLookup] FOREIGN KEY([ParkedReasonId])
REFERENCES [dbo].[ParkedReasonsLookup] ([ID])
GO
ALTER TABLE [dbo].[UniqueGroupResponseParkedReasonCrossRef] CHECK CONSTRAINT [FK_UniqueGroupResponseParkedReasonCrossRef_ParkedReasonsLookup]
GO
ALTER TABLE [dbo].[UniqueGroupResponseParkedReasonCrossRef]  WITH CHECK ADD  CONSTRAINT [FK_UniqueGroupResponseParkedReasonCrossRef_UniqueGroupResponseParkedReasonCrossRef] FOREIGN KEY([UniqueGroupResponseId])
REFERENCES [dbo].[UniqueGroupResponses] ([ID])
GO
ALTER TABLE [dbo].[UniqueGroupResponseParkedReasonCrossRef] CHECK CONSTRAINT [FK_UniqueGroupResponseParkedReasonCrossRef_UniqueGroupResponseParkedReasonCrossRef]
GO
ALTER TABLE [dbo].[UniqueGroupResponses]  WITH CHECK ADD  CONSTRAINT [FK_UniqueGroupResponses_GroupDefinitions] FOREIGN KEY([GroupDefinitionID])
REFERENCES [dbo].[GroupDefinitions] ([ID])
GO
ALTER TABLE [dbo].[UniqueGroupResponses] CHECK CONSTRAINT [FK_UniqueGroupResponses_GroupDefinitions]
GO
ALTER TABLE [dbo].[UniqueGroupResponses]  WITH NOCHECK ADD  CONSTRAINT [FK_UniqueGroupResponses_TokenStore] FOREIGN KEY([TokenId])
REFERENCES [dbo].[TokenStore] ([id])
GO
ALTER TABLE [dbo].[UniqueGroupResponses] CHECK CONSTRAINT [FK_UniqueGroupResponses_TokenStore]
GO
ALTER TABLE [dbo].[UniqueGroupResponses]  WITH CHECK ADD  CONSTRAINT [FK_UniqueGroupResponses_UniqueGroupResponses] FOREIGN KEY([OriginalUniqueGroupResponseId])
REFERENCES [dbo].[UniqueGroupResponses] ([ID])
GO
ALTER TABLE [dbo].[UniqueGroupResponses] CHECK CONSTRAINT [FK_UniqueGroupResponses_UniqueGroupResponses]
GO
ALTER TABLE [dbo].[UniqueResponseDocuments]  WITH CHECK ADD  CONSTRAINT [FK_UniqueResponseDocuments_UniqueResponses] FOREIGN KEY([UniqueResponseID])
REFERENCES [dbo].[UniqueResponses] ([id])
GO
ALTER TABLE [dbo].[UniqueResponseDocuments] CHECK CONSTRAINT [FK_UniqueResponseDocuments_UniqueResponses]
GO
ALTER TABLE [dbo].[UniqueResponses]  WITH CHECK ADD  CONSTRAINT [FK_UniqueResponseTable_Items] FOREIGN KEY([itemId])
REFERENCES [dbo].[Items] ([ID])
GO
ALTER TABLE [dbo].[UniqueResponses] CHECK CONSTRAINT [FK_UniqueResponseTable_Items]
GO
ALTER TABLE [dbo].[UniqueResponses]  WITH CHECK ADD  CONSTRAINT [FK_UniqueResponseTable_MarkSchemeTable] FOREIGN KEY([markSchemeId])
REFERENCES [dbo].[MarkSchemes] ([id])
GO
ALTER TABLE [dbo].[UniqueResponses] CHECK CONSTRAINT [FK_UniqueResponseTable_MarkSchemeTable]
GO
ALTER TABLE [dbo].[UniqueResponses]  WITH CHECK ADD  CONSTRAINT [FK_UniqueResponseTable_MarkSchemeTable1] FOREIGN KEY([latestMarkSchemeIdAtTimeOfImport])
REFERENCES [dbo].[MarkSchemes] ([id])
GO
ALTER TABLE [dbo].[UniqueResponses] CHECK CONSTRAINT [FK_UniqueResponseTable_MarkSchemeTable1]
GO
ALTER TABLE [dbo].[UploadedFilesDetails]  WITH CHECK ADD  CONSTRAINT [FK_UploadedFilesDetails_UniqueResponseDocuments] FOREIGN KEY([UniqueResponseDocumentID])
REFERENCES [dbo].[UniqueResponseDocuments] ([ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[UploadedFilesDetails] CHECK CONSTRAINT [FK_UploadedFilesDetails_UniqueResponseDocuments]
GO
ALTER TABLE [dbo].[UploadedFilesDetails]  WITH CHECK ADD  CONSTRAINT [FK_UploadedFilesDetails_UploadReasons] FOREIGN KEY([ReasonTypeID])
REFERENCES [dbo].[UploadReasons] ([ID])
GO
ALTER TABLE [dbo].[UploadedFilesDetails] CHECK CONSTRAINT [FK_UploadedFilesDetails_UploadReasons]
GO
ALTER TABLE [dbo].[UploadedFilesDetails]  WITH CHECK ADD  CONSTRAINT [FK_UploadedFilesDetails_Users] FOREIGN KEY([UploadedByUserID])
REFERENCES [dbo].[Users] ([ID])
GO
ALTER TABLE [dbo].[UploadedFilesDetails] CHECK CONSTRAINT [FK_UploadedFilesDetails_Users]
GO
ALTER TABLE [dbo].[UserCompetenceStatus]  WITH CHECK ADD  CONSTRAINT [FK_UserCompetenceStatus_GroupDefinitions] FOREIGN KEY([GroupDefinitionID])
REFERENCES [dbo].[GroupDefinitions] ([ID])
GO
ALTER TABLE [dbo].[UserCompetenceStatus] CHECK CONSTRAINT [FK_UserCompetenceStatus_GroupDefinitions]
GO
ALTER TABLE [dbo].[UserCompetenceStatus]  WITH CHECK ADD  CONSTRAINT [FK_UserQualifiedStatus_QualifiedStatusLookup] FOREIGN KEY([CompetenceStatus])
REFERENCES [dbo].[CompetenceStatusLookup] ([ID])
GO
ALTER TABLE [dbo].[UserCompetenceStatus] CHECK CONSTRAINT [FK_UserQualifiedStatus_QualifiedStatusLookup]
GO
ALTER TABLE [dbo].[UserCompetenceStatus]  WITH CHECK ADD  CONSTRAINT [FK_UserQualifiedStatus_Users] FOREIGN KEY([UserID])
REFERENCES [dbo].[Users] ([ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[UserCompetenceStatus] CHECK CONSTRAINT [FK_UserQualifiedStatus_Users]
GO
ALTER TABLE [dbo].[UserQualifiedStatusAudit]  WITH CHECK ADD  CONSTRAINT [FK_UserQualifiedStatusAudit_Items] FOREIGN KEY([ItemID])
REFERENCES [dbo].[Items] ([ID])
GO
ALTER TABLE [dbo].[UserQualifiedStatusAudit] CHECK CONSTRAINT [FK_UserQualifiedStatusAudit_Items]
GO
ALTER TABLE [dbo].[UserQualifiedStatusAudit]  WITH CHECK ADD  CONSTRAINT [FK_UserQualifiedStatusAudit_Users] FOREIGN KEY([UserID])
REFERENCES [dbo].[Users] ([ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[UserQualifiedStatusAudit] CHECK CONSTRAINT [FK_UserQualifiedStatusAudit_Users]
GO
ALTER TABLE [dbo].[UserRecentExams]  WITH CHECK ADD  CONSTRAINT [FK_UserRecentExams_Exams] FOREIGN KEY([ExamId])
REFERENCES [dbo].[Exams] ([ID])
GO
ALTER TABLE [dbo].[UserRecentExams] CHECK CONSTRAINT [FK_UserRecentExams_Exams]
GO
ALTER TABLE [dbo].[UserRecentExams]  WITH CHECK ADD  CONSTRAINT [FK_UserRecentExams_Users] FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([ID])
GO
ALTER TABLE [dbo].[UserRecentExams] CHECK CONSTRAINT [FK_UserRecentExams_Users]
GO
/****** Object:  StoredProcedure [dbo].[sm_CONSISTENCYCHECK_AllExamsHaveExamVersion]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sm_CONSISTENCYCHECK_AllExamsHaveExamVersion]
AS
BEGIN
	SET NOCOUNT ON
	
	SELECT E.[ID] as ExamId
		, E.[Name] as ExamName
		, E.[QualificationID] as QualificationId
		, E.[Reference] as ExamReference
		, E.[ExternalID] as ExamExternalId
		FROM dbo.Exams E
		FULL OUTER JOIN dbo.ExamVersions EV
			ON E.ID = EV.ExamID
		WHERE EV.ID IS NULL
END




GO
/****** Object:  StoredProcedure [dbo].[sm_CONSISTENCYCHECK_AssignedGroupMarks]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sm_CONSISTENCYCHECK_AssignedGroupMarks]
AS
BEGIN
	SET NOCOUNT ON
	
	SELECT [ID] as AssignedGroupMarkId
      ,[UserId] as UserId
      ,[UniqueGroupResponseId] as UniqueGroupResponseId
      ,[MarkingMethodId] as MarkingMethodId
      ,[GroupDefinitionID] as GroupDefinitionId
  FROM [dbo].[AssignedGroupMarks]
  WHERE [MarkingMethodId] IS NULL
	OR [UserId] IS NULL
	OR [GroupDefinitionID] IS NULL
END
GO
/****** Object:  StoredProcedure [dbo].[sm_CONSISTENCYCHECK_AutoGroupsHaveOneItem]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sm_CONSISTENCYCHECK_AutoGroupsHaveOneItem]
AS
BEGIN
	SET NOCOUNT ON
	
	SELECT GD.[ID] as GroupId
	, GD.[Name] as GroupName
	, GD.[IsAutomaticGroup] as IsAutoGroup
	, GD.[ExamID] as ExamId
	, I.[Id] as ItemId
	, I.[ExternalItemName] as ItemName
	FROM [dbo].[GroupDefinitions] GD
	INNER JOIN [dbo].GroupDefinitionItems GDI
		ON Gd.ID = GDI.GroupID
	INNER JOIN [dbo].Items I
		ON I.ID = GDI.ItemID
	WHERE GD.ID IN (
			SELECT GD.[ID] as GroupId
				FROM [dbo].[GroupDefinitions] GD
				INNER JOIN [dbo].GroupDefinitionItems GDI
					ON Gd.ID = GDI.GroupID
				INNER JOIN [dbo].Items I
					ON I.ID = GDI.ItemID
				WHERE GD.IsAutomaticGroup = 0
				GROUP BY GD.ID
				HAVING COUNT(I.ID) = 1
		)
		OR GD.ID IN (
			SELECT GD.[ID] as GroupId
				FROM [dbo].[GroupDefinitions] GD
				INNER JOIN [dbo].GroupDefinitionItems GDI
					ON Gd.ID = GDI.GroupID
				INNER JOIN [dbo].Items I
					ON I.ID = GDI.ItemID
				WHERE GD.IsAutomaticGroup = 1
				GROUP BY GD.ID
				HAVING COUNT(I.ID) > 1
		)
END
GO
/****** Object:  StoredProcedure [dbo].[sm_CONSISTENCYCHECK_CIAwaitingReview]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--
-- Value in CIs Awaiting Review column in Quality Control = Value in brackets in Marked column in Quota Management /Item View/ Items grid
--
CREATE PROCEDURE [dbo].[sm_CONSISTENCYCHECK_CIAwaitingReview]
AS
BEGIN
	SET NOCOUNT ON
		
	DECLARE @qualityControl	 TABLE(GroupId int
			,Name nvarchar(max)
			,TotalMark DECIMAL(18,10)
			,CITolerance DECIMAL(18,10)
			,NumCIsRequired int
			,UniqueResponses int					
			,Released int
			,ScriptType int
			,ItemId int
			,MarkingType int
            ,AllowAnnotations bit
			,IsAutomaticGroup bit
			,NumCIsCreated int
			,NumCIsReviewed int
			,NumUnmarked int		
	) 
	
	DECLARE @quotaManagement TABLE(GroupID int
			, GroupName nvarchar(4000)
			, IsManualMarkingType bit
			, NumResponses int
			, NumMarkedResponses int
			, NumAwaitingReview int
			, NumHumanMarkedResponses int
			, HasParkedResponses bit)
							
	declare @examIds table(examId int)	
	declare @examId int
		
	insert @examIds select e.ID FROM Exams e WHERE e.ID > 0
		
		while exists (select top 1 * from @examIds)
		begin
			select top 1 @examId = examId from @examIds
			delete from @examIds where examId = @examId									
																
			insert @qualityControl exec [dbo].[sm_QUALITYCONTROLSERVICE_getGroupsForExam_sp] @examId, null, 1			
			insert @quotaManagement exec [dbo].[sm_QUOTAMANAGEMENTSERVICE_getGroupsForExamWithNumResponses_sp] @examId																
		end	
		
		SELECT QC.GroupId,						
			(QC.NumCIsCreated - QC.NumCIsReviewed), 			
			QM.NumAwaitingReview				
			FROM @qualityControl QC
			left join @quotaManagement QM on QM.GroupID = QC.GroupId
			WHERE 	(QC.NumCIsCreated - QC.NumCIsReviewed) <> QM.NumAwaitingReview	
						
END



GO
/****** Object:  StoredProcedure [dbo].[sm_CONSISTENCYCHECK_CompareUniqueResponse]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--
-- Number of unique responses available for marking should be the same on Home, Quota Mngm and on Marking pages.
--
CREATE PROCEDURE [dbo].[sm_CONSISTENCYCHECK_CompareUniqueResponse]
AS
BEGIN
	SET NOCOUNT ON

           
	DECLARE @quotas TABLE(ID int,
                Forename nvarchar(50),
                Surname nvarchar(50),
                Quota int,
                NumberMarked int,
                Within1CIOfSuspension int,
                NumItemsParked int,
                Suspended bit,
                NumberSuspended int,
                HasRealMarkedResponses bit,
				HasMarkedCIs bit)
				
	DECLARE @marking TABLE(GroupId int
			, Name nvarchar(max)
			, IsReleased bit
			, AssignedQuota int
			, NumberMarked int
			, MarkingSuspended int
			, UniqueResponsesAvailableToMark int
			, AffectedUniqueResponses int)
			
				
	DECLARE @temp TABLE (userId int
			, groupId int		
			, quota int
			, marked int)
			
	DECLARE @temp2 TABLE (userId int
			, quota int
			, marked int)	
			
	DECLARE @result TABLE (userId int
			, groupId int		
			, quota int
			, marked int
			, quota1 int
			, marked1 int)	
	
	declare @groupIds table(groupId int)
	declare @groupId int
	
	DECLARE @users TABLE(UserId int)
	INSERT INTO @users
		SELECT Id FROM Users
	
	declare @userWithMostExam int
	
	WHILE EXISTS (select top 1 * from @users)
	BEGIN
		SET @userWithMostExam = 1
		select top 1 @userWithMostExam = UserId from @users
		delete from @users where UserId = @userWithMostExam
				
		DECLARE @tokenId INT = (
			SELECT TOP(1) ts.id 
			FROM TokenStore ts 
			WHERE ts.userId = @userWithMostExam 
			ORDER BY ts.lastInteractionDateTime DESC
		)
																		
		INSERT INTO @temp(userId, groupId, quota, marked)
		SELECT @userWithMostExam, T.Id, T.RemainingQuota, T.Marked
		FROM sm_MARKINGSERVICE_GetAllGroupsForUser_fn(@userWithMostExam, @tokenId, 0) AS T													
		
		insert @groupIds select TR.GroupId from @temp TR 	
		while exists (select top 1 * from @groupIds)
		begin
			select top 1 @groupId = groupId from @groupIds
			delete from @groupIds where groupId = @groupId
			
			insert @quotas exec [dbo].[sm_QUOTAMANAGEMENTSERVICE_getExaminersForGroupForExam_sp] @groupId
			
			INSERT INTO @temp2
				SELECT QS.ID as UserId
					, CASE WHEN QS.Quota = -1 THEN -1 ELSE QS.Quota - QS.NumberMarked END
					, QS.NumberMarked
				FROM @quotas as QS
			
			insert @result  
			SELECT TR.userId, TR.groupId, TR.quota, TR.marked, QS.quota, QS.marked				
			FROM @temp TR
			inner join @temp2 QS on QS.userId = TR.userId 
			WHERE TR.groupId = @groupId AND QS.quota <> TR.quota AND QS.marked <> TR.marked
				
			delete from @quotas
			DELETE FROM @temp2
		end
	END
	
	SELECT * FROM @result
END

GO
/****** Object:  StoredProcedure [dbo].[sm_CONSISTENCYCHECK_ContolGroupsHaveCIMark]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sm_CONSISTENCYCHECK_ContolGroupsHaveCIMark]
AS
BEGIN
	SET NOCOUNT ON

	;WITH TT AS 
	(
		SELECT UGR.ID, 
		(
			 SELECT COUNT(*)
				 FROM AssignedGroupMarks AGM
				 WHERE AGM.UniqueGroupResponseId = UGR.ID
					AND AGM.MarkingMethodId = 5
		) AS T
		FROM UniqueGroupResponses UGR 
			--JOIN AssignedGroupMarks AGM ON AGM.UniqueGroupResponseId = UGR.ID
		WHERE UGR.CI = 1
			AND UGR.ID >= 0 -- filter out test data
		GROUP by UGR.ID
	)
	SELECT * FROM TT WHERE TT.T = 0	
END
GO
/****** Object:  StoredProcedure [dbo].[sm_CONSISTENCYCHECK_EachAutoGroupContainsOnlyOneItem]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sm_CONSISTENCYCHECK_EachAutoGroupContainsOnlyOneItem]
AS
BEGIN

SELECT 
	gd.[ID],
	COUNT(gdi.[ItemID])
FROM [dbo].[GroupDefinitions] gd
INNER JOIN [dbo].[GroupDefinitionItems] gdi ON gd.[ID] = gdi.[GroupID]
WHERE gd.[IsAutomaticGroup] = 1
GROUP BY gd.[ID]
HAVING COUNT(gdi.[ItemID]) > 1
ORDER BY COUNT(gdi.[ItemID]) DESC

END
GO
/****** Object:  StoredProcedure [dbo].[sm_CONSISTENCYCHECK_GroupDefinitionStructureCrossRefConsistent]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sm_CONSISTENCYCHECK_GroupDefinitionStructureCrossRefConsistent]
AS
BEGIN
	SET NOCOUNT ON
	
	SELECT GD.[ID]
		FROM [dbo].[GroupDefinitions] GD
		WHERE NOT EXISTS(SELECT 1 FROM [dbo].[GroupDefinitionStructureCrossRef] R WHERE GD.[ID] = R.[GroupDefinitionID])
		
	SELECT S.[ID]
		FROM [dbo].[ExamVersionStructures] S
		WHERE NOT EXISTS(SELECT 1 FROM [dbo].[GroupDefinitionStructureCrossRef] R WHERE S.[ID] = R.[ExamVersionStructureID])		
END
GO
/****** Object:  StoredProcedure [dbo].[sm_CONSISTENCYCHECK_GroupsAndItemsAreBound]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sm_CONSISTENCYCHECK_GroupsAndItemsAreBound]
AS
BEGIN
	SET NOCOUNT ON
	
	SELECT GD.[ID] as GroupId
		, GD.[Name] as GroupName
		, GD.[IsAutomaticGroup] as IsAutoGroup
		, GD.[ExamID] as ExamId
		FROM [dbo].[GroupDefinitions] GD
		FULL OUTER JOIN [dbo].[GroupDefinitionItems] GDI
			ON GD.ID = GDI.GroupID
		WHERE GDI.GroupID IS NULL
	
	SELECT I.[ID] as ItemId
		, I.[ExternalItemName] as ItemName
		FROM [dbo].[Items] I
		FULL OUTER JOIN [dbo].[GroupDefinitionItems] GDI
			ON I.ID = GDI.ItemID
		WHERE GDI.ItemID IS NULL
END
GO
/****** Object:  StoredProcedure [dbo].[sm_CONSISTENCYCHECK_GroupsForHome]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[sm_CONSISTENCYCHECK_GroupsForHome]
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @freeResponses TABLE 
	(
		UgrId BIGINT
	)

	DECLARE @affectedScripts TABLE (
		CEVID INT
	)

	DECLARE @responsesFromAffectedScripts TABLE 
	(
		UgrId BIGINT
	)

	DECLARE @resultTable TABLE (
		GroupId INT,
		ResponsesToMark INT
	)

	DECLARE @spResultTable TABLE
	(
		QualificationName NVARCHAR(MAX),
		QualificationId INT,
		ExamName NVARCHAR(MAX),
		ExamID INT,
		ExamVersionName NVARCHAR(MAX),
		GroupId INT,
		GroupName NVARCHAR(MAX),
		ResponsesToMark INT,
		Suspended BIT,
		VIP DECIMAL,
		EarliestDate DATETIME
	)
	
	DECLARE @results TABLE
	(
		UserId INT,
		GroupId_Simple INT,
		GroupId_Function INT,
		ToMark_Simple INT,
		ToMark_Function INT,
		GroupsCount_Simple INT,
		GroupsCount_Function INT
	)

	DECLARE @exam INT
	DECLARE @countOfAffectedUGRs INT
	DECLARE @countOfFreeUGRs INT
	DECLARE @shouldApplyRestriction BIT
	DECLARE @restrictionFlag BIT

	DECLARE @defaultMarkingRestriction BIT = 1

	DECLARE @userId INT
	DECLARE @users CURSOR
	SET @users = CURSOR FOR SELECT u.ID FROM Users u

	OPEN @users
		   FETCH NEXT
		   FROM @users INTO @userId
		   WHILE @@FETCH_STATUS = 0
				 BEGIN

					--PRINT N'Processing ... ' + RTRIM(CAST(@usersCounter AS nvarchar(15))) + N'/' + RTRIM(CAST(@usersCount AS nvarchar(15))) + ''

					DELETE FROM @resultTable
					DELETE FROM @spResultTable

					DECLARE @tokenStoreID INT = (SELECT TOP 1 ID FROM TokenStore WHERE userId = @userId)

					DECLARE @IsUserHasGlobalUnrestrictedExaminerPermission BIT = 
							CASE WHEN 

								(SELECT COUNT(*)
									FROM AssignedUserRoles AUR
									INNER JOIN RolePermissions
									ON AUR.RoleID = RolePermissions.RoleID
									WHERE AUR.UserID = @userId 
									AND RolePermissions.PermissionID = 42 
									AND AUR.ExamID = 0
								) > 0 
								THEN 1 
								ELSE 0 
							END

					DECLARE @groupId INT
					DECLARE @groups CURSOR
					SET @groups = CURSOR FOR SELECT gd.ID
											 FROM GroupDefinitions gd 
											 INNER JOIN QuotaManagement qm
											 ON qm.GroupId = gd.ID
											 WHERE qm.AssignedQuota = -1
											 AND qm.UserId = @userId
											 AND GD.IsReleased = 1
											 AND gd.id IN 
											 (
                         						SELECT GDSCR.GroupDefinitionID
												FROM   dbo.GroupDefinitionStructureCrossRef GDSCR
												INNER JOIN dbo.ExamVersionStructures EVS ON  EVS.ID = GDSCR.ExamVersionStructureID
												WHERE  GDSCR.Status = 0
												AND EVS.StatusID = 0
											 )
											 AND EXISTS 
											 (
						 						SELECT * FROM dbo.AssignedUserRoles AUR
												INNER JOIN RolePermissions RP on RP.RoleID = AUR.RoleID
												WHERE AUR.UserID = @userId AND RP.PermissionID in (12,13,14) 
												and (AUR.ExamID = 0 or AUR.ExamID = GD.ExamID)
											 )
											 AND EXISTS
											 (
												SELECT *
												FROM UniqueGroupResponses ugr
												WHERE ugr.GroupDefinitionID = gd.ID	
											 )

					OPEN @groups
						   FETCH NEXT
						   FROM @groups INTO @groupId
						   WHILE @@FETCH_STATUS = 0
								 BEGIN

									DELETE FROM @freeResponses
									DELETE FROM @affectedScripts
									DELETE FROM @responsesFromAffectedScripts


									-- Unique Group Responses which are neither Marked nor Escalated, nor CI, nor occupied by other users
									INSERT INTO @freeResponses
									SELECT ugr.ID
									FROM UniqueGroupResponses ugr
									WHERE ugr.GroupDefinitionID = @groupId
									AND UGR.CI = 0
									AND UGR.ConfirmedMark IS NULL
									AND UGR.ParkedUserID IS NULL
									AND
									( 
										UGR.[tokenid] IS NULL 
										OR 
										UGR.[tokenid] = @tokenStoreID
									)

									SET @exam = (SELECT gd.ExamID
												 FROM GroupDefinitions gd 
												 WHERE gd.ID = @groupId)

									-- scripts of current Exam Version which contain Unique Group Responses that were marked by current User
									INSERT INTO @affectedScripts
									(
										CEVID
									)
									SELECT DISTINCT ACEV.CandidateExamVersionId AS AffectedScript
									FROM CandidateExamVersions CEV
									INNER JOIN CandidateGroupResponses cgr
									ON cgr.CandidateExamVersionID = CEV.ID
									INNER JOIN UniqueGroupResponses ugr
									ON ugr.id = cgr.UniqueGroupResponseID
									INNER JOIN AffectedCandidateExamVersions ACEV
									ON ACEV.CandidateExamVersionId = CEV.ID
									WHERE ugr.GroupDefinitionID = @groupId
									AND ACEV.UserId = @userId
									AND ACEV.GroupDefinitionID IN 
									(
										SELECT GDSCR.GroupDefinitionID
										FROM   dbo.GroupDefinitionStructureCrossRef GDSCR
										INNER JOIN dbo.ExamVersionStructures EVS ON  EVS.ID = GDSCR.ExamVersionStructureID
										WHERE  GDSCR.Status = 0
										AND EVS.StatusID = 0
									)

									-- get Free Unique Group Responses (neither Marked nor Escalated, nor CI, nor occupied by other users) 
									-- from Affected Scripts for current Group
									INSERT INTO @responsesFromAffectedScripts
									(
										UgrId
									)
									SELECT DISTINCT cgr.UniqueGroupResponseID
									FROM CandidateGroupResponses cgr
									INNER JOIN @affectedScripts AFS	
									ON AFS.CEVID = cgr.CandidateExamVersionID
									INNER JOIN UniqueGroupResponses ugr
									ON ugr.ID = cgr.UniqueGroupResponseID
									WHERE UGR.CI = 0
									AND UGR.ConfirmedMark IS NULL
									AND UGR.ParkedUserID IS NULL
									AND
									( 
										 UGR.[tokenid] IS NULL 
										 OR 
										 UGR.[tokenid] = @tokenStoreID
									)
									AND ugr.GroupDefinitionID = @groupId
									--AND NOT EXISTS(
									--				SELECT *
									--				FROM AffectedCandidateExamVersions ACEV
									--				INNER JOIN @affectedScripts afs
									--				ON afs.CEVID = acev.CandidateExamVersionId
									--				WHERE UserId = @userId
									--				AND GroupDefinitionId = @groupId
									--				AND acev.CandidateExamVersionId = cgr.CandidateExamVersionID
									--)

									SET @countOfAffectedUGRs = (SELECT COUNT(*) FROM @responsesFromAffectedScripts)
									SET @countOfFreeUGRs = (SELECT COUNT(*) FROM @freeResponses)

									-- user has Global Unrestricted Examiner Permission 
									-- OR has Unrestricted Examiner Permission for current Exam
									IF(@IsUserHasGlobalUnrestrictedExaminerPermission = 1 OR (SELECT COUNT(*)
											FROM AssignedUserRoles AUR
											INNER JOIN RolePermissions RP
											ON AUR.RoleID = RP.RoleID
											WHERE AUR.UserID = @userId
											AND RP.PermissionID = 42
											AND AUR.ExamID = @exam) > 0)
										BEGIN
											-- we have not apply restrictions
											SET @shouldApplyRestriction = 0
										END
									ELSE
									BEGIN

											-- user hoesn't have Unrestricted Examiner Permission
											-- so we have to check Restriction Flag for Exam Version
											SET @restrictionFlag = (
																		SELECT e.IsMarkOneItemPerScript
																		FROM Exams e
																		WHERE e.ID = @exam
																	)

											SET @shouldApplyRestriction = ISNULL(@restrictionFlag, @defaultMarkingRestriction)

										END

									DECLARE @message NVARCHAR(max) = '@shouldApplyRestriction = '
										+ CAST(@shouldApplyRestriction as NVARCHAR(100))
										+ '; @groupId = '
										+ CAST(@groupId as NVARCHAR(100))
										+ '; @countOfFreeUGRs = '
										+ CAST(@countOfFreeUGRs as NVARCHAR(100))
										+ '; @countOfAffectedUGRs = '
										+ CAST(@countOfAffectedUGRs as NVARCHAR(100))
									IF(@shouldApplyRestriction = 1)
									BEGIN
										IF ((@countOfFreeUGRs - @countOfAffectedUGRs) > 0)
											BEGIN

												INSERT INTO @resultTable
												(
													GroupId, 
													ResponsesToMark
												)
												VALUES
												(
													@groupId,
													@countOfFreeUGRs - @countOfAffectedUGRs
												)

											END
										ELSE
											BEGIN
												PRINT('Not Added: ' + @message)
											END
									END
									ELSE
											BEGIN
												IF(@countOfFreeUGRs > 0)
												BEGIN
													INSERT INTO @resultTable
													(
														GroupId, 
														ResponsesToMark
													)
													VALUES
													(
														@groupId,
														@countOfFreeUGRs
													)
												END
												ELSE
												BEGIN
													PRINT('Not Added: ' + @message)
												END
											END			



									FETCH NEXT FROM @groups INTO @groupId
								 END
					CLOSE @groups
					DEALLOCATE @groups

					INSERT INTO @spResultTable 
					EXEC sm_WELCOMESERVICE_getGroupsToMark_sp
						@userId = @userId,
						@maxNumItems = 10000,
						@tokenStoreID = @tokenStoreID,
						@defaultMarkingRestriction = @defaultMarkingRestriction

					INSERT INTO @results
						SELECT @userId as UserId
							, rt.GroupId as GroupId_Simple
							, srt.GroupId as GroupId_Function
							, rt.ResponsesToMark AS ToMark_Simple
							, srt.ResponsesToMark AS ToMark_Function
							, (SELECT COUNT(*) FROM @resultTable) AS GroupsCount_Simple
							, (SELECT COUNT(*) FROM @spResultTable) as GroupsCount_Function
							FROM @spResultTable srt
							FULL JOIN @resultTable rt
								ON srt.GroupId = rt.GroupId
							WHERE srt.ResponsesToMark != rt.ResponsesToMark
								OR srt.ResponsesToMark IS NULL
								OR rt.ResponsesToMark IS NULL
								OR rt.GroupId IS NULL
								OR srt.GroupId IS NULL

					FETCH NEXT FROM @users INTO @userId
				 END
	CLOSE @users
	DEALLOCATE @users
	
	SELECT * FROM @results
END
GO
/****** Object:  StoredProcedure [dbo].[sm_CONSISTENCYCHECK_ItemsHaveCandidateExamVersions]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sm_CONSISTENCYCHECK_ItemsHaveCandidateExamVersions]
AS
BEGIN
	SET NOCOUNT ON
	
	SELECT I.Id as ItemId
	, I.ExternalItemId
	FROM [dbo].[Items] I
	LEFT OUTER JOIN [dbo].[ExamVersionItems] EVI
		ON I.Id = EVI.ItemId
	WHERE EVI.ItemId IS NULL
END
GO
/****** Object:  StoredProcedure [dbo].[sm_CONSISTENCYCHECK_ItemsHaveExamSections]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sm_CONSISTENCYCHECK_ItemsHaveExamSections]
AS
BEGIN
	SET NOCOUNT ON
	
	SELECT I.Id as Items_ItemId
	, ESI.ItemId as ExamSectionItems_ItemId
	FROM [dbo].ExamSectionItems ESI
	FULL OUTER JOIN [dbo].[Items] I
		ON I.Id = ESI.ItemId
	WHERE I.Id is null or ESI.ItemId is null
END
GO
/****** Object:  StoredProcedure [dbo].[sm_CONSISTENCYCHECK_ItemsHaveExamVersions]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sm_CONSISTENCYCHECK_ItemsHaveExamVersions]
AS
BEGIN
	SET NOCOUNT ON
	
	SELECT I.Id as ItemId
	, I.ExternalItemId
	FROM [dbo].[Items] I
	LEFT OUTER JOIN [dbo].[ExamVersionItems] EVI
		ON I.Id = EVI.ItemId
	WHERE EVI.ItemId IS NULL
END
GO
/****** Object:  StoredProcedure [dbo].[sm_CONSISTENCYCHECK_ItemsHaveExamVersionStructures]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sm_CONSISTENCYCHECK_ItemsHaveExamVersionStructures]
AS
BEGIN
	SET NOCOUNT ON
	
	SELECT I.Id as Items_ItemId
	, EVSI.ItemId as ExamVersionStructureItems_ItemId
	FROM [dbo].ExamVersionStructureItems EVSI
	FULL OUTER JOIN [dbo].[Items] I
		ON I.Id = EVSI.ItemId
	WHERE I.Id is null or EVSI.ItemId is null
END
GO
/****** Object:  StoredProcedure [dbo].[sm_CONSISTENCYCHECK_ItemsHaveUniqueResponses]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[sm_CONSISTENCYCHECK_ItemsHaveUniqueResponses]
AS
BEGIN
	SET NOCOUNT ON
	
	SELECT I.Id as ItemId
	, I.ExternalItemId
	FROM [dbo].[Items] I
	LEFT OUTER JOIN [dbo].[UniqueResponses] UR
		ON I.Id = UR.ItemId
	WHERE UR.ItemId IS NULL
END
GO
/****** Object:  StoredProcedure [dbo].[sm_CONSISTENCYCHECK_MarkableItemCannotBeInSeveralGroups]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--
-- Items set within a group is unique: so cannot have exactly same markable items (ViewOnly=0) within several groups.
-- The sproc returns items failing the validation.
--
CREATE PROCEDURE [dbo].[sm_CONSISTENCYCHECK_MarkableItemCannotBeInSeveralGroups]
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @result TABLE(StructureId int, ItemId int, DuplicateCount int)

	DECLARE @structures TABLE(StructureId int)
	INSERT INTO @structures(StructureId)
		SELECT ID FROM dbo.ExamVersionStructures

	DECLARE @structureId int
	WHILE EXISTS (SELECT TOP 1 * FROM @structures)
	BEGIN
		SELECT TOP 1 @structureId = StructureId FROM @structures
		DELETE FROM @structures WHERE StructureId = @structureId

		INSERT @result(StructureId, ItemId, DuplicateCount)
			SELECT 
				 @structureId
				,GDI.ItemID
				,COUNT(*)
			FROM dbo.GroupDefinitionItems GDI
				JOIN dbo.GroupDefinitions GD ON GD.ID = GDI.GroupID
				JOIN dbo.GroupDefinitionStructureCrossRef GDSCR ON GDSCR.GroupDefinitionID = GD.ID AND GDSCR.ExamVersionStructureID = @structureId AND GDSCR.Status = 0
			WHERE GDI.ViewOnly = 0
			GROUP BY GDI.ItemID
			HAVING COUNT(*) > 1
	END

	SELECT * FROM @result
END
GO
/****** Object:  StoredProcedure [dbo].[sm_CONSISTENCYCHECK_Marked]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--
-- Values in Marked column per item in Quota Management /Examiners grid/ Item View =  Values in Marked column per item on Marking
--
CREATE PROCEDURE [dbo].[sm_CONSISTENCYCHECK_Marked]
AS
BEGIN
	SET NOCOUNT ON
	
	DECLARE @quotaService TABLE(ID int,
                Forename nvarchar(50),
                Surname nvarchar(50),
                Quota int,
                NumberMarked int,
                Within1CIOfSuspension int,
                NumItemsParked int,
                Suspended bit,
                NumberSuspended int,
                HasRealMarkedResponses bit,
				HasMarkedCIs bit)
			
	DECLARE @tempMarking TABLE (userId int
			, groupId int
			, examId int	
			, marked int)
	
	DECLARE @tempQuota TABLE (userId int
			, groupId int							
			, marked int)
				
	declare @groupIds table(groupId int)	
	declare @groupId int
	
	-- all active groups
	INSERT @groupIds 
		SELECT GD.ID 
		FROM GroupDefinitions GD
		JOIN GroupDefinitionStructureCrossRef GDSCR ON GDSCR.GroupDefinitionID = GD.ID
		WHERE GD.ID > 0 AND GDSCR.Status = 0
	
	-- for each group we execute sp and get NumberMarked for users
	while exists (select top 1 * from @groupIds)
	begin
		select top 1 @groupId = groupId from @groupIds
		delete from @groupIds where groupId = @groupId		
							
		insert @quotaService exec [dbo].[sm_QUOTAMANAGEMENTSERVICE_getExaminersForGroupForExam_sp] @groupId				
		
		insert @tempQuota 
		SELECT QS.ID, @groupId, QS.NumberMarked
		FROM @quotaService QS 		
			
		delete from @quotaService 	 
	end		
					
	-- Run for a single user who has access to the max number of exam versions
	declare @userWithMostExam int
	select TOP 1 @userWithMostExam = UserId
		from @tempQuota
		group by UserId
		order by count(*) DESC
						
	DECLARE @tokenId INT = (
		SELECT TOP(1) ts.id 
		FROM TokenStore ts 
		WHERE ts.userId = @userWithMostExam 
		ORDER BY ts.lastInteractionDateTime DESC
	)
																	
	INSERT INTO @tempMarking
	(
		userId,
		groupId,
		examId,
		marked
	)
	SELECT @userWithMostExam, T.Id, T.ExamId, T.Marked
	FROM sm_MARKINGSERVICE_GetAllGroupsForUser_fn(@userWithMostExam, @tokenId, 0) AS T			
		
	SELECT TQ.userId userId, TQ.groupId groupId, TM.examId examId, TQ.marked qmMarked, TM.marked msMarked 
	FROM @tempQuota TQ
	left join GroupDefinitions GD on GD.ID = TQ.groupId
	left join @tempMarking TM on TM.userId = TQ.userId AND TM.groupId = TQ.groupId 
	WHERE TM.examId IS NOT NULL AND TQ.marked <> TM.marked		
END

GO
/****** Object:  StoredProcedure [dbo].[sm_CONSISTENCYCHECK_MarkedCIsDeviation]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--
-- Value in MarkinDeviation column should be equal to difference between the mark awarded by marker and mark awarded by CI-creator. For groups and for items.
--
CREATE PROCEDURE [dbo].[sm_CONSISTENCYCHECK_MarkedCIsDeviation]
AS
BEGIN
    SET NOCOUNT ON
    SELECT 'Groups' AS 'DataLevel'
		  ,mc.UniqueGroupResponseID
		  ,mc.AssignedMarkID
		  ,-1                         AS 'UniqueResponseId'
		  ,mc.GroupName
		  ,mc.GroupID
		  ,-1                         AS 'ItemId'
		  ,'' AS 'ItemName'
		  ,mc.MarkingDeviation
		  ,ABS(mc.AssignedMark- mc.ConfirmedMark) AS 'RealMarkingDeviation'
		  ,mc.[Timestamp]
	FROM   dbo.[MarkedCIGroups_view]  AS mc
	WHERE  ABS(mc.AssignedMark- mc.ConfirmedMark)<>mc.MarkingDeviation
		   AND mc.IsEscalated = 0
	UNION ALL 
	SELECT 'Items' AS 'DataLevel'
		  ,ugrl.UniqueGroupResponseID
		  ,mc.AssignedMarkID
		  ,ugrl.UniqueResponseId
		  ,mc.GroupName
		  ,mc.GroupID
		  ,i.ID                     AS 'ItemId'
		  ,i.ExternalItemName       AS 'ItemName'
		  ,aim.MarkingDeviation
		  ,(
			   ABS(
				   (
					   SELECT TOP(1) aim2.AssignedMark
					   FROM   dbo.AssignedGroupMarks AGM
							  INNER JOIN dbo.AssignedItemMarks aim2 ON  aim2.GroupMarkId = AGM.ID
									   AND aim2.UniqueResponseId = aim.UniqueResponseId
					   WHERE  AGM.UniqueGroupResponseId = UGR.ID
							  AND AGM.MarkingMethodId = 6
					   ORDER BY
							  TIMESTAMP DESC
				   )- aIM.AssignedMark
			   )
		   )                        AS 'RealMarkingDeviation'
		  ,mc.[Timestamp]
	FROM   dbo.MarkedCIGroups_view  AS mc
		   INNER JOIN dbo.UniqueGroupResponses UGR ON  mc.UniqueGroupResponseID = UGR.ID
		   INNER JOIN dbo.UniqueGroupResponseLinks ugrl ON  ugrl.UniqueGroupResponseID = mc.UniqueGroupResponseID
		   INNER JOIN dbo.AssignedItemMarks aim ON  aim.GroupMarkId = mc.AssignedMarkID
					AND aim.UniqueResponseId = ugrl.UniqueResponseId
		   INNER JOIN dbo.GroupDefinitionItems gdi ON  gdi.GroupID = mc.GroupID
		   INNER JOIN dbo.Items i ON  i.ID = gdi.ItemID
	WHERE  mc.IsEscalated = 0
		   AND ABS(
				   (
					   SELECT TOP(1) aim2.AssignedMark
					   FROM   dbo.AssignedGroupMarks AGM
							  INNER JOIN dbo.AssignedItemMarks aim2 ON  aim2.GroupMarkId = AGM.ID
									   AND aim2.UniqueResponseId = aim.UniqueResponseId
					   WHERE  AGM.UniqueGroupResponseId = UGR.ID
							  AND AGM.MarkingMethodId = 6
					   ORDER BY
							  TIMESTAMP DESC
				   )- aIM.AssignedMark
			   )<>aim.MarkingDeviation
    
END
GO
/****** Object:  StoredProcedure [dbo].[sm_CONSISTENCYCHECK_MarkingExaminerReport]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sm_CONSISTENCYCHECK_MarkingExaminerReport]
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @startDate datetime = DATEADD(YEAR, -1, GETDATE())
	DECLARE @endDate datetime = GETDATE()

	DECLARE @markingReport TABLE(UniqueResponses INT, CIs INT)
	DECLARE @markingExaminerReport TABLE(UserId INT, UniqueResponses INT, CIs INT)

	INSERT INTO @markingExaminerReport (UserId
										, UniqueResponses
										, CIs)
	SELECT UserId as UserId
	, UniqueResponses as UniqueResponses
	, ControlItems as CIs FROM sm_getMarkingExaminerReport_fn(1, @startDate, @endDate)

	DECLARE @userId INT = 0

	DECLARE userIdcursor CURSOR FOR
		SELECT DISTINCT UserId FROM @markingExaminerReport
	OPEN userIdcursor
	FETCH NEXT FROM userIdcursor
		INTO @userId
	WHILE @@FETCH_STATUS = 0
	BEGIN
		INSERT INTO @markingReport
			SELECT UniqueResponses as UniqueResponses
					, ControlItems as CIs
				FROM sm_getMarkingReportItems_fn(@userId, @startDate, @endDate)
		FETCH NEXT FROM userIdcursor 
			INTO @userId
	END 
	CLOSE userIdcursor;
	DEALLOCATE userIdcursor;

	DECLARE @count INT
	DECLARE @uniqueResponsesCount INT
	DECLARE @cisCount INT
	SELECT @count = ISNULL(COUNT(*), 0), @uniqueResponsesCount = ISNULL(Sum(UniqueResponses), 0), @cisCount = ISNULL(Sum(CIs), 0) FROM @markingExaminerReport
	PRINT('Count: ' + Cast(@count as NVARCHAR(100)) + '; URs: ' + Cast(@uniqueResponsesCount as NVARCHAR(100)) + '; CIs: ' + Cast(@cisCount as NVARCHAR(100)))

	DECLARE @oldCount INT
	DECLARE @oldUniqueResponsesCount INT
	DECLARE @oldCisCount INT
	SELECT @oldCount = ISNULL(COUNT(*), 0), @oldUniqueResponsesCount = ISNULL(Sum(UniqueResponses), 0), @oldCisCount = ISNULL(Sum(CIs), 0) FROM @markingReport
	PRINT('Count: ' + Cast(@oldCount as NVARCHAR(100)) + '; URs: ' + Cast(@oldUniqueResponsesCount as NVARCHAR(100)) + '; CIs: ' + Cast(@oldCisCount as NVARCHAR(100)))

	IF (@oldCount = @count AND @oldUniqueResponsesCount = @uniqueResponsesCount AND @oldCisCount = @cisCount)
		PRINT('GREAT!')
	ELSE
		SELECT('Test failed')
END
GO
/****** Object:  StoredProcedure [dbo].[sm_CONSISTENCYCHECK_Quota]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--
-- Quota on Quotas Tab and Assigned Quota on Marking tab
--
CREATE PROCEDURE [dbo].[sm_CONSISTENCYCHECK_Quota]
AS
BEGIN
	SET NOCOUNT ON
	
	DECLARE @quotas TABLE(ID int,
                Forename nvarchar(50),
                Surname nvarchar(50),
                Quota int,
                NumberMarked int,
                Within1CIOfSuspension int,
                NumItemsParked int,
                Suspended bit,
                NumberSuspended int,
                HasRealMarkedResponses bit,
				HasMarkedCIs bit)

	
	DECLARE @tempMarking TABLE (userId int
			, groupId int
			, examId int	
			, quota int)
	
	DECLARE @tempQuota TABLE (userId int
			, groupId int							
			, quota int)
			
	declare @groupIds table(groupId int)	
	declare @groupId int
	
	insert @groupIds 
		select GD.ID 
		FROM GroupDefinitions GD
		JOIN GroupDefinitionStructureCrossRef GDSCR ON GDSCR.GroupDefinitionID = GD.ID
		WHERE GD.ID > 0 AND GDSCR.Status = 0
		
	while exists (select top 1 * from @groupIds)
	begin
		select top 1 @groupId = groupId from @groupIds
		delete from @groupIds where groupId = @groupId		
							
		insert @quotas exec [dbo].[sm_QUOTAMANAGEMENTSERVICE_getExaminersForGroupForExam_sp] @groupId								
		insert @tempQuota 
		SELECT QS.ID, @groupId, CASE WHEN QS.Quota - QS.NumberMarked < 0 THEN -1 ELSE QS.Quota - QS.NumberMarked END
		FROM @quotas QS 		
			
		delete from @quotas 	 
	end		

	-- Run for a single user who has access to the max number of exam versions
	declare @userWithMostExam int
	select TOP 1 @userWithMostExam = UserId
		from @tempQuota
		group by UserId
		order by count(*) desc
					
	DECLARE @tokenId INT = (
		SELECT TOP(1) ts.id 
		FROM TokenStore ts 
		WHERE ts.userId = @userWithMostExam 
		ORDER BY ts.lastInteractionDateTime DESC
	)
																	
	INSERT INTO @tempMarking
	(
		userId,
		groupId,
		examId,
		quota
	)
	SELECT @userWithMostExam, T.Id, T.ExamId, T.RemainingQuota
	FROM sm_MARKINGSERVICE_GetAllGroupsForUser_fn(@userWithMostExam, @tokenId, 0) AS T			
												

	SELECT TQ.userId userId, TQ.groupId groupId, TM.examId examId, TQ.quota qmQuota, TM.quota msQuota 
	FROM @tempQuota TQ
	left join GroupDefinitions GD on GD.ID = TQ.groupId
	left join @tempMarking TM on TM.userId = TQ.userId AND TM.groupId = TQ.groupId 
	WHERE TM.examId IS NOT NULL AND TQ.quota <> TM.quota		
END

GO
/****** Object:  StoredProcedure [dbo].[sm_CONSISTENCYCHECK_ScriptStatus]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--
-- Script Review: 
-- 1. [Marking completed %] against script status
-- 2. Mark against TotalMark
--
CREATE PROCEDURE [dbo].[sm_CONSISTENCYCHECK_ScriptStatus]
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @scripsService TABLE(ID INT NOT NULL
           ,Qualification NVARCHAR(100)
           ,QualificationID INT NOT NULL
           ,Exam VARCHAR(200)
           ,ExamID INT NOT NULL
           ,ExamVersion NVARCHAR(100)
           ,ExamVersionID INT NOT NULL
           ,Keycode NVARCHAR(12)
           ,MARK DECIMAL(18 ,10) NULL
           ,TotalMarks DECIMAL(18 ,10) NOT NULL
           ,ExamSessionState INT NULL
           ,Percentage DECIMAL(38 ,16) NOT NULL
           ,PercentageMarked INT NOT NULL
           ,Submitted DATETIME NULL -- the date when the student push submit button. value comes from SA and can be NULL
           ,Imported DATETIME NOT NULL -- the date when script comes to SM from SA
           ,IsFlagged BIT NOT NULL
           ,VIP BIT NOT NULL
           ,CandidateSurname NVARCHAR(50) NULL
           ,CandidateForename NVARCHAR(50) NULL
           ,CandidateFullName NVARCHAR(101) NULL
           ,CandidateRef NVARCHAR(300) NULL
           ,CentreName NVARCHAR(100) NULL
           ,CentreCode NVARCHAR(50) NULL
           ,Country NVARCHAR(50) NULL
           ,IsAllowReview BIT
           ,IsAllowModerate BIT
           ,IsAllowConfirm BIT
           ,IsAllowVip BIT
           ,IsAllowFlag BIT
           ,IsModerated BIT)           	
			
	DECLARE @result TABLE (
		 type varchar(20)
		,scriptId int
		,percentageMarked decimal(38 ,16)
		,scriptStatus int
		,mark DECIMAL(18 ,10)
		,totalMark DECIMAL(18 ,10)
		)	
		
	
	declare @users table(userId int)
	declare @userId int
	insert @users select id from Users
	
		
	while exists (select top 1 * from @users)
	begin
		select top 1 @userId = userId from @users
		delete from @users where userId = @userId
				
		insert @scripsService 
		(
			ID
           ,Qualification
           ,QualificationID
           ,Exam
           ,ExamID
           ,ExamVersion
           ,ExamVersionID
           ,Keycode
           ,MARK
           ,TotalMarks
           ,ExamSessionState
           ,Percentage
           ,PercentageMarked
           ,Submitted
           ,Imported
           ,IsFlagged
           ,VIP
           ,CandidateSurname
           ,CandidateForename
           ,CandidateFullName
           ,CandidateRef
           ,CentreName
           ,CentreCode
           ,Country
           ,IsAllowReview
           ,IsAllowModerate
           ,IsAllowConfirm
           ,IsAllowVip
           ,IsAllowFlag
           ,IsModerated
		)
		SELECT
			ID
           ,Qualification
           ,QualificationID
           ,Exam
           ,srv.ExamID
           ,ExamVersion
           ,ExamVersionID
           ,Keycode
           ,MARK
           ,TotalMarks
           ,ExamSessionState
           ,Percentage
           ,PercentageMarked
           ,Submitted
           ,Imported
           ,IsFlagged
           ,VIP
           ,CandidateSurname
           ,CandidateForename
           ,CandidateFullName
           ,CandidateRef
           ,CentreName
           ,CentreCode
           ,Country
           ,IsAllowReview
           ,IsAllowModerate
           ,IsAllowConfirm
           ,IsAllowVip
           ,IsAllowFlag
           ,IsModerated
		FROM ScriptReview_view srv 
		INNER JOIN sm_getExamPermissionsForScriptReview_fn(@userId) perms 
		ON srv.ExamID = perms.PermExamID
		
		-- [Marking completed %] against script status
		insert @result (type, scriptId, percentageMarked, scriptStatus, mark, totalMark)
			SELECT 'script status', SC.ID, SC.PercentageMarked, SC.ExamSessionState, SC.MARK, SC.TotalMarks
				FROM @scripsService SC
				WHERE 
				   (SC.PercentageMarked = 0 AND SC.ExamSessionState <> 1) 
				OR (SC.PercentageMarked = 100 AND SC.ExamSessionState < 3) 
				OR (SC.PercentageMarked > 0 AND SC.PercentageMarked < 100 AND SC.ExamSessionState NOT IN (2, 5))

		-- Mark > TotalMark
		insert @result (type, scriptId, percentageMarked, scriptStatus, mark, totalMark)
			SELECT 'Mark > TotalMark', SC.ID, SC.PercentageMarked, SC.ExamSessionState, SC.MARK, SC.TotalMarks
				FROM @scripsService SC
				WHERE 
				   (SC.MARK > SC.TotalMarks)
		
		delete from @scripsService	
	end
		
	SELECT distinct * FROM @result

	-- D.Adov: there can be old scripts having Mark > TotalMark. You
	-- You can use ther following query to fix them.
	/*
	update CandidateExamVersions
	set Mark = TotalMark
	where Mark > TotalMark AND ExamSessionState = 7
	*/
END
GO
/****** Object:  StoredProcedure [dbo].[sm_CONSISTENCYCHECK_TotalUniqueResponce]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--
-- Quota management -> Review Marked Items window: Total Unique Responses ( overall number of URs) and Total marked ( = number of items marked during Marking Session + Submit Escalated Response + Create CI + Revoke CI)
--
CREATE PROCEDURE [dbo].[sm_CONSISTENCYCHECK_TotalUniqueResponce]
AS
BEGIN
	SET NOCOUNT ON


	DECLARE @responceInfo TABLE(Total int, NumMarked int)
    
	DECLARE @examMarked TABLE(GroupID int
			, GroupName nvarchar(4000)
			, IsManualMarkingType bit
			, NumResponses int
			, NumMarkedResponses int
			, NumAwaitingReview int
			, NumHumanMarkedResponses int
			, HasParkedResponses bit)
			
	DECLARE @result TABLE (examVersionId int
			, totalUnique int
			, numMarked int
			, vmUnique int
			, vmMarked int)	
		
	declare @examIds table(examId int)	
	declare @examId int
			
	insert @examIds select ID from Exams	
	
	while exists (select top 1 * from @examIds)
	begin
		select top 1 @examId = examId from @examIds
		delete from @examIds where examId = @examId
													
		insert @responceInfo exec [sm_QUOTAMANAGEMENTSERVICE_getResponseInformationforExam_sp] @examId
		insert @examMarked exec [dbo].[sm_QUOTAMANAGEMENTSERVICE_getGroupsForExamWithNumResponses_sp] @examId
		
		--compare
		insert @result SELECT @examId, RI.Total, RI.NumMarked,
		(SELECT SUM(VM.NumResponses) FROM @examMarked VM),
		(SELECT SUM(VM.NumMarkedResponses) FROM @examMarked VM)
		FROM @responceInfo RI
		
		delete from @examMarked 
		delete from @responceInfo 	 								
	end								
	
	
	SELECT * FROM @result R
	WHERE R.numMarked <> R.vmMarked OR R.totalUnique <> R.vmUnique
END

/*  
USE [SecureMarker]
GO

DECLARE	@return_value int

EXEC	@return_value = [dbo].[sm_WELCOMESERVICE_getGroupsToMark_sp]
		@userId = 1,
		@maxNumItems = 100,
		@tokenStoreID = 0,
		@defaultMarkingRestriction = 0

SELECT	'Return Value' = @return_value

GO
------------------
USE [SecureMarker]
GO

DECLARE	@return_value int

EXEC	@return_value = [dbo].[sm_QUOTAMANAGEMENTSERVICE_getExaminersForGroupForExamVersion_sp]
		@groupID = 3783

SELECT	'Return Value' = @return_value

GO
------------------
USE [SecureMarker]
GO

DECLARE	@return_value int

EXEC @return_value = [dbo].[sm_MARKINGSERVICE_GetAllGroupsForExamVersion_sp]
	@userId = 1,
	@examVersionId = 234,
	@tokenStoreID = 0
SELECT	'Return Value' = @return_value

GO

  */
GO
/****** Object:  StoredProcedure [dbo].[sm_CONSISTENCYCHECK_UniqueResponses]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--
-- Quota management -> Edit Quota dialogue: Number of unassigned unique responses = Value in red brackets in Unique Responses column in Quota Management /Item View/ Items grid
--
CREATE PROCEDURE [dbo].[sm_CONSISTENCYCHECK_UniqueResponses]
AS
BEGIN
    SET NOCOUNT ON
    
    DECLARE @quotaService TABLE(UnassignedQuota INT, NumUniqueResponses INT)
    
    DECLARE @examMarked TABLE(
                GroupID INT
               ,GroupName NVARCHAR(4000)
               ,IsManualMarkingType BIT
               ,NumResponses INT
               ,NumMarkedResponses INT
               ,NumAwaitingReview INT
               ,NumHumanMarkedResponses INT
               ,NumParkedResponses INT
            )
    
    DECLARE @examQuotaMarked TABLE(
                GroupId INT
               ,AssignedQuota INT
               ,ParkedResponse INT
               ,CloseToSuspension INT
               ,SuspendedMarker INT
               ,NumMarked INT
            )
    
    DECLARE @result TABLE (
                groupId INT
               ,numMarked INT
               ,numResponses INT
               ,assignedQuota INT
               ,diff INT
               ,awaiting INT
               ,uniqueResponses INT
               ,unassignedQuota INT
               ,NumParkedResponses INT
            )	
    
    DECLARE @examIds TABLE(examId INT)	
    DECLARE @examId INT
    
    INSERT @examIds
    SELECT ID
    FROM   Exams
    WHERE  ID>0
    
    WHILE EXISTS ( SELECT TOP 1 * FROM   @examIds )
    BEGIN
        SELECT TOP 1 @examId = examId
        FROM   @examIds
        
        DELETE 
        FROM   @examIds
        WHERE  examId = @examId
        
        DELETE FROM @examMarked
        DElETE FROM @examQuotaMarked						
        
        INSERT @examMarked
        EXEC [dbo].[sm_QUOTAMANAGEMENTSERVICE_getGroupsForExamWithNumResponses_sp] @examId
        
        INSERT @examQuotaMarked
        EXEC [dbo].[sm_QUOTAMANAGEMENTSERVICE_getGroupsForExamWithQuotaInformation_sp] @examId
    
    
    DECLARE @groupIds TABLE(groupId INT)	
    DECLARE @groupId INT
    
    DELETE FROM @groupIds
    
    INSERT @groupIds
    SELECT VM.groupId
    FROM   @examMarked VM 
    UNION
    SELECT VQM.groupID
    FROM   @examQuotaMarked VQM  
    
    WHILE EXISTS ( SELECT TOP 1 * FROM @groupIds )
    BEGIN
        SELECT TOP 1 @groupId = groupId
        FROM   @groupIds
        
        DELETE 
        FROM   @groupIds
        WHERE  groupId = @groupId	
        
        INSERT @quotaService
        EXEC [dbo].[sm_QUOTAMANAGEMENTSERVICE_getNumUniqueAndAssignedResponsesForGroup_sp] @groupId, @examId
              
        INSERT @result
        SELECT VM.GroupID
              ,VM.NumMarkedResponses
              ,VM.NumResponses
              ,VQM.AssignedQuota
              ,(
                   CASE 
                        WHEN VQM.AssignedQuota- VQM.NumMarked<0 THEN VQM.AssignedQuota
                        ELSE VQM.AssignedQuota- VQM.NumMarked
                   END
               )
              ,VM.NumAwaitingReview
              ,(
                   SELECT QS.NumUniqueResponses
                   FROM   @quotaService QS
               )
              ,(
                   SELECT QS.UnassignedQuota
                   FROM   @quotaService QS
               )
              ,VM.NumParkedResponses
        FROM   @examMarked VM
               LEFT JOIN @examQuotaMarked VQM
                    ON  VQM.GroupId = VM.GroupID
        WHERE  VM.GroupID = @groupId
        
        DELETE FROM @quotaService
    END
    
    END
    
    SELECT *
    FROM   @result R
    WHERE  R.assignedQuota>-1
           AND R.unassignedQuota>-1
           AND (
                   R.uniqueResponses - R.numMarked - R.diff - R.awaiting - R.NumParkedResponses
               )>0
           AND (
                   R.uniqueResponses - R.numMarked- R.diff - R.awaiting - R.NumParkedResponses
               )<>R.unassignedQuota
END

GO
/****** Object:  StoredProcedure [dbo].[sm_ESCALATIONS_FILTER_GetCentres_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sm_ESCALATIONS_FILTER_GetCentres_sp]
-- Add the parameters for the stored procedure here
	@userId INT,
	@maxNum INT
AS
BEGIN

    SET NOCOUNT ON;
    
    SELECT	DISTINCT TOP(@maxNum) 
			CEV.CentreName    AS 'Centre'
    FROM	sm_EscalatedUGRs_fn(@userId) EUV
			INNER JOIN CandidateGroupResponses CGR
			ON CGR.UniqueGroupResponseID = EUV.ID
			INNER JOIN CandidateExamVersions CEV
			ON CEV.ID = CGR.CandidateExamVersionID
    
END
GO
/****** Object:  StoredProcedure [dbo].[sm_ESCALATIONS_FILTER_GetDateEscalated_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sm_ESCALATIONS_FILTER_GetDateEscalated_sp]
-- Add the parameters for the stored procedure here
	@userId INT
AS
BEGIN

    SET NOCOUNT ON;
    
    SELECT	MIN(EUV.DateEscalated)   AS 'MinDateEscalated'
           ,MAX(EUV.DateEscalated)   AS 'MaxDateEscalated'
    FROM	sm_EscalatedUGRs_fn(@userId) EUV
END
GO
/****** Object:  StoredProcedure [dbo].[sm_ESCALATIONS_FILTER_GetExaminers_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sm_ESCALATIONS_FILTER_GetExaminers_sp] 

-- Add the parameters for the stored procedure here
	@userId INT,
	@maxNum INT
AS
BEGIN

    SET NOCOUNT ON;
    
    SELECT DISTINCT TOP(@maxNum) 
           EUV.Examiner    AS 'Examiner'
          ,EUV.ExaminerId  AS 'ExaminerId'
    FROM   sm_EscalatedUGRs_fn(@userId) EUV
END
GO
/****** Object:  StoredProcedure [dbo].[sm_ESCALATIONS_FILTER_GetExams_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sm_ESCALATIONS_FILTER_GetExams_sp] 

-- Add the parameters for the stored procedure here
	@userId INT,
	@maxNum INT
AS
BEGIN

    SET NOCOUNT ON;
    
    SELECT DISTINCT TOP(@maxNum) 
           EUV.Exam    AS 'Exam'
          ,EUV.ExamID  AS 'ExamID'
    FROM   sm_EscalatedUGRs_fn(@userId) EUV
END
GO
/****** Object:  StoredProcedure [dbo].[sm_ESCALATIONS_FILTER_GetExamVersions_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sm_ESCALATIONS_FILTER_GetExamVersions_sp] 

-- Add the parameters for the stored procedure here
	@userId INT,
	@maxNum INT
AS
BEGIN

    SET NOCOUNT ON;
    
    SELECT DISTINCT TOP(@maxNum) 
           EUV.ExamVersion    AS 'ExamVersion'
          ,EUV.ExamVersionID  AS 'ExamVersionID'
    FROM   sm_EscalatedUGRs_fn(@userId) EUV
END
GO
/****** Object:  StoredProcedure [dbo].[sm_ESCALATIONS_FILTER_GetIssues_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sm_ESCALATIONS_FILTER_GetIssues_sp]
-- Add the parameters for the stored procedure here

AS
BEGIN

    SET NOCOUNT ON;
    
    SELECT Name    AS 'Issue'
          ,ID	   AS 'ReasonId'
    FROM   ParkedReasonsLookup
    
END
GO
/****** Object:  StoredProcedure [dbo].[sm_ESCALATIONS_FILTER_GetItems_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sm_ESCALATIONS_FILTER_GetItems_sp]
-- Add the parameters for the stored procedure here
	@userId INT,
	@maxNum INT
AS
BEGIN

    SET NOCOUNT ON;
    
    SELECT DISTINCT TOP(@maxNum) 
           EUV.ItemName    AS 'ItemName'
          ,EUV.ItemID  AS 'ItemID'
    FROM   sm_EscalatedUGRs_fn(@userId) EUV
END
GO
/****** Object:  StoredProcedure [dbo].[sm_ESCALATIONS_FILTER_GetQualifications_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sm_ESCALATIONS_FILTER_GetQualifications_sp] 

-- Add the parameters for the stored procedure here
	@userId INT,
	@maxNum INT
AS
BEGIN

    SET NOCOUNT ON;
    
    SELECT DISTINCT TOP(@maxNum) 
           EUV.Qualification    AS 'Qualification'
          ,EUV.QualificationID  AS 'QualificationID'
    FROM   sm_EscalatedUGRs_fn(@userId) EUV
END
GO
/****** Object:  StoredProcedure [dbo].[sm_ESCALATIONS_GetEmptyUniqueResponse_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [dbo].[sm_ESCALATIONS_GetEmptyUniqueResponse_sp]
-- Add the parameters for the stored procedure here
	@myGroupResponseID BIGINT
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;
    
    -- Unique responses of group response
    DECLARE @uniqueItemResponses TABLE (
                UniqueResponseID BIGINT
               ,ItemId INT
               ,MarkedMetadata XML
               ,IsFilled BIT NOT NULL DEFAULT 0
            )
    
    INSERT INTO @uniqueItemResponses
      (
        UniqueResponseID, ItemId
      )
    SELECT ID, itemId
    FROM   dbo.UniqueResponses
    WHERE  ID IN (SELECT UniqueResponseId
                  FROM   dbo.UniqueGroupResponseLinks
                  WHERE  UniqueGroupResponseID = @myGroupResponseID) 
    
    -- Get empty marked metadata for each UniqueResponse in Group response------------------
    -- @currentId is used for loop to get MarkedMetadata
    DECLARE @myMarkedMetadata     XML
    
    DECLARE @currentId            BIGINT=(
                SELECT TOP 1 UniqueResponseID
                FROM   @uniqueItemResponses
            )
    
    WHILE @currentId IS NOT NULL
    BEGIN
        EXEC dbo.sm_SHARED_removeMarksFromMarkedMetadata_sp 
             @uniqueResponseId=@currentId
            ,@updateTable=0
            ,@newMarkedMetadata=@myMarkedMetadata OUTPUT
        
        UPDATE @uniqueItemResponses
        SET    MarkedMetadata = @myMarkedMetadata
              ,IsFilled = 1
        WHERE  UniqueResponseID = @currentId
        
        SET @currentId = (
                SELECT TOP 1 UniqueResponseID
                FROM   @uniqueItemResponses
                WHERE  IsFilled = 0
            )
    END
    
    DECLARE @myGroupID INT=(
                SELECT ugr.GroupDefinitionID
                FROM   dbo.UniqueGroupResponses ugr
                WHERE  Id = @myGroupResponseID
            )
    
    -- Select Unique Group Response level data --------------------------------------------
    SELECT ID                    AS 'ID'
          ,GroupDefinitionID     AS 'GroupId'
    FROM   dbo.UniqueGroupResponses
    WHERE  ID = @myGroupResponseID
    
    -- Select Unique Response level data ---------------------------------------------
    SELECT GUR.MarkedMetadata  AS 'MarkedMetadata'
          ,GUR.UniqueResponseID AS 'UniqueResponseID'
    FROM   @uniqueItemResponses GUR
           INNER JOIN dbo.GroupDefinitionItems GDI ON  GDI.ItemID = GUR.itemId
    WHERE  GDI.ViewOnly = 0 AND GDI.GroupID = @myGroupID
END
GO
/****** Object:  StoredProcedure [dbo].[sm_ESCALATIONS_getGroupsForViewScript]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sm_ESCALATIONS_getGroupsForViewScript]
	@uniqueGroupResponseId BIGINT,
	@userId INT
AS
BEGIN
    SET NOCOUNT ON;
    
    IF (
           (
               SELECT COUNT(*)
               FROM   dbo.CandidateGroupResponses CGR
               WHERE  CGR.UniqueGroupResponseID = @uniqueGroupResponseId
           )=1
       )
    BEGIN
        DECLARE @scriptId INT=(
                    SELECT TOP 1 CGR.CandidateExamVersionID
                    FROM   dbo.CandidateGroupResponses CGR
                    WHERE  CGR.UniqueGroupResponseID = @uniqueGroupResponseId
                ) 
                            
       ;WITH T AS ( 
        SELECT GD.ID                  AS 'GroupID'
              ,GD.Name                AS 'GroupName'
              ,(
                   CASE 
                        WHEN (
                                 (
                                     SELECT COUNT(*)
                                     FROM   dbo.GroupDefinitionItems GDI
                                     WHERE  GDI.GroupID = GD.ID
                                 )=1
                             ) THEN (
                                 SELECT I.MarkingType
                                 FROM   dbo.GroupDefinitionItems GDI 
                                 INNER JOIN dbo.Items I ON  I.ID = GDI.ItemID
                                 WHERE  GDI.GroupID = GD.ID
                             )
                        ELSE 1
                   END
               )                      AS 'MarkingType'
               ,(
                   SELECT TOP 1 GDI.ItemID
                    FROM   dbo.GroupDefinitionItems GDI                                        
                    WHERE  GDI.GroupID = GD.ID
				)                      AS 'ItemId'
				,(
                   
                    SELECT COUNT(*)
                    FROM   dbo.GroupDefinitionItems GDI
                    WHERE  GDI.GroupID = GD.ID
				)                      AS 'ItemsCount'
              ,(
                   CASE 
                        WHEN (
                                 (
                                     SELECT COUNT(*)
                                     FROM dbo.UniqueGroupResponseParkedReasonCrossRef  UGRPRC
                                     WHERE  UGRPRC.UniqueGroupResponseId = UGR.ID
                                 )>0 
                                 OR (UGR.CI_Review = 0 AND UGR.CI = 1) 
                             ) THEN (
                                 SELECT TOP 1 CASE WHEN (AM.MarkingMethodId = 11 AND AM.EscalatedWithoutMark = 1) THEN NULL ELSE AM.AssignedMark END
                                 FROM   dbo.AssignedGroupMarks AM
                                 WHERE  AM.UniqueGroupResponseID = UGR.ID
                                 ORDER BY
                                        TIMESTAMP DESC
                             )
                        ELSE (
                                 SELECT UGR.ConfirmedMark
                             )
                   END
               )                      AS 'Mark'
              ,GD.TotalMark           AS 'TotalMark'
              ,UGR.IsEscalated		  AS 'IsEscalated'             
              ,CAST(
                   (
                       CASE 
                            WHEN (
                                     (
                                         SELECT COUNT(*)
                                         FROM   dbo.CandidateGroupResponses CGR
                                         WHERE  CGR.UniqueGroupResponseID = UGR.ID
                                     )>1
                                 ) THEN 1
                            ELSE 0
                       END
                   ) AS BIT
               )                      AS 'IsManyScripts'
              ,UGR.ID                 AS 'UniqueGroupResponseID'
              ,CEV.CandidateSurname   AS 'LastName'
              ,CEV.CandidateForename  AS 'FirstName'
              ,CEV.CandidateRef       AS 'CandidateRef'
              ,E.Name                 AS 'Exam'
              ,CAST((CASE (QM.MarkingSuspended) WHEN 1 THEN 1 ELSE 0 END) AS BIT ) AS 'Suspended'
              ,UGR.CI				  AS 'IsCI'
              ,UGR.CI_Review		  AS 'IsAwaitingReview'
        FROM   dbo.UniqueGroupResponses UGR
               INNER JOIN dbo.GroupDefinitions GD ON  UGR.GroupDefinitionID = GD.ID
               INNER JOIN dbo.CandidateGroupResponses CGR ON  CGR.UniqueGroupResponseID = UGR.ID
               INNER JOIN dbo.CandidateExamVersions cev ON  cev.ID = CGR.CandidateExamVersionID
               INNER JOIN dbo.Exams e ON  e.ID = GD.ExamID
               LEFT JOIN dbo.QuotaManagement QM ON  QM.GroupId = GD.ID AND QM.UserId = @userId			   
        WHERE  CGR.CandidateExamVersionID = @scriptId
               AND GD.ID IN (SELECT GDSCR.GroupDefinitionID
                             FROM   dbo.GroupDefinitionStructureCrossRef GDSCR
                                    INNER JOIN dbo.ExamVersionStructures EVS ON  EVS.ID = GDSCR.ExamVersionStructureID
                             WHERE  GDSCR.Status = 0 -- 0 = Active
                                    AND EVS.StatusID = 0 -- 0 = Released
               )       
       )
	               
	SELECT * FROM T ORDER BY ItemsCount, ItemId               
	    
    END
    ELSE
    BEGIN
        --The Unique Group Response is assigned to more than 1 Script
        return 50013;
    END
END
GO
/****** Object:  StoredProcedure [dbo].[sm_ESCALATIONS_getUniqueGroupResponseInfo_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sm_ESCALATIONS_getUniqueGroupResponseInfo_sp]
	@uniqueGroupResponseId BIGINT
AS
BEGIN
    SET NOCOUNT ON;
    
    SELECT ScriptType
    FROM dbo.UniqueGroupResponses
    WHERE ID = @uniqueGroupResponseId
END
GO
/****** Object:  StoredProcedure [dbo].[sm_ESCALATIONS_GetUniqueResponseToMark_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[sm_ESCALATIONS_GetUniqueResponseToMark_sp]
-- Add the parameters for the stored procedure here
--@myUserID INT,
	@myGroupResponseID INT,
	@myTokenStoreID INT
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;
    
    -- Unique responses of group response
    DECLARE @uniqueItemResponses TABLE (
                UniqueResponseID BIGINT
               ,MarkedMetadata XML
               ,IsFilled BIT NOT NULL DEFAULT 0
            )
    
    -- @currentId is used for loop to get MarkedMetadata
    DECLARE @currentId BIGINT
    
    DECLARE @myMarkedMetadata     XML
    
    DECLARE @myGroupID            INT=(
                SELECT ugr.GroupDefinitionID
                FROM   dbo.UniqueGroupResponses ugr
                WHERE  Id = @myGroupResponseID
            )
    
    DECLARE @isReadonly BIT = 0
        
    -- Set the token ID of the unique group response to check it out, and also set the mark scheme ID
    BEGIN TRANSACTION
    
    UPDATE dbo.UniqueGroupResponses
    SET    TokenId = @myTokenStoreID
    WHERE  dbo.UniqueGroupResponses.ID = (
               SELECT TOP(1) UGR.ID
               FROM   dbo.UniqueGroupResponses UGR WITH (UPDLOCK)
               WHERE  UGR.ID = @myGroupResponseID
                      AND (UGR.TokenId IS NULL OR UGR.TokenId=@myTokenStoreID)
                      AND UGR.ConfirmedMark IS NULL
                      AND UGR.IsEscalated = 0
                      AND UGR.CI = 0
               ORDER BY
                      VIP               DESC
                     ,insertionDate  ASC
           )
    
    -- Check to see if UPDATE happened
    IF @@ROWCOUNT=0
    BEGIN  	
    	SET @isReadonly = 1
        -- this UGR is changed his "status" from unmarked to smthing else or checked out to some user
    END
    
    COMMIT TRANSACTION
    -------------------------------------------------------
    --************************************************
    INSERT INTO @uniqueItemResponses
      (
        UniqueResponseID
      )
    SELECT ID
    FROM   dbo.UniqueResponses
    WHERE  ID IN (SELECT UniqueResponseId
                  FROM   dbo.UniqueGroupResponseLinks
                  WHERE  UniqueGroupResponseID = @myGroupResponseID)
    
    --Update MarkScheme
    UPDATE dbo.UniqueResponses
    SET    markSchemeId = (
               SELECT TOP 1 MS.ID
               FROM   dbo.Items I
                      INNER JOIN dbo.MarkSchemes MS
                           ON  MS.ExternalItemID = I.ExternalItemID
               WHERE  I.ID = dbo.UniqueResponses.itemId
                      AND MS.version = (
                              SELECT MAX(version)
                              FROM   dbo.MarkSchemes MS1
                              WHERE  MS1.ExternalItemID = MS.ExternalItemID
                          )
           )
    WHERE  dbo.UniqueResponses.ID IN (SELECT UniqueResponseId FROM   @uniqueItemResponses)
    
    -- Select Unique Group Response level data --------------------------------------------
    SELECT UGR.ID          AS ID
          ,UGR.ScriptType  AS ScriptType
          ,GD.ID           AS GroupId
          ,GD.Name         AS GroupName
          ,GD.TotalMark    AS TotalMark
          ,@isReadonly	   AS IsReadOnly  
    FROM   dbo.UniqueGroupResponses UGR
           INNER JOIN dbo.GroupDefinitions GD
                ON  GD.ID = UGR.GroupDefinitionID
    WHERE  UGR.ID = @myGroupResponseID
    
    -- Get empty marked metadata for each UniqueResponse in Group response------------------	
    SET @currentId = (
            SELECT TOP 1 UniqueResponseID
            FROM   @uniqueItemResponses
            WHERE  IsFilled = 0
        )
    
    WHILE @currentId IS NOT NULL
    BEGIN
        EXEC dbo.sm_SHARED_removeMarksFromMarkedMetadata_sp 
             @uniqueResponseId=@currentId
            ,@updateTable=0
            ,@newMarkedMetadata=@myMarkedMetadata OUTPUT
        
        UPDATE @uniqueItemResponses
        SET    MarkedMetadata = @myMarkedMetadata
              ,IsFilled = 1
        WHERE  UniqueResponseID = @currentId
        
        SET @currentId = (
                SELECT TOP 1 UniqueResponseID
                FROM   @uniqueItemResponses
                WHERE  IsFilled = 0
            )
    END
    
    -- Select Unique Response level data ---------------------------------------------
    SELECT I.ID                AS ItemId
          ,I.ExternalItemName  AS ItemName
          ,I.ExternalItemID    AS ExternalItemID
          ,I.Version           AS ItemVersion
          ,I.LayoutName        AS LayoutName
          ,I.TotalMark         AS TotalMark
          ,I.MarkingType       AS MarkingType
          ,I.AllowAnnotations  AS AllowAnnotations
          ,GUR.MarkedMetadata  AS MarkedMetadata
          ,UR.id               AS responseID
          ,UR.responseData     AS ResponseData
          ,GDI.ViewOnly        AS ViewOnly
    FROM   dbo.UniqueResponses UR
           INNER JOIN dbo.Items I
                ON  UR.itemId = I.ID
           INNER JOIN @uniqueItemResponses GUR
                ON  GUR.UniqueResponseID = UR.ID
           INNER JOIN dbo.GroupDefinitionItems GDI
                ON  GDI.ItemID = UR.itemId
                    AND GDI.GroupID = @myGroupID
    ORDER BY
           GDI.GroupDefinitionItemOrder

	-- uncheck UGR checked to this user to allow other users mark them in 'ordinary marking flow'
	UPDATE dbo.UniqueGroupResponses
	SET			
		TokenId = NULL
	WHERE ID <> @myGroupResponseID AND TokenId = @myTokenStoreID

END


GO
/****** Object:  StoredProcedure [dbo].[sm_ESCALATIONS_SplitUniqueGroupResponse_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[sm_ESCALATIONS_SplitUniqueGroupResponse_sp]

	@uniqueGroupResponseId BIGINT,
	@scriptId INT
AS
BEGIN

	DECLARE @splittedUgrId BIGINT

	BEGIN TRY
		BEGIN TRAN
			
			-- check that ugr is related to many scripts, otherwise return -1
			DECLARE @countOfScripts INT = 
				(
					SELECT COUNT(*)
					FROM dbo.CandidateGroupResponses
					WHERE UniqueGroupResponseID = @uniqueGroupResponseId
				)	
			IF @countOfScripts = 1
				BEGIN
					SET @splittedUgrId = -1
				END
			ELSE
				BEGIN
			
				-- copy Unique Group Response
				INSERT INTO dbo.UniqueGroupResponses
				(
					GroupDefinitionID,
					CheckSum,
					InsertionDate,
					CI_Review,
					CI,
					VIP,
					ConfirmedMark,
					ParkedAnnotation,
					ParkedDate,
					TokenId,
					ParkedUserID,
					ScriptType,
					Feedback,
					IsEscalated,
					OriginalUniqueGroupResponseId
				)
				SELECT 
					GroupDefinitionID,
					CheckSum, -- the checksum will be updated later in order to use UGR ID which is being now created
					InsertionDate,
					CI_Review,
					CI,
					(
						CASE 
							WHEN
							(
								SELECT COUNT(*)
								FROM dbo.CandidateGroupResponses CGR
								INNER JOIN dbo.CandidateExamVersions CEV
								ON CGR.CandidateExamVersionID = CEV.ID
								WHERE CGR.UniqueGroupResponseID = UGR.ID
								AND CEV.VIP = 1
							) > 0
							THEN 1
							ELSE 0
						END
					) AS VIP, -- if at least one script which related to the UGR has the VIP state => we consider the UGR as VIP
					ConfirmedMark,
					ParkedAnnotation,
					ParkedDate,
					NULL,
					ParkedUserID,
					ScriptType,
					Feedback,
					IsEscalated,
					@uniqueGroupResponseId
				FROM dbo.UniqueGroupResponses UGR
				WHERE ID = @uniqueGroupResponseId
				SET @splittedUgrId = SCOPE_IDENTITY()
			

				-- update splitted UGR to add new unique check sum
				UPDATE dbo.UniqueGroupResponses
				SET CheckSum = HASHBYTES('sha1', Checksum + CAST(ID AS nvarchar(20)))
				WHERE ID = @splittedUgrId
				
					
				-- update CandidateGroupResponses. We have to change the UGR ID for current Script
				UPDATE dbo.CandidateGroupResponses
				SET UniqueGroupResponseID = @splittedUgrId
				WHERE CandidateExamVersionID = @scriptId
				AND UniqueGroupResponseID = @uniqueGroupResponseId
				
				
				-- copy UniqueGroupResponseParkedReasonCrossRef
				INSERT INTO dbo.UniqueGroupResponseParkedReasonCrossRef
				(
					ParkedReasonId,
					UniqueGroupResponseId
				)
				SELECT 
					ParkedReasonId,
					@splittedUgrId
				FROM dbo.UniqueGroupResponseParkedReasonCrossRef
				WHERE UniqueGroupResponseId = @uniqueGroupResponseId
				
				
				-- Update QM table in case of splitting already parked unique group response
				IF EXISTS(SELECT *
						  FROM dbo.UniqueGroupResponses ugr 
				          WHERE ugr.ID = @uniqueGroupResponseId AND ugr.IsEscalated = 1) 
				BEGIN					
					DECLARE @groupId INT
					DECLARE @parkedUserId INT
										
					SELECT 
						@groupId = ugr.GroupDefinitionID,
						@parkedUserId = ugr.ParkedUserID
					FROM dbo.UniqueGroupResponses ugr 
					WHERE ugr.ID = @uniqueGroupResponseId
					
					IF EXISTS( SELECT * 
					           FROM dbo.QuotaManagement qm 
					           WHERE qm.GroupId = @groupId	AND qm.UserId = @parkedUserId )
						BEGIN
							 UPDATE dbo.QuotaManagement
							 SET
							 	NumItemsParked = NumItemsParked + 1
							WHERE GroupId = @groupId AND UserId = @parkedUserId
						END
				END
			
			
				-- copy Assigned Group Marks
				DECLARE @splittedAgms TABLE
				(
					OriginalAgmId BIGINT,
					SplittedAgmId BIGINT
				)
				MERGE dbo.AssignedGroupMarks
				USING 
				(
					SELECT 
						ID,
						UserId,
						@splittedUgrId AS UniqueGroupResponseId,
						Timestamp,
						AssignedMark,
						IsConfirmedMark,
						UserName,
						FullName,
						Surname,
						Disregarded,
						MarkingDeviation,
						MarkingMethodId,
						IsEscalated,
						GroupDefinitionID,
						IsActualMarking,
						ParkedAnnotation,
						IsReportableCI
					FROM dbo.AssignedGroupMarks AGM
					WHERE AGM.UniqueGroupResponseId = @uniqueGroupResponseId
				) AS I
				ON 0 = 1
				WHEN NOT MATCHED THEN
				INSERT
				(
					UserId,
					UniqueGroupResponseId,
					Timestamp,
					AssignedMark,
					IsConfirmedMark,
					UserName,
					FullName,
					Surname,
					Disregarded,
					MarkingDeviation,
					MarkingMethodId,
					IsEscalated,
					GroupDefinitionID,
					IsActualMarking,
					ParkedAnnotation,
					IsReportableCI
				) 
				VALUES
				(
					I.UserId,
					I.UniqueGroupResponseId,
					I.Timestamp,
					I.AssignedMark,
					I.IsConfirmedMark,
					I.UserName,
					I.FullName,
					I.Surname,
					I.Disregarded,
					I.MarkingDeviation,
					I.MarkingMethodId,
					I.IsEscalated,
					I.GroupDefinitionID,
					I.IsActualMarking,
					I.ParkedAnnotation,
					I.IsReportableCI
				)
				OUTPUT I.ID, INSERTED.ID 
				INTO @splittedAgms;
				
				
				-- copy AGM Parked Reason Cross Ref
				INSERT INTO dbo.AssignedGroupMarksParkedReasonCrossRef
				(
					AssignedGroupMarkId,
					ParkedReasonId
				)
				SELECT 
					SA.SplittedAgmId,
					ParkedReasonId
				FROM dbo.AssignedGroupMarksParkedReasonCrossRef AGMPRCR
				INNER JOIN @splittedAgms SA
				ON SA.OriginalAgmId = AGMPRCR.AssignedGroupMarkId
				
				
				-- copy Unique Responses
				-- we have to copy all unique responses of the ugr but view only
				DECLARE @splittedURs TABLE
				(
					OriginalUrId BIGINT,
					SplittedUrId BIGINT
				)
				MERGE dbo.UniqueResponses
				USING 
				(
					SELECT 
						UR.id,
						UR.itemId,
						UR.responseData,
						UR.checksum,
						UR.confirmedMark,
						UR.insertionDate,
						UR.markSchemeId,
						UR.latestMarkSchemeIdAtTimeOfImport,
						UR.markedMetadataResponse
					FROM dbo.UniqueResponses UR
					INNER JOIN dbo.UniqueGroupResponseLinks UGRL
					ON UR.ID = UGRL.uniqueResponseId
					INNER JOIN dbo.GroupDefinitionItems gdi
					ON gdi.itemId = UR.itemId
					INNER JOIN dbo.UniqueGroupResponses ugr
					ON ugr.ID = UGRL.UniqueGroupResponseID
					WHERE UGRL.UniqueGroupResponseId = @uniqueGroupResponseId
					AND ugr.GroupDefinitionID = gdi.GroupID -- GroupDefinitionItems only for group of current ugr
					AND gdi.ViewOnly = 0 -- if a group contains an item as ViewOnly we shouldn't split unique responses of the item\
					AND gdi.GroupID IN (
						SELECT GDSCR.GroupDefinitionID
						FROM   dbo.GroupDefinitionStructureCrossRef GDSCR
							   INNER JOIN dbo.ExamVersionStructures EVS ON  EVS.ID = GDSCR.ExamVersionStructureID
						WHERE  GDSCR.Status = 0 AND EVS.StatusID = 0
					)
				) AS I
				ON 0 = 1
				WHEN NOT MATCHED THEN
				INSERT
				(
					itemId,
					responseData,
					checksum,
					confirmedMark,
					insertionDate,
					markSchemeId,
					latestMarkSchemeIdAtTimeOfImport,
					markedMetadataResponse
				) 
				VALUES
				(
					I.itemId,
					I.responseData,
					I.checksum,
					I.confirmedMark,
					I.insertionDate,
					I.markSchemeId,
					I.latestMarkSchemeIdAtTimeOfImport,
					I.markedMetadataResponse
				)
				OUTPUT I.ID, INSERTED.ID 
				INTO @splittedURs;
				
				
				-- update UR's checksums
				UPDATE UR
				SET checksum = HASHBYTES('sha1', checksum + CAST(UR.ID AS nvarchar(20)))
				FROM dbo.UniqueResponses UR
				INNER JOIN @splittedURs SUR
				ON UR.id = SUR.SplittedUrId
				
				-- copy Assigned Items Marks
				INSERT INTO dbo.AssignedItemMarks
				(
					GroupMarkId,
					UniqueResponseId,
					AnnotationData,
					AssignedMark,
					MarkingDeviation,
					MarkedMetadataResponse
				)
				SELECT
					SA.SplittedAgmId,
					SUR.SplittedUrId,
					AIM.AnnotationData,
					AIM.AssignedMark,
					AIM.MarkingDeviation,
					AIM.MarkedMetadataResponse
				FROM dbo.AssignedItemMarks AIM
				INNER JOIN @splittedURs SUR
				ON SUR.OriginalUrId = AIM.UniqueResponseId
				INNER JOIN @splittedAgms SA
				ON SA.OriginalAgmId = AIM.GroupMarkId
				
				
				-- copy Unique Group Responses Links
				INSERT INTO dbo.UniqueGroupResponseLinks
				(
					UniqueGroupResponseID,
					UniqueResponseId,
					confirmedMark,
					markedMetadataResponse
				)
				SELECT
					@splittedUgrId,
					SUR.SplittedUrId,
					UGRL.confirmedMark,
					UGRL.markedMetadataResponse
				FROM dbo.UniqueGroupResponseLinks UGRL
				INNER JOIN @splittedURs SUR
				ON SUR.OriginalUrId = UGRL.UniqueResponseId
				WHERE UGRL.UniqueGroupResponseID = @uniqueGroupResponseId
				
				-- update CandidateResponses. We have to change the UR ID for current Script
				UPDATE CR
				SET CR.UniqueResponseID = SU.SplittedUrId
				FROM dbo.CandidateResponses CR
				INNER JOIN @splittedURs SU
				ON CR.UniqueResponseID = SU.OriginalUrId
				AND CR.CandidateExamVersionID = @scriptId
 				
 				-- connect view only unique responses from original ugr to splitted ugr
 				INSERT INTO dbo.UniqueGroupResponseLinks
 				(
 					UniqueGroupResponseID,
 					UniqueResponseId,
 					confirmedMark,
 					markedMetadataResponse
 				)
 				SELECT 
 					@splittedUgrId,
 					ugrl.UniqueResponseId,
 					ugrl.confirmedMark,
 					ugrl.markedMetadataResponse
 				FROM dbo.UniqueGroupResponseLinks ugrl
 				INNER JOIN dbo.UniqueResponses ur
 				ON ur.id = ugrl.UniqueResponseId
 				INNER JOIN dbo.GroupDefinitionItems gdi
				ON gdi.itemId = UR.itemId
 				INNER JOIN dbo.UniqueGroupResponses ugr
				ON ugr.ID = UGRL.UniqueGroupResponseID
				WHERE UGRL.UniqueGroupResponseId = @uniqueGroupResponseId
				AND ugr.GroupDefinitionID = gdi.GroupID -- GroupDefinitionItems only for group of current ugr
				AND gdi.ViewOnly = 1 -- if a group contains an item as ViewOnly we shouldn't split unique responses of the item\
				AND gdi.GroupID IN (
					SELECT GDSCR.GroupDefinitionID
					FROM   dbo.GroupDefinitionStructureCrossRef GDSCR
						   INNER JOIN dbo.ExamVersionStructures EVS ON  EVS.ID = GDSCR.ExamVersionStructureID
					WHERE  GDSCR.Status = 0 AND EVS.StatusID = 0
				)
 				
			END
		COMMIT TRAN	
	END TRY
	BEGIN CATCH
		ROLLBACK TRAN
		DECLARE @myErrorNum INT = ERROR_NUMBER()
		DECLARE @myMessage NVARCHAR(MAX) = ERROR_MESSAGE()
		DECLARE @myFullMessage NVARCHAR(MAX) = CONVERT(NVARCHAR(10), @myErrorNum)
			+ ':' + @myMessage
		RAISERROR (@myFullMessage, 16, 1)
	END CATCH
	
	SELECT @splittedUgrId
END

GO
/****** Object:  StoredProcedure [dbo].[sm_GETCONTENTSERVICE_GetExamsForItem_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sm_GETCONTENTSERVICE_GetExamsForItem_sp]
	-- Add the parameters for the stored procedure here
	@externalId	nvarchar(50),
	@version int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	SELECT ExamID
	FROM dbo.Items
	WHERE ExternalItemID = @externalId and [Version] = @version
  
END
GO
/****** Object:  StoredProcedure [dbo].[sm_GETCONTENTSERVICE_getItemContent_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Tom Gomersall
-- Create date: 25/02/11
-- Description:	Gets the content for an item version
-- =============================================
CREATE PROCEDURE [dbo].[sm_GETCONTENTSERVICE_getItemContent_sp]
	-- Add the parameters for the stored procedure here
	 @externalID	nvarchar(50),
	 @itemVersion	int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT DISTINCT	[ExternalContent]
	FROM [Items]
	WHERE [Version] = @itemVersion AND [ExternalItemID] = @externalID 
	

END
GO
/****** Object:  StoredProcedure [dbo].[sm_GETMARKSCHEMESERVICE_getExamForItem_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sm_GETMARKSCHEMESERVICE_getExamForItem_sp]
	-- Add the parameters for the stored procedure here
	 @itemID	INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	SELECT ExamID
	FROM Items
	WHERE ID = @itemID

END
GO
/****** Object:  StoredProcedure [dbo].[sm_GETMARKSCHEMESERVICE_getExamForUniqueResponse_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sm_GETMARKSCHEMESERVICE_getExamForUniqueResponse_sp]
	-- Add the parameters for the stored procedure here
	 @uniqueResponseID	INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	SELECT i.ExamID
	FROM dbo.Items i
	INNER JOIN dbo.UniqueResponses ur
	ON ur.itemId = i.ID
	WHERE ur.ID = @uniqueResponseID

END
GO
/****** Object:  StoredProcedure [dbo].[sm_GETMARKSCHEMESERVICE_getMarkSchemeForItem_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================    
-- Author:  Tom Gomersall    
-- Create date: 25/02/11    
-- Description: Gets the mark scheme for an item.    
-- =============================================    
CREATE PROCEDURE [dbo].[sm_GETMARKSCHEMESERVICE_getMarkSchemeForItem_sp]    
 -- Add the parameters for the stored procedure here    
  @itemID INT    
AS    
BEGIN    
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements.    
 SET NOCOUNT ON;    
     
	SELECT TOP 1 
		markSchemeBlOb as Content, 
		Extension as Extension, 
		ISNULL(externalUrl, '') as ExternalUrl
	FROM MarkSchemes
	WHERE ExternalItemId = (
		SELECT ExternalItemID
		FROM Items
		WHERE ID = @itemID
	)
	ORDER BY [version] DESC
    
END
GO
/****** Object:  StoredProcedure [dbo].[sm_GETMARKSCHEMESERVICE_getMarkSchemeForUniqueResponse_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sm_GETMARKSCHEMESERVICE_getMarkSchemeForUniqueResponse_sp]
	-- Add the parameters for the stored procedure here
	 @uniqueResponseId	BIGINT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	DECLARE @myMarkSchemeID		INT
	DECLARE @myConfirmedMark	DECIMAL(9, 5)
	DECLARE @myItemID			INT
	
	SELECT 
		@myMarkSchemeID		= UniqueResponses.markSchemeId
		,@myConfirmedMark	= UniqueResponses.confirmedMark
		,@myItemID			= UniqueResponses.itemId
	FROM UniqueResponses
	WHERE ID = @uniqueResponseId
	
	IF(@myConfirmedMark IS NULL)
		BEGIN
			--The response has been marked, so return the mark scheme that was used when it was marked.
			SELECT 
				markSchemeBlOb as Content, 
				Extension as Extension, 
				ISNULL(externalUrl, '') as ExternalUrl
			FROM MarkSchemes
			WHERE ID = @myMarkSchemeID
		END
	ELSE
		BEGIN
			--The response has not been marked, so return the latest mark scheme.
			DECLARE @myExternalItemID	NCHAR(20)
			--Get the external item ID of the item.
			SELECT @myExternalItemID = Items.ExternalItemID
			FROM Items
			WHERE Items.ID = @myItemID
			
			--Select the latest mark scheme.
			SELECT TOP 1 
				markSchemeBlOb as Content, 
				Extension as Extension, 
				ISNULL(externalUrl, '') as ExternalUrl
			FROM MarkSchemes
			WHERE MarkSchemes.ExternalItemID = @myExternalItemID
			ORDER BY MarkSchemes.[version] DESC
			
		END
END

GO
/****** Object:  StoredProcedure [dbo].[sm_GETUPLOADEDDOCUMENTSERVICE_DeleteFile_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Maxim Erachin
-- Create date: 07.12.2015
-- Description:	Deletes uploaded file
-- =============================================
CREATE PROCEDURE [dbo].[sm_GETUPLOADEDDOCUMENTSERVICE_DeleteFile_sp]
	@uniqueResponseDocumentId int
AS
BEGIN
	SET NOCOUNT ON;

	DELETE FROM [dbo].[UniqueResponseDocuments] 
	WHERE ID = @uniqueResponseDocumentId;

END

GO
/****** Object:  StoredProcedure [dbo].[sm_GETUPLOADEDDOCUMENTSERVICE_GetUniqueResponse_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Dmitry Shiryaev>
-- Create date: <07.12.2015>
-- Description:	<Get response data with unique response id>
-- =============================================
CREATE PROCEDURE [dbo].[sm_GETUPLOADEDDOCUMENTSERVICE_GetUniqueResponse_sp]

	@uniqueResponseId INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT 
		UR.id, 
		UR.responseData, 
		UR.itemId,
		I.ExternalItemID,
		I.Version
	FROM [dbo].[UniqueResponses] UR
	JOIN [dbo].[Items] I On I.ID = UR.itemId
	WHERE UR.Id = @uniqueResponseId

END

GO
/****** Object:  StoredProcedure [dbo].[sm_GETUPLOADEDDOCUMENTSERVICE_getUploadedDocumentByName_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================    
-- Author:		Tom Gomersall    
-- Date:		18/11/11    
-- Description: Gets an uploaded document.    
-- Modifier:	Vitaly Bevzik
-- Date:		22/10/13    
-- Description: Changed parameters from  @docID to @docName and @uniqueResponseId
-- =============================================    
CREATE PROCEDURE [dbo].[sm_GETUPLOADEDDOCUMENTSERVICE_getUploadedDocumentByName_sp]    
 -- Add the parameters for the stored procedure here    
  @uniqueResponseId BIGINT,
  @docName NVARCHAR(100)
AS    
BEGIN    
	-- SET NOCOUNT ON added to prevent extra result sets from    
	-- interfering with SELECT statements.    
	SET NOCOUNT ON;      
	  		
	SELECT TOP(1) 
		URD.Blob			AS [Document],
		URD.FileName		AS [DocName],
		I.ExamID			AS [ExamID]
	FROM dbo.UniqueResponseDocuments URD
	INNER JOIN UniqueResponses UR ON UR.id = URD.UniqueResponseID
	INNER JOIN Items I ON I.ID = UR.itemId	
	WHERE UR.id = @uniqueResponseId	AND URD.Name = @docName
	ORDER BY URD.ExternalDocID DESC

END
GO
/****** Object:  StoredProcedure [dbo].[sm_GETUPLOADEDDOCUMENTSERVICE_GetUploadedFile_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sm_GETUPLOADEDDOCUMENTSERVICE_GetUploadedFile_sp]
	@uniqueResponseDocumentId INT
AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		URD.Id,
		UFD.ReasonText,
		UFD.ReasonTypeID,
		UFD.UserType,
		U.Username,
		UFD.UploadedByUserID,
		URD.FileName,
		URD.Blob,
		URD.UploadedDate
	FROM [dbo].[UniqueResponseDocuments] URD
		LEFT JOIN  [dbo].[UploadedFilesDetails] UFD ON  UFD.UniqueResponseDocumentID = URD.ID
		LEFT JOIN [dbo].[Users] U ON U.Id = UFD.UploadedByUserID
	WHERE URD.ID =  @uniqueResponseDocumentId
END

GO
/****** Object:  StoredProcedure [dbo].[sm_GETUPLOADEDDOCUMENTSERVICE_GetUploadedFiles_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Erachin Maxim>
-- Create date: <04.12.2015>
-- Description:	<Returns details of uploaded files by unique response id>
-- =============================================
CREATE PROCEDURE [dbo].[sm_GETUPLOADEDDOCUMENTSERVICE_GetUploadedFiles_sp]
	@uniqueResponseId int
AS
BEGIN
	SET NOCOUNT ON; 

	SELECT
		URD.Id,
		UFD.ReasonText,
		UFD.ReasonTypeID,
		UFD.UserType,
		UFD.UploadedByUserID,
		U.Forename + ' ' + U.Surname AS Username,
		URD.FileName,
		URD.UploadedDate
	FROM [dbo].[UniqueResponseDocuments] URD
		LEFT JOIN [dbo].[UploadedFilesDetails] UFD ON  UFD.UniqueResponseDocumentID = URD.ID
		LEFT JOIN [dbo].[UniqueResponses] UR ON UR.id = URD.UniqueResponseID
		LEFT JOIN [dbo].[Users] U ON U.Id = UFD.UploadedByUserID
	WHERE UR.id =  @uniqueResponseId

END

GO
/****** Object:  StoredProcedure [dbo].[sm_GETUPLOADEDDOCUMENTSERVICE_UploadFile_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Dmitry Shiryaev>
-- Create date: <07.12.2015>
-- Description:	<Uploads file with unique response id>
-- =============================================
CREATE PROCEDURE [dbo].[sm_GETUPLOADEDDOCUMENTSERVICE_UploadFile_sp]

	@uniqueResponseId INT,
	@name NVARCHAR(200),
	@uploadedDate DATETIME,
	@content VARBINARY(MAX),
	@userType INT,
	@userId INT,
	@reasonType INT,
	@reasonText NVARCHAR(1000)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	BEGIN TRAN

	INSERT INTO [dbo].[UniqueResponseDocuments] (
		Blob, 
		BlobType, 
		UploadedDate, 
		FileName, 
		UniqueResponseID)
	VALUES (
		@content, 
		'Image', 
		@uploadedDate, 
		@name, 
		@uniqueResponseId)

	DECLARE @urd_id INT;
	SET @urd_id = (SELECT @@IDENTITY)

	INSERT INTO [dbo].[UploadedFilesDetails] (
		UserType, 
		UploadedByUserID, 
		ReasonTypeID, 
		ReasonText, 
		UniqueResponseDocumentID)
	VALUES (
		@userType, 
		@userId, 
		@reasonType, 
		@reasonText, 
		@urd_id)

	COMMIT TRAN
END

GO
/****** Object:  StoredProcedure [dbo].[sm_INTEGRATIONSERVICE_AddAutoAssignedMarks_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Umair Rafique
-- Create date: 21/06/2011
-- Description:	sproc to add the record to Assigned marks table for computer and smart marking. 
-- Modified:	Tom Gomersall
-- Date:		30/09/2011
-- Description:	Changed so that the procedure is called for every response that is being imported, but it now checks the marking type of the
--				item before deciding whether to assign a mark, and which marking method to use.

-- Modified:	George Chernov
-- Date:		30/09/2011
-- Description:	Add assigned mark for UniqueGroupResponses and for UniqueResponses
-- =============================================
CREATE PROCEDURE [dbo].[sm_INTEGRATIONSERVICE_AddAutoAssignedMarks_sp]
	@UserId INT,
	@UniqueGroupResponseId BIGINT,
	@Timestamp DATETIME,
	@MarkingDeviation DECIMAL(18 ,10)=0,
	@CandidateMarks [CandidateMarksType] READONLY
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;
    
    -- item is considered appropriate if it has markingtype = automatic or (markingtype = smart and the candidate has full marks)
    DECLARE @AppropriateUniqueResponses TABLE (UniqueResponseId INT)
    
    INSERT INTO @AppropriateUniqueResponses
      (
        UniqueResponseId
      )
    SELECT UR.ID AS UniqueResponseId
    FROM   dbo.UniqueResponses UR
    INNER JOIN dbo.Items I ON  UR.itemId = I.ID
    INNER JOIN @CandidateMarks CM ON  CM.UniqueResponseId = UR.id
    INNER JOIN dbo.UniqueGroupResponseLinks UGRL ON  UGRL.UniqueResponseId = UR.id
            AND UGRL.UniqueGroupResponseID = @UniqueGroupResponseId
    INNER JOIN dbo.UniqueGroupResponses ugr ON  ugr.id = ugrl.UniqueGroupResponseID
    INNER JOIN dbo.GroupDefinitions gd ON  gd.id = ugr.GroupDefinitionID
    WHERE  (
               I.MarkingType=0
               OR (I.MarkingType=3 AND I.TotalMark=CM.Mark)
           )
           AND gd.IsAutomaticGroup = 1
           AND EXISTS (
                   SELECT *
                   FROM   dbo.GroupDefinitionStructureCrossRef CR
				   INNER JOIN dbo.ExamVersionStructures EVS ON  EVS.ID = CR.ExamVersionStructureID
                   WHERE  CR.Status = 0 -- 'Active'
                          AND CR.GroupDefinitionID = gd.ID
                          AND EVS.StatusID = 0
               )
    
    IF (
           (
               SELECT COUNT(*)
               FROM   @AppropriateUniqueResponses
           )>0
       )
    BEGIN
        DECLARE @SumOfAssignedMarks DECIMAL=(
                    SELECT SUM(CM.Mark)
                    FROM   dbo.UniqueResponses UR
                    INNER JOIN dbo.UniqueGroupResponseLinks UGRL ON  UGRL.UniqueResponseId = UR.id
                    INNER JOIN @CandidateMarks CM ON  CM.UniqueResponseId = UR.id
                            AND UGRL.UniqueGroupResponseID = @UniqueGroupResponseId
                    INNER JOIN @AppropriateUniqueResponses AUR ON  AUR.UniqueResponseId = UR.id
                )
        
        IF EXISTS (
               SELECT ID
               FROM   dbo.AssignedGroupMarks AGM
               WHERE  AGM.UniqueGroupResponseId = @UniqueGroupResponseId
           )
        BEGIN
            SELECT 0
        END
        ELSE
        BEGIN
            DECLARE @myAssignedMarkMarkingMethod INT=12
            
            UPDATE dbo.UniqueGroupResponses
            SET    ConfirmedMark = @SumOfAssignedMarks
            WHERE  ID = @UniqueGroupResponseId
            
            DECLARE @assignedGroupMarkId INT=(
                        SELECT ID
                        FROM   AssignedGroupMarks
                        WHERE  UserId = @UserId
                               AND UniqueGroupResponseId = @uniqueGroupResponseId
                    )
            
            IF (@assignedGroupMarkId IS NULL)
            BEGIN
                DECLARE @groupId INT=(
                            SELECT GroupDefinitionID
                            FROM   UniqueGroupResponses
                            WHERE  ID = @uniqueGroupResponseId
                        ) 
                
                INSERT INTO dbo.AssignedGroupMarks
                  (
                    UserId
                   ,UniqueGroupResponseId
                   ,TIMESTAMP
                   ,AssignedMark
                   ,MarkingMethodId
                   ,MarkingDeviation
                   ,isConfirmedMark
                   ,GroupDefinitionID
                  )
                VALUES
                  (
                    @UserId
                   ,@uniqueGroupResponseId
                   ,@Timestamp
                   ,@SumOfAssignedMarks
                   ,@myAssignedMarkMarkingMethod
                   ,@MarkingDeviation
                   ,1
                   ,@groupId
                  )
                
                SET @assignedGroupMarkId = SCOPE_IDENTITY()
            END
            
            
            -- update confirmed marks for UniqueGroupResponseLinks
            UPDATE UGRL
            SET    ConfirmedMark = CM.Mark
            FROM   @CandidateMarks CM
            INNER JOIN dbo.UniqueGroupResponseLinks UGRL ON  CM.UniqueResponseId = UGRL.UniqueResponseId
            INNER JOIN @AppropriateUniqueResponses AUR ON  AUR.UniqueResponseId = UGRL.UniqueResponseId
            
            INSERT INTO dbo.AssignedItemMarks
              (
                GroupMarkId
               ,UniqueResponseId
               ,AssignedMark
               ,MarkingDeviation
              )
            SELECT @assignedGroupMarkId
                  ,AUR.UniqueResponseId
                  ,CM.Mark
                  ,1
            FROM   @AppropriateUniqueResponses AUR
            INNER JOIN @CandidateMarks CM ON  CM.UniqueResponseId = AUR.UniqueResponseId
        END
    END
END
GO
/****** Object:  StoredProcedure [dbo].[sm_INTEGRATIONSERVICE_AddExamSectionItems_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Vitaly Bevzik
-- Create date: 22/04/2013
-- Description:	Add ExamSectionItem
-- =============================================
CREATE PROCEDURE [dbo].[sm_INTEGRATIONSERVICE_AddExamSectionItems_sp]
	@extrenalItemId VARCHAR(50),
	@sectionId INT,
	@structureId INT,	
	@ExamID INT
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
    SET NOCOUNT ON;
    
    DECLARE @itemId INT=(
                SELECT TOP(1) ID
                FROM   dbo.Items
                WHERE  ExternalItemID = @extrenalItemId
                       AND ExamID = @ExamID
            )
    
    IF NOT EXISTS(
           SELECT *
           FROM   dbo.ExamSectionItems
           WHERE  ExamSectionID           = @sectionId
                  AND ItemID              = @itemId
                  AND ExamStructureId     = @structureId
       )
    BEGIN
        INSERT INTO dbo.ExamSectionItems
          (
            ExamSectionID
           ,ItemID
           ,ExamStructureId
          )
        VALUES
          (
            @sectionId
           ,@itemId
           ,@structureId
          )
    END
END
GO
/****** Object:  StoredProcedure [dbo].[sm_INTEGRATIONSERVICE_AssignGroupsToExamStructure_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Dmitri Adov
-- Create date: 29/04/2013
-- Description:	Assign groups to exam structure.
-- =============================================
CREATE PROCEDURE [dbo].[sm_INTEGRATIONSERVICE_AssignGroupsToExamStructure_sp] 
	@ExamVersionStructureID int,
	@GroupsIds varchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
	SET NOCOUNT ON;

	-- XML => table
	BEGIN
		DECLARE @idoc INT
		EXEC sp_xml_preparedocument @idoc OUTPUT, @GroupsIds
		DECLARE @Groups TABLE ([GroupID] int);
		
		INSERT INTO @Groups
			SELECT GroupID
			FROM OPENXML(@idoc,'/items/item') 
				WITH ([GroupID] int '.')
						
		EXEC sp_xml_removedocument @idoc
	END
	
	-- Associate groups with the exam structure	
	INSERT INTO dbo.GroupDefinitionStructureCrossRef
		(
			GroupDefinitionID,
			ExamVersionStructureID,
			Status
		)
		SELECT 
			GroupID,
			@ExamVersionStructureID,
			0 -- 'Active'
		FROM @Groups
		LEFT JOIN dbo.GroupDefinitionStructureCrossRef GDSCR 
			ON (GDSCR.GroupDefinitionID = GroupID AND GDSCR.ExamVersionStructureID = @ExamVersionStructureID)
		WHERE GDSCR.GroupDefinitionID IS NULL
END
GO
/****** Object:  StoredProcedure [dbo].[sm_INTEGRATIONSERVICE_AttachCandidateExamVersionToStructure_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Dmitri Adov
-- Create date: 22/04/2013
-- Description:	Attaches candidate script to exam structure and updates earliest/latest dates in structure
-- =============================================
CREATE PROCEDURE [dbo].[sm_INTEGRATIONSERVICE_AttachCandidateExamVersionToStructure_sp] 
	@CandidateExamVersionID int, 
	@CompletionDate datetime,
	@ExamStructureId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
	SET NOCOUNT ON;
	
	UPDATE dbo.CandidateExamVersions
		SET ExamVersionStructureID = @ExamStructureId
		WHERE ID = @CandidateExamVersionID;
	
	UPDATE dbo.ExamVersionStructures
		SET 
			EarliestDate = CASE
				WHEN @CompletionDate < EarliestDate THEN @CompletionDate
				ELSE EarliestDate
			END,
			LatestDate = CASE
				WHEN @CompletionDate > LatestDate THEN @CompletionDate
				ELSE LatestDate
			END
		WHERE ID = @ExamStructureId;
END
GO
/****** Object:  StoredProcedure [dbo].[sm_INTEGRATIONSERVICE_Create_CandidateExamVersion_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Rathin Sundar	
-- Create date: 04/02/2011
-- Description:	Inserts a record into CandidateExamVersions table
-- Modified:	Tom Gomersall
-- Date:		27/04/2012
-- Description:	Added a check to make sure that the exam version does not already exists, in which case it should not be added again.
--				This has been added because some sessions were imported twice, which kills the confirmation back to SecureAssess.
-- Modified:	Tom Gomersall
-- Date:		15/01/2013
-- Description:	Added new parameters to insert to new columns.
-- =============================================
CREATE PROCEDURE [dbo].[sm_INTEGRATIONSERVICE_Create_CandidateExamVersion_sp] 
-- Add the parameters for the stored procedure here
	@ExternalSessionID INT,
	@ExamVersionID INT,
	@DateSubmitted DATETIME,
	@Keycode NVARCHAR(12),
	@totalmark DECIMAL(18 ,10),
	@scriptType SMALLINT,
	
	@candidateForename NVARCHAR(50)=NULL,
	@candidateSurname NVARCHAR(50)=NULL,
	@candidateRef NVARCHAR(300)=NULL,
	@centreName NVARCHAR(50)=NULL,
	@centreCode NVARCHAR(50)=NULL,
	@countryName NVARCHAR(50)=NULL,	
	@completionDate DATETIME=NULL
	AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;
    
    IF (
           EXISTS(
               SELECT ID
               FROM   dbo.CandidateExamVersions
               WHERE  ExternalSessionID = @ExternalSessionID
           )
       )
    BEGIN
        --The session has been imported already, so don't import again.
        SELECT -1
    END
    ELSE
    BEGIN
    	BEGIN TRY
			BEGIN TRAN
        
			-- Insert statements for procedure here
			INSERT INTO dbo.CandidateExamVersions
			  (
				ExternalSessionID
			   ,ExamVersionID
			   ,DateSubmitted
			   ,Keycode
			   ,CandidateForename
			   ,CandidateSurname
			   ,CandidateRef
			   ,CentreName
			   ,CentreCode
			   ,CountryName
			   ,CompletionDate
			   ,TotalMark
			   ,ScriptType
			  )
			VALUES
			  (
				@ExternalSessionID
			   ,@ExamVersionID
			   ,@DateSubmitted
			   ,@Keycode
			   ,@candidateForename
			   ,@candidateSurname
			   ,@candidateRef
			   ,@centreName
			   ,@centreCode
			   ,@countryName
			   ,@completionDate
			   ,@totalmark
			   ,@scriptType
			  )
        
			DECLARE @insertedScriptID INT=SCOPE_IDENTITY()
        
			INSERT INTO dbo.CandidateExamVersionStatuses
			  (
				-- ID -- this column value is auto-generated
				Timestamp
			   ,StateID
			   ,CandidateExamVersionID
			  )
			VALUES
			  (
				GETDATE()
			   ,1
			   ,@insertedScriptID
			  )
        
			SELECT @insertedScriptID
        
			COMMIT TRAN	
        END TRY
        BEGIN CATCH
            IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
            DECLARE @myErrorNum INT = ERROR_NUMBER()
            DECLARE @myMessage NVARCHAR(MAX) = ERROR_MESSAGE()
            DECLARE @myFullMessage NVARCHAR(MAX) = CONVERT(NVARCHAR(10), @myErrorNum)
                + ':' + @myMessage
            RAISERROR (@myFullMessage, 16, 1)
        END CATCH
    END
END
GO
/****** Object:  StoredProcedure [dbo].[sm_INTEGRATIONSERVICE_Create_CandidateResponses_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Rathin Sundar	
-- Create date: 21/02/2011
-- Description:	sproc to enter value into Candidate Responses
-- =============================================
CREATE PROCEDURE [dbo].[sm_INTEGRATIONSERVICE_Create_CandidateResponses_sp] 
	-- Add the parameters for the stored procedure here
	@CandidateExamVersionID int,
    @UniqueResponseID bigint
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO [dbo].[CandidateResponses]
           ([CandidateExamVersionID],[UniqueResponseID])
     VALUES
           (@CandidateExamVersionID, @UniqueResponseID)
END
GO
/****** Object:  StoredProcedure [dbo].[sm_INTEGRATIONSERVICE_Create_Exams_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Rathin Sundar	
-- Create date: 08/02/2011
-- Description:	Insert records in Exams for the Integration Service.
-- =============================================
CREATE PROCEDURE [dbo].[sm_INTEGRATIONSERVICE_Create_Exams_sp] 
-- Add the parameters for the stored procedure here
	@Name VARCHAR(200),
	@QualificationID INT,
	@Reference NVARCHAR(100),
	@ExternalID INT,
	
	@AnnotationSchema XML,
	@AutoCIReview BIT=1,
	@DefaultAllowAnnotationsInMarking INT=0,
	@DefaultNoOfCIsRequired INT=1,
	@DefaultTotalItemsInCompetencyRun INT=1,
	@DefaultCorrectItemsWithinToleranceWithinCompetenceRunDefault INT=1,
	@DefaultMaximumCumulativeErrorWithinCompetenceRun DECIMAL(18, 10)=1.0,
	@DefaultProbabilityOfGettingCI DECIMAL(18, 0)=1.0,
	@DefaultMaximumResponseBeforePresentingCI INT=1,
	@DeafultMaxConsecutiveCIsThatCanBeMarkedOutOfTolerance INT=1,
	@DefaultNumberOfCIsInRollingReview INT=1,
	@DefaultMaximumCIsMarkedOutOfToleranceInRollingReview INT=1
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;
    
    DECLARE @examId INT
    SET @examId = (
                SELECT ID
                FROM   dbo.Exams
                WHERE  ExternalID = @ExternalID
            )
    
    -- Insert statements for procedure here
    IF @examId IS NULL
    BEGIN
    	
         INSERT INTO dbo.Exams
          (
            NAME
           ,QualificationID
           ,Reference
           ,ExternalID
           ,AnnotationSchema           
           ,AutoCIReview
           ,DefaultAllowAnnotationsInMarking
           ,DefaultNoOfCIsRequired
           ,DefaultTotalItemsInCompetencyRun
           ,DefaultCorrectItemsWithinToleranceWithinCompetenceRunDefault
           ,DefaultMaximumCumulativeErrorWithinCompetenceRun
           ,DefaultProbabilityOfGettingCI
           ,DefaultMaximumResponseBeforePresentingCI
           ,DeafultMaxConsecutiveCIsThatCanBeMarkedOutOfTolerance
           ,DefaultNumberOfCIsInRollingReview
           ,DefaultMaximumCIsMarkedOutOfToleranceInRollingReview
           
          )
        VALUES
          (
            @Name
           ,@QualificationID
           ,@Reference
           ,@ExternalID
           ,@AnnotationSchema		              
           ,@AutoCIReview
           ,@DefaultAllowAnnotationsInMarking
           ,@DefaultNoOfCIsRequired
           ,@DefaultTotalItemsInCompetencyRun
           ,@DefaultCorrectItemsWithinToleranceWithinCompetenceRunDefault
           ,@DefaultMaximumCumulativeErrorWithinCompetenceRun
           ,@DefaultProbabilityOfGettingCI
           ,@DefaultMaximumResponseBeforePresentingCI
           ,@DeafultMaxConsecutiveCIsThatCanBeMarkedOutOfTolerance
           ,@DefaultNumberOfCIsInRollingReview
           ,@DefaultMaximumCIsMarkedOutOfToleranceInRollingReview
          )
          
        SET @examId = SCOPE_IDENTITY()
        
    END
    ELSE
    BEGIN
        
        UPDATE dbo.Exams
        SET    NAME = @Name
              ,QualificationID = @QualificationID
              ,Reference = @Reference
              ,AnnotationSchema = @AnnotationSchema
        WHERE  ExternalID = @ExternalID
               
    END
    
    SELECT @examId;
END
GO
/****** Object:  StoredProcedure [dbo].[sm_INTEGRATIONSERVICE_Create_ExamVersions_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Rathin Sundar
-- Create date: 04/02/2011
-- Description:	Create an Entry  into the ExamVersions table
-- =============================================
CREATE PROCEDURE [dbo].[sm_INTEGRATIONSERVICE_Create_ExamVersions_sp] 
-- Add the parameters for the stored procedure here
	@Name NVARCHAR(100),
	@ExternalID INT,
	@ExamID INT,
	@ExternalExamReference NVARCHAR(50)
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;
    
    DECLARE @examVersionID INT    
    SET @examVersionID = (
            SELECT TOP 1 ID
            FROM   dbo.ExamVersions
            WHERE  ExternalID = @ExternalID
        )
    
    IF @examVersionID IS NULL
    BEGIN
        -- Create a new Record in ExamVersions Table.
        INSERT INTO dbo.ExamVersions
          (
            NAME
           ,ExternalID
           ,ExamID
           ,ExternalExamReference
          )
        VALUES
          (
            @Name
           ,@ExternalID
           ,@ExamID
           ,@ExternalExamReference
          )
        
        SET @examVersionID = SCOPE_IDENTITY()
        
    END
    ELSE
    BEGIN
    	
        UPDATE dbo.ExamVersions
        SET    NAME                 = @Name
        WHERE  ExternalID           = @ExternalID
        
    END
    
    SELECT @examVersionID
END
GO
/****** Object:  StoredProcedure [dbo].[sm_INTEGRATIONSERVICE_Create_Items_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Rathin Sundar	
-- Create date: 16/02/2011
-- Description:	Sproc to enter values into Items Table.
-- Please Do not edit this sproc Return values as this may affect the string manipulation code
-- in the calling procedures in Secure Marker.
-- Author:		Vitaly Bevzik
-- Create date: 5/6/2013
-- Description:	Removed old competency settings
-- =============================================
CREATE PROCEDURE [dbo].[sm_INTEGRATIONSERVICE_Create_Items_sp] 
-- Add the parameters for the stored procedure here
-- Assign Default values if the calling code does not supply the system defined parameters.
	@CandidateExamVersionID INT,
	@AllowAnnotations INT=1,
	@Version INT,
	@ExternalItemID NCHAR(20),
	@MarkingType INT,
	@TotalMark DECIMAL(18 ,10),
	@ItemTolerance DECIMAL(18 ,10),
	@ExternalContent VARBINARY(MAX),
	@LayoutName NVARCHAR(50)=NULL,
	@ExamID INT,
	@ExamVersionID INT,	
	@ExamGroupID INT,
	@ExamGroupName NVARCHAR(255)=NULL,
	@ExternalItemName NVARCHAR(MAX)
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;
    
    DECLARE @existItem INT=NULL	
    SELECT TOP 1 @existItem = ID
    FROM   dbo.Items
    WHERE  ExternalItemID = @ExternalItemID
    
    DECLARE @itemId INT=NULL
    DECLARE @itemVersion INT
    SELECT TOP 1 @itemId = I.ID
          ,@itemVersion = I.Version
    FROM   dbo.Items I
    JOIN dbo.ExamVersionItemsRef REF 
		ON  REF.ItemID=I.ID AND REF.ExamVersionID=@ExamVersionID
    WHERE  ExternalItemID = @ExternalItemID
           AND ExamID = @ExamID
    
    IF (@itemId IS NOT NULL)
       AND (@itemVersion<>@Version)
    BEGIN
        SET @itemId = (
                SELECT TOP 1 ID
                FROM   dbo.Items
                WHERE  ExternalItemID     = @ExternalItemID
                       AND Version        = @Version
                       AND ExamID         = @ExamID
            )
    END
    
    IF (@itemId IS NULL)
    BEGIN
        SELECT TOP 1 @itemId = ID
        FROM   dbo.Items
        WHERE  ExternalItemID     = @ExternalItemID
               AND ExamID         = @ExamID
        ORDER BY
               Version DESC
    END		
    
    IF @itemId IS NULL
    BEGIN
        IF EXISTS (
               SELECT TOP 1 ev.ID
               FROM   dbo.ExamVersions AS ev
               WHERE  ev.ID = @ExamVersionID
           )
        BEGIN
            SELECT TOP 1
                   @AllowAnnotations = Ex.DefaultAllowAnnotationsInMarking
            FROM   dbo.Exams AS EX
            WHERE  Ex.ID = @ExamID
            ORDER BY
                   ID DESC
        END
        
        -- Copy content of the item existing in other exam version.
        DECLARE @sampleContent VARBINARY(MAX)
        SELECT TOP 1 @sampleContent = ExternalContent
        FROM   dbo.Items
        WHERE  ExternalItemID     = @ExternalItemID
               AND Version        = @Version
        
        -- Use dummy content if real is not found
        SELECT @sampleContent = ISNULL(@sampleContent ,@ExternalContent)
        
        -- Insert statements for procedure here
        INSERT INTO dbo.Items
          (
            AllowAnnotations
           ,Version
           ,ExternalItemID
           ,MarkingType
           ,CIMarkingTolerance
           ,TotalMark
           ,MaximumCumulativeErrorWithinCompetenceRun
           ,ExternalContent
           ,MaxConsecutiveCIsThatCanBeMarkedOutOfTolerance
           ,MaximumCIsMarkedOutOfToleranceInRollingReview
           ,LayoutName
           ,ExamID
           ,ExternalItemName
           ,ExamGroupID
           ,ExamGroupName
          )
        SELECT @AllowAnnotations
              ,@Version
              ,@ExternalItemID
              ,@MarkingType
              ,@ItemTolerance
              ,@TotalMark
              ,Exams.DefaultMaximumCumulativeErrorWithinCompetenceRunItemLevel
              ,@sampleContent
              ,Exams.DeafultMaxConsecutiveCIsThatCanBeMarkedOutOfToleranceItemLevel
              ,Exams.DefaultMaximumCIsMarkedOutOfToleranceInRollingReviewItemLevel
              ,@LayoutName
              ,@ExamID
              ,@ExternalItemName
              ,@ExamGroupID
              ,@ExamGroupName
        FROM   dbo.Exams Exams
        WHERE  Exams.ID = @ExamID
        
        SET @itemId = SCOPE_IDENTITY()
    END
    ELSE
    BEGIN
        UPDATE dbo.Items
        SET    Version     = @Version
        WHERE  ID          = @itemID
    END
    
    --Insert into ExamVersionItemsRef	
    IF NOT EXISTS (
           SELECT TOP 1*
           FROM   dbo.ExamVersionItemsRef
           WHERE  ItemID                = @itemId
                  AND ExamVersionID     = @ExamVersionID
       )
    BEGIN
        INSERT INTO dbo.ExamVersionItemsRef
          (
            ExamVersionID
           ,ItemID
          )
        VALUES
          (
            @ExamVersionID
           ,@itemId
          )
    END
    
    --after processing the item enter a record in ExamversionItems Table.
    INSERT INTO dbo.ExamVersionItems
      (
        CandidateExamVersionID
       ,ItemID
      )
    VALUES
      (
        @CandidateExamVersionID
       ,@itemId
      )
    
    IF (@existItem IS NULL)
       -- New item version
    BEGIN
        SELECT 'ONLYVERSION' + CAST(@itemId AS VARCHAR(20));
    END
    ELSE
        -- new or existing item
    BEGIN
        SELECT @itemId
    END
END

GO
/****** Object:  StoredProcedure [dbo].[sm_INTEGRATIONSERVICE_Create_ItemTools_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sm_INTEGRATIONSERVICE_Create_ItemTools_sp] 
	@ToolData XML
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;

	DECLARE @hdoc	INT
	EXEC sp_xml_preparedocument @hdoc OUTPUT, @ToolData	
		
	DECLARE @ToolItemsTable	TABLE
	(
		[ParentItemId]			INT,
		[ToolItemId]			INT,
		[ToolName]				NVARCHAR(1024)
	)
		
	INSERT INTO @ToolItemsTable(		
		[ParentItemId],
		[ToolItemId],
		[ToolName])
	SELECT
		[ParentItemId],
		[ToolItemId],
		[ToolName] 
	FROM  
	OPENXML(@hdoc, 'CSecureMarkerToolInfos/ItemTools/CSecureMarkerToolInfo',1)  
	WITH (
		[ParentItemId]			INT				'./ParentItemId',
		[ToolItemId]			INT				'./ToolItemId',
		[ToolName]				NVARCHAR(1024)	'./ToolName'
	)
		
	EXEC sp_xml_removedocument @hdoc

	DELETE dbo.ItemTools
	FROM @ToolItemsTable newTools
	RIGHT JOIN dbo.ItemTools existingTools 
	ON (existingTools.[ParentItemId] = newTools.[ParentItemId] and existingTools.[ToolItemId] = newTools.[ToolItemId])
	JOIN (SELECT DISTINCT [ParentItemId] FROM @ToolItemsTable) items
	ON (existingTools.[ParentItemId] = items.[ParentItemId])
	WHERE newTools.[ToolItemId] is NULL

	UPDATE dbo.ItemTools
	SET dbo.ItemTools.[ToolName] = newTools.[ToolName]
	FROM @ToolItemsTable newTools
	JOIN dbo.ItemTools existingTools 
	ON (existingTools.[ParentItemId] = newTools.[ParentItemId] and existingTools.[ToolItemId] = newTools.[ToolItemId])
	WHERE existingTools.[ToolName] <> newTools.[ToolName]

	INSERT INTO dbo.ItemTools(		
		[ParentItemId],
		[ToolItemId],
		[ToolName])
	SELECT
		newTools.[ParentItemId],
		newTools.[ToolItemId],
		newTools.[ToolName] 
	FROM @ToolItemsTable newTools
	LEFT JOIN dbo.ItemTools existingTools 
	ON (existingTools.[ParentItemId] = newTools.[ParentItemId] and existingTools.[ToolItemId] = newTools.[ToolItemId])
	WHERE existingTools.[ToolItemId] is NULL

END
GO
/****** Object:  StoredProcedure [dbo].[sm_INTEGRATIONSERVICE_Create_Qualifications_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Rathin Sundar	
-- Create date: 08/02/2011
-- Description:	Inserts a record into Qualifications table
-- =============================================
CREATE PROCEDURE [dbo].[sm_INTEGRATIONSERVICE_Create_Qualifications_sp] 
-- Add the parameters for the stored procedure here
	@Name NVARCHAR(100),
	@Reference NVARCHAR(50),
	@ExternalID INT
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;
    
    DECLARE @QualID INT
    -- Insert statements for procedure here
    IF EXISTS (
           SELECT ExternalID
           FROM   dbo.Qualifications
           WHERE  ExternalID = @ExternalID
       )
    BEGIN
    	
        UPDATE dbo.Qualifications
        SET    NAME = @Name
              ,Reference = @Reference
        WHERE  ExternalID = @ExternalID
        
        SET @QualID = (
                SELECT ID
                FROM   dbo.Qualifications
                WHERE  ExternalID = @ExternalID
            ) 
    END
    ELSE
    BEGIN
    	
        INSERT INTO dbo.Qualifications
          (
            NAME
           ,Reference
           ,ExternalID
          )
        VALUES
          (
            @Name
           ,@Reference
           ,@ExternalID
          )
        
        SET @QualID = SCOPE_IDENTITY()
    END		
    
    SELECT @QualID;
END
GO
/****** Object:  StoredProcedure [dbo].[sm_INTEGRATIONSERVICE_Create_UniqueGroupResponses_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Dmitri Adov
-- Create date: 13/05/13
-- Description:	For candidate script calculates unique group responses for all groups within exam structure

-- Author:			George Chernov
-- Modified date:	10/06/13
-- Description:		call sm_INTEGRATIONSERVICE_AddAutoAssignedMarks_sp
-- =============================================

CREATE PROCEDURE [dbo].[sm_INTEGRATIONSERVICE_Create_UniqueGroupResponses_sp]
    @examVersionStructureId int,
    @candidateExamVersionId	int,
    @candidateMarks [CandidateMarksType] READONLY,
    @userId INT,
    @timestamp DATETIME
AS
BEGIN
SET NOCOUNT ON;

	-- Go throw all scripts with curent examVersionStructureID and create unique group responses
	DECLARE groupsCursor CURSOR FAST_FORWARD FOR  
		SELECT GD.id 
			FROM dbo.GroupDefinitions GD
				JOIN dbo.GroupDefinitionStructureCrossRef CDSCR ON CDSCR.GroupDefinitionID = GD.ID
				JOIN dbo.ExamVersionStructures EVS ON EVS.ID = CDSCR.ExamVersionStructureID									
		WHERE EVS.ID = @examVersionStructureID 
			AND CDSCR.Status = 0 -- 'Active'

	DECLARE @groupDefinitionId int
	OPEN groupsCursor   
	FETCH NEXT FROM groupsCursor INTO @groupDefinitionId 
	
	WHILE @@FETCH_STATUS = 0  
	BEGIN   	
		DECLARE @uniqueGroupResponseId INT 
		
		EXEC [dbo].[sm_ITEMGROUPSSERVICE_Create_UniqueGroupResponses_sp]
			 @groupDefinitionId = @groupDefinitionId,
			 @examVersionStructureId = NULL,
			 @candidateExamVersionId = @candidateExamVersionId,
			 @uniqueGroupResponseId = @uniqueGroupResponseId OUTPUT
			
		EXEC [dbo].[sm_INTEGRATIONSERVICE_AddAutoAssignedMarks_sp]
			 @UserId  = @userId,
			 @uniqueGroupResponseId = @uniqueGroupResponseId,
			 @Timestamp = @timestamp,
			 @MarkingDeviation = 0,
			 @CandidateMarks = @candidateMarks
		
		FETCH NEXT FROM groupsCursor INTO @groupDefinitionId 
	END   

	CLOSE groupsCursor   
	DEALLOCATE groupsCursor
			
END
GO
/****** Object:  StoredProcedure [dbo].[sm_INTEGRATIONSERVICE_Create_UniqueResponses_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Rathin Sundar	
-- Create date: 17/02/2011
-- Description:	sproc that add a new record to UniqueResponses Table
-- =============================================
CREATE PROCEDURE [dbo].[sm_INTEGRATIONSERVICE_Create_UniqueResponses_sp] 
	-- Add the parameters for the stored procedure here
           @itemId int,
           @responseData xml = null,
           @checksum nchar(10),
           @tokenId int = null,
           @confirmedMark decimal(18,0) = NULL,
           @CI_Review bit = 0,
           @CI bit = 0,
           @insertionDate datetime,
           @latestMarkSchemeIdAtTimeOfImport int,
           @markedMetadataResponse NVARCHAR(MAX) = NULL,
           @Action nvarchar(10) = ''
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    DECLARE @UniqueResponseID nvarchar(50)
    DECLARE @myUniqueResponseID int = null
    DECLARE @MarkedMarkSchemeID int
    DECLARE @myMarkedMetadataXml	XML = NULL
    IF(LEN(@markedMetadataResponse) > 0)
		BEGIN
			SET @myMarkedMetadataXml = '<item>' + @markedMetadataResponse + '</item>'
		END
    
    
    IF(@Action = 'NEW')
		BEGIN
			INSERT INTO [dbo].[UniqueResponses]
				   (
					[itemId]
					,[responseData]
					,[checksum]
					,[confirmedMark]
					,[insertionDate]
					,[latestMarkSchemeIdAtTimeOfImport]
					,[markedMetadataResponse]
					)
			 VALUES
				   (
					@itemId
					,@responseData
					,@checksum
					,@confirmedMark
					,@insertionDate
					,@latestMarkSchemeIdAtTimeOfImport
					,@myMarkedMetadataXml
					)
				   
			SET @UniqueResponseID = CAST ( scope_identity() AS nvarchar(10)) 
		END
	ELSE
		BEGIN
		    SET @myUniqueResponseID = (SELECT TOP (1) [id] FROM [dbo].[UniqueResponses] WHERE itemId = @itemId and [checksum] = @checksum)
		    
		    DECLARE @myUniqueResponseConfirmedMark DECIMAL
		    DECLARE @myUniqueResponseMarkSchemeId INT
		    
		    SELECT
				@myUniqueResponseConfirmedMark = confirmedMark
				,@myUniqueResponseMarkSchemeId = markSchemeId
			FROM UniqueResponses
			WHERE ID = @myUniqueResponseID
			
			--If there is not already a unique response or if there is one but it has been marked already with a different mark scheme, then a new one must be inserted
			IF ((@myUniqueResponseID IS NULL) OR ((@myUniqueResponseConfirmedMark IS NOT NULL) AND (@myUniqueResponseMarkSchemeId <> @latestMarkSchemeIdAtTimeOfImport)))
				BEGIN
					INSERT INTO [dbo].[UniqueResponses]
						   (
							[itemId]
							,[responseData]
							,[checksum]
							,[confirmedMark]
							,[insertionDate]
							,[latestMarkSchemeIdAtTimeOfImport]
							,[markedMetadataResponse]
							)
					 VALUES
						   (
							@itemId
							,@responseData
							,@checksum
							,@confirmedMark
							,@insertionDate
							,@latestMarkSchemeIdAtTimeOfImport 
							,@myMarkedMetadataXml
							)
						   
					SET @UniqueResponseID = CAST ( scope_identity() AS nvarchar(10)) 
				END   
			 ELSE
				--Record already exist so Return the current SM marked, MarkSchemeID of this UniqueResponse, back to the calling procedure 
				BEGIN
					
					SET @MarkedMarkSchemeID = (SELECT TOP (1) [markSchemeId] FROM [dbo].[UniqueResponses] WHERE itemId = @itemId and [checksum] = @checksum )
					IF (@MarkedMarkSchemeID IS NOT NULL)
						BEGIN
							SET @UniqueResponseID =    CAST ( @MarkedMarkSchemeID AS nvarchar(10)) + 'MARKSCHEMEID' + CAST ( @myUniqueResponseID AS nvarchar(10)) 
						END	
					ELSE
						BEGIN
							SET @UniqueResponseID =    CAST ( @myUniqueResponseID AS nvarchar(10)) 
						END	
				END    
		END	
     SELECT @UniqueResponseID
END
GO
/****** Object:  StoredProcedure [dbo].[sm_INTEGRATIONSERVICE_CreateAutomaticGroups_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Dmitri Adov
-- Create date: 29/04/2013
-- Description:	Create automatic groups for exam structure items not matching to existing groups.
-- =============================================
CREATE PROCEDURE [dbo].[sm_INTEGRATIONSERVICE_CreateAutomaticGroups_sp] 
	@ExamVersionStructureID int,
	@ExamID int,
	@ItemsIds varchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
	SET NOCOUNT ON;

	-- XML => table
	BEGIN
		DECLARE @idoc INT
		EXEC sp_xml_preparedocument @idoc OUTPUT, @ItemsIds
		DECLARE @ItemsNotInGroups TABLE ([ItemID] int);
		
		INSERT INTO @ItemsNotInGroups
			SELECT ItemID
			FROM OPENXML(@idoc,'/items/item') 
				WITH ([ItemID] int '.')
						
		EXEC sp_xml_removedocument @idoc
	END
	
	DECLARE @InsertedGroups TABLE(GroupID int, ItemID int)

	-- MERGE is used instead of old good INSERT to be able to OUTPUT ItemID
	MERGE dbo.GroupDefinitions
		USING 
		(
			SELECT 
				 INIG.ItemID
				,I.ExternalItemName
				,I.TotalMark
				,E.DefaultNoOfCIsRequired AS NoOfCIsRequired
				,I.CIMarkingTolerance
				,E.DefaultTotalItemsInCompetencyRun AS TotalItemsInCompetencyRun
				,E.DefaultCorrectItemsWithinToleranceWithinCompetenceRunDefault AS CorrectItemsWithinToleranceWithinCompetenceRun
				,E.DefaultMaximumCumulativeErrorWithinCompetenceRun AS MaximumCumulativeErrorWithinCompetenceRun 
				,E.DefaultProbabilityOfGettingCI AS ProbabilityOfGettingCI
				,E.DefaultMaximumResponseBeforePresentingCI AS MaximumResponseBeforePresentingCI
				,E.DeafultMaxConsecutiveCIsThatCanBeMarkedOutOfTolerance AS MaxConsecutiveCIsThatCanBeMarkedOutOfTolerance
				,E.DefaultNumberOfCIsInRollingReview AS NumberOfCIsInRollingReview
				,E.DefaultMaximumCIsMarkedOutOfToleranceInRollingReview AS MaximumCIsMarkedOutOfToleranceInRollingReview				
			FROM @ItemsNotInGroups INIG
				JOIN dbo.Items I ON INIG.ItemID = I.ID
				JOIN dbo.Exams E ON E.ID = @ExamID
		) AS source
		ON (0 = 1)
		WHEN NOT MATCHED THEN
			INSERT
			(
				 IsAutomaticGroup
				,Name
				,TotalMark
				,NoOfCIsRequired
				,CIMarkingTolerance
				,TotalItemsInCompetencyRun
				,CorrectItemsWithinToleranceWithinCompetenceRun
				,MaximumCumulativeErrorWithinCompetenceRun
				,ProbabilityOfGettingCI
				,MaximumResponseBeforePresentingCI
				,MaxConsecutiveCIsThatCanBeMarkedOutOfTolerance
				,NumberOfCIsInRollingReview
				,MaximumCIsMarkedOutOfToleranceInRollingReview
				,ExamID
			)
			VALUES 
				(
				 1
				,source.ExternalItemName
				,source.TotalMark
				,source.NoOfCIsRequired
				,source.CIMarkingTolerance
				,source.TotalItemsInCompetencyRun
				,source.CorrectItemsWithinToleranceWithinCompetenceRun
				,source.MaximumCumulativeErrorWithinCompetenceRun
				,source.ProbabilityOfGettingCI
				,source.MaximumResponseBeforePresentingCI
				,source.MaxConsecutiveCIsThatCanBeMarkedOutOfTolerance
				,source.NumberOfCIsInRollingReview
				,source.MaximumCIsMarkedOutOfToleranceInRollingReview
				,@ExamID	
				)
			OUTPUT INSERTED.ID, source.ItemID INTO @InsertedGroups(GroupID, ItemID);
			
	-- Add group-item associations for automatic groups
	INSERT INTO dbo.GroupDefinitionItems
		(
			 GroupID
			,ItemID
			,ViewOnly
			,GroupDefinitionItemOrder
		)
		SELECT 
			 GroupID
			,ItemID
			,0
			,0
		FROM @InsertedGroups
	
	-- Associate created groups with the exam structure
	
	INSERT INTO dbo.GroupDefinitionStructureCrossRef
		(
			GroupDefinitionID,
			ExamVersionStructureID,
			Status
		)
		SELECT 
			GroupID,
			@ExamVersionStructureID,
			0 -- 'Active'
		FROM @InsertedGroups
		LEFT JOIN dbo.GroupDefinitionStructureCrossRef GDSCR 
			ON (GDSCR.GroupDefinitionID = GroupID AND GDSCR.ExamVersionStructureID = @ExamVersionStructureID)
		WHERE GDSCR.GroupDefinitionID IS NULL	
	
END
GO
/****** Object:  StoredProcedure [dbo].[sm_INTEGRATIONSERVICE_CreateExamSection_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Dmitri Adov
-- Create date: 22/04/2013
-- Description:	Inserts/updates record in ExamSections for the Integration Service.
-- =============================================
CREATE PROCEDURE [dbo].[sm_INTEGRATIONSERVICE_CreateExamSection_sp]
	@ExternalID NVARCHAR(50),
	@ExamVersionID INT,
	@Name NVARCHAR(255),
	@IsDynamic BIT
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
    SET NOCOUNT ON;
    
    DECLARE @ExamSectionID INT
        
    SELECT @ExamSectionID = ID
    FROM   dbo.ExamSections
    WHERE  ExternalID = @ExternalID  AND ExamVersionID = @ExamVersionID
    
    IF @ExamSectionID IS NOT NULL
    BEGIN
    	
        UPDATE dbo.ExamSections
        SET    NAME = @Name
              ,ExamVersionID = @ExamVersionID
               -- Once section is dynamic, it will be always dynamic
              ,IsDynamic = CASE 
                                WHEN IsDynamic=1 OR @IsDynamic=1 THEN 1
                                ELSE 0
                           END
        WHERE  ID = @ExamSectionID
        
    END
    ELSE
    BEGIN					     	
        INSERT INTO dbo.ExamSections
          (
            NAME
           ,IsDynamic
           ,ExternalID
           ,ExamVersionID
          )
        VALUES
          (
            @Name
           ,@IsDynamic
           ,@ExternalID
           ,@ExamVersionID
          )
        
        SET @ExamSectionID = SCOPE_IDENTITY()
        
    END
    
    SELECT @ExamSectionID;
END
GO
/****** Object:  StoredProcedure [dbo].[sm_INTEGRATIONSERVICE_CreateExamVersionStructure_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Dmitri Adov
-- Create date: 22/04/2013
-- Description:	Inserts/updates record in ExamVersionStructures and ExamVersionStructureItems for the Integration Service.
-- =============================================
CREATE PROCEDURE [dbo].[sm_INTEGRATIONSERVICE_CreateExamVersionStructure_sp]
	@ExamVersionID INT,
	@ItemsIDs NVARCHAR(MAX),
	@EarliestDate DATETIME,
	@LatestDate DATETIME
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
    SET NOCOUNT ON;
    
    -- Resolve items by external IDs
    DECLARE @ItemsIdsTable TABLE(ItemID INT)
    
    INSERT @ItemsIdsTable
      (
       ItemID
      )
    SELECT CAST(P.Value AS INT)
    FROM   dbo.ParamsToList(@ItemsIDs) P
    
    -- Find a structure matching the items 	
    DECLARE @ExamStructureID INT;
    
    DECLARE structuresCursor CURSOR  
    FOR
        SELECT ID
        FROM   dbo.ExamVersionStructures EVS
        WHERE  EVS.ExamVersionID = @ExamVersionID
    
    DECLARE @tempStructureID INT
    OPEN structuresCursor 
    FETCH NEXT FROM structuresCursor INTO @tempStructureID 
    
    DECLARE @StructureItems TABLE(ItemID INT)
    WHILE @@FETCH_STATUS=0
    BEGIN
        DELETE 
        FROM   @StructureItems
        
        INSERT @StructureItems
        SELECT ItemID
        FROM   dbo.ExamVersionStructureItems
        WHERE  ExamStructureID = @tempStructureID
        
        -- try to found absolutelly equal structure
        IF NOT EXISTS (
               SELECT 1
               FROM   @ItemsIdsTable
               WHERE  ItemID NOT IN (SELECT ItemID FROM @StructureItems)
           )
           AND NOT EXISTS (
                   SELECT 1
                   FROM   @StructureItems
                   WHERE  ItemID NOT IN (SELECT ItemID FROM   @ItemsIdsTable)
               )
        BEGIN
            SET @ExamStructureID = @tempStructureID
            BREAK
        END
        
        FETCH NEXT FROM structuresCursor INTO @tempStructureID
    END 
    
    CLOSE structuresCursor 
    DEALLOCATE structuresCursor
    
    -- Create new structure 
    IF @ExamStructureID IS NULL
    BEGIN
        
        INSERT INTO dbo.ExamVersionStructures
          (
            ExamVersionID
           ,StatusID
           ,EarliestDate
           ,LatestDate
          )
        VALUES
          (
            @ExamVersionID
           ,0 -- 'Released'
           ,@EarliestDate
           ,@LatestDate
          )
        
        SELECT @ExamStructureID = SCOPE_IDENTITY()
        
        INSERT INTO dbo.ExamVersionStructureItems
          (
            ExamStructureID
           ,ItemID
          )
        SELECT @ExamStructureID
              ,IDS.ItemID
        FROM   @ItemsIdsTable IDS
    END
    
    SELECT @ExamStructureID;
END
GO
/****** Object:  StoredProcedure [dbo].[sm_INTEGRATIONSERVICE_CreateMarkSchemes_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sm_INTEGRATIONSERVICE_CreateMarkSchemes_sp]
	@markSchemeBlOb VARBINARY(MAX)=NULL,
	@version INT=0,
	@ExternalItemID NCHAR(20),
	@IsMarkingCriteria BIT=1
AS
BEGIN
    SET NOCOUNT ON;
    
    UPDATE dbo.MarkSchemes
    SET    markSchemeBlOb = @markSchemeBlOb
    WHERE  ExternalItemID = @ExternalItemID
           --AND version = @version
    
    -- if nothing was updated then we need to INSERT it as expected	
    IF @@ROWCOUNT=0
    BEGIN
        INSERT INTO dbo.MarkSchemes
          (
            markSchemeBlOb
           ,version
           ,ExternalItemID
           ,IsMarkingCriteria
          )
        VALUES
          (
            @markSchemeBlOb
           ,@version
           ,@ExternalItemID
           ,@IsMarkingCriteria
          )
    END
    
    SELECT ID
    FROM   dbo.MarkSchemes
    WHERE  ExternalItemID = @ExternalItemID
           --AND version = @version
END

GO
/****** Object:  StoredProcedure [dbo].[sm_INTEGRATIONSERVICE_CreateUniqueResponseDocument_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Rathin Sundar	
-- Create date: 17/02/2011
-- Description:	sproc that creates records for unique responses document.
-- =============================================
CREATE PROCEDURE [dbo].[sm_INTEGRATIONSERVICE_CreateUniqueResponseDocument_sp]  
	-- Add the parameters for the stored procedure here
		@Name				nvarchar(100),
        @Blob				varbinary(max),
        @UniqueResponseID	bigint,
        @BlobType			nvarchar(20),
        @externalID			INT,
        @uploadedDate		DATETIME,
        @fileName			NVARCHAR(200)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO [dbo].[UniqueResponseDocuments]
	(
		[Name],
		[Blob],
		[UniqueResponseID],
		[BlobType],
		[ExternalDocID],
		[UploadedDate],
		[FileName]
	)
	VALUES
	(
		@Name,
		@Blob,
		@UniqueResponseID,
		@BlobType,
		@externalID,
		@uploadedDate,
		@fileName
	)
	
	 SELECT scope_identity()            
           
END
GO
/****** Object:  StoredProcedure [dbo].[sm_INTEGRATIONSERVICE_Get_AssignedMarks_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Rathin Sundar	
-- Create date: 21/02/2011
-- Description:	sproc to get AssignedMarks record for a given UniqueResponse Table
-- This sproc can be used to check if an uniqueresponse item has beeen marked in SM.
-- =============================================
CREATE PROCEDURE [dbo].[sm_INTEGRATIONSERVICE_Get_AssignedMarks_sp] 
-- Add the parameters for the stored procedure here
	@uniqueResponseId BIGINT
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;
    
    SELECT TOP 1 AIM.AssignedMark
    FROM   dbo.AssignedItemMarks AIM
    INNER JOIN dbo.AssignedGroupMarks AGM
	ON  AGM.ID = AIM.GroupMarkId AND AGM.IsConfirmedMark = 1
    WHERE  AIM.UniqueResponseId = @uniqueResponseId
    
END
GO
/****** Object:  StoredProcedure [dbo].[sm_INTEGRATIONSERVICE_Get_DistinctExistingItemsList_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================    
-- Author:  Umair Rafique    
-- Create date: 09/05/2011    
-- Description: sproc that gets the distincts items list  
-- =============================================    
CREATE PROCEDURE [dbo].[sm_INTEGRATIONSERVICE_Get_DistinctExistingItemsList_sp]     
AS    
BEGIN      
  SELECT DISTINCT RTRIM(ExternalItemID), [Version]    
  FROM dbo.Items
  WHERE DATALENGTH(ExternalContent) >= 100 -- Do not consider dummy content "Put Some stuff and Update in items Processing"
END
GO
/****** Object:  StoredProcedure [dbo].[sm_INTEGRATIONSERVICE_Get_ItemTools_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sm_INTEGRATIONSERVICE_Get_ItemTools_sp] 
	@externalId nvarchar(50),
	@itemVersion int,
	@toolname varchar(1024),
	@toolVersion int OUTPUT,
	@toolExternalId nvarchar(50) OUTPUT
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;
	
	SELECT TOP 1 @toolVersion = i.Version, @toolExternalId = i.ExternalItemID FROM dbo.ItemTools it 
	JOIN dbo.Items i ON i.ID = it.ToolItemId
	JOIN dbo.Items ip ON ip.ID = it.ParentItemId
	WHERE it.ToolName = @toolname AND ip.ExternalItemID = @externalId AND ip.[Version] = @itemVersion
END
GO
/****** Object:  StoredProcedure [dbo].[sm_INTEGRATIONSERVICE_Get_LatestMarkScheme_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sm_INTEGRATIONSERVICE_Get_LatestMarkScheme_sp] 
-- Add the parameters for the stored procedure here
	@ExternalItemID NCHAR(20)
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;
    
    -- Insert statements for procedure here
    SELECT TOP 1 id
    FROM   dbo.MarkSchemes AS M
    WHERE  M.ExternalItemID = @ExternalItemID
		   -- Commentend because we don't use MarkScheme versions
           --AND M.version = ( SELECT MAX(version) FROM dbo.MarkSchemes WHERE ExternalItemID = @ExternalItemID )
END

GO
/****** Object:  StoredProcedure [dbo].[sm_INTEGRATIONSERVICE_Update_ExamsWithAnnotation_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Rathin Sundar	
-- Create date: 04/03/2011.
-- Description:	sproc to update the examversions in SM using the Annotation xml from SA
-- =============================================
CREATE PROCEDURE [dbo].[sm_INTEGRATIONSERVICE_Update_ExamsWithAnnotation_sp] 
-- Add the parameters for the stored procedure here
	@ExternalID INT ,
	@AnnotationSchema XML
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;
    
    -- Insert statements for procedure here
    UPDATE dbo.Exams
    SET    AnnotationSchema     = @AnnotationSchema
	FROM dbo.Exams
	JOIN dbo.ExamVersions ON dbo.Exams.ID = dbo.ExamVersions.ExamID
    WHERE  dbo.ExamVersions.ExternalID = @ExternalID
    
END
GO
/****** Object:  StoredProcedure [dbo].[sm_INTEGRATIONSERVICE_UpdateItemsWithContent_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sm_INTEGRATIONSERVICE_UpdateItemsWithContent_sp] 
-- Add the parameters for the stored procedure here
	@Version INT,
	@ExternalItemID NCHAR(20),
	@ExternalContent VARBINARY(MAX)
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;
    
    -- Insert statements for procedure here
    UPDATE dbo.Items
    SET    ExternalContent = @ExternalContent
    WHERE  ExternalItemID = @ExternalItemID
           AND [Version] = @Version
END

GO
/****** Object:  StoredProcedure [dbo].[sm_INTERNAL_recordAudit_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brendan Doyle
-- Create date: 22/2/11
-- Description:	Records audit of what users are doing. deletes records if there are more than '@maximumRecords'
-- =============================================
CREATE PROCEDURE [dbo].[sm_INTERNAL_recordAudit_sp]
	-- Add the parameters for the stored procedure here
	@userId			 int,
	@callingObject   nvarchar(200),
	@callingMethod   nvarchar(50),
	@parametersXml   XML,
	@success		 bit,
	@maximumRecords	 int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO [dbo].[UserInteractionAudit]
           (
           [UserID]
           ,[CallingClass]
           ,[CallingMethod]
           ,[Arguments]
           ,[Date]
           ,[Success])
     VALUES
           (
           @userId
           ,@callingObject
           ,@callingMethod
           ,@parametersXml
           ,GETDATE()
           ,@success
           )
           
      DECLARE @totalRecordCount	int
      SELECT  @totalRecordCount = COUNT(ID) FROM  [dbo].[UserInteractionAudit]
      print 'total records' + CONVERT(nvarchar(max), @totalRecordCount)

      IF @totalRecordCount > @maximumRecords
		  BEGIN
		  DECLARE @recordsToDelete int
		  SET @recordsToDelete = ABS(@totalRecordCount - @maximumRecords)
		  print '@recordsToDelete' + CONVERT(nvarchar(max), @recordsToDelete)
	      
		  DECLARE @mySelectToDelete	nvarchar(max)
		  SET @mySelectToDelete = 'SELECT TOP ' + CONVERT(varchar, @recordsToDelete) + 
		  ' [ID] FROM [dbo].[UserInteractionAudit]'
		  + ' ORDER BY [Date] ASC'

		  DECLARE @myDeleteStatement nvarchar(max)
		  SET @myDeleteStatement = 'DELETE FROM [dbo].[UserInteractionAudit] WHERE [ID] IN ( '+ @mySelectToDelete + ' )'

		  Execute (@myDeleteStatement)
	   END

END
GO
/****** Object:  StoredProcedure [dbo].[sm_ITEMGROUPSSERVICE_Create_UniqueGroupResponses_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Vitaly Bevzk
-- Create date: 07/05/13
-- Description:	Calculates unique group responses for new group for all scripts
-- =============================================
CREATE PROCEDURE [dbo].[sm_ITEMGROUPSSERVICE_Create_UniqueGroupResponses_sp]
    @groupDefinitionId		int,
    @examVersionStructureId int = NULL,
    @candidateExamVersionId	int = NULL,
    @uniqueGroupResponseId int = NULL OUTPUT
AS
BEGIN
	SET NOCOUNT ON;

	IF (@examVersionStructureID IS NULL AND @candidateExamVersionId IS NULL) OR (@examVersionStructureID IS NOT NULL AND @candidateExamVersionId IS NOT NULL)
		RAISERROR ('Either @examVersionStructureId or @candidateExamVersionId must be specified.', 16, 1);	


	-- Fill scope table
	DECLARE @scope TABLE(CandidateExamVersionID int PRIMARY KEY)
	
	IF (@candidateExamVersionId IS NOT NULL)
	BEGIN		
		-- Single script (ImportExams integration case)
		INSERT @scope(CandidateExamVersionID)
			VALUES (@candidateExamVersionId)
	END
	ELSE BEGIN
		-- All scripts (Item Groups UI case)
		INSERT @scope(CandidateExamVersionID)
			SELECT DISTINCT CEV.ID
				FROM [dbo].[CandidateExamVersions] CEV
					JOIN [dbo].CandidateResponses CR ON CEV.ID = CR.CandidateExamVersionID
					JOIN [dbo].[GroupDefinitionItems] GDI oN GDI.GroupID = @groupDefinitionId
				WHERE CEV.ExamVersionStructureID = @examVersionStructureID
					AND CEV.ExamSessionState NOT IN (6, 7)				
	END

	-- Fetch affected unique responses 
	DECLARE @uniqueResponses TABLE(CandidateExamVersionID int, UniqueResponseID bigint)
	INSERT @uniqueResponses(CandidateExamVersionID, UniqueResponseID)
		SELECT CEV.CandidateExamVersionID, CR.UniqueResponseID
		FROM dbo.CandidateResponses CR									
			JOIN [dbo].[GroupDefinitionItems] GDI ON GDI.GroupID = @groupDefinitionId
			JOIN [dbo].[UniqueResponses] UR ON UR.id = CR.UniqueResponseID and UR.itemId = GDI.ItemID
			JOIN @scope CEV ON CR.CandidateExamVersionID = CEV.CandidateExamVersionID
		WHERE CR.CandidateExamVersionID = CEV.CandidateExamVersionID

--select '@uniqueResponses', @@ROWCOUNT
			
-- Calculate checksums for each candidate response in the group	
	DECLARE @checksums TABLE(CandidateExamVersionID int, ids varchar(max), [Checksum] nchar(10) COLLATE Latin1_General_CI_AS)

	INSERT @checksums(CandidateExamVersionID, ids)
			SELECT 
				CEV.CandidateExamVersionID,
							STUFF(
								(
									SELECT CAST(UR1.UniqueResponseID AS VARCHAR(max))
									FROM @uniqueResponses UR1
									WHERE UR1.CandidateExamVersionID = CEV.CandidateExamVersionID
									FOR XML PATH ('')
								)
							,1,0,'') AS ids				   
			FROM @scope CEV 				
			GROUP BY CEV.CandidateExamVersionID
						

--select '@checksums', @@ROWCOUNT

	UPDATE @checksums SET [Checksum] = HASHBYTES('sha1', ids)

-- Create missing unique group responses
	DECLARE @uniqueGroupResponsesNotFiltered TABLE(
			GroupDefinitionID INT
			,[Checksum] NVARCHAR(50) COLLATE Latin1_General_CI_AS
			,InsertionDate DATETIME
			,CI_Review BIT
			,CI	BIT		
			,ConfirmedMark DECIMAL(9,5)
			,ScriptType SMALLINT
			,VIP BIT)
			
	INSERT INTO @uniqueGroupResponsesNotFiltered
		(					
			 [GroupDefinitionID]
			,[Checksum]
			,[InsertionDate]
			,[CI_Review]
			,[CI]			
			,[ConfirmedMark]		
			,[ScriptType]
			,[VIP]
		)
		SELECT DISTINCT
			 @groupDefinitionId
			,C.[checksum]
			,GETDATE()
			,0
			,0
			,NULL
			,CEV.ScriptType
			,CEV.VIP
		FROM dbo.CandidateExamVersions CEV
			JOIN @checksums C ON CEV.ID = C.CandidateExamVersionID
			WHERE NOT EXISTS (
				SELECT TOP 1 id
				FROM dbo.UniqueGroupResponses 
				WHERE [Checksum] = C.[checksum] and GroupDefinitionID = @groupDefinitionId
			)
		UPDATE  ugrnf				
		SET VIP = 1
		FROM @uniqueGroupResponsesNotFiltered ugrnf
		WHERE EXISTS (SELECT * FROM @uniqueGroupResponsesNotFiltered ugrnf1 
			            WHERE ugrnf1.VIP = 1 AND ugrnf1.[CHECKSUM] = ugrnf.[CHECKSUM] )
			            
	DECLARE @uniqueGroupResponsesIds TABLE(UniqueGroupResponseId INT, GroupId INT, Checksum NVARCHAR(50) COLLATE Latin1_General_CI_AS)
		
	INSERT INTO [dbo].[UniqueGroupResponses]
		(					
			 [GroupDefinitionID]
			,[Checksum]
			,[InsertionDate]
			,[CI_Review]
			,[CI]			
			,[ConfirmedMark]		
			,[ScriptType]
			,VIP
		)
		OUTPUT inserted.ID, inserted.GroupDefinitionID, inserted.[Checksum] 
		INTO @uniqueGroupResponsesIds(UniqueGroupResponseId, GroupId, [Checksum])
		SELECT DISTINCT
			 @groupDefinitionId
			,[Checksum]
			,GETDATE()
			,0
			,0
			,NULL
			,ScriptType
			,VIP
		FROM @uniqueGroupResponsesNotFiltered

--select '[UniqueGroupResponses]', @@ROWCOUNT

	DECLARE @uniqueGroupResponseLinks TABLE(UniqueGroupResponseId INT, UniqueResponseId INT)

-- Group-item unique response links
	INSERT INTO @uniqueGroupResponseLinks
	(					
		UniqueGroupResponseId
		,UniqueResponseId
	)
	SELECT DISTINCT 
		 UGRID.UniqueGroupResponseId				 
		,CR.UniqueResponseID as UniqueResponseId	
	FROM @uniqueGroupResponsesIds UGRID
		JOIN @checksums C ON c.[Checksum] = UGRID.[Checksum]
		JOIN dbo.CandidateResponses CR ON CR.CandidateExamVersionID = C.CandidateExamVersionID
		JOIN @uniqueResponses TUR ON TUR.UniqueResponseID = CR.UniqueResponseID

-- Sort by [UniqueGroupResponseLinks] clustered index

	INSERT INTO [dbo].[UniqueGroupResponseLinks] 
		(
			UniqueGroupResponseID,
			UniqueResponseId
		)
		SELECT UniqueGroupResponseId, UniqueResponseId
		FROM @uniqueGroupResponseLinks

--select '[UniqueGroupResponseLinks]', @@ROWCOUNT			

	INSERT INTO [dbo].[CandidateGroupResponses] (CandidateExamVersionID, UniqueGroupResponseID)
		SELECT C.CandidateExamVersionID, UGR.ID
		FROM @checksums C
			JOIN dbo.UniqueGroupResponses UGR ON UGR.[Checksum] = C.[Checksum] AND UGR.GroupDefinitionID = @groupDefinitionId
		WHERE NOT EXISTS 
			(
				SELECT 1 FROM dbo.CandidateGroupResponses CGR1 
				WHERE CGR1.CandidateExamVersionID = C.CandidateExamVersionID AND CGR1.UniqueGroupResponseID = UGR.ID
			)
							
--select '[CandidateGroupResponses]', @@ROWCOUNT

	IF (@candidateExamVersionId IS NOT NULL)
	BEGIN		
		-- Integration case: one row is expected
		SELECT TOP 1 @uniqueGroupResponseId = UGR.ID
			FROM @checksums C
				JOIN dbo.UniqueGroupResponses UGR ON UGR.[Checksum] = C.[Checksum] AND UGR.GroupDefinitionID = @groupDefinitionId
	END
END


GO
/****** Object:  StoredProcedure [dbo].[sm_ITEMGROUPSSERVICE_CreateGroup_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		George Chernov
-- Create date: 29/04/13
-- Description:	Add Group and assigns items for group.

-- Author:		George Chernov
-- Create date: 05/06/13
-- Description:	Insert default competancy settings
-- =============================================

CREATE PROCEDURE [dbo].[sm_ITEMGROUPSSERVICE_CreateGroup_sp]
	@groupIsAutomatic BIT,
	@groupName VARCHAR(255),	
	@groupItems NVARCHAR(MAX),
	@examId INT,
	@examStructureId INT
AS
BEGIN
    SET NOCOUNT ON;
    
    DECLARE @groupId INT
    
    DECLARE @NewGroupsTable TABLE (ItemID INT ,ViewOnly INT ,GroupDefinitionItemOrder INT)
    
    INSERT INTO @NewGroupsTable
      (
        ItemID
       ,ViewOnly
       ,GroupDefinitionItemOrder
      )
    SELECT Value1
          ,Value2
          ,Value3
    FROM   dbo.ParamsToThreeColumnTable(@groupItems)
    
    -- This Group doesn't exist, create new group with default competancy settings
    INSERT INTO dbo.GroupDefinitions
      (
        NAME
       ,ExamID
       ,IsAutomaticGroup
       ,NoOfCIsRequired
       ,CIMarkingTolerance
       ,TotalMark
       ,TotalItemsInCompetencyRun
       ,CorrectItemsWithinToleranceWithinCompetenceRun
       ,MaximumCumulativeErrorWithinCompetenceRun
       ,ProbabilityOfGettingCI
       ,MaximumResponseBeforePresentingCI
       ,MaxConsecutiveCIsThatCanBeMarkedOutOfTolerance
       ,NumberOfCIsInRollingReview
       ,MaximumCIsMarkedOutOfToleranceInRollingReview
      )
    SELECT @groupName
          ,@examId
          ,@groupIsAutomatic
          ,E.DefaultNoOfCIsRequired
          ,0
          ,0
          ,E.DefaultTotalItemsInCompetencyRun
          ,E.DefaultCorrectItemsWithinToleranceWithinCompetenceRunDefault
          ,E.DefaultMaximumCumulativeErrorWithinCompetenceRun
          ,E.DefaultProbabilityOfGettingCI
          ,E.DefaultMaximumResponseBeforePresentingCI
          ,E.DeafultMaxConsecutiveCIsThatCanBeMarkedOutOfTolerance
          ,E.DefaultNumberOfCIsInRollingReview
          ,E.DefaultMaximumCIsMarkedOutOfToleranceInRollingReview
    FROM   dbo.Exams E
    WHERE  E.ID = @examId
    
    SET @groupId = (
            SELECT SCOPE_IDENTITY() AS [SCOPE_IDENTITY]
        )	
    
    INSERT INTO dbo.GroupDefinitionItems
      (
        GroupID
       ,ItemID
       ,ViewOnly
       ,GroupDefinitionItemOrder
      )
    SELECT @groupId
          ,ItemID
          ,ViewOnly
          ,GroupDefinitionItemOrder
    FROM   @NewGroupsTable
    
	IF (NOT EXISTS(SELECT * FROM dbo.GroupDefinitionStructureCrossRef GDSCR 
			WHERE GDSCR.GroupDefinitionID = @groupId AND GDSCR.ExamVersionStructureID = @examStructureId))
	BEGIN
		INSERT INTO dbo.GroupDefinitionStructureCrossRef
		VALUES
		  (
			@groupId
		   ,@examStructureId
		   ,0
		  )
	END
    
    DECLARE @GroupTotalMark INT=(
                SELECT SUM(I.TotalMark)
                FROM   dbo.GroupDefinitionItems	GDI
                       JOIN dbo.Items I
                            ON  GDI.ItemID = I.ID
                WHERE  GDI.GroupID = @groupId
                       AND GDI.ViewOnly = 0
            )
    
    IF @GroupTotalMark IS NOT NULL
    BEGIN
        UPDATE dbo.GroupDefinitions
        SET    TotalMark = @GroupTotalMark
        WHERE  GroupDefinitions.ID = @groupId
    END
    
    --set markingType to Manual
    UPDATE dbo.Items
    SET    MarkingType = 1
    WHERE  MarkingType = 0
           AND ID IN (SELECT ItemID
                      FROM   @NewGroupsTable
                      WHERE  ViewOnly = 0)
    
    SELECT @groupId
END
GO
/****** Object:  StoredProcedure [dbo].[sm_ITEMGROUPSSERVICE_DeleteGroup_GetListOfConsequences_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Vitaly Bevzik
-- Create date: 14/05/13
-- Description:	Get list of consequences when delete group
-- =============================================

CREATE PROCEDURE [dbo].[sm_ITEMGROUPSSERVICE_DeleteGroup_GetListOfConsequences_sp]
	@groupDefinitionIds varchar(max)
AS
BEGIN

	SET NOCOUNT ON;
	
	-- select marked responses and CIs and check that they are part of incompleted scripts
	SELECT
		GD.ID									as GroupID,
		GD.Name									as GroupName,
		SUM(CASE WHEN UGR.CI = 0 THEN 1 ELSE 0 END) AS MarkedResponses,
		SUM(CASE WHEN UGR.CI = 1 THEN 1 ELSE 0 END) AS CIs,
		(SELECT COUNT(*) FROM GroupDefinitionItems GDI 
			INNER JOIN UniqueResponses UR ON UR.itemId = GDI.ItemID
			INNER JOIN Moderations M on M.UniqueResponseId = UR.id
			INNER JOIN CandidateExamVersions CEV ON CEV.ID = M.CandidateExamVersionId
		WHERE GDI.GroupID = GD.ID AND M.IsActual = 1 AND CEV.ExamSessionState  NOT IN (6,7)) AS Moderations		 		
	FROM dbo.UniqueGroupResponses UGR
	INNER JOIN GroupDefinitions GD on UGR.GroupDefinitionID = GD.ID	
	--INNER JOIN GroupDefinitionItems GDI ON GDI.GroupID = UGR.GroupDefinitionID
	WHERE GroupDefinitionID in (select * from dbo.ParamsToList(@groupDefinitionIds)) 
		AND (ConfirmedMark IS NOT NULL OR CI = 1 AND CI_Review = 0)
		AND UGR.ID in 
			(
			SELECT CGR.UniqueGroupResponseID 
				FROM dbo.CandidateGroupResponses CGR
					INNER JOIN dbo.CandidateExamVersions CEV on CEV.ID = CGR.CandidateExamVersionID
				WHERE CGR.UniqueGroupResponseID = UGR.ID AND CEV.ExamSessionState NOT IN (6,7)
			)
	group by GD.ID, GD.Name
	
END
GO
/****** Object:  StoredProcedure [dbo].[sm_ITEMGROUPSSERVICE_DeleteGroup_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Tseiko Roman
-- Create date: 2013-04-16
-- Description:	delete a group from table GroupDefinitions

-- Author:		George Chernov
-- Modified:	03/06/2013
-- Description:	Delete only groups, which doesn't have scripts with status Confirmed or Archived

-- Author:		George Chernov
-- Modified:	01/07/2013
-- Description:	Delete only groups, which doesn't have scripts with status Confirmed or Archived,
--				else mark group as "RetiredWithConfirmedScripts"

-- Author:		George Chernov
-- Modified:	19/12/2013
-- Description:	Change IsModerated flag for non-moderated Scripts

-- Author:		Vitaly Bevzik
-- Modified:	19/12/2013
-- Description:	Remove unused UniqueGroupResponses to fix problems with several exam structures for single exam version
-- =============================================
CREATE PROCEDURE [dbo].[sm_ITEMGROUPSSERVICE_DeleteGroup_sp]
	@GroupId INT,
	@StructureId INT
AS

BEGIN
	
	 BEGIN TRY
            BEGIN TRAN
	
			-- get count of confirmed or archived scripts for group
			DECLARE @IsConfirmedOrArchived INT = 
			(select COUNT(*)
			from CandidateExamVersions
			inner join CandidateGroupResponses
			on CandidateExamVersions.ID = CandidateGroupResponses.CandidateExamVersionID
			inner join UniqueGroupResponses
			on UniqueGroupResponses.ID = CandidateGroupResponses.UniqueGroupResponseID
			where UniqueGroupResponses.GroupDefinitionID = @GroupId AND ExamSessionState in (6, 7))
			
			-- if group does't have scripts with state 6 (Confirmed) or 7 (Archived)
			IF @IsConfirmedOrArchived = 0
				BEGIN 
					-- delete group
					UPDATE [dbo].[GroupDefinitionStructureCrossRef]
					SET [Status] = 1
					WHERE GroupDefinitionID = @GroupId AND ExamVersionStructureID = @StructureId 
				END
			ELSE
				BEGIN
					-- mark group as "RetiredWithConfirmedScripts"
					UPDATE [dbo].[GroupDefinitionStructureCrossRef]
					SET [Status] = 2
					WHERE GroupDefinitionID = @GroupId AND ExamVersionStructureID = @StructureId 
			END
			
			--Remove unused Unique group responses ------------------------------------------------------
			declare @ugrsToDelete TABLE (
				 Id BIGINT
			)
			
			INSERT @ugrsToDelete(Id)
			select CGR.UniqueGroupResponseID from CandidateExamVersions CEV
			inner join CandidateGroupResponses CGR on CGR.CandidateExamVersionID = CEV.ID
			INNER JOIN ..UniqueGroupResponses UGR on UGR.ID = CGR.UniqueGroupResponseID	
			where 
			CEV.ExamVersionStructureID = @StructureId
			AND UGR.GroupDefinitionID = @GroupId
			AND NOT EXISTS (
				SELECT TOP 1 * FROM CandidateGroupResponses CGR1
				INNER JOIN ..CandidateExamVersions CEV1 ON CEV1.ID = CGR1.CandidateExamVersionID
				WHERE CGR1.UniqueGroupResponseID = UGR.ID AND CGR1.CandidateExamVersionID != CEV.ID
				AND CEV1.ExamVersionStructureID != CEV.ExamVersionStructureID		
			)
			AND CEV.ExamSessionState NOT IN (6,7)
			
			select * from @ugrsToDelete
					
			DELETE FROM CandidateGroupResponses
			WHERE UniqueGroupResponseID in (SELECT * FROM @ugrsToDelete)
			
			DECLARE @deletedAGMs TABLE(
				Id BIGINT
			)
			
			INSERT INTO @deletedAGMs
			SELECT ID
			FROM AssignedGroupMarks
			WHERE UniqueGroupResponseId in (SELECT * FROM @ugrsToDelete)
			
			DELETE FROM AssignedGroupMarksParkedReasonCrossRef
			WHERE AssignedGroupMarkId IN (SELECT * FROM @deletedAGMs)
			
			DELETE FROM AssignedGroupMarks
			WHERE ID in (SELECT * FROM @deletedAGMs)
			
			UPDATE QuotaManagement
			SET PreviousAssignedMarkId = NULL
			where PreviousAssignedMarkId in (SELECT * FROM @deletedAGMs)
			
			DELETE FROM UniqueGroupResponseLinks
			WHERE UniqueGroupResponseID IN (SELECT * FROM @ugrsToDelete)
			
			DELETE FROM UniqueGroupResponseParkedReasonCrossRef
			WHERE UniqueGroupResponseId IN (SELECT * FROM @ugrsToDelete)
			
			-- delete all affected users from current ugrs
			DELETE FROM AffectedCandidateExamVersions
			WHERE UniqueGroupResponseId IN (SELECT * FROM @ugrsToDelete)
			
			DELETE FROM UniqueGroupResponses
			WHERE ID IN (SELECT * FROM @ugrsToDelete)
			
			-----------------------------------------------------------------------------------------------
			
			DECLARE @nonCompleteCandidateExamVersionsAndUniqueResponses TABLE
			(
				CandidateExamVersionID INT,
				UniqueResponseID INT
			)
			
			INSERT @nonCompleteCandidateExamVersionsAndUniqueResponses
				SELECT CEV.ID, UR.ID
				FROM dbo.GroupDefinitionItems GDI
				INNER JOIN dbo.UniqueResponses UR on UR.itemId = GDI.ItemID
				INNER JOIN dbo.CandidateResponses CR on CR.UniqueResponseID = UR.id
				INNER JOIN dbo.CandidateExamVersions CEV on CEV.ID = CR.CandidateExamVersionID
				WHERE GDI.GroupID = @GroupId
				AND CEV.ExamVersionStructureID = @StructureId
				AND CEV.ExamSessionState NOT IN (6,7)
			
			--delete Moderator's  annotations for not-completed scripts
			DELETE M 
			FROM @nonCompleteCandidateExamVersionsAndUniqueResponses T
			INNER JOIN dbo.Moderations M on M.UniqueResponseId = T.UniqueResponseID 
			AND M.CandidateExamVersionId = T.CandidateExamVersionID
			
			--update IsModerate flag for non-completed scripts
			UPDATE CEV
			SET CEV.IsModerated = 0
			FROM CandidateExamVersions CEV
			INNER JOIN @nonCompleteCandidateExamVersionsAndUniqueResponses T
			ON CEV.ID = T.CandidateExamVersionID
			WHERE NOT EXISTS (	SELECT *
								FROM Moderations AS M
								WHERE M.CandidateExamVersionId = T.CandidateExamVersionID
								AND M.IsActual = 1)
								
			
			
			
			COMMIT TRAN			
		END TRY
        BEGIN CATCH
            ROLLBACK TRAN
            DECLARE @myErrorNum INT = ERROR_NUMBER()
            DECLARE @myMessage NVARCHAR(MAX) = ERROR_MESSAGE()
            DECLARE @myFullMessage NVARCHAR(MAX) = CONVERT(NVARCHAR(10), @myErrorNum)
                + ':' + @myMessage
            RAISERROR (@myFullMessage, 16, 1)
        END CATCH

END
GO
/****** Object:  StoredProcedure [dbo].[sm_ITEMGROUPSSERVICE_getExamVersionStructures_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Tseiko Roman
-- Create date: 2013-04-19
-- Description:	GET ExamVersionStructures
-- =============================================
CREATE PROCEDURE [dbo].[sm_ITEMGROUPSSERVICE_getExamVersionStructures_sp]
	@examId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT 
		EVS.ID as ID, 
		RTRIM(LTRIM(EV.Name)) as Name, 
		EVS.LatestDate as ExamVersionStructuresDate, 
		EVSS.Name as Status,  
		COUNT(CEV.ID) as ScriptsCount,
		EVS.ExamVersionId as ExamVersionId
	FROM 
		ExamVersions EV
		JOIN ExamVersionStructures EVS ON EV.ID = EVS.ExamVersionID
		JOIN ExamVersionStructuresStatuses EVSS ON EVS.StatusID = EVSS.ID
		LEFT JOIN CandidateExamVersions CEV ON CEV.ExamVersionStructureID = EVS.ID
	WHERE 
		EV.ExamID = @examId
	GROUP BY
		EVS.ID, 
		EV.Name, 
		EVS.LatestDate,
		EVSS.Name, 
		EVS.ExamVersionId

END
GO
/****** Object:  StoredProcedure [dbo].[sm_ITEMGROUPSSERVICE_getGroupByID_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Roman Tseiko	
-- Create date: 2013-04-17 
-- Description:	Sproc to get items from GroupDefinitions.

-- Modified:	George Chernov
-- Date:		8/5/2013
-- Description:	Return name, suto state and status for group, return appropriate items for group
-- =============================================
CREATE PROCEDURE [dbo].[sm_ITEMGROUPSSERVICE_getGroupByID_sp]
	@GroupId INT,
	@StructureId INT
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @Groups TABLE(GroupID int, Name nvarchar(255), IsAutomaticGroup BIT, IsReleased BIT, IsDeleted BIT)
		
	INSERT @Groups(GroupID, Name, IsAutomaticGroup, IsReleased, IsDeleted)
		SELECT 
			 GD.ID as GroupID
			,GD.Name
			,GD.IsAutomaticGroup
			,GD.IsReleased
			,(CASE WHEN (GDSCR.Status = 1) THEN 1  ELSE 0 END)				AS IsDeleted
		FROM dbo.GroupDefinitions GD
			LEFT JOIN dbo.GroupDefinitionStructureCrossRef GDSCR ON GDSCR.GroupDefinitionID = GD.ID
			LEFT JOIN dbo.GroupDefinitionStructureCrossRefStatuses GDSCRS ON GDSCRS.ID = GDSCR.[Status] 
		WHERE GD.ID = @GroupId AND GDSCR.ExamVersionStructureID = @StructureId
			

	SELECT * FROM @Groups

	SELECT 
		 GDI.GroupID
		,GDI.ItemID
		,GDI.ViewOnly
		,I.ExternalItemName as ItemName
		,I.ExternalItemID
		,GDI.GroupDefinitionItemOrder as GroupDefinitionItemOrder
		FROM dbo.GroupDefinitionItems GDI
			JOIN dbo.Items I ON I.ID = GDI.ItemID
			JOIN @Groups G ON G.GroupID = GDI.GroupID	
		order by GroupDefinitionItemOrder		
	
END
GO
/****** Object:  StoredProcedure [dbo].[sm_ITEMGROUPSSERVICE_GetGroupItemsByExamStructureId_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Dmitri Adov
-- Create date: 29/04/2013
-- Description:	Returns all groups & items within the exam version.
-- =============================================
CREATE PROCEDURE [dbo].[sm_ITEMGROUPSSERVICE_GetGroupItemsByExamStructureId_sp]
	@ExamVersionStructureID INT
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
    SET NOCOUNT ON;
    
    SELECT GD.ID AS GroupID
          ,GD.IsAutomaticGroup
          ,GDI.ItemID
          ,GDI.ViewOnly
    FROM   dbo.GroupDefinitionItems GDI
           INNER JOIN dbo.GroupDefinitions GD ON  GD.ID = GDI.GroupID
           INNER JOIN dbo.GroupDefinitionStructureCrossRef GDSCR ON  GDSCR.GroupDefinitionID = GD.ID
    WHERE  GDSCR.ExamVersionStructureID = @ExamVersionStructureID
           AND GDSCR.[Status] = 0 -- 'Active'
END
GO
/****** Object:  StoredProcedure [dbo].[sm_ITEMGROUPSSERVICE_GetGroupsByExamId_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Dmitri Adov
-- Create date: 29/04/2013
-- Description:	Returns all groups & items within the exam.
-- =============================================
CREATE PROCEDURE [dbo].[sm_ITEMGROUPSSERVICE_GetGroupsByExamId_sp] 
	@examId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT 
			GD.ID AS GroupID,
			GD.IsAutomaticGroup,
			GDI.ItemID,
			GDI.ViewOnly

		FROM dbo.GroupDefinitionItems GDI
			JOIN dbo.GroupDefinitions GD ON GD.ID = GDI.GroupID
		WHERE GD.ExamID = @examId
			AND GD.ID IN 
				(
					SELECT GDSCR.GroupDefinitionID 
					FROM dbo.GroupDefinitionStructureCrossRef GDSCR
						INNER JOIN dbo.ExamVersionStructures EVS ON EVS.ID = GDSCR.ExamVersionStructureID
					WHERE GDSCR.Status = 0 -- 0 = Active
						AND EVS.StatusID = 0 -- 0 = Released
				)					
END
GO
/****** Object:  StoredProcedure [dbo].[sm_ITEMGROUPSSERVICE_GetGroupsForExamVersionStructure_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Dmitri Adov
-- Create date: 25/04/2013
-- Description:	Returns groups and items for specified exam structure.
-- =============================================
CREATE PROCEDURE [dbo].[sm_ITEMGROUPSSERVICE_GetGroupsForExamVersionStructure_sp] 
	@ExamVersionStructureID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
	SET NOCOUNT ON;
	
	DECLARE @Groups TABLE(GroupID int, Name nvarchar(max), IsAutomaticGroup BIT)
		
	INSERT @Groups(GroupID, Name, IsAutomaticGroup)
		SELECT 
			 GD.ID as GroupID
			,GD.Name
			,GD.IsAutomaticGroup
		FROM dbo.GroupDefinitions GD
			JOIN dbo.GroupDefinitionStructureCrossRef GDSCR ON GDSCR.GroupDefinitionID = GD.ID
			JOIN dbo.GroupDefinitionStructureCrossRefStatuses GDSCRS ON GDSCRS.ID = GDSCR.[Status] 
		WHERE GDSCR.ExamVersionStructureID = @ExamVersionStructureID
			AND GDSCRS.Name = 'Active' AND GDSCR.Status = 0

	SELECT * FROM @Groups
	
	SELECT 
		 GDI.GroupID
		,GDI.ItemID
		,GDI.ViewOnly
		,I.ExternalItemName as ItemName
		,I.ExternalItemID
		,I.Version AS Version
		,GDI.GroupDefinitionItemOrder as GroupDefinitionItemOrder
		FROM dbo.GroupDefinitionItems GDI
			JOIN dbo.Items I ON I.ID = GDI.ItemID
			JOIN @Groups G ON G.GroupID = GDI.GroupID		
		Order by GDI.GroupDefinitionItemOrder ASC
END
GO
/****** Object:  StoredProcedure [dbo].[sm_ITEMGROUPSSERVICE_getItems_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Stored Procedure


CREATE PROCEDURE [dbo].[sm_ITEMGROUPSSERVICE_getItems_sp] 
@groupId int
AS
BEGIN	
SET NOCOUNT ON;
SELECT        Items.ExternalItemName as Name, Items.ID as ID, GroupDefinitionItems.ViewOnly as ViewOnly
FROM            GroupDefinitionItems INNER JOIN
                         Items ON GroupDefinitionItems.ItemID = Items.ID
WHERE        (GroupDefinitionItems.GroupID = @groupId)
	
END
GO
/****** Object:  StoredProcedure [dbo].[sm_ITEMGROUPSSERVICE_GetItemsByExamStructureId_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Dmitri Adov
-- Create date: 29/04/2013
-- Description:	Returns items for specified exam structure.
-- =============================================
CREATE PROCEDURE [dbo].[sm_ITEMGROUPSSERVICE_GetItemsByExamStructureId_sp]
	@ExamVersionStructureID INT
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
    SET NOCOUNT ON;
    
    SELECT EVSI.ItemID
    FROM   dbo.ExamVersionStructureItems EVSI
           INNER JOIN dbo.ExamVersionStructures EVS ON  EVS.ID = EVSI.ExamStructureID
    WHERE  EVS.ID = @ExamVersionStructureID
           AND EVS.StatusID = 0 -- 'Released'
END
GO
/****** Object:  StoredProcedure [dbo].[sm_ITEMGROUPSSERVICE_getItemsForStructure_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Anton Burdyugov
-- Create date: 17/04/2013
-- Description:	Gets the Items for a Structure ID.
-- =============================================
CREATE PROCEDURE [dbo].[sm_ITEMGROUPSSERVICE_getItemsForStructure_sp]
	-- Add the parameters for the stored procedure here
	@structureId int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	Select 
		dbo.Items.ExamGroupName as ExamGroupName
		,dbo.Items.ExamGroupID as ExamGroupID
		,dbo.Items.ID as ID
		,dbo.Items.ExternalItemID as ExternalItemID
		,dbo.Items.ExternalItemName as ExternalItemName
		,dbo.Items.Version as ItemVersion
		,dbo.Items.LayoutName as LayoutName
		,dbo.ExamSections.Name as SectionName
		,dbo.ExamSections.ID as SectionId
		,dbo.sm_getIsNewItem_fn(dbo.Items.ID) AS IsNewItem
	From 
		dbo.ExamVersionStructureItems 
		Inner join dbo.Items On dbo.ExamVersionStructureItems.ItemID = dbo.Items.ID
		Inner join dbo.ExamSectionItems  ESI On dbo.ExamVersionStructureItems.ItemID = ESI.ItemID AND ESI.ExamStructureId = @structureId
		Inner join dbo.ExamSections On ESI.ExamSectionID = dbo.ExamSections.ID
	Where 
		dbo.ExamVersionStructureItems.ExamStructureID = @structureId and dbo.ExamSections.IsDynamic = 0
    ORDER BY dbo.Items.ID
  
END
GO
/****** Object:  StoredProcedure [dbo].[sm_ITEMGROUPSSERVICE_isExamContainingDynamicSections_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Tseiko Roman
-- Create date: 2013-04-27
-- Description:	Return 'true' if ExamVersions contain GroupDefinitions
-- =============================================
CREATE PROCEDURE [dbo].[sm_ITEMGROUPSSERVICE_isExamContainingDynamicSections_sp] 
@ExamId INT
AS
BEGIN 

IF EXISTS (SELECT * 
			FROM dbo.ExamSections 
			JOIN dbo.ExamVersions ON ExamSections.ExamVersionID = ExamVersions.ID 
			JOIN dbo.Exams ON Exams.ID = ExamVersions.ExamID 
			WHERE   (dbo.ExamSections.IsDynamic = 1 and dbo.ExamVersions.ExamID= @ExamId)) 
	BEGIN
		SELECT 1;
	END
ELSE
	BEGIN
		SELECT 0;
	END	

END
GO
/****** Object:  StoredProcedure [dbo].[sm_ITEMGROUPSSERVICE_UpdateGroup_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		George Chernov
-- Create date: 25/04/13
-- Description:	Saves Groups and assigns items for group.
-- =============================================

CREATE PROCEDURE [dbo].[sm_ITEMGROUPSSERVICE_UpdateGroup_sp]
	@groupId INT,
	@groupName VARCHAR(255),
	@groupItems NVARCHAR(MAX)
AS
BEGIN
SET NOCOUNT ON;

	BEGIN

		DECLARE @TempTable Table (
			Value1 INT,
			Value2 INT,
			Value3 INT)
		
		INSERT INTO @TempTable (Value1, Value2, Value3)
		SELECT Value1, Value2, Value3
		FROM dbo.ParamsToThreeColumnTable(@groupItems)
	
		Update GroupDefinitions 
		Set Name = @groupName
		where ID = @groupId
		
		DELETE GroupDefinitionItems where GroupID = @groupId
		
		INSERT INTO GroupDefinitionItems (GroupID, ItemID, ViewOnly, GroupDefinitionItemOrder)
		SELECT @groupId, Value1, Value2, Value3
		from @TempTable
	END
END
GO
/****** Object:  StoredProcedure [dbo].[sm_ITEMGROUPSSERVICE_UpdateGroupName_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sm_ITEMGROUPSSERVICE_UpdateGroupName_sp]
	-- Add the parameters for the stored procedure here
	@GroupId int,
	@GroupName nvarchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	BEGIN TRY
		DECLARE @ReturnVal nvarchar(100)
		-- Insert statements for procedure here
		UPDATE [dbo].[GroupDefinitions]
		SET [Name] = @GroupName     
		WHERE ID = @GroupId;
		SET @ReturnVal = 'success'	
	END TRY
	BEGIN CATCH 
		SET @ReturnVal = 'UpdateGroupNameFailed' + ERROR_MESSAGE()		
	END CATCH 	
	SELECT @ReturnVal  
		
END
GO
/****** Object:  StoredProcedure [dbo].[sm_MANAGEEXAMINERS_getAssignedRolesForExam_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sm_MANAGEEXAMINERS_getAssignedRolesForExam_sp]
	-- Add the parameters for the stored procedure here
	@examID	INT
	,@maxNum		INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	SET ROWCOUNT @maxNum
	
	SELECT DISTINCT
		dbo.AssignedUserRoles.RoleId	AS RoleId
		,dbo.Roles.Name					AS RoleName
	FROM dbo.AssignedUserRoles
	INNER JOIN Roles
	ON dbo.AssignedUserRoles.RoleID = Roles.ID
	WHERE 
		dbo.AssignedUserRoles.ExamID = @examID
		AND
		dbo.Roles.TrainingRole = 0
	
	SET ROWCOUNT 0
END
GO
/****** Object:  StoredProcedure [dbo].[sm_MANAGEEXAMINERS_getAvailableMarkersForExam_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sm_MANAGEEXAMINERS_getAvailableMarkersForExam_sp]
	-- Add the parameters for the stored procedure here
	@examID			INT
	,@maxNum		INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	SET ROWCOUNT @maxNum
	
	SELECT
		dbo.Users.ID									AS 'ID'
		,dbo.Users.Forename + ' ' + dbo.Users.Surname	AS 'Fullname'
	FROM dbo.Users
	WHERE 
		dbo.Users.ID NOT IN(
			SELECT UserID
			FROM dbo.AssignedUserRoles
			WHERE AssignedUserRoles.ExamID = @examID)
		AND
		dbo.Users.Retired = 0
		AND
		(dbo.Users.InternalUser = 0
		OR
		dbo.Users.Username = 'superuser'
		)
	
	SET ROWCOUNT 0
END
GO
/****** Object:  StoredProcedure [dbo].[sm_MANAGEEXAMINERS_getMarkersForExam_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sm_MANAGEEXAMINERS_getMarkersForExam_sp]
	-- Add the parameters for the stored procedure here
	@examID	INT
	,@maxNum		INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	SET ROWCOUNT @maxNum
	
	SELECT
		dbo.Users.ID									AS ID
		,dbo.Users.Forename + ' ' +	dbo.Users.Surname	AS Fullname
	FROM dbo.Users
	WHERE 
		dbo.Users.ID IN(
			SELECT UserID
			FROM dbo.AssignedUserRoles
			WHERE AssignedUserRoles.ExamID = @examID)
		AND
		dbo.Users.Retired = 0
		AND
		(dbo.Users.InternalUser = 0		
		OR 
		dbo.Users.Username = 'superuser')
	SET ROWCOUNT 0
END
GO
/****** Object:  StoredProcedure [dbo].[sm_MANAGEEXAMINERS_getRoles_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Tom Gomersall
-- Create date: 05/05/11
-- Description:	Gets the exam version roles.
-- =============================================
CREATE PROCEDURE [dbo].[sm_MANAGEEXAMINERS_getRoles_sp]
	-- Add the parameters for the stored procedure here
 @userID INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	BEGIN TRY
	
		BEGIN TRAN
		
		CREATE TABLE #Temp_GrantableRoles
		(
			roleId INT
		)
		
		INSERT INTO #Temp_GrantableRoles
		SELECT dbo.GrantableRoles.GrantableRoleID
		FROM dbo.AssignedUserRoles
		INNER JOIN dbo.GrantableRoles
		ON dbo.GrantableRoles.RoleID = AssignedUserRoles.RoleID
		WHERE AssignedUserRoles.UserID = @userID
		
		SELECT
			dbo.Roles.ID						AS ID
			,dbo.Roles.Name						AS Name
			,dbo.Roles.AssignedByIntegration	AS AssignedByIntegration
			,(
				CASE 
				WHEN 
				(
					dbo.Roles.ID IN 
					(
						SELECT roleId
						FROM #Temp_GrantableRoles
					)
				)
				THEN 1
				ELSE 0 
				END
			)									AS Grantable
			,dbo.Roles.TrainingRole				AS TrainingRole
		FROM dbo.Roles
		WHERE 
			dbo.Roles.AssignableToExams = 1
		
		DROP TABLE #Temp_GrantableRoles
		
		COMMIT TRAN
	
	END TRY
	BEGIN CATCH
		ROLLBACK TRAN
		DECLARE @myErrorNum INT = ERROR_NUMBER()
		DECLARE @myMessage NVARCHAR(MAX) = ERROR_MESSAGE()
		DECLARE @myFullMessage NVARCHAR(MAX) = CONVERT(NVARCHAR(10), @myErrorNum) + ':' + @myMessage
		RAISERROR (@myFullMessage, 16, 1)
	END CATCH

END
GO
/****** Object:  StoredProcedure [dbo].[sm_MANAGEEXAMINERS_setTrainingModeForUsersForExam_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Tom Gomersall
-- Create date: 21/07/11
-- Description:	Gets the users for an exam version, which means all the users that have a permission for the exam.
-- =============================================

CREATE PROCEDURE [dbo].[sm_MANAGEEXAMINERS_setTrainingModeForUsersForExam_sp]

	-- Add the parameters for the stored procedure here
	 @examId		INT
	,@userIds		NVARCHAR(MAX)
	,@trainingOn	BIT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	DECLARE @myTrainingRoleId INT
	
	SELECT @myTrainingRoleId = ID
	FROM Roles
	WHERE TrainingRole = 1
		
	IF(@trainingOn = 1)
		BEGIN
			INSERT INTO AssignedUserRoles
			SELECT
				UserIDs.value
				,@myTrainingRoleId
				,@examId
			FROM dbo.ParamsToList(@userIDs)
			AS UserIDs
			WHERE NOT EXISTS(
				SELECT UserID
				FROM AssignedUserRoles
				WHERE
					UserID = UserIDs.value
					AND
					RoleID = @myTrainingRoleId
					AND
					ExamID = @examId
			)
		END
	ELSE
		BEGIN
			DELETE FROM AssignedUserRoles
			WHERE
				UserId IN (
					SELECT *
					FROM dbo.ParamsToList(@userIDs)
				)
				AND
				RoleID = @myTrainingRoleId
				AND
				ExamID = @examId
		END

END
GO
/****** Object:  StoredProcedure [dbo].[sm_MANAGEEXAMINERS_updateRolesForUsersForExam_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Tom Gomersall
-- Create date: 06/05/11
-- Description:	Adds and removes roles for users for an exam.
-- Last modified: Anna Kovru - roles are now assigned on exam, not exam version, level.
-- =============================================
CREATE PROCEDURE [dbo].[sm_MANAGEEXAMINERS_updateRolesForUsersForExam_sp]
	-- Add the parameters for the stored procedure here
	@userID INT,
	@examID INT,
	@userIDs NVARCHAR(MAX),
	@addedRoles NVARCHAR(MAX),
	@removedRoles NVARCHAR(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--First check that the user can grant the roles that are being added and removed.													
	IF((
		SELECT COUNT(Value)
		FROM [dbo].[ParamsToList](@addedRoles)
		AS addedRoles
		WHERE Value NOT IN
		(
			SELECT dbo.GrantableRoles.GrantableRoleID
			FROM dbo.GrantableRoles
			INNER JOIN dbo.AssignedUserRoles
			ON dbo.GrantableRoles.RoleID = AssignedUserRoles.RoleID
			WHERE AssignedUserRoles.UserID = @userID
			UNION
			SELECT dbo.Roles.ID
			FROM dbo.Roles
			WHERE dbo.Roles.TrainingRole = 1
		)
		) > 0)
		BEGIN
			--The user is trying to add a role that he is not allowed to grant, so an error must be thrown.
			return 50004;
		END
	ELSE
		BEGIN
			IF((
				SELECT COUNT(Value)
				FROM [dbo].[ParamsToList](@removedRoles)
				AS addedRoles
				WHERE Value NOT IN
				(
					SELECT dbo.GrantableRoles.GrantableRoleID
					FROM dbo.GrantableRoles
					INNER JOIN dbo.AssignedUserRoles
					ON dbo.GrantableRoles.RoleID = AssignedUserRoles.RoleID
					WHERE AssignedUserRoles.UserID = @userID
					UNION
					SELECT dbo.Roles.ID
					FROM dbo.Roles
					WHERE dbo.Roles.TrainingRole = 1
				)
				) > 0)
				BEGIN
					--The user is trying to remove a role that he is not allowed to grant, so an error must be thrown.
					return 50004;
				END
			ELSE
				BEGIN
					BEGIN TRY
						BEGIN TRAN
							DELETE FROM dbo.AssignedUserRoles
							WHERE
								dbo.AssignedUserRoles.ExamId = @examID
								AND
								dbo.AssignedUserRoles.UserID IN (SELECT * FROM [dbo].[ParamsToList](@userIDs))
								AND
								dbo.AssignedUserRoles.RoleID IN (SELECT * FROM [dbo].[ParamsToList](@removedRoles))
							
							INSERT INTO dbo.AssignedUserRoles	
							SELECT 
								users.Value AS UserID
								,roles.Value AS RoleID
								,@examID AS ExamID
							FROM [dbo].[ParamsToList](@userIDs)
							AS users
							INNER JOIN [dbo].[ParamsToList](@addedRoles)
							AS roles
							ON 1=1
							
						COMMIT TRAN
					END TRY
					BEGIN CATCH
						ROLLBACK TRAN

						DECLARE @myErrorNum INT = ERROR_NUMBER()
						DECLARE @myMessage NVARCHAR(MAX) = ERROR_MESSAGE()
						DECLARE @myFullMessage NVARCHAR(MAX) = CONVERT(NVARCHAR(10), @myErrorNum) + ':' + @myMessage

						RAISERROR (@myFullMessage, 16, 1)
					END CATCH
				END
		END
	
END
GO
/****** Object:  StoredProcedure [dbo].[sm_MARKINGSERVICE_AddAssignedMarksForGroupResponse_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Vitaly Bevzik
-- Create date: 24/5/2013
-- Description:	sproc to add the record to Assigned marks table based on user marking.
--				Check for the token ID was missing, so added it.			
-- =============================================
CREATE PROCEDURE [dbo].[sm_MARKINGSERVICE_AddAssignedMarksForGroupResponse_sp]

			@userId					int
           ,@uniqueGroupResponseId	bigint
           ,@assignedMark			decimal(18,10)
           ,@markingMethodId		int
           ,@markingDeviation		decimal(18,10)
           ,@tokenId				int
           ,@uniqueResponses		UniqueResponsesType1 READONLY	
           ,@groupId				int	
		   ,@parkedAnnotation       NVARCHAR(max)=NULL
           ,@reasonIds		        NVARCHAR(200)=NULL	  
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	BEGIN TRY
		BEGIN TRANSACTION

		INSERT INTO dbo.AssignedGroupMarks
		(
			UserId,
			UniqueGroupResponseId,
			Timestamp,
			AssignedMark,
			MarkingDeviation,
			MarkingMethodId,
			GroupDefinitionID			
		)
		VALUES
		(
			@userId,
			@uniqueGroupResponseId,
			GETDATE(),
			@assignedMark,
			@markingDeviation,
			@markingMethodId,
			@groupId
		)	

		DECLARE @groupMarkId BIGINT = (SELECT SCOPE_IDENTITY())
		--Insert an entry in the dbo.AssignedItemMarks.					
		INSERT INTO dbo.AssignedItemMarks
				( GroupMarkId,
				  UniqueResponseId,
				  AnnotationData,
				  AssignedMark,
				  MarkingDeviation,
				  MarkedMetadataResponse
				)
		SELECT @groupMarkId, UR.ID, UR.Annotations, UR.Mark, UR.MarkingDeviation, UR.MarkedMetadata
		FROM @uniqueResponses UR

		IF (@markingMethodId IN(4,15))
			BEGIN
				--First get the token ID for the unique response so that we can check that the user has got it checked out
				DECLARE @myUniqueGroupResponseTokenID INT

				SELECT @myUniqueGroupResponseTokenID = tokenID
				FROM dbo.UniqueGroupResponses
				WHERE ID = @uniqueGroupResponseId

				IF(@myUniqueGroupResponseTokenID IS NULL OR @tokenID <> @myUniqueGroupResponseTokenID)
					BEGIN
						ROLLBACK TRANSACTION
						--The response is not checked out to this user, so throw an error
						RETURN 50001
					END

				UPDATE dbo.UniqueGroupResponses
				SET 
					confirmedMark = @assignedMark				
				WHERE ID = @uniqueGroupResponseId

				UPDATE UGRL
				SET UGRL.ConfirmedMark= UR.Mark, UGRL.MarkedMetadataResponse= UR.MarkedMetadata
				FROM dbo.UniqueGroupResponseLinks UGRL
					INNER JOIN @uniqueResponses UR on UR.ID = UGRL.UniqueResponseId
				WHERE UGRL.UniqueGroupResponseID = @uniqueGroupResponseId


				UPDATE dbo.AssignedGroupMarks
				SET IsConfirmedMark=1
					,IsActualMarking=1
				WHERE ID = @groupMarkId

			END

		IF (@markingMethodId IN(4))
			BEGIN

				-- when user marks response from Marking the Script is considered affected for the marker
				-- therefore we add rows to AffectedCandidateExamVersions for each Script which contains current UGR
				INSERT INTO dbo.AffectedCandidateExamVersions
				(
					CandidateExamVersionId,
					UserId,
					UniqueGroupResponseId,
					GroupDefinitionId
				)
				SELECT CEV.CandidateExamVersionID, @userID, @uniqueGroupResponseId, @groupId
				FROM dbo.CandidateGroupResponses CEV
				WHERE CEV.UniqueGroupResponseID = @uniqueGroupResponseId
				AND NOT EXISTS
							(
								SELECT TOP 1 *
								FROM dbo.AffectedCandidateExamVersions ACEV
								WHERE ACEV.CandidateExamVersionID = CEV.CandidateExamVersionID
								AND ACEV.UserId = @userID
								AND ACEV.UniqueGroupResponseId = @uniqueGroupResponseId
							)
			END

		ELSE IF (@markingMethodId IN(15))
			BEGIN
				UPDATE dbo.AssignedGroupMarks
				SET ParkedAnnotation = @parkedAnnotation
				WHERE ID = @groupMarkId

				INSERT INTO dbo.AssignedGroupMarksParkedReasonCrossRef
						SELECT @groupMarkId, R.Value
						FROM dbo.ParamsToList(@reasonIds) as R
			END

		-- Check in the unique Response for the user (if it was checked out to them)
		UPDATE dbo.UniqueGroupResponses
		SET tokenId = null 
		WHERE (tokenId = @tokenId) AND (id = @uniqueGroupResponseId)

		IF (@markingMethodId IN (2,3,4))--,15
			BEGIN
				-- Update quota management table
				UPDATE dbo.QuotaManagement
				SET NumberMarked = NumberMarked + 1
				WHERE (UserId = @userId) AND GroupId = @groupId
			END

		IF (@markingMethodId IN (1,2,3,4))--,15
			BEGIN
				-- Update quota management table
				UPDATE dbo.QuotaManagement
				SET PreviousAssignedMarkId = @groupMarkId
				WHERE (UserId = @userId) AND GroupId = @groupId
			END

		IF (@markingMethodId in(4))--,15
			BEGIN
				UPDATE dbo.UserCompetenceStatus
				   SET ItemCountSinceLastCISubmitted = ItemCountSinceLastCISubmitted + 1
				 WHERE UserID = @UserId and GroupDefinitionID = @groupId
			END
		ELSE IF (@markingMethodId IN (2,3))
			BEGIN
				UPDATE dbo.UserCompetenceStatus
				   SET ItemCountSinceLastCISubmitted = 0
				 WHERE UserID = @UserId and GroupDefinitionID = @groupId
			END	


		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		DECLARE @myErrorNum INT = ERROR_NUMBER()
		DECLARE @myMessage NVARCHAR(MAX) = ERROR_MESSAGE()
		DECLARE @myFullMessage NVARCHAR(MAX) = CONVERT(NVARCHAR(10), @myErrorNum) + ':' + @myMessage

		ROLLBACK TRANSACTION

		RAISERROR (@myFullMessage, 16, 1)
	END CATCH	
END

GO
/****** Object:  StoredProcedure [dbo].[sm_MARKINGSERVICE_AddRemarkedAssignedMark_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sm_MARKINGSERVICE_AddRemarkedAssignedMark_sp]
	 @userId INT	
	,@uniqueGroupResponseId BIGINT
	,@assignedMark DECIMAL(18 ,10)
	,@uniqueResponses UniqueResponsesType1 READONLY 
	,@groupId INT
	,@parkedAnnotation NVARCHAR(MAX)=NULL
	,@reasonIds NVARCHAR(200)=NULL
AS
BEGIN
    SET NOCOUNT ON; -- added to prevent extra result sets from interfering with SELECT statements.
    BEGIN TRY
        BEGIN TRANSACTION
        
        DECLARE @markingMethodId INT=17 -- ReMark
        DECLARE @markingDeviation DECIMAL(18 ,10)=NULL
               
        --Insert an entry in the dbo.AssignedGroupMarks.
        INSERT INTO dbo.AssignedGroupMarks
          (
            UserId
           ,UniqueGroupResponseId
           ,TIMESTAMP
           ,AssignedMark
           ,MarkingDeviation
           ,MarkingMethodId
           ,GroupDefinitionID
          )
        VALUES
          (
            @userId
           ,@uniqueGroupResponseId
           ,GETDATE()
           ,@assignedMark
           ,@markingDeviation
           ,@markingMethodId
           ,@groupId
          )
        
        DECLARE @groupMarkId BIGINT=(
                    SELECT SCOPE_IDENTITY()
                )
        
        --Insert an entry in the dbo.AssignedItemMarks.					
        INSERT INTO dbo.AssignedItemMarks
          (
            GroupMarkId
           ,UniqueResponseId
           ,AnnotationData
           ,AssignedMark
           ,MarkingDeviation
           ,MarkedMetadataResponse
          )
        SELECT @groupMarkId
              ,UR.ID
              ,UR.Annotations
              ,UR.Mark
              ,UR.MarkingDeviation
              ,UR.MarkedMetadata
        FROM   @uniqueResponses UR
        
        IF EXISTS(SELECT * FROM dbo.UniqueGroupResponses ugr WHERE ugr.ID = @uniqueGroupResponseId AND ugr.IsEscalated = 1) 
        BEGIN
        	 DECLARE @myParkedUserID INT = (
				SELECT  ParkedUserID
				FROM    dbo.UniqueGroupResponses
				WHERE   ID = @uniqueGroupResponseId		
        	 )
        
        	UPDATE dbo.QuotaManagement
			SET
        	NumItemsParked = NumItemsParked - 1
			WHERE GroupId = @groupId AND UserId = @myParkedUserID
        END
        
        DELETE FROM UniqueGroupResponseParkedReasonCrossRef
        WHERE UniqueGroupResponseId = @uniqueGroupResponseId
        
        UPDATE dbo.UniqueGroupResponses
        SET    confirmedMark = @assignedMark
			   ,ParkedUserID = NULL
               ,ParkedAnnotation = NULL
               ,ParkedDate = NULL
			   ,IsEscalated = 0
        WHERE  ID = @uniqueGroupResponseId
        
        UPDATE UGRL
        SET    UGRL.ConfirmedMark = UR.Mark
              ,UGRL.MarkedMetadataResponse = UR.MarkedMetadata
        FROM   dbo.UniqueGroupResponseLinks UGRL
               INNER JOIN @uniqueResponses UR
                    ON  UR.ID = UGRL.UniqueResponseId
        WHERE  UGRL.UniqueGroupResponseID = @uniqueGroupResponseId
                
        UPDATE dbo.AssignedGroupMarks
        SET    IsConfirmedMark = 0
        WHERE  UniqueGroupResponseId = @uniqueGroupResponseId
        
        UPDATE dbo.AssignedGroupMarks
        SET    IsConfirmedMark = 1
              ,ParkedAnnotation = @parkedAnnotation
        WHERE  ID = @groupMarkId
        
        INSERT INTO dbo.AssignedGroupMarksParkedReasonCrossRef
        SELECT @groupMarkId
              ,R.Value
        FROM   dbo.ParamsToList(@reasonIds) AS R
        
        COMMIT TRANSACTION
    END TRY
    BEGIN CATCH
        DECLARE @myErrorNum INT=ERROR_NUMBER()
        DECLARE @myMessage NVARCHAR(MAX)=ERROR_MESSAGE()
        DECLARE @myFullMessage NVARCHAR(MAX)=CONVERT(NVARCHAR(10) ,@myErrorNum) 
               +':'+@myMessage
        
        ROLLBACK TRANSACTION
        
        RAISERROR (@myFullMessage ,16 ,1)
    END CATCH
END

GO
/****** Object:  StoredProcedure [dbo].[sm_MARKINGSERVICE_CheckMarkerRestrictedForUniqueGroupResponse_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sm_MARKINGSERVICE_CheckMarkerRestrictedForUniqueGroupResponse_sp]

	@uniqueGroupResponseId BIGINT,
	@userID INT
AS
BEGIN

    SET NOCOUNT ON;

    SELECT CAST((CASE WHEN COUNT(CGR.UniqueGroupResponseID) > 0 THEN 1 ELSE 0 END) AS BIT) AS IsRestricted 
    FROM dbo.CandidateGroupResponses CGR
	INNER JOIN dbo.AffectedCandidateExamVersions ACEV
	ON ACEV.CandidateExamVersionId = CGR.CandidateExamVersionID
	WHERE cgr.UniqueGroupResponseID = @uniqueGroupResponseId
	AND ACEV.UserId = @userID
	AND ACEV.GroupDefinitionID IN 
	(
		SELECT GDSCR.GroupDefinitionID 
		FROM dbo.GroupDefinitionStructureCrossRef GDSCR
			INNER JOIN dbo.ExamVersionStructures EVS ON EVS.ID = GDSCR.ExamVersionStructureID
		WHERE GDSCR.Status = 0 -- 0 = Active
			AND EVS.StatusID = 0 -- 0 = Released
	)
    
    
END


GO
/****** Object:  StoredProcedure [dbo].[sm_MARKINGSERVICE_CheckMarkerSuspendedForGroup_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Tom Gomersall
-- Create date: 24/06/2011
-- Description:	Checks whether a user is suspended for an item.
-- Author:		Anton Burdyugov
-- Create date: 31/05/2013
-- Description:	Make work with group data
-- =============================================
CREATE PROCEDURE [dbo].[sm_MARKINGSERVICE_CheckMarkerSuspendedForGroup_sp]
-- Add the parameters for the stored procedure here
	@groupDefinitionId INT ,
	@userID INT
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;
    
    IF EXISTS (
           SELECT *
           FROM   dbo.QuotaManagement
           WHERE  UserId          = @userID
                  AND GroupId     = @groupDefinitionId
       )
    BEGIN
        SELECT MarkingSuspended
        FROM   dbo.QuotaManagement
        WHERE  UserId          = @userID
               AND GroupId     = @groupDefinitionId
    END
    ELSE
    BEGIN
        SELECT CAST(0 AS BIT)
    END
END


GO
/****** Object:  StoredProcedure [dbo].[sm_MARKINGSERVICE_CheckUserIsOkayToBeMarkingThisGroup_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Dave Dixon
-- Create date: 16/06/2011
-- Description:	Checks to see if the user is suspended for the passed in item and also whether the item is released or not

-- Author:		Dave Dixon
-- Create date: 16/06/2011
-- Description:	Change ItemId to GroupDefinitionId

-- Author:		Vitaly Bevzik
-- modify date: 27/06/2013
-- Description:	Change the way we calculate @numResponsesLeftToMark

-- Author:		George George
-- Modify date: 22/04/2014
-- Description:	add only one item per script functionality

-- =============================================
CREATE PROCEDURE [dbo].[sm_MARKINGSERVICE_CheckUserIsOkayToBeMarkingThisGroup_sp]
	@userId				INT,
	@groupDefinitionId		INT,
	@trainingMode		BIT,
	@TokenStoreID		INT,
	@isMarkOneItemPerScript		BIT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	IF NOT EXISTS (SELECT TOP (1) [UserId] FROM QuotaManagement
						WHERE UserId = @userId
						AND GroupId = @groupDefinitionId)
		BEGIN
			SELECT 4
			RETURN		
		END	
				
	DECLARE @numResponsesLeftToMark	int
	
	DECLARE @IsUserHasUnrestrictedExaminerPermission BIT = 
		CASE WHEN 
			(SELECT COUNT(*)
				FROM AssignedUserRoles
				INNER JOIN RolePermissions
				ON AssignedUserRoles.RoleID = RolePermissions.RoleID
				WHERE AssignedUserRoles.UserID = @userId AND RolePermissions.PermissionID = 42) > 0 
			THEN 1 
			ELSE 0 
		END


	
	IF(@isMarkOneItemPerScript = 1 AND @IsUserHasUnrestrictedExaminerPermission = 0)
		BEGIN	
			SELECT  @numResponsesLeftToMark = COUNT(UGR.[id])
				FROM    UniqueGroupResponses UGR
							INNER JOIN GroupDefinitions GD ON UGR.GroupDefinitionID = GD.ID
				WHERE   ( UGR.TokenId IS NULL
							  OR UGR.TokenId = @TokenStoreID) 
							-- unecessary check as we are passing in item id which determines this 
							AND UGR.IsEscalated = 0
							AND 
								(
									UGR.[confirmedmark] IS NULL
									OR
									@trainingMode = 1
								)
							AND UGR.CI = 0
							AND GD.ID = @groupDefinitionId
							AND UGR.ID NOT IN 
							(
								SELECT CGR1.UniqueGroupResponseID
								FROM CandidateGroupResponses CGR1
								INNER JOIN UniqueGroupResponses UGR2
								ON UGR2.ID = CGR1.UniqueGroupResponseID
								INNER JOIN
								(	-- all Scripts which contain current group and affected by current user 
									SELECT DISTINCT ACEV.CandidateExamVersionId AS AffectedScript
									FROM UniqueGroupResponses UGR1
									INNER JOIN CandidateGroupResponses CGR
									ON CGR.UniqueGroupResponseID = UGR1.ID
									INNER JOIN AffectedCandidateExamVersions ACEV
									ON ACEV.CandidateExamVersionId = CGR.CandidateExamVersionID
									WHERE UGR1.GroupDefinitionID = GD.ID
									AND ACEV.UserId = @userId
									AND ACEV.GroupDefinitionID IN 
									(
										SELECT GDSCR.GroupDefinitionID 
										FROM dbo.GroupDefinitionStructureCrossRef GDSCR
											INNER JOIN dbo.ExamVersionStructures EVS ON EVS.ID = GDSCR.ExamVersionStructureID
										WHERE GDSCR.Status = 0 -- 0 = Active
											AND EVS.StatusID = 0 -- 0 = Released
									)
								) AS AffectedScripts
								ON CGR1.CandidateExamVersionID = AffectedScripts.AffectedScript
								WHERE UGR2.GroupDefinitionID = GD.ID
							)
		END
	ELSE
		BEGIN
			SELECT  @numResponsesLeftToMark = COUNT(UGR.[id])
				FROM    UniqueGroupResponses UGR
							INNER JOIN GroupDefinitions GD ON UGR.GroupDefinitionID = GD.ID
				WHERE   ( UGR.TokenId IS NULL
							  OR UGR.TokenId = @TokenStoreID) 
							-- unecessary check as we are passing in item id which determines this 
							AND UGR.IsEscalated = 0
							AND 
								(
									UGR.[confirmedmark] IS NULL
									OR
									@trainingMode = 1
								)
							AND UGR.CI = 0
							AND GD.ID = @groupDefinitionId
		END
                            
	IF (@numResponsesLeftToMark < 1)
			BEGIN
			SELECT 5
			RETURN
		END		
				
	DECLARE @markingSuspended bit
	DECLARE @numMarked int
	DECLARE @assignedQuota	int

	SELECT 
		@markingSuspended=MarkingSuspended, 
		@numMarked=NumberMarked, 
		@assignedQuota=AssignedQuota
	FROM QuotaManagement
	WHERE 
		UserId = @userId
		AND 
		GroupId = @groupDefinitionId
		
	IF (@trainingMode = 1)
		BEGIN
			SET @markingSuspended = 0
		END

	IF (@numMarked >= @assignedQuota AND @assignedQuota != -1)
		BEGIN
			SELECT 3
			RETURN
		END
		
	IF (@markingSuspended = 1)
		BEGIN
			SELECT 1
			RETURN
		END
		
	DECLARE @groupReleased bit
			
	SELECT @groupReleased = ISNULL(IsReleased, 0)
	FROM GroupDefinitions
	WHERE ID = @groupDefinitionId

	IF (@groupReleased != 1)
		BEGIN
			SELECT 2
			RETURN
		END
	
	-- if we're here, we're good to mark!
	SELECT 0
 
END
GO
/****** Object:  StoredProcedure [dbo].[sm_MARKINGSERVICE_DeleteAssignedGroupMark_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Vitaly Bevzik
-- Create date: 24/05/2013
-- Description:	sproc to get the competence for the user w.r.t. group
-- =============================================
CREATE PROCEDURE [dbo].[sm_MARKINGSERVICE_DeleteAssignedGroupMark_sp]

	@assignedGroupMarkId bigint

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	DECLARE @markingMethodId	int
	DECLARE @uniqueGroupResponseId	bigint
	DECLARE @userId				bigint
	DECLARE @isConfirmedMark	bit

	SELECT @markingMethodId=MarkingMethodId,
			@uniqueGroupResponseId = UniqueGroupResponseId,
			@isConfirmedMark = IsConfirmedMark,
			@userId = userId
	FROM AssignedGroupMarks
	WHERE id = @assignedGroupMarkId
	
	DECLARE @groupId int
		
	SELECT @groupId = GroupDefinitions.ID
	FROM GroupDefinitions
	INNER JOIN [UniqueGroupResponses] ON
		[UniqueGroupResponses].GroupDefinitionID = GroupDefinitions.ID
	WHERE [UniqueGroupResponses].ID = @uniqueGroupResponseId
	
	IF (@markingMethodId = 3) 
		BEGIN
			-- set competency to undetermined
			UPDATE UserCompetenceStatus
			SET CompetenceStatus = 1
			WHERE	UserID = @userId
			AND		GroupDefinitionID = @groupId
		END
		
	IF (@isConfirmedMark = 1) 
		BEGIN
			-- set confirmedMark to null
			UPDATE UniqueGroupResponses
			SET confirmedMark = NULL
			WHERE id = @uniqueGroupResponseId

			UPDATE UniqueGroupResponseLinks
			SET confirmedMark = NULL
			WHERE UniqueGroupResponseID = @uniqueGroupResponseId
		END
	
	--Decrement the number marked because the item is being 'un-marked'
	IF (@markingMethodId <> 1)
	BEGIN
		UPDATE QuotaManagement
			SET NumberMarked = NumberMarked - 1
			WHERE UserId = @userId AND GroupId = @groupId
	END
		
	DELETE FROM AffectedCandidateExamVersions
	WHERE UniqueGroupResponseId = @uniqueGroupResponseId
	AND UserId = @userId
		
	-- delete row from AssignedItemMarks and AssignedGroupMarks tables
	DELETE FROM AssignedItemMarks
	WHERE GroupMarkId = @assignedGroupMarkId
	
	DELETE FROM AssignedGroupMarks
	WHERE id = @assignedGroupMarkId
	    
END
GO
/****** Object:  StoredProcedure [dbo].[sm_MARKINGSERVICE_FILTER_GetExams_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sm_MARKINGSERVICE_FILTER_GetExams_sp] 

-- Add the parameters for the stored procedure here
	@userId INT,
	@tokenStoreId INT,
	@defaultMarkingRestriction BIT,
	@maxNum INT
AS
BEGIN

    SET NOCOUNT ON;
    
    SELECT DISTINCT TOP(@maxNum) 
           T.Exam    AS 'Exam'
          ,T.ExamId  AS 'ExamID'
    FROM   dbo.sm_MARKINGSERVICE_GetAllGroupsForUser_fn(@userId, @tokenStoreId, @defaultMarkingRestriction) T
END
GO
/****** Object:  StoredProcedure [dbo].[sm_MARKINGSERVICE_FILTER_GetItems_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sm_MARKINGSERVICE_FILTER_GetItems_sp] 

-- Add the parameters for the stored procedure here
	@userId INT,
	@tokenStoreId INT,
	@defaultMarkingRestriction BIT,
	@maxNum INT
AS
BEGIN

    SET NOCOUNT ON;
    
    SELECT DISTINCT TOP(@maxNum) 
           T.ItemName    AS 'ItemName'
          ,T.ID  AS 'ID'
    FROM   dbo.sm_MARKINGSERVICE_GetAllGroupsForUser_fn(@userId, @tokenStoreId, @defaultMarkingRestriction) T
END
GO
/****** Object:  StoredProcedure [dbo].[sm_MARKINGSERVICE_FILTER_GetMarked_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sm_MARKINGSERVICE_FILTER_GetMarked_sp] 

-- Add the parameters for the stored procedure here
	@userId INT,
	@tokenStoreId INT,
	@defaultMarkingRestriction BIT
AS
BEGIN

    SET NOCOUNT ON;
    
    SELECT MAX(T.Marked)     AS 'MaxMarked'
          ,MIN(T.Marked)     AS 'MinMarked'
    FROM   dbo.sm_MARKINGSERVICE_GetAllGroupsForUser_fn(@userId, @tokenStoreId, @defaultMarkingRestriction) T
    
END
GO
/****** Object:  StoredProcedure [dbo].[sm_MARKINGSERVICE_FILTER_GetNumToMark_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sm_MARKINGSERVICE_FILTER_GetNumToMark_sp] 

-- Add the parameters for the stored procedure here
	@userId INT,
	@tokenStoreId INT,
	@defaultMarkingRestriction BIT
AS
BEGIN

    SET NOCOUNT ON;
    
    SELECT MAX(T.RemainingQuota)     AS 'MaxNumToMark'
          ,MIN(T.RemainingQuota)     AS 'MinNumToMark'
    FROM   dbo.sm_MARKINGSERVICE_GetAllGroupsForUser_fn(@userId, @tokenStoreId, @defaultMarkingRestriction) T
    WHERE T.RemainingQuota >= 0 -- we shouldn't get unlimited quotas, because we shouldn't show '-1' at the filter
    
END
GO
/****** Object:  StoredProcedure [dbo].[sm_MARKINGSERVICE_FILTER_GetQualifications_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sm_MARKINGSERVICE_FILTER_GetQualifications_sp] 

-- Add the parameters for the stored procedure here
	@userId INT,
	@tokenStoreId INT,
	@defaultMarkingRestriction BIT,
	@maxNum INT
AS
BEGIN

    SET NOCOUNT ON;
    
    SELECT DISTINCT TOP(@maxNum) 
           T.Qualification    AS 'Qualification'
          ,T.QualificationID  AS 'QualificationID'
    FROM   dbo.sm_MARKINGSERVICE_GetAllGroupsForUser_fn(@userId, @tokenStoreId, @defaultMarkingRestriction) T
END
GO
/****** Object:  StoredProcedure [dbo].[sm_MARKINGSERVICE_FILTER_GetResponsesLeftToMark_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sm_MARKINGSERVICE_FILTER_GetResponsesLeftToMark_sp] 

-- Add the parameters for the stored procedure here
	@userId INT,
	@tokenStoreId INT,
	@defaultMarkingRestriction BIT
AS
BEGIN

    SET NOCOUNT ON;
    
    SELECT MAX(T.UGRsAvailableToMarkCount)     AS 'MaxNumToMark'
          ,MIN(T.UGRsAvailableToMarkCount)     AS 'MinNumToMark'
    FROM   dbo.sm_MARKINGSERVICE_GetAllGroupsForUser_fn(@userId, @tokenStoreId, @defaultMarkingRestriction) T
    
END
GO
/****** Object:  StoredProcedure [dbo].[sm_MARKINGSERVICE_GetCountOfCIsMarkedInCompetencyRun_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Rathin Sundar	
-- Create date: 05/04/2011
-- Description:	sproc that gives the count of CIs marked on a particular day,
--              This could be used for competency run check.
-- Author:		Vitaly Bevzik	
-- Modify date: 24/5/2013
-- Description:	Change Itemid to GroupId
-- =============================================
CREATE PROCEDURE [dbo].[sm_MARKINGSERVICE_GetCountOfCIsMarkedInCompetencyRun_sp]
	@userId INT,
	@groupId INT
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;
    
    SELECT COUNT(*)
    FROM   dbo.AssignedGroupMarks AGM
           INNER JOIN dbo.UniqueGroupResponses UGR ON  AGM.UniqueGroupResponseId = UGR.id
    WHERE  AGM.userId = @userId
           AND UGR.GroupDefinitionID = @groupId
           AND AGM.markingMethodId = 3
           AND (DATEDIFF(MINUTE ,AGM.timestamp ,GETDATE())<=1440)
           AND AGM.disregarded = 0
END
GO
/****** Object:  StoredProcedure [dbo].[sm_MARKINGSERVICE_GetCountOfCIsMarkedOutOfTolSinceLastCI_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sm_MARKINGSERVICE_GetCountOfCIsMarkedOutOfTolSinceLastCI_sp]
    @userId INT ,
    @groupId INT
AS 
    BEGIN
        SET NOCOUNT ON;
			
				-- calculate the last datetime a CI was marked within tolerance by this user for this item
        DECLARE @timeForLastCI DATETIME
				
        SELECT TOP 1
                @timeForLastCI = [timestamp]
        FROM    [dbo].[AssignedGroupMarks] AGM
                INNER JOIN [dbo].[UniqueGroupResponses] UGR ON AGM.UniqueGroupResponseId = UGR.[id]
                INNER JOIN [dbo].GroupDefinitions GD ON UGR.GroupDefinitionID = GD.[ID]
        WHERE   AGM.userId = @userId
                AND UGR.GroupDefinitionID = @groupId
                AND AGM.markingMethodId IN ( 2, 3 )
                AND AGM.disregarded = 0
                AND dbo.sm_checkGroupAndItemsMarkedInTolerance_fn(AGM.id,
                                                              UGR.id,
                                                              AGM.MarkingDeviation,
                                                              GD.CIMarkingTolerance, GD.IsAutomaticGroup) = 0
        ORDER BY [timestamp] DESC					
						
        SELECT  COUNT(*)
        FROM    [dbo].[AssignedGroupMarks] AGM
                INNER JOIN [dbo].[UniqueGroupResponses] UGR ON AGM.UniqueGroupResponseId = UGR.[id]
                INNER JOIN [dbo].GroupDefinitions GD ON UGR.GroupDefinitionID = GD.[ID]
        WHERE   AGM.userId = @userId
                AND AGM.markingMethodId IN ( 2, 3 )
                AND UGR.GroupDefinitionID = @groupId
                AND AGM.[timestamp] > ISNULL(@timeForLastCI, '01/01/1900')
                AND AGM.disregarded = 0
                AND dbo.sm_checkGroupAndItemsMarkedInTolerance_fn(AGM.id,
                                                              UGR.id,
                                                              AGM.MarkingDeviation,
                                                              GD.CIMarkingTolerance, Gd.IsAutomaticGroup) = 1	
    END
GO
/****** Object:  StoredProcedure [dbo].[sm_MARKINGSERVICE_GetCountOfCIsMarkedOutOfTolWithinRollingReview_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sm_MARKINGSERVICE_GetCountOfCIsMarkedOutOfTolWithinRollingReview_sp]
    @userId INT ,
    @groupId INT ,
    @numberofItemsWithinRollingReview INT
AS 
    BEGIN
        SET NOCOUNT ON;
			

        SELECT  COUNT(x.[markingDeviation])
        FROM    ( SELECT TOP ( @numberofItemsWithinRollingReview )
                            agm.ID AS [AgmId] ,
                            UGR.ID AS [UGRId] ,
                            [markingDeviation] ,
                            [CIMarkingTolerance],
                            GD.IsAutomaticGroup
                  FROM      [dbo].[AssignedGroupMarks] AGM
                            INNER JOIN [dbo].[UniqueGroupResponses] UGR ON AGM.UniqueGroupResponseId = UGR.[id]
                            INNER JOIN [dbo].GroupDefinitions GD ON UGR.GroupDefinitionID = GD.[ID]
                  WHERE     AGM.userId = @userId
                            AND AGM.markingMethodId IN ( 2, 3 )
                            AND UGR.GroupDefinitionID = @groupId
                            AND AGM.disregarded = 0
                  ORDER BY Timestamp DESC
                ) x
        WHERE   dbo.sm_checkGroupAndItemsMarkedInTolerance_fn([AgmId], [UGRId],
                                                              [markingDeviation],
                                                              [CIMarkingTolerance], IsAutomaticGroup) = 1
					

	
	   
    END
GO
/****** Object:  StoredProcedure [dbo].[sm_MARKINGSERVICE_GetCountOfCIsMarkedWithinToleranceInCompetencyRun_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Dave Dixon
-- Create date: 16/06/2011
-- Description:	sproc that gives the count of CIs marked within tolerance 
--				in the competency CIs marked within the last 24 hours
-- Author:		Vitaly BEvzik
-- Create date: 24/5/2013
-- Description:	Changed to use with groups
-- =============================================
CREATE PROCEDURE [dbo].[sm_MARKINGSERVICE_GetCountOfCIsMarkedWithinToleranceInCompetencyRun_sp]
	@userId INT,
	@groupId INT
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;	
     
    SELECT COUNT(*)
    FROM   dbo.AssignedGroupMarks AGM
           INNER JOIN dbo.UniqueGroupResponses UGR ON  AGM.UniqueGroupResponseId = UGR.ID
           INNER JOIN dbo.GroupDefinitions GD ON  GD.ID = UGR.GroupDefinitionID
    WHERE  AGM.userId = @userId
           AND AGM.markingMethodId = 3
           AND UGR.GroupDefinitionID = @groupId
           AND (DATEDIFF(MINUTE ,AGM.timestamp ,GETDATE())<=1440)
           AND AGM.markingDeviation<= GD.CIMarkingTolerance
           AND AGM.disregarded = 0
           AND dbo.sm_checkGroupAndItemsMarkedInTolerance_fn(
                   AGM.id
                  ,UGR.id
                  ,AGM.MarkingDeviation
                  ,GD.CIMarkingTolerance
                  ,GD.IsAutomaticGroup
               ) = 0
END
GO
/****** Object:  StoredProcedure [dbo].[sm_MARKINGSERVICE_GetCumulativeErrorOfCIsMarkedWithinCompetencyRun_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sm_MARKINGSERVICE_GetCumulativeErrorOfCIsMarkedWithinCompetencyRun_sp]	
	@userId		int,
	@groupId	int	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;	 
	SELECT SUM([markingDeviation]) 
	FROM dbo.[AssignedGroupMarks] AGM
	INNER JOIN dbo.[UniqueGroupResponses] UGR ON AGM.UniqueGroupResponseId = UGR.[id]
	INNER JOIN dbo.GroupDefinitions GD ON GD.ID = UGR.GroupDefinitionID
	WHERE 
			AGM.userId = @userId
		and	AGM.markingMethodId = 3
		and UGR.GroupDefinitionID = @groupId
		and (DATEDIFF(MINUTE, AGM.[Timestamp],GetDate()) <= 1440)
		and AGM.Disregarded = 0						
END
GO
/****** Object:  StoredProcedure [dbo].[sm_MARKINGSERVICE_GetExamAnnotationSchema_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Tom Gomersall
-- Create date: 22/03/2011
-- Description:	Gets the annotation schema for an exam
-- =============================================
CREATE PROCEDURE [dbo].[sm_MARKINGSERVICE_GetExamAnnotationSchema_sp]
	-- Add the parameters for the stored procedure here
	@examID int

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @CurDate date = GetDate()
	
	SELECT ISNULL(e.AnnotationSchema, '')
	FROM dbo.Exams e
	WHERE e.ID = @examID  
  
END

GO
/****** Object:  StoredProcedure [dbo].[sm_MARKINGSERVICE_GetExamIDForAssignedMark_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Tom Gomersall
-- Create date: 27/06/2011
-- Description:	Gets the exam ID for an assigned mark.
-- Modified:	Anton Burdyugov
-- Date:		31/05/2013
-- Description:	Make work with group level data.
-- =============================================
CREATE PROCEDURE [dbo].[sm_MARKINGSERVICE_GetExamIDForAssignedMark_sp]
	-- Add the parameters for the stored procedure here
    @assignedGroupMarkID INT
AS 
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON;
	
        SELECT  GD.ExamID
        FROM    dbo.GroupDefinitions GD
                INNER JOIN dbo.AssignedGroupMarks AGM ON AGM.GroupDefinitionID = GD.ID
        WHERE   AGM.id = @assignedGroupMarkID
		AND EXISTS (SELECT * FROM dbo.GroupDefinitionStructureCrossRef CR WHERE cr.GroupDefinitionID = GD.ID AND CR.Status = 0)
    END

GO
/****** Object:  StoredProcedure [dbo].[sm_MARKINGSERVICE_GetExamIDForGroup_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Vitaly Bevzik
-- Create date: 6/7/2013
-- Description:	Gets the exam ID for a group. 
-- =============================================
CREATE PROCEDURE [dbo].[sm_MARKINGSERVICE_GetExamIDForGroup_sp]
	@groupID int	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT GD.ExamID
	FROM GroupDefinitions GD 
	WHERE GD.id = @groupID  
	AND EXISTS (SELECT * FROM GroupDefinitionStructureCrossRef CR WHERE cr.GroupDefinitionID = GD.ID AND CR.Status = 0)
END

GO
/****** Object:  StoredProcedure [dbo].[sm_MARKINGSERVICE_GetExamIDForUniqueGroupResponse_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Tseyko Roman
-- Create date: 2013-06-05
-- Description:	Get exam ID by UniqueGroupResponse ID
-- =============================================
CREATE PROCEDURE [dbo].[sm_MARKINGSERVICE_GetExamIDForUniqueGroupResponse_sp]
    @UniqueGroupResponseID INT
AS 
    BEGIN
        SET NOCOUNT ON;
	
        SELECT GD.ExamID
        FROM dbo.UniqueGroupResponses UGR
             INNER JOIN dbo.GroupDefinitions GD ON UGR.GroupDefinitionID = GD.ID
        WHERE  UGR.ID = @UniqueGroupResponseID  
        AND EXISTS (SELECT * FROM dbo.GroupDefinitionStructureCrossRef CR WHERE cr.GroupDefinitionID = GD.ID AND CR.Status = 0)
    END

GO
/****** Object:  StoredProcedure [dbo].[sm_MARKINGSERVICE_GetExamIDForUniqueResponse_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Tom Gomersall
-- Create date: 07/04/2011
-- Description:	Gets the exam version ID for a unique response.
-- =============================================
CREATE PROCEDURE [dbo].[sm_MARKINGSERVICE_GetExamIDForUniqueResponse_sp]
	-- Add the parameters for the stored procedure here
	@responseID int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	SELECT I.ExamID
	FROM dbo.UniqueResponses UR
	INNER JOIN dbo.Items I ON UR.itemId = I.ID
	WHERE UR.id = @responseID
  
END
GO
/****** Object:  StoredProcedure [dbo].[sm_MARKINGSERVICE_GetMarkedResponseForMarkedCI_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sm_MARKINGSERVICE_GetMarkedResponseForMarkedCI_sp]
-- Add the parameters for the stored procedure here
	@assignedGroupMarkId INT
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;
    
    DECLARE @uniqueGroupResponseId INT
    SET @uniqueGroupResponseId = (
            SELECT TOP 1
                   AGM.UniqueGroupResponseId
            FROM   dbo.AssignedGroupMarks AGM
            WHERE  AGM.ID = @assignedGroupMarkID
        )
    
    --Select Group information
    SELECT TOP 1
           GD.ID                  AS 'ID'
          ,GD.TotalMark           AS 'TotalMark'
          ,GD.Name                AS 'Name'
          ,AGM.AssignedMark       AS 'AssignedMark'
          ,ISNULL(
               UGR.confirmedMark
              ,(
                   SELECT TOP(1) assignedMark
                   FROM   dbo.AssignedGroupMarks AM
                   WHERE  (AM.UniqueGroupResponseId=UGR.id)
                          AND (markingMethodId=6)
                   ORDER BY
                          TIMESTAMP DESC
               )
           )                      AS 'ConfirmedMark'
          ,GD.CIMarkingTolerance  AS 'Tolerance'
          ,ISNULL(E.AnnotationSchema ,'') AS 'AnnotationSchema'
          ,CEV.ScriptType         AS 'ScriptType'
          ,UGR.ID                 AS 'UniqueGroupResponseId'
          ,dbo.sm_checkGroupAndItemsMarkedInTolerance_fn(
               AGM.id
              ,UGR.id
              ,AGM.MarkingDeviation
              ,GD.CIMarkingTolerance
              ,GD.IsAutomaticGroup
           )                      AS 'IsOutOfTolerance'
          ,AGM.IsEscalated        AS 'IsEscalated'
          ,AGM.MarkingDeviation   AS 'MarkingDeviation'
          ,UGR.Feedback           AS 'Feedback'
    FROM   dbo.UniqueGroupResponses UGR
           INNER JOIN dbo.GroupDefinitions GD ON  GD.ID = UGR.GroupDefinitionID
           INNER JOIN dbo.AssignedGroupMarks AGM ON  AGM.UniqueGroupResponseId = UGR.ID
           INNER JOIN dbo.Exams E ON  E.ID = GD.ExamID
           INNER JOIN dbo.CandidateGroupResponses CGR ON  CGR.UniqueGroupResponseID = UGR.ID
           INNER JOIN dbo.CandidateExamVersions CEV ON  CEV.ID = CGR.CandidateExamVersionID
    WHERE  AGM.ID = @assignedGroupMarkID
           AND AGM.markingMethodId IN (2 ,3)
    
    -- Select unique responses				
    SELECT UR.id                         AS 'id'
          ,UR.responseData               AS 'response'
          ,AIM.assignedMark              AS 'assignedMark'
          ,AIM.annotationData            AS 'annotationData'
          ,AIM.markedMetadataResponse    AS 'markedMetadataResponse'
          ,I.ID                          AS 'ItemId'
          ,I.Version                     AS 'ItemVersion'
          ,I.LayoutName                  AS 'LayoutName'
          ,I.AllowAnnotations            AS 'AllowAnnotations'
          ,I.TotalMark                   AS 'TotalMark'
          ,I.ExternalItemID              AS 'ExternalID'
          ,GDI.ViewOnly                  AS 'ViewOnly'
          ,I.CIMarkingTolerance          AS 'MarkingTolerance'
          ,L.confirmedMark               AS 'ConfirmedMark'
          ,AIM.MarkingDeviation          AS 'MarkingDeviation'
    FROM   dbo.UniqueGroupResponses UGR
           INNER JOIN dbo.UniqueGroupResponseLinks L ON  UGR.Id = L.UniqueGroupResponseID
           INNER JOIN dbo.UniqueResponses UR ON  UR.id = L.UniqueResponseId
           INNER JOIN dbo.Items I ON  i.id = UR.itemId
           INNER JOIN dbo.GroupDefinitionItems GDI ON  GDI.GroupID = UGR.GroupDefinitionID
                    AND GDI.ItemID = UR.itemId
           LEFT JOIN dbo.AssignedItemMarks AIM ON  AIM.UniqueResponseId = UR.id
                    AND AIM.GroupMarkId = @assignedGroupMarkId
    WHERE  UGR.ID = @uniqueGroupResponseId
    ORDER BY
           GDI.GroupDefinitionItemOrder  ASC
END

GO
/****** Object:  StoredProcedure [dbo].[sm_MARKINGSERVICE_GetMarkingExaminerReportCounters_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Kirill Nevedrov
-- Date:		02/03/2015
-- Description:	Gets the information about how many Markers marked available Exams and how many available Qualifications, Exams, Exam Versions, Items was marked.
-- =============================================

CREATE PROCEDURE [dbo].[sm_MARKINGSERVICE_GetMarkingExaminerReportCounters_sp]
	-- Add the parameters for the stored procedure here
	@fromDate DATETIME,
	@toDate DATETIME,
	@userId INT
AS 
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
    SET NOCOUNT ON;

	DECLARE @examIds TABLE (ExamId INT)
	INSERT INTO @examIds
	SELECT ExamId FROM dbo.AssignedUserRoles AUR
		INNER JOIN dbo.RolePermissions RP ON AUR.RoleID = RP.RoleID
		WHERE RP.PermissionID = 53 AND AUR.UserID = @userId
		
	DECLARE @isGlobal BIT = 0
	if (EXISTS(SELECT ExamId FROM @examIds WHERE ExamId = 0))
		SET @isGlobal = 1

	DECLARE @examVersions INT
	SELECT @examVersions = COUNT (DISTINCT CEV.[ExamVersionId])				   
    FROM   dbo.MarkingReport_view  MRV
    INNER JOIN dbo.CandidateGroupResponses CGR ON (CGR.[UniqueGroupResponseID] = MRV.[UniqueGroupResponseId])
    INNER JOIN dbo.CandidateExamVersions CEV ON (CEV.[ID] = CGR.[CandidateExamVersionID])
	WHERE  
		(MRV.[Timestamp] BETWEEN @fromDate AND @toDate) AND 
		(@isGlobal = 1 OR MRV.[ExamId] IN (SELECT EI.[ExamId] FROM @examIds EI))

	SELECT COUNT (DISTINCT MRV.[UserId])					AS 'Users'
          ,COUNT (DISTINCT MRV.[QualificationId])			AS 'Qualifications'
          ,COUNT (DISTINCT MRV.[ExamId])					AS 'Exams' 
          ,@examVersions									AS 'ExamVersions'
          ,COUNT (DISTINCT MRV.[ID])						AS 'Items'
          ,SUM (MRV.[UniqueResponse])						AS 'UniqueResponsesMarked'
          ,SUM (MRV.[ControlItem])							AS 'CIsMarked'							
    FROM   dbo.MarkingReport_view  MRV
    WHERE  
		(MRV.[Timestamp] BETWEEN @fromDate AND @toDate) AND 
		(@isGlobal = 1 OR MRV.[ExamId] IN (SELECT EI.[ExamId] FROM @examIds EI))
		AND MRV.EscalatedWithoutMark = 0	
END  
GO
/****** Object:  StoredProcedure [dbo].[sm_MARKINGSERVICE_GetMarkingProgressData_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Anton Burdyugov
-- Date:		20/06/2013
-- Description:	Gets the information about how many Unique Responses and Control Items across all Exam Versions was marked.
-- =============================================

CREATE PROCEDURE [dbo].[sm_MARKINGSERVICE_GetMarkingProgressData_sp]
	-- Add the parameters for the stored procedure here
	@fromDate DATETIME,
	@toDate DATETIME,
	@userId INT
AS 
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON;
	
        SELECT  COUNT(AGM.GroupDefinitionID) AS TotalMarked ,
				ISNULL(SUM(
						CASE
							WHEN AGM.MarkingMethodId IN (2, 3) THEN 1
							ELSE 0
						END
						), 0) AS TotalCGsMarked
        FROM    dbo.AssignedGroupMarks AGM
				JOIN dbo.GroupDefinitionItems GDI 
					ON (GDI.GroupId = AGM.GroupDefinitionID AND GDI.ViewOnly = 0)
        WHERE   AGM.GroupDefinitionID > 0
                AND AGM.UserId = @userId
                AND Timestamp BETWEEN @fromDate AND @toDate
				AND GDI.ViewOnly = 0
                AND ((AGM.MarkingMethodId = 4 AND AGM.IsActualMarking = 1)
					OR (AGM.MarkingMethodId IN (2, 3, 10))
					OR (AGM.MarkingMethodId = 11 AND AGM.EscalatedWithoutMark = 0)
				    OR (AGM.MarkingMethodId = 5 AND AGM.IsReportableCI = 1))
			   AND EXISTS (
					   SELECT TOP 1*
					   FROM   dbo.GroupDefinitionStructureCrossRef CR
							  INNER JOIN dbo.ExamVersionStructures EVS
								   ON  CR.ExamVersionStructureID = EVS.ID
					   WHERE  CR.GroupDefinitionID = GDI.GroupID
							  AND CR.Status IN (0 ,2)
							  AND EVS.StatusID = 0
				   )
    END
GO
/****** Object:  StoredProcedure [dbo].[sm_MARKINGSERVICE_GetPreviousMarkedResponse_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Tom Gomersall	
-- Create date: 15/07/2011
-- Description:	Gets the last marked response for the user for an item.
-- Modified:	Tom Gomersall
-- Date:		22/11/2011
-- Description:	Set to undo check-out for any other responses for the token, and then check out the response that is being returned.
-- Modified		Tom Gomersall
-- Date:		04/09/2012
-- Description:	Added markedMetadataResponse to the select.
-- Modified		Vitaly Bevzik
-- Date:		28/03/2013
-- Description:	Added ScriptType
-- Modified		Vitaly Bevzik
-- Date:		28/03/2013
-- Description:	Changed to work with group
-- =============================================
CREATE PROCEDURE [dbo].[sm_MARKINGSERVICE_GetPreviousMarkedResponse_sp]
	-- Add the parameters for the stored procedure here
	@userId		INT,
	@groupId	INT,
	@tokenId	INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    DECLARE @myAssignedMarkId BIGINT    
    SELECT @myAssignedMarkId = PreviousAssignedMarkId
    FROM dbo.QuotaManagement Q
    WHERE
		UserId = @userId
		AND
		GroupId = @groupId
		
	DECLARE @myUniqueGroupResponseId BIGINT
	
	SELECT @myUniqueGroupResponseId = UniqueGroupResponseId
	FROM dbo.AssignedGroupMarks AGM
	WHERE ID = @myAssignedMarkId
	
	--Check out the previous item
	UPDATE dbo.UniqueGroupResponses 
	SET tokenID = @tokenID
	WHERE ID = @myUniqueGroupResponseId    
    -- Select Unique Group Response level data --------------------------------------------
	SELECT 
		 UGR.ID					AS [ID]
		,@myAssignedMarkId							AS AssigneMarkID
		,GD.ID						AS [GroupId]
		,GD.Name						AS [GroupName]
		,GD.TotalMark					AS [TotalMark]
		,UGR.ScriptType
		
		
	FROM dbo.UniqueGroupResponses UGR
	INNER JOIN dbo.GroupDefinitions GD ON GD.ID = UGR.GroupDefinitionID	
	WHERE UGR.ID = @myUniqueGroupResponseId

	----------------------------------------------------------------------------------------
	-- Select Unique Response level data ---------------------------------------------
	SELECT 
		I.ID								AS ItemId,
		I.[ExternalItemName]				AS ItemName, 
		I.[ExternalItemID]					AS ExternalItemID, 
		I.[Version]							AS ItemVersion, 
		I.[LayoutName]						AS LayoutName, 
		I.[TotalMark]						AS TotalMark, 				
		I.[MarkingType]						AS MarkingType,
		I.[AllowAnnotations]				AS AllowAnnotations,
		AIM.AnnotationData					AS AnnotationData,
		AIM.AssignedMark					AS AssignedMark,
		AIM.MarkedMetadataResponse			AS MarkedMetadata,
		UR.[id]								AS responseID, 
		UR.[responseData]					AS ResponseData,				
		GDI.ViewOnly						AS ViewOnly
	FROM dbo.UniqueGroupResponseLinks UGRL
	INNER JOIN dbo.UniqueResponses UR ON UR.id = UGRL.UniqueResponseId
	INNER JOIN dbo.[Items] I ON UR.itemId = I.ID
	INNER JOIN dbo.GroupDefinitionItems GDI on GDI.ItemID = UR.itemId AND GDI.GroupID = @groupId
	LEFT JOIN dbo.AssignedItemMarks AIM ON AIM.UniqueResponseId = UR.id AND AIM.GroupMarkId = @myAssignedMarkId
	WHERE UGRL.UniqueGroupResponseID = @myUniqueGroupResponseId		
	ORDER BY GDI.GroupDefinitionItemOrder
	
END
GO
/****** Object:  StoredProcedure [dbo].[sm_MARKINGSERVICE_GetRecentlyMarkedCIsForGroup_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Vitaly Bevzik
-- Create date: 30/05/2013
-- Description:	Gets the recently marked CIs for a user for a group. 

-- Author:		George Chernov
-- Modify date: 9/7/2013
-- Description:	Add return IsEscalated
-- =============================================

CREATE PROCEDURE [dbo].[sm_MARKINGSERVICE_GetRecentlyMarkedCIsForGroup_sp]
	-- Add the parameters for the stored procedure here
    @groupID INT,
    @userID INT
AS 
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON;
	
    --First get the number of items in the rolling review and the competency check for the item.
        DECLARE @myNumItemsInRollingReview INT
        DECLARE @myNumItemsInCompetencyCheck INT
	  
        SELECT  @myNumItemsInRollingReview = GD.NumberOfCIsInRollingReview,
                @myNumItemsInCompetencyCheck = GD.TotalItemsInCompetencyRun
        FROM    dbo.GroupDefinitions GD
        WHERE   GD.ID = @groupID
	  
        IF ( @myNumItemsInRollingReview > @myNumItemsInCompetencyCheck ) 
            BEGIN
                SET ROWCOUNT @myNumItemsInRollingReview
            END
        ELSE 
            BEGIN
                SET ROWCOUNT @myNumItemsInCompetencyCheck
            END
	  
        SELECT  UniqueGroupResponseID,
                timestamp,
                assignedMark,
                confirmedMark,
                TotalMark,
                CIMarkingTolerance,
                AssignedMarkID,
                MarkingMethod,
                IsOutOfTolerance,
                IsEscalated,
                MarkingDeviation
        FROM    dbo.MarkedCIGroups_view
        WHERE   UserId = @userId
                AND GroupID = @groupID
        ORDER BY timestamp DESC
	  
        SET ROWCOUNT 0                  
    END
GO
/****** Object:  StoredProcedure [dbo].[sm_MARKINGSERVICE_GetUserCompetence_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Batch submitted through debugger: SQLQuery8.sql|7|0|C:\Users\RathinS\AppData\Local\Temp\~vsF16E.sql
-- =============================================
-- Author:		Rathin Sundar
-- Create date: 09/03/2011
-- Description:	sproc to get the competence for the user w.r.t. item
-- Author:		Vitaly Bevzik
-- Modify date: 24/5/2013
-- Description:	changed item to group
-- =============================================
CREATE PROCEDURE [dbo].[sm_MARKINGSERVICE_GetUserCompetence_sp]
	-- Add the parameters for the stored procedure here
	@UserID int,
	@GroupID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT U.[Datecertified],
		   U.[Competencestatus],
		   U.[Itemcountsincelastcisubmitted]
	FROM   [dbo].[Usercompetencestatus] U
	WHERE  U.[Userid] = @UserID
		   AND U.[Groupdefinitionid] = @GroupID   
END
GO
/****** Object:  StoredProcedure [dbo].[sm_MARKINGSERVICE_GetUserCompetenceAndGroupResponse_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- =============================================
-- Author:		Vitaly Bevzik
-- Create date: 09/03/2011
-- Description:	sproc to get the competence for the user w.r.t. group

-- Author:		George George
-- Modify date: 22/04/2014
-- Description:	add only one item per script functionality

-- Author:		Alexey Kharitonov
-- Modify date: 29/05/2014
-- Description:	added flag CILimitAchieved - the number of CIs for group is exceeded
-- =============================================

CREATE PROCEDURE [dbo].[sm_MARKINGSERVICE_GetUserCompetenceAndGroupResponse_sp]
-- Add the parameters for the stored procedure here
	@myUserID INT,
	@myGroupID INT,
	@myTokenStoreID INT,
	@myCompetencyCheckInterval INT,
	@trainingMode BIT,
	@isMarkOneItemPerScript BIT,
	@randomPercentage DECIMAL
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;
    
    DECLARE @CountSinceLastCI INT=0
    DECLARE @MaxResBeforeCI INT=0
    
    --Get the user's quota and number marked for the group.
    DECLARE @myNumMarkedByUser INT
    DECLARE @myUserQuota INT
    
    SELECT @myNumMarkedByUser = QM.NumberMarked
          ,@myUserQuota = QM.AssignedQuota
    FROM   dbo.QuotaManagement QM
    WHERE  QM.GroupId = @myGroupID
           AND QM.UserID = @myUserID
    
    DECLARE @myUniqueGroupResponseId BIGINT
    DECLARE @myMarkedMetadata XML
    
    -- Unique responses of group response
    DECLARE @groupUniqueResponses TABLE (
                UniqueResponseID BIGINT
               ,MarkedMetadata XML
               ,IsFilled BIT NOT NULL DEFAULT 0
            )
    -- @currentId is used for loop to get MarkedMetadata
    DECLARE @currentId BIGINT
    
    IF (@trainingMode=1)
    BEGIN
        --The marker has the training permission, so select a marked response at random
        
        SELECT TOP 1
               @myUniqueGroupResponseId = UGR.id
        FROM   dbo.UniqueGroupResponses UGR
               INNER JOIN dbo.GroupDefinitions GD ON  UGR.GroupDefinitionID = GD.ID
        WHERE  GD.ID = @myGroupID
        ORDER BY NEWID()
        
        INSERT INTO @groupUniqueResponses
          (
            UniqueResponseID
          )
        SELECT ID
        FROM   dbo.UniqueResponses UR
        WHERE  ID IN (SELECT UniqueResponseId
                      FROM   dbo.UniqueGroupResponseLinks
                      WHERE  UniqueGroupResponseID = @myUniqueGroupResponseId)
        
        --Update MarkScheme
        UPDATE dbo.UniqueResponses
        SET    markSchemeId = (
                   SELECT TOP 1 MS.id
                   FROM   dbo.Items I
                          INNER JOIN dbo.MarkSchemes MS ON  MS.ExternalItemID = I.ExternalItemID
                   WHERE  I.ID = UniqueResponses.itemId
                          AND MS.[version] = (
                                  SELECT MAX(MS1.[version])
                                  FROM   dbo.MarkSchemes MS1
                                  WHERE  MS1.ExternalItemID = MS.ExternalItemID
                              )
               )
        WHERE  UniqueResponses.ID IN (SELECT UniqueResponseId FROM @groupUniqueResponses)
        
        -- Select Unique Group Response level data --------------------------------------------
        SELECT 'Passed-ResCheckedOut' AS     Competency
              ,UGR.ID                     AS ID
              ,@myNumMarkedByUser         AS NumMarked
              ,@myUserQuota               AS UserQuota
              ,QM.PreviousAssignedMarkId  AS PreviousAssignedMarkId
              ,UGR.ScriptType             AS ScriptType
              ,GD.ID                      AS GroupId
              ,GD.Name                    AS GroupName
              ,GD.TotalMark               AS TotalMark
              ,dbo.sm_checkGroupCIsExceeded_fn(GD.ID) AS CILimitAchieved
              ,Feedback                   AS Feedback
        FROM   dbo.UniqueGroupResponses UGR
               INNER JOIN dbo.GroupDefinitions GD ON  GD.ID = UGR.GroupDefinitionID
               INNER JOIN dbo.QuotaManagement QM ON  QM.GroupId = UGR.GroupDefinitionID
                        AND QM.UserId = @myUserID
        WHERE  UGR.ID = @myUniqueGroupResponseId
        ----------------------------------------------------------------------------------------
        
        -- Get empty marked metadata for each UniqueResponse in Group response------------------	
        SET @currentId = (
                SELECT TOP 1 UniqueResponseID
                FROM   @groupUniqueResponses
                WHERE  IsFilled = 0
            )
        
        WHILE @currentId IS NOT NULL
        BEGIN
            EXEC dbo.sm_SHARED_removeMarksFromMarkedMetadata_sp 
                 @uniqueResponseId=@currentId
                ,@updateTable=0
                ,@newMarkedMetadata=@myMarkedMetadata OUTPUT
            
            UPDATE @groupUniqueResponses
            SET    MarkedMetadata = @myMarkedMetadata
                  ,IsFilled = 1
            WHERE  UniqueResponseID = @currentId
            
            SET @currentId = (
                    SELECT TOP 1 UniqueResponseID
                    FROM   @groupUniqueResponses
                    WHERE  IsFilled = 0
                )
        END
        ----------------------------------------------------------------------------------------			
        
        -- Select Unique Response level data ---------------------------------------------
        SELECT I.ID                AS ItemId
              ,I.ExternalItemName  AS ItemName
              ,I.ExternalItemID    AS ExternalItemID
              ,I.[Version]         AS ItemVersion
              ,I.LayoutName        AS LayoutName
              ,I.TotalMark         AS TotalMark
              ,I.MarkingType       AS MarkingType
              ,I.AllowAnnotations  AS AllowAnnotations
              ,GUR.MarkedMetadata  AS MarkedMetadata
              ,UR.id               AS responseID
              ,UR.responseData     AS ResponseData
              ,GDI.ViewOnly        AS ViewOnly
        FROM   dbo.UniqueResponses UR
               INNER JOIN dbo.Items I ON  UR.itemId = I.ID
               INNER JOIN @groupUniqueResponses GUR ON  GUR.UniqueResponseID = UR.ID
               INNER JOIN dbo.GroupDefinitionItems GDI ON  GDI.ItemID = UR.itemId
                        AND GDI.GroupID = @myGroupID
        ORDER BY GDI.GroupDefinitionItemOrder
               ----------------------------------------------------------------------------------------
    END
    ELSE
    BEGIN
        -- Not training mode
        DECLARE @numberOfCIsAvailable INT
        -- calculate the number of CIs available for this group
        SELECT @numberOfCIsAvailable = COUNT(*)
        FROM   dbo.GroupDefinitions GD
               INNER JOIN dbo.UniqueGroupResponses UGR
                    ON  UGR.GroupDefinitionID = GD.ID
        WHERE  GD.ID = @myGroupID
               AND UGR.CI_Review = 1
               AND UGR.CI = 1
        
        -- first we need to determine if the user is currently competent or not at marking this item
        DECLARE @myCompetentUserID INT
        SELECT @myCompetentUserID = UserID
        FROM   dbo.UserCompetenceStatus
        WHERE  UserID                    = @myUserID
               AND GroupDefinitionID     = @myGroupID
               AND DATEDIFF(MINUTE ,DateCertified ,GETDATE())<= @myCompetencyCheckInterval
               AND CompetenceStatus = 2
        
        -- if there are no CIs, we must assume user is competent
        IF ((@numberOfCIsAvailable=0) AND (@myCompetentUserID IS NULL))
        BEGIN
            UPDATE dbo.UserCompetenceStatus
            SET    CompetenceStatus = 2
                  ,DateCertified = GETDATE()
                  ,ItemCountSinceLastCISubmitted = 0
            WHERE  UserID = @myUserID
                   AND GroupDefinitionID = @myGroupID
            
            -- Check to see if UPDATE happened, if it didn't then we need to INSERT
            -- as examiner is new to marking this item
            IF @@ROWCOUNT=0
            BEGIN
                INSERT INTO dbo.UserCompetenceStatus
                  (
                    UserID
                   ,GroupDefinitionID
                   ,DateCertified
                   ,CompetenceStatus
                   ,ItemCountSinceLastCISubmitted
                  )
                VALUES
                  (
                    @myUserID
                   ,@myGroupID
                   ,GETDATE()
                   ,2
                   ,0
                  )
            END
            
            -- re-get competent used id now that we have just populated it!
            SELECT @myCompetentUserID = UserID
            FROM   dbo.UserCompetenceStatus
            WHERE  UserID                    = @myUserID
                   AND GroupDefinitionID     = @myGroupID
                   AND DATEDIFF(MINUTE ,DateCertified ,GETDATE())<= @myCompetencyCheckInterval
                   AND CompetenceStatus = 2
        END
        
        
        -- check if we have a competent user or not
        IF (@myCompetentUserID IS NOT NULL)
        BEGIN
            -- user is existing and competant right now so check Random CI & send the unique response item.		           
            DECLARE @ProbGetCI DECIMAL
            -- set @ProbGetCI to value specified against the item
            SELECT @ProbGetCI = ProbabilityOfGettingCI
            FROM   dbo.GroupDefinitions
            WHERE  ID = @myGroupID
            
            IF ( @randomPercentage < @ProbGetCI AND @numberOfCIsAvailable > 0 )
            BEGIN
                --Get the unique group response ID
                SELECT TOP 1
                       @myUniqueGroupResponseId = UGR.id
                FROM   dbo.UniqueGroupResponses UGR
                       INNER JOIN dbo.GroupDefinitions GDI ON  UGR.GroupDefinitionID = GDI.ID
                WHERE  GDI.ID = @myGroupID
                       AND UGR.CI_Review = 1
                       AND UGR.CI = 1
                ORDER BY NEWID()
                
                -- Get UniqueResponses Ids for group response
                INSERT INTO @groupUniqueResponses
                  (
                    UniqueResponseID
                  )
                SELECT UR.ID
                FROM   dbo.UniqueResponses UR
                WHERE  UR.ID IN (SELECT UniqueResponseId
                                 FROM   dbo.UniqueGroupResponseLinks
                                 WHERE  UniqueGroupResponseID = @myUniqueGroupResponseId)
                
                -- Select Unique Group Response level data --------------------------------------------
                SELECT 'Passed-RandomCheck' AS Competency
                      ,UGR.ID          AS ID
                      ,@myNumMarkedByUser AS NumMarked
                      ,@myUserQuota    AS UserQuota
                      ,QM.PreviousAssignedMarkId AS PreviousAssignedMarkId
                      ,UGR.ScriptType  AS ScriptType
                      ,GD.ID           AS GroupId
                      ,GD.Name         AS GroupName
                      ,GD.TotalMark    AS TotalMark
                      ,dbo.sm_checkGroupCIsExceeded_fn(GD.ID) AS CILimitAchieved
                      ,Feedback        AS Feedback
                FROM   dbo.UniqueGroupResponses UGR
                       INNER JOIN dbo.GroupDefinitions GD ON  GD.ID = UGR.GroupDefinitionID
                       INNER JOIN dbo.QuotaManagement QM ON  QM.GroupId = UGR.GroupDefinitionID
                                AND QM.UserId = @myUserID
                WHERE  UGR.ID = @myUniqueGroupResponseId
                ----------------------------------------------------------------------------------------
                
                -- Get empty marked metadata for each UniqueResponse in Group response------------------	
                SET @currentId = (
                        SELECT TOP 1 UniqueResponseID
                        FROM   @groupUniqueResponses
                        WHERE  IsFilled = 0
                    )
                
                WHILE @currentId IS NOT NULL
                BEGIN
                    EXEC dbo.sm_SHARED_removeMarksFromMarkedMetadata_sp 
                         @uniqueResponseId=@currentId
                        ,@updateTable=0
                        ,@newMarkedMetadata=@myMarkedMetadata OUTPUT
                    
                    UPDATE @groupUniqueResponses
                    SET    MarkedMetadata = @myMarkedMetadata
                          ,IsFilled = 1
                    WHERE  UniqueResponseID = @currentId
                    
                    SET @currentId = (
                            SELECT TOP 1 UniqueResponseID
                            FROM   @groupUniqueResponses
                            WHERE  IsFilled = 0
                        )
                END
                ----------------------------------------------------------------------------------------			
                
                -- Select Unique Response level data ---------------------------------------------
                SELECT I.ID              AS ItemId
                      ,I.ExternalItemName AS ItemName
                      ,I.ExternalItemID  AS ExternalItemID
                      ,I.[Version]       AS ItemVersion
                      ,I.LayoutName      AS LayoutName
                      ,I.TotalMark       AS TotalMark
                      ,I.MarkingType     AS MarkingType
                      ,I.AllowAnnotations AS AllowAnnotations
                      ,GUR.MarkedMetadata AS MarkedMetadata
                      ,UR.id             AS responseID
                      ,UR.responseData   AS ResponseData
                      ,GDI.ViewOnly      AS ViewOnly
                FROM   dbo.UniqueResponses UR
                       INNER JOIN dbo.Items I ON  UR.itemId = I.ID
                       INNER JOIN @groupUniqueResponses GUR ON  GUR.UniqueResponseID = UR.ID
                       INNER JOIN dbo.GroupDefinitionItems GDI ON  GDI.ItemID = UR.itemId
                                AND GDI.GroupID = @myGroupID
                ORDER BY GDI.GroupDefinitionItemOrder
                       ----------------------------------------------------------------------------------------
            END
            ELSE
            BEGIN
                -- probability says no to random CI, so check to see if we need to send a mandatory one!
                SET @CountSinceLastCI = (
                        SELECT TOP 1 ucs.ItemCountSinceLastCISubmitted
                        FROM   dbo.UserCompetenceStatus ucs
                        WHERE  ucs.UserID = @myUserID
                               AND ucs.GroupDefinitionID = @myGroupID
                    ) 
                
                SET @MaxResBeforeCI = (
                        SELECT MaximumResponseBeforePresentingCI
                        FROM   dbo.GroupDefinitions
                        WHERE  ID = @myGroupID
                    )									
                
                
                IF ( @CountSinceLastCI >= @MaxResBeforeCI AND @numberOfCIsAvailable > 0 )
                BEGIN
                    -- send mandatory CI as one hasn't been sent in a while
                    SELECT TOP 1
                           @myUniqueGroupResponseId = UGR.id
                    FROM   dbo.UniqueGroupResponses UGR
                           INNER JOIN dbo.GroupDefinitions GD ON  GD.ID = UGR.GroupDefinitionID
                    WHERE  GD.ID = @myGroupID
                           AND UGR.CI_Review = 1
                           AND UGR.CI = 1
                    ORDER BY NEWID(); 
                    
                    -- Get UniqueResponses Ids for group response
                    INSERT INTO @groupUniqueResponses
                      (
                        UniqueResponseID
                      )
                    SELECT ID
                    FROM   dbo.UniqueResponses UR
                    WHERE  UR.ID IN (SELECT UniqueResponseId
                                     FROM   dbo.UniqueGroupResponseLinks
                                     WHERE  UniqueGroupResponseID = @myUniqueGroupResponseId)
                    
                    -- Select Unique Group Response level data --------------------------------------------
                    SELECT 'Passed-MaxResCheck' AS Competency
                          ,UGR.ID        AS ID
                          ,@myNumMarkedByUser AS NumMarked
                          ,@myUserQuota  AS UserQuota
                          ,QM.PreviousAssignedMarkId AS PreviousAssignedMarkId
                          ,UGR.ScriptType AS ScriptType
                          ,GD.ID         AS GroupId
                          ,GD.Name       AS GroupName
                          ,GD.TotalMark  AS TotalMark
                          ,dbo.sm_checkGroupCIsExceeded_fn(GD.ID) AS 
                           CILimitAchieved
                          ,Feedback      AS Feedback
                    FROM   dbo.UniqueGroupResponses UGR
                           INNER JOIN dbo.GroupDefinitions GD ON  GD.ID = UGR.GroupDefinitionID
                           INNER JOIN dbo.QuotaManagement QM ON  QM.GroupId = UGR.GroupDefinitionID
                                    AND QM.UserId = @myUserID
                    WHERE  UGR.ID = @myUniqueGroupResponseId
                    ----------------------------------------------------------------------------------------
                    
                    -- Get empty marked metadata for each UniqueResponse in Group response------------------	
                    SET @currentId = (
                            SELECT TOP 1 UniqueResponseID
                            FROM   @groupUniqueResponses
                            WHERE  IsFilled = 0
                        )
                    
                    WHILE @currentId IS NOT NULL
                    BEGIN
                        EXEC dbo.sm_SHARED_removeMarksFromMarkedMetadata_sp 
                             @uniqueResponseId=@currentId
                            ,@updateTable=0
                            ,@newMarkedMetadata=@myMarkedMetadata OUTPUT
                        
                        UPDATE @groupUniqueResponses
                        SET    MarkedMetadata = @myMarkedMetadata
                              ,IsFilled = 1
                        WHERE  UniqueResponseID = @currentId
                        
                        SET @currentId = (
                                SELECT TOP 1 UniqueResponseID
                                FROM   @groupUniqueResponses
                                WHERE  IsFilled = 0
                            )
                    END
                    ----------------------------------------------------------------------------------------			
                    
                    -- Select Unique Response level data ---------------------------------------------
                    SELECT I.ID           AS ItemId
                          ,I.ExternalItemName AS ItemName
                          ,I.ExternalItemID AS ExternalItemID
                          ,I.[Version]    AS ItemVersion
                          ,I.LayoutName   AS LayoutName
                          ,I.TotalMark    AS TotalMark
                          ,I.MarkingType  AS MarkingType
                          ,I.AllowAnnotations AS AllowAnnotations
                          ,GUR.MarkedMetadata AS MarkedMetadata
                          ,UR.id          AS responseID
                          ,UR.responseData AS ResponseData
                          ,GDI.ViewOnly   AS ViewOnly
                    FROM   dbo.UniqueResponses UR
                           INNER JOIN dbo.Items I ON  UR.itemId = I.ID
                           INNER JOIN @groupUniqueResponses GUR ON  GUR.UniqueResponseID = UR.ID
                           INNER JOIN dbo.GroupDefinitionItems GDI ON  GDI.ItemID = UR.itemId
                                    AND GDI.GroupID = @myGroupID
                    ORDER BY GDI.GroupDefinitionItemOrder
                           ----------------------------------------------------------------------------------------
                END
                ELSE
                BEGIN
                    -- no CI required to be sent to examiner, so choose a new random unique response for them to mark
                    -- Set the token ID of the unique group response to check it out, and also set the mark scheme ID
                    BEGIN TRANSACTION
                    DECLARE @myUniqueGroupResponseIdTable TABLE (ID INT)
                    
                    DECLARE @IsUserHasUnrestrictedExaminerPermission BIT=
                            CASE 
                                 WHEN (
                                          SELECT COUNT(*)
                                          FROM   dbo.AssignedUserRoles AUR
                                                 INNER JOIN dbo.RolePermissions 
                                                      RP
                                                      ON  AUR.RoleID = RP.RoleID
                                          WHERE  AUR.UserID = @myUserID
                                                 AND RP.PermissionID = 42
                                      )>0 THEN 1
                                 ELSE 0
                            END
                    
                    -- if @isMarkOneItemPerScript feature is ON
                    IF ( @isMarkOneItemPerScript=1 AND @IsUserHasUnrestrictedExaminerPermission=0 )
                    BEGIN
                        UPDATE dbo.UniqueGroupResponses
                        SET    TokenId = @myTokenStoreID
                               OUTPUT INSERTED.Id INTO @myUniqueGroupResponseIdTable
                        WHERE  dbo.UniqueGroupResponses.ID = (
                                   SELECT TOP(1) UGR.ID
                                   FROM   dbo.UniqueGroupResponses UGR WITH (UPDLOCK)
                                   WHERE  UGR.GroupDefinitionID = @myGroupID
                                          AND (UGR.TokenId IS NULL OR UGR.TokenId=@myTokenStoreID)
                                          AND UGR.ConfirmedMark IS NULL
                                          AND UGR.ParkedUserID IS NULL
                                          AND UGR.CI = 0
                                          AND UGR.ID NOT IN (-- all UGRs for particular group in scripts affected by current user 
                                                            SELECT UGR1.ID
                                                            FROM   dbo.UniqueGroupResponses UGR1
                                                                   INNER JOIN dbo.CandidateGroupResponses CGR ON  CGR.UniqueGroupResponseID = UGR1.ID
                                                                   INNER JOIN dbo.AffectedCandidateExamVersions ACEV ON  ACEV.CandidateExamVersionId = CGR.CandidateExamVersionID
                                                            WHERE  UGR1.GroupDefinitionID = @myGroupID
                                                                   AND ACEV.UserId = @myUserID
                                                                   AND ACEV.GroupDefinitionID IN (
                                                                   		SELECT GDSCR.GroupDefinitionID
                                                                        FROM dbo.GroupDefinitionStructureCrossRef GDSCR
                                                                        INNER JOIN dbo.ExamVersionStructures EVS ON EVS.ID = GDSCR.ExamVersionStructureID
                                                                        WHERE GDSCR.Status = 0 -- 0 = Active
                                                                                AND EVS.StatusID = 0 -- 0 = Released
                                                                        )
															)
                                   ORDER BY
                                          VIP DESC, insertionDate ASC
                               )
                    END
                    ELSE
                    BEGIN
                        UPDATE dbo.UniqueGroupResponses
                        SET    TokenId = @myTokenStoreID
                               OUTPUT INSERTED.Id INTO @myUniqueGroupResponseIdTable
                        WHERE  dbo.UniqueGroupResponses.ID = (
                                   SELECT TOP(1) UGR.ID
                                   FROM   dbo.UniqueGroupResponses UGR WITH (UPDLOCK)
                                   WHERE  UGR.GroupDefinitionID = @myGroupID
                                          AND (UGR.TokenId IS NULL OR UGR.TokenId=@myTokenStoreID)
                                          AND UGR.ConfirmedMark IS NULL
                                          AND UGR.ParkedUserID IS NULL
                                          AND UGR.CI = 0
                                   ORDER BY
                                          VIP DESC, insertionDate ASC
                               )
                    END
                    
                    SELECT TOP 1 @myUniqueGroupResponseId = ID
                    FROM   @myUniqueGroupResponseIdTable
                    
                    COMMIT TRANSACTION
                    -------------------------------------------------------
                    --************************************************
                    INSERT INTO @groupUniqueResponses
                      (
                        UniqueResponseID
                      )
                    SELECT ID
                    FROM   dbo.UniqueResponses UR
                    WHERE  UR.ID IN (SELECT UniqueResponseId
                                     FROM   dbo.UniqueGroupResponseLinks
                                     WHERE  UniqueGroupResponseID = @myUniqueGroupResponseId)
                    
                    --Update MarkScheme
                    UPDATE dbo.UniqueResponses
                    SET    markSchemeId = (
                               SELECT TOP 1 MS.ID
                               FROM   dbo.Items I
                                      INNER JOIN dbo.MarkSchemes MS ON  MS.ExternalItemID = I.ExternalItemID
                               WHERE  I.ID = UniqueResponses.itemId
                                      AND MS.[version] = (
                                              SELECT MAX([version])
                                              FROM   dbo.MarkSchemes MS1
                                              WHERE  MS1.ExternalItemID = 
                                                     MS.ExternalItemID
                                          )
                           )
                    WHERE  UniqueResponses.ID IN (SELECT UniqueResponseId
                                                  FROM   @groupUniqueResponses)
                    
                    -- Select Unique Group Response level data --------------------------------------------
                    SELECT 'Passed-ResCheckedOut' AS Competency
                          ,UGR.ID        AS ID
                          ,@myNumMarkedByUser AS NumMarked
                          ,@myUserQuota  AS UserQuota
                          ,QM.PreviousAssignedMarkId AS PreviousAssignedMarkId
                          ,UGR.ScriptType AS ScriptType
                          ,GD.ID         AS GroupId
                          ,GD.Name       AS GroupName
                          ,GD.TotalMark  AS TotalMark
                          ,dbo.sm_checkGroupCIsExceeded_fn(GD.ID) AS 
                           CILimitAchieved
                          ,Feedback      AS Feedback
                    FROM   dbo.UniqueGroupResponses UGR
                           INNER JOIN dbo.GroupDefinitions GD ON  GD.ID = UGR.GroupDefinitionID
                           INNER JOIN dbo.QuotaManagement QM ON  QM.GroupId = UGR.GroupDefinitionID
                                    AND QM.UserId = @myUserID
                    WHERE  UGR.ID = @myUniqueGroupResponseId
                    ----------------------------------------------------------------------------------------
                    
                    -- Get empty marked metadata for each UniqueResponse in Group response------------------	
                    SET @currentId = (
                            SELECT TOP 1 UniqueResponseID
                            FROM   @groupUniqueResponses
                            WHERE  IsFilled = 0
                        )
                    
                    WHILE @currentId IS NOT NULL
                    BEGIN
                        EXEC dbo.sm_SHARED_removeMarksFromMarkedMetadata_sp 
                             @uniqueResponseId=@currentId
                            ,@updateTable=0
                            ,@newMarkedMetadata=@myMarkedMetadata OUTPUT
                        
                        UPDATE @groupUniqueResponses
                        SET    MarkedMetadata = @myMarkedMetadata
                              ,IsFilled = 1
                        WHERE  UniqueResponseID = @currentId
                        
                        SET @currentId = (
                                SELECT TOP 1 UniqueResponseID
                                FROM   @groupUniqueResponses
                                WHERE  IsFilled = 0
                            )
                    END
                    ----------------------------------------------------------------------------------------			
                    
                    -- Select Unique Response level data ---------------------------------------------
                    SELECT I.ID           AS ItemId
                          ,I.ExternalItemName AS ItemName
                          ,I.ExternalItemID AS ExternalItemID
                          ,I.[Version]    AS ItemVersion
                          ,I.LayoutName   AS LayoutName
                          ,I.TotalMark    AS TotalMark
                          ,I.MarkingType  AS MarkingType
                          ,I.AllowAnnotations AS AllowAnnotations
                          ,GUR.MarkedMetadata AS MarkedMetadata
                          ,UR.id          AS responseID
                          ,UR.responseData AS ResponseData
                          ,GDI.ViewOnly   AS ViewOnly
                    FROM   dbo.UniqueResponses UR
                           INNER JOIN dbo.Items I ON  UR.itemId = I.ID
                           INNER JOIN @groupUniqueResponses GUR ON  GUR.UniqueResponseID = UR.ID
                           INNER JOIN dbo.GroupDefinitionItems GDI ON  GDI.ItemID = UR.itemId
                                    AND GDI.GroupID = @myGroupID
                    ORDER BY GDI.GroupDefinitionItemOrder
                           ----------------------------------------------------------------------------------------
                END
            END
        END
        ELSE
            -- and if the user was incompetent they are either
            -- 1. new to marking this item
            -- 2. passed, but not in the last 24 hours
            -- 3. undetermined
        BEGIN
            -- user has passed but not in last 24 hour so update to be undetermined
            UPDATE dbo.UserCompetenceStatus
            SET    CompetenceStatus = 1
                  ,DateCertified = GETDATE()
                  ,ItemCountSinceLastCISubmitted = 0
            WHERE  UserID = @myUserID
                   AND GroupDefinitionID = @myGroupID
            
            -- Check to see if UPDATE happened, if it didn't then we need to INSERT
            -- as examiner is new to marking this item
            IF @@ROWCOUNT=0
            BEGIN
                INSERT INTO dbo.UserCompetenceStatus
                  (
                    UserID
                   ,GroupDefinitionID
                   ,DateCertified
                   ,CompetenceStatus
                   ,ItemCountSinceLastCISubmitted
                  )
                VALUES
                  (
                    @myUserID
                   ,@myGroupID
                   ,GETDATE()
                   ,1
                   ,0
                  )
            END
            
            -- we now know that the record exists for this user/item combination
            -- in the UserCompetenceStatustable as undetermined
            
            -- user is not competent today so send the control item.
            
            --*******************************************************************
            SELECT @myUniqueGroupResponseId = UGR.id
            FROM   dbo.UniqueGroupResponses UGR
                   INNER JOIN dbo.GroupDefinitions GD ON  GD.ID = UGR.GroupDefinitionID
            WHERE  GD.ID = @myGroupID
                   AND UGR.CI_Review = 1
                   AND UGR.CI = 1
            ORDER BY NEWID(); 
            
            -- Get UniqueResponses Ids for group response
            INSERT INTO @groupUniqueResponses
              (
                UniqueResponseID
              )
            SELECT UR.ID
            FROM   dbo.UniqueResponses UR
            WHERE  UR.ID IN (SELECT UniqueResponseId
                             FROM   dbo.UniqueGroupResponseLinks
                             WHERE  UniqueGroupResponseID = @myUniqueGroupResponseId)
            
            -- Select Unique Group Response level data --------------------------------------------
            SELECT 'Undetermined' AS      Competency
                  ,UGR.ID              AS ID
                  ,@myNumMarkedByUser  AS NumMarked
                  ,@myUserQuota        AS UserQuota
                  ,QM.PreviousAssignedMarkId AS PreviousAssignedMarkId
                  ,UGR.ScriptType      AS ScriptType
                  ,GD.ID               AS GroupId
                  ,GD.Name             AS GroupName
                  ,GD.TotalMark        AS TotalMark
                  ,dbo.sm_checkGroupCIsExceeded_fn(GD.ID) AS CILimitAchieved
                  ,UGR.Feedback        AS Feedback
            FROM   dbo.UniqueGroupResponses UGR
                   INNER JOIN dbo.GroupDefinitions GD ON  GD.ID = UGR.GroupDefinitionID
                   INNER JOIN dbo.QuotaManagement QM ON  QM.GroupId = UGR.GroupDefinitionID
                            AND QM.UserId = @myUserID
            WHERE  UGR.ID = @myUniqueGroupResponseId
            ----------------------------------------------------------------------------------------
            
            -- Get empty marked metadata for each UniqueResponse in Group response------------------	
            SET @currentId = (
                    SELECT TOP 1 UniqueResponseID
                    FROM   @groupUniqueResponses
                    WHERE  IsFilled = 0
                )
            
            WHILE @currentId IS NOT NULL
            BEGIN
                EXEC dbo.sm_SHARED_removeMarksFromMarkedMetadata_sp 
                     @uniqueResponseId=@currentId
                    ,@updateTable=0
                    ,@newMarkedMetadata=@myMarkedMetadata OUTPUT
                
                UPDATE @groupUniqueResponses
                SET    MarkedMetadata = @myMarkedMetadata
                      ,IsFilled = 1
                WHERE  UniqueResponseID = @currentId
                
                SET @currentId = (
                        SELECT TOP 1 UniqueResponseID
                        FROM   @groupUniqueResponses
                        WHERE  IsFilled = 0
                    )
            END
            ----------------------------------------------------------------------------------------			
            
            -- Select Unique Response level data ---------------------------------------------
            SELECT I.ID                AS ItemId
                  ,I.ExternalItemName  AS ItemName
                  ,I.ExternalItemID    AS ExternalItemID
                  ,I.[Version]         AS ItemVersion
                  ,I.LayoutName        AS LayoutName
                  ,I.TotalMark         AS TotalMark
                  ,I.MarkingType       AS MarkingType
                  ,I.AllowAnnotations  AS AllowAnnotations
                  ,GUR.MarkedMetadata  AS MarkedMetadata
                  ,UR.id               AS responseID
                  ,UR.responseData     AS ResponseData
                  ,GDI.ViewOnly        AS ViewOnly
            FROM   dbo.UniqueResponses UR
                   INNER JOIN dbo.Items I ON  UR.itemId = I.ID
                   INNER JOIN @groupUniqueResponses GUR ON  GUR.UniqueResponseID = UR.ID
                   INNER JOIN dbo.GroupDefinitionItems GDI ON  GDI.ItemID = UR.itemId
                            AND GDI.GroupID = @myGroupID
            ORDER BY GDI.GroupDefinitionItemOrder
                   ----------------------------------------------------------------------------------------
        END
    END
END
GO
/****** Object:  StoredProcedure [dbo].[sm_MARKINGSERVICE_GetValuesForMarkedGroupResponse_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Vitaly Bevzik
-- Create date: 24/5/2013
-- Description:	sproc to get values needed for competency run calculation
--when a item table id and and uniqueresponse table id are passed in
-- =============================================
CREATE PROCEDURE [dbo].[sm_MARKINGSERVICE_GetValuesForMarkedGroupResponse_sp]
	@uniqueGroupResponseId BIGINT
AS
BEGIN
    SET NOCOUNT ON;
    
    --Get group level info
    SELECT UGR.Id												AS 'UNIQUEGROUPRESPONSEID'
          ,UGR.Confirmedmark									AS 'Confirmedmark'
          ,UGR.Ci_review                  						AS 'Ci_review'
          ,GD.Id                          						AS 'GROUPID'
          ,GD.Noofcisrequired             						AS 'Noofcisrequired'
          ,GD.Cimarkingtolerance          						AS 'Cimarkingtolerance'
          ,GD.Totalmark                   						AS 'Totalmark'
          ,GD.Totalitemsincompetencyrun							AS 'Totalitemsincompetencyrun'
          ,GD.Correctitemswithintolerancewithincompetencerun	AS 'Correctitemswithintolerancewithincompetencerun'
          ,GD.Maximumcumulativeerrorwithincompetencerun			AS 'Maximumcumulativeerrorwithincompetencerun'
          ,GD.Probabilityofgettingci      						AS 'Probabilityofgettingci'
          ,GD.Maximumresponsebeforepresentingci 				AS 'Maximumresponsebeforepresentingci'
          ,GD.Maxconsecutivecisthatcanbemarkedoutoftolerance	AS 'Maxconsecutivecisthatcanbemarkedoutoftolerance'
          ,GD.Numberofcisinrollingreview						AS 'Numberofcisinrollingreview'
          ,GD.Maximumcismarkedoutoftoleranceinrollingreview		AS 'Maximumcismarkedoutoftoleranceinrollingreview'
          ,GD.ExamID		               						AS 'ExamId'
          ,UGR.Feedback											AS 'Feedback'
          ,GD.IsFeedbackOnDailyCompetencyCheck					AS 'IsFeedbackOnDailyCompetencyCheck'
          ,GD.IsFeedbackOnOngoingCompetencyCheck				AS 'IsFeedbackOnOngoingCompetencyCheck'
    FROM   dbo.UniqueGroupResponses AS UGR
           INNER JOIN dbo.Groupdefinitions AS GD ON  UGR.Groupdefinitionid = GD.Id
    WHERE  UGR.Id = @uniqueGroupResponseId
    
    --Get items info for group
    SELECT I.Id                   AS 'Id'
          ,I.Cimarkingtolerance   AS 'Cimarkingtolerance'
          ,I.Totalmark            AS 'Totalmark'
          ,I.MarkingType		  AS 'MarkingType'
          ,UGRL.Uniqueresponseid  AS 'UNIQUERESPONSEID'
          ,UGRL.Confirmedmark     AS 'Confirmedmark'
    FROM   dbo.Uniqueresponses    AS UR
           INNER JOIN dbo.UniqueGroupResponseLinks UGRL ON  UGRL.Uniqueresponseid = UR.Id
           INNER JOIN dbo.Items I ON  I.Id = UR.Itemid
    WHERE  UGRL.Uniquegroupresponseid = @uniqueGroupResponseId
END
GO
/****** Object:  StoredProcedure [dbo].[sm_MARKINGSERVICE_ParkResponse_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================= 
-- Author:    Tom Gomersall 
-- Create date: 07/04/2011 
-- Description:  Parks a unique response, updating the unique response table and incrementing the number of parked responses for the user in the Quota management table
-- Modified:  Tom Gomersall 
-- Date:    24/09/2012 
-- Description:  Added marked metadata. 
-- Modified:  Anton Burdyugov 
-- Date:    27/05/2013 
-- Description:  Make work with group data. 
-- ============================================= 
CREATE PROCEDURE [dbo].[sm_MARKINGSERVICE_ParkResponse_sp]
-- Add the parameters for the stored procedure here
	@userID INT,
	@groupResponseID INT,
	@mark DECIMAL(18, 10),
	@parkedAnnotation NVARCHAR(MAX),
	@reasonIds NVARCHAR(200),
	@uniqueResponses [UNIQUERESPONSESTYPE1] READONLY
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements. 
    SET NOCOUNT ON;
    
    --First check that the user does have the unique response checked out. 
    DECLARE @myUserIdCheck INT=(
                SELECT TS.Userid
                FROM   dbo.Tokenstore TS
                       INNER JOIN dbo.Uniquegroupresponses UGR
                            ON  TS.Id = UGR.Tokenid
                WHERE  UGR.Id = @groupResponseID
            )
    --Get the item ID so which is needed to update the QuotaManagement table. Also check whether the response is a CI.
    DECLARE @myGroupId INT
    DECLARE @myGroupIsCI BIT
    
    SELECT @myGroupId = UGR.Groupdefinitionid
          ,@myGroupIsCI = UGR.Ci
    FROM   dbo.Uniquegroupresponses UGR
    WHERE  UGR.Id = @groupResponseID
    
    IF (@myUserIdCheck=@userID OR @myGroupIsCI=1)
    BEGIN
        --The reponse is checked out to this user, so can be parked by him. If it's a CI, then it won't be checked out.
        IF @myGroupIsCI=0
        BEGIN
            --It is not a CI, so it can be parked 
            BEGIN TRANSACTION
            
            BEGIN TRY
                --Increment the NumItemsParked column in the QuotaManagement table. 
                UPDATE QM
                SET    QM.Numitemsparked = QM.Numitemsparked+1
                      ,QM.NumberMarked = CASE WHEN @mark IS NOT NULL THEN QM.NumberMarked+1 ELSE QM.NumberMarked END
                      ,QM.Previousassignedmarkid = NULL
                FROM   dbo.Quotamanagement QM
                WHERE  QM.Userid = @userID
                       AND QM.GroupId = @myGroupId
                
                IF (@@ROWCOUNT=1)
                BEGIN
                    --The same time must be used for the parked date and the assigned mark entry becuase it is used to link them
                    DECLARE @myTimeStamp DATETIME=GETDATE()
                    
                    --The quota management table was updated, so the Unique responses table can be updated and the item checked in
                    UPDATE UGR
                    SET    PARKEDUSERID = @userId
                          ,PARKEDANNOTATION = @parkedAnnotation
                          ,PARKEDDATE = @myTimeStamp
                          ,TOKENID = NULL
                          ,IsEscalated = 1
                    FROM   dbo.Uniquegroupresponses UGR
                    WHERE  UGR.Id = @groupResponseID
                    
                    DELETE FROM dbo.UniqueGroupResponseParkedReasonCrossRef
					WHERE UniqueGroupResponseId = @groupResponseID
                    AND ParkedReasonId IN (SELECT R.Value  FROM   dbo.ParamsToList(@reasonIds) AS R)

                    INSERT INTO dbo.UniqueGroupResponseParkedReasonCrossRef
                    SELECT @groupResponseID
                          ,R.Value
                    FROM   dbo.ParamsToList(@reasonIds) AS R
                    
                    -- update UniqueGroupResponseLinks 
                    UPDATE UGRL
                    SET    UGRL.Confirmedmark = UR.Mark
                          ,UGRL.Markedmetadataresponse = UR.Markedmetadata
                    FROM   dbo.Uniquegroupresponselinks UGRL
                           INNER JOIN @uniqueResponses UR
                                ON  UR.Id = UGRL.Uniqueresponseid
                    WHERE  UGRL.Uniquegroupresponseid = @groupResponseID
                    
                    --Insert an entry in the AssignedGroupMarks table. 
                    INSERT INTO dbo.Assignedgroupmarks
                      (
                        Userid
                       ,Uniquegroupresponseid
                       ,TIMESTAMP
                       ,Assignedmark
                       ,Isconfirmedmark
                       ,Username
                       ,Fullname
                       ,Surname
                       ,Disregarded
                       ,Markingdeviation
                       ,Markingmethodid
                       ,GroupDefinitionID
                       ,IsEscalated
                       ,ParkedAnnotation
                       ,EscalatedWithoutMark
                      )
                    VALUES
                      (
                        @userID
                       ,@groupResponseID
                       ,@myTimeStamp
                       ,ISNULL(@mark, 0)
                       ,0
                       ,NULL
                       ,NULL
                       ,NULL
                       ,0
                       ,0
                       ,11
                       ,@myGroupId
                       ,1
                       ,@parkedAnnotation
					   ,CASE WHEN @mark IS NULL THEN 1 ELSE 0 END
                      )
                    
                    DECLARE @groupMarkId BIGINT=SCOPE_IDENTITY()
                    
                    -- Store history
                    INSERT INTO dbo.AssignedGroupMarksParkedReasonCrossRef
                    SELECT @groupMarkId
                          ,R.Value
                    FROM   dbo.ParamsToList(@reasonIds) AS R
                    
                    --Insert an entry in the AssignedItemMarks
                    INSERT INTO dbo.Assigneditemmarks
                      (
                        Groupmarkid
                       ,Uniqueresponseid
                       ,Annotationdata
                       ,Assignedmark
                       ,Markingdeviation
                       ,Markedmetadataresponse
                      )
                    SELECT @groupMarkId
                          ,UR.Id
                          ,UR.Annotations
                          ,UR.Mark
                          ,0
                          ,UR.Markedmetadata
                    FROM   @uniqueResponses UR
                    
                    
                    COMMIT TRANSACTION
                    
                    SELECT @myGroupIsCI
                END
                ELSE
                BEGIN
                    --The quota management table wasn't updated, so throw an exception 
                    return 50002;
                END
            END TRY
            
            BEGIN CATCH
                ROLLBACK TRANSACTION
                
                DECLARE @myErrorNum INT=ERROR_NUMBER()                
                DECLARE @myMessage NVARCHAR(MAX)=ERROR_MESSAGE()
                DECLARE @myFullMessage NVARCHAR(MAX)=CONVERT(NVARCHAR(10) ,@myErrorNum) +':'+@myMessage
                    
                RAISERROR (@myFullMessage ,16 ,1)
            END CATCH
        END
        ELSE
        BEGIN
            --It is a CI, so cannot be parked. 
            
            UPDATE QM
            SET    QM.NumberMarked = QM.NumberMarked+1
            FROM   dbo.Quotamanagement QM
            WHERE  QM.Userid = @userID
                   AND QM.GroupId = @myGroupId
            
            SELECT @myGroupIsCI -- 1
        END
    END
    ELSE
    BEGIN
        --The response is not checked out to this user 
		return 50001;
    END
END
GO
/****** Object:  StoredProcedure [dbo].[sm_MARKINGSERVICE_SetUniqueGroupResponseAsControlItem_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sm_MARKINGSERVICE_SetUniqueGroupResponseAsControlItem_sp] 

	@uniqueGroupResponseId BIGINT,
	@userID INT,
	@maxCIs INT,
	@feedback NVARCHAR(MAX)=NULL
AS
BEGIN
    DECLARE @AGMId INT	
    DECLARE @GDId INT
    
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;
    
    IF EXISTS(
           SELECT ID
           FROM   dbo.UniqueGroupResponses
           WHERE  ID         = @uniqueGroupResponseId
                  AND CI     = 1
       )
    BEGIN
		-- Unique group response is already a CI
        return 50014;
    END
    
    SELECT TOP 1 
           @AGMId = ID
          ,@GDId  = GroupDefinitionID
    FROM   dbo.AssignedGroupMarks
    WHERE  UniqueGroupResponseId     = @uniqueGroupResponseId
    ORDER BY
           TIMESTAMP DESC
    
    IF (
           (
               SELECT COUNT(ID)
               FROM   dbo.UniqueGroupResponses
               WHERE  GroupDefinitionID     = @GDId
                      AND CI                = 1
           )>=@maxCIs
       )
    BEGIN
        return 50011;
    END
    ELSE
    BEGIN
        BEGIN TRY
            BEGIN TRANSACTION
            
            UPDATE dbo.UniqueGroupResponses
            SET    
				   CI = 1
                  ,CI_Review = 0
                  ,ConfirmedMark = NULL
                  ,Feedback = @feedback
            WHERE  ID = @uniqueGroupResponseId
            
            UPDATE AssignedGroupMarks
            SET    
				   IsConfirmedMark = 0
                  ,MarkingMethodId = 5 -- Actual CI Marking
                  ,IsActualMarking = NULL
                  ,IsReportableCI = 1 -- CI, that is created from Marking, is considered in Report
            WHERE  ID = @AGMId
                        
            UPDATE dbo.QuotaManagement
            SET    
				   PreviousAssignedMarkId = NULL                  
            WHERE  UserId = @userId
                   AND GroupId = @GDId
            
            UPDATE dbo.UserCompetenceStatus
            SET    
				   ItemCountSinceLastCISubmitted = ItemCountSinceLastCISubmitted - 1
            WHERE  UserID = @UserId
                   AND GroupDefinitionID = @GDId
                   
            -- when user Propose as CI we have to delete all AffecterCandidateExamVersions rows with current UGR 
			-- in order to release all affected users related to the UGR
			DELETE 
			FROM AffectedCandidateExamVersions
			WHERE UniqueGroupResponseId = @uniqueGroupResponseId
            
            
            COMMIT TRANSACTION
        END TRY
        BEGIN CATCH
            ROLLBACK TRANSACTION
            DECLARE @myErrorNum INT=ERROR_NUMBER()
            DECLARE @myMessage NVARCHAR(MAX)=ERROR_MESSAGE()
            DECLARE @myFullMessage NVARCHAR(MAX)=CONVERT(NVARCHAR(10) ,@myErrorNum) +':'+@myMessage
                
            RAISERROR (@myFullMessage ,16 ,1)
        END CATCH
    END
END
GO
/****** Object:  StoredProcedure [dbo].[sm_MARKINGSERVICE_SuspendExaminerForGroup_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Dave Dixon
-- Create date: 16/06/2011
-- Description:	Suspend the user for the passed in Item
-- Modifier:		Vitaly BEvzik
-- Create date: 24/5/2013
-- Description:	Changed to use with groups
-- Modifier:	Anton Burdyugov
-- Create date: 19/06/2013
-- Description:	 Incremention number of times a particular Examiner has been suspended added
-- =============================================
CREATE PROCEDURE [dbo].[sm_MARKINGSERVICE_SuspendExaminerForGroup_sp]
	@userId INT ,
	@groupId INT ,
	@suspendedReason INT ,
	@suspendedReasonCustomText NVARCHAR(MAX)
AS
BEGIN
    BEGIN TRY
        BEGIN TRANSACTION
        INSERT INTO dbo.[SuspendedReasons]
          (
            Reasonid
           ,Reason
           ,Userid
           ,Date
          )
        SELECT @suspendedReason
              ,@suspendedReasonCustomText
              ,@userID
              ,GETDATE()
        
        DECLARE @mySuspendedReasonID INT=SCOPE_IDENTITY()
        
        UPDATE dbo.QuotaManagement
        SET    MarkingSuspended = 1
              ,SuspendedReason = @mySuspendedReasonID
              ,Within1CIofSuspension = 0
              ,NumberSuspended = NumberSuspended+1
              ,PreviousAssignedMarkId = NULL
        WHERE  UserId = @userId
               AND GroupId = @groupId
        
        COMMIT TRANSACTION
    END TRY
    BEGIN CATCH
        ROLLBACK TRANSACTION
        DECLARE @myErrorNum INT=ERROR_NUMBER()
        DECLARE @myMessage NVARCHAR(MAX)=ERROR_MESSAGE()
        DECLARE @myFullMessage NVARCHAR(MAX)=CONVERT(NVARCHAR(10) ,@myErrorNum)
               +':'+@myMessage
        
        RAISERROR (@myFullMessage ,16 ,1)
    END CATCH
END
GO
/****** Object:  StoredProcedure [dbo].[sm_MARKINGSERVICE_SuspendUserForAttemptingToParkCG_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Tom Gomersall
-- Create date: 31/05/2011
-- Description:	Suspends a marker who attempts to park a control item. Puts an entry in the AssignedGroupMarks table.
-- Modified:	Tom Gomersall
-- Date:		24/09/2012
-- Description:	Added marked metadata.
-- Modified:	Anton Burdyugov
-- Date:		27/05/2013
-- Description:	Make work with group data.
-- Modifier:	Anton Burdyugov
-- Create date: 19/06/2013
-- Description:	 Incremention number of times a particular Examiner has been suspended added
-- =============================================
CREATE PROCEDURE [dbo].[sm_MARKINGSERVICE_SuspendUserForAttemptingToParkCG_sp]
  -- Add the parameters for the stored procedure here
  @groupResponseID INT,
  @mark            DECIMAL(18, 10),  
  @userID          INT,
  @suspendedReason NVARCHAR(max),
  @uniqueResponses [UNIQUERESPONSESTYPE1] readonly
AS
  BEGIN
      -- SET NOCOUNT ON added to prevent extra result sets from
      -- interfering with SELECT statements.
      SET NOCOUNT ON;

      --Get the item ID so which is needed to update the QuotaManagement table. Also check whether the response is a CI.
      DECLARE @myGroupId INT

      SELECT @myGroupId = UGR.Groupdefinitionid
      FROM   dbo.Uniquegroupresponses UGR
      WHERE  UGR.Id = @groupResponseID

      BEGIN TRANSACTION

      BEGIN TRY
          DECLARE @countOfNotEmptyAnnotations INT

          SET @countOfNotEmptyAnnotations = (SELECT Count(*)
                                             FROM   @uniqueResponses UR
                                             WHERE  Len(Cast(UR.Annotations AS
                                                             NVARCHAR(
                                                             max))) > 0)

          DECLARE @myTimeStamp DATETIME = Getdate()

          --Create a suspended reason entry
          INSERT INTO dbo.Suspendedreasons
                      (Reasonid,
                       Reason,
                       Userid,
                       Date)
          VALUES      ( 2,
                        @suspendedReason,
                        @userID,
                        @myTimeStamp )

          DECLARE @mySuspendedReasonID INT = Scope_identity()

          UPDATE QM
          SET    QM.Markingsuspended = 1,
                 QM.Suspendedreason = @mySuspendedReasonID,
                 QM.Within1ciofsuspension = 0,
				 QM.NumberSuspended = QM.NumberSuspended + 1
          -- can't be within 1 of being suspended if they are suspended!
          FROM   dbo.Quotamanagement QM
          WHERE  QM.Userid = @userID
                 AND QM.GroupId = @myGroupId

          IF ( @@ROWCOUNT = 1 )
            BEGIN
                IF ( @countOfNotEmptyAnnotations > 0 )
                  BEGIN
                      --Insert an entry in the AssignedGroupMarks table only if there is annotation
                      DECLARE @myUserCompetenceStatus INT = (SELECT
                              Competencestatus
                         FROM   dbo.Usercompetencestatus UCS
                         WHERE  UCS.Userid = @userId
                                AND UCS.Groupdefinitionid = @myGroupId)
                      --Marking method ID is competency control item
                      DECLARE @myMarkingMethodId INT = 3

                      IF ( @myUserCompetenceStatus = 2 )
                        BEGIN
                            --The marker has passed competency, so it must be a random CI, so the marking method ID must be 2
                            SET @myMarkingMethodId = 2
                        END

                      --Insert an entry in the AssignedGroupMarks tables
                      --Insert an entry in the dbo.AssignedGroupMarks.
                      INSERT INTO dbo.Assignedgroupmarks
                                  (Userid,
                                   Uniquegroupresponseid,
                                   Timestamp,
                                   Assignedmark,
                                   Isconfirmedmark,
                                   Username,
                                   Fullname,
                                   Surname,
                                   Disregarded,
                                   Markingdeviation,
                                   Markingmethodid,
                                   IsEscalated,
                                   GroupDefinitionID)
                      VALUES      ( @userID,
                                    @groupResponseID,
                                    @myTimeStamp,
                                    @mark,
                                    0,
                                    NULL,
                                    NULL,
                                    NULL,
                                    0,
                                    0,
                                    @myMarkingMethodId,
                                    1,
                                    @myGroupId)

                      --Insert an entry in the dbo.AssignedItemMarks.
                      DECLARE @groupMarkId BIGINT = Scope_identity()

                      INSERT INTO dbo.Assigneditemmarks
                                  (Groupmarkid,
                                   Uniqueresponseid,
                                   Annotationdata,
                                   Assignedmark,
                                   Markingdeviation,
                                   Markedmetadataresponse)
                      SELECT @groupMarkId,
                             UR.Id,
                             UR.Annotations,
                             UR.Mark,
                             0,
                             UR.Markedmetadata
                      FROM   @uniqueResponses UR

                      SELECT ugr.Feedback FROM UniqueGroupResponses ugr WHERE ugr.ID = @groupResponseID
                      
                  END

                COMMIT TRANSACTION
            END
          ELSE
            BEGIN
                --The quota management table wasn't updated, so throw an exception
                return 50002;
            END
      END TRY

      BEGIN CATCH

          ROLLBACK TRANSACTION

			DECLARE @myErrorNum INT = Error_number()
			DECLARE @myMessage NVARCHAR(max) = Error_message()
			DECLARE @myFullMessage NVARCHAR(max) = CONVERT(NVARCHAR(10), @myErrorNum) + ':' + @myMessage

			RAISERROR (@myFullMessage,16,1)
      END CATCH
  END
GO
/****** Object:  StoredProcedure [dbo].[sm_MARKINGSERVICE_UpdateQuotaManagementWithUserCI_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sm_MARKINGSERVICE_UpdateQuotaManagementWithUserCI_sp]
-- Add the parameters for the stored procedure here
	@userId INT,
	@groupId INT,
	@withinOneCIOfSuspension BIT
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;
    
    UPDATE dbo.QuotaManagement
    SET    Within1CIofSuspension     = @withinOneCIOfSuspension
    WHERE  UserId                    = @userId
           AND GroupId               = @groupId
END
GO
/****** Object:  StoredProcedure [dbo].[sm_MARKINGSERVICE_UpdateUserCompetenceForGroupByPassCR_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Rathin Sundar
-- Create date: 30/03/2011
-- Description:	Bypass competency run Update the user for the given item to be competent
-- Author:		Vitaly Bevzik
-- Modify date: 30/03/2011
-- Description:	Change itemId to GroupDefinitionID
-- =============================================
CREATE PROCEDURE [dbo].[sm_MARKINGSERVICE_UpdateUserCompetenceForGroupByPassCR_sp]
	-- Add the parameters for the stored procedure here
	@userId int,
	@groupDefinitionId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	BEGIN TRY
		-- Insert statements for procedure here
		IF EXISTS(SELECT [UserID] FROM [dbo].[UserCompetenceStatus] UCS
						WHERE UCS.[UserID] = @userId and UCS.GroupDefinitionID = @groupDefinitionId )
			  BEGIN
					UPDATE [dbo].[UserCompetenceStatus]
					   SET [DateCertified] = GETDATE(), [CompetenceStatus] = 2, [ItemCountSinceLastCISubmitted] = 0
					 WHERE [UserID] = @userId and GroupDefinitionID = @groupDefinitionId
			  END	
		ELSE
			BEGIN
				INSERT INTO [dbo].[UserCompetenceStatus]
						 ([UserID],[GroupDefinitionID],[DateCertified],[CompetenceStatus],[ItemCountSinceLastCISubmitted]) 
						 VALUES (@userId, @groupDefinitionId, GetDate(), 2, 0)
			END			
	END TRY
	BEGIN CATCH 
		ROLLBACK TRANSACTION  

		DECLARE @myErrorNum INT = ERROR_NUMBER()
		DECLARE @myMessage NVARCHAR(MAX) = ERROR_MESSAGE()
		DECLARE @myFullMessage NVARCHAR(MAX) = CONVERT(NVARCHAR(10), @myErrorNum) + ':' + @myMessage

		RAISERROR (@myFullMessage, 16, 1)
	END CATCH  
END
GO
/****** Object:  StoredProcedure [dbo].[sm_MARKINGSERVICE_UpdateUserCompetenceForGroupToPassed_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Dave Dixon
-- Create date: 16/06/2011
-- Description:	Mark the passed in user as competent for the supplied item
-- =============================================
CREATE PROCEDURE [dbo].[sm_MARKINGSERVICE_UpdateUserCompetenceForGroupToPassed_sp]
	-- Add the parameters for the stored procedure here
	@userId int,
	@groupId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	UPDATE [dbo].[Usercompetencestatus]
	SET    [Datecertified] = Getdate(),
		   [Competencestatus] = 2
	WHERE  [Userid] = @userId
		   AND Groupdefinitionid = @groupId 
END
GO
/****** Object:  StoredProcedure [dbo].[sm_MEMBERSHIPSERVICE_AddUserExam]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- Adds or updates information about recent user exams
CREATE PROCEDURE [dbo].[sm_MEMBERSHIPSERVICE_AddUserExam]
	@userId INT,
	@examId INT,
	@maxCount INT
AS
BEGIN
	SET NOCOUNT ON;
	
	IF NOT EXISTS(SELECT * FROM Exams WHERE ID = @examId)
	BEGIN
		RETURN
	END

    IF EXISTS (SELECT UserId, ExamId
					FROM [dbo].[UserRecentExams]
					WHERE UserId = @userId AND ExamId = @examId)
		BEGIN
			UPDATE [dbo].[UserRecentExams]
				SET LastUsageDate = GETUTCDATE()
				WHERE UserId = @userId AND ExamId = @examId
		END
	ELSE
		BEGIN
			DECLARE @currentExamsCount INT
			SET @currentExamsCount = (SELECT COUNT(*)
											FROM [dbo].[UserRecentExams]
											WHERE UserId = @userId)
			IF (@currentExamsCount >= @maxCount)
				BEGIN
					UPDATE [dbo].[UserRecentExams]
						SET LastUsageDate = GETUTCDATE(),
							ExamId = @examId
						WHERE UserId = @userId AND LastUsageDate = (SELECT MIN(LastUsageDate)
																			FROM [dbo].[UserRecentExams]
																			WHERE UserId = @userId)
				END
			ELSE
				BEGIN
					INSERT INTO [dbo].[UserRecentExams](ExamId, UserId, LastUsageDate)
						VALUES(@examId, @userId, GETUTCDATE())
				END
		END
END
GO
/****** Object:  StoredProcedure [dbo].[sm_MEMBERSHIPSERVICE_changeUserPassword_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Tom Gomersall
-- Create date: 02/06/11
-- Description:	Changes the user's password.
-- =============================================
CREATE PROCEDURE [dbo].[sm_MEMBERSHIPSERVICE_changeUserPassword_sp]
	-- Add the parameters for the stored procedure here
	 @userId INT,
	 @newPassword NVARCHAR(100)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	DECLARE @myCurrentPassword NVARCHAR(100)
	
	SELECT @myCurrentPassword = [Password]
	FROM Users
	WHERE ID = @userID
	
	IF (@myCurrentPassword = @newPassword)
		BEGIN
			--The new password matches the current one, so throw an error
			return 50006;
		END
	ELSE
		BEGIN
			UPDATE Users
			SET 
				[Password] = @newPassword
				,requiresPasswordReset = 0
			WHERE ID = @userId
		END

END
GO
/****** Object:  StoredProcedure [dbo].[sm_MEMBERSHIPSERVICE_getAllExams_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Tom Gomersall
-- Create date: 15/03/11
-- Description:	Gets all the exams
-- =============================================
CREATE PROCEDURE [dbo].[sm_MEMBERSHIPSERVICE_getAllExams_sp]
-- Add the parameters for the stored procedure here
	@userID INT
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;
    IF (
           EXISTS
           (
               SELECT R.ID
               FROM   dbo.AssignedUserRoles AUR
                      INNER JOIN dbo.Roles R ON  R.ID = AUR.RoleID
                      INNER JOIN dbo.RolePermissions RP ON  AUR.RoleID = RP.RoleID
                      INNER JOIN dbo.[Permissions] P ON  RP.PermissionID = P.ID
               WHERE  AUR.ExamID = 0
                      AND AUR.UserID = @userID
                      AND P.GlobalSupport = 1
           )
       )
    BEGIN
        SELECT Exams.ID         AS 'ID'
              ,Exams.Name       AS 'Name'
              ,QualificationID  AS 'QualificationID'
              ,Exams.Reference  AS 'Reference' 
        FROM   Exams
        WHERE  ID<>0
    END
    ELSE
    BEGIN
        SELECT DISTINCT
               E.ID               AS 'ID'
              ,E.Name             AS 'Name'
              ,E.QualificationID  AS 'QualificationID'
              ,E.Reference		  AS 'Reference'
        FROM   dbo.Exams E
               INNER JOIN dbo.AssignedUserRoles AUR ON  AUR.ExamID = E.ID
        WHERE  AUR.UserID = @userID
               AND E.ID<>0
    END
END
GO
/****** Object:  StoredProcedure [dbo].[sm_MEMBERSHIPSERVICE_getAllQualifications_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Tom Gomersall
-- Create date: 15/03/11
-- Description:	Gets all the qualifications
-- =============================================
CREATE PROCEDURE [dbo].[sm_MEMBERSHIPSERVICE_getAllQualifications_sp]
-- Add the parameters for the stored procedure here
	@userID INT
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    
    SET NOCOUNT ON;
    IF (
           EXISTS
           (
               SELECT R.ID
               FROM   dbo.AssignedUserRoles AUR
                      INNER JOIN dbo.Roles R ON  R.ID = AUR.RoleID
                      INNER JOIN dbo.RolePermissions RP ON  AUR.RoleID = RP.RoleID
                      INNER JOIN dbo.[Permissions] P ON  RP.PermissionID = P.ID
               WHERE  AUR.ExamID = 0
                      AND AUR.UserID = @userID
                      AND P.GlobalSupport = 1
           )
       )
    BEGIN
        SELECT ID    AS 'ID'
              ,NAME  AS 'Name'
        FROM   Qualifications
        WHERE  ID<>0
    END
    ELSE
    BEGIN
        SELECT DISTINCT
               Q.ID    AS 'ID'
              ,Q.Name  AS 'Name'
        FROM   dbo.Qualifications Q
               INNER JOIN dbo.Exams E ON  E.QualificationID = Q.ID
               INNER JOIN dbo.AssignedUserRoles AUR ON  AUR.ExamID = E.ID
        WHERE  AUR.UserID = @userID
               AND Q.ID<>0
    END
END
GO
/****** Object:  StoredProcedure [dbo].[sm_MEMBERSHIPSERVICE_getFirstTwoExamsForWhichUserHasPermission_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Tom Gomersall
-- Create date: 03/06/11
-- Description:	Gets the first two exam versions for which the user has a permission. This is intended to be used to check whether the user has more than one exam version.
-- =============================================
CREATE PROCEDURE [dbo].[sm_MEMBERSHIPSERVICE_getFirstTwoExamsForWhichUserHasPermission_sp]
-- Add the parameters for the stored procedure here
	@userId INT
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;
    
    SET ROWCOUNT 2
    
    IF NOT EXISTS
       (
           SELECT ExamID
           FROM   dbo.AssignedUserRoles
           WHERE  UserID         = @userId
                  AND ExamID     = 0
       )
    BEGIN
        --The user does not have any permissions for the global exam version
        
        SELECT DISTINCT
               E.Name       AS 'Name'
              ,E.Reference  AS 'Ref'
              ,E.ID         AS 'ExamId'
        FROM   dbo.RolePermissions RP
               INNER JOIN dbo.AssignedUserRoles AUR
                    ON  AUR.RoleId = RP.RoleId
               INNER JOIN dbo.Exams E
                    ON  E.ID = AUR.ExamId
        WHERE  AUR.UserId = @userId
    END
    ELSE
    BEGIN
        --The user has permission for the global exam version, so select from all the exam versions that aren't global
        
        SELECT E.Name       AS 'Name'
              ,E.Reference  AS 'Ref'
              ,E.ID         AS 'ExamId'
        FROM   dbo.Exams E
        WHERE  ID>0
    END
    
    SET ROWCOUNT 0
END
GO
/****** Object:  StoredProcedure [dbo].[sm_MEMBERSHIPSERVICE_getPermissionsForUser_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Tom Gomersall
-- Create date: 03/02/11
-- Description:	Gets all the permissions for the user
-- =============================================
CREATE PROCEDURE [dbo].[sm_MEMBERSHIPSERVICE_getPermissionsForUser_sp]
-- Add the parameters for the stored procedure here
	@userId INT
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;
    
    SELECT DISTINCT 
		   ExamID				   AS 'ExamID'
          ,PermissionID            AS 'PermissionID'
          ,Training                AS 'Training'
    FROM   dbo.AssignedUserRoles AUR
           INNER JOIN dbo.Roles R ON  AUR.RoleID = R.ID
           INNER JOIN dbo.RolePermissions RP ON  RP.RoleID = AUR.RoleID
           INNER JOIN dbo.[PERMISSIONS] P ON  P.ID = RP.PermissionID
    WHERE  AUR.UserID = @userId
END
GO
/****** Object:  StoredProcedure [dbo].[sm_MEMBERSHIPSERVICE_getPersonalProfile_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  Brendan Dolye  
-- Create date: 7/6/2011  
-- Description: Gets personal profile info  
  
--NOTE the ordinals are directly coupled in the .NET code  
-- =============================================  
CREATE PROCEDURE [dbo].[sm_MEMBERSHIPSERVICE_getPersonalProfile_sp]  
 @userID int  
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
  
    -- Insert statements for procedure here  
 SELECT Forename, Surname, ISNULL(Email, '') as Email, ISNULL(Phone, '') as Phone FROM dbo.USERS  
 WHERE ID = @userID  
END
GO
/****** Object:  StoredProcedure [dbo].[sm_MEMBERSHIPSERVICE_GetRecentUserExam]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[sm_MEMBERSHIPSERVICE_GetRecentUserExam]
	@userId INT
AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		e.ID AS ExamID, 
		e.Name AS ExamName,
		q.ID AS QualificationId,
		q.Name AS QualificationName, 
		e.Reference AS Reference
	FROM Exams e 
	INNER JOIN Qualifications q 
	ON q.ID = e.QualificationID
	WHERE e.ID = 
    (
    	SELECT TOP 1 ExamId
		FROM [dbo].[UserRecentExams]
		WHERE UserId = @userId
		ORDER BY LastUsageDate DESC
	)
END
GO
/****** Object:  StoredProcedure [dbo].[sm_MEMBERSHIPSERVICE_GetRecentUserExams]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- Gets recent user exams
CREATE PROCEDURE [dbo].[sm_MEMBERSHIPSERVICE_GetRecentUserExams]
	@userId INT,
	@count INT
AS
BEGIN
	SET NOCOUNT ON;

    SELECT TOP(@count) ExamId
		FROM [dbo].[UserRecentExams]
		WHERE UserId = @userId
		ORDER BY LastUsageDate Desc
END
GO
/****** Object:  StoredProcedure [dbo].[sm_MEMBERSHIPSERVICE_logOutUser_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Tom Gomersall
-- Create date: 15/07/11
-- Description:	Logs out a user.
-- Modified:	Anton Burdyugov
-- Date:		10/07/2013
-- Description: Added top 10 token deletion and update UniqueGroupResponse table to set the token back to NULL
-- =============================================

CREATE PROCEDURE [dbo].[sm_MEMBERSHIPSERVICE_logOutUser_sp]
	-- Add the parameters for the stored procedure here
	@tokenId INT,
	@tokenExpiryTime int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	UPDATE UniqueGroupResponses
	SET tokenId = NULL
	WHERE tokenId = @tokenId
	
	DELETE FROM TokenStore
	WHERE id = @tokenId
	
	-- clear expired tokens	
	DECLARE @myExpiredTokens TABLE
	(
		ID	INT
	)
	
	INSERT INTO @myExpiredTokens
	SELECT ID
	FROM TokenStore
	WHERE DATEDIFF(minute, lastInteractionDateTime, GETDATE()) > @tokenExpiryTime
	
	--Sets the token ID to null for any unique responses that are checked out to expired tokens.
	UPDATE UniqueGroupResponses
	SET tokenId = NULL
	WHERE tokenId IS NOT NULL AND tokenId IN  (SELECT ID FROM @myExpiredTokens)
		
	-- Delete top 10 that have expired
	DELETE FROM TokenStore
	WHERE ID IN  (SELECT TOP 10 ID FROM @myExpiredTokens)

END
GO
/****** Object:  StoredProcedure [dbo].[sm_MEMBERSHIPSERVICE_modifyPersonalProfile_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  Brendan Dolye  
-- Create date: 7/6/2011  
-- Description: Gets personal profile info  
  
--NOTE the ordinals are directly coupled in the .NET code  
-- =============================================  
CREATE PROCEDURE [dbo].[sm_MEMBERSHIPSERVICE_modifyPersonalProfile_sp]  
 @userID int,  
 @forename nvarchar(50),  
 @surname nvarchar(50),  
 @phoneNo nvarchar(20),  
 @email nvarchar(100),  
 @successOutput nvarchar(10) output  
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
 DECLARE @userCount int  
 SET @userCount = 0  
    -- Insert statements for procedure here  
 SELECT @userCount = COUNT(ID)  FROM USERS  
 WHERE ID = @userID  
   
 IF @@ROWCOUNT > 0  
 BEGIN  
  UPDATE [dbo].[Users]  
     SET [Forename] = @forename  
     ,[Surname] = @surname  
     ,[Email] = @email  
     ,[Phone] = ISNULL(@phoneNo, '')  
   WHERE ID = @userID  
   SET @successOutput = 'true'  
 END   
 ELSE  
 BEGIN  
  SET @successOutput = 'false'  
 END  
END
GO
/****** Object:  StoredProcedure [dbo].[sm_MEMBERSHIPSERVICE_validateUser_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================    
-- Author:  Brendan Doyle    
-- Create date: 21/1/11    
-- Description: Main validation procedure    
-- =============================================    
-- 06-07-2011: Umair Added check for retired and internal user  
-- =============================================    

-- Modified by: Awais Masood
-- Date :       29/06/2012
-- Description: Added User Account Maximum attempts detail and Account Expiry.
CREATE PROCEDURE [dbo].[sm_MEMBERSHIPSERVICE_validateUser_sp]     
 --Add the parameters for the stored procedure here    
   @userName   nvarchar(50),      
   @hashedPassword nvarchar(50),
   @maxPasswordAttempts int,    
   @UserIDOutput int output,    
   @forenameOutput nvarchar(50) output,    
   @surnameOutput nvarchar(50) output,    
   @requiresPasswordReset BIT OUTPUT    
AS    
BEGIN    
-- SET NOCOUNT ON added to prevent extra result sets from    
-- interfering with SELECT statements.    
SET NOCOUNT ON;    
    
--DECLARE @FullReturnString nvarchar(max)      
--DECLARE @forename nvarchar(max),@surname nvarchar(max), @UserEmail nvarchar(50)     
 SET @UserIDOutput = @UserIDOutput    
 SET @forenameOutput = @forenameOutput    
 SET @surnameOutput = @surnameOutput    
 
 DECLARE @UserID int  
 SET @UserID=-1  
     
     SELECT @UserID=ID FROM dbo.Users WHERE lower(Username)  = lower(@userName) AND Password = @hashedPassword AND Retired = 0
          
	 if NOT (@UserID=-1)
	 BEGIN
	 
	         IF EXISTS(SELECT ID FROM dbo.Users  
			 WHERE ID=@UserID AND ExpiryDate < GetDate())  
			 BEGIN  
				 --the user account is expired       
			    	 SELECT 3  
			 END             
			 ELSE
			 BEGIN
			         UPDATE dbo.Users
	                 SET [PasswordAttempts] = 0,
	                 [LastLoginDate] = GetDate()      
		             WHERE ID=@UserID
			 
			         SELECT     
						@UserIDOutput = ID,     
						@forenameOutput=Replace([Forename],'''', '&apos;'),     
						@surnameOutput=Replace([Surname], '''', '&apos;'),    
						@requiresPasswordReset = requiresPasswordReset    
					 FROM dbo.Users     
					 WHERE ID=@UserID
							
			    	SELECT 1
			  END	 	 			  			  			   
	 END 
	 ELSE
	 BEGIN	
	 
	      DECLARE @passwordAttempts int	  
	      
	      SELECT @UserID=ID FROM dbo.Users WHERE lower(Username) = lower(@userName) AND Retired = 0 AND InternalUser=0
	      	          
	      if NOT(@UserID=-1)
          BEGIN
          
               UPDATE dbo.Users
	           SET [PasswordAttempts] = ISNULL([PasswordAttempts],0)+1		          
		       WHERE ID=@UserID
		       
		       SELECT @passwordAttempts=[PasswordAttempts] FROM dbo.Users WHERE ID=@UserID
		       
		       if @passwordAttempts>@maxPasswordAttempts
		        BEGIN
		             UPDATE dbo.Users
	                 SET [ExpiryDate]  = GETDATE()		          
	                    ,[PasswordAttempts] = 0           
		             WHERE ID=@UserID
		        END             
          END
	 	       	        			 
		  SELECT 2			
	 END
               
END
GO
/****** Object:  StoredProcedure [dbo].[sm_QUALITYCONTROLSERVICE_DiscardCI_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Tom Gomersall
-- Create date: 14/04/11
-- Description:	Discards a CI. Changes the unique response back to being a normal item, and deletes the assigned mark entry.
-- Modified:	George Chernov
-- Date:		23/04/2013
-- Description:	Discards a control group.
-- Modified:	George Chernov
-- Date:		29/05/2013
-- Description:	Change calling of sm_SHARED_removeMarksFromMarkedMetadataFromUGRLinks_sp
-- Modified:	Anton Burdyugov
-- Date:		07/06/2013
-- Description:	Make work with assigned group marks
-- =============================================

CREATE PROCEDURE [dbo].[sm_QUALITYCONTROLSERVICE_DiscardCI_sp]
-- Add the parameters for the stored procedure here
	@groupResponseID BIGINT ,
	@tokenId INT ,
	@userID INT ,
	@reviewing BIT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;		
	
	IF (
	       (
	           SELECT UGR.TokenId
	           FROM   dbo.UniqueGroupResponses UGR
	           WHERE  UGR.ID = @groupResponseID
	       ) = @tokenID
	       OR @reviewing = 0
	   )
	BEGIN
	    --The response is checked out to this user, so the marker response can be used.
	    
	    BEGIN TRY
	    	BEGIN TRANSACTION
	    	
	    	IF (
	    	       dbo.sm_checkIsUniqueGroupResponseInConfirmedScript(@groupResponseID) 
	    	       = 1
	    	   )
	    	    RAISERROR (
	    	        'It is not allowed to discard confirmed script''s responses',
	    	        16,
	    	        1,
	    	        @@spid
	    	    )
	    	
	    	--@markingMethod = 9 because the item is not being reviewed.
	    	DECLARE @markingMethod INT = 9
	    	IF (@reviewing = 1)
	    	BEGIN
	    	    --@markingMethod = 8 because the item is being reviewed.
	    	    SET @markingMethod = 8
	    	END
	    	
	    	DECLARE @myCICreationAssignedMarkId INT
	    	IF (@reviewing = 0)
	    	BEGIN
	    	    SET @myCICreationAssignedMarkId = (
	    	            SELECT TOP 1 AGM.ID
	    	            FROM   dbo.AssignedGroupMarks AGM
	    	            WHERE  AGM.UniqueGroupResponseId = @groupResponseID
	    	                   AND AGM.markingMethodId = 5
	    	                   AND AGM.IsConfirmedMark = 1
	    	            ORDER BY
	    	                   TIMESTAMP DESC
	    	        )
	    	END
	    	ELSE
	    	BEGIN
	    	    SET @myCICreationAssignedMarkId = (
	    	            SELECT TOP 1 AGM.ID
	    	            FROM   dbo.AssignedGroupMarks AGM
	    	            WHERE  AGM.UniqueGroupResponseId = @groupResponseID
	    	                   AND AGM.markingMethodId = 5
	    	            ORDER BY
	    	                   TIMESTAMP DESC
	    	        )
	    	END
	    	
	    	DECLARE @myCICreationAssignedMarks TABLE
	    	        (
	    	            ID BIGINT,
	    	            UniqueGroupResponseId INT,
	    	            UniqueResponseId BIGINT
	    	        )
	    	
	    	INSERT INTO @myCICreationAssignedMarks
	    	SELECT AGM.ID,
	    	       AGM.UniqueGroupResponseId,
	    	       AIM.UniqueResponseId
	    	FROM   dbo.AssignedGroupMarks AGM
	    	       INNER JOIN dbo.AssignedItemMarks AIM
	    	            ON  AIM.GroupMarkId = AGM.ID
	    	WHERE  AGM.ID = @myCICreationAssignedMarkId
	    	
	    	--Update the CI and CI_Review columns and set the confirmed mark to NULL.
	    	UPDATE UGR
	    	SET    CI = 0,
	    	       CI_Review = 0,
	    	       ConfirmedMark = NULL,
	    	       UGR.Feedback = NULL
	    	FROM   dbo.UniqueGroupResponses UGR
	    	WHERE  UGR.ID = @groupResponseID
	    	
	    	-- Update UR links - done by sm_SHARED_removeMarksFromMarkedMetadataFromUGRLinks_sp
	    	-- TODO is there need to update UR too? -no
	    	
	    	--Sets the marks in the marked metadata back to zero
	    	EXEC sm_SHARED_removeMarksFromMarkedMetadataFromUGRLinks_sp 
	    	     @uniqueGroupResponseId = @groupResponseID,
	    	     @updateTable = 1
	    	
	    	--Update any entries for the group in the AssignedGroupMarks table not to be the confirmed mark.
	    	UPDATE AGM
	    	SET    IsConfirmedMark = 0
	    	FROM   dbo.AssignedGroupMarks AGM
	    	WHERE  AGM.id IN (SELECT id
	    	                  FROM   @myCICreationAssignedMarks)
	    	
	    	DECLARE @groupId INT = (
	    	            SELECT GroupDefinitionID
	    	            FROM   dbo.UniqueGroupResponses
	    	            WHERE  ID = @groupResponseID
	    	        )
	    	
	    	------insert new marks with new @markingMethod---------
	    	INSERT INTO dbo.AssignedGroupMarks
	    	  (
	    	    UserId,
	    	    UniqueGroupResponseId,
	    	    TIMESTAMP,
	    	    AssignedMark,
	    	    IsConfirmedMark,
	    	    UserName,
	    	    FullName,
	    	    Surname,
	    	    Disregarded,
	    	    MarkingDeviation,
	    	    MarkingMethodId,
	    	    GroupDefinitionID
	    	  )
	    	SELECT @userID,	-- UserId - int
	    	       @groupResponseID,	-- UniqueGroupResponseId - bigint 
	    	       GETDATE(),	-- Timestamp - datetime
	    	       AGM.AssignedMark,	-- AssignedMark - decimal
	    	       0,	-- IsConfirmedMark - bit
	    	       NULL,	-- UserName - nvarchar(50)
	    	       NULL,	-- FullName - nvarchar(101)
	    	       NULL,	-- Surname - nvarchar(50)
	    	       0,	-- Disregarded - bit
	    	       0,	-- MarkingDeviation - decimal
	    	       @markingMethod,	-- MarkingMethodId - int
	    	       @groupId -- GroupId
	    	FROM   dbo.AssignedGroupMarks AGM
	    	WHERE  AGM.id = @myCICreationAssignedMarkId
	    	
	    	DECLARE @myNewAssignnedGroupMarkId INT = SCOPE_IDENTITY()
	    	
	    	INSERT INTO dbo.AssignedItemMarks
	    	  (
	    	    GroupMarkId,
	    	    UniqueResponseId,
	    	    AnnotationData,
	    	    AssignedMark,
	    	    MarkingDeviation,
	    	    MarkedMetadataResponse
	    	  )
	    	SELECT @myNewAssignnedGroupMarkId,	-- GroupMarkId - bigint
	    	       AIM.UniqueResponseId,	-- UniqueResponseId - bigint
	    	       NULL,	-- AnnotationData - xml
	    	       AIM.AssignedMark,	-- AssignedMark - decimal
	    	       AIM.MarkingDeviation,	-- MarkingDeviation - decimal
	    	       AIM.MarkedMetadataResponse -- MarkedMetadataResponse - xml
	    	FROM   dbo.AssignedItemMarks AIM
	    	WHERE  AIM.GroupMarkId = @myCICreationAssignedMarkId
	    	       AND AIM.UniqueResponseId IN (SELECT UniqueResponseId
	    	                                    FROM   @myCICreationAssignedMarks)
	    	
	    	------ end insert ----------
	    	
	    	------ delete rows from AffectedCandidateExaVersions for current UGR
	    	
	    	DELETE 
	    	FROM   AffectedCandidateExamVersions
	    	WHERE  UniqueGroupResponseId = @groupResponseID
	    	
	    	------ end AffectedCandidateExaVersions deleting
	    	
	    	------ update QuotaManagement ---------
	    	UPDATE QM
	    	SET    QM.Within1CIofSuspension = dbo.sm_checkUser1CiawayFromSuspend(@userID, @groupId)
	    	FROM   dbo.QuotaManagement QM
	    	WHERE  QM.GroupId = @groupId
	    	       AND QM.UserId = @userID
	    	---------------------------------------
	    	COMMIT TRANSACTION
	    	
	    	--Set the token to null for all responses that are currently checked out to this logged in user.
	    	UPDATE UGR
	    	SET    UGR.TokenId = NULL
	    	FROM   dbo.UniqueGroupResponses UGR
	    	WHERE  UGR.TokenId = @tokenId
	    END TRY
	    BEGIN CATCH
	    	ROLLBACK TRANSACTION
	    	DECLARE @myErrorNum INT = ERROR_NUMBER()
	    	DECLARE @myMessage NVARCHAR(MAX) = ERROR_MESSAGE()
	    	DECLARE @myFullMessage NVARCHAR(MAX) = CONVERT(NVARCHAR(10), @myErrorNum)
	    	        + ':' + @myMessage
	    	
	    	RAISERROR (@myFullMessage, 16, 1)
	    END CATCH
	END
	ELSE
	BEGIN
	    --The response is not checked out to this user
	    RETURN 50001;
	END
END
GO
/****** Object:  StoredProcedure [dbo].[sm_QUALITYCONTROLSERVICE_getCGCreationDateRangeForGroup_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		George Chernov
-- Create date: 24/04/13
-- Description:	Gets the CG creation date range for a group.
-- =============================================

CREATE PROCEDURE [dbo].[sm_QUALITYCONTROLSERVICE_getCGCreationDateRangeForGroup_sp]
	-- Add the parameters for the stored procedure here
 @groupId INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	SELECT 
		MIN(dbo.ControlGroups_view.CreationDate)		AS Earliest
		,MAX(dbo.ControlGroups_view.CreationDate)		AS Latest
	FROM dbo.ControlGroups_view
	WHERE dbo.ControlGroups_view.GroupDefinitionID = @groupId
		
END
GO
/****** Object:  StoredProcedure [dbo].[sm_QUALITYCONTROLSERVICE_getCGCreatorsForGroups_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		George Chernov
-- Create date: 24/04/13
-- Description:	Gets the CG creators for a group.
-- =============================================
CREATE PROCEDURE [dbo].[sm_QUALITYCONTROLSERVICE_getCGCreatorsForGroups_sp]
	-- Add the parameters for the stored procedure here
 @groupId INT,
 @maxNum INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	SET ROWCOUNT @maxNum
	
	BEGIN TRY
		SELECT 
			dbo.ControlGroups_view.CreatorID			AS ID
			,dbo.ControlGroups_view.CreatorFullName	AS FullName
		FROM dbo.ControlGroups_view
		WHERE dbo.ControlGroups_view.GroupDefinitionID = @groupId
		GROUP BY
			dbo.ControlGroups_view.CreatorID
			,dbo.ControlGroups_view.CreatorFullName
		
		SET ROWCOUNT 0
	END TRY
	BEGIN CATCH
		SET ROWCOUNT 0
		return 50005;
	END CATCH
END
GO
/****** Object:  StoredProcedure [dbo].[sm_QUALITYCONTROLSERVICE_getCGMarkMarkDistributionGroup_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		George Chernov
-- Create date: 24/04/13
-- Description:	Gets the CG mark distribution for a group.
-- =============================================
CREATE PROCEDURE [dbo].[sm_QUALITYCONTROLSERVICE_getCGMarkMarkDistributionGroup_sp]
	-- Add the parameters for the stored procedure here
 @groupId INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	SELECT
		dbo.ControlGroups_view.ConfirmedMark			AS Mark
		,COUNT(dbo.ControlGroups_view.ConfirmedMark)	AS Occurrences
	FROM dbo.ControlGroups_view
	WHERE dbo.ControlGroups_view.GroupDefinitionID = @groupId
	GROUP BY ConfirmedMark
	ORDER BY Mark ASC
		
END
GO
/****** Object:  StoredProcedure [dbo].[sm_QUALITYCONTROLSERVICE_getCGMarkRangeForGroup_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		George Chernov
-- Create date: 24/04/13
-- Description:	Gets the CG mark range for a group.
-- =============================================

CREATE PROCEDURE [dbo].[sm_QUALITYCONTROLSERVICE_getCGMarkRangeForGroup_sp]
	-- Add the parameters for the stored procedure here
 @groupId INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	SELECT 
		MIN(dbo.ControlGroups_view.ConfirmedMark)	AS MinVal
		,MAX(dbo.ControlGroups_view.ConfirmedMark)	AS MaxVal
	FROM dbo.ControlGroups_view
	WHERE dbo.ControlGroups_view.GroupDefinitionID = @groupId
		
END
GO
/****** Object:  StoredProcedure [dbo].[sm_QUALITYCONTROLSERVICE_getCGMeanMarkForGroup_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Vitaly Bevzik
-- Create date: 24/04/13
-- Description:	Gets the CG Mean mark range for a group.
-- =============================================
CREATE PROCEDURE [dbo].[sm_QUALITYCONTROLSERVICE_getCGMeanMarkForGroup_sp]
	-- Add the parameters for the stored procedure here
 @groupId INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	SELECT 
		MIN(dbo.ControlGroups_view.MeanMark)	AS MinVal
		,MAX(dbo.ControlGroups_view.MeanMark)	AS MaxVal
	FROM dbo.ControlGroups_view
	WHERE dbo.ControlGroups_view.GroupDefinitionID = @groupId
		
END
GO
/****** Object:  StoredProcedure [dbo].[sm_QUALITYCONTROLSERVICE_getCGNumFailedRangeForGroup_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		George Chernov
-- Create date: 24/04/13
-- Description:	Gets the CG number of times failed range for a group.
-- =============================================
CREATE PROCEDURE [dbo].[sm_QUALITYCONTROLSERVICE_getCGNumFailedRangeForGroup_sp]
	-- Add the parameters for the stored procedure here
 @groupId INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	SELECT 
		MIN(dbo.ControlGroups_view.FailedPercent)	AS MinVal
		,MAX(dbo.ControlGroups_view.FailedPercent)	AS MaxVal
	FROM dbo.ControlGroups_view
	WHERE dbo.ControlGroups_view.GroupDefinitionID = @groupId
		
END
GO
/****** Object:  StoredProcedure [dbo].[sm_QUALITYCONTROLSERVICE_getCGNumPassedNotExactRangeForGroup_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Vitaly Bevzik
-- Create date: 24/04/13
-- Description:	Gets the CG number of times Passed in tolerance range for a group.
-- =============================================
CREATE PROCEDURE [dbo].[sm_QUALITYCONTROLSERVICE_getCGNumPassedNotExactRangeForGroup_sp]
	-- Add the parameters for the stored procedure here
 @groupId INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	SELECT 
		MIN(dbo.ControlGroups_view.PassedNotExactPercent)	AS MinVal
		,MAX(dbo.ControlGroups_view.PassedNotExactPercent)	AS MaxVal
	FROM dbo.ControlGroups_view
	WHERE dbo.ControlGroups_view.GroupDefinitionID = @groupId
		
END
GO
/****** Object:  StoredProcedure [dbo].[sm_QUALITYCONTROLSERVICE_getCGNumPresentedRangeForGroup_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		George Chernov
-- Create date: 24/04/13
-- Description:	Gets the CG number of times presented range for a group.
-- =============================================

CREATE PROCEDURE [dbo].[sm_QUALITYCONTROLSERVICE_getCGNumPresentedRangeForGroup_sp]
	-- Add the parameters for the stored procedure here
 @groupId INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	SELECT 
		MIN(dbo.ControlGroups_view.NumPresented)		AS MinVal
		,MAX(dbo.ControlGroups_view.NumPresented)	AS MaxVal
	FROM dbo.ControlGroups_view
	WHERE dbo.ControlGroups_view.GroupDefinitionID = @groupId
		
END
GO
/****** Object:  StoredProcedure [dbo].[sm_QUALITYCONTROLSERVICE_getCGReviewDateRangeForGroup_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		George Chernov
-- Create date: 24/04/13
-- Description:	Gets the CG review date range for a group.
-- =============================================
CREATE PROCEDURE [dbo].[sm_QUALITYCONTROLSERVICE_getCGReviewDateRangeForGroup_sp]
	-- Add the parameters for the stored procedure here
 @groupId INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	SELECT 
		MIN(dbo.ControlGroups_view.ReviewDate)	AS Earliest
		,MAX(dbo.ControlGroups_view.ReviewDate)	AS Latest
	FROM dbo.ControlGroups_view
	WHERE dbo.ControlGroups_view.GroupDefinitionID = @groupId
		
END
GO
/****** Object:  StoredProcedure [dbo].[sm_QUALITYCONTROLSERVICE_getCGReviewersForGroup_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		George Chernov
-- Create date: 24/04/13
-- Description:	Gets the CG reviewers for a group.
-- =============================================
CREATE PROCEDURE [dbo].[sm_QUALITYCONTROLSERVICE_getCGReviewersForGroup_sp]
	-- Add the parameters for the stored procedure here
 @groupId INT,
 @maxNum INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	SET ROWCOUNT @maxNum
	
	BEGIN TRY
		SELECT 
			dbo.ControlGroups_view.ReviewerID				AS ID
			,MAX(dbo.ControlGroups_view.ReviewerFullName)	AS FullName
		FROM dbo.ControlGroups_view
		WHERE dbo.ControlGroups_view.GroupDefinitionID = @groupId
		GROUP BY
			dbo.ControlGroups_view.ReviewerID
		
		SET ROWCOUNT 0
	END TRY
	BEGIN CATCH
		SET ROWCOUNT 0
		return 50005;
	END CATCH
END
GO
/****** Object:  StoredProcedure [dbo].[sm_QUALITYCONTROLSERVICE_getCGUniqueGroupResponseIdsForGroup_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		George Chernov
-- Create date: 24/04/13
-- Description:	Gets the unique response IDs the CGs for a group.
-- =============================================
CREATE PROCEDURE [dbo].[sm_QUALITYCONTROLSERVICE_getCGUniqueGroupResponseIdsForGroup_sp]
	-- Add the parameters for the stored procedure here
 @groupId INT,
 @maxNum INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	SET ROWCOUNT @maxNum
	
	BEGIN TRY
		SELECT DISTINCT dbo.ControlGroups_view.UniqueGroupResponseId
		FROM dbo.ControlGroups_view
		WHERE dbo.ControlGroups_view.GroupDefinitionID = @groupId
		
		SET ROWCOUNT 0
	END TRY
	BEGIN CATCH
		SET ROWCOUNT 0
		return 50005;
	END CATCH
END
GO
/****** Object:  StoredProcedure [dbo].[sm_QUALITYCONTROLSERVICE_getCGUniqueResponseIdRangeForGroup_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		George Chernov
-- Create date: 22/04/13
-- Description:	Gets the range of unique response IDs the CGs for a group.
-- =============================================
CREATE PROCEDURE [dbo].[sm_QUALITYCONTROLSERVICE_getCGUniqueResponseIdRangeForGroup_sp]
	-- Add the parameters for the stored procedure here
 @groupId INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
		
	SELECT 
		MAX(dbo.ControlGroups_view.UniqueGroupResponseId) AS maxID
		,MIN(dbo.ControlGroups_view.UniqueGroupResponseId) AS minID
	FROM dbo.ControlGroups_view
	WHERE dbo.ControlGroups_view.GroupDefinitionID = @groupId
		
END
GO
/****** Object:  StoredProcedure [dbo].[sm_QUALITYCONTROLSERVICE_getCIInfoForGroup_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Tom Gomersall
-- Create date: 14/04/11
-- Description:	Gets the CI information for an item.
-- =============================================
CREATE PROCEDURE [dbo].[sm_QUALITYCONTROLSERVICE_getCIInfoForGroup_sp]
	-- Add the parameters for the stored procedure here
	@groupID INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	SELECT 
		GD.NoOfCIsRequired									AS 'NumCIsRequired'
		,SUM(CASE UGR.CI WHEN 1 THEN 1 ELSE 0 END)			AS 'CIsCreated'
		,SUM(CASE UGR.CI_Review WHEN 1 THEN 1 ELSE 0 END)	AS 'CIsReviewed'
		,GD.Name											AS 'Name'
		,MAX(GD.CIMarkingTolerance)							AS 'CIMarkingTolerance'
		,GD.IsReleased										AS 'IsReleased'
	FROM dbo.GroupDefinitions GD
	INNER JOIN dbo.UniqueGroupResponses UGR ON GD.ID = UGR.GroupDefinitionID 
	WHERE 
		GD.ID = @groupID
	GROUP BY GD.NoOfCIsRequired, GD.Name, GD.IsReleased
END
GO
/****** Object:  StoredProcedure [dbo].[sm_QUALITYCONTROLSERVICE_getCIReviewModeForExam_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Tom Gomersall
-- Create date: 28/04/11
-- Description:	Gets the auto CI review mode for an exam.
-- =============================================
CREATE PROCEDURE [dbo].[sm_QUALITYCONTROLSERVICE_getCIReviewModeForExam_sp]
-- Add the parameters for the stored procedure here
	@examId INT
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;
    
    SELECT E.AutoCIReview  AS 'AutoCIReview'
    FROM   dbo.Exams E
    WHERE  E.ID = @examId
END
GO
/****** Object:  StoredProcedure [dbo].[sm_QUALITYCONTROLSERVICE_getCompetenceSettingsForGroup_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		George Chernov
-- Create date: 19/04/13
-- Description:	Gets the competency settings for a group.
-- =============================================
CREATE PROCEDURE [dbo].[sm_QUALITYCONTROLSERVICE_getCompetenceSettingsForGroup_sp]
	@groupId INT
AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT
		dbo.GroupDefinitions.TotalItemsInCompetencyRun							AS 'NumItemsInRun'
		,dbo.GroupDefinitions.CorrectItemsWithinToleranceWithinCompetenceRun	AS 'NumItemsWithinToleranceInRun'
		,dbo.GroupDefinitions.MaximumCumulativeErrorWithinCompetenceRun			AS 'MaxErrorInRun'
		,dbo.GroupDefinitions.ProbabilityOfGettingCI							AS 'CIProbability'
		,dbo.GroupDefinitions.MaximumResponseBeforePresentingCI					AS 'MaxItemsBetweenCIs'
		,dbo.GroupDefinitions.MaxConsecutiveCIsThatCanBeMarkedOutOfTolerance	AS 'MaxCIsOutOfTol'
		,dbo.GroupDefinitions.NumberOfCIsInRollingReview						AS 'NumCIsInRollingReview'
		,dbo.GroupDefinitions.MaximumCIsMarkedOutOfToleranceInRollingReview		AS 'MaxOutOfTolInRollingReview'
	FROM dbo.GroupDefinitions
	WHERE dbo.GroupDefinitions.ID = @groupId
END
GO
/****** Object:  StoredProcedure [dbo].[sm_QUALITYCONTROLSERVICE_getDetailsForGroup_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		George Chernov
-- Create date: 17/04/13
-- Description:	Gets details for group.
-- =============================================
CREATE PROCEDURE [dbo].[sm_QUALITYCONTROLSERVICE_getDetailsForGroup_sp]
	@groupID INT,
	@userId  INT,
	@tokenStoreId INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	DECLARE @Responses INT = (
				select COUNT(CGR.UniqueGroupResponseID)
				from dbo.UniqueGroupResponses UGR
				inner join dbo.CandidateGroupResponses CGR on UGR.ID = CGR.UniqueGroupResponseID
				where UGR.GroupDefinitionID = @groupID)	
	
	DECLARE @UResponses INT = (
				select COUNT(DISTINCT CGR.UniqueGroupResponseID)
				from dbo.UniqueGroupResponses UGR
				inner join dbo.CandidateGroupResponses CGR on UGR.ID = CGR.UniqueGroupResponseID
				where UGR.GroupDefinitionID = @groupID)
	
	DECLARE @NumCIsCreated INT
	DECLARE @NumCIsReviewed INT 
	
	SELECT @NumCIsCreated = COUNT(NULLIF(UGR.CI, 0)), @NumCIsReviewed = COUNT(NULLIF(UGR.CI_Review, 0))
	FROM UniqueGroupResponses UGR
	WHERE UGR.GroupDefinitionID = @groupID
	
	DECLARE @AnnotationsMode INT
	DECLARE @MarkingType INT 
	
	SELECT @AnnotationsMode = I.AllowAnnotations, @MarkingType = I.MarkingType
	FROM dbo.Items I
	INNER JOIN dbo.GroupDefinitionItems GDI ON I.ID = GDI.ItemID
	WHERE GDI.GroupID = @groupID
	
	DECLARE @NumberUnmarked INT
	SELECT @NumberUnmarked = COUNT(UGR.ID)
	FROM UniqueGroupResponses UGR
	WHERE UGR.GroupDefinitionID = @groupID
		AND UGR.CI_Review = 0
		AND ParkedUserID IS NULL
		AND UGR.ConfirmedMark IS NULL
		AND (TokenId IS NULL OR (SELECT TS.userId FROM TokenStore TS WHERE TS.id = UGR.TokenId) = @userId)
	
	
	DECLARE @NumCIsToReview INT
	SELECT @NumCIsToReview = COUNT(CiMarks.UniqueGroupResponseId)
	FROM (
            SELECT AGM.UniqueGroupResponseId													AS UniqueGroupResponseID
				,AGM.UserId													                AS UserID
				,(ROW_NUMBER() OVER(PARTITION BY AGM.UniqueGroupResponseId ORDER BY AGM.timestamp DESC)) AS [Row]
			FROM dbo.AssignedGroupMarks AGM 
			WHERE AGM.GroupDefinitionID = @groupID AND AGM.markingMethodId = 5
		) CiMarks
	JOIN dbo.UniqueGroupResponses UGR ON UGR.ID = CiMarks.UniqueGroupResponseId 
			AND UGR.CI = 1 AND UGR.IsEscalated = 0 AND (UGR.CI_Review IS NULL OR UGR.CI_Review = 0)
	WHERE CiMarks.[Row] = 1 AND CiMarks.UserID <> @userId
	
	SELECT	@groupID																AS ID
			,GD.TotalMark 															AS TotalMark
			,GD.CIMarkingTolerance 													AS CITolerance
			,@AnnotationsMode														AS AnnotationsMode
			,@MarkingType															AS MarkingType	
			,@Responses																AS Responses
			,@UResponses															AS UniqueResponses
			,GD.NoOfCIsRequired														AS NumCIsRequired
			,@NumCIsCreated															AS NumCIsCreated
			,@NumCIsReviewed														AS NumCIsReviewed
			,@NumCIsToReview														AS NumCIsToReview
			,GD.IsReleased															AS Released
			,ISNULL(@NumberUnmarked, 0)												AS NumUnmarked
	FROM dbo.GroupDefinitions GD
	WHERE GD.ID = @groupID
END
GO
/****** Object:  StoredProcedure [dbo].[sm_QUALITYCONTROLSERVICE_getDetailsforItem_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Tom Gomersall
-- Create date: 18/04/11
-- Description:	Gets the number of responses and unique responses for an item, as well as the version number.
-- =============================================
CREATE PROCEDURE [dbo].[sm_QUALITYCONTROLSERVICE_getDetailsforItem_sp]
	-- Add the parameters for the stored procedure here
	@itemID INT,
	@groupID INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	DECLARE @ViewOnlyTemp BIT = (SELECT GDI.ViewOnly
								 FROM dbo.GroupDefinitionItems GDI
								 WHERE GDI.ItemID = @itemID 
								 AND GDI.GroupID = @groupID)
	
	SELECT	@itemID									AS 'ID'
			,I.Version								AS 'Version'
			,I.AllowAnnotations						AS 'AnnotationsMode'
			,I.CIMarkingTolerance					AS 'CITolerance'
			,@ViewOnlyTemp							AS 'ViewOnly'
			,I.MarkingType          				AS 'MarkingType'
			,I.TotalMark          					AS 'TotalMark'
			,COUNT(CR.UniqueResponseID)				AS 'Responses'
			,COUNT(DISTINCT UR.id)					AS 'UniqueResponses'
			
	FROM dbo.Items I
	INNER JOIN dbo.UniqueResponses UR ON UR.itemId = I.ID
	INNER JOIN dbo.CandidateResponses CR ON CR.UniqueResponseID = UR.id 
	WHERE I.ID = @itemID
	
	GROUP BY 
		I.ID
		,I.Version
		,I.AllowAnnotations
		,I.CIMarkingTolerance
		,I.MarkingType
		,I.TotalMark
END
GO
/****** Object:  StoredProcedure [dbo].[sm_QUALITYCONTROLSERVICE_GetExamAnnotationSchema_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Tom Gomersall
-- Create date: 14/04/2011
-- Description:	Gets the annotation schema for an exam
-- =============================================

CREATE PROCEDURE [dbo].[sm_QUALITYCONTROLSERVICE_GetExamAnnotationSchema_sp]
	-- Add the parameters for the stored procedure here
	@examID INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	SELECT ISNULL(E.AnnotationSchema, '')
	FROM  dbo.Exams E
	WHERE E.ID = @examID  
  
END
GO
/****** Object:  StoredProcedure [dbo].[sm_QUALITYCONTROLSERVICE_getExamIdForGroup_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		George Chernov
-- Create date: 16/04/13
-- Description:	Gets the exam ID for a group.
-- =============================================

CREATE PROCEDURE [dbo].[sm_QUALITYCONTROLSERVICE_getExamIdForGroup_sp]
	-- Add the parameters for the stored procedure here
	@groupID INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	SELECT GD.ExamID
	FROM dbo.GroupDefinitions GD
	WHERE GD.ID = @groupID
END
GO
/****** Object:  StoredProcedure [dbo].[sm_QUALITYCONTROLSERVICE_getExamIDforGroupResponse_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		George Chernov
-- Create date: 24/04/13
-- Description:	Gets the exam ID for a unique group response.
-- =============================================

CREATE PROCEDURE [dbo].[sm_QUALITYCONTROLSERVICE_getExamIDforGroupResponse_sp]
	@responseID INT
AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT GD.ExamID
	FROM dbo.GroupDefinitions GD
	INNER JOIN dbo.UniqueGroupResponses UGR	ON GD.ID = UGR.GroupDefinitionID
	WHERE UGR.ID = @responseID
END
GO
/****** Object:  StoredProcedure [dbo].[sm_QUALITYCONTROLSERVICE_getExamIdForItem_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Tom Gomersall
-- Create date: 13/04/11
-- Description:	Gets the exam ID for an item.
-- =============================================

CREATE PROCEDURE [dbo].[sm_QUALITYCONTROLSERVICE_getExamIdForItem_sp]
	-- Add the parameters for the stored procedure here
	@itemID INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	SELECT dbo.Items.ExamID
	FROM dbo.Items
	WHERE dbo.Items.ID = @itemID
END
GO
/****** Object:  StoredProcedure [dbo].[sm_QUALITYCONTROLSERVICE_getExamIDsforGroups_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Tom Gomersall
-- Create date: 27/06/11
-- Description:	Gets the exam IDs for a list of items.
-- =============================================
CREATE PROCEDURE [dbo].[sm_QUALITYCONTROLSERVICE_getExamIDsforGroups_sp]
	-- Add the parameters for the stored procedure here
	@groupsIds NVARCHAR(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	SELECT DISTINCT dbo.GroupDefinitions.ExamID
	FROM dbo.GroupDefinitions
	WHERE dbo.GroupDefinitions.ID IN (SELECT * FROM dbo.ParamsToList(@groupsIds))
END

GO
/****** Object:  StoredProcedure [dbo].[sm_QUALITYCONTROLSERVICE_getExamItemDefaults_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Tom Gomersall
-- Create date: 12/04/11
-- Description:	Gets the default values for items from the exam table.
-- =============================================
CREATE PROCEDURE [dbo].[sm_QUALITYCONTROLSERVICE_getExamItemDefaults_sp]
	-- Add the parameters for the stored procedure here
 @examID INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	SELECT	dbo.Exams.DefaultAllowAnnotationsInMarking								AS 'Annotations'
			,dbo.Exams.DefaultNoOfCIsRequired										AS 'CIsRequired'
			,dbo.Exams.DefaultTotalItemsInCompetencyRun								AS 'NumCIsInRun'
			,dbo.Exams.DefaultCorrectItemsWithinToleranceWithinCompetenceRunDefault	AS 'NumCorrectRequiredForRun'
			,dbo.Exams.DefaultMaximumCumulativeErrorWithinCompetenceRun				AS 'MaxErrorInRun'
			,dbo.Exams.DefaultProbabilityOfGettingCI								AS 'CIProbability'
			,dbo.Exams.DefaultMaximumResponseBeforePresentingCI						AS 'MaxItemsBetweenCIs'
			,dbo.Exams.DeafultMaxConsecutiveCIsThatCanBeMarkedOutOfTolerance		AS 'MaxConsecutiveIncorrectInRollingReview'
			,dbo.Exams.DefaultNumberOfCIsInRollingReview							AS 'NumCIsInRollingReview'
			,dbo.Exams.DefaultMaximumCIsMarkedOutOfToleranceInRollingReview			AS 'MaxTotalIncorrectInRollingReview'
	FROM dbo.Exams
	WHERE dbo.Exams.ID = @examID
END

GO
/****** Object:  StoredProcedure [dbo].[sm_QUALITYCONTROLSERVICE_getGroupForCICreation_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:	Vitaly Bevzik
-- Date:		14/04/2013
-- Description:	Adoptation for groups. Don't returns user response information
-- =============================================
CREATE PROCEDURE [dbo].[sm_QUALITYCONTROLSERVICE_getGroupForCICreation_sp]
-- Add the parameters for the stored procedure here
	@groupID INT,
	@userID INT,
	@tokenId INT,
	@hasCreatePermission BIT,
	@hasReviewPermission BIT
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;
    
    --Set the token to null for all responses that are currently checked out to this logged in user.
    UPDATE dbo.UniqueGroupResponses
    SET    dbo.UniqueGroupResponses.TokenId = NULL
    WHERE  dbo.UniqueGroupResponses.TokenId = @tokenId
    
    DECLARE @myResponseForReview     BIT=1
    
    DECLARE @myResponseId            INT=(
                --First try to get a response that needs to be reviewed.
                --It must be a CI that has not yet been reviewed, and its CI creation assigned mark must be from a different user.
                SELECT TOP(1)
                       UGR.id
                FROM   dbo.UniqueGroupResponses UGR
                WHERE  UGR.GroupDefinitionID = @groupID
                       AND UGR.CI = 1
                       AND UGR.CI_Review = 0
                       AND UGR.IsEscalated = 0
                       AND TokenId IS NULL
                       AND (
                               SELECT TOP(1) AGM.userID
                               FROM   dbo.AssignedGroupMarks AGM
                                      --inner join uniqueGroupResponseLinks UGRL on UGRL.UniqueGroupResponseID = AGM.uniqueGroupResponseId
                               WHERE  AGM.uniqueGroupResponseId = UGR.id
                                      AND AGM.markingMethodId = 5
                               ORDER BY
                                      AGM.timestamp DESC
                           )<>@userID
                ORDER BY
                       NEWID()
            )
    
    IF (@myResponseId IS NULL OR @hasReviewPermission=0)
    BEGIN
        --Either there is not an item that needs to be reviewed, or the user does not have the review permission,
        --so get an unmarked item that isn't already a CI.
        
        --The user must have the create permission to create a CI.
        IF (@hasCreatePermission=1)
        BEGIN
            SET @myResponseForReview = 0
            SET @myResponseId = (
                    SELECT TOP(1)
                           UGR.id
                    FROM   dbo.UniqueGroupResponses UGR
                    WHERE  UGR.GroupDefinitionID = @groupID
                           AND UGR.CI = 0
                           AND UGR.IsEscalated = 0
                           AND TokenId IS NULL
                           AND UGR.ConfirmedMark IS NULL
                    ORDER BY
                           NEWID()
                )
        END
    END
    
    --Set the token to check out the response.
    UPDATE dbo.UniqueGroupResponses
    SET    dbo.UniqueGroupResponses.tokenId = @tokenId
    WHERE  dbo.UniqueGroupResponses.id = @myResponseId
           AND dbo.UniqueGroupResponses.tokenId IS NULL
    
    IF (@@ROWCOUNT=1)
    BEGIN
            
        IF (@myResponseForReview=1)
        BEGIN  -- Returns only UniqueGroupResponse to be reviewed
			;WITH cteIsInConfirmedScript AS
			(
				SELECT CGR.UniqueGroupResponseID
					  ,1 AS IsInConfirmedScript
				FROM   dbo.CandidateGroupResponses CGR
					   INNER JOIN dbo.CandidateExamVersions CEV ON  CEV.ID = CGR.CandidateExamVersionID
				WHERE  CEV.ExamSessionState IN (6 ,7)
			) 
		
            SELECT TOP(1)
                   UGR.id          AS 'id'
                  ,UGR.CI          AS 'CI'
                  ,UGR.ScriptType  AS 'scriptType'
                  ,UGR.Feedback    AS 'Feedback'
                  ,CAST(ISNULL(CONFIRMEDSCRIPTS.IsInConfirmedScript ,0) AS BIT) AS 'IsInConfirmedScript' 
            FROM   dbo.UniqueGroupResponses UGR
				LEFT JOIN cteIsInConfirmedScript CONFIRMEDSCRIPTS ON CONFIRMEDSCRIPTS.UniqueGroupResponseID = UGR.ID
            WHERE  UGR.id = @myResponseId
        END
        ELSE
        BEGIN 
            SELECT TOP(1)
                   UGR.id          AS 'id'
                  ,UGR.CI          AS 'CI'
                  ,UGR.ScriptType  AS 'scriptType'
                  ,UGR.Feedback    AS 'Feedback'
                  ,CAST(0 AS BIT)  AS 'IsInConfirmedScript' 
            FROM   dbo.UniqueGroupResponses UGR
            WHERE  UGR.id = @myResponseId
        END
    END
END


GO
/****** Object:  StoredProcedure [dbo].[sm_QUALITYCONTROLSERVICE_getGroupsForExam_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Chernov George
-- Create date: 15/04/13
-- Description:	Gets all groups for an exam, including their number of responses and the number of CIs that have been created and reviewed
			--  and returns MarkingType and AllowAnnotations for one item in group 
			-- @ResultSetIndicator is a parameter to get the first or second table in SQL. 
			-- Default value is 0 - return both tables
			-- Value is 1 - return the first tables
			-- Value is 0 - return the second tables
			
-- Modified:	George Chernov
-- Date:		8/5/2013
-- Description:	Return appropriate items for groups
-- =============================================
CREATE PROCEDURE [dbo].[sm_QUALITYCONTROLSERVICE_getGroupsForExam_sp]
	@examID INT,
	@tokenStoreId INT,
	@ResultSetIndicator smallint = 0
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @Groups TABLE(GroupID INT)
		
	INSERT @Groups(GroupID)
		SELECT GD.ID AS GroupID
		FROM dbo.GroupDefinitions GD
		WHERE GD.ExamID = @examID
			AND GD.ID IN 
				(
					SELECT GDSCR.GroupDefinitionID 
					FROM dbo.GroupDefinitionStructureCrossRef GDSCR
						INNER JOIN dbo.ExamVersionStructures EVS ON EVS.ID = GDSCR.ExamVersionStructureID
					WHERE GDSCR.Status = 0 -- 0 = Active
						AND EVS.StatusID = 0 -- 0 = Released
				)	
								
	DECLARE @ScriptType SMALLINT = (SELECT TOP(1) ScriptType
			 FROM dbo.CandidateExamVersions cev
			 INNER JOIN dbo.ExamVersions ev
				ON cev.ExamVersionID = ev.ID
			 WHERE ev.ExamID = @examID)
			 
	DECLARE @MaxItems TABLE (GroupID INT, ItemID INT)
	
	INSERT @MaxItems(GroupID, ItemID)
	SELECT 
			G.GroupID AS GroupID,
			MAX(GDI.ItemID) AS ItemID
		FROM @Groups G
		INNER JOIN GroupDefinitionItems GDI ON GDI.GroupID = G.GroupID
		GROUP BY G.GroupID
	
	if(@ResultSetIndicator = 0	OR @ResultSetIndicator = 1)
	BEGIN
	SELECT	GD.ID																	AS ID
			,GD.Name																AS Name
			,GD.TotalMark 															AS TotalMark
			,GD.CIMarkingTolerance 													AS CITolerance	
			,GD.NoOfCIsRequired														AS NumCIsRequired
			,UGRT.UniqueResponses													AS UniqueResponses					
			,GD.IsReleased															AS Released
			,@ScriptType															AS ScriptType
			,MI.ItemID																AS ItemId
			,I.MarkingType															AS MarkingType
            ,I.AllowAnnotations														AS AllowAnnotations  
			,GD.IsAutomaticGroup													AS IsAutomaticGroup 
			,UGRT.NumCIsCreated														AS NumCIsCreated
			,UGRT.NumCIsReviewed													AS NumCIsReviewed
			,ISNULL(AUGR.Unmarked, 0)												AS NumUnmarked
	 FROM @Groups G 
			 INNER JOIN dbo.GroupDefinitions GD ON G.GroupID = GD.ID
			 INNER JOIN @MaxItems MI ON G.GroupID = MI.GroupID 
			 INNER JOIN dbo.Items I ON MI.ItemID = I.ID
             INNER JOIN
             (
             	SELECT 
					G.GroupID																	AS GroupID
					,COUNT(UGR.ID)																AS UniqueResponses
					,COUNT(NULLIF(UGR.CI, 0))													AS NumCIsCreated
					,COUNT(NULLIF(UGR.CI_Review, 0))											AS NumCIsReviewed
				FROM @Groups G
				LEFT JOIN dbo.UniqueGroupResponses UGR ON G.GroupID = UGR.GroupDefinitionID
				GROUP BY 
				G.GroupID
             ) AS UGRT
				ON G.GroupID = UGRT.GroupID
             LEFT JOIN 
             (
				SELECT COUNT(UniqueGroupResponseId) as Unmarked
					, GroupDefinitionID as GroupId
					FROM dbo.getAvailableUniqueGroupResponses_fn(@tokenStoreId)
					GROUP BY GroupDefinitionID
             ) AS AUGR
				ON AUGR.GroupId = G.GroupID
	ORDER BY GD.ID 
	END
	
	if(@ResultSetIndicator = 0	OR @ResultSetIndicator = 2)
	BEGIN
	SELECT 
		 GDI.GroupID
		,GDI.ItemID
		,GDI.ViewOnly
		,GDI.GroupDefinitionItemOrder
		,I.TotalMark
		,I.CIMarkingTolerance
		,I.MarkingType
		,I.ExternalItemID AS ExternalItemID
		,I.LayoutName AS LayoutName
		,I.Version AS Version
		,I.ExternalItemName AS Name
		FROM @Groups G
			JOIN dbo.GroupDefinitionItems GDI ON GDI.GroupID = G.GroupID	 
			JOIN dbo.Items I ON I.ID = GDI.ItemID		
		ORDER BY 
			 GDI.GroupID
			,GDI.GroupDefinitionItemOrder
	END
			
END

GO
/****** Object:  StoredProcedure [dbo].[sm_QUALITYCONTROLSERVICE_getResponseForCG_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Vitaly Bevzik
-- Create date: 8/5/13
-- Description:	The user and marker response for a control group.
-- =============================================
CREATE PROCEDURE [dbo].[sm_QUALITYCONTROLSERVICE_getResponseForCG_sp]
-- Add the parameters for the stored procedure here
	@uniqueGroupResponseId INT
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;
    
    --select only 1 assigned mark for one unique response
    DECLARE @assignedMarkId BIGINT
    
    SET @assignedMarkId = (
            SELECT TOP 1 AGM.ID
            FROM   dbo.UniqueGroupResponses UGR
                   INNER JOIN dbo.AssignedGroupMarks AGM
                        ON  UGR.ID = AGM.UniqueGroupResponseId
            WHERE  UGR.ID = @uniqueGroupResponseId
                   AND UGR.CI = 1
                   AND AGM.IsConfirmedMark = 1
            ORDER BY
                   AGM.Timestamp DESC
        )
    
    -- Select Unique Group Response level data --------------------------------------------
    SELECT TOP 1 
           GD.ID              AS ID
          ,GD.TotalMark       AS TotalMark
          ,UGR.ConfirmedMark  AS ConfirmedMark
          ,UGR.ScriptType     AS ScriptType
          ,UGR.Feedback       AS Feedback
    FROM   dbo.UniqueGroupResponses UGR
           INNER JOIN dbo.GroupDefinitions GD ON  GD.ID = UGR.GroupDefinitionID
    WHERE  UGR.ID = @uniqueGroupResponseId
    ----------------------------------------------------------------------------------------
    
    -- Select Unique Response level data ---------------------------------------------
    SELECT Items.ID                    AS ItemId
          ,Items.ExternalItemName      AS ItemName
          ,Items.ExternalItemID        AS ExternalID
          ,Items.Version               AS ItemVersion
          ,Items.LayoutName            AS LayoutName
          ,Items.TotalMark             AS TotalMark
          ,Items.MarkingType           AS MarkingType
          ,Items.AllowAnnotations      AS AllowAnnotations
          ,AIM.AnnotationData          AS AnnotationData
          ,AIM.AssignedMark            AS AssignedMark
          ,AIM.MarkedMetadataResponse  AS MarkedMetadataResponse
          ,UR.id                       AS ID
          ,UR.responseData             AS Response
          ,GDFI.ViewOnly               AS ViewOnly
    FROM   dbo.UniqueGroupResponseLinks UGRL
           INNER JOIN dbo.UniqueResponses UR ON  UR.id = UGRL.UniqueResponseId
           INNER JOIN dbo.Items Items ON  UR.itemId = Items.ID
           INNER JOIN dbo.UniqueGroupResponses UGR ON  UGR.ID = UGRL.UniqueGroupResponseID
           INNER JOIN dbo.GroupDefinitions GD ON  GD.ID = UGR.GroupDefinitionID
           INNER JOIN dbo.GroupDefinitionItems GDFI ON  GDFI.ItemID = UR.itemId AND GDFI.GroupID = GD.ID
           LEFT JOIN dbo.AssignedItemMarks AIM ON  AIM.UniqueResponseId = UR.id AND AIM.GroupMarkId = @assignedMarkId
    WHERE  UGRL.UniqueGroupResponseID = @uniqueGroupResponseId
    ORDER BY
           GDFI.GroupDefinitionItemOrder
    ----------------------------------------------------------------------------------------
END
GO
/****** Object:  StoredProcedure [dbo].[sm_QUALITYCONTROLSERVICE_getResponsesForCICreation_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sm_QUALITYCONTROLSERVICE_getResponsesForCICreation_sp]
-- Add the parameters for the stored procedure here
	@groupID INT,
	@uniqGroupResponseId INT,
	@IsReview BIT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	IF (@IsReview = 1)
	BEGIN
	    --The response needs to be reviewed, so the assigned mark info should be included in the select
	    --select only 1 assigned mark for one unique response
	    DECLARE @assignedMarkId BIGINT = (
	                SELECT TOP 1         ID
	                FROM   dbo.AssignedGroupMarks
	                WHERE  UniqueGroupResponseId = @uniqGroupResponseId
	                       AND MarkingMethodId = 5
	                ORDER BY
	                       TIMESTAMP     DESC
	            )
	    
	    --The response needs to be reviewed, so the assigned mark info should be included in the select
	    SELECT UR.id                      AS 'id',
	           UR.responseData            AS 'response',
	           AM.assignedMark            AS 'assignedMark',
	           AM.annotationData          AS 'annotationData',
	           AM.markedMetadataResponse  AS 'markedMetadataResponse',
	           I.ID                       AS 'ItemId',
	           I.Version                  AS 'ItemVersion',
	           I.LayoutName               AS 'layoutName',
	           I.AllowAnnotations         AS 'AllowAnnotations',
	           I.TotalMark                AS 'TotalMark',
	           I.ExternalItemID           AS 'ExternalID',
	           GDI.ViewOnly               AS 'ViewOnly'
	    FROM   dbo.UniqueGroupResponseLinks UGRL
	           INNER JOIN dbo.UniqueResponses UR
	                ON  UR.id = UGRL.UniqueResponseId
	           LEFT JOIN dbo.AssignedItemMarks AM
	                ON  UGRL.UniqueResponseId = AM.uniqueResponseId
	                AND AM.GroupMarkId = @assignedMarkId
	           INNER JOIN dbo.Items I
	                ON  I.ID = UR.itemId
	           INNER JOIN dbo.GroupDefinitionItems GDI
	                ON  GDI.ItemID = UR.itemId
	                AND GDI.GroupID = @groupID
	    WHERE  UGRL.UniqueGroupResponseID = @uniqGroupResponseId
	    ORDER BY
	           GDI.GroupDefinitionItemOrder ASC
	END
	ELSE
	BEGIN
	    DECLARE @resultTable TABLE(
	                id BIGINT,
	                response XML,
	                assignedMark DECIMAL(18, 10),
	                annotationData NVARCHAR(MAX),
	                markedMetadataResponse XML,
	                ItemId INT,
	                ItemVersion INT,
	                layoutName NVARCHAR(50),
	                AllowAnnotations INT,
	                TotalMark DECIMAL(18, 10),
	                ExternalID NVARCHAR(20),
	                ViewOnly BIT,
	                GroupDefinitionItemOrder INT
	            )
	    
	    --The responses has not been marked, so the assigned mark info is left empty
	    INSERT INTO @resultTable
	    SELECT UR.id                      AS 'id',
	           UR.responseData            AS 'response',
	           CONVERT(DECIMAL(18, 10), 0) AS 'assignedMark',
	           '' AS 'annotationData',
	           UR.markedMetadataResponse  AS 'markedMetadataResponse',
	           I.ID                       AS 'ItemId',
	           I.Version                  AS 'ItemVersion',
	           I.LayoutName               AS 'LayoutName',
	           I.AllowAnnotations         AS 'AllowAnnotations',
	           I.TotalMark                AS 'TotalMark',
	           I.ExternalItemID           AS 'ExternalID',
	           GDI.ViewOnly               AS 'ViewOnly',
	           GDI.GroupDefinitionItemOrder AS 'GroupDefinitionItemOrder'
	    FROM   dbo.UniqueGroupResponseLinks UGRL
	           INNER JOIN dbo.UniqueResponses UR
	                ON  UR.id = UGRL.UniqueResponseId
	           INNER JOIN dbo.Items I
	                ON  I.ID = UR.itemId
	           INNER JOIN dbo.GroupDefinitionItems GDI
	                ON  GDI.ItemID = UR.itemId
	                AND GDI.GroupID = @groupID
	    WHERE  UGRL.UniqueGroupResponseID = @uniqGroupResponseId
	    ORDER BY
	           GDI.GroupDefinitionItemOrder ASC 
	    --Get the latest mark scheme for the items
	    --Set the mark scheme for the unique response that is to be returned.
	    UPDATE dbo.UniqueResponses
	    SET    markSchemeId = result.shemaId
	    FROM   (
	               SELECT MS.id  AS 'shemaId',
	                      rt.id  AS 'uniqResponseID'
	               FROM   @resultTable rt
	                      INNER JOIN dbo.Items I
	                           ON  I.ID = rt.itemId
	                      INNER JOIN dbo.MarkSchemes MS
	                           ON  MS.ExternalItemID = I.ExternalItemID
	               WHERE  MS.[version] = (
	                          SELECT MAX([version])
	                          FROM   dbo.MarkSchemes MS2
	                          WHERE  MS2.ExternalItemID = I.ExternalItemID
	                      )
	           ) AS result
	    WHERE  UniqueResponses.id = result.uniqResponseID
	    
	    SELECT id,
	           response,
	           assignedMark,
	           annotationData,
	           markedMetadataResponse,
	           ItemId,
	           ItemVersion,
	           layoutName,
	           AllowAnnotations,
	           TotalMark,
	           ExternalID,
	           ViewOnly,
	           GroupDefinitionItemOrder
	    FROM   @resultTable
	    ORDER BY
	           GroupDefinitionItemOrder
	END
END

/*----------------------------------------------------------------------------------------------------------------
------    ALTERING sm_STATEMANAGEMENT_UpdateMarkSchemes_sp to extend length of @ExternalItemID param
----------------------------------------------------------------------------------------------------------------*/

GO
/****** Object:  StoredProcedure [dbo].[sm_QUALITYCONTROLSERVICE_getResponsesForGroup_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Anton Burdyugov
-- Create date: 10/06/13
-- Description:	Gets the group and item information.
-- =============================================
CREATE PROCEDURE [dbo].[sm_QUALITYCONTROLSERVICE_getResponsesForGroup_sp]
    @groupId BIGINT
AS 
BEGIN
    SET NOCOUNT ON;

	--Select Group information
    SELECT  GD.ID ,
            GD.Name
    FROM    dbo.GroupDefinitions GD
    WHERE   GD.ID = @groupId    

	-- Select item information
    SELECT  I.ID AS ItemId ,
            I.Version AS ItemVersion ,
            I.LayoutName AS LayoutName ,
            I.ExternalItemID AS ExternalID,
			GDI.ViewOnly AS ViewOnly
    FROM    dbo.GroupDefinitionItems GDI
            INNER JOIN dbo.Items I ON I.Id = GDI.ItemID
    WHERE   GDI.GroupID = @groupId    
    ORDER BY GDI.GroupDefinitionItemOrder ASC

END
GO
/****** Object:  StoredProcedure [dbo].[sm_QUALITYCONTROLSERVICE_modifyDetailsForGroup_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		George Chernov
-- Create date: 18/04/13
-- Description:	Updates a group with amended values.
-- =============================================
CREATE PROCEDURE [dbo].[sm_QUALITYCONTROLSERVICE_modifyDetailsForGroup_sp]
	-- Add the parameters for the stored procedure here
	@groupId INT,
	@ciTolerance DECIMAL(18,10),
	@minCIs	INT,
	@annotationType INT,
	@markingType INT,
	@userId INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	BEGIN TRY
		BEGIN TRANSACTION;
		
			DECLARE @IsAutoGroup BIT = (select IsAutomaticGroup from GroupDefinitions where ID = @groupId)

			IF(@IsAutoGroup = 1)
				BEGIN
					UPDATE dbo.GroupDefinitions
					SET
						dbo.GroupDefinitions.CIMarkingTolerance = @ciTolerance
						,dbo.GroupDefinitions.NoOfCIsRequired = @minCIs
					WHERE dbo.GroupDefinitions.ID = @groupId
					
					DECLARE @currentMarkingType INT = (Select MarkingType from Items where ID in (Select ItemID from GroupDefinitionItems where GroupID = @groupId))
					
					IF (@currentMarkingType <> 1 AND @markingType = 1)
						BEGIN
							IF(@currentMarkingType = 0 OR @currentMarkingType = 2)
								BEGIN
									--UPDATE dbo.AssignedGroupMarks
									--SET IsConfirmedMark = 0
									--WHERE
									--	MarkingMethodId = 12
									--	AND
									--	(
									--		SELECT GroupDefinitionID
									--		FROM UniqueGroupResponses
									--		WHERE ID = uniqueGroupResponseId
									--	) = @groupId
									
									--UPDATE dbo.UniqueGroupResponses
									--SET confirmedMark = NULL
									--WHERE GroupDefinitionID = @groupId
									
									declare @UniqueGroupResponses nvarchar(max)

									select @UniqueGroupResponses = coalesce(@UniqueGroupResponses + ', ', '') + cast(ID as nvarchar(20))
									from UniqueGroupResponses
									where GroupDefinitionID = @groupId 
									
									exec sm_QUOTAMANAGEMENTSERVICE_discardMarkedResponses_sp @UniqueGroupResponses, @userId 
									
								END
							ELSE IF(@currentMarkingType = 3)
								BEGIN
									DECLARE @uniqueGroupResponsesToUpdate TABLE(UniqueGroupResponseId bigint)
									INSERT @uniqueGroupResponsesToUpdate
										SELECT ID
										FROM dbo.UniqueGroupResponses
										WHERE
											GroupDefinitionID = @groupId
											AND
											EXISTS(
												SELECT ID
												FROM dbo.AssignedGroupMarks
												WHERE 
													MarkingMethodId = 13
													AND
													IsConfirmedMark = 1
													AND
													UniqueGroupResponseId = UniqueGroupResponses.ID
											)

									UPDATE dbo.UniqueGroupResponses
										SET confirmedMark = NULL
										WHERE ID IN (SELECT UniqueGroupResponseId FROM @uniqueGroupResponsesToUpdate)
									
									UPDATE dbo.AssignedGroupMarks
									SET IsConfirmedMark = 0
									WHERE
										markingMethodId = 13
										AND
										(
											SELECT GroupDefinitionID
											FROM UniqueGroupResponses
											WHERE ID = UniqueGroupResponseId
										) = @groupId

									UPDATE dbo.UniqueGroupResponseLinks
										SET confirmedMark = NULL
										WHERE UniqueGroupResponseId IN (SELECT UniqueGroupResponseId FROM @uniqueGroupResponsesToUpdate)

								END
							END
							
							UPDATE dbo.Items
							SET
								MarkingType = @markingType,
								AllowAnnotations = @annotationType,
								CIMarkingTolerance = @ciTolerance
							WHERE dbo.Items.ID in (select ItemID from GroupDefinitionItems where GroupDefinitionItems.GroupID = @groupId)
							
						END
			ELSE
				BEGIN
					UPDATE dbo.GroupDefinitions
					SET
						dbo.GroupDefinitions.CIMarkingTolerance = @ciTolerance
						,dbo.GroupDefinitions.NoOfCIsRequired = @minCIs
					WHERE dbo.GroupDefinitions.ID = @groupId
				END
		COMMIT TRANSACTION;
	END TRY
	BEGIN CATCH
			ROLLBACK TRANSACTION

			DECLARE @myErrorNum INT = ERROR_NUMBER()
			DECLARE @myMessage NVARCHAR(MAX) = ERROR_MESSAGE()
			DECLARE @myFullMessage NVARCHAR(MAX) = CONVERT(NVARCHAR(10), @myErrorNum) + ':' + @myMessage

			RAISERROR (@myFullMessage, 16, 1)
		END CATCH
END
GO
/****** Object:  StoredProcedure [dbo].[sm_QUALITYCONTROLSERVICE_modifyDetailsforItem_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Tom Gomersall
-- Create date: 26/04/11
-- Description:	Updates an item with amended values.

-- Modified:	George Chernov
-- Date:		13/5/2013
-- Description:	Update only ci tolerance and annotations
-- =============================================
CREATE PROCEDURE [dbo].[sm_QUALITYCONTROLSERVICE_modifyDetailsforItem_sp]
	-- Add the parameters for the stored procedure here
	@itemID INT,
	@ciTolerance DECIMAL(18,10),
	@annotations INT
AS
BEGIN
	SET NOCOUNT ON;
	
		BEGIN
			UPDATE dbo.Items
			SET
				dbo.Items.CIMarkingTolerance = @ciTolerance
				,dbo.Items.AllowAnnotations = @annotations
			WHERE dbo.Items.ID = @itemID
		END
END
GO
/****** Object:  StoredProcedure [dbo].[sm_QUALITYCONTROLSERVICE_releaseAllGroupsForExam_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		George Chernov
-- Create date: 24/04/13
-- Description:	Releases all the groups for an exam from quality control.
-- =============================================
CREATE PROCEDURE [dbo].[sm_QUALITYCONTROLSERVICE_releaseAllGroupsForExam_sp]

	@examID INT
AS
BEGIN

	SET NOCOUNT ON;
	
	UPDATE dbo.GroupDefinitions
	SET dbo.GroupDefinitions.IsReleased = 1
	WHERE dbo.GroupDefinitions.ExamID = @examID 
END

GO
/****** Object:  StoredProcedure [dbo].[sm_QUALITYCONTROLSERVICE_releaseGroups_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		George Chernov
-- Create date: 23/04/13
-- Description:	Releases groups from quality control.
-- =============================================
CREATE PROCEDURE [dbo].[sm_QUALITYCONTROLSERVICE_releaseGroups_sp]
	-- Add the parameters for the stored procedure here
	@groupsIds NVARCHAR(MAX)
AS
BEGIN

	SET NOCOUNT ON;
	
	UPDATE dbo.GroupDefinitions
	SET dbo.GroupDefinitions.IsReleased = 1
	WHERE dbo.GroupDefinitions.ID IN (SELECT * FROM dbo.ParamsToList(@groupsIds))
	
	IF (@@ROWCOUNT < 1)
		BEGIN
			return 50002;
		END
END
GO
/****** Object:  StoredProcedure [dbo].[sm_QUALITYCONTROLSERVICE_ReviewControlItem_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sm_QUALITYCONTROLSERVICE_ReviewControlItem_sp]
-- Add the parameters for the stored procedure here
	@groupResponseId BIGINT,
	@userID INT,
	@tokenId INT,
	@agree BIT,
	@canEditFeedback BIT,
	@feedback NVARCHAR(MAX)
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;
    
    DECLARE @lastAssignedGroupMarkId BIGINT
    IF (
           (
               SELECT dbo.UniqueGroupResponses.tokenId
               FROM   dbo.UniqueGroupResponses
               WHERE  dbo.UniqueGroupResponses.id = @groupResponseId
           )=@tokenID
       )
    BEGIN
        --The response is checked out to this user, so the marker response can be used.
        
        BEGIN TRY
            --Get the ID of the AssignedGroupMarks entry for the creation of the CI. This is the most recent entry whose marking method is 5.
            DECLARE @myCICreationAssignedMarkId BIGINT=(
                        SELECT TOP(1) ID
                        FROM   dbo.AssignedGroupMarks
                        WHERE  dbo.AssignedGroupMarks.UniqueGroupResponseId = @groupResponseId
                               AND dbo.AssignedGroupMarks.MarkingMethodId = 5
                        ORDER BY
                               dbo.AssignedGroupMarks.Timestamp DESC
                    )
            
            DECLARE @groupId INT=(
                        SELECT GroupDefinitionID
                        FROM   UniqueGroupResponses
                        WHERE  ID = @groupResponseID
                    )
            
            BEGIN TRANSACTION
            
            IF (@agree=1)
            BEGIN
                --Set the review flag on the unique response and set its confirmed mark from the assigned mark table.
                UPDATE UGR
                SET    UGR.CI_Review = 1
                      ,UGR.confirmedMark = (
                           SELECT AssignedMark
                           FROM   dbo.AssignedGroupMarks
                           WHERE  dbo.AssignedGroupMarks.id = @myCICreationAssignedMarkId
                       )
                      ,UGR.Feedback = CASE @canEditFeedback
                                           WHEN 1 THEN @feedback
                                           ELSE UGR.Feedback
                                      END
                FROM   dbo.UniqueGroupResponses UGR
                WHERE  UGR.id = @groupResponseId
                
                UPDATE UGRL
                SET    UGRL.ConfirmedMark = AIM.AssignedMark
                      ,UGRL.MarkedMetadataResponse = AIM.MarkedMetadataResponse
                FROM   dbo.UniqueGroupResponseLinks UGRL
                       INNER JOIN AssignedItemMarks AIM
                            ON  AIM.UniqueResponseId = UGRL.UniqueResponseId
                WHERE  UGRL.UniqueGroupResponseID = @groupResponseID
                       AND AIM.GroupMarkId = @myCICreationAssignedMarkId
                
                --Insert a new entry in the AssignedGroupMarks table, which uses the mark and annotations from the existing
                --one from when it was first made into a CI.
                INSERT INTO dbo.AssignedGroupMarks
                  (
                    [UserId]
                   ,[UniqueGroupResponseId]
                   ,[Timestamp]
                   ,[AssignedMark]
                   ,[IsConfirmedMark]
                   ,[MarkingDeviation]
                   ,[MarkingMethodId]
                   ,[GroupDefinitionID]
                  )
                SELECT @userID
                      ,AssignedGroupMarks.UniqueGroupResponseId
                      ,GETDATE()
                      ,dbo.AssignedGroupMarks.AssignedMark
                      ,0
                      ,0
                      ,6
                      ,@groupId
                FROM   dbo.AssignedGroupMarks
                WHERE  dbo.AssignedGroupMarks.id = @myCICreationAssignedMarkId
                
                --Copy 	AssignedItemMarks from CI	
                SET @lastAssignedGroupMarkId = SCOPE_IDENTITY()
                INSERT INTO dbo.AssignedItemMarks
                  (
                    [GroupMarkId]
                   ,[UniqueResponseId]
                   ,[AnnotationData]
                   ,[AssignedMark]
                   ,[MarkingDeviation]
                   ,[MarkedMetadataResponse]
                  )
                SELECT @lastAssignedGroupMarkId
                      ,[UniqueResponseId]
                      ,[AnnotationData]
                      ,[AssignedMark]
                      ,[MarkingDeviation]
                      ,[MarkedMetadataResponse]
                FROM   AssignedItemMarks
                WHERE  [GroupMarkId] = @myCICreationAssignedMarkId
                
                --Update the CG creation entry to set it to be the confirmed mark
                UPDATE dbo.AssignedGroupMarks
                SET    dbo.AssignedGroupMarks.IsConfirmedMark = 1
                WHERE  dbo.AssignedGroupMarks.id = @myCICreationAssignedMarkId
            END
            ELSE
            BEGIN
                --Change the response to no longer be a CI, and set its confirmed mark from the CI creation assigned mark entry.
                UPDATE dbo.UniqueGroupResponses
                SET    dbo.UniqueGroupResponses.CI = 0
                      ,dbo.UniqueGroupResponses.CI_Review = 0
                      ,dbo.UniqueGroupResponses.confirmedMark = (
                           SELECT AssignedMark
                           FROM   dbo.AssignedGroupMarks
                           WHERE  dbo.AssignedGroupMarks.id = @myCICreationAssignedMarkId
                       )
                      ,dbo.UniqueGroupResponses.Feedback = NULL
                WHERE  dbo.UniqueGroupResponses.id = @groupResponseId
                -- update UniqueGroupResponseLinks
                UPDATE UGRL
                SET    UGRL.ConfirmedMark = AIM.AssignedMark
                      ,UGRL.MarkedMetadataResponse = AIM.MarkedMetadataResponse
                FROM   UniqueGroupResponseLinks UGRL
                       INNER JOIN AssignedItemMarks AIM
                            ON  AIM.UniqueResponseId = UGRL.UniqueResponseId
                WHERE  UGRL.UniqueGroupResponseID = @groupResponseID
                       AND AIM.GroupMarkId = @myCICreationAssignedMarkId
                
                --Insert entry into the AssignedGroupMarks table for the disagreement
                INSERT INTO dbo.AssignedGroupMarks
                  (
                    [UserId]
                   ,[UniqueGroupResponseId]
                   ,[Timestamp]
                   ,[AssignedMark]
                   ,[IsConfirmedMark]
                   ,[MarkingDeviation]
                   ,[MarkingMethodId]
                   ,[GroupDefinitionID]
                  )
                SELECT [UserId]
                      ,AssignedGroupMarks.UniqueGroupResponseId
                      ,GETDATE()
                      ,dbo.AssignedGroupMarks.AssignedMark
                      ,1
                      ,0
                      ,7
                      ,@groupId
                FROM   dbo.AssignedGroupMarks
                WHERE  dbo.AssignedGroupMarks.id = @myCICreationAssignedMarkId
                
                --Copy 	AssignedItemMarks from CI
                SET @lastAssignedGroupMarkId = SCOPE_IDENTITY()
                INSERT INTO dbo.AssignedItemMarks
                  (
                    [GroupMarkId]
                   ,[UniqueResponseId]
                   ,[AnnotationData]
                   ,[AssignedMark]
                   ,[MarkingDeviation]
                   ,[MarkedMetadataResponse]
                  )
                SELECT @lastAssignedGroupMarkId
                      ,[UniqueResponseId]
                      ,[AnnotationData]
                      ,[AssignedMark]
                      ,[MarkingDeviation]
                      ,[MarkedMetadataResponse]
                FROM   AssignedItemMarks
                WHERE  [GroupMarkId] = @myCICreationAssignedMarkId
            END
            
            COMMIT TRANSACTION
            
            --Set the token to null for all responses that are currently checked out to this logged in user.
            UPDATE dbo.UniqueGroupResponses
            SET    dbo.UniqueGroupResponses.tokenId = NULL
            WHERE  dbo.UniqueGroupResponses.tokenId = @tokenId
        END TRY
        BEGIN CATCH
            ROLLBACK TRANSACTION
            DECLARE @myErrorNum INT=ERROR_NUMBER()
            DECLARE @myMessage NVARCHAR(MAX)=ERROR_MESSAGE()
            DECLARE @myFullMessage NVARCHAR(MAX)=CONVERT(NVARCHAR(10) ,@myErrorNum) 
                   +':'+@myMessage
            
            RAISERROR (@myFullMessage ,16 ,1)
        END CATCH
    END
    ELSE
    BEGIN
        --The response is not checked out to this user
		return 50001;
    END
END
GO
/****** Object:  StoredProcedure [dbo].[sm_QUALITYCONTROLSERVICE_revokeCI_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Tom Gomersall
-- Create date: 24/05/11
-- Description:	Revokes a CI. Changes the unique response back to being a normal item, but keeps the confirmed mark.

-- Modified:	Tom Gomersall
-- Date:		03/08/2012
-- Description:	Added the marked metadata response to the insert into the assignedMarks table.

-- Modified:	George Chernov
-- Date:		24/04/2013
-- Description:	Revoke a control group.

-- Modified:	Roman Tseyko
-- Date:		2013-06-07 15:51:40.740
-- Description:	changed way of selecting mark for CI.
-- =============================================


CREATE PROCEDURE [dbo].[sm_QUALITYCONTROLSERVICE_revokeCI_sp]
-- Add the parameters for the stored procedure here
	@responseID BIGINT,
	@userID INT
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;
    
    BEGIN TRY
        BEGIN TRANSACTION
        
        --Set the response back to being a normal response
        UPDATE UGR 
        SET    UGR.CI = 0
              ,UGR.CI_review = 0
              ,UGR.Feedback = NULL
        FROM dbo.UniqueGroupResponses UGR
        WHERE  UGR.ID = @responseID
        
        --Get the ID of the AssignedMarks entry for the creation of the CI
        DECLARE @myCICreationAssignedResponseID BIGINT=(
                    SELECT TOP(1) AGM.ID
                    FROM   dbo.AssignedGroupMarks AGM
                    WHERE  AGM.UniqueGroupResponseId = @responseID
                           AND AGM.markingMethodId = 5
                           AND AGM.IsConfirmedMark = 1
                    ORDER BY
                           AGM.Timestamp DESC
                )
        
        DECLARE @groupId INT=(
                    SELECT UGR.GroupDefinitionID
                    FROM   dbo.UniqueGroupResponses UGR
                    WHERE  UGR.ID = @responseID
                )
        
        UPDATE AGM
        SET    AGM.IsConfirmedMark = 0
        FROM dbo.AssignedGroupMarks AGM
        WHERE  AGM.ID = @myCICreationAssignedResponseID
        
        INSERT INTO dbo.AssignedGroupMarks
          (
            [UserId]
           ,[UniqueGroupResponseId]
           ,[Timestamp]
           ,[AssignedMark]
           ,[IsConfirmedMark]
           ,[Disregarded]
           ,[MarkingDeviation]
           ,[MarkingMethodId]
           ,[GroupDefinitionID]
           ,[IsActualMarking]
          )
        SELECT @userID
              ,@responseID
              ,GETDATE()
              ,AGM.AssignedMark
              ,1
              ,0
              ,0
              ,4
              ,@groupId
              ,0
        FROM   dbo.AssignedGroupMarks AGM
        WHERE  AGM.id = @myCICreationAssignedResponseID
        
        DECLARE @newGroupMarkId BIGINT=(
                    SELECT SCOPE_IDENTITY()
                )
        
        INSERT INTO dbo.AssignedItemMarks
          (
            [GroupMarkId]
           ,[UniqueResponseId]
           ,[AnnotationData]
           ,[AssignedMark]
           ,[MarkingDeviation]
           ,[MarkedMetadataResponse]
          )
        SELECT @newGroupMarkId
              ,[UniqueResponseId]
              ,[AnnotationData]
              ,[AssignedMark]
              ,[MarkingDeviation]
              ,[MarkedMetadataResponse]
        FROM   dbo.AssignedItemMarks AIM
        WHERE  AIM.[GroupMarkId] = @myCICreationAssignedResponseID 
        
        ------ update QuotaManagement ---------
        UPDATE QM
        SET    QM.Within1CIofSuspension = dbo.sm_checkUser1CiawayFromSuspend(@userID ,@groupId)
        FROM   dbo.QuotaManagement QM
        WHERE  QM.GroupId = @groupId
               AND QM.UserId = @userID
        ---------------------------------------
        COMMIT TRANSACTION
    END TRY
    BEGIN CATCH
        ROLLBACK TRANSACTION
        --Standard unable to update error
        return 50002
    END CATCH
END
GO
/****** Object:  StoredProcedure [dbo].[sm_QUALITYCONTROLSERVICE_SaveFeedback_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sm_QUALITYCONTROLSERVICE_SaveFeedback_sp]
-- Add the parameters for the stored procedure here
	@groupResponseId BIGINT,
	@feedback NVARCHAR(MAX)
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;
    
    
    UPDATE UGR
    SET    UGR.Feedback = @feedback
    FROM   dbo.UniqueGroupResponses UGR
    WHERE  UGR.id = @groupResponseId
END
GO
/****** Object:  StoredProcedure [dbo].[sm_QUALITYCONTROLSERVICE_setCIReviewModeForExam_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Tom Gomersall
-- Create date: 28/04/11
-- Description:	Sets the auto CI review mode for an exam.
-- =============================================
CREATE PROCEDURE [dbo].[sm_QUALITYCONTROLSERVICE_setCIReviewModeForExam_sp]
	-- Add the parameters for the stored procedure here
 @examID INT,
 @val BIT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	UPDATE dbo.Exams
	SET	dbo.Exams.AutoCIReview	= @val
	WHERE dbo.Exams.ID = @examID
END

GO
/****** Object:  StoredProcedure [dbo].[sm_QUALITYCONTROLSERVICE_setCompetenceSettingsForGroup_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		George Chernov
-- Create date: 19/04/13
-- Description:	Sets the competency settings for a group.
-- =============================================
CREATE PROCEDURE [dbo].[sm_QUALITYCONTROLSERVICE_setCompetenceSettingsForGroup_sp]
	-- Add the parameters for the stored procedure here
	@groupId INT,
	@numItemsInRun INT,
	@numItemsWithinToleranceInRun INT,
	@maxErrorInRun DECIMAL(18,10),
	@CIProbability DECIMAL(18,15),
	@maxItemsBetweenCIs INT,
	@maxCIsOutOfTol INT,
	@numCIsInRollingReview INT,
	@maxOutOfTolInRollingReview INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	UPDATE dbo.GroupDefinitions
	SET
		dbo.GroupDefinitions.TotalItemsInCompetencyRun						= @numItemsInRun
		,dbo.GroupDefinitions.CorrectItemsWithinToleranceWithinCompetenceRun= @numItemsWithinToleranceInRun
		,dbo.GroupDefinitions.MaximumCumulativeErrorWithinCompetenceRun		= @maxErrorInRun
		,dbo.GroupDefinitions.ProbabilityOfGettingCI						= @CIProbability
		,dbo.GroupDefinitions.MaximumResponseBeforePresentingCI				= @maxItemsBetweenCIs
		,dbo.GroupDefinitions.MaxConsecutiveCIsThatCanBeMarkedOutOfTolerance= @maxCIsOutOfTol
		,dbo.GroupDefinitions.NumberOfCIsInRollingReview					= @numCIsInRollingReview
		,dbo.GroupDefinitions.MaximumCIsMarkedOutOfToleranceInRollingReview	= @maxOutOfTolInRollingReview
	WHERE dbo.GroupDefinitions.ID = @groupId
	
	-- Update QuotaManagement 
	UPDATE QM
	SET QM.Within1CIofSuspension = dbo.sm_checkUser1CiawayFromSuspend(QM.UserId, QM.GroupId)
	FROM QuotaManagement QM
	WHERE QM.GroupId = @groupId
END
GO
/****** Object:  StoredProcedure [dbo].[sm_QUALITYCONTROLSERVICE_setExamItemDefaults_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Tom Gomersall
-- Create date: 28/04/11
-- Description:	Sets the default values for items from the exam table.
-- =============================================
CREATE PROCEDURE [dbo].[sm_QUALITYCONTROLSERVICE_setExamItemDefaults_sp]
	-- Add the parameters for the stored procedure here
 @examID INT,
 @annotationsType INT,
 @numCIsRequired INT,
 @numCIsInRun INT,
 @numCorrectRequiredInRun INT,
 @maxErrorInRun DECIMAL(18,10),
 @ciProbability INT,
 @maxItemsBeteenCIs INT,
 @maxConsecutiveIncorrectInRollingReview INT,
 @numCIsInRollingReview INT,
 @maxTotalIncorrectInRollingReview INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	UPDATE dbo.Exams
	SET
		dbo.Exams.DefaultAllowAnnotationsInMarking								= @annotationsType
		,dbo.Exams.DefaultNoOfCIsRequired										= @numCIsRequired
		,dbo.Exams.DefaultTotalItemsInCompetencyRun								= @numCIsInRun
		,dbo.Exams.DefaultCorrectItemsWithinToleranceWithinCompetenceRunDefault	= @numCorrectRequiredInRun
		,dbo.Exams.DefaultMaximumCumulativeErrorWithinCompetenceRun				= @maxErrorInRun
		,dbo.Exams.DefaultProbabilityOfGettingCI									= @ciProbability
		,dbo.Exams.DefaultMaximumResponseBeforePresentingCI						= @maxItemsBeteenCIs
		,dbo.Exams.DeafultMaxConsecutiveCIsThatCanBeMarkedOutOfTolerance			= @maxConsecutiveIncorrectInRollingReview
		,dbo.Exams.DefaultNumberOfCIsInRollingReview								= @numCIsInRollingReview
		,dbo.Exams.DefaultMaximumCIsMarkedOutOfToleranceInRollingReview			= @maxTotalIncorrectInRollingReview
	WHERE dbo.Exams.ID = @examID
END

GO
/****** Object:  StoredProcedure [dbo].[sm_QUALITYCONTROLSERVICE_setIsDecimalMarksEnabled_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sm_QUALITYCONTROLSERVICE_setIsDecimalMarksEnabled_sp]
	-- Add the parameters for the stored procedure here
	@examId	INT,
	@isDecimalMarksEnabled BIT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	UPDATE dbo.Exams
	SET IsDecimalMarksEnabled = @isDecimalMarksEnabled
	WHERE Id = @examId
	
END

GO
/****** Object:  StoredProcedure [dbo].[sm_QUALITYCONTROLSERVICE_setIsFeedbackEnabled_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sm_QUALITYCONTROLSERVICE_setIsFeedbackEnabled_sp]
	-- Add the parameters for the stored procedure here
	@examId	INT,
	@isFeedbackEnabled BIT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	UPDATE dbo.Exams
	SET IsFeedbackEnabled = @isFeedbackEnabled
	WHERE Id = @examId
	
END
GO
/****** Object:  StoredProcedure [dbo].[sm_QUALITYCONTROLSERVICE_setIsMarkOneItemPerScript_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		George Chernov
-- Create date: 18/04/14
-- Description:	Set value to IsMarkOneItemPerScript
-- =============================================
CREATE PROCEDURE [dbo].[sm_QUALITYCONTROLSERVICE_setIsMarkOneItemPerScript_sp]
	-- Add the parameters for the stored procedure here
	@examId	INT,
	@isMarkOneItemPerScript BIT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	UPDATE dbo.Exams
	SET IsMarkOneItemPerScript = @isMarkOneItemPerScript
	WHERE Id = @examId
	
END
GO
/****** Object:  StoredProcedure [dbo].[sm_QUALITYCONTROLSERVICE_stopAllGroupsForExam_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		George Chernov
-- Create date: 24/04/13
-- Description:	Stops all the groups that have been released for an exam from quality control.
-- =============================================
CREATE PROCEDURE [dbo].[sm_QUALITYCONTROLSERVICE_stopAllGroupsForExam_sp]
	-- Add the parameters for the stored procedure here
	@examID INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	UPDATE dbo.GroupDefinitions
	SET dbo.GroupDefinitions.IsReleased = 0
	WHERE 
		dbo.GroupDefinitions.ExamID = @examID
END

GO
/****** Object:  StoredProcedure [dbo].[sm_QUALITYCONTROLSERVICE_stopGroups_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		George Chernov
-- Create date: 24/04/13
-- Description:	Stops marking for groups.
-- =============================================
CREATE PROCEDURE [dbo].[sm_QUALITYCONTROLSERVICE_stopGroups_sp]
	-- Add the parameters for the stored procedure here
	@groupIds NVARCHAR(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	UPDATE dbo.GroupDefinitions
	SET dbo.GroupDefinitions.IsReleased = 0
	WHERE dbo.GroupDefinitions.ID IN (SELECT * FROM dbo.ParamsToList(@groupIds))
	
	IF (@@ROWCOUNT < 1)
		BEGIN
			return 50002;
		END
END
GO
/****** Object:  StoredProcedure [dbo].[sm_QUALITYCONTROLSERVICE_submitMarkedGroupResponseForCICreation_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sm_QUALITYCONTROLSERVICE_submitMarkedGroupResponseForCICreation_sp]
-- Add the parameters for the stored procedure here
	@tokenID INT,
	@userID INT,
	@groupResponseID BIGINT,
	@mark DECIMAL(18, 10),
	@maxCIs INT,
	@uniqueResponses [UniqueResponsesType1] READONLY,
	@feedback NVARCHAR(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	IF (
	       (
	           SELECT dbo.UniqueGroupResponses.tokenId
	           FROM   dbo.UniqueGroupResponses
	           WHERE  dbo.UniqueGroupResponses.id = @groupResponseID
	       ) = @tokenID
	   )
	BEGIN
	    --The response is checked out to this user, so the marker response can be used.
	    
	    DECLARE @myGroupID INT
	    
	    SELECT @myGroupID = UniqueGroupResponses.GroupDefinitionID
	    FROM   UniqueGroupResponses
	    WHERE  ID = @groupResponseID
	    
	    IF (
	           (
	               SELECT COUNT(ID)
	               FROM   UniqueGroupResponses
	               WHERE  UniqueGroupResponses.GroupDefinitionID = @myGroupID
	                      AND CI = 1
	           ) >= @maxCIs
	       )
	    BEGIN
	        RETURN 50011;
	    END
	    ELSE
	    BEGIN
	        BEGIN TRY
	        	BEGIN TRANSACTION
	        	
	        	--Find out whether the exam has auto review on CIs.
	        	DECLARE @myAutoCIReview BIT =(
	        	            SELECT e.AutoCIReview
	        	            FROM   dbo.UniqueGroupResponses ugr
	        	                   INNER JOIN dbo.GroupDefinitions gd
	        	                        ON  ugr.GroupDefinitionID = gd.ID
	        	                   INNER JOIN dbo.Exams e
	        	                        ON  gd.ExamId = e.ID
	        	            WHERE  ugr.id = @groupResponseID
	        	        )
	        	
	        	IF (@myAutoCIReview = 1)
	        	BEGIN
	        	    --Make the response a CI and set its confirmed mark.
	        	    UPDATE dbo.UniqueGroupResponses
	        	    SET    CI                = 1,
	        	           CI_Review         = 1,
	        	           confirmedMark     = @mark,
	        	           Feedback          = @feedback
	        	           --markedMetadataResponse = @markedMetadata
	        	    WHERE  dbo.UniqueGroupResponses.id = @groupResponseID
	        	END
	        	ELSE
	        	BEGIN
	        	    --Make the response a CI but don't set its confirmed mark.
	        	    UPDATE dbo.UniqueGroupResponses
	        	    SET    CI           = 1,
	        	           Feedback     = @feedback
	        	    WHERE  dbo.UniqueGroupResponses.id = @groupResponseID
	        	END 
	        	-- update UniqueGroupResponseLinks
	        	UPDATE UGRL
	        	SET    UGRL.ConfirmedMark = UR.Mark,
	        	       UGRL.MarkedMetadataResponse = UR.MarkedMetadata
	        	FROM   UniqueGroupResponseLinks UGRL
	        	       INNER JOIN @uniqueResponses UR ON UR.ID = UGRL.UniqueResponseId
	        	WHERE  UGRL.UniqueGroupResponseID = @groupResponseID
	        	
	        	--Insert an entry in the AssignedGroupMarkstable.
	        	INSERT INTO dbo.AssignedGroupMarks
	        	  (
	        	    UserId,
	        	    UniqueGroupResponseId,
	        	    TIMESTAMP,
	        	    AssignedMark,
	        	    MarkingMethodId,
	        	    MarkingDeviation,
	        	    IsConfirmedMark,
	        	    GroupDefinitionID,
	        	    IsReportableCI
	        	  )
	        	VALUES
	        	  (
	        	    @userID,
	        	    @groupResponseID,
	        	    GETDATE(),
	        	    @mark,
	        	    5,
	        	    0,
	        	    @myAutoCIReview,
	        	    @myGroupID,
	        	    0 -- CI, that is created from QC, isn't considered in Report
	        	  )
	        	
	        	DECLARE @groupMarkId BIGINT = SCOPE_IDENTITY()
	        	--Insert an entry in the dbo.AssignedItemMarks.
	        	INSERT INTO dbo.AssignedItemMarks
	        	  (
	        	    GroupMarkId,
	        	    UniqueResponseId,
	        	    AnnotationData,
	        	    AssignedMark,
	        	    MarkingDeviation,
	        	    MarkedMetadataResponse
	        	  )
	        	SELECT @groupMarkId,
	        	       UR.ID,
	        	       UR.Annotations,
	        	       UR.Mark,
	        	       0,
	        	       UR.MarkedMetadata
	        	FROM   @uniqueResponses UR
	        	
	        	IF (@myAutoCIReview = 1)
	        	BEGIN
	        	    --The group is auto-review, so insert an entry in the assigned marks table for the review as well.
	        	    --Insert an entry in the AssignedGroupMarkstable.
	        	    INSERT INTO dbo.AssignedGroupMarks
	        	      (
	        	        UserId,
	        	        UniqueGroupResponseId,
	        	        TIMESTAMP,
	        	        AssignedMark,
	        	        MarkingMethodId,
	        	        MarkingDeviation,
	        	        IsConfirmedMark,
	        	        GroupDefinitionID
	        	      )
	        	    VALUES
	        	      (
	        	        @userID,
	        	        @groupResponseID,
	        	        GETDATE(),
	        	        @mark,
	        	        6,
	        	        0,
	        	        0,
	        	        @myGroupID
	        	      )	
	        	    SET @groupMarkId = SCOPE_IDENTITY() 
	        	    --Insert an entry in the dbo.AssignedItemMarks.
	        	    INSERT INTO dbo.AssignedItemMarks
	        	      (
	        	        GroupMarkId,
	        	        UniqueResponseId,
	        	        AnnotationData,
	        	        AssignedMark,
	        	        MarkingDeviation,
	        	        MarkedMetadataResponse
	        	      )
	        	    SELECT @groupMarkId,
	        	           UR.ID,
	        	           UR.Annotations,
	        	           UR.Mark,
	        	           0,
	        	           UR.MarkedMetadata
	        	    FROM   @uniqueResponses UR
	        	END
	        	
	        	COMMIT TRANSACTION
	        	
	        	--Set the token to null for all responses that are currently checked out to this logged in user.
	        	UPDATE dbo.UniqueGroupResponses
	        	SET    dbo.UniqueGroupResponses.tokenId = NULL
	        	WHERE  dbo.UniqueGroupResponses.tokenId = @tokenId
	        	
	        	SELECT COUNT(ID)  AS 'NumCIs',
	        	       SUM(
	        	           CASE dbo.UniqueGroupResponses.CI_Review
	        	                WHEN 1 THEN 1
	        	                ELSE 0
	        	           END
	        	       )          AS 'NumCIsReviewed'
	        	FROM   dbo.UniqueGroupResponses
	        	WHERE  dbo.UniqueGroupResponses.GroupDefinitionID = @myGroupID
	        	       AND dbo.UniqueGroupResponses.CI = 1
	        END TRY
	        BEGIN CATCH
	        	ROLLBACK TRANSACTION
	        	DECLARE @myErrorNum INT = ERROR_NUMBER()
	        	DECLARE @myMessage NVARCHAR(MAX) = ERROR_MESSAGE()
	        	DECLARE @myFullMessage NVARCHAR(MAX) = CONVERT(NVARCHAR(10), @myErrorNum) 
	        	        + ':' + @myMessage
	        	
	        	RAISERROR (@myFullMessage, 16, 1)
	        END CATCH
	    END
	END
	ELSE
	BEGIN
	    --The response is not checked out to this user
	    RETURN 50001;
	END
END
GO
/****** Object:  StoredProcedure [dbo].[sm_QUOTAMANAGEMENTSERVICE_discardMarkedResponses_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Tom Gomersall
-- Create date: 24/05/11
-- Description:	Discards the marked response for an item
-- Modified:	Tom Gomersall
-- Date:		04/09/2012
-- Description:	Now resets markedMetadataResponse for the unique response.
-- Modified:	Anton Burdyugov
-- Date:		20/05/2013
-- Description:	Change to work with groups instead of items.

-- Modified:	George Chernov
-- Date:		31/05/2013
-- Description:	Change using sm_SHARED_removeMarksFromMarkedMetadataFromUGRLinks_sp 

-- Modified:	George Chernov
-- Date:		26/09/2013
-- Description:	Add AssignedGroupMark with MarkingMethod 'Live Mark Discard'
-- =============================================
CREATE PROCEDURE [dbo].[sm_QUOTAMANAGEMENTSERVICE_discardMarkedResponses_sp]
	-- Add the parameters for the stored procedure here
    @uniqueGroupResponseIDs NVARCHAR(MAX),
    @userID INT
AS 
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON;
	
        DECLARE @myGroupResponseIDs TABLE ( id BIGINT )

        DECLARE @myResponseIDs TABLE
            (
              uniqueGroupResponseId BIGINT ,
              uniqueResponseId BIGINT
            )
            
        INSERT  INTO @myGroupResponseIDs
                SELECT  *
                FROM    dbo.ParamsToList(@uniqueGroupResponseIDs)                
                WHERE dbo.sm_checkIsUniqueGroupResponseInConfirmedScript(Value) = 0
                
        

        INSERT  INTO @myResponseIDs
                ( uniqueGroupResponseId ,
                  uniqueResponseId
                )
                SELECT  UniqueGroupResponseID ,
                        UniqueResponseId
                FROM    dbo.UniqueGroupResponseLinks
                WHERE   UniqueGroupResponseID IN (
                        SELECT  *
                        FROM    @myGroupResponseIDs )
                        
        DECLARE @output TABLE (id int)
                        
        INSERT  INTO dbo.AssignedGroupMarks
                    ( UserId,
                        UniqueGroupResponseId ,
                        Timestamp ,
                        AssignedMark ,
                        IsConfirmedMark ,
                        UserName ,
                        FullName ,
                        Surname ,
                        Disregarded ,
                        MarkingDeviation ,
                        MarkingMethodId,
                        GroupDefinitionID
                    )
       OUTPUT inserted.ID INTO @output
       SELECT    @userID , -- UserId - int
                    UGRs.id , -- UniqueGroupResponseId - bigint 
                    GETDATE() , -- Timestamp - datetime
                    AGM.AssignedMark , -- AssignedMark - decimal
                    0 , -- IsConfirmedMark - bit
                    NULL , -- UserName - nvarchar(50)
                    NULL , -- FullName - nvarchar(101)
                    NULL , -- Surname - nvarchar(50)
                    0 , -- Disregarded - bit
                    0 , -- MarkingDeviation - decimal
                    14, -- MarkingMethodId - int
                    UGR.GroupDefinitionID -- GroupId
		FROM   @myGroupResponseIDs UGRs
		INNER JOIN dbo.UniqueGroupResponses UGR on UGR.ID = UGRs.id
		INNER JOIN dbo.AssignedGroupMarks AGM ON AGM.UniqueGroupResponseId = UGR.ID
		WHERE  AGM.IsConfirmedMark = 1
					
		INSERT INTO dbo.AssignedItemMarks
		        ( GroupMarkId ,
		          UniqueResponseId ,
		          AnnotationData ,
		          AssignedMark ,
		          MarkingDeviation ,
		          MarkedMetadataResponse
		        )
		SELECT   o.id , -- GroupMarkId - bigint
		         UGRL.UniqueResponseId , -- UniqueResponseId - bigint
		         null , -- AnnotationData - xml
		         0 , -- AssignedMark - decimal
		         null , -- MarkingDeviation - decimal
		         null  -- MarkedMetadataResponse - xml
		FROM AssignedGroupMarks AGM
		INNER JOIN @output o
		ON o.id = AGM.ID
		INNER JOIN UniqueGroupResponseLinks UGRL
		ON UGRL.UniqueGroupResponseID = AGM.UniqueGroupResponseId 
	
        UPDATE  AM
        SET     IsConfirmedMark = 0
        FROM    dbo.AssignedGroupMarks AM
        WHERE   AM.uniqueGroupResponseId IN ( SELECT * FROM @myGroupResponseIDs )
                AND 
                (
					AM.markingMethodId = 4 
					OR AM.markingMethodId = 7 
					OR AM.MarkingMethodId = 12 
					OR AM.MarkingMethodId = 15 
					OR AM.MarkingMethodId = 17
                )
                
	
		--Sets the marks in the marked metadata back to zero
        UPDATE  UGR
        SET     confirmedMark = NULL
        FROM    dbo.UniqueGroupResponses UGR
        WHERE   UGR.ID IN ( SELECT  *
                            FROM    @myGroupResponseIDs )
	
		-- when user Discard UGR we have to delete all AffecterCandidateExamVersions rows with current UGR 
		-- in order to release all affected users related to the UGR
		DELETE 
		FROM AffectedCandidateExamVersions
		WHERE UniqueGroupResponseId IN (SELECT Id FROM @myGroupResponseIDs)
	
		-- Update UR links - done by sm_SHARED_removeMarksFromMarkedMetadataFromUGRLinks_sp

        DECLARE @myGroupResponseID BIGINT		
		WHILE EXISTS (SELECT TOP 1 * FROM @myGroupResponseIDs)
		BEGIN
			SELECT TOP 1 @myGroupResponseID = id FROM @myGroupResponseIDs
			DELETE FROM @myGroupResponseIDs WHERE id = @myGroupResponseID

			EXEC sm_SHARED_removeMarksFromMarkedMetadataFromUGRLinks_sp
				@uniqueGroupResponseID = @myGroupResponseID,
				@updateTable = 1
		END

    END
GO
/****** Object:  StoredProcedure [dbo].[sm_QUOTAMANAGEMENTSERVICE_FILTER_GetCentres_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sm_QUOTAMANAGEMENTSERVICE_FILTER_GetCentres_sp]

-- Add the parameters for the stored procedure here
    @examID INT ,
    @maxNum INT
AS 
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON;
	
        SET ROWCOUNT @maxNum
	
        SELECT	DISTINCT TOP(@maxNum) 
			CEV.CentreName    AS 'Centres'
        FROM   EscalatedGroups_view EG
			INNER JOIN CandidateGroupResponses CGR
			ON CGR.UniqueGroupResponseID = EG.ID
			INNER JOIN CandidateExamVersions CEV
			ON CEV.ID = CGR.CandidateExamVersionID
        WHERE EG.ExamID = @examID
	
        SET ROWCOUNT 0
    END
GO
/****** Object:  StoredProcedure [dbo].[sm_QUOTAMANAGEMENTSERVICE_FILTER_GetConfirmedMark_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	George Chernov
-- Create date: 26/02/2013
-- Description:	Filtes information for recently marked items grid for ConfirmedMark column
-- =============================================
CREATE PROCEDURE [dbo].[sm_QUOTAMANAGEMENTSERVICE_FILTER_GetConfirmedMark_sp]

	-- Add the parameters for the stored procedure here
	@examId	INT

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
		
	SELECT
		MAX(confirmedMark)		AS maxValConfirmedMark
		,MIN(confirmedMark)		AS minValConfirmedMark
	FROM MarkedCIGroups_view
	WHERE ExamID = @examId
	
END

GO
/****** Object:  StoredProcedure [dbo].[sm_QUOTAMANAGEMENTSERVICE_FILTER_GetCountries_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sm_QUOTAMANAGEMENTSERVICE_FILTER_GetCountries_sp]

-- Add the parameters for the stored procedure here
    @examID INT ,
    @maxNum INT
AS 
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON;
	
        SET ROWCOUNT @maxNum
	
        SELECT	DISTINCT TOP(@maxNum) 
			CEV.CountryName    AS 'Countries'
        FROM   EscalatedGroups_view EG
			INNER JOIN CandidateGroupResponses CGR
			ON CGR.UniqueGroupResponseID = EG.ID
			INNER JOIN CandidateExamVersions CEV
			ON CEV.ID = CGR.CandidateExamVersionID
        WHERE EG.ExamID = @examID
	
        SET ROWCOUNT 0
    END
GO
/****** Object:  StoredProcedure [dbo].[sm_QUOTAMANAGEMENTSERVICE_FILTER_GetDate_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	George Chernov
-- Create date: 26/02/2013
-- Description:	Filtes information for recently marked items grid for Date column
-- =============================================
CREATE PROCEDURE [dbo].[sm_QUOTAMANAGEMENTSERVICE_FILTER_GetDate_sp]

	-- Add the parameters for the stored procedure here
	@examId	INT

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
		
	SELECT
		MAX(timestamp)		AS maxValTimestamp
		,MIN(timestamp)		AS minValTimestamp
	FROM MarkedCIGroups_view
	WHERE ExamID = @examId
	
END

GO
/****** Object:  StoredProcedure [dbo].[sm_QUOTAMANAGEMENTSERVICE_FILTER_GetGroups_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	George Chernov
-- Create date: 26/02/2013
-- Description:	Filtes information for recently marked items grid for GroupName column
-- =============================================
CREATE PROCEDURE [dbo].[sm_QUOTAMANAGEMENTSERVICE_FILTER_GetGroups_sp]

	-- Add the parameters for the stored procedure here
	@examId	INT,
	@maxNum INT

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
		
	SELECT DISTINCT TOP(@maxNum) GroupID, GroupName
	FROM MarkedCIGroups_view
	WHERE ExamID = @examId
	
END

GO
/****** Object:  StoredProcedure [dbo].[sm_QUOTAMANAGEMENTSERVICE_FILTER_GetIDs_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	George Chernov
-- Create date: 26/02/2013
-- Description:	Filtes information for recently marked items grid for ID column
-- =============================================
CREATE PROCEDURE [dbo].[sm_QUOTAMANAGEMENTSERVICE_FILTER_GetIDs_sp]

	-- Add the parameters for the stored procedure here
	@examId	INT,
	@maxNum INT

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
		
	SELECT DISTINCT TOP(@maxNum) UniqueGroupResponseID AS ID
	FROM MarkedCIGroups_view
	WHERE ExamID = @examId
	
END


GO
/****** Object:  StoredProcedure [dbo].[sm_QUOTAMANAGEMENTSERVICE_FILTER_GetMark_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	George Chernov
-- Create date: 26/02/2013
-- Description:	Filtes information for recently marked items grid for Mark column
-- =============================================
CREATE PROCEDURE [dbo].[sm_QUOTAMANAGEMENTSERVICE_FILTER_GetMark_sp]

	-- Add the parameters for the stored procedure here
	@examId	INT

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
		
	SELECT
		MAX(assignedMark)		AS maxValAssignedMark
		,MIN(assignedMark)		AS minValAssignedMark
	FROM MarkedCIGroups_view
	WHERE ExamID = @examId
	
END

GO
/****** Object:  StoredProcedure [dbo].[sm_QUOTAMANAGEMENTSERVICE_FILTER_GetMarkers_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	George Chernov
-- Create date: 26/02/2013
-- Description:	Filtes information for recently marked items grid for Marker column
-- =============================================
CREATE PROCEDURE [dbo].[sm_QUOTAMANAGEMENTSERVICE_FILTER_GetMarkers_sp]

	-- Add the parameters for the stored procedure here
	@examId	INT,
	@maxNum INT

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
		
	SELECT DISTINCT TOP(@maxNum) 
		UserID		AS ID
		,FullName	AS FullName
	FROM MarkedCIGroups_view
	WHERE ExamID = @examId
	
END

GO
/****** Object:  StoredProcedure [dbo].[sm_QUOTAMANAGEMENTSERVICE_FILTER_getParkedReasons_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Pavel Pargachev
-- Create date: 15/05/2014
-- Description:	Gets the all parked reasons
-- =============================================
CREATE PROCEDURE [dbo].[sm_QUOTAMANAGEMENTSERVICE_FILTER_getParkedReasons_sp]
    @examID INT
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    
    SET NOCOUNT ON;
    
    SELECT ID, Name
    FROM   ParkedReasonsLookup
    ORDER BY ID
          
END
GO
/****** Object:  StoredProcedure [dbo].[sm_QUOTAMANAGEMENTSERVICE_FILTER_GetTolerance_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	George Chernov
-- Create date: 26/02/2013
-- Description:	Filtes information for recently marked items grid for Tolerance column
-- =============================================
CREATE PROCEDURE [dbo].[sm_QUOTAMANAGEMENTSERVICE_FILTER_GetTolerance_sp]

	-- Add the parameters for the stored procedure here
	@examId	INT

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
		
	SELECT
		MAX(CIMarkingTolerance)		AS maxValCIMarkingTolerance
		,MIN(CIMarkingTolerance)		AS minValCIMarkingTolerance
	FROM MarkedCIGroups_view
	WHERE ExamID = @examId
	
END

GO
/****** Object:  StoredProcedure [dbo].[sm_QUOTAMANAGEMENTSERVICE_getDateRangeForParkedResponses_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Tom Gomersall
-- Create date: 07/06/2011
-- Description:	Gets the date range for the parked responses for an exam, up to a maximum number.
-- Editor:		Anton Burdyugov
-- Create date: 23/05/2013
-- Description:	Changed to work with group data.
-- =============================================
CREATE PROCEDURE [dbo].[sm_QUOTAMANAGEMENTSERVICE_getDateRangeForParkedResponses_sp]
	-- Add the parameters for the stored procedure here
    @examID INT
AS 
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON;
	
        SELECT  MIN(ParkedDate) AS Earliest ,
                MAX(ParkedDate) AS Latest
        FROM   EscalatedGroups_view
        WHERE  ExamID = @examID
        
    END
GO
/****** Object:  StoredProcedure [dbo].[sm_QUOTAMANAGEMENTSERVICE_getExamIdForAssignedMark_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Tom Gomersall
-- Create date: 16/06/11
-- Description:	Gets the exam ID for an assigned mark.
-- =============================================
CREATE PROCEDURE [dbo].[sm_QUOTAMANAGEMENTSERVICE_getExamIdForAssignedMark_sp]
-- Add the parameters for the stored procedure here
	@assignedMarkId BIGINT
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;
    
    SELECT GD.ExamID AS ID
    FROM   AssignedGroupMarks
           INNER JOIN UniqueGroupResponses UGR ON  UGR.ID = AssignedGroupMarks.UniqueGroupResponseId
           INNER JOIN GroupDefinitions GD ON  GD.ID = UGR.GroupDefinitionID
    WHERE  AssignedGroupMarks.id = @assignedMarkId
           AND EXISTS (
                   SELECT *
                   FROM   GroupDefinitionStructureCrossRef GDSCR
                   WHERE  GDSCR.GroupDefinitionID = GD.ID
                          AND GDSCR.Status = 0
               )
END

GO
/****** Object:  StoredProcedure [dbo].[sm_QUOTAMANAGEMENTSERVICE_getExamIdForMarkedResponse_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Tom Gomersall
-- Create date: 24/05/11
-- Description:	Gets the exam ID for a unique response
-- Modified:	Anton Burdyugov
-- Create date: 21/05/13
-- Description:	Changed to use group response id 
-- =============================================
CREATE PROCEDURE [dbo].[sm_QUOTAMANAGEMENTSERVICE_getExamIdForMarkedResponse_sp]
	-- Add the parameters for the stored procedure here
	@uniqueGroupResponseID BIGINT

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	SELECT GD.ExamID AS ID
	FROM dbo.UniqueGroupResponses UGR
	INNER JOIN dbo.GroupDefinitions GD
	ON GD.ID = UGR.GroupDefinitionID
	WHERE UGR.id = @uniqueGroupResponseID

END


GO
/****** Object:  StoredProcedure [dbo].[sm_QUOTAMANAGEMENTSERVICE_getExaminersForExam_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO






-- =============================================
-- Author:		Tom Gomersall
-- Create date: 11/04/11
-- Description:	Gets all the users for an exam, as well as their number of marked items and parking and suspension information.
-- Modifier:	Anton Burdyugov
-- Create date: 20/06/13
-- Description:	Number Suspended added.
-- Modifier:	Anna Kovru
-- Create date: 28/01/15
-- Description:	Moved from exam version to exam level.
-- =============================================
CREATE PROCEDURE [dbo].[sm_QUOTAMANAGEMENTSERVICE_getExaminersForExam_sp]
-- Add the parameters for the stored procedure here
	 @examId INT
	,@getAllExaminers bit = 1
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;
    

     SELECT U.ID															 AS 'ID'
           ,U.Forename														 AS 'Forename'
           ,U.Surname														 AS 'Surname'
           ,dbo.sm_getQuotaForUserForExam_fn(@examId,U.ID)					 AS 'AssignedQuota'
           ,ISNULL(QMI.NumberMarked ,0)										 AS 'NumberMarked'
           ,CAST(
                CASE 
                    WHEN ISNULL(QMI.MarkerSuspended ,0)=0 THEN 0
                    ELSE 1
                END
                AS BIT
            )																 AS 'MarkerSuspended'
           ,CAST(
                CASE 
                    WHEN ISNULL(QMI.MarkerCloseToBeingSuspended ,0)=0 THEN 0
                    ELSE 1
                END
                AS BIT
            )																 AS 'MarkerCloseToBeingSuspended'
           ,CAST(CASE WHEN ISNULL(QMI.Parked ,0)=0 THEN 0 ELSE 1 END AS BIT) AS 'Parked'
           ,ISNULL(QMI.NumberSuspended ,0)									 AS 'NumberSuspended'
           ,CAST(
                CASE 
                    WHEN ISNULL(HRM.HasRealMarkedResponses ,0)=0 THEN 0
                    ELSE 1
                END
                AS BIT
            )																 AS 'HasRealMarkedResponses'
    FROM   Users U
           LEFT JOIN sm_getQuotaManagementInfoForUser_fn(@examId) QMI ON  QMI.UserId = U.ID
           LEFT JOIN sm_hasUserRealMarkedResponsesForExam_fn(@examId) HRM ON HRM.UserId = U.ID
    WHERE  U.ID IN (SELECT UserID
                    FROM   dbo.AssignedUserRoles AR
                           INNER JOIN dbo.RolePermissions RP ON  RP.RoleID = AR.RoleID
                    WHERE  RP.PermissionID IN (12 ,13 ,14)
                           AND (AR.ExamID=@examId OR AR.ExamID=0))
           AND U.Retired = 0
           AND (U.InternalUser=0 OR U.Username='superuser')
	       AND (ISNULL(QMI.NumberMarked ,0) > 0 AND @getAllExaminers = 0 OR @getAllExaminers = 1)
END





GO
/****** Object:  StoredProcedure [dbo].[sm_QUOTAMANAGEMENTSERVICE_getExaminersForGroupForExam_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		George Chernov
-- Create date: 20/05/13
-- Description:	Gets all the user for a group in an exam.

-- Modifier:	Anton Burdyugov
-- Create date: 19/06/13
-- Description:	Number Suspended added.

-- Modifier:	Anna
-- Create date: 28/1/15
-- Description:	Moved from exam version to exam level.
-- =============================================
CREATE PROCEDURE [dbo].[sm_QUOTAMANAGEMENTSERVICE_getExaminersForGroupForExam_sp]
    @groupId INT
	,@getAllExaminers bit = 1
AS 
    BEGIN
        SET NOCOUNT ON;
        
        DECLARE @examId INT=(
            SELECT ExamId
            FROM   dbo.GroupDefinitions
            WHERE  ID = @groupID
        )
        
	;WITH RealMarkedResponses AS (	
		SELECT U.ID AS ID,
			   UGR.GroupDefinitionID AS GroupId
		FROM   dbo.Users U
			   INNER JOIN dbo.UniqueGroupResponses UGR ON  UGR.GroupDefinitionID = @groupId
			   INNER JOIN dbo.AssignedGroupMarks AGM ON AGM.UniqueGroupResponseId = UGR.ID  AND AGM.UserId = U.ID
		WHERE  UGR.CI=0
			   AND AGM.IsConfirmedMark=1
			   AND AGM.UserId = U.ID
		GROUP BY U.ID,
				 UGR.GroupDefinitionID
				 
		),
	MarkedCIs AS (
		SELECT U.ID AS ID,
			   UGR.GroupDefinitionID AS GroupId
		FROM   dbo.Users U
			   LEFT JOIN dbo.UniqueGroupResponses UGR ON  UGR.GroupDefinitionID = @groupId
			   LEFT JOIN dbo.AssignedGroupMarks AGM ON  AGM.UniqueGroupResponseId = UGR.ID AND AGM.UserId = U.ID
		WHERE  UGR.CI_Review=1
			   AND (AGM.MarkingMethodId=2 OR AGM.MarkingMethodId=3)
		GROUP BY U.ID,
				 UGR.GroupDefinitionID
	)
	SELECT U.ID										AS ID
			  ,U.Forename							AS Forename
			  ,U.Surname							AS Surname
			  ,ISNULL(QM.AssignedQuota ,0)			AS Quota
			  ,ISNULL(QM.NumberMarked ,0)			AS NumberMarked
			  ,ISNULL(QM.Within1CIofSuspension ,0)	AS Within1CIOfSuspension
			  ,ISNULL(QM.NumItemsParked ,0)			AS NumItemsParked
			  ,ISNULL(QM.MarkingSuspended ,0)		AS Suspended
			  ,ISNULL(QM.NumberSuspended ,0)		AS NumberSuspended
			  ,CAST(CASE WHEN RealMarkedResponses.ID IS NULL THEN 0 ELSE 1 END AS BIT) AS HasRealMarkedResponses
			  ,CAST(CASE WHEN MarkedCIs.ID IS NULL THEN 0 ELSE 1 END  AS BIT) AS HasMarkedCIs
		FROM   dbo.Users U
			   LEFT JOIN dbo.QuotaManagement QM ON (QM.UserId = U.ID AND QM.GroupId = @groupId)
			   LEFT JOIN RealMarkedResponses ON RealMarkedResponses.ID = U.ID AND RealMarkedResponses.GroupId = @groupId
			   LEFT JOIN MarkedCIs ON MarkedCIs.ID = U.ID AND MarkedCIs.GroupId = QM.GroupId
		WHERE  U.ID IN (SELECT AR.UserID
						FROM   dbo.AssignedUserRoles AR
							   INNER JOIN dbo.RolePermissions RP
									ON  RP.RoleID = AR.RoleID
						WHERE  RP.PermissionID IN (12 ,13 ,14)
							   AND (AR.ExamID=@examId OR AR.ExamID=0))
			   AND U.Retired = 0
			   AND (U.InternalUser=0 OR U.Username='superuser')
			   AND (ISNULL(QM.NumberMarked ,0) > 0 AND @getAllExaminers = 0 OR @getAllExaminers = 1)
		OPTION(OPTIMIZE FOR (@groupId UNKNOWN))
	
	
    END

GO
/****** Object:  StoredProcedure [dbo].[sm_QUOTAMANAGEMENTSERVICE_getExamsForGroupIds_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		George Chernov
-- Create date: 22/05/13
-- Description:	Gets the exam IDs for a list of groups.

-- Modifier:	Anna
-- Create date: 28/1/15
-- Description:	Moved from exam version to exam level.
-- =============================================

CREATE PROCEDURE [dbo].[sm_QUOTAMANAGEMENTSERVICE_getExamsForGroupIds_sp]
	@groupIds NVARCHAR(MAX)

AS
BEGIN

	SET NOCOUNT ON;

	SELECT DISTINCT dbo.GroupDefinitions.ExamID	AS ID				
	FROM dbo.GroupDefinitions
	WHERE ID IN (
		SELECT *
		FROM dbo.ParamsToList(@groupIds)
	)

END
GO
/****** Object:  StoredProcedure [dbo].[sm_QUOTAMANAGEMENTSERVICE_getExamsForUniqueGroupResponseIds_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Stored Procedure

-- =============================================
-- Author:		Tom Gomersall
-- Create date: 27/06/11
-- Description:	Gets the exam IDs for a list of unique responses.
-- Modified:	Anton Burdyugov
-- Date:		20/05/2013
-- Description:	Change to work with groups instead of items.
-- =============================================
CREATE PROCEDURE [dbo].[sm_QUOTAMANAGEMENTSERVICE_getExamsForUniqueGroupResponseIds_sp]
	-- Add the parameters for the stored procedure here
	@responseIDs NVARCHAR(MAX)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT DISTINCT GD.ExamID AS ID				
	FROM dbo.GroupDefinitions GD
	INNER JOIN UniqueGroupResponses UGR
	ON UGR.GroupDefinitionID = GD.ID
	WHERE UGR.ID IN (
		SELECT *
		FROM dbo.ParamsToList(@responseIDs)
	)

END
GO
/****** Object:  StoredProcedure [dbo].[sm_QUOTAMANAGEMENTSERVICE_getExternalNamesForExam_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- Stored Procedure

-- =============================================
-- Author:		Tom Gomersall
-- Create date: 10/05/11
-- Description:	Gets the external item names for all the marked items for an exam.
-- Editor:		Anton Burdyugov
-- Create date: 29/05/2013
-- Description:	Changed to work with group data.
-- Editor:		Anton Burdyugov
-- Create date: 17/07/2013
-- Description:	Optimized to better performance with lardge database
-- =============================================
CREATE PROCEDURE [dbo].[sm_QUOTAMANAGEMENTSERVICE_getExternalNamesForExam_sp]
-- Add the parameters for the stored procedure here
	@examId INT,
	@maxNum INT
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;

    BEGIN TRY

        DECLARE @myReturn TABLE (GroupId INT ,GroupName NVARCHAR(max))

        INSERT INTO @myReturn
          (
            GroupId
           ,GroupName
          )
        SELECT DISTINCT TOP(@maxNum)
               GMRV.GroupId
              ,GMRV.GroupName
        FROM   dbo.GroupMarkedResponses_view GMRV
        WHERE  GMRV.examID = @examId

        IF (
               SELECT COUNT(*)
               FROM   @myReturn
           )>=@maxNum
        BEGIN
            SELECT GroupId
                  ,GroupName
            FROM   @myReturn
        END
        ELSE
        BEGIN
            SELECT GroupId
                  ,GroupName
            FROM   @myReturn
            ORDER BY
                   GroupName
        END
    END TRY
    BEGIN CATCH
        return 50005;
    END CATCH
END
GO
/****** Object:  StoredProcedure [dbo].[sm_QUOTAMANAGEMENTSERVICE_getGroupsForExaminerForExam_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		George Chernov
-- Create date: 20/05/13
-- Description:	Gets all the groups for an exam and examiner.

-- Modified by:	Anna
-- Create date: 28/1/15
-- Description:	Moved from exam version to exam level.
-- =============================================
CREATE PROCEDURE [dbo].[sm_QUOTAMANAGEMENTSERVICE_getGroupsForExaminerForExam_sp]
	@examId INT ,
	@examinerId INT
AS
BEGIN
    SET NOCOUNT ON;	


	DECLARE @groupsWithoutMarks table(
	ID int,
	Name nvarchar(max),
	IsManualMarkingType int,
	NumUniqueResponses int,
	Quota int,
	NumMarked int,
	NumParked int,
	Within1CIofSuspension int,
	Suspended int,
	HasRealMarkedResponses int,
	HasMarkedCIs int)

	INSERT INTO @groupsWithoutMarks	
    SELECT GD.ID
          ,GD.Name
		  ,(
               SELECT CASE 
                            WHEN (
                                    (
                                        SELECT COUNT(*)
                                        FROM   dbo.Items I
                                                INNER JOIN dbo.GroupDefinitionItems GDI ON  GDI.ItemID = I.ID
                                                INNER JOIN dbo.GroupDefinitions GDF ON  GDF.ID = GDI.GroupID
                                        WHERE  I.MarkingType = 1
                                                AND GDF.ID = GD.ID
                                    )>0
                                ) THEN 1
                            ELSE 0
                        END
           )
          ,COUNT(ISNULL(UGR.ID ,0))
          ,MAX(ISNULL(QM.AssignedQuota ,0))
          ,MAX(ISNULL(QM.NumberMarked ,0))
          ,MAX(ISNULL(QM.NumItemsParked ,0))
          ,ISNULL(               MAX(CASE (QM.Within1CIofSuspension) WHEN 1 THEN 1 ELSE 0 END)              ,0           )
          ,ISNULL(               MAX(CASE (QM.MarkingSuspended) WHEN 1 THEN 1 ELSE 0 END)              ,0           )
		   ,0
		   ,0
    FROM   dbo.GroupDefinitions GD
           JOIN dbo.UniqueGroupResponses UGR ON  (GD.ID = UGR.GroupDefinitionID)
           LEFT JOIN dbo.QuotaManagement QM ON  (GD.ID = QM.GroupId AND QM.UserId = @examinerId AND QM.ExamID = @examId)
    WHERE  GD.ExamID = @examId
           AND GD.ID IN (SELECT GDSCR.GroupDefinitionID
                         FROM   dbo.GroupDefinitionStructureCrossRef GDSCR
                                INNER JOIN dbo.ExamVersionStructures EVS ON  EVS.ID = GDSCR.ExamVersionStructureID
                         WHERE  GDSCR.Status = 0 -- 0 = Active
                                AND EVS.StatusID = 0 -- 0 = Released
                            )
    GROUP BY GD.ID ,GD.Name


	INSERT INTO @groupsWithoutMarks	
    SELECT GD.ID
          ,GD.Name
          ,0
          ,0
          ,0
          ,0
          ,0
          ,0
          ,0     
		  ,CASE 
                WHEN SUM(
                            CASE 
                                WHEN  AGM.IsConfirmedMark=1
									AND UGR.CI=0
									THEN 1 ELSE 0 
                                END
                        )>0 THEN 1
                ELSE 0
            END
          ,CASE 
                WHEN SUM(
                            CASE 
                                WHEN (AGM.MarkingMethodId=2 OR AGM.MarkingMethodId=3)
                            AND UGR.CI_Review=1 THEN 1 ELSE 0 END
                        )>0 THEN 1
                ELSE 0
            END
    FROM   @groupsWithoutMarks GD
           JOIN dbo.UniqueGroupResponses UGR ON  (GD.ID = UGR.GroupDefinitionID)
           JOIN dbo.AssignedGroupMarks AGM ON  (AGM.UniqueGroupResponseId = UGR.ID AND AGM.UserId = @examinerId)
    GROUP BY GD.ID ,GD.Name

	SELECT ID,	Name,
	CAST(SUM(IsManualMarkingType) AS BIT) as IsManualMarkingType,
	SUM(NumUniqueResponses) as NumUniqueResponses,
	SUM(Quota) as Quota,
	SUM(NumMarked) as NumMarked,
	SUM(NumParked) as NumParked,
	SUM(Within1CIofSuspension) as Within1CIofSuspension,
	SUM(Suspended) as Suspended,
	CAST(SUM(HasRealMarkedResponses) AS BIT) as HasRealMarkedResponses,
	CAST(SUM(HasMarkedCIs) AS BIT) as HasMarkedCIs
	FROM @groupsWithoutMarks
    GROUP BY ID, Name
    ORDER BY ID
END
GO
/****** Object:  StoredProcedure [dbo].[sm_QUOTAMANAGEMENTSERVICE_getGroupsForExamWithNumResponses_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		George Chernov
-- Create date: 20/05/13
-- Description:	Gets all the groups for an exam with the number of unique responses and the number marked.
-- Last modified: Anna - Moved from exam version to exam level
-- =============================================
CREATE PROCEDURE [dbo].[sm_QUOTAMANAGEMENTSERVICE_getGroupsForExamWithNumResponses_sp]
	@examId INT
AS
BEGIN
    SET NOCOUNT ON;
    
    SELECT GD.ID          AS 'ID'
          ,GD.Name        AS 'NAME'
          ,(
               SELECT CAST(
                          CASE 
                               WHEN (
                                        (
                                            SELECT COUNT(*)
                                            FROM   dbo.Items I
                                                   INNER JOIN dbo.GroupDefinitionItems GDI ON  GDI.ItemID = I.ID
                                                   INNER JOIN dbo.GroupDefinitions GDF ON  GDF.ID = GDI.GroupID
                                            WHERE  I.MarkingType = 1
                                                   AND GDF.ID = GD.ID
                                        )>0
                                    ) THEN 1
                               ELSE 0
                          END AS BIT
                      )
           )              AS 'IsManualMarkingType'
          ,COUNT(UGR.ID)  AS 'NumResponses'
          ,SUM(CASE WHEN UGR.ConfirmedMark IS NULL THEN 0 ELSE 1 END)	 AS 'NumMarkedResponses'
          ,SUM(CASE WHEN UGR.CI=1 AND UGR.CI_Review=0 THEN 1 ELSE 0 END) AS 'NumAwaitingReview'
          ,SUM(
               CASE 
                    WHEN UGR.ConfirmedMark IS NOT NULL
               AND UGR.CI=0
               AND AGM.ID IS NULL THEN 1
                   ELSE 0
                   END
           )              AS 'NumHumanMarkedResponses'        
          ,SUM(CASE WHEN UGR.IsEscalated = 1 THEN 1 ELSE 0 END)             AS 'NumParkedResponses'
    FROM   dbo.GroupDefinitions GD
           LEFT JOIN dbo.UniqueGroupResponses UGR ON  UGR.GroupDefinitionID = GD.ID
                    --detect auto marked UGRs              
           LEFT JOIN dbo.AssignedGroupMarks AGM ON  AGM.UniqueGroupResponseId = UGR.ID
                    AND AGM.MarkingMethodId = 12
                    AND AGM.IsConfirmedMark = 1
                    AND AGM.GroupDefinitionID = GD.ID
    WHERE  GD.ExamID = @examId
           AND GD.ID IN (SELECT GDSCR.GroupDefinitionID
                         FROM   dbo.GroupDefinitionStructureCrossRef GDSCR
                                INNER JOIN dbo.ExamVersionStructures EVS ON  EVS.ID = GDSCR.ExamVersionStructureID
                         WHERE  GDSCR.Status = 0 -- 0 = Active
                                AND EVS.StatusID = 0 -- 0 = Released
                        )
    GROUP BY
           GD.ID
          ,GD.Name
END
GO
/****** Object:  StoredProcedure [dbo].[sm_QUOTAMANAGEMENTSERVICE_getGroupsForExamWithQuotaInformation_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		George Chernov
-- Create date: 20/05/13
-- Description:	Gets all the groups for an exam with the quota information.
-- Last modified: 28/1/15 Anna - Moved from exam version to exam level
-- =============================================
CREATE PROCEDURE [dbo].[sm_QUOTAMANAGEMENTSERVICE_getGroupsForExamWithQuotaInformation_sp]
	@examId INT
AS
BEGIN

	SET NOCOUNT ON;

	SELECT 
		GD.ID											AS 'ID'
		,(
			CASE
				WHEN MIN(QM.AssignedQuota) < 0 THEN -1
				ELSE SUM(QM.AssignedQuota)
			END
		)												AS 'AssignedQuota'
		,SUM(QM.NumItemsParked)							AS 'ParkedResponse'
		,SUM(CAST(QM.Within1CIofSuspension AS INT))		AS 'CloseToSuspension'
		,SUM(CAST(QM.MarkingSuspended AS INT))			AS 'SuspendedMarker'
		,SUM(QM.NumberMarked)							AS 'NumMarked'
	FROM dbo.GroupDefinitions GD
		INNER JOIN dbo.QuotaManagement QM ON QM.GroupId = GD.ID
	WHERE GD.ExamID = @examId
			AND GD.ID IN 
				(
					SELECT GDSCR.GroupDefinitionID 
					FROM dbo.GroupDefinitionStructureCrossRef GDSCR
						INNER JOIN dbo.ExamVersionStructures EVS ON EVS.ID = GDSCR.ExamVersionStructureID
					WHERE GDSCR.Status = 0 -- 0 = Active
						AND EVS.StatusID = 0 -- 0 = Released
				)
				AND EXISTS(
					SELECT * FROM dbo.AssignedUserRoles AUR
					INNER JOIN dbo.RolePermissions RP on RP.RoleID = AUR.RoleID 
					INNER JOIN dbo.Users U ON U.ID = AUR.UserID
					WHERE  (AUR.ExamID = 0 or AUR.ExamID = @examId)
						AND U.Retired = 0
						AND ( U.InternalUser = 0 OR U.Username = 'superuser' )
						AND U.ID = QM.UserId
						AND RP.PermissionID in (12,13,14)
					)
	GROUP BY GD.ID	
	OPTION (OPTIMIZE FOR UNKNOWN)
END
GO
/****** Object:  StoredProcedure [dbo].[sm_QUOTAMANAGEMENTSERVICE_getIDsForParkedResponses_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Tom Gomersall
-- Create date: 07/06/2011
-- Description:	Gets the IDs of the parked responses for an exam, up to a maximum number.
-- Editor:		Anton Burdyugov
-- Create date: 23/05/2013
-- Description:	Changed to work with group data.
-- =============================================
CREATE PROCEDURE [dbo].[sm_QUOTAMANAGEMENTSERVICE_getIDsForParkedResponses_sp]
	-- Add the parameters for the stored procedure here
    @examId INT ,
    @maxNum INT
AS 
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON;
	
        SET ROWCOUNT @maxNum
	
        SELECT Id
        FROM   EscalatedGroups_view
        WHERE  ExamID = @examId
	
        SET ROWCOUNT 0
    END
GO
/****** Object:  StoredProcedure [dbo].[sm_QUOTAMANAGEMENTSERVICE_getMarkDistributionForGroup_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Stored Procedure

-- =============================================
-- Author:		Tom Gomersall
-- Create date: 12/07/11
-- Description:	Gets the mark distribution for an item version.
-- Editor:		Anton Burdyugov
-- Create date: 22/05/13
-- Description:	Make work with group data
-- =============================================
CREATE PROCEDURE [dbo].[sm_QUOTAMANAGEMENTSERVICE_getMarkDistributionForGroup_sp]
	-- Add the parameters for the stored procedure here
    @uniqueGroupResponseID INT
AS 
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON;
	
        DECLARE @groupVersionID INT
	
        SELECT  @groupVersionID = GroupDefinitionID
        FROM    UniqueGroupResponses
        WHERE   ID = @uniqueGroupResponseID
	
        SELECT  MRV.AssignedMark AS MARK ,
                COUNT(MRV.AssignedMark) AS Occurrences ,
                MAX(MRV.TotalMark) AS TotalMark
        FROM    GroupMarkedResponses_view MRV
        WHERE   MRV.GroupID = @groupVersionID
        GROUP BY AssignedMark
        ORDER BY Mark ASC
		
    END
GO
/****** Object:  StoredProcedure [dbo].[sm_QUOTAMANAGEMENTSERVICE_getMarkDistributionForGroupForMarker_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Stored Procedure

-- =============================================
-- Author:		Tom Gomersall
-- Create date: 12/07/11
-- Description:	Gets the mark distribution for an item version for a marker.
-- Editor:		Anton Burdyugov
-- Create date: 22/05/13
-- Description:	Make work with group data
-- =============================================
CREATE PROCEDURE [dbo].[sm_QUOTAMANAGEMENTSERVICE_getMarkDistributionForGroupForMarker_sp]
	-- Add the parameters for the stored procedure here
    @uniqueGroupResponseID INT
AS 
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON;
	
        DECLARE @myGroupVersionID INT
	
        SELECT  @myGroupVersionID = UGR.GroupDefinitionID
        FROM    UniqueGroupResponses UGR
        WHERE   UGR.ID = @uniqueGroupResponseID
	
        DECLARE @myUserId INT
	
        SELECT  @myUserId = MRV.UserID
        FROM    GroupMarkedResponses_view MRV
        WHERE   MRV.UniqueGroupResponseID = @uniqueGroupResponseID
	
        SELECT  MRV.AssignedMark AS Mark ,
                COUNT(MRV.AssignedMark) AS Occurrences ,
                MAX(MRV.TotalMark) AS TotalMark
        FROM    GroupMarkedResponses_view MRV
        WHERE   MRV.GroupID = @myGroupVersionID
                AND MRV.UserID = @myUserId
        GROUP BY AssignedMark
        ORDER BY Mark ASC
		
    END
GO
/****** Object:  StoredProcedure [dbo].[sm_QUOTAMANAGEMENTSERVICE_GetMarkedResponseForMarkedCG_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Tom Gomersall
-- Create date: 27/06/2011
-- Description:	Gets the marked response for a marked CI.
-- Modified:	Tom Gomersall
-- Date:		04/09/2012
-- Description:	Added marked metadata response to select.
-- Modified:	Vitaly Bevzik
-- Date:		03/04/2013
-- Description:	Added ScriptType
-- Modified:	Vitaly Bevzik
-- Date:		10/04/2013
-- Description:	Removed cheking Is item a CI, to allow se most recently Ci after it was discarded or 
-- Modified:	Anton Burdyugov
-- Date:		31/05/2013
-- Description:	Make work with group level data.
-- Modified:	Anton Burdyugov
-- Date:		17/06/2013
-- Description:	Confirmed mark for (Actual CI Review) added

-- Modified:	Roman Tseyko
-- Date:		21/06/2013
-- Description:	MarkingTolerance and ConfirmedMark for unique response added
-- =============================================
CREATE PROCEDURE [dbo].[sm_QUOTAMANAGEMENTSERVICE_GetMarkedResponseForMarkedCG_sp]
-- Add the parameters for the stored procedure here
	@assignedGroupMarkId INT
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;
    
    DECLARE @uniqueGroupResponseId INT
    SET @uniqueGroupResponseId = (
            SELECT TOP 1
                   AGM.UniqueGroupResponseId
            FROM   dbo.AssignedGroupMarks AGM
            WHERE  AGM.ID = @assignedGroupMarkID
        )
    
    --Select Group information
    SELECT TOP 1
           GD.ID
          ,GD.TotalMark
          ,GD.Name
          ,AGM.AssignedMark
          ,ISNULL(
               UGR.ConfirmedMark
              ,(
                   SELECT TOP(1) AssignedMark
                   FROM   dbo.AssignedGroupMarks AGM
                   WHERE  (AGM.UniqueGroupResponseId=UGR.id)
                          AND (markingMethodId=6)
                   ORDER BY
                          TIMESTAMP DESC
               )
           )                      AS ConfirmedMark
          ,GD.CIMarkingTolerance  AS Tolerance
          ,ISNULL(Exams.AnnotationSchema ,'') AS AnnotationSchema
          ,CEV.ScriptType
          ,UGR.ID                 AS UniqueGroupResponseId
          ,dbo.sm_checkGroupAndItemsMarkedInTolerance_fn(
               AGM.id
              ,UGR.id
              ,AGM.MarkingDeviation
              ,GD.CIMarkingTolerance
              ,GD.IsAutomaticGroup
           )                      AS IsOutOfTolerance
          ,AGM.IsEscalated
          ,AGM.MarkingDeviation
          ,UGR.Feedback
    FROM   dbo.UniqueGroupResponses UGR
           INNER JOIN dbo.GroupDefinitions GD ON  GD.ID = UGR.GroupDefinitionID
           INNER JOIN dbo.AssignedGroupMarks AGM ON  AGM.UniqueGroupResponseId = UGR.ID
           INNER JOIN dbo.Exams Exams ON  Exams.ID = GD.ExamID
           INNER JOIN dbo.CandidateGroupResponses CGR ON  CGR.UniqueGroupResponseID = UGR.ID
           INNER JOIN CandidateExamVersions CEV ON  CEV.ID = CGR.CandidateExamVersionID
    WHERE  AGM.ID = @assignedGroupMarkID
           AND (AGM.markingMethodId=2 OR AGM.markingMethodId=3)
    
    -- Select unique responses				
    SELECT UR.id                         AS id
          ,UR.responseData               AS response
          ,AIM.assignedMark              AS assignedMark
          ,AIM.annotationData            AS annotationData
          ,AIM.markedMetadataResponse    AS markedMetadataResponse
          ,I.ID                          AS ItemId
          ,I.Version                     AS ItemVersion
          ,I.LayoutName                  AS LayoutName
          ,I.AllowAnnotations            AS AllowAnnotations
          ,I.TotalMark                   AS TotalMark
          ,I.ExternalItemID              AS ExternalID
          ,GDI.ViewOnly                  AS ViewOnly
          ,I.CIMarkingTolerance          AS MarkingTolerance
          ,L.confirmedMark               AS ConfirmedMark
          ,AIM.MarkingDeviation          AS MarkingDeviation
    FROM   dbo.UniqueGroupResponses UGR
           INNER JOIN dbo.UniqueGroupResponseLinks L ON  UGR.Id = L.UniqueGroupResponseID
           INNER JOIN dbo.UniqueResponses UR ON  UR.id = L.UniqueResponseId
           INNER JOIN dbo.Items I ON  i.id = UR.itemId
           INNER JOIN dbo.GroupDefinitionItems GDI ON  GDI.GroupID = UGR.GroupDefinitionID AND GDI.ItemID = UR.itemId
           LEFT JOIN dbo.AssignedItemMarks AIM ON  AIM.UniqueResponseId = UR.id AND AIM.GroupMarkId = @assignedGroupMarkId
    WHERE  UGR.ID = @uniqueGroupResponseId
    ORDER BY
           GDI.GroupDefinitionItemOrder  ASC
END
GO
/****** Object:  StoredProcedure [dbo].[sm_QUOTAMANAGEMENTSERVICE_getMarkedResponseForMarkedGroup_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Anton Burdyugov
-- Create date: 21/05/13
-- Description:	Gets the marked response for a marked group.

-- Editor:		Anton Burdyugov
-- Create date: 03/07/13
-- Description:	Deleted users support added
-- ==========================================================================
CREATE PROCEDURE [dbo].[sm_QUOTAMANAGEMENTSERVICE_getMarkedResponseForMarkedGroup_sp]
-- Add the parameters for the stored procedure here
	@uniqueGroupResponseId BIGINT
	,@isEscalations BIT = 0
AS
BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;

    --select only 1 assigned mark for one unique response
    DECLARE @assignedGroupMarkId BIGINT
    SET @assignedGroupMarkId = (
            SELECT TOP 1
                   AGM.ID
            FROM   dbo.AssignedGroupMarks AGM
            WHERE  AGM.UniqueGroupResponseId = @uniqueGroupResponseId
                   AND AGM.IsConfirmedMark = 1
            ORDER BY
                   AGM.ID DESC
    )

    
    IF @assignedGroupMarkId IS NULL AND @isEscalations = 1 
		BEGIN
    		SET @assignedGroupMarkId = (
				SELECT TOP 1
					   AGM.ID
				FROM   dbo.AssignedGroupMarks AGM
				WHERE  AGM.UniqueGroupResponseId = @uniqueGroupResponseId
					   AND AGM.MarkingMethodId = 5
				ORDER BY
					   AGM.ID DESC
		)
		END
    
    --Select Group information
    SELECT TOP 1
           GD.ID              AS 'ID'
          ,GD.TotalMark       AS 'TotalMark'
          ,GD.Name            AS 'Name'         
          ,
          (   
          	CASE 
                WHEN (
                        UGR.ConfirmedMark IS NOT NULL
                        ) 
                THEN UGR.ConfirmedMark
                ELSE
                	CASE 
					WHEN (
							UGR.CI = 1 AND UGR.CI_Review = 0
							) 
					THEN AGM.AssignedMark
					ELSE NULL 
				END
            END
          )					  AS 'ConfirmedMark'
          ,UGR.ScriptType     AS 'ScriptType'
          ,UGR.Feedback       AS 'Feedback'
          ,ISNULL(U.Surname + ', ' + U.Forename ,AGM.fullName) AS 'FullName'
          ,ISNULL(Exams.AnnotationSchema ,'') AS 'AnnotationSchema'
          ,UGR.ID             AS 'UniqueGroupResponseId'
          ,dbo.sm_checkGroupCIsExceeded_fn(GD.ID) AS 'CILimitAchieved'
          ,dbo.sm_checkIsUniqueGroupResponseInConfirmedScript(UGR.ID) AS 'IsInConfirmedScript'
    FROM   dbo.UniqueGroupResponses UGR
           INNER JOIN dbo.GroupDefinitions GD ON  GD.ID = UGR.GroupDefinitionID
           INNER JOIN dbo.AssignedGroupMarks AGM ON  AGM.ID = @assignedGroupMarkId
           LEFT JOIN dbo.Users U ON  U.ID = AGM.userId
           INNER JOIN dbo.Exams Exams ON  Exams.ID = GD.ExamID
    WHERE  UGR.ID = @uniqueGroupResponseId
    
    -- Select unique responses				
    SELECT UR.id                         AS 'id'
          ,UR.responseData               AS 'response'
          ,AIM.assignedMark              AS 'assignedMark'
          ,AIM.annotationData            AS 'annotationData'
          ,AIM.markedMetadataResponse    AS 'markedMetadataResponse'
          ,I.ID                          AS 'ItemId'
          ,I.Version                     AS 'ItemVersion'
          ,I.LayoutName                  AS 'layoutName'
          ,I.AllowAnnotations            AS 'AllowAnnotations'
          ,I.TotalMark                   AS 'TotalMark'
          ,I.ExternalItemID              AS 'ExternalID'
          ,I.ExternalItemName            AS 'ExternalName'
          ,GDI.ViewOnly                  AS 'ViewOnly'
    FROM   dbo.UniqueGroupResponses UGR
           INNER JOIN dbo.UniqueGroupResponseLinks L ON  UGR.Id = L.UniqueGroupResponseID
           INNER JOIN dbo.UniqueResponses UR ON  UR.id = L.UniqueResponseId
           INNER JOIN dbo.Items I ON  i.id = UR.itemId
           INNER JOIN dbo.GroupDefinitionItems GDI ON  GDI.GroupID = UGR.GroupDefinitionID
                    AND GDI.ItemID = UR.itemId
           LEFT JOIN dbo.AssignedItemMarks AIM ON  AIM.UniqueResponseId = UR.id
                    AND AIM.GroupMarkId = @assignedGroupMarkId
    WHERE  UGR.ID = @uniqueGroupResponseId
    ORDER BY
           GDI.GroupDefinitionItemOrder  ASC
END
GO
/****** Object:  StoredProcedure [dbo].[sm_QUOTAMANAGEMENTSERVICE_getMarkedResponseForParkedGroup_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Stored Procedure

-- =============================================
-- Author:		Tom Gomersall
-- Create date: 09/06/11
-- Description:	Gets the marked response for a parked item.
-- Modified:	Tom Gomersall
-- Date:		04/09/2012
-- Description:	Added markedMetadataResponse to the select statement.
-- Modified:	Vitaly Bevzik
-- Date:		29/03/2013
-- Description:	Added ScriptType
-- Editor:		Anton Burdyugov
-- Create date: 23/05/2013
-- Description:	Changed to work with group data.
-- Editor:		Anton Burdyugov
-- Create date: 03/07/2013
-- Description:	Deleted users support
-- Editor:		Pavel Pargachev
-- Create date: 19/05/2014
-- Description:	Added parked reasons
-- =============================================
CREATE PROCEDURE [dbo].[sm_QUOTAMANAGEMENTSERVICE_getMarkedResponseForParkedGroup_sp]
-- Add the parameters for the stored procedure here
	@uniqueGroupResponseId BIGINT
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;
    
    --select only 1 assigned mark for one unique response
    DECLARE @assignedGroupMarkId BIGINT
    SET @assignedGroupMarkId = (
            SELECT TOP 1 AGM.ID
            FROM   dbo.AssignedGroupMarks AGM
            WHERE  AGM.UniqueGroupResponseId = @uniqueGroupResponseId
            ORDER BY AGM.Timestamp DESC
        )
    
	DECLARE @escalatedWithoutMark bit = 
	(SELECT CASE WHEN (AGM.MarkingMethodId = 11 AND AGM.EscalatedWithoutMark = 1) THEN 1 ELSE 0 END 
	FROM dbo.AssignedGroupMarks AGM WHERE  AGM.ID = @assignedGroupMarkId)

    --Select Group information
    SELECT TOP 1
           GD.ID                 AS 'ID'
          ,GD.TotalMark          AS 'TotalMark'
          ,CASE WHEN (AGM.MarkingMethodId = 11 AND AGM.EscalatedWithoutMark = 1) THEN NULL ELSE AGM.AssignedMark END      AS 'AssignedMark'
          ,GD.Name               AS 'Name'
          ,UGR.ScriptType        AS 'ScriptType'
          ,ISNULL(U.Surname+', '+U.Forename ,AGM.fullName) AS 'FullName'
          ,ISNULL(E.AnnotationSchema ,'') AS 'AnnotationSchema'
          ,UGR.ParkedAnnotation  AS 'ParkedAnnotation'
          ,UGR.ParkedDate        AS 'ParkedDate'
          ,(
               SELECT RTRIM(PRL.Name)+','
               FROM   dbo.UniqueGroupResponseParkedReasonCrossRef UGRPRCS
                      INNER JOIN dbo.ParkedReasonsLookup PRL
                           ON  UGRPRCS.ParkedReasonId = PRL.ID
               WHERE  UGRPRCS.UniqueGroupResponseId = UGR.Id
               ORDER BY
                      PRL.Id
                      FOR XML PATH('')
           )                     AS 'ParkedReasons'

    FROM   dbo.UniqueGroupResponses UGR
           INNER JOIN dbo.GroupDefinitions GD ON  GD.ID = UGR.GroupDefinitionID
           INNER JOIN dbo.AssignedGroupMarks AGM ON  AGM.ID = @assignedGroupMarkId
           LEFT JOIN dbo.Users U ON  U.ID = AGM.userId
           INNER JOIN dbo.Exams E ON E.ID = GD.ExamID
    WHERE  UGR.ID = @uniqueGroupResponseId
    
    -- Select unique responses				
    SELECT UR.id                         AS 'id'
          ,UR.responseData               AS 'response'
          ,CAST((CASE WHEN (AIM.assignedMark = 0 AND @escalatedWithoutMark = 1) THEN 1 ELSE 0 END) as bit) AS 'escalatedWithoutMark'
          ,AIM.assignedMark AS 'assignedMark'
          ,AIM.annotationData            AS 'annotationData'
          ,AIM.markedMetadataResponse    AS 'markedMetadataResponse'
          ,I.ID                          AS 'ItemId'
          ,I.Version                     AS 'ItemVersion'
          ,I.LayoutName                  AS 'layoutName'
          ,I.AllowAnnotations            AS 'AllowAnnotations'
          ,I.TotalMark                   AS 'TotalMark'
          ,I.ExternalItemID              AS 'ExternalID'
          ,I.ExternalItemName            AS 'ExternalName'
          ,GDI.ViewOnly                  AS 'ViewOnly'
    FROM   dbo.UniqueGroupResponses UGR
           INNER JOIN dbo.UniqueGroupResponseLinks L ON  UGR.Id = L.UniqueGroupResponseID
           INNER JOIN dbo.UniqueResponses UR ON  UR.id = L.UniqueResponseId
           INNER JOIN dbo.Items I ON  i.id = UR.itemId
           INNER JOIN dbo.GroupDefinitionItems GDI ON  GDI.GroupID = UGR.GroupDefinitionID
                    AND GDI.ItemID = UR.itemId
           LEFT JOIN dbo.AssignedItemMarks AIM ON  AIM.UniqueResponseId = UR.id
                    AND AIM.GroupMarkId = @assignedGroupMarkId
    WHERE  UGR.ID = @uniqueGroupResponseId
           AND UGR.CI = 0
    ORDER BY
           GDI.GroupDefinitionItemOrder  ASC
END
GO
/****** Object:  StoredProcedure [dbo].[sm_QUOTAMANAGEMENTSERVICE_getMarkedTimeRangeForExam_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Stored Procedure

-- =============================================
-- Author:		Tom Gomersall
-- Create date: 10/05/11
-- Description:	Gets the earliest and latest times of marking for an exam.
-- Editor:		Anton Burdyugov
-- Create date: 29/05/2013
-- Description:	Changed to work with group data.
-- =============================================
CREATE PROCEDURE [dbo].[sm_QUOTAMANAGEMENTSERVICE_getMarkedTimeRangeForExam_sp]
	-- Add the parameters for the stored procedure here
 @examId INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT 
		MIN(GMRV.timestamp)		AS Earliest
		,MAX(GMRV.timestamp)	AS Latest
	FROM dbo.GroupMarkedResponses_view GMRV
	WHERE GMRV.examID = @examId

END
GO
/****** Object:  StoredProcedure [dbo].[sm_QUOTAMANAGEMENTSERVICE_getMarkersForExam_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- Stored Procedure

-- =============================================
-- Author:		Tom Gomersall
-- Create date: 10/05/11
-- Description:	Gets the markers for all the marked items for an exam version.
-- Editor:		Anton Burdyugov
-- Create date: 29/05/2013
-- Description:	Changed to work with group data.
-- Editor:		Anton Burdyugov
-- Create date: 17/07/2013
-- Description:	Optimized to better performance with lardge database
-- =============================================
CREATE PROCEDURE [dbo].[sm_QUOTAMANAGEMENTSERVICE_getMarkersForExam_sp]
-- Add the parameters for the stored procedure here
	@examId INT,
	@maxNum INT
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;
    
    BEGIN TRY
        DECLARE @myReturn TABLE (ID INT ,FullName NVARCHAR(255))
        
        INSERT INTO @myReturn
          (
            ID
           ,FullName
          )
        SELECT DISTINCT TOP(@maxNum)
               GMRV.UserID
              ,GMRV.FullName
        FROM   dbo.GroupMarkedResponses_view GMRV
        WHERE  GMRV.examID = @examId
        
        IF (
               SELECT COUNT(*)
               FROM   @myReturn
           )=@maxNum
        BEGIN
            SELECT ID
                  ,FullName
            FROM   @myReturn
        END
        ELSE
        BEGIN
            SELECT ID
                  ,FullName
            FROM   @myReturn
            ORDER BY
                   FullName
        END
    END TRY
    BEGIN CATCH
        return 50005;
    END CATCH
END

GO
/****** Object:  StoredProcedure [dbo].[sm_QUOTAMANAGEMENTSERVICE_getMarkersForParkedResponses_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Tom Gomersall
-- Create date: 07/06/2011
-- Description:	Gets the markers for the parked responses for an exam version, up to a maximum number.
-- =============================================
-- 06-07-2011: Umair Added check for retired and internal user
-- Editor:		Anton Burdyugov
-- Create date: 23/05/2013
-- Description:	Changed to work with group data.
-- =============================================
CREATE PROCEDURE [dbo].[sm_QUOTAMANAGEMENTSERVICE_getMarkersForParkedResponses_sp]
-- Add the parameters for the stored procedure here
	@examID INT ,
	@maxNum INT
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;
    
    SET ROWCOUNT @maxNum
    
    SELECT UserID    AS ID
          ,Examiner  AS FullName
    FROM   dbo.EscalatedGroups_view
    WHERE  ExamID = @examID
    GROUP BY
           UserID
          ,Examiner
    
    SET ROWCOUNT 0
END
GO
/****** Object:  StoredProcedure [dbo].[sm_QUOTAMANAGEMENTSERVICE_getMarkingHistoryItems_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Alexey Kharitonov 
-- Create date: 04/07/2014
-- Description:	Get items for marking history for current groupdId
-- =============================================
CREATE PROCEDURE [dbo].[sm_QUOTAMANAGEMENTSERVICE_getMarkingHistoryItems_sp]
	@uniqueGroupResponseId BIGINT,
	@markingMethods VARCHAR(512)
AS
BEGIN
    SET NOCOUNT ON;
    
    DECLARE @resultTable TABLE(
                DateOfMarking DATETIME
               ,Firstname NVARCHAR(50)
               ,Surname NVARCHAR(50)
               ,Mark DECIMAL(18 ,10)
               ,TotalMark DECIMAL(18 ,10)
               ,Comment NVARCHAR(MAX)
               ,MarkingMethod INT
               ,Issues NVARCHAR(MAX)
            );
           
    
    INSERT INTO @resultTable
    SELECT AGM.Timestamp			AS DateOfMarking
          ,U.Forename				AS Firstname
          ,U.Surname				AS Surname
		  ,CASE WHEN (AGM.MarkingMethodId = 11 AND AGM.EscalatedWithoutMark = 1) THEN -1 ELSE AGM.AssignedMark END			AS Mark
          ,GD.TotalMark				AS TotalMark
          ,AGM.ParkedAnnotation		AS Comment
		  ,AGM.MarkingMethodId      AS MarkingMethod
          ,(
				SELECT DISTINCT RTRIM(PRL.Name) + ', '
				FROM dbo.AssignedGroupMarksParkedReasonCrossRef AGMPRC 
				INNER JOIN dbo.ParkedReasonsLookup PRL 
				ON PRL.ID = AGMPRC.ParkedReasonId
				WHERE AGMPRC.AssignedGroupMarkId = AGM.ID
				FOR XML PATH('')
		   )						AS Issues
    FROM   dbo.AssignedGroupMarks AGM
		   INNER JOIN dbo.GroupDefinitions GD ON AGM.GroupDefinitionID = GD.ID
           INNER JOIN dbo.Users U ON  U.ID = AGM.UserId
           INNER JOIN dbo.ParamsToList(@markingMethods) MM ON  MM.Value = AGM.MarkingMethodId
    WHERE  AGM.UniqueGroupResponseId = @uniqueGroupResponseId           
    
    SELECT DateOfMarking
          ,Firstname
          ,Surname
          ,Mark
          ,TotalMark
          ,Comment
          ,MarkingMethod
          ,Issues
    FROM   @resultTable
    ORDER BY DateOfMarking

END


GO
/****** Object:  StoredProcedure [dbo].[sm_QUOTAMANAGEMENTSERVICE_getMarkRangeForExam_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Stored Procedure

-- =============================================
-- Author:		Tom Gomersall
-- Create date: 10/05/11
-- Description:	Gets the maximum and minimum awarded marks for an exam.
-- Editor:		Anton Burdyugov
-- Create date: 29/05/2013
-- Description:	Changed to work with group data.
-- =============================================
CREATE PROCEDURE [dbo].[sm_QUOTAMANAGEMENTSERVICE_getMarkRangeForExam_sp]
	-- Add the parameters for the stored procedure here
 @examId INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT 
		MIN(GMRV.assignedMark)	AS Lowest
		,MAX(GMRV.assignedMark)	AS Highest
	FROM dbo.GroupMarkedResponses_view GMRV
	WHERE GMRV.examID = @examId

END
GO
/****** Object:  StoredProcedure [dbo].[sm_QUOTAMANAGEMENTSERVICE_getNamesForParkedResponses_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Tom Gomersall
-- Create date: 07/06/2011
-- Description:	Gets the names of the parked responses for an exam version, up to a maximum number.
-- Editor:		Anton Burdyugov
-- Create date: 23/05/2013
-- Description:	Changed to work with group data.
-- =============================================
CREATE PROCEDURE [dbo].[sm_QUOTAMANAGEMENTSERVICE_getNamesForParkedResponses_sp]
	-- Add the parameters for the stored procedure here
    @examID INT ,
    @maxNum INT
AS 
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON;
	
        SET ROWCOUNT @maxNum
	
        SELECT DISTINCT
                Name
        FROM   EscalatedGroups_view
        WHERE  ExamID = @examID
	
	
        SET ROWCOUNT 0
    END
GO
/****** Object:  StoredProcedure [dbo].[sm_QUOTAMANAGEMENTSERVICE_getNumUniqueAndAssignedResponsesForGroup_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		George Chernov
-- Create date: 23/05/13
-- Description:	Gets number of unique responses and the sum of the quotas that have been assigned to markers for groups.
-- =============================================

CREATE PROCEDURE [dbo].[sm_QUOTAMANAGEMENTSERVICE_getNumUniqueAndAssignedResponsesForGroup_sp]
	@groupIDs NVARCHAR(MAX),
	@examId INT
AS
BEGIN
    SET NOCOUNT ON;
    
    DECLARE @users TABLE(UserId int, AssignedQuota int)
    INSERT INTO @users
		SELECT U.ID AS UserId
		, CASE WHEN QM.AssignedQuota IS NULL THEN 0 ELSE QM.AssignedQuota END AS AssignedQuota
		FROM   dbo.Users U
			   LEFT JOIN dbo.QuotaManagement QM ON (QM.UserId = U.ID AND QM.GroupId IN (SELECT * FROM   dbo.ParamsToList(@groupIDs)))
		WHERE  U.ID IN (SELECT AR.UserID
						FROM   dbo.AssignedUserRoles AR
							   INNER JOIN dbo.RolePermissions RP
									ON  RP.RoleID = AR.RoleID
						WHERE  RP.PermissionID IN (12 ,13 ,14)
							   AND (AR.ExamID = @examId OR AR.ExamID = 0))
			   AND U.Retired = 0
			   AND (U.InternalUser=0 OR U.Username='superuser')
    
    -- count of unique group responses for current groups
    DECLARE @myNumUniqueGroupResponses     INT=(
                SELECT COUNT(UGR.GroupDefinitionID)
                FROM dbo.UniqueGroupResponses UGR
                WHERE UGR.GroupDefinitionID IN (SELECT * FROM   dbo.ParamsToList(@groupIDs))
            )
    
    -- count of confirmed marks exept not reviewed ci
    DECLARE @myNumConfirmed     INT=(
                SELECT ISNULL(SUM(CASE WHEN UGR.ConfirmedMark IS NOT NULL THEN 1 ELSE 0 END), 0) AS 'NumMarkedResponses'
                FROM dbo.UniqueGroupResponses UGR
                WHERE UGR.GroupDefinitionID IN (SELECT * FROM   dbo.ParamsToList(@groupIDs))
            )
    -- count of assigned quotas for current groups
    DECLARE @myNumQuotas           INT=(
                SELECT ISNULL(SUM(Q.AssignedQuota) ,0)
                FROM dbo.QuotaManagement Q
					INNER JOIN @users U ON U.UserId = Q.UserId
                WHERE Q.GroupId IN (SELECT * FROM dbo.ParamsToList(@groupIDs))
            )
    
    -- count of marked quotas for current groups				 
    DECLARE @myNumMarkedQuotas       INT=(
                SELECT ISNULL(SUM(Q.NumberMarked) ,0)
                FROM dbo.QuotaManagement Q
					INNER JOIN @users U ON U.UserId = Q.UserId
                WHERE Q.GroupId IN (SELECT * FROM dbo.ParamsToList(@groupIDs))
            ) 
    
    DECLARE @myNumAwaitingReview     INT=(
                SELECT ISNULL(SUM(CASE WHEN UGR.CI=1 AND UGR.CI_Review=0 THEN 1 ELSE 0 END), 0) AS 'NumAwaitingReview'
                FROM dbo.UniqueGroupResponses UGR
                WHERE UGR.GroupDefinitionID IN (SELECT * FROM   dbo.ParamsToList(@groupIDs))
			)
    
    DECLARE @numParked INT=(
                SELECT ISNULL(SUM(CASE WHEN UGR.IsEscalated = 1 THEN 1 ELSE 0 END ), 0) AS 'NumParked'
                FROM dbo.UniqueGroupResponses UGR
                WHERE UGR.GroupDefinitionID IN (SELECT * FROM   dbo.ParamsToList(@groupIDs))
            )
    
    DECLARE @diffQuotasAndMarkQuotas INT= @myNumQuotas - @myNumMarkedQuotas
    
    IF @diffQuotasAndMarkQuotas < 0
    BEGIN
        SET @diffQuotasAndMarkQuotas = @myNumQuotas
    END

    IF (
		EXISTS (
			SELECT U.UserId
			FROM   @users U
			WHERE  U.AssignedQuota = -1
           )
       )
    BEGIN
        SELECT -1							AS 'UnassignedQuota'
              ,@myNumUniqueGroupResponses	AS 'NumUniqueResponses'
    END
    ELSE
    BEGIN
		DECLARE @myUnassignedQuotas INT = @myNumUniqueGroupResponses- @myNumConfirmed - @diffQuotasAndMarkQuotas- @myNumAwaitingReview - @numParked
        SELECT @myUnassignedQuotas			AS 'UnassignedQuota'
              ,@myNumUniqueGroupResponses	AS 'NumUniqueResponses'
    END
END
GO
/****** Object:  StoredProcedure [dbo].[sm_QUOTAMANAGEMENTSERVICE_getRemainingQuotaForUsersForGroups_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		George Chernov
-- Create date: 23/05/13
-- Description:	Gets a user's assigned quota for an item.
-- Modifier:		Vitaly Bevzik
-- Create date: 19/09/13
-- Description:	Gets a multiple users assigned quota for an item.
-- =============================================

CREATE PROCEDURE [dbo].[sm_QUOTAMANAGEMENTSERVICE_getRemainingQuotaForUsersForGroups_sp]
    @groupIDs varchar(max) ,
    @userIDs varchar(max)
AS 
    BEGIN
        SET NOCOUNT ON;	

        SELECT 
			CAST(U.Value AS INT) AS UserID,
			CAST(G.Value AS INT) AS GroupID,
			CASE Q.AssignedQuota WHEN -1 THEN -1 
				ELSE ISNULL(Q.AssignedQuota, 0) - ISNULL(Q.NumberMarked, 0) END as RemainingQuota
        FROM dbo.ParamsToList(@userIDs) as U
		LEFT JOIN dbo.QuotaManagement Q ON Q.UserId = U.Value
		INNER JOIN dbo.ParamsToList(@groupIDs) as G ON Q.GroupId = G.Value
    END


GO
/****** Object:  StoredProcedure [dbo].[sm_QUOTAMANAGEMENTSERVICE_getResponseInformationforExam_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Tom Gomersall
-- Create date: 25/08/11
-- Description:	Gets the unique response information for an exam.
-- Modified:	Anton Burdyugov
-- Date:		20/05/2013
-- Description:	Change to work with groups instead of items.
-- =============================================
CREATE  PROCEDURE [dbo].[sm_QUOTAMANAGEMENTSERVICE_getResponseInformationforExam_sp]
	@examId	INT
AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT 
		COUNT(UGR.ID)	AS Total
		,SUM(
			CASE
			WHEN UGR.ConfirmedMark IS NULL
			THEN 0
			ELSE 1
			END
		)				AS NumMarked
	FROM dbo.UniqueGroupResponses UGR
		INNER JOIN dbo.GroupDefinitions GD	ON GD.ID = UGR.GroupDefinitionID
	WHERE ExamId = @examId
		AND EXISTS ( SELECT TOP 1 *
                FROM   dbo.GroupDefinitionStructureCrossRef AS CR
                    INNER JOIN dbo.ExamVersionStructures AS EVS ON EVS.ID = CR.ExamVersionStructureID
                WHERE  ( CR.Status = 0 )
                    AND ( CR.GroupDefinitionID = GD.ID )
                    AND ( EVS.StatusID = 0 ) )	
	
END

GO
/****** Object:  StoredProcedure [dbo].[sm_QUOTAMANAGEMENTSERVICE_getUniqueGroupResponseIdsForExam_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Stored Procedure

-- =============================================
-- Author:		Tom Gomersall
-- Create date: 10/05/11
-- Description:	Gets the unique response IDs for all the marked items for an exam version.
-- Editor:		Anton Burdyugov
-- Create date: 29/05/2013
-- Description:	Changed to work with group data.
-- =============================================
CREATE PROCEDURE [dbo].[sm_QUOTAMANAGEMENTSERVICE_getUniqueGroupResponseIdsForExam_sp]
	-- Add the parameters for the stored procedure here
 @examId INT,
 @maxNum INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	SET ROWCOUNT @maxNum
	
	BEGIN TRY
		SELECT DISTINCT GMRV.UniqueGroupResponseID
		FROM dbo.GroupMarkedResponses_view GMRV
		WHERE GMRV.examID = @examId
		
		SET ROWCOUNT 0
	END TRY
	BEGIN CATCH
		SET ROWCOUNT 0
		return 50005;
	END CATCH
END
GO
/****** Object:  StoredProcedure [dbo].[sm_QUOTAMANAGEMENTSERVICE_isEnableUniqueGroupResponseAsControlItem_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- =============================================
-- Author:		Alexey Kharitonov
-- Create date: 13/05/14
-- Description:	Returns flag regarding availablility to propose item as CI 
-- =============================================
CREATE PROCEDURE [dbo].[sm_QUOTAMANAGEMENTSERVICE_isEnableUniqueGroupResponseAsControlItem_sp]
	-- Add the parameters for the stored procedure here
    @uniqueGroupResponseId BIGINT,
	@maxCIs INT
AS 
    BEGIN

	DECLARE @responseGroupDefinitionID INT

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	SELECT @responseGroupDefinitionID=GroupDefinitionID
	FROM dbo.UniqueGroupResponses
	WHERE ID=@uniqueGroupResponseId

	SELECT CONVERT(BIT,
		CASE WHEN EXISTS(SELECT ID 
			            FROM dbo.UniqueGroupResponses 
			            WHERE   ID=@uniqueGroupResponseId 
				            AND CI=1) 
					OR
					(SELECT COUNT(ID)
			        FROM dbo.UniqueGroupResponses
			        WHERE     GroupDefinitionID=@responseGroupDefinitionID
				        AND CI=1
			        )>=@maxCIs
			THEN 0 
		ELSE 
			1 
		END)
    END
GO
/****** Object:  StoredProcedure [dbo].[sm_QUOTAMANAGEMENTSERVICE_setQuotasForGroupsAndUsers_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Dmitriy Shelemov
-- Description:	Sets a marker's quota for a group in an exam
-- =============================================
CREATE PROCEDURE [dbo].[sm_QUOTAMANAGEMENTSERVICE_setQuotasForGroupsAndUsers_sp]
	-- Add the parameters for the stored procedure here
    @groupIDs NVARCHAR(MAX) ,
    @userIDs NVARCHAR(MAX) = NULL,
    @remainingQuota INT
AS 
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON;
	
        BEGIN TRY
            BEGIN TRAN

				DECLARE @affectedUsers TABLE(UserId int)
				IF (@userIDs IS NULL)
				BEGIN 
					INSERT INTO @affectedUsers
					SELECT    UserID
					FROM      AssignedUserRoles AUR
							JOIN Users U ON (U.ID = UserID AND U.Retired = 0 
											AND ( U.InternalUser = 0 OR U.Username = 'superuser'))
							JOIN RolePermissions RP ON RP.RoleID = AUR.RoleID
							JOIN Exams EV ON EV.ID = AUR.ExamID OR AUR.ExamID = 0
					WHERE     PermissionID IN ( 13, 14 )
					GROUP BY  UserID
				END
				ELSE
				BEGIN 
					INSERT INTO @affectedUsers
					SELECT    Users.Value
					FROM      dbo.ParamsToList(@userIDs) AS Users
				END
		
				DECLARE @affectedGroups TABLE(GroupId int)
				INSERT INTO @affectedGroups
				SELECT    Grps.Value
				FROM      dbo.ParamsToList(@groupIDs) AS Grps

				INSERT  INTO dbo.QuotaManagement
						( UserId,            
						  ExamID,
						  AssignedQuota,
						  MarkingSuspended,
						  SuspendedReason,
						  NumberMarked,
						  Within1CIofSuspension,
						  NumItemsParked,
						  PreviousAssignedMarkId,
						  GroupId
						)
						SELECT  Users.UserId,                     
								GD.ExamID,
								@remainingQuota,
								0,
								NULL,
								0,
								0,
								0,
								NULL,
								Groups.GroupId
						FROM    @affectedUsers AS Users
								FULL JOIN @affectedGroups AS Groups ON 1 = 1
								JOIN dbo.GroupDefinitions GD ON GD.Id = Groups.GroupId
						WHERE   NOT EXISTS ( SELECT QM.UserId
											 FROM   QuotaManagement QM
											 WHERE  QM.UserID = Users.UserId
													AND QM.GroupId = Groups.GroupId )
	
				UPDATE  dbo.QuotaManagement
				SET     dbo.QuotaManagement.AssignedQuota = CASE @remainingQuota WHEN -1 THEN -1 
															ELSE (@remainingQuota + qm.NumberMarked) END
				FROM dbo.QuotaManagement qm
				JOIN @affectedUsers AS usr ON (usr.UserId = qm.UserId)
				JOIN @affectedGroups AS grp ON (grp.GroupId = qm.GroupId)

            COMMIT TRAN
        END TRY
        BEGIN CATCH
            ROLLBACK TRAN
            DECLARE @myErrorNum INT = ERROR_NUMBER()
            DECLARE @myMessage NVARCHAR(MAX) = ERROR_MESSAGE()
            DECLARE @myFullMessage NVARCHAR(MAX) = CONVERT(NVARCHAR(10), @myErrorNum)
                + ':' + @myMessage
            RAISERROR (@myFullMessage, 16, 1)
        END CATCH
    END
GO
/****** Object:  StoredProcedure [dbo].[sm_QUOTAMANAGEMENTSERVICE_setUniqueGroupResponseAsControlItem_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sm_QUOTAMANAGEMENTSERVICE_setUniqueGroupResponseAsControlItem_sp]
	-- Add the parameters for the stored procedure here
    @uniqueGroupResponseId BIGINT,
    @userID INT,
	@maxCIs INT,
	@feedback NVARCHAR(MAX)=NULL	
AS 
    BEGIN

		DECLARE @responseGroupDefinitionID INT	
		DECLARE @previousGroupMarkId BIGINT
		DECLARE @newGroupMarkId BIGINT

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON;

        IF EXISTS(SELECT ID 
		          FROM dbo.UniqueGroupResponses 
				  WHERE     ID=@uniqueGroupResponseId 
				        AND CI=1)
		BEGIN
			-- Unique group response is already a CI
			return 50014;
		END
		
		SELECT @responseGroupDefinitionID=GroupDefinitionID
		FROM dbo.UniqueGroupResponses
		WHERE ID=@uniqueGroupResponseId

		IF((SELECT COUNT(ID)
			FROM dbo.UniqueGroupResponses
			WHERE     GroupDefinitionID=@responseGroupDefinitionID
				  AND CI=1
			)>=@maxCIs)
			BEGIN
				return 50011;
			END
		ELSE
			BEGIN
				BEGIN TRY
					BEGIN TRANSACTION

						UPDATE dbo.UniqueGroupResponses
						SET CI=1,
							CI_Review=0,
							ConfirmedMark=NULL,
							Feedback=@feedback     						
						WHERE ID=@uniqueGroupResponseId

						SELECT TOP 1 @previousGroupMarkId=ID 
						FROM dbo.AssignedGroupMarks
						WHERE UniqueGroupResponseId=@uniqueGroupResponseId
						ORDER BY Timestamp DESC

						INSERT INTO dbo.AssignedGroupMarks
							(   UserId,
								UniqueGroupResponseId,
								Timestamp,
								AssignedMark,
								IsConfirmedMark,
								Disregarded,
								MarkingDeviation,
								MarkingMethodId,
								IsEscalated,
								GroupDefinitionID,
								IsActualMarking,
								IsReportableCI
							)
						SELECT @userID,
								UniqueGroupResponseId,
								GETDATE(),
								AssignedMark,
								0, -- IsConfirmedMark
								Disregarded,					   
								MarkingDeviation,
								5, -- Actual CI Marking
								IsEscalated,
								GroupDefinitionID,
								IsActualMarking,
								0 -- CI, that is created from QM, isn't considered in Report		        
						FROM dbo.AssignedGroupMarks 
						WHERE ID=@previousGroupMarkId

						SET @newGroupMarkId=SCOPE_IDENTITY()
						
						UPDATE dbo.AssignedGroupMarks
						SET
							IsConfirmedMark = 0
						WHERE ID = @previousGroupMarkId

						UPDATE dbo.AssignedGroupMarks
						SET UserName=U.UserName,
							FullName=U.Forename+' '+U.Surname,
							Surname=U.Surname
						FROM dbo.AssignedGroupMarks AGM INNER JOIN
								dbo.Users U ON AGM.UserId=U.ID
						WHERE AGM.ID=@newGroupMarkId

						INSERT INTO dbo.AssignedItemMarks
								(GroupMarkId,
								UniqueResponseId,
								AnnotationData,
								AssignedMark,
								MarkingDeviation,
								MarkedMetadataResponse)
						SELECT @newGroupMarkId,
								UniqueResponseId,
								AnnotationData,
								AssignedMark,
								MarkingDeviation,
								MarkedMetadataResponse
						FROM dbo.AssignedItemMarks
						WHERE GroupMarkId=@previousGroupMarkId 																		
						
					COMMIT TRANSACTION
				END TRY
				BEGIN CATCH
					ROLLBACK TRANSACTION
					
					DECLARE @myErrorNum INT=ERROR_NUMBER()
					DECLARE @myMessage NVARCHAR(MAX)=ERROR_MESSAGE()
					DECLARE @myFullMessage NVARCHAR(MAX)=CONVERT(NVARCHAR(10), @myErrorNum)+':'+@myMessage
					RAISERROR (@myFullMessage, 16, 1)

				END CATCH
			END
    END

GO
/****** Object:  StoredProcedure [dbo].[sm_QUOTAMANAGEMENTSERVICE_startMarkingForUsersForGroups_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		George Chernov
-- Create date: 30/05/13
-- Description:	Un-suspends marking for the specified users and groups.
-- =============================================
CREATE PROCEDURE [dbo].[sm_QUOTAMANAGEMENTSERVICE_startMarkingForUsersForGroups_sp]
	-- Add the parameters for the stored procedure here
    @groupIDs NVARCHAR(MAX) ,
    @userIDs NVARCHAR(MAX) ,
    @userId INT ,
    @cleanSlate BIT
AS 
    BEGIN

        SET NOCOUNT ON;

        UPDATE  dbo.QuotaManagement
        SET     dbo.QuotaManagement.MarkingSuspended = 0 ,
                dbo.QuotaManagement.SuspendedReason = NULL
        WHERE   dbo.QuotaManagement.UserID IN (
                SELECT  *
                FROM    dbo.ParamsToList(@userIDs) )
                AND dbo.QuotaManagement.GroupId IN (
                SELECT  *
                FROM    dbo.ParamsToList(@groupIDs) )

	-- if we are to give the users a clean slate then disregard all CIs that
	--they've marked up to now		
        IF @cleanSlate = 1 
            BEGIN	
                UPDATE  AssignedGroupMarks
                SET     AssignedGroupMarks.Disregarded = 1
                FROM    AssignedGroupMarks
                        INNER JOIN UniqueGroupResponses ON UniqueGroupResponses.ID = AssignedGroupMarks.UniqueGroupResponseId
                WHERE   AssignedGroupMarks.UserID IN (
                        SELECT  *
                        FROM    dbo.ParamsToList(@userIDs) )
                        AND UniqueGroupResponses.GroupDefinitionID IN (
                        SELECT  *
                        FROM    dbo.ParamsToList(@groupIDs) )
                        AND AssignedGroupMarks.MarkingMethodId IN ( 2, 3 )
            END

    END
GO
/****** Object:  StoredProcedure [dbo].[sm_QUOTAMANAGEMENTSERVICE_submitParkedResponse_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Tom Gomersall
-- Create date: 14/06/11
-- Description:	Submits a marker response for a parked unique response.
-- Modified:	Tom Gomersall
-- Date:		24/10/2011
-- Description:	Added an update to decrement the number of parked responses in the QuotaManagement table.
-- Modified:	Tom Gomersall
-- Date:		21/11/2011
-- Description:	Changed the update QuotaManagement table to use the ID of the user who parked the response, instead of the user who is un-parking it.
-- Modified:	Tom Gomersall
-- Date:		24/09/2012
-- Description:	Added marked metadata.
-- Modified:	Anton Burdyugov
-- Date:		24/05/2013
-- Description:	Make work with group data.
-- =============================================
CREATE PROCEDURE [dbo].[sm_QUOTAMANAGEMENTSERVICE_submitParkedResponse_sp]
	-- Add the parameters for the stored procedure here
    @userID           INT ,
    @groupResponseID  BIGINT ,
    @mark             DECIMAL(18, 10) ,
    @parkedAnnotation NVARCHAR(max),
    @reasonIds		  NVARCHAR(200),
    @uniqueResponses  [UniqueResponsesType1] READONLY
AS 
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON;
	
	--Get the ID of the user who parked the response.
        DECLARE @myParkedUserID INT
        SELECT  @myParkedUserID = ParkedUserID
        FROM    dbo.UniqueGroupResponses
        WHERE   ID = @groupResponseID
	
        UPDATE  UGR
        SET     ParkedUserID = NULL ,
                ParkedAnnotation = NULL ,
                ParkedDate = NULL,
                IsEscalated = 0
        FROM    dbo.UniqueGroupResponses UGR
        WHERE   ID = @groupResponseID
                AND UGR.IsEscalated = 1
	
        IF ( @@ROWCOUNT = 0 ) 
            BEGIN
				--No record has been updated, so it must not have been parked, so throw an error
				return 50007;
            END
        ELSE 
            BEGIN
                BEGIN TRY
                    BEGIN TRANSACTION
					
					-- update UniqueGroupResponseLinks
                    UPDATE  UGRL
                    SET     UGRL.ConfirmedMark = UR.Mark ,
                            UGRL.MarkedMetadataResponse = UR.MarkedMetadata
                    FROM    dbo.UniqueGroupResponseLinks UGRL
                            INNER JOIN @uniqueResponses UR ON UR.ID = UGRL.UniqueResponseId
                    WHERE   UGRL.UniqueGroupResponseID = @groupResponseID
                    
                    DECLARE @groupId INT = (select GroupDefinitionID from UniqueGroupResponses where ID = @groupResponseID)
					
					--Insert an entry in the AssignedGroupMarks table.
                    INSERT  INTO dbo.AssignedGroupMarks
                            ( UserId ,
                              UniqueGroupResponseId ,
                              Timestamp ,
                              AssignedMark ,
                              IsConfirmedMark ,
                              UserName ,
                              FullName ,
                              Surname ,
                              Disregarded ,
                              MarkingDeviation ,
                              MarkingMethodId,
                              GroupDefinitionID,
                              IsActualMarking,
                              ParkedAnnotation    
                            )
                    VALUES  ( @userID ,
                              @groupResponseID ,
                              GETDATE() ,
                              @mark ,
                              1 ,
                              NULL ,
                              NULL ,
                              NULL ,
                              0 ,
                              0 ,
                              4,
                              @groupId,
                              0,
                              @parkedAnnotation
                            )
					
                    DECLARE @groupMarkId BIGINT = SCOPE_IDENTITY()
                    
                    -- Store history
                    INSERT INTO dbo.AssignedGroupMarksParkedReasonCrossRef
                            SELECT @groupMarkId, R.Value
                            FROM dbo.ParamsToList(@reasonIds) as R

                    --Insert an entry in the dbo.AssignedItemMarks.
                    INSERT  INTO dbo.AssignedItemMarks
                            ( GroupMarkId ,
                              UniqueResponseId ,
                              AnnotationData ,
                              AssignedMark ,
                              MarkingDeviation ,
                              MarkedMetadataResponse
								    
                            )
                            SELECT  @groupMarkId ,
                                    UR.ID ,
                                    UR.Annotations ,
                                    UR.Mark ,
                                    0 ,
                                    UR.MarkedMetadata
                            FROM    @uniqueResponses UR                         

                    UPDATE  UGR
                    SET     UGR.confirmedMark = @mark
                    FROM    dbo.UniqueGroupResponses UGR
                    WHERE   UGR.ID = @groupResponseID
					
					--Get the item's ID
                    DECLARE @myGroupID INT
					
                    SELECT  @myGroupID = GroupDefinitionID
                    FROM    dbo.UniqueGroupResponses UGR
                    WHERE   UGR.ID = @groupResponseID
								
					--Decrement the number of parked items for the user who parked the response in the QuotaManagement table.
                    UPDATE  QM
                    SET     QM.NumItemsParked = QM.NumItemsParked - 1
					FROM QuotaManagement QM
                    WHERE   QM.UserID = @myParkedUserID
                            AND QM.GroupId = @myGroupID
                            
                    DELETE FROM UniqueGroupResponseParkedReasonCrossRef
					WHERE UniqueGroupResponseId = @groupResponseID
					
                    COMMIT TRANSACTION
                END TRY
                BEGIN CATCH
                    ROLLBACK TRANSACTION
                    
					DECLARE @myErrorNum INT = ERROR_NUMBER()
                    DECLARE @myMessage NVARCHAR(MAX) = ERROR_MESSAGE()
                    DECLARE @myFullMessage NVARCHAR(MAX) = CONVERT(NVARCHAR(10), @myErrorNum) + ':' + @myMessage
                    
					RAISERROR (@myFullMessage, 16, 1)
                END CATCH
            END
	
    END
GO
/****** Object:  StoredProcedure [dbo].[sm_QUOTAMANAGEMENTSERVICE_suspendMarkingForUsersForGroups_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		George Chernov
-- Create date: 30/05/13
-- Description:	Suspends marking for the specified users and groups.
-- Modifier:	Anton Burdyugov
-- Create date: 19/06/2013
-- Description:	 Incremention number of times a particular Examiner has been suspended added
-- =============================================
CREATE PROCEDURE [dbo].[sm_QUOTAMANAGEMENTSERVICE_suspendMarkingForUsersForGroups_sp]
    @groupIDs NVARCHAR(MAX) ,
    @userIDs NVARCHAR(MAX) , -- users which should be suspended
    @userId INT -- user who suspends
AS 
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON;
        BEGIN TRY
	--Create an entry in the SuspendedReasons table
            INSERT  INTO dbo.SuspendedReasons
                    ( ReasonID, Reason, UserID, Date )
            VALUES  ( 1, 'Manual', @userId, GETDATE() )
	
            DECLARE @myReasonID INT = SCOPE_IDENTITY()
	
	--The user may not yet have a quota, so an entry is added for any users that don't have one, with the quota set to zero.
            INSERT  INTO dbo.QuotaManagement
					( UserId,
                      ExamID,
                      AssignedQuota,
                      MarkingSuspended,
                      SuspendedReason,
                      NumberMarked,
                      Within1CIofSuspension,
                      NumItemsParked,
                      PreviousAssignedMarkId,
                      GroupId
                    )
                    SELECT  UserIds.Value,
                            GroupDefinitions.ExamId,
                            0,
                            0,
                            NULL,
                            0,
                            0,
                            0,
                            NULL,
                            GroupIds.Value
                    FROM    dbo.ParamsToList(@userIds) AS UserIds
                            FULL JOIN dbo.ParamsToList(@groupIDs) AS GroupIds ON 1 = 1
                            INNER JOIN dbo.GroupDefinitions ON dbo.GroupDefinitions.ID = GroupIds.Value
                    WHERE   NOT EXISTS ( SELECT AssignedQuota
                                         FROM   dbo.QuotaManagement Q
                                         WHERE  Q.GroupId = GroupIds.Value
                                                AND Q.UserId = UserIds.Value )


            UPDATE  dbo.QuotaManagement
            SET     dbo.QuotaManagement.MarkingSuspended = 1 ,
                    dbo.QuotaManagement.SuspendedReason = @myReasonID ,
					dbo.QuotaManagement.NumberSuspended = dbo.QuotaManagement.NumberSuspended + 1
            WHERE   dbo.QuotaManagement.UserID IN (
                    SELECT  *
                    FROM    dbo.ParamsToList(@userIDs) )
                    AND dbo.QuotaManagement.GroupId IN (
                    SELECT  *
                    FROM    dbo.ParamsToList(@groupIDs) )



        END TRY
        BEGIN CATCH 
            ROLLBACK TRANSACTION  
            
			DECLARE @myErrorNum INT = ERROR_NUMBER()
            DECLARE @myMessage NVARCHAR(MAX) = ERROR_MESSAGE()
            DECLARE @myFullMessage NVARCHAR(MAX) = CONVERT(NVARCHAR(10), @myErrorNum) + ':' + @myMessage

            RAISERROR (@myFullMessage, 16, 1)
        END CATCH  
    END
GO
/****** Object:  StoredProcedure [dbo].[sm_QUOTAMANAGEMENTSERVICE_unParkResponse_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Tom Gomersall
-- Create date: 14/06/11
-- Description:	Removes the parked information for a unique response.
-- Editor:		Anton Burdyugov
-- Create date: 24/05/2013
-- Description:	Make work with group data.
-- =============================================
CREATE PROCEDURE [dbo].[sm_QUOTAMANAGEMENTSERVICE_unParkResponse_sp]
	-- Add the parameters for the stored procedure here
    @uniqueGroupResponseId BIGINT
AS 
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON;
	
        BEGIN TRY
            BEGIN TRAN
		
            DECLARE @myUserId INT,
                    @myGroupId INT,
			        @previousGroupMarkId BIGINT
		
            SELECT  @myUserId = UGR.ParkedUserID ,
                    @myGroupId = UGR.GroupDefinitionID
            FROM    dbo.UniqueGroupResponses UGR
            WHERE   UGR.ID = @uniqueGroupResponseId
		
		--Decrement the group parked count for the user who originally parked the group.
            UPDATE  QM
            SET     QM.NumItemsParked = QM.NumItemsParked - 1
            FROM    dbo.QuotaManagement QM
            WHERE   QM.GroupId = @myGroupId
                    AND QM.UserId = @myUserId
		
            UPDATE  UGR
            SET     UGR.ParkedUserID = NULL ,
                    UGR.ParkedAnnotation = NULL ,
                    UGR.ParkedDate = NULL,
                    UGR.IsEscalated = 0
            FROM    dbo.UniqueGroupResponses UGR
            WHERE   UGR.ID = @uniqueGroupResponseId          

      		SELECT TOP 1 @previousGroupMarkId=ID 
			FROM dbo.AssignedGroupMarks
			WHERE UniqueGroupResponseId=@uniqueGroupResponseId
			ORDER BY Timestamp DESC

            --Insert an entry in the AssignedGroupMarks table. 
            INSERT INTO dbo.Assignedgroupmarks
                        (Userid,
                        Uniquegroupresponseid,
                        Timestamp,
                        Assignedmark,
                        Isconfirmedmark,
                        Username,
                        Fullname,
                        Surname,
                        Disregarded,
                        Markingdeviation,
                        Markingmethodid,
                        GroupDefinitionID)
            VALUES      (@myUserId,
                        @uniqueGroupResponseId,
                        Getdate(),
                        0,
                        0,
                        NULL,
                        NULL,
                        NULL,
                        0,
                        0,
                        16,
                        @myGroupId)

            DECLARE @groupMarkId BIGINT = SCOPE_IDENTITY()
						
			INSERT INTO dbo.AssignedItemMarks
					(GroupMarkId,
					UniqueResponseId,
					AnnotationData,
					AssignedMark,
					MarkingDeviation,
					MarkedMetadataResponse)
			SELECT @groupMarkId,
					UniqueResponseId,
					NULL,
					0,
					NULL,
					NULL
			FROM dbo.AssignedItemMarks
			WHERE GroupMarkId=@previousGroupMarkId
            
            DELETE FROM UniqueGroupResponseParkedReasonCrossRef
            WHERE UniqueGroupResponseId = @uniqueGroupResponseId
		
            COMMIT TRAN
        END TRY
        BEGIN CATCH
            ROLLBACK TRAN
            DECLARE @myErrorNum INT = ERROR_NUMBER()
            DECLARE @myMessage NVARCHAR(MAX) = ERROR_MESSAGE()
            DECLARE @myFullMessage NVARCHAR(MAX) = CONVERT(NVARCHAR(10), @myErrorNum)
                + ':' + @myMessage
            RAISERROR (@myFullMessage, 16, 1)
        END CATCH
    END
GO
/****** Object:  StoredProcedure [dbo].[sm_ROLEMANAGEMENTSERVICE_CreateNewRole_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Umair Rafique
-- Create date: 25-06-2011
-- Description:	creates a new role
-- Modify date: 12-04-2013
-- Description:	add just created role as assignable to super user
-- =============================================
CREATE PROCEDURE [dbo].[sm_ROLEMANAGEMENTSERVICE_CreateNewRole_sp]
	@RoleName NVarchar(60)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	IF EXISTS (SELECT ID FROM dbo.Roles Where Name = @RoleName)
	BEGIN
		SELECT 'ROLE_ALREADY_EXISTS'
	END
    ELSE
    BEGIN
		INSERT INTO dbo.Roles (Name)
			Values (@RoleName)	
			
		DECLARE @myNewRoleId INT = @@IDENTITY
			
		INSERT INTO dbo.GrantableRoles
		(
			RoleId
			,GrantableRoleID
		)
		SELECT
			@myNewRoleId
			,ID
		FROM [dbo].[Roles]
		WHERE TrainingRole = 1			
		
		DECLARE @suRoleId INT = (SELECT ID FROM dbo.Roles WHERE Name = 'Super User Role')	
		-- Assign new role to 'super user'		
		INSERT INTO dbo.GrantableRoles
		(
			RoleId
			,GrantableRoleID
		)
		values(@suRoleId, @myNewRoleId)
		
		SELECT @myNewRoleId
    END
	
END
GO
/****** Object:  StoredProcedure [dbo].[sm_ROLEMANAGEMENTSERVICE_DeleteRole_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Umair Rafique
-- Create date: 25-06-2011
-- Description:	delete a new role
-- =============================================
CREATE PROCEDURE [dbo].[sm_ROLEMANAGEMENTSERVICE_DeleteRole_sp]
	@RoleId Int
AS
BEGIN
	DELETE FROM dbo.AssignedUserRoles
	WHERE RoleId = @RoleId
	
	DELETE FROM dbo.GrantableRoles
	WHERE
		RoleId = @RoleId
		OR
		GrantableRoleId = @RoleId
		
	DELETE FROM dbo.RolePermissions
	WHERE RoleId = @RoleId

	DELETE From dbo.Roles Where Id = @RoleId	
	SELECT @@ROWCOUNT
END
GO
/****** Object:  StoredProcedure [dbo].[sm_ROLEMANAGEMENTSERVICE_UpdateRole_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Umair Rafique
-- Create date: 29/06/2011
-- Description:	Updates a role, its permssions and grantable roles
-- Modifier:	Vitaly Bevzik
-- Create date: 15/08/2013
-- Description:	Remove roles assigned to exam from users if AssignableToExams is set to 0
-- =============================================
CREATE PROCEDURE [dbo].[sm_ROLEMANAGEMENTSERVICE_UpdateRole_sp]
	-- Add the parameters for the stored procedure here
	@RoleId Int, 
	@AssignableToGlobal bit,
	@AssignableToExams bit,
	@GrantableRoles NVarchar(Max),
	@RolesPermissions NVarchar(Max)
AS
BEGIN
	
	UPDATE dbo.Roles 
	SET 
		AssignableToGlobal = @AssignableToGlobal,
		AssignableToExams = @AssignableToExams
	Where ID = @RoleID
	
	IF (@AssignableToExams = 0)
	BEGIN
		--Remove roles assigned to exam from users if AssignableToExams is set to 0
		DELETE FROM AssignedUserRoles
		WHERE RoleID = @RoleId AND ExamID != 0
		
		--Remove orphan assigned trainig roles
		DECLARE	 @trainingRoleId INT = (SELECT TOP (1) ID FROM Roles WHERE TrainingRole = 1)		
		DELETE AUR FROM
		AssignedUserRoles AUR
		WHERE AUR.RoleID = @trainingRoleId AND (
			SELECT COUNT(*) FROM AssignedUserRoles AUR1
			WHERE AUR1.UserID = AUR.UserID AND AUR1.ExamID = AUR.ExamID
		) = 1
		
	END
	
	
	DELETE From dbo.GrantableRoles Where RoleId = @RoleId
	
	INSERT INTO dbo.GrantableRoles (RoleID, GrantableRoleID)
		SELECT @RoleId, * FROM dbo.ParamsToList(@GrantableRoles)	 	
	
	DELETE RP
	From dbo.RolePermissions RP
		INNER JOIN dbo.Permissions P ON p.ID = RP.PermissionID
	Where RoleID = @RoleID AND P.InternalPermission = 0
	
	
	INSERT INTO dbo.RolePermissions (RoleID, PermissionID)
		SELECT @RoleID , * From dbo.ParamsToList(@RolesPermissions)	 	
END
GO
/****** Object:  StoredProcedure [dbo].[sm_ROLESMANAGEMENTSERVICE_getAssignableRoles_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Tom Gomersall
-- Create date: 08/02/11
-- Description:	Gets all the permissions
-- =============================================
CREATE PROCEDURE [dbo].[sm_ROLESMANAGEMENTSERVICE_getAssignableRoles_sp]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT	RoleID				AS 'RoleID'
			,GrantableRoleID	AS 'GrantableRoleID'
	FROM GrantableRoles
END
GO
/****** Object:  StoredProcedure [dbo].[sm_ROLESMANAGEMENTSERVICE_getPermissions_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Tom Gomersall
-- Create date: 08/02/11
-- Description:	Gets all the permissions
-- =============================================
CREATE PROCEDURE [dbo].[sm_ROLESMANAGEMENTSERVICE_getPermissions_sp]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT	ID						AS 'ID'
			,Name					AS 'Name'
			,GlobalSupport			AS 'Global'
			,AssignableToExams		AS 'Exam'
			,IsNULL(ParentID, 0)	AS 'GroupId'
			,InternalPermission		AS InternalPermission
	FROM dbo.[Permissions]
	WHERE Training = 0
	ORDER BY Name				
END
GO
/****** Object:  StoredProcedure [dbo].[sm_ROLESMANAGEMENTSERVICE_getRoles_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Tom Gomersall
-- Create date: 08/02/11
-- Description:	Gets all the roles
-- =============================================
CREATE PROCEDURE [dbo].[sm_ROLESMANAGEMENTSERVICE_getRoles_sp]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	  -- Insert statements for procedure here
	SELECT [ID]						As RoleID
      ,[Name]						AS Name
      ,[AssignableToGlobal]			AS AssignableToGlobal
      ,[AssignableToExams]			AS AssignableToExams
      ,[AssignedByIntegration]		AS AssignedByIntegration
      ,PermissionID					AS PermissionID
      ,TrainingRole					AS TrainingRole
      ,InternalRole					AS[InternalRole]
	FROM [dbo].[Roles]
	LEFT JOIN RolePermissions
	ON RolePermissions.RoleID = Roles.ID	
	ORDER BY NAME
	

END
GO
/****** Object:  StoredProcedure [dbo].[sm_SCRIPTREVIEW_confirmScripts_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sm_SCRIPTREVIEW_confirmScripts_sp]
-- Add the parameters for the stored procedure here
	@scriptIDs NVARCHAR(MAX)
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;
    
    --First check that all of the exams are either marking complete or already complete.
    IF (
           EXISTS
           (
               SELECT ID
               FROM   dbo.CandidateExamVersions
               WHERE  ID IN (SELECT *
                             FROM   dbo.ParamsToList(@scriptIDs))
                      AND ExamSessionState NOT IN (4 ,6)
                      AND IsFlagged = 1
           )
       )
    BEGIN
        --Raise an error because not all the scripts are in the correct states.
        return 50010;
    END

    BEGIN TRY
		BEGIN TRAN
    
			UPDATE dbo.CandidateExamVersions
			SET    ExamSessionState = 6
			WHERE  ID IN (SELECT * FROM dbo.ParamsToList(@scriptIDs))
				   AND ExamSessionState IN (4 ,6)
				   AND IsFlagged = 0
    
			DECLARE @date DATETIME=GETDATE()
    
			INSERT INTO dbo.CandidateExamVersionStatuses
			  (
				-- ID -- this column value is auto-generated
				[Timestamp]
			   ,StateID
			   ,CandidateExamVersionID
			  )
			SELECT @date
				  ,6
				  ,[Value]
			FROM   dbo.ParamsToList(@scriptIDs) temp
			INNER JOIN dbo.CandidateExamVersions cev on temp.Value = cev.ID
			WHERE cev.ExamSessionState != 6

		COMMIT TRAN
    END TRY
        BEGIN CATCH
            IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION

            DECLARE @myErrorNum INT = ERROR_NUMBER()
            DECLARE @myMessage NVARCHAR(MAX) = ERROR_MESSAGE()
            DECLARE @myFullMessage NVARCHAR(MAX) = CONVERT(NVARCHAR(10), @myErrorNum) + ':' + @myMessage
            
			RAISERROR (@myFullMessage, 16, 1)
        END CATCH
END
GO
/****** Object:  StoredProcedure [dbo].[sm_SCRIPTREVIEW_FILTER_GetCountries_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	George Chernov
-- Create date: 27/09/2013
-- Description:	Filtes information for ScriptReview for Countries column
-- =============================================
CREATE PROCEDURE [dbo].[sm_SCRIPTREVIEW_FILTER_GetCountries_sp]
-- Add the parameters for the stored procedure here
	@userId INT,
	@maxNum INT
AS
BEGIN

    SET NOCOUNT ON;
    
    SELECT DISTINCT TOP(@maxNum) 
           Country AS 'Country'
    FROM   dbo.ScriptReview_view srv 
    INNER JOIN dbo.sm_getExamPermissionsForScriptReview_fn(@userId) perms 
    ON perms.PermExamID  = srv.ExamID
END
GO
/****** Object:  StoredProcedure [dbo].[sm_SCRIPTREVIEW_FILTER_GetExams_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	George Chernov
-- Create date: 27/09/2013
-- Description:	Filtes information for ScriptReview for Exams column
-- =============================================
CREATE PROCEDURE [dbo].[sm_SCRIPTREVIEW_FILTER_GetExams_sp]
-- Add the parameters for the stored procedure here
	@userId INT,
	@maxNum INT
AS
BEGIN

    SET NOCOUNT ON;
    
    SELECT DISTINCT TOP(@maxNum) 
           Exam			AS 'Exam'
          ,ExamID		AS 'ExamID'
    FROM   dbo.ScriptReview_view srv 
    INNER JOIN dbo.sm_getExamPermissionsForScriptReview_fn(@userId) perms 
    ON perms.PermExamID = srv.ExamID
END
GO
/****** Object:  StoredProcedure [dbo].[sm_SCRIPTREVIEW_FILTER_GetExamVersions_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	George Chernov
-- Create date: 27/09/2013
-- Description:	Filtes information for ScriptReview for ExamVersion column
-- =============================================
CREATE PROCEDURE [dbo].[sm_SCRIPTREVIEW_FILTER_GetExamVersions_sp]
-- Add the parameters for the stored procedure here
	@userId INT,
	@maxNum INT
AS
BEGIN

    SET NOCOUNT ON;
    
    SELECT DISTINCT TOP(@maxNum) 
           ExamVersion    AS 'ExamVersion'
          ,ExamVersionID  AS 'ExamVersionID'
    FROM   dbo.ScriptReview_view srv 
    INNER JOIN dbo.sm_getExamPermissionsForScriptReview_fn(@userId) perms 
    ON perms.PermExamID  = srv.ExamID
END
GO
/****** Object:  StoredProcedure [dbo].[sm_SCRIPTREVIEW_FILTER_GetImported_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	George Chernov
-- Create date: 27/09/2013
-- Description:	Filtes information for ScriptReview for Imported column
-- =============================================
CREATE PROCEDURE [dbo].[sm_SCRIPTREVIEW_FILTER_GetImported_sp]
-- Add the parameters for the stored procedure here
	@userId INT
AS
BEGIN

    SET NOCOUNT ON;
    
    SELECT	MIN(Imported)   AS 'MinImportedDate'
           ,MAX(Imported)   AS 'MaxImportedDate'
    FROM   dbo.ScriptReview_view srv 
    INNER JOIN dbo.sm_getExamPermissionsForScriptReview_fn(@userId) perms 
    ON perms.PermExamID  = srv.ExamID
END
GO
/****** Object:  StoredProcedure [dbo].[sm_SCRIPTREVIEW_FILTER_GetKeycode_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	George Chernov
-- Create date: 27/09/2013
-- Description:	Filtes information for ScriptReview for Keycode column
-- =============================================
CREATE PROCEDURE [dbo].[sm_SCRIPTREVIEW_FILTER_GetKeycode_sp]
-- Add the parameters for the stored procedure here
	@userId INT,
	@maxNum INT
AS
BEGIN

    SET NOCOUNT ON;
    
    SELECT DISTINCT TOP(@maxNum) 
           Keycode AS 'Keycode'
    FROM   dbo.ScriptReview_view srv 
    INNER JOIN dbo.sm_getExamPermissionsForScriptReview_fn(@userId) perms 
    ON perms.PermExamID  = srv.ExamID
END
GO
/****** Object:  StoredProcedure [dbo].[sm_SCRIPTREVIEW_FILTER_GetMarks_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	George Chernov
-- Create date: 27/09/2013
-- Description:	Filtes information for ScriptReview for Marks column
-- =============================================
CREATE PROCEDURE [dbo].[sm_SCRIPTREVIEW_FILTER_GetMarks_sp]
-- Add the parameters for the stored procedure here
	@userId INT
AS
BEGIN

    SET NOCOUNT ON;
    
    SELECT	MAX(MARK)           AS 'MaxMark'
			,MIN(MARK)          AS 'MinMark'
    FROM   dbo.ScriptReview_view srv 
    INNER JOIN dbo.sm_getExamPermissionsForScriptReview_fn(@userId) perms 
    ON perms.PermExamID  = srv.ExamID
END
GO
/****** Object:  StoredProcedure [dbo].[sm_SCRIPTREVIEW_FILTER_GetPercentage_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	George Chernov
-- Create date: 27/09/2013
-- Description:	Filtes information for ScriptReview for Percentage column
-- =============================================
CREATE PROCEDURE [dbo].[sm_SCRIPTREVIEW_FILTER_GetPercentage_sp]
-- Add the parameters for the stored procedure here
	@userId INT
AS
BEGIN

    SET NOCOUNT ON;
    
    SELECT MAX(Percentage)     AS 'MaxPercentage'
          ,MIN(Percentage)     AS 'MinPercentage'
    FROM   dbo.ScriptReview_view srv 
    INNER JOIN dbo.sm_getExamPermissionsForScriptReview_fn(@userId) perms 
    ON perms.PermExamID  = srv.ExamID
END
GO
/****** Object:  StoredProcedure [dbo].[sm_SCRIPTREVIEW_FILTER_GetQualifications_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	George Chernov
-- Create date: 27/09/2013
-- Description:	Filtes information for ScriptReview for Qualifications column
-- =============================================
CREATE PROCEDURE [dbo].[sm_SCRIPTREVIEW_FILTER_GetQualifications_sp]
-- Add the parameters for the stored procedure here
	@userId INT,
	@maxNum INT
AS
BEGIN

    SET NOCOUNT ON;
    
    SELECT DISTINCT TOP(@maxNum) 
           Qualification    AS 'Qualification'
          ,QualificationID  AS 'QualificationID'
    FROM   dbo.ScriptReview_view srv 
    INNER JOIN dbo.sm_getExamPermissionsForScriptReview_fn(@userId) perms 
    ON perms.PermExamID  = srv.ExamID
END
GO
/****** Object:  StoredProcedure [dbo].[sm_SCRIPTREVIEW_FILTER_GetSubmitted_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	George Chernov
-- Create date: 27/09/2013
-- Description:	Filtes information for ScriptReview for Submitted column
-- =============================================
CREATE PROCEDURE [dbo].[sm_SCRIPTREVIEW_FILTER_GetSubmitted_sp]
-- Add the parameters for the stored procedure here
	@userId INT
AS
BEGIN

    SET NOCOUNT ON;
    
    SELECT	MIN(Submitted)  AS 'MinSubmittedDate'
          ,MAX(Submitted)  AS 'MaxSubmittedDate'
    FROM   dbo.ScriptReview_view srv 
    INNER JOIN dbo.sm_getExamPermissionsForScriptReview_fn(@userId) perms 
    ON perms.PermExamID  = srv.ExamID
END
GO
/****** Object:  StoredProcedure [dbo].[sm_SCRIPTREVIEW_getAllItemsForScript_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Vitaly Bevzik
-- Create date: 6/03/2013
-- Description:	Get Items for script
-- =============================================
CREATE PROCEDURE [dbo].[sm_SCRIPTREVIEW_getAllItemsForScript_sp]
    @candidateExamVersionID INT
AS 
    BEGIN
        SET NOCOUNT ON;

        SELECT  I.ID AS ID ,
                I.ExternalItemName AS Name ,
                I.ExternalItemID AS ExternalID ,
                I.Version AS Version ,
                I.LayoutName AS LayoutName ,
                I.AllowAnnotations AS AnnotationType
        FROM    dbo.CandidateExamVersions CEV
                INNER JOIN dbo.CandidateResponses CR ON CR.CandidateExamVersionID = CEV.ID
                INNER JOIN dbo.UniqueResponses UR ON UR.id = CR.UniqueResponseID
                INNER JOIN dbo.Items I ON I.ID = UR.itemId
        WHERE   CEV.ID = @candidateExamVersionID
        ORDER BY I.ID ASC
    END
GO
/****** Object:  StoredProcedure [dbo].[sm_SCRIPTREVIEW_getExamIdsForScripts_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		George Chernov
-- Create date: 11/09/13
-- Description:	Gets the exam IDs for some exam scripts
-- =============================================

CREATE PROCEDURE [dbo].[sm_SCRIPTREVIEW_getExamIdsForScripts_sp]
	-- Add the parameters for the stored procedure here
    @scriptIDs NVARCHAR(MAX),
	@forSingleVersion bit = 0
AS 
    BEGIN
    
        SET NOCOUNT ON;

		IF @forSingleVersion = 1
			IF ( 
				( SELECT COUNT(DISTINCT CEV.ExamVersionID)
				FROM CandidateExamVersions CEV
				WHERE CEV.ID IN ( SELECT * 
								  FROM dbo.ParamsToList(@scriptIDs) )
			) > 1 ) 
			BEGIN
				return 50008;
			END
		
        SELECT DISTINCT
                ev.ExamID
        FROM    dbo.CandidateExamVersions CEV INNER JOIN
		        dbo.ExamVersions ev ON ev.ID = CEV.ExamVersionID
        WHERE   CEV.ID IN ( SELECT  *
                            FROM    dbo.ParamsToList(@scriptIDs) )
    END


GO
/****** Object:  StoredProcedure [dbo].[sm_SCRIPTREVIEW_getMarkingHistoryItems_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Vitaly Bevzik
-- Create date: 10/04/2013
-- Description:	Get items for marking history for current script Id uniqueResponse Id
-- =============================================
CREATE PROCEDURE [dbo].[sm_SCRIPTREVIEW_getMarkingHistoryItems_sp]
	@scriptId INT,
	@uniqueResponseId BIGINT,
	@markingMethods VARCHAR(512)
AS
BEGIN
    SET NOCOUNT ON;
    
    DECLARE @resultTable TABLE(
                DateOfMarking DATETIME
               ,Firstname NVARCHAR(50)
               ,Surname NVARCHAR(50)
               ,Mark DECIMAL(18 ,10)
               ,TotalMark DECIMAL(18 ,10)
               ,Comment NVARCHAR(MAX)
               ,MarkingMethod INT
               ,Issues NVARCHAR(MAX)
            );
    
    
    -- get last valid group for a script
	DECLARE @lastValidGroup INT = 
	(
		SELECT TOP 1 UGR.GroupDefinitionID 
		FROM dbo.CandidateGroupResponses CGR
			INNER JOIN dbo.UniqueGroupResponses UGR ON UGR.ID = CGR.UniqueGroupResponseID
			INNER JOIN dbo.UniqueGroupResponseLinks UGRL ON UGRL.UniqueGroupResponseID = UGR.ID
			INNER JOIN dbo.UniqueResponses UR ON UR.id = UGRL.UniqueResponseId
			INNER JOIN dbo.GroupDefinitionItems GDI ON GDI.ItemID = UR.itemId AND GDI.GroupID = UGR.GroupDefinitionID
			INNER JOIN dbo.GroupDefinitionStructureCrossRef GDSCR ON GDSCR.GroupDefinitionID = UGR.GroupDefinitionID
			INNER JOIN dbo.ExamVersionStructures EVS ON  EVS.ID = GDSCR.ExamVersionStructureID
		WHERE CGR.CandidateExamVersionID = @scriptId
			AND UGRL.UniqueResponseId = @uniqueResponseId
			AND GDI.ViewOnly = 0
			AND GDSCR.Status = 0 -- 0 = Active
			AND EVS.StatusID = 0 -- 0 = Released
		ORDER BY GroupDefinitionID DESC
	)
    
    INSERT INTO @resultTable
    SELECT AGM.Timestamp			AS DateOfMarking
          ,U.Forename				AS Firstname
          ,U.Surname				AS Surname
          ,CASE WHEN (AGM.MarkingMethodId = 11 AND AGM.EscalatedWithoutMark = 1) THEN -1 ELSE AIM.AssignedMark END AS Mark
          ,I.TotalMark				AS TotalMark
          ,AGM.ParkedAnnotation		AS Comment
		  ,AGM.MarkingMethodId		AS MarkingMethod
           ,(
				SELECT DISTINCT RTRIM(PRL.Name) + ', '
				FROM dbo.AssignedGroupMarksParkedReasonCrossRef AGMPRC 
				INNER JOIN dbo.ParkedReasonsLookup PRL 
				ON PRL.ID = AGMPRC.ParkedReasonId
				WHERE AGMPRC.AssignedGroupMarkId = AGM.ID
				FOR XML PATH('')
		   )						AS Issues
    FROM   dbo.AssignedGroupMarks AGM
           INNER JOIN dbo.AssignedItemMarks AIM ON  AGM.ID = AIM.GroupMarkId
           INNER JOIN dbo.Users U ON  U.ID = AGM.UserId
           INNER JOIN dbo.UniqueResponses UR ON  UR.id = AIM.UniqueResponseId
           INNER JOIN dbo.Items I ON  UR.itemId = I.ID
           INNER JOIN dbo.ParamsToList(@markingMethods) MM ON  MM.Value = AGM.MarkingMethodId
    WHERE  UR.id = @uniqueResponseId
           AND AGM.UniqueGroupResponseId in (SELECT CGR.UniqueGroupResponseID FROM dbo.CandidateGroupResponses CGR WHERE CGR.CandidateExamVersionID = @scriptId)
           AND AGM.GroupDefinitionID = @lastValidGroup
                      
    INSERT INTO @resultTable
    SELECT M.Timestamp  AS DateOfMarking
          ,U.Forename   AS Firstname
          ,U.Surname    AS Surname
          ,M.Mark       AS Mark
          ,I.TotalMark  AS TotalMark
          ,M.Comment    AS Comment
          ,NULL         AS MarkingMethod
		  ,NULL			AS Issues
    FROM   dbo.Moderations M
           INNER JOIN dbo.Users U ON  U.ID = M.UserId
           INNER JOIN dbo.UniqueResponses UR ON  UR.id = M.UniqueResponseId
           INNER JOIN dbo.Items I ON  UR.itemId = I.ID
    WHERE  UR.id = @uniqueResponseId AND M.CandidateExamVersionId = @scriptId
    
    SELECT DateOfMarking
          ,Firstname
          ,Surname
          ,Mark
          ,TotalMark
          ,Comment
          ,MarkingMethod
          ,Issues
    FROM   @resultTable
    ORDER BY DateOfMarking
END



GO
/****** Object:  StoredProcedure [dbo].[sm_SCRIPTREVIEW_getScriptResponses_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Vitaly Bevzik
-- Create date: 11/09/2013
-- Description:	Script user responses for review script
-- =============================================
CREATE PROCEDURE [dbo].[sm_SCRIPTREVIEW_getScriptResponses_sp]
    @candidateExamVersionID INT,
    @isModerate BIT
AS 
    BEGIN
        SET NOCOUNT ON;
        
        -- if script requested for moderation check its state is complete or pending
        IF (@isModerate = 1)
			BEGIN
				DECLARE @status INT = (SELECT ExamSessionState FROM CandidateExamVersions WHERE ID = @candidateExamVersionID)
				IF @status NOT IN (3,4)
					BEGIN
					--Raise an error because not all the scripts are in the correct states.				
						DECLARE @myMessage NVARCHAR(MAX) = 'Script is not in complete or pending state'					
						RAISERROR (@myMessage, 16, 1)
					END
			END
        
		-- Select script information
		
		SELECT 
				CEV.ID,				
				CEV.CandidateForename,
				CEV.CandidateSurname,
				CEV.CandidateRef,
				CEV.ScriptType,
				E.Name as ExamName,
				E.ID as ExamId,
				E.AnnotationSchema
		FROM dbo.CandidateExamVersions CEV
			INNER JOIN dbo.ExamVersions EV	ON EV.ID = CEV.ExamVersionID
			INNER JOIN dbo.Exams E			ON E.ID = EV.ExamID
		WHERE CEV.ID = @candidateExamVersionID
		
		DECLARE @structureId INT = (SELECT ExamVersionStructureID FROM dbo.CandidateExamVersions WHERE ID = @candidateExamVersionID)
		-- Select Script user's unique responses
		;WITH MarkedResponses AS
		(SELECT  ROW_NUMBER() OVER( PARTITION BY AIM.UniqueResponseId ORDER BY AGM.Timestamp DESC ) AS RowNum,
								AIM.UniqueResponseId,
								AIM.annotationData,
								CASE WHEN (AGM.MarkingMethodId = 11 AND AGM.EscalatedWithoutMark = 1) THEN -1 ELSE AIM.AssignedMark END AssignedMark,
								AIM.markedMetadataResponse,
								AGM.MarkingMethodId,
								AGM.Timestamp
						FROM dbo.CandidateGroupResponses CGR
						INNER JOIN dbo.UniqueGroupResponses UGR ON UGR.ID = CGR.UniqueGroupResponseID
						INNER JOIN dbo.AssignedGroupMarks AGM 
						ON (AGM.UniqueGroupResponseId = UGR.ID AND AGM.MarkingMethodId NOT IN (2,3))
						INNER JOIN dbo.AssignedItemMarks AIM ON AIM.GroupMarkId = AGM.ID						
						WHERE 
							CGR.CandidateExamVersionID = @candidateExamVersionID
							AND AGM.GroupDefinitionID = (
															SELECT TOP 1 UGR1.GroupDefinitionID FROM ..CandidateGroupResponses CGR1
															INNER JOIN UniqueGroupResponses UGR1 ON UGR1.ID = CGR1.UniqueGroupResponseID
															INNER JOIN UniqueGroupResponseLinks UGRL1 ON UGRL1.UniqueGroupResponseID = UGR1.ID
															INNER JOIN UniqueResponses UR ON UR.id = UGRL1.UniqueResponseId
															INNER JOIN GroupDefinitionItems GDI ON GDI.ItemID = UR.itemId AND GDI.GroupID = UGR1.GroupDefinitionID
															WHERE  CGR1.CandidateExamVersionID = @candidateExamVersionID
															AND UGRL1.UniqueResponseId = AIM.UniqueResponseId
															AND GDI.ViewOnly = 0
															AND UGR1.GroupDefinitionID IN (
																SELECT GDSCR1.GroupDefinitionID
																FROM   dbo.GroupDefinitionStructureCrossRef GDSCR1
																	   INNER JOIN dbo.ExamVersionStructures EVS1 ON  EVS1.ID = GDSCR1.ExamVersionStructureID
																WHERE  GDSCR1.Status = 0 -- 0 = Active
																	   AND EVS1.StatusID = 0 -- 0 = Released
															)
															ORDER BY UGR1.GroupDefinitionID DESC
												         )
		)
		SELECT 		
			I.ID						AS ItemId ,		
            I.ExternalItemID			AS ExternalId ,
            I.Version					AS ItemVersion ,
            I.LayoutName				AS LayoutName ,
			I.ExternalItemName			AS ExternalItemName,
			I.TotalMark					AS ItemTotalMark ,
			I.AllowAnnotations			AS AllowAnnotations,
			I.ExamGroupID				AS ExamGroupID,
			I.ExamGroupName				AS ExamGroupName,
			ES.ID						AS ExamSectionId,
			ES.Name						AS ExamSectionName,
			UR.responseData				AS ResponseData,
            UR.id						AS ResponseId,
			MR.annotationData			AS AnnotationData,
			CASE WHEN MR.MarkingMethodId IN (8,9,14,16)
				THEN NULL 
			ELSE 
				MR.assignedMark 
			END                         AS AssignedMark,
			MR.Timestamp				AS MarkerTimestamp,
			ISNULL(MR.markedMetadataResponse, UR.markedMetadataResponse) AS MarkedMetadataResponse,
			MO.annotationData			AS ModeratorAnnotationData,
			MO.Mark						AS ModeratorMark, 
			MO.Timestamp				AS ModeratorTimestamp,
			MO.Comment					AS ModeratorComment,
			ISNULL(MO.markedMetadataResponse, UR.markedMetadataResponse) AS ModeratorMarkedMetadataResponse,
			CAST(CASE WHEN MR.MarkingMethodId = 12 THEN 1 ELSE 0 END AS BIT) AS IsComputerMarked
		FROM dbo.CandidateResponses CR
			INNER JOIN dbo.UniqueResponses UR on UR.id = CR.UniqueResponseID
			INNER JOIN dbo.Items I ON I.ID = UR.itemId
			INNER JOIN dbo.ExamSectionItems ESI ON ESI.ItemID = I.ID AND ESI.ExamStructureId = @structureId
			INNER JOIN dbo.ExamSections ES ON ES.ID = ESI.ExamSectionID  
			LEFT JOIN MarkedResponses MR ON MR.UniqueResponseId = UR.id		
			LEFT JOIN dbo.Moderations MO on MO.UniqueResponseId = UR.id 
											AND MO.CandidateExamVersionId = @candidateExamVersionID 
											AND MO.IsActual = 1	
		WHERE    CR.CandidateExamVersionID = @candidateExamVersionID
			 AND (MR.RowNum = 1 OR MR.RowNum IS NULL)
		ORDER BY I.ID

    END
GO
/****** Object:  StoredProcedure [dbo].[sm_SCRIPTREVIEW_remarkScripts_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Tom Gomersall
-- Create date: 22/06/11
-- Description:	Sets the exams to be re-marked.

-- Modified:	George Chernov
-- Date:		29/05/2013
-- Description:	Change for group level
-- =============================================
CREATE PROCEDURE [dbo].[sm_SCRIPTREVIEW_remarkScripts_sp]
-- Add the parameters for the stored procedure here
	@scriptIDs NVARCHAR(MAX)
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;
    BEGIN TRY
        BEGIN TRAN
        --First check that all of the exams are either marking complete or already set to re-mark.
        IF (
               EXISTS
               (
                   SELECT ID
                   FROM   CandidateExamVersions
                   WHERE  ID IN (SELECT *
                                 FROM   dbo.ParamsToList(@scriptIDs))
                          AND ExamSessionState NOT IN (4, 5)
               )
           )
        BEGIN
            --Raise an error because not all the scripts are in the correct states.
            return 50009;
        END
        
        DECLARE @cevsToUpdate TABLE([Timestamp] DATETIME ,StateID INT ,cevId INT)
        DECLARE @date DATETIME=GETDATE()
        
        INSERT INTO @cevsToUpdate
        SELECT @date
              ,5
              ,cev.ID
        FROM   CandidateExamVersions cev
        WHERE  cev.ID IN (SELECT *
                      FROM   dbo.ParamsToList(@scriptIDs))
               AND cev.ExamSessionState in (4)	 -- 5
        
        BEGIN TRAN        
        
			UPDATE CandidateExamVersions
			SET    ExamSessionState = 5
			WHERE  ID IN (SELECT cevId FROM @cevsToUpdate)
        
			INSERT INTO CandidateExamVersionStatuses
			(
        		-- ID -- this column value is auto-generated
        		[Timestamp],
        		StateID,
        		CandidateExamVersionID
			)
			SELECT
        		ctu.[Timestamp],
        		ctu.StateID,
        		ctu.cevId
			FROM
        		@cevsToUpdate ctu
        	
        COMMIT TRAN
        
        DECLARE @myUniqueGroupResponsesToUnMark TABLE (ID BIGINT)
        
        INSERT INTO @myUniqueGroupResponsesToUnMark
        SELECT ID
        FROM   dbo.UniqueGroupResponses
        WHERE  ID IN (SELECT UniqueGroupResponseID
                      FROM   dbo.CandidateGroupResponses
                             JOIN dbo.CandidateExamVersions
                                  ON  CandidateGroupResponses.CandidateExamVersionID = 
                                      CandidateExamVersions.ID
                      WHERE  ID IN (SELECT *
                                    FROM   dbo.ParamsToList(@scriptIDs)))
               AND EXISTS(
                       SELECT AssignedGroupMarks.id
                       FROM   AssignedGroupMarks
                       WHERE  dbo.AssignedGroupMarks.uniqueGroupResponseId = dbo.UniqueGroupResponses.ID
                              AND dbo.AssignedGroupMarks.isConfirmedMark = 1
                              AND dbo.AssignedGroupMarks.markingMethodId IN (4 ,15, 17)
                   )
        
        -- ExamState moved to remark pending, so reset the confirmed marks. State management will reset it to state = 1
        -- It un-marks only items that were marked normally (markingMethodId 4 or 15)
        DECLARE @myUniqueGroupResponseID BIGINT
                
        WHILE (
                  SELECT COUNT(ID)
                  FROM   @myUniqueGroupResponsesToUnMark
              )>0
        BEGIN
            SET @myUniqueGroupResponseID = (
                    SELECT TOP 1 ID
                    FROM   @myUniqueGroupResponsesToUnMark
                )
            
            UPDATE UniqueGroupResponses
            SET    ConfirmedMark = NULL
            WHERE  ID = @myUniqueGroupResponseID
            
            EXEC sm_SHARED_removeMarksFromMarkedMetadataFromUGRLinks_sp 
                 @uniqueGroupResponseID=@myUniqueGroupResponseID
                ,@updateTable=1
            
            DELETE 
            FROM   @myUniqueGroupResponsesToUnMark
            WHERE  ID = @myUniqueGroupResponseID
        END
        
        --Reset isConfirmMark in the assigned marks to 0		
        UPDATE dbo.AssignedGroupMarks
        SET    isConfirmedMark = 0
        WHERE  uniqueGroupResponseId IN (
											SELECT UniqueGroupResponseID
											FROM   dbo.CandidateGroupResponses
											JOIN dbo.CandidateExamVersions
                                            ON  CandidateGroupResponses.CandidateExamVersionID = CandidateExamVersions.ID
											WHERE  ID IN (
															SELECT * FROM   dbo.ParamsToList(@scriptIDs)
														 )
                                        )
        AND dbo.AssignedGroupMarks.markingMethodId IN (4 ,15, 17)
        
        COMMIT TRANSACTION
    END TRY
    BEGIN CATCH
		IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION

        DECLARE @myErrorNum INT=ERROR_NUMBER()
        DECLARE @myMessage NVARCHAR(MAX)=ERROR_MESSAGE()
        DECLARE @myFullMessage NVARCHAR(MAX)=CONVERT(NVARCHAR(10) ,@myErrorNum) +':'+@myMessage
            
        RAISERROR (@myFullMessage ,16 ,1)
    END CATCH
END
GO
/****** Object:  StoredProcedure [dbo].[sm_SCRIPTREVIEW_setFlagged_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		George Chernov
-- Create date: 10/09/13
-- Description:	Sets the flags of the scripts.
-- =============================================
CREATE PROCEDURE [dbo].[sm_SCRIPTREVIEW_setFlagged_sp]

	@scriptIDs	NVARCHAR(MAX),
	@flagged	BIT

AS
BEGIN

	SET NOCOUNT ON;
	
	IF(@flagged = 1)
		BEGIN
			UPDATE dbo.CandidateExamVersions
			SET IsFlagged = 1
			WHERE 
				ID IN 
				(
					SELECT * 
					FROM dbo.ParamsToList(@scriptIDs)
				)
		END
	ELSE
		BEGIN	
			UPDATE dbo.CandidateExamVersions
			SET IsFlagged = 0
			WHERE 
				ID IN 
				(
					SELECT * 
					FROM dbo.ParamsToList(@scriptIDs)
				)
		END
END
GO
/****** Object:  StoredProcedure [dbo].[sm_SCRIPTREVIEW_setModeratedMarkToNotActual_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		George Chernov
-- Create date: 26/09/2013
-- Description:	Delete moderated mark
-- =============================================
CREATE PROCEDURE [dbo].[sm_SCRIPTREVIEW_setModeratedMarkToNotActual_sp]
	@scriptId INT,
	@uniqueResponseId BIGINT
AS
BEGIN
    SET NOCOUNT ON;
    
    UPDATE dbo.Moderations
    SET    IsActual                       = 0
    WHERE  UniqueResponseId               = @uniqueResponseId
           AND CandidateExamVersionId     = @scriptId
           
    -- update examState to resend it to SA
    UPDATE dbo.CandidateExamVersions
    SET ExamSessionState = 3 -- Complete
    WHERE ID = @scriptId
    
    -- check if Script does't have Actual moderated marks
    IF NOT EXISTS(SELECT *
					FROM Moderations AS M
					WHERE M.CandidateExamVersionId = @scriptId
					AND M.IsActual = 1)
	BEGIN
		-- if so change IsModerated flag of CandidateExamVersion to 0 
		UPDATE CandidateExamVersions
		SET IsModerated = 0
		WHERE CandidateExamVersions.ID = @scriptId
	
	END
    
END
GO
/****** Object:  StoredProcedure [dbo].[sm_SCRIPTREVIEW_setVIPMode_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Tom Gomersall
-- Create date: 22/06/11
-- Description:	Sets the VIP mode of the scripts.
-- =============================================
CREATE PROCEDURE [dbo].[sm_SCRIPTREVIEW_setVIPMode_sp]
	-- Add the parameters for the stored procedure here
	@scriptIDs	NVARCHAR(MAX),
	@vip		BIT

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	--Use a transaction here because there are two updates
	BEGIN TRY
		BEGIN TRAN
					
			DECLARE @vipScripts table(id bigint not null)
			INSERT INTO @vipScripts
			SELECT * FROM dbo.ParamsToList(@scriptIDs)
			
			--First set the exam script VIP mode.
			UPDATE dbo.CandidateExamVersions
			SET VIP = @vip
			WHERE ID IN (SELECT id FROM @vipScripts)

			IF(@vip = 1)
				BEGIN
					--Then set the VIP mode for each of the unique responses
					UPDATE dbo.UniqueGroupResponses
					SET VIP = 1
					FROM dbo.UniqueGroupResponses UGR
					JOIN dbo.CandidateGroupResponses CGR ON CGR.UniqueGroupResponseID = UGR.ID
					JOIN @vipScripts CEV ON CEV.id = CGR.CandidateExamVersionID
				END
			ELSE
				BEGIN	
					
					--Then unset the VIP mode for each of the unique responses if we can
					WITH ResponsesToUnVip_CTE (ugrid)
					AS
					(
						SELECT DISTINCT UGR.ID
						FROM dbo.UniqueGroupResponses UGR
						JOIN dbo.CandidateGroupResponses CGR ON CGR.UniqueGroupResponseID = UGR.ID
						JOIN @vipScripts CEV ON CEV.id = CGR.CandidateExamVersionID
					),
					ResponsesToUnVipWithExtra_CTE (ugrid, extraScripts)
					AS
					(
						SELECT UNVIP.ugrid, COUNT(*) AS extraScripts 	
						FROM ResponsesToUnVip_CTE UNVIP
						JOIN dbo.CandidateGroupResponses CGR ON (CGR.UniqueGroupResponseID = UNVIP.ugrid)
						JOIN dbo.CandidateExamVersions CEV ON (CEV.ID = CGR.CandidateExamVersionID AND CEV.VIP = 1)
						WHERE CEV.ID NOT IN (SELECT id FROM @vipScripts)
						GROUP BY UNVIP.ugrid
					)
					UPDATE dbo.UniqueGroupResponses
					SET VIP = 0
					FROM dbo.UniqueGroupResponses UGR
					JOIN ResponsesToUnVip_CTE IDS ON IDS.ugrid = UGR.ID
					LEFT JOIN ResponsesToUnVipWithExtra_CTE IDScripts ON IDScripts.ugrid = IDS.ugrid
					WHERE IDScripts.extraScripts IS NULL OR IDScripts.extraScripts = 0;
			END
				
		COMMIT TRAN
	END TRY
	BEGIN CATCH
		ROLLBACK TRAN
		DECLARE @myErrorNum INT = ERROR_NUMBER()
		DECLARE @myMessage NVARCHAR(MAX) = ERROR_MESSAGE()
		DECLARE @myFullMessage NVARCHAR(MAX) = CONVERT(NVARCHAR(10), @myErrorNum) + ':' + @myMessage
		RAISERROR (@myFullMessage, 16, 1)
	END CATCH
END
GO
/****** Object:  StoredProcedure [dbo].[sm_SCRIPTREVIEW_submitModeratorResponse_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Stored Procedure

-- =============================================
-- Author:		Vitaly Bevzik
-- Create date: 27/09/2013
-- Description:	Submit moderations for script response

-- Modifier: Anton Burdyugov
-- Create date: 2/10/2013
-- Description:	Add state update for script

-- Modifier: George Chernov
-- Create date: 19/12/2013
-- Description:	Update IsModerated flag to CandidateExamVersion
-- =============================================

CREATE PROCEDURE [dbo].[sm_SCRIPTREVIEW_submitModeratorResponse_sp]
	@candidateExamVersionID INT,
	@uniqueResponseId BIGINT,
	@userId INT,
	@mark DECIMAL(18 ,10),
	@comment NVARCHAR(MAX),
	@annotationData XML=NULL,
	@markedMetadataResponse XML=NULL
AS
BEGIN
    SET NOCOUNT ON;
    
    DECLARE @date DATETIME=GETDATE()
    DECLARE @stateID INT=3 -- marking complete, to send script to SA again
    BEGIN TRY
		BEGIN TRAN
    
			UPDATE dbo.Moderations
			SET    IsActual = 0
			WHERE  UniqueResponseId = @uniqueResponseId
				   AND CandidateExamVersionId = @candidateExamVersionID
    
			UPDATE dbo.CandidateExamVersions
			SET    ExamSessionState = @stateID
				  ,IsModerated = 1
			WHERE  ID = @candidateExamVersionID
    
    
			INSERT INTO dbo.Moderations
			  (
				[UniqueResponseId]
			   ,[CandidateExamVersionId]
			   ,[UserId]
			   ,[Timestamp]
			   ,[Mark]
			   ,[Comment]
			   ,[AnnotationData]
			   ,[MarkedMetadataResponse]
			   ,[IsActual]
			  )
			VALUES
			  (
				@uniqueResponseId
			   ,@candidateExamVersionID
			   ,@userId
			   ,@date
			   ,@mark
			   ,@comment
			   ,@annotationData
			   ,@markedMetadataResponse
			   ,1
			  )
    
			INSERT INTO CandidateExamVersionStatuses
			  (
				-- ID -- this column value is auto-generated
				[Timestamp]
			   ,StateID
			   ,CandidateExamVersionID
			  )
			VALUES
			  (
				@date
			   ,@stateID
			   ,@candidateExamVersionID
			  )
      
		COMMIT TRAN			
	END TRY
    BEGIN CATCH
        IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
        DECLARE @myErrorNum INT = ERROR_NUMBER()
        DECLARE @myMessage NVARCHAR(MAX) = ERROR_MESSAGE()
        DECLARE @myFullMessage NVARCHAR(MAX) = CONVERT(NVARCHAR(10), @myErrorNum)
            + ':' + @myMessage
        RAISERROR (@myFullMessage, 16, 1)
    END CATCH	      
END
GO
/****** Object:  StoredProcedure [dbo].[sm_SECUREMARKERINTEGRATIONSERVICE_DeleteUser_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Dave Dixon
-- Create date: 19/05/2011
-- Description:	sproc to delete a passed in user and their associated userroles
-- Editor:		Vitaly Bevzik
-- Create date: 6/4/2013
-- Description:	AssignedMarks changed to AssignedGroupMarks
-- Editor:		Anton Burdyugov
-- Create date: 3/7/2013
-- Description:	Move to Soft delete
-- =============================================
CREATE PROCEDURE [dbo].[sm_SECUREMARKERINTEGRATIONSERVICE_DeleteUser_sp]
	@username NVARCHAR(50)
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;
    
    DECLARE @userId INT
    
    SELECT @userId = [ID]
    FROM   [Users]
    WHERE  [Username] = @username
    
    IF (@userId IS NOT NULL)
    BEGIN
        UPDATE dbo.Users
        SET    Retired                = 1
              ,RetiredUsername        = Username
              ,Username               = SUBSTRING(CONVERT(NVARCHAR(50) ,NEWID()) ,0 ,9)
        WHERE  Id                     = @userID
               AND [InternalUser]     = 0 -- to prevent internal user's being deleted via integration!
    END
END
GO
/****** Object:  StoredProcedure [dbo].[sm_SECUREMARKERINTEGRATIONSERVICE_Get_MarkingProgress_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================    
-- Author:  Umair Rafique
-- Create date: 12/07/2011    
-- Description: Gets marking progress
-- Editor:  Anton Burdyugov
-- Create date: 05/06/2013    
-- Description: Make work with group data.
-- Editor:  Anton Burdyugov
-- Create date: 03/07/2013    
-- Description: Ignoring deleted users added
-- =============================================
CREATE PROC [dbo].[sm_SECUREMARKERINTEGRATIONSERVICE_Get_MarkingProgress_sp]
    @ExternalExamReference NVARCHAR(100) ,
    @StartDate DATETIME ,
    @EndDate DATETIME ,
    @PageSize INT = 10 ,
    @PageNo INT = 1
AS 
    BEGIN
	
        DECLARE @PageIndex INT		
		DECLARE @ItemCount INT
		DECLARE @PageCount INT
		DECLARE @ItemStartID INT
		DECLARE @ExamVersionID INT
		DECLARE @ItemsIDToFilterTable TABLE ( ItemId INT )		
		DECLARE @GroupsIDsToFilterTable TABLE ( GroupId INT )		
		DECLARE @ItemsIDTable TABLE ( ID INT )
		
		IF @PageNo > 0 
			SET @PageIndex = @PageNo - 1 -- 0-1 base 
		ELSE 
			SET @PageIndex = 0
			
		SELECT  @ExamVersionID = ID
		FROM    dbo.ExamVersions ev
		WHERE   ev.ExternalExamReference = @ExternalExamReference

		-- all active groups which have assigned mark and was marked in specified interval and assigned to @ExamVersionID
		INSERT  INTO @GroupsIDsToFilterTable 
				SELECT  DISTINCT ugr.GroupDefinitionID
				FROM    dbo.UniqueGroupResponses ugr INNER JOIN 
				        dbo.AssignedGroupMarks agm ON agm.UniqueGroupResponseId = ugr.Id INNER JOIN 
						dbo.CandidateGroupResponses cgr ON cgr.UniqueGroupResponseId = ugr.Id INNER JOIN 
						dbo.CandidateExamVersions cev ON cev.ID = cgr.CandidateExamVersionID INNER JOIN 
						dbo.GroupDefinitionStructureCrossRef gdscr ON gdscr.GroupDefinitionID = ugr.GroupDefinitionID
																  AND gdscr.Status = 0 -- search only active groups(not deleted)
				WHERE       agm.Timestamp BETWEEN @StartDate AND @EndDate 
				        AND cev.ExamVersionID = @ExamVersionID

        -- all items set to be marked in some group. Groups are the same as in @GroupsIDsToFilterTable
		INSERT  INTO @ItemsIDToFilterTable 
				SELECT  DISTINCT gdi.ItemID
				FROM    dbo.GroupDefinitionItems gdi INNER JOIN 
				        @GroupsIDsToFilterTable gitft ON gitft.GroupId = gdi.GroupID
				WHERE   gdi.ViewOnly = 0 -- only to be marked items

		-- total count of unique items in groups filtered in @ItemsIDToFilterTable.
        SELECT  @ItemCount = COUNT(*) FROM @ItemsIDToFilterTable				

		SELECT  @PageCount = CEILING(CAST(@ItemCount AS FLOAT) / @PageSize)

		SELECT  @ItemStartID = ( SELECT TOP 1
										I.ID
								 FROM   dbo.Items I
								 WHERE I.ID IN ( SELECT  ItemId FROM    @ItemsIDToFilterTable )
										AND I.ID NOT IN (
											SELECT TOP ( @PageSize * @PageIndex )
													I1.ID
											FROM    dbo.Items I1
											WHERE I1.ID IN ( SELECT ItemId FROM @ItemsIDToFilterTable ) )
							   )

		INSERT  INTO @ItemsIDTable
				SELECT  TOP(@PageSize) I.ID
				FROM    dbo.Items I
				WHERE I.ID >= @ItemStartID
					AND I.ID IN (SELECT ItemId FROM @ItemsIDToFilterTable)


		SELECT  @PageNo AS '@pageNo' ,
				@PageSize AS '@pageSize' ,
				@ItemCount AS '@totalItems' ,
				@PageCount AS '@totalPages' ,
				( SELECT    qf.name AS qualificationName ,
							qf.reference AS qualificationRef ,
							( SELECT    ex1.Name AS examName ,
										ex1.Reference AS examRef ,
										( SELECT    exv1.Name AS examVersionName ,
													exv1.ExternalExamReference AS examVersionRef ,
													( SELECT    i.ID AS itemId ,
																i.ExternalItemID AS itemRef ,
																i.TotalMark AS totalMark ,
																gd.ID,
																( SELECT DISTINCT
																	  u.UserName AS userName ,
																	  qm.AssignedQuota AS quota ,
																	  qm.NumberMarked AS numberMarked ,
																	  ( SELECT COUNT(DISTINCT agm.ID)
																		FROM dbo.AssignedGroupMarks agm INNER JOIN 
																		     dbo.UniqueGroupResponses ugr ON agm.UniqueGroupResponseId = ugr.Id INNER JOIN 
																			 dbo.CandidateGroupResponses cgr ON cgr.UniqueGroupResponseId = ugr.Id INNER JOIN 
																			 dbo.CandidateExamVersions cev ON cev.ID = cgr.CandidateExamVersionID
																		WHERE ugr.GroupDefinitionID = qm.GroupId
																		  AND agm.UserId = qm.UserID
																		  AND agm.MarkingMethodId IN ( 2, 3 )
																		  AND cev.ExamVersionID = ev.ID
																	  ) AS totalSeedsMarked ,
																	  ( SELECT COUNT(DISTINCT agm.ID)
																		FROM dbo.AssignedGroupMarks agm INNER JOIN
																		     dbo.UniqueGroupResponses ugr ON agm.UniqueGroupResponseId = ugr.Id INNER JOIN 
																			 dbo.CandidateGroupResponses cgr ON cgr.UniqueGroupResponseId = ugr.Id INNER JOIN 
																			 dbo.CandidateExamVersions cev ON cev.ID = cgr.CandidateExamVersionID
																		WHERE ugr.GroupDefinitionID = qm.GroupId
																		  AND agm.UserId = qm.UserID
																		  AND agm.MarkingMethodId IN ( 3 )
																		  AND cev.ExamVersionID = ev.ID
																	  ) AS competencySeedsMarked
																  FROM dbo.Users u INNER JOIN 
																       dbo.QuotaManagement qm ON u.ID = qm.UserID INNER JOIN 
																	   dbo.ExamVersions ev ON ev.ExamID = qm.ExamID
																  WHERE qm.GroupId = gd.Id
																    AND qm.AssignedQuota <> 0
																	AND ev.ID = exv.ID
																	AND u.Retired = 0
																FOR
																  XML PATH('examiner') ,
																	  TYPE ,
																	  ROOT('examiners')
																)
													  FROM      dbo.GroupDefinitions gd INNER JOIN 
													            dbo.GroupDefinitionItems gdi ON gdi.GroupID = gd.ID INNER JOIN 
																dbo.Items i ON gdi.ItemID = i.ID INNER JOIN 
																@ItemsIDTable iidt ON iidt.ID = gdi.ItemID INNER JOIN
																@GroupsIDsToFilterTable gidtft ON gidtft.GroupId = gd.ID
													  WHERE     gdi.ViewOnly = 0
													FOR
													  XML PATH('item') ,
														  TYPE ,
														  ROOT('items')
													)
										  FROM      dbo.ExamVersions exv1
										  WHERE     exv1.Id = exv.Id
										FOR
										  XML PATH('examVersion') ,
											  TYPE ,
											  ROOT('examVersions')
										)
							  FROM      dbo.Exams ex1
							  WHERE     ex1.QualificationID = qf.Id
							FOR
							  XML PATH('exam') ,
								  TYPE ,
								  ROOT('exams')
							)
				  FROM      dbo.Qualifications qf INNER JOIN 
				            dbo.Exams ex ON ex.QualificationID = qf.Id INNER JOIN 
							dbo.ExamVersions exv ON exv.ExamId = ex.Id
				  WHERE     exv.ExternalExamReference = @ExternalExamReference
				FOR
				  XML PATH('qualification') ,
					  TYPE ,
					  ROOT('qualifications')
				)
		FOR     XML PATH('return') ,
					TYPE ,
					ROOT('result')
		
    END


GO
/****** Object:  StoredProcedure [dbo].[sm_SECUREMARKERINTEGRATIONSERVICE_GetExamVersionIdFromRef_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Dave Dixon
-- Create date: 20/05/2011
-- Description:	sproc to get an exam version id from its ref
-- =============================================
CREATE PROCEDURE [dbo].[sm_SECUREMARKERINTEGRATIONSERVICE_GetExamVersionIdFromRef_sp]

		@examversionref nvarchar(50)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @examversionid	int
	
	SELECT @examversionid=[ID] FROM [dbo].[ExamVersions]
	WHERE [ExternalExamReference] = @examversionref
	
	SELECT @examversionid

END
GO
/****** Object:  StoredProcedure [dbo].[sm_SECUREMARKERINTEGRATIONSERVICE_GetRoleIdFromName_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Dave Dixon
-- Create date: 20/05/2011
-- Description:	sproc to get a role id from its name
-- =============================================
CREATE PROCEDURE [dbo].[sm_SECUREMARKERINTEGRATIONSERVICE_GetRoleIdFromName_sp]

		@rolename nchar(30)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @roleid	int
	
	SELECT @roleid=[ID] FROM [dbo].[Roles]
	WHERE [Name] = @rolename
	
	SELECT @roleid


END
GO
/****** Object:  StoredProcedure [dbo].[sm_SECUREMARKERINTEGRATIONSERVICE_SynchroniseUser_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Dave Dixon
-- Create date: 19/05/2011
-- Description:	sproc to insert/update a passed in user and their associated userroles
-- =============================================
CREATE PROCEDURE [dbo].[sm_SECUREMARKERINTEGRATIONSERVICE_SynchroniseUser_sp]

		@username nvarchar(50),
		@password nvarchar(100),
		@forename nvarchar(50),
		@surname nvarchar(50),
		@retired bit = 0,
		@email nvarchar(100),
		@phone nvarchar(20),
        @externalRef nvarchar(50),
        @expiryDate date,
        @assignedRoles nvarchar(max)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	DECLARE @userid		int
	DECLARE @count int 
	DECLARE @role nvarchar(max)
	DECLARE @examVersion nvarchar(max) 
	DECLARE @spot smallint 
  
	   -- Attempt UPDATE first
	UPDATE [dbo].[Users] 
	SET  [Password] = @password,
		 [Forename] = @forename,
		 [Surname] = @surname,
		 [Retired] = @retired,
		 [Email] = @email,
		 [Phone] = @phone,
		 [ExternalRef] = @externalRef,
		 [ExpiryDate] = @expiryDate
	WHERE [Username] = @username
	
	DECLARE @examId int
	-- Check to see if UPDATE happened, if it didn't then we need to INSERT
	IF @@ROWCOUNT=0
		BEGIN
			INSERT INTO [dbo].[Users]
				   ([Username]
				   ,[Password]
				   ,[Forename]
				   ,[Surname]
				   ,[Retired]
				   ,[Email]
				   ,[RequiresPasswordReset]
				   ,[Phone]
				   ,[ExternalRef]
				   ,[ExpiryDate])
			 VALUES
				   (@Username,
					@Password,
					@Forename,
					@Surname,
					@Retired,
					@Email,
					0, -- [RequiresPasswordReset]
					@Phone,
					@ExternalRef,
					@ExpiryDate)
		
			SET @userid = SCOPE_IDENTITY()
						
			WHILE @assignedRoles <> '' 
			BEGIN 			
				SET @spot = CHARINDEX('¬', @assignedRoles) 

				IF @spot > 0 
					BEGIN
						SET @role = CAST(LEFT(@assignedRoles, @spot-1) AS nvarchar)
						SET @assignedRoles = RIGHT(@assignedRoles, LEN(@assignedRoles)-@spot)
											
						SET @spot = CHARINDEX('¬', @assignedRoles)
						
						IF @spot > 0 
							BEGIN
								SET @examVersion = CAST(LEFT(@assignedRoles, @spot-1) AS nvarchar)
								SET @assignedRoles = RIGHT(@assignedRoles, LEN(@assignedRoles)-@spot)
							END
						ELSE
							BEGIN
								SET @examVersion = CAST(@assignedRoles AS nvarchar)
								SET @assignedRoles = ''
						END
					END
				ELSE
					BEGIN
						SET @role = CAST(@assignedRoles AS nvarchar)
						SET @assignedRoles = ''
					END
		        
		        SET @examId = (SELECT TOP 1 ExamId FROM [dbo].[ExamVersions] WHERE ID = @examVersion)
		        
				-- insert this assigned role in the AssignedUserRolesTable
				INSERT INTO [dbo].[AssignedUserRoles]
				SELECT @userid, @role, @examId
					
			END
		END			
	ELSE -- the UPDATE happened!
		BEGIN
			-- now we need to UPDATE roles
			SELECT @userid=[ID] FROM Users
			WHERE Username=@username
						
			DELETE FROM AssignedUserRoles
			WHERE UserID = @userid							
						
			WHILE @assignedRoles <> '' 
			BEGIN 			
				SET @spot = CHARINDEX('¬', @assignedRoles) 

				IF @spot > 0 
					BEGIN
						SET @role = CAST(LEFT(@assignedRoles, @spot-1) AS nvarchar)
						SET @assignedRoles = RIGHT(@assignedRoles, LEN(@assignedRoles)-@spot)
											
						SET @spot = CHARINDEX('¬', @assignedRoles)
						
						IF @spot > 0 
							BEGIN
								SET @examVersion = CAST(LEFT(@assignedRoles, @spot-1) AS nvarchar)
								SET @assignedRoles = RIGHT(@assignedRoles, LEN(@assignedRoles)-@spot)
							END
						ELSE
							BEGIN
								SET @examVersion = CAST(@assignedRoles AS nvarchar)
								SET @assignedRoles = ''
						END
					END
				ELSE
					BEGIN
						SET @role = CAST(@assignedRoles AS nvarchar)
						SET @assignedRoles = ''
					END
		        
		        SET @examId = (SELECT TOP 1 ExamId FROM [dbo].[ExamVersions] WHERE ID = @examVersion)
				-- insert this assigned role in the AssignedUserRolesTable
				INSERT INTO [dbo].[AssignedUserRoles]
				SELECT @userid, @role, @examId
							
			END
	END
END
GO
/****** Object:  StoredProcedure [dbo].[sm_SECUREMARKERINTEGRATIONSERVICE_SynchroniseUserInfo_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Dave Dixon
-- Create date: 19/05/2011
-- Description:	sproc to update a passed in user and their associated info (not roles)
-- =============================================
CREATE PROCEDURE [dbo].[sm_SECUREMARKERINTEGRATIONSERVICE_SynchroniseUserInfo_sp]

		@username nvarchar(50),
		@password nvarchar(100),
		@forename nvarchar(50),
		@surname nvarchar(50),
		@retired bit = 0,
		@email nvarchar(100),
		@phone nvarchar(20),
        @externalRef nvarchar(50),
        @expiryDate date
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	DECLARE @userid	int
	
	-- check to see if user exists
	SELECT @userid=[ID] FROM dbo.Users
	WHERE Username=@username
				
	IF @userid IS NOT NULL
		BEGIN  
		   UPDATE [dbo].[Users] 
		   SET  [Password] = @password,
				[Forename] = @forename,
				[Surname] = @surname,
				[Retired] = @retired,
				[Email] = @email,
				[Phone] = @phone,
				[ExternalRef] = @externalRef,
				[ExpiryDate] = @expiryDate
			WHERE [ID] = @userid
		END
	ELSE
		BEGIN
			INSERT INTO [dbo].[Users]
				   ([Username]
				   ,[Password]
				   ,[Forename]
				   ,[Surname]
				   ,[Retired]
				   ,[Email]
				   ,[RequiresPasswordReset]
				   ,[Phone]
				   ,[ExternalRef]
				   ,[ExpiryDate])
			 VALUES
				   (@username,
					@password,
					@forename,
					@surname,
					@retired,
					@email,
					0, -- [RequiresPasswordReset]
					@phone,
					@externalRef,
					@expiryDate)
			
				SET @userid = SCOPE_IDENTITY()
				
		END
		
END
GO
/****** Object:  StoredProcedure [dbo].[sm_SECUREMARKERINTEGRATIONSERVICE_SynchroniseUserRoles_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Dave Dixon
-- Create date: 19/05/2011
-- Description:	sproc to update the roles for the passed in user
-- =============================================
CREATE PROCEDURE [dbo].[sm_SECUREMARKERINTEGRATIONSERVICE_SynchroniseUserRoles_sp]

		@username nvarchar(50),
        @assignedRoles nvarchar(max)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	DECLARE @userid		int
	DECLARE @count int 
	DECLARE @role nvarchar(max)
	DECLARE @examVersion nvarchar(max) 
	DECLARE @spot smallint 

	-- now we need to UPDATE roles
	SELECT @userid=[ID] FROM Users
	WHERE Username=@username
				
	if @userid IS NULL
		SELECT -10
				
	DELETE FROM AssignedUserRoles
	WHERE UserID = @userid							
				
	WHILE @assignedRoles <> '' 
	BEGIN 			
		SET @spot = CHARINDEX('¬', @assignedRoles) 

		IF @spot > 0 
			BEGIN
				SET @role = CAST(LEFT(@assignedRoles, @spot-1) AS nvarchar)
				SET @assignedRoles = RIGHT(@assignedRoles, LEN(@assignedRoles)-@spot)
									
				SET @spot = CHARINDEX('¬', @assignedRoles)
				
				IF @spot > 0 
					BEGIN
						SET @examVersion = CAST(LEFT(@assignedRoles, @spot-1) AS nvarchar)
						SET @assignedRoles = RIGHT(@assignedRoles, LEN(@assignedRoles)-@spot)
					END
				ELSE
					BEGIN
						SET @examVersion = CAST(@assignedRoles AS nvarchar)
						SET @assignedRoles = ''
				END
			END
		ELSE
			BEGIN
				SET @role = CAST(@assignedRoles AS nvarchar)
				SET @assignedRoles = ''
			END
        
        DECLARE @examId INT = (SELECT TOP 1 ExamId FROM [dbo].[ExamVersions] WHERE ID = @examVersion)
		-- insert this assigned role in the AssignedUserRolesTable
		INSERT INTO [dbo].[AssignedUserRoles]
		SELECT @userid, @role, @examId
	END				
END
GO
/****** Object:  StoredProcedure [dbo].[sm_SHARED_GetExamById_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[sm_SHARED_GetExamById_sp]
	@examId INT
WITH RECOMPILE
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON; 
    
    SELECT e.ID    AS ExamID
          ,e.Name  AS ExamName
          ,e.Reference
          ,e.IsMarkOneItemPerScript
          ,e.IsFeedbackEnabled
          ,e.IsDecimalMarksEnabled
          ,q.ID    AS QualificationId
          ,q.Name  AS QualificationName
    FROM   dbo.Exams e
           INNER JOIN dbo.Qualifications q
                ON  q.ID = e.QualificationID
    WHERE  e.ID = @examId
END
GO
/****** Object:  StoredProcedure [dbo].[sm_SHARED_removeMarksFromMarkedMetadata_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Tom Gomersall
-- Create date: 03/08/2012
-- Description:	Removes the assigned mark values from the marked metadata and sets them back to zero.
-- =============================================
CREATE PROCEDURE [dbo].[sm_SHARED_removeMarksFromMarkedMetadata_sp]
(
	@uniqueResponseId	BIGINT
	,@updateTable		BIT
	,@newMarkedMetadata	XML	OUTPUT
)
AS
BEGIN
	-- Declare the return variable here
	DECLARE @myMarkedMetadata XML
	
	SELECT @myMarkedMetadata = markedMetadataResponse
	FROM dbo.UniqueResponses
	WHERE ID = @uniqueResponseId
	
	IF((@myMarkedMetadata IS NOT NULL) AND (LEN(CONVERT(NVARCHAR(MAX), @myMarkedMetadata)) > 0))
		BEGIN
			DECLARE @hdoc INT
			EXEC sp_xml_preparedocument @hdoc OUTPUT, @myMarkedMetadata

			SET @myMarkedMetadata = (
			SELECT (
				SELECT
					0					AS '@mark'
					,learningOutcome	AS '@learningOutcome'
					,maxMark			AS '@maxMark'
					,displayName		AS '@displayName'
				FROM(
					SELECT 
						learningOutcome
						,maxMark
						,displayName
					FROM
					OPENXML(@hdoc, '//mark',8)
					WITH(
						learningOutcome	INT				'@learningOutcome',
						maxMark			INT				'@maxMark',
						displayName		NVARCHAR(MAX)	'@displayName'
					)
				) AS mainSelect
				FOR XML PATH('mark'), TYPE
			)FOR XML PATH('item'))
			
			EXEC sp_xml_removedocument @hdoc
		END
	
	IF(@updateTable = 1)
		BEGIN
			UPDATE dbo.UniqueResponses
			SET markedMetadataResponse = @myMarkedMetadata
			WHERE ID = @uniqueResponseId
		END
	
	SET @newMarkedMetadata = @myMarkedMetadata
END
GO
/****** Object:  StoredProcedure [dbo].[sm_SHARED_removeMarksFromMarkedMetadataFromUGRLinks_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:  Anton Burdyugov
-- Create date: 20/05/2013
-- Description: Removes the assigned mark values from the marked metadata and sets them back to zero.

-- Modified:	George Chernov
-- Date:		23/04/2013
-- Description:	Remove uniqueResponseID param
-- =============================================
CREATE PROCEDURE [dbo].[sm_SHARED_removeMarksFromMarkedMetadataFromUGRLinks_sp]
(
 @uniqueGroupResponseId BIGINT
 ,@updateTable  BIT
)
AS
BEGIN

 DECLARE @myMarkedMetadatas TABLE (
			MarkedMetadata XML,
			UniqueResponseID INT)
			
 DECLARE @myMarkedMetadatasChanged TABLE (
			MarkedMetadata XML,
			UniqueResponseID INT)
			
 INSERT INTO @myMarkedMetadatas
					SELECT markedMetadataResponse, UniqueResponseId
					FROM UniqueGroupResponseLinks UGRL
					WHERE UGRL.UniqueGroupResponseID = @uniqueGroupResponseId
					
					
DECLARE @countOfRows INT
SET @countOfRows = (select COUNT(*) from @myMarkedMetadatas)

WHILE (@countOfRows > 0)
 BEGIN
 
	 DECLARE @myUniqueResponseID INT = (SELECT TOP 1 UniqueResponseID FROM @myMarkedMetadatas)
	 
	 DECLARE @myMarkedMetadata XML = (select MarkedMetadata from @myMarkedMetadatas where UniqueResponseID = @myUniqueResponseID)
 
	 IF((@myMarkedMetadata IS NOT NULL) AND (LEN(CONVERT(NVARCHAR(MAX), @myMarkedMetadata)) > 0))
	  BEGIN
	   DECLARE @hdoc INT
	   EXEC sp_xml_preparedocument @hdoc OUTPUT, @myMarkedMetadata

	   SET @myMarkedMetadata = (
	   SELECT (
		SELECT
		 0     AS '@mark'
		 ,learningOutcome AS '@learningOutcome'
		 ,maxMark   AS '@maxMark'
		 ,displayName  AS '@displayName'
		FROM(
		 SELECT 
		  learningOutcome
		  ,maxMark
		  ,displayName
		 FROM
		 OPENXML(@hdoc, '//mark',8)
		 WITH(
		  learningOutcome INT    '@learningOutcome',
		  maxMark   INT    '@maxMark',
		  displayName  NVARCHAR(MAX) '@displayName'
		 )
		) AS mainSelect
		FOR XML PATH('mark'), TYPE
	   )FOR XML PATH('item'))
	   
	   EXEC sp_xml_removedocument @hdoc
	  END
	  
	  INSERT INTO @myMarkedMetadatasChanged VALUES (@myMarkedMetadata, @myUniqueResponseID) 
	  
	  DELETE @myMarkedMetadatas WHERE UniqueResponseID = @myUniqueResponseID
	  
	  SET @countOfRows = (select COUNT(*) from @myMarkedMetadatas)
 END
 
 IF(@updateTable = 1)
	BEGIN
		UPDATE UGRL
		SET UGRL.markedMetadataResponse = N.MarkedMetadata,
			confirmedMark = NULL
		FROM UniqueGroupResponseLinks UGRL INNER JOIN @myMarkedMetadatasChanged N ON UGRL.UniqueResponseID = N.UniqueResponseID
		WHERE UGRL.UniqueGroupResponseID = @uniqueGroupResponseId
	END
	
 SELECT * from @myMarkedMetadatasChanged
END
GO
/****** Object:  StoredProcedure [dbo].[sm_STATEMANAGEMENT_GetDistinctItemsWithLatestMarkSchemeVersion_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =================================================================================
-- Author:		Dave Dixon
-- Create date: 24/06/2011
-- Description:	sproc to select all distict items in the SecureMarker database and their
--				associated mark scheme versions
-- =================================================================================
CREATE PROCEDURE [dbo].[sm_STATEMANAGEMENT_GetDistinctItemsWithLatestMarkSchemeVersion_sp]
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;
    
    SELECT DISTINCT 
           I.ExternalItemID              AS 'ExternalItemID'
          ,MS.IsMarkingCriteria          AS 'IsMarkingCriteria'
          ,ISNULL(MAX(MS.[version]) ,0)  AS 'latestMarkSchemeVersion'
    FROM   dbo.Items I
           LEFT OUTER JOIN dbo.MarkSchemes MS ON I.ExternalItemID = MS.ExternalItemID
    GROUP BY
           I.ExternalItemID
          ,MS.IsMarkingCriteria
END
GO
/****** Object:  StoredProcedure [dbo].[sm_STATEMANAGEMENT_GetMarkingCompleteExamSessionsForExport_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =================================================================================
-- Author:		Dave Dixon
-- Create date: 27/05/2011
-- Description:	sproc to select Candidate Exam Versions (sessions) to export to SecureAssess
--				as Result Pending (State 3 - Marking Complete)
-- Modified:	Tom Gomersall
-- Date:		04/09/2012
-- Description:	Added marked metadata response.

-- Modified:	Anton Burdyugov
-- Date:		01/10/2013
-- Description:	Moderator changes support added. Corrected marked metadata response determination.

-- Modified:	George Chernov
-- Date:		01/10/2013
-- Description:	Return 10 top scripts
-- =================================================================================
CREATE PROCEDURE [dbo].[sm_STATEMANAGEMENT_GetMarkingCompleteExamSessionsForExport_sp]
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
    SET NOCOUNT ON;
    
    DECLARE @TempTable TABLE
		(
			CEV_Id INT,
			CEV_ExternalSessionID INT,
			CEV_Keycode NVARCHAR(12),
			CEV_VIP BIT,
			CEV_DateSubmitted DATETIME
		)
		
	INSERT INTO @TempTable
	SELECT distinct TOP 10 
		CEV.ID,
		CEV.ExternalSessionID,
		CEV.Keycode,
		CEV.VIP,
		CEV.DateSubmitted
	FROM   dbo.CandidateExamVersions CEV
           INNER JOIN dbo.CandidateGroupResponses CGR ON  CGR.CandidateExamVersionID = CEV.ID          
           INNER JOIN dbo.UniqueGroupResponses UGR ON  UGR.ID = CGR.UniqueGroupResponseID
    WHERE  CEV.ExamSessionState = 3 
		   AND UGR.GroupDefinitionID IN 
			(
				SELECT  MAX(UGR2.GroupDefinitionID) 
				FROM ..CandidateGroupResponses CGR2
					INNER JOIN dbo.UniqueGroupResponses UGR2 ON UGR2.ID = CGR2.UniqueGroupResponseID
					INNER JOIN dbo.UniqueGroupResponseLinks UGRL2 ON UGRL2.UniqueGroupResponseID = UGR2.ID
					INNER JOIN dbo.GroupDefinitionStructureCrossRef GDSCR ON GDSCR.GroupDefinitionID = UGR2.GroupDefinitionID
					INNER JOIN dbo.ExamVersionStructures EVS ON  EVS.ID = GDSCR.ExamVersionStructureID				
				WHERE   CGR2.CandidateExamVersionID = CEV.ID
				    AND GDSCR.Status = 0 -- 0 = Active
					AND EVS.StatusID = 0 -- 0 = Released
				GROUP BY UGRL2.UniqueResponseId
			)	
    ORDER BY
           CEV.VIP DESC
          ,CEV.DateSubmitted ASC
    
	;WITH ExamsToExport AS 
	(
		SELECT TT.CEV_ID														AS 'ID'
			  ,TT.CEV_ExternalSessionID											AS 'ExternalSessionID'
			  ,TT.CEV_Keycode													AS 'Keycode'
			  ,TT.CEV_VIP													    AS 'VIP'
			  ,TT.CEV_DateSubmitted												AS 'DateSubmitted'
			  ,I.ExternalItemID													AS 'ExternalItemID'
			  ,MS.version														AS 'MarkSchemeVersion'      
			  ,ISNULL(M.Mark, AIM.AssignedMark)									AS 'confirmedMark'
			  ,ISNULL(M.markedMetadataResponse, AIM.markedMetadataResponse)		AS 'MarkedMetadataResponse'
			  ,ISNULL(M.Timestamp, ISNULL(AGM.timestamp, TT.CEV_DateSubmitted))	AS 'dateMarked'
			  ,ISNULL(U1.Username ,ISNULL(U.Username ,'unknown'))				AS 'Username'
			  ,(CASE 
					WHEN M.ID IS NOT NULL THEN ISNULL(M.AnnotationData, '')
					ELSE ISNULL(AIM.AnnotationData, '')
				END)															AS 'annotationData'
			  , (SELECT CAST(
					CASE WHEN EXISTS(
								SELECT * 
								  FROM dbo.UploadedFilesDetails ufd
							INNER JOIN dbo.UniqueResponseDocuments urd ON ufd.UniqueResponseDocumentID = urd.ID
								 WHERE urd.UniqueResponseID = UR.ID and ufd.UploadedByUserID IS NOT NULL) THEN 1 
						ELSE 0 
					END 
				AS BIT))														AS 'hasAdditionalFiles'

			  ,ROW_NUMBER() OVER (PARTITION BY TT.CEV_ExternalSessionID, I.ExternalItemID ORDER BY ISNULL(M.Timestamp, ISNULL(AGM.timestamp, TT.CEV_DateSubmitted	)) DESC) AS RowNumber
          
		FROM   @TempTable TT
			   INNER JOIN dbo.CandidateGroupResponses CGR ON  CGR.CandidateExamVersionID = TT.CEV_ID
			   ----------                    
			   INNER JOIN dbo.UniqueGroupResponses UGR ON  UGR.ID = CGR.UniqueGroupResponseID
			   INNER JOIN dbo.GroupDefinitionItems GDI ON  GDI.GroupID = UGR.GroupDefinitionID
						AND GDI.ViewOnly = 0 -- only for marking
			   ----------                        
			   INNER JOIN dbo.CandidateResponses CR ON  CR.CandidateExamVersionID = TT.CEV_ID
			   INNER JOIN dbo.UniqueResponses UR ON  UR.id = CR.UniqueResponseId AND UR.itemId = GDI.ItemID
			   INNER JOIN dbo.Items I ON  I.ID = UR.itemId
			   LEFT JOIN dbo.AssignedGroupMarks AGM ON  AGM.UniqueGroupResponseId = CGR.UniqueGroupResponseID AND AGM.IsConfirmedMark = 1
			   LEFT JOIN dbo.AssignedItemMarks AIM ON  AIM.UniqueResponseId = CR.UniqueResponseId AND AIM.GroupMarkId = AGM.ID           
			   ----------
			   LEFT JOIN dbo.Moderations M ON M.UniqueResponseId = UR.id AND M.CandidateExamVersionId = TT.CEV_ID AND M.IsActual = 1
			   ----------		                        
			   LEFT JOIN dbo.Users U ON  AGM.userId = U.id
			   LEFT JOIN dbo.Users U1 ON  U1.id = M.UserId
			   LEFT JOIN dbo.MarkSchemes MS ON  UR.markSchemeId = MS.id	                        
		WHERE  UGR.GroupDefinitionID IN 
				(
					SELECT  MAX(UGR2.GroupDefinitionID) 
					FROM ..CandidateGroupResponses CGR2
						INNER JOIN dbo.UniqueGroupResponses UGR2 ON UGR2.ID = CGR2.UniqueGroupResponseID
						INNER JOIN dbo.UniqueGroupResponseLinks UGRL2 ON UGRL2.UniqueGroupResponseID = UGR2.ID
						INNER JOIN dbo.GroupDefinitionStructureCrossRef GDSCR ON GDSCR.GroupDefinitionID = UGR2.GroupDefinitionID
						INNER JOIN dbo.ExamVersionStructures EVS ON  EVS.ID = GDSCR.ExamVersionStructureID				
					WHERE   CGR2.CandidateExamVersionID = TT.CEV_ID
						AND GDSCR.Status = 0 -- 0 = Active
						AND EVS.StatusID = 0 -- 0 = Released
					GROUP BY UGRL2.UniqueResponseId
				)
	)
	SELECT *
	FROM ExamsToExport
	WHERE RowNumber = 1
	ORDER BY 
			VIP DESC,
			ExternalSessionID
END

GO
/****** Object:  StoredProcedure [dbo].[sm_STATEMANAGEMENT_GetResultConfirmedExamSessions_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =================================================================================
-- Author:		Tom Gomersall
-- Create date: 22/08/2011
-- Description:	Gets the top 100 exams whose results have been confirmed in Script Review.
-- =================================================================================
CREATE PROCEDURE [dbo].[sm_STATEMANAGEMENT_GetResultConfirmedExamSessions_sp]

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	SELECT TOP 100 
	[CandidateExamVersions].ExternalSessionID
	FROM [CandidateExamVersions]
	WHERE	[CandidateExamVersions].ExamSessionState = 6
	ORDER BY [CandidateExamVersions].VIP DESC, [CandidateExamVersions].DateSubmitted ASC

END
GO
/****** Object:  StoredProcedure [dbo].[sm_STATEMANAGEMENT_SetResultsArchivedForExamSessions_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =================================================================================
-- Author:		Tom Gomersall
-- Create date: 23/08/2011
-- Description:	Sets the status of exams to archived when they have been confirmed in SecureAssess.
-- =================================================================================
CREATE PROCEDURE [dbo].[sm_STATEMANAGEMENT_SetResultsArchivedForExamSessions_sp]
	@externalIds NVARCHAR(MAX)
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;
    
    DECLARE @cevIDs TABLE([Timestamp] DATETIME ,StateID INT ,cevId INT)
    DECLARE @date DATETIME=GETDATE()
    
    INSERT INTO @cevIDs
    SELECT @date
          ,7
          ,cev.ID
    FROM   CandidateExamVersions cev
    WHERE  cev.ExternalSessionID IN (SELECT *
                                     FROM   dbo.ParamsToList(@externalIds))
    BEGIN TRY
		BEGIN TRAN
    
		UPDATE [CandidateExamVersions]
		SET    ExamSessionState = 7
		WHERE  ID IN (SELECT cevId FROM @cevIDs)
    
		INSERT INTO CandidateExamVersionStatuses
		  (
			-- ID -- this column value is auto-generated
			[Timestamp]
		   ,StateID
		   ,CandidateExamVersionID
		  )
		SELECT ci.[Timestamp], ci.StateID, ci.cevId FROM @cevIDs ci
    
		COMMIT TRAN			
	END TRY
    BEGIN CATCH
        IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
        DECLARE @myErrorNum INT = ERROR_NUMBER()
        DECLARE @myMessage NVARCHAR(MAX) = ERROR_MESSAGE()
        DECLARE @myFullMessage NVARCHAR(MAX) = CONVERT(NVARCHAR(10), @myErrorNum)
            + ':' + @myMessage
        RAISERROR (@myFullMessage, 16, 1)
    END CATCH
    
    SELECT TOP 100 
           [CandidateExamVersions].ExternalSessionID
    FROM   [CandidateExamVersions]
    WHERE  [CandidateExamVersions].ExamSessionState = 6
    ORDER BY
           [CandidateExamVersions].VIP DESC
          ,[CandidateExamVersions].DateSubmitted ASC
END
GO
/****** Object:  StoredProcedure [dbo].[sm_STATEMANAGEMENT_UpdateExamSessionStates_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sm_STATEMANAGEMENT_UpdateExamSessionStates_sp]
	@examSessionState INT
AS
BEGIN
	SET NOCOUNT ON;
		BEGIN TRY
		

		DECLARE @State5Thread TABLE (
			cevID int
		);

		IF @examSessionState = 5
		BEGIN
			BEGIN TRAN

				DECLARE @cevs TABLE (
					cevid int
				)

				INSERT INTO @cevs(cevId)	
				SELECT DISTINCT CEV.ID
				FROM dbo.CandidateExamVersions CEV WITH(NOLOCK)
					INNER JOIN dbo.CandidateGroupResponses CGR WITH(NOLOCK) ON CEV.ID = CGR.CandidateExamVersionID
					INNER JOIN dbo.AssignedGroupMarks AGM WITH(NOLOCK) ON AGM.UniqueGroupResponseId = CGR.UniqueGroupResponseID
				WHERE CEV.ExamSessionState = 4
					AND	AGM.isConfirmedMark = 1
					AND	AGM.[timestamp] > CEV.lastExportDate

				UPDATE dbo.CandidateExamVersions
					SET ExamSessionState = 5,
						[LastManagedDate] = GETDATE()
						OUTPUT Inserted.ID INTO @State5Thread
				WHERE ID IN (SELECT cevId FROM @cevs)
	
				DELETE @cevs
			COMMIT TRAN
		END

		DECLARE @examsToCheck TABLE ( 
			cevId INT
			, ExamSessionState INT
			, ActualMark INT
		);
				
		DECLARE @examsToUpdate TABLE(
				cevId INT
				,ExamSessionState INT
				,PercentageMarkingComplete INT
				,Mark DECIMAL(18 ,10)
		);

		INSERT INTO @examsToCheck(cevId, ExamSessionState, ActualMark)
		SELECT TOP 500 ID
			, ExamSessionState 
			, Mark 
		FROM CandidateExamVersions WITH(NOLOCK) 
		where ExamSessionState= @examSessionState
		ORDER BY LastManagedDate ASC;
								

		INSERT INTO @examsToUpdate(
			cevId
			,ExamSessionState
			,PercentageMarkingComplete
			,Mark	
		)
		SELECT B.ID
			, B.NewState
			, B.PercentageMarkingComplete
			, B.CurrentMark
		FROM (
			SELECT A.ID
					,  A.ExamSessionState
					,  A.ActualMark
					,	CASE 
						WHEN (PercentageMarkingComplete = 0 AND ExamSessionState NOT IN (4)) THEN 1
						WHEN (PercentageMarkingComplete = 0 AND ExamSessionState = 4) THEN 5
						WHEN (PercentageMarkingComplete > 0 AND PercentageMarkingComplete < 100 AND ExamSessionState NOT IN (4,5)) THEN 2
						WHEN (PercentageMarkingComplete > 0 AND PercentageMarkingComplete < 100 AND ExamSessionState = 4) THEN 5
						WHEN (PercentageMarkingComplete = 100 AND ExamSessionState != 4) THEN 3
						WHEN (PercentageMarkingComplete = 100 AND ExamSessionState = 4) THEN 4
						WHEN (PercentageMarkingComplete < 100 AND ExamSessionState = 5) THEN 2
					END AS 'NewState'
					, A.PercentageMarkingComplete
					, A.CurrentMark
			FROM (
				SELECT CEV.cevId AS 'ID'
					,ExamSessionState	AS 'ExamSessionState'
					,ActualMark
					,COUNT(NULLIF(UGR.confirmedMark, NULL))*100/COUNT(UGR.Id) AS 'PercentageMarkingComplete'
					,ISNULL(SUM(ISNULL(m.[Mark], UGRL.confirmedMark)), 0) as 'CurrentMark'
				FROM @examsToCheck CEV
					INNER JOIN dbo.CandidateGroupResponses CGR WITH(NOLOCK) ON CGR.CandidateExamVersionID = CEV.cevId
					INNER JOIN dbo.UniqueGroupResponses UGR WITH(NOLOCK) on UGR.id = CGR.UniqueGroupResponseID							
					INNER JOIN dbo.ActiveGroups_view ACT WITH(NOLOCK) ON  ACT.GroupDefinitionID = UGR.GroupDefinitionID
					INNER JOIN dbo.UniqueGroupResponseLinks UGRL WITH(NOLOCK) ON  UGRL.UniqueGroupResponseId = UGR.ID
					INNER JOIN [dbo].[CandidateExamVersionGroups_view] CEVG WITH(NOLOCK) ON CEVG.CandidateExamVersionID = CEV.cevId AND CEVG.GroupDefinitionID = UGR.GroupDefinitionID
					LEFT JOIN dbo.Moderations M WITH(NOLOCK) ON  M.UniqueResponseId = UGRL.UniqueResponseId AND M.CandidateExamVersionID = CEV.cevId AND m.IsActual = 1
				
				GROUP BY CEV.cevId, CEV.ExamSessionState, CEV.ActualMark
				) A
			) B
		WHERE (ExamSessionState != NewState or ActualMark != CurrentMark) and ExamSessionState IS NOT NULL;  --Only get exams that need to change.

		BEGIN TRAN

			UPDATE CandidateExamVersions
			SET [LastManagedDate] = GETDATE()
			FROM CandidateExamVersions
			INNER JOIN @examsToCheck AS cevs ON cevs.cevId = CandidateExamVersions.ID;

			--This only fires when a change has been made--

			UPDATE CandidateExamVersions
			SET
				ExamSessionState = cevs.ExamSessionState, 		
				PercentageMarkingComplete = cevs.PercentageMarkingComplete,
				[Mark] = cevs.[Mark]--,
				--[LastManagedDate] = GETDATE()  Not needed?
			FROM CandidateExamVersions 
			INNER JOIN @examsToUpdate AS cevs ON cevs.cevId = CandidateExamVersions.ID;
				
			--Record all required auditing--

			INSERT CandidateExamVersionStatuses([Timestamp],StateID,CandidateExamVersionID)
			SELECT GETDATE(), cev.ExamSessionState, cevs.cevId
			FROM @examsToCheck AS cevs
			INNER JOIN @examsToUpdate cev on cev.cevId = cevs.cevId
			WHERE cevs.ExamSessionState != cev.ExamSessionState
			UNION
			SELECT GETDATE(), 5, cevId
			FROM @State5Thread;

			SELECT @@ROWCOUNT
		
		COMMIT TRAN
		
	END TRY
    BEGIN CATCH
        IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
        DECLARE @myErrorNum INT = ERROR_NUMBER()
        DECLARE @myMessage NVARCHAR(MAX) = ERROR_MESSAGE()
        DECLARE @myFullMessage NVARCHAR(MAX) = CONVERT(NVARCHAR(10), @myErrorNum) + ':' + @myMessage
        RAISERROR (@myFullMessage, 16, 1)
    END CATCH
END

GO
/****** Object:  StoredProcedure [dbo].[sm_STATEMANAGEMENT_UpdateMarkSchemes_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sm_STATEMANAGEMENT_UpdateMarkSchemes_sp]
	@ExternalItemID NCHAR(20),
	@version INT,
	@markSchemeBlOb VARBINARY(MAX)=NULL,
	@IsMarkingCriteria BIT,
	@externalUrl NVARCHAR(MAX)=NULL,
	@extension NVARCHAR(20) = NULL
AS
BEGIN
    UPDATE dbo.MarkSchemes
    SET    version               = @version
          ,markSchemeBlOb        = @markSchemeBlOb
          ,IsMarkingCriteria     = @IsMarkingCriteria
          ,externalUrl           = @externalUrl
		  ,Extension			 = @extension
    WHERE  ExternalItemID        = @ExternalItemID
END

GO
/****** Object:  StoredProcedure [dbo].[sm_STATEMANAGEMENT_UpdateSuccessfullyExportedExamSessionState_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================
-- Author:		Dave Dixon
-- Create date: 31/05/2011
-- Description:	sproc to update a state 3 Candidate Exam Version (session) to state 4.
--				This sproc will only be called if the exam session has successfully exported
--				back to SecureAssess.
-- =================================================================================

CREATE PROCEDURE [dbo].[sm_STATEMANAGEMENT_UpdateSuccessfullyExportedExamSessionState_sp]
	@keycode NVARCHAR(12),
	@newState int = 4
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;
    
    DECLARE @cevID INT=(
                SELECT TOP 1 cev.ID
                FROM   CandidateExamVersions cev
                WHERE  cev.Keycode = @keycode
                       AND cev.ExamSessionState = 3
            )
    BEGIN TRY
		IF @cevID IS NOT NULL
			BEGIN
				DECLARE @lastExportDate DATETIME=GETDATE() 
        
				BEGIN TRAN
        
					INSERT INTO CandidateExamVersionStatuses
					  (
						-- ID -- this column value is auto-generated
						[Timestamp]
					   ,StateID
					   ,CandidateExamVersionID
					  )
					VALUES
					  (
						@lastExportDate
					   ,@newState
					   ,@cevID
					  )
        
					UPDATE CandidateExamVersions
					SET    ExamSessionState = @newState
						  ,lastExportDate = @lastExportDate
					WHERE  ID = @cevID  
			
					SELECT @@ROWCOUNT
				
				COMMIT TRAN	
			END
		ELSE
			BEGIN
				SELECT 0
			END
    END TRY
        BEGIN CATCH
            IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
            DECLARE @myErrorNum INT = ERROR_NUMBER()
            DECLARE @myMessage NVARCHAR(MAX) = ERROR_MESSAGE()
            DECLARE @myFullMessage NVARCHAR(MAX) = CONVERT(NVARCHAR(10), @myErrorNum)
                + ':' + @myMessage
            RAISERROR (@myFullMessage, 16, 1)
        END CATCH
END
GO
/****** Object:  StoredProcedure [dbo].[sm_TOKENSTORE_getUser_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Tom Gomersall
-- Create date: 03/02/11
-- Description:	Deletes any expired tokens and retrieves the serialised user from the token store.
-- Modified:	Tom Gomersall
-- Date:		28/09/2012
-- Description:	Added table variable to ensure that the same tokens are used when setting to NULL in UniqueResponses and deleting.
-- Modified:	Vitaly Bevzik
-- Date:		24/04/2013
-- Description:	Delete token from UniqueGroupResponses instead of UniqueResponses
-- =============================================
CREATE PROCEDURE [dbo].[sm_TOKENSTORE_getUser_sp]
	-- Add the parameters for the stored procedure here
	 @token				nchar(30),
	 @tokenExpiryTime	int

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	DECLARE @myExpiredTokens TABLE
	(
		ID	INT
	)
	
	INSERT INTO @myExpiredTokens
	SELECT ID
	FROM TokenStore
	WHERE DATEDIFF(minute, lastInteractionDateTime, GETDATE()) > @tokenExpiryTime
	
	--Sets the token ID to null for any unique responses that are checked out to expired tokens.
	UPDATE UniqueGroupResponses
	SET tokenId = NULL
	WHERE tokenId IS NOT NULL AND tokenId IN  (SELECT ID FROM @myExpiredTokens)
		
	-- Delete any that have expired
	DELETE FROM TokenStore
	WHERE ID IN  (SELECT ID FROM @myExpiredTokens)
	
	UPDATE TokenStore
	SET lastInteractionDateTime = GETDATE()
	WHERE token = @token
	
	SELECT [ID], serialisedUser
	FROM TokenStore
	WHERE token = @token
END
GO
/****** Object:  StoredProcedure [dbo].[sm_TOKENSTORE_storeToken_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Tom Gomersall
-- Create date: 03/02/11
-- Description:	Adds the token to the token store table
-- Modified:	Tom Gomersall
-- Date:		13/09/2012
-- Description:	Added token deletion to delete any tokens that have exceeded their lifespan.
-- Modified:	Tom Gomersall
-- Date:		28/09/2012
-- Description:	Added update to the UniqueResponse table to set the token back to NULL if the token is being deleted.
-- Modified:	Vitaly Bevzik
-- Date:		28/06/2013
-- Description: Make work with group level data
-- Modified:	Anton Burdyugov
-- Date:		10/07/2013
-- Description: Deleteion only top 10 expiried token to prevent long login
-- =============================================
CREATE PROCEDURE [dbo].[sm_TOKENSTORE_storeToken_sp]
	-- Add the parameters for the stored procedure here
	 @userId		int,
	 @token			nchar(30),
	 @tokenImage	image,
	 @tokenExpiryTime	int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	UPDATE TokenStore
	SET serialisedUser = @tokenImage,
		lastInteractionDateTime = GETDATE()
	WHERE userID = @userID AND token = @token
	
	IF(@@ROWCOUNT = 0)
		BEGIN
			INSERT INTO TokenStore
			(
				[userId]
				,[token]
				,[lastInteractionDateTime]
				,[serialisedUser]
			)
			VALUES
			(
				@userId
				,@token
				,GETDATE()
				,@tokenImage
			)
		END
	
	DECLARE @myExpiredTokens TABLE
	(
		ID	INT
	)
	
	INSERT INTO @myExpiredTokens
	SELECT ID
	FROM TokenStore
	WHERE DATEDIFF(minute, lastInteractionDateTime, GETDATE()) > @tokenExpiryTime
	
	--Sets the token ID to null for any unique responses that are checked out to expired tokens.
	UPDATE UniqueGroupResponses
	SET tokenId = NULL
	WHERE tokenId IS NOT NULL AND tokenId IN  (SELECT ID FROM @myExpiredTokens)
		
	-- Delete top 10 that have expired
	DELETE FROM TokenStore
	WHERE ID IN  (SELECT TOP 10 ID FROM @myExpiredTokens)

END
GO
/****** Object:  StoredProcedure [dbo].[sm_TOKENSTORE_updateToken_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Tom Gomersall
-- Create date: 02/06/11
-- Description:	Updates the token for a logged in user.
-- =============================================
CREATE PROCEDURE [dbo].[sm_TOKENSTORE_updateToken_sp]
	-- Add the parameters for the stored procedure here
	 @tokenStoreID	int,
	 @tokenImage	image
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	UPDATE TokenStore
	SET serialisedUser = @tokenImage,
		lastInteractionDateTime = GETDATE()
	WHERE ID = @tokenStoreID

END
GO
/****** Object:  StoredProcedure [dbo].[sm_USERMANAGEMENTSERVICE_DeleteUser_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =======================================================  
-- Author:  Umair Rafique
-- Create date: 25/05/2011  
-- Description: To delete(retire) a User
-- Modifier: Anton Burdyugov
-- Create date: 26/06/2013  
-- Description: Add assigned group mark update
-- Modifier: Anton Burdyugov
-- Create date: 03/07/2013  
-- Description: Move to soft delete
-- ========================================================  
CREATE PROCEDURE [dbo].[sm_USERMANAGEMENTSERVICE_DeleteUser_sp]
	@userID INT
AS
BEGIN
    UPDATE dbo.Users
    SET    Retired             = 1
          ,RetiredUsername     = Username
          ,Username            = SUBSTRING(CONVERT(NVARCHAR(50) ,NEWID()) ,0 ,9)
    WHERE  Id                  = @userID
END
GO
/****** Object:  StoredProcedure [dbo].[sm_USERMANAGEMENTSERVICE_GetAllGrantableRoleNamesWithRoleId_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Rathin Sundar
-- Create date: 28/04/2011
-- Description:	Gets all the roles that can be assigend by a user. 
-- =============================================
CREATE PROCEDURE [dbo].[sm_USERMANAGEMENTSERVICE_GetAllGrantableRoleNamesWithRoleId_sp]
	-- Add the parameters for the stored procedure here
	@userId	INT

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT 
		[ID],
		[Name],
		[AssignableToGlobal] 
	FROM [dbo].[Roles]
	WHERE ID IN(
		SELECT GrantableRoles.GrantableRoleId
		FROM AssignedUserRoles
		INNER JOIN GrantableRoles
		ON GrantableRoles.RoleId = AssignedUserRoles.RoleId
		WHERE AssignedUserRoles.UserId = @userId
	)
	ORDER BY Name
	
END
GO
/****** Object:  StoredProcedure [dbo].[sm_USERMANAGEMENTSERVICE_GetGlobalRoles_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:		Tom Gomersall  
-- Create date: 11/07/2011 
-- Description: Gets all the roles that can be assigned to the global exam version.
-- Modified:	Tom Gomersall
-- Date:		04/11/2011
-- Description:	Added roles that have been assigned to the global exam version, because the role may have been set to be assignable to global, assigned
--				to the global exam version, and then set not to be assignable to global.
-- =============================================  
CREATE PROCEDURE [dbo].[sm_USERMANAGEMENTSERVICE_GetGlobalRoles_sp]  
 -- Add the parameters for the stored procedure here    
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
	SET NOCOUNT ON;    
	
	SELECT
		ID
		,Name
	FROM Roles
	WHERE 
		AssignableToGlobal = 1
		OR
		EXISTS(
			SELECT dbo.AssignedUserRoles.RoleID
			FROM dbo.AssignedUserRoles
			WHERE 
				dbo.AssignedUserRoles.RoleID = dbo.Roles.ID
				AND
				dbo.AssignedUserRoles.ExamID = 0
		)
	
END
GO
/****** Object:  StoredProcedure [dbo].[sm_USERMANAGEMENTSERVICE_GetUserNames_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:		Tom Gomersall  
-- Create date: 11/07/2011 
-- Description: Gets all the user names for the users.  
-- Editor:		Anton Burdyugov
-- Create date: 03/07/2013
-- Description: Ignoring deleted users added
-- =============================================  
CREATE PROCEDURE [dbo].[sm_USERMANAGEMENTSERVICE_GetUserNames_sp] 
-- Add the parameters for the stored procedure here
	@userId INT
	 ,
	@maxNum INT
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.  
    SET NOCOUNT ON;    
    
    SET ROWCOUNT @maxNum
    
    SELECT ID
          ,Forename+' '+Surname AS FullName
    FROM   Users
    WHERE  InternalUser = 0
           AND ID<>@userId
           AND  Retired = 0
    
    SET ROWCOUNT 0
END
GO
/****** Object:  StoredProcedure [dbo].[sm_USERMANAGEMENTSERVICE_ResetPasswordForUser_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  Rathin Sundar  
-- Create date: 12/04/2011  
-- Description: sproc to update a user and their roles   
-- =============================================  
CREATE PROCEDURE [dbo].[sm_USERMANAGEMENTSERVICE_ResetPasswordForUser_sp]  
 -- Add the parameters for the stored procedure here  
   @UserID int,  
   @RequiresPasswordReset BIT,
   @Password NVARCHAR(100)  
AS  
BEGIN  
	 UPDATE [dbo].[Users]  
	 SET 
		[RequiresPasswordReset] = @RequiresPasswordReset,
		[Password] = @Password
	WHERE [ID] = @UserID      
END
GO
/****** Object:  StoredProcedure [dbo].[sm_WELCOMESERVICE_getGroupsToMark_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Tom Gomersall
-- Create date: 23/11/2012
-- Description:	Get the details for the items for which a user has an unlimited quota assigned.
-- Modified:	Vitaly Bevzik
-- Date:		22/03/2013
-- Description:	Added 'Suspended' field. Removed items which was not released to mark
-- Modified:	Anton Burdyugov
-- Date:		14/05/2013
-- Description:	Make procedure work with groups instead items
-- =============================================

CREATE PROCEDURE [dbo].[sm_WELCOMESERVICE_getGroupsToMark_sp]
	@userId INT ,
	@maxNumItems INT,
	@tokenStoreID INT,
	@defaultMarkingRestriction BIT
WITH RECOMPILE
AS
BEGIN
    SET NOCOUNT ON;
    
    IF OBJECT_ID('tempdb..#groupAggregatedSource') IS NOT NULL
		DROP TABLE #groupAggregatedSource
    CREATE TABLE #groupAggregatedSource (GroupId INT NOT NULL
		, GroupName nvarchar(4000)
		, ExamID INT NOT NULL
		, EarliestDate DATETIME
		, NumResponses INT
		, VIP FLOAT
		, Suspended INT
		, AffectedUniqueResponses INT
		, ResponsesDif INT)
	DECLARE @script NVARCHAR(200) = 'ALTER TABLE #groupAggregatedSource
		ADD CONSTRAINT GroupAggregatedSource_PK' + CAST(@@SPID as NVARCHAR(10)) + ' PRIMARY KEY (GroupId, ExamID)'
	EXEC(@script)
	INSERT INTO #groupAggregatedSource
		SELECT GroupId
				, GroupName
				, ExamID
				, EarliestDate
				, NumResponses
				, VIP
				, Suspended
				, AffectedUniqueResponses
				, ResponsesDif
			FROM dbo.getGroupsWithAffectedResponsesForHome_fn(@userId, @tokenStoreID)
    
    SELECT TOP(@maxNumItems)
           Q.Name		AS QualificationName
          ,Q.ID			AS QualificationId
          ,EWR.Name		AS ExamName
          ,EWR.ExamId	AS ExamID
		  ,EVNC.VersionNames AS ExamVersionName
          ,T.GroupId                  AS GroupId
          ,T.GroupName                AS GroupName
          ,(
               -- if the group has the Restriction we have to recalculate the number of responses to mark
               CASE (EWR.IsMarkingRestrictionEnableForExam)
                    WHEN 1 THEN (T.ResponsesDif)
                    ELSE T.NumResponses
               END
           )                          AS ResponsesToMark
          ,CONVERT(BIT ,T.Suspended)  AS Suspended
          ,VIP
          ,EarliestDate
    FROM   #groupAggregatedSource AS T
           INNER JOIN dbo.getExamsWithRestrictions_fn(@userId, @defaultMarkingRestriction) EWR ON  EWR.ExamID = T.ExamID
           INNER JOIN dbo.Qualifications Q ON  EWR.QualificationID = Q.ID
           INNER JOIN ExamVersionNamesConcatenated_view EVNC ON EVNC.ExamId = T.ExamID AND EVNC.GroupId = T.GroupId
    WHERE  -- we have to get only groups, which contains more responses then affected responses if Exam Version of the Group has the Restriction
           (
               T.ResponsesDif > 0
               AND EWR.IsMarkingRestrictionEnableForExam=1
           )
           OR -- or get all groups from Exam Versions without the Restriction
              EWR.IsMarkingRestrictionEnableForExam = 0
    ORDER BY T.VIP DESC,
			T.EarliestDate
			
	IF OBJECT_ID('tempdb..#groupAggregatedSource') IS NOT NULL
		DROP TABLE #groupAggregatedSource
END
GO
/****** Object:  StoredProcedure [dbo].[sm_WELCOMESERVICE_getLatestWelcomeMessage_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sm_WELCOMESERVICE_getLatestWelcomeMessage_sp] 
	@welcomeMessage nvarchar(max) output

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT TOP 1 @welcomeMessage = welcomeText 
	FROM [dbo].[WelcomeMessageTable]
	ORDER BY modifiedDate DESC
END
GO
/****** Object:  StoredProcedure [dbo].[sm_WELCOMESERVICE_ModifyWelcomeScreenMessage_sp]    Script Date: 30/08/2017 08:26:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sm_WELCOMESERVICE_ModifyWelcomeScreenMessage_sp] 
	@modifiedBy int,
	@WelcomeText nvarchar(max)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    DECLARE @errorReturnString 	nvarchar(max)
	DECLARE @errorNum			nvarchar(100)
	DECLARE @errorMess			nvarchar(max)
	DECLARE @ReturnVal			nvarchar(max)	
	
	
	BEGIN TRY
		INSERT INTO [dbo].[WelcomeMessageTable]
           ([welcomeText]
           ,[modifiedBy]
           ,[modifiedDate])
		 VALUES
			   (@WelcomeText
			   ,@modifiedBy
			   ,GETDATE());
		IF( (SELECT COUNT(*) FROM [dbo].[WelcomeMessageTable] WHERE WelcomeText = @WelcomeText) > 0)
			BEGIN
				SELECT @ReturnVal = 'true';
			END
		ELSE
			BEGIN
				SELECT @ReturnVal = 'false';
			END	

	END TRY
				BEGIN CATCH
					SET @errorNum = (SELECT ERROR_NUMBER() AS ErrorNumber)
					SET @errorMess = (SELECT ERROR_MESSAGE() AS ErrorMessage)
					SET @errorReturnString = '<r errorCode="2">SQL Error, Number: ' + @errorNum + ' MESSAGE: ' + @errorMess + '</r>'					
				END	CATCH
	SELECT  @ReturnVal
END
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Holds the CP page XML and the related images, audio etc.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Items', @level2type=N'COLUMN',@level2name=N'ExternalContent'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[41] 4[20] 2[31] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "AGM"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 125
               Right = 246
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "U"
            Begin Extent = 
               Top = 126
               Left = 38
               Bottom = 245
               Right = 243
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "UGR"
            Begin Extent = 
               Top = 6
               Left = 284
               Bottom = 125
               Right = 459
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "GD"
            Begin Extent = 
               Top = 246
               Left = 38
               Bottom = 365
               Right = 387
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 17
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         S' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'MarkedCIGroups_view'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'ortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'MarkedCIGroups_view'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'MarkedCIGroups_view'
GO
USE [master]
GO
ALTER DATABASE [2.10.0.15_SecureMarker] SET  READ_WRITE 
GO
