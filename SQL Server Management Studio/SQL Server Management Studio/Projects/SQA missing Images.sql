SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

--PRV_SQA_ItemBank
--PRV_SQA_CPProjectAdmin
	
SELECT b.name as 'Project Name'
	, SUBSTRING(A.ID, 0,  CHARINDEX('P', A.ID)) as ProjectID
	, SUBSTRING(A.ID, CHARINDEX('P', A.ID) + 1, CHARINDEX('P', A.ID)) 'Page Number'
	, a.ver 'Page Version'
FROM
(
      Select i.id, i.ver, i.moD, it.aLI
      from PRV_SQA_CPProjectAdmin.dbo.ItemGraphicTable it 
            INNER JOIN ComponentTable c ON c.ID = it.ParentID 
            INNER JOIN SceneTable s ON s.ID = c.ParentID 
            INNER JOIN PageTable i ON i.id = S.ParentID
      UNION
      Select i.id, i.ver, i.moD, it.aLI  
      from PRV_SQA_CPProjectAdmin.dbo.ItemVideoTable it 
            INNER JOIN ComponentTable c ON c.ID = it.ParentID 
            INNER JOIN SceneTable s ON s.ID = c.ParentID 
            INNER JOIN PageTable i ON i.id = S.ParentID
      UNION
      Select i.id, i.ver, i.moD, it.aLI  
            from PRV_SQA_CPProjectAdmin.dbo.ItemHotSpotTable it 
            INNER JOIN ComponentTable c ON c.ID = it.ParentID 
            INNER JOIN SceneTable s ON s.ID = c.ParentID 
            INNER JOIN PageTable i ON i.id = S.ParentID
      UNION
      Select i.id, i.ver, i.moD, it.aLI 
      from PRV_SQA_CPProjectAdmin.dbo.ItemCustomQuestionTable it 
            INNER JOIN ComponentTable c ON c.ID = it.ParentID 
            INNER JOIN SceneTable s ON s.ID = c.ParentID 
            INNER JOIN PageTable i ON i.id = S.ParentID
) A

INNER JOIN PRV_SQA_CPProjectAdmin.dbo.ProjectListTable B
on SUBSTRING(A.ID, 0,CHARINDEX('P', A.ID)) = B.ID

LEFT JOIN PRV_SQA_CPProjectAdmin.dbo.ProjectManifestTable PMT
on PMT.ProjectId = B.ID
	and a.ali in (cast(pmt.id as nvarchar(10)), SUBSTRING(pmt.name, 0, CHARINDEX('.',pmt.name)))

INNER JOIN PRV_SQA_CPProjectAdmin.dbo.PageTable PT
ON PT.id = SUBSTRING(A.ID, 0,  CHARINDEX('P', A.ID)) + 'P' + SUBSTRING(A.ID, CHARINDEX('P', A.ID) + 1, CHARINDEX('P', A.ID)) 

INNER JOIN PRV_SQA_ItemBank.dbo.ItemTable IB
on IB.ProjectID = SUBSTRING(A.ID, 0,  CHARINDEX('P', A.ID)) 
	and IB.ItemRef = SUBSTRING(A.ID, CHARINDEX('P', A.ID) + 1, CHARINDEX('P', A.ID))

where 
	PMT.ID IS NULL
	and a.ali !=''
	and a.ali !='placeholder'
	and pt.ver = a.ver
