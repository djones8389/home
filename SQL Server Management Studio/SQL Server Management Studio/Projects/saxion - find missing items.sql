SELECT ID [AssessmentID]
	, REPLACE(REPLACE(cast(a.b.query('.') as NVARCHAR(MAX)), '<XPath>@ID="',''), '"</XPath>','') ItemID
INTO [ExamsBeingReferenced]
FROM AssessmentTable AT WITH (NOLOCK)
		
CROSS APPLY AssessmentRules.nodes('PaperRules//XPath') a(b)

where REPLACE(REPLACE(cast(a.b.query('.') as NVARCHAR(MAX)), '<XPath>@ID="',''), '"</XPath>','') not like '<XPath%'
	and IsValid = 0;

select id, assessmentrules
from AssessmentTable
where ExternalReference = 'AGZ_FYS_44028_1.1_1718'

SELECT *
FROM [ExamsBeingReferenced]
LEFT JOIN (
	select distinct cast(projectid as nvarchar(10))+ 'P' + cast(ItemRef as nvarchar(10)) [ItemID]
	from ItemTable  WITH (NOLOCK) 
) ItemTable
ON ItemTable.ItemID = [ExamsBeingReferenced].ItemID
	WHERE ItemTable.ItemID  IS NULL




select distinct cast(projectid as nvarchar(10))+ 'P' + cast(ItemRef as nvarchar(10))
from ItemTable  WITH (NOLOCK)
where cast(projectid as nvarchar(10))+ 'P' + cast(ItemRef as nvarchar(10))  in (

	SELECT REPLACE(REPLACE(cast(a.b.query('.') as NVARCHAR(MAX)), '<XPath>@ID="',''), '"</XPath>','') ItemID
	FROM AssessmentTable AT WITH (NOLOCK)
		
	CROSS APPLY AssessmentRules.nodes('PaperRules//XPath') a(b)

	where REPLACE(REPLACE(cast(a.b.query('.') as NVARCHAR(MAX)), '<XPath>@ID="',''), '"</XPath>','') not like '<XPath%'
		
) order by 1

--select top 10 cast(projectid as nvarchar(10))+ 'P' + cast(ItemRef as nvarchar(10))
--from ItemTable
