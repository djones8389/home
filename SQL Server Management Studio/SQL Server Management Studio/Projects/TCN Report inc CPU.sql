WITH CTE AS 
(
	SELECT ROW_NUMBER() OVER(PARTITION BY [Computer Name] ORDER BY WarehouseTime DESC) AS [N], *
	FROM (
		SELECT E.clientInformation.value('(/clientInformation/systemConfiguration[1]/identification/computerName)[1]', 'nvarchar(max)') AS [Computer Name]
			,E.WarehouseTime
			,E.clientInformation.value('(/clientInformation/systemConfiguration[1]/environment/operatingSystem/version)[1]', 'nvarchar(max)') AS [Operating System]
			,E.clientInformation.value('(/clientInformation/systemConfiguration[1]/environment/operatingSystem/servicePack)[1]', 'nvarchar(max)') AS [Service Pack]
			,SUBSTRING((
					SELECT N', '
						,net.ver.value('.', 'nvarchar(max)')
					FROM E.clientInformation.nodes('/clientInformation/systemConfiguration/dotNet[1]/version') net(ver)
					FOR XML PATH('')
					), 3, 1000) AS [.NET Framework(s)]
			,E.clientInformation.value('(/clientInformation/systemConfiguration[1]/flashPlayer/name)[1]', 'nvarchar(max)') AS [Flash Player]
			,E.clientInformation.value('(/clientInformation/systemConfiguration[1]/flashPlayer/version)[1]', 'nvarchar(max)') AS [Flash Player Version]
			,SUBSTRING((
					SELECT N', '
						,(disc.drive.query('.').value('(drive/name)[1]', 'nvarchar(max)')) + N' ' + (disc.drive.query('.').value('(drive/totalSize)[1]', 'nvarchar(max)')) + N' (' + (disc.drive.query('.').value('(drive/freeSpace)[1]', 'nvarchar(max)')) + N' Free)'
					FROM E.clientInformation.nodes('/clientInformation/systemConfiguration[1]/environment/hardDisk/drives/drive') disc(drive)
					FOR XML PATH('')
					), 3, 1000) AS [Hard Disk(s)]
			,E.clientInformation.value('(/clientInformation/systemConfiguration[1]/environment/memory/installedPhysicalMemory)[1]', 'nvarchar(max)') AS [Memory]

			,E.clientInformation.value('(/clientInformation/systemConfiguration[1]/environment/processor/name)[1]', 'nvarchar(max)') AS [CPU]

			,E.clientInformation.value('(/clientInformation/systemConfiguration[1]/environment/video/resolution)[1]', 'nvarchar(max)') AS [Display Resolution]
			,CASE E.clientInformation.exist('/clientInformation/systemConfiguration[1]/environment/soundCard/name')
				WHEN 1
					THEN N'Yes'
				ELSE N'No'
				END AS [Audio Available]
			,E.clientInformation.value('(/clientInformation/systemConfiguration[1]/identification/macAddress)[1]', 'nvarchar(max)') AS [MAC Address]
			,E.clientInformation.value('(/clientInformation/systemConfiguration[1]/identification/ipAddress)[1]', 'nvarchar(max)') AS [IP Address]
			,E.KeyCode
		FROM dbo.WAREHOUSE_ExamSessionTable E WITH (NOLOCK)
		INNER JOIN dbo.WAREHOUSE_ExamSessionTable_Shreded S WITH (NOLOCK) ON E.ID = S.examSessionId
		WHERE ClientInformation IS NOT NULL
		AND CentreID = @CentreID) X
)
SELECT [Computer Name]
	,[Operating System]
	,[Service Pack]
	,[.NET Framework(s)]
	,[Flash Player]
	,[Flash Player Version]
	,[Hard Disk(s)]
	,Memory
	,CPU
	,[Display Resolution]
	,[Audio Available]
	,[MAC Address]
	,[IP Address]
	,KeyCode
	,WarehouseTime
FROM CTE
WHERE N = 1


/*
select count(examsessionid),centreID
from WAREHOUSE_ExamSessionTable_Shreded
group by centreID
order by 1 desc


select top 10 clientInformation
from WAREHOUSE_ExamSessionTable
where clientInformation is not null
*/