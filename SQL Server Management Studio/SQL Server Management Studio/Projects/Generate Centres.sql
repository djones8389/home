IF OBJECT_ID('tempdb..##NewCentres') IS NOT NULL DROP TABLE ##NewCentres;

CREATE TABLE ##NewCentres (
CentreID INT  
);

declare @i int = 0
declare @max int = 133;

while (@i < @max)

BEGIN

DECLARE @Tsql nvarchar(MAX)=''

select @Tsql +=CHAR(13) + '

INSERT [CentreTable] ([CentreName],[CentreCode],[AddressLine1],[County],[Country],[PrimaryContactID],[TechnicalContactID],[Retired],[version],[offlineEnabled])
VALUES(''RickTest'+cast(@i as char(3))+''', ''12345'', ''123 fake street'', 0,0,1,1,0,1,0)

INSERT ##NewCentres
select SCOPE_IDENTITY()
'
EXEC(@Tsql)

SELECT @i = @i + 1
END


INSERT CentreQualificationsTable(CentreID, QualificationID)
select CentreID
	, IB3QualificationLookup.ID
from ##NewCentres
cross apply IB3QualificationLookup;