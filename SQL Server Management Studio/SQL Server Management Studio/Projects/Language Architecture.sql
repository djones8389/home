CREATE TABLE ExamSessionTable (
	[ID] INT NOT NULL,
	[Keycode] nvarchar(10) NOT NULL,
	[StructureXML] XML NOT NULL
CONSTRAINT [PK_ExamSessionTable] PRIMARY KEY CLUSTERED (
      [ID] ASC
	)
);

CREATE TABLE [dbo].[ExamSessionLanguageLookupTable](
      [ID] [tinyint] NOT NULL,
      [LanguageCode] [nvarchar](20) NOT NULL
CONSTRAINT [PK_ExamSessionItemsLanguageVariantTable] PRIMARY KEY CLUSTERED (
      [ID] ASC
	)
);

CREATE TABLE [dbo].[ExamSessionItemsTable] (
      [LanguageID] [tinyint] NOT NULL,
      [ItemID] [nvarchar](50) NOT NULL,
      [Version] [smallint] NOT NULL
CONSTRAINT [PK_ExamSessionItemsTable] PRIMARY KEY CLUSTERED (
      [LanguageID] ASC
 )
);


