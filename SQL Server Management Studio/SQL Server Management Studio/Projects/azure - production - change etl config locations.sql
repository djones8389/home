use msdb

DECLARE @NewStart nvarchar(200) = 'F:\S_Drive\Surpass\AnalyticsManagement_12.12.60.15\SurpassDataWarehouse\Bin\Config\';

select 'update sysjobsteps set command = ''' + replace(command, [Start],@NewStart) + ''' where job_id = '''+cast(job_id as nvarchar(MAX))+''' and step_id = '+cast(step_id as varchar(10))+''
from (
	select c.*	
		, SUBSTRING(k, 0, charindex(reverse(substring(REVERSE(k), 0, charindex('\',REVERSE(k)))),K)) [Start]
		, reverse(substring(REVERSE(k), 0, charindex('\',REVERSE(k)))) [End]
	from (
		select 
			a.name
			, b.step_id
			, b.job_id
			, b.command
			, substring(substring(b.command, CHARINDEX('CONFIGFILE',b.command)+12, 150), 0, CHARINDEX('"',substring(b.command, CHARINDEX('CONFIGFILE',b.command)+12, 150))) K
		from sysjobs a
		inner join sysjobsteps b
		on a.job_id = b.job_id
		where name like '%etl%'
	) c
) d

