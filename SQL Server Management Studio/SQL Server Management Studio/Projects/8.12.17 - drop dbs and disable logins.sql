sp_whoisactive

use master

select 'kill ' + convert(varchar(5),spid)
	,db_name(dbid)
from sys.sysprocesses s
where dbid > 4
	--and  db_name(dbid) = 'SaxionDefect_ItemBank'
order by 2; 

SELECT 'ALTER DATABASE ' + QUOTENAME(Name) + ' SET OFFLINE;'
from sys.databases
where database_id > 4
	and name like '%AQA%'
	--and state_desc = 'Offline';

select 'ALTER LOGIN ' + QUOTENAME(NAME) + ' DISABLE;'
from sys.syslogins
WHERE name like '%AQA%'

order by Name


--ALTER DATABASE [SANDBOX_STG_AQA_ContentProducer] SET OFFLINE;
--ALTER DATABASE [STG_AQA_ContentProducer] SET OFFLINE;
--ALTER DATABASE [STG_AQA_ItemBank] SET OFFLINE;
--ALTER DATABASE [STG_AQA_SecureAssess] SET OFFLINE;
--ALTER DATABASE [STG_AQA_SurpassDataWarehouse] SET OFFLINE;

--ALTER LOGIN [STG_AQA_ContentProducer_Login] DISABLE;
--ALTER LOGIN [STG_AQA_ItemBank_Login] DISABLE;
--ALTER LOGIN [STG_AQA_SecureAssess_Login] DISABLE;

SELECT name, type_desc, is_disabled 
FROM sys.server_principals
WHERE is_disabled = 0
	AND type_desc = 'SQL_LOGIN'
ORDER BY 1;