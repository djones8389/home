select *
from DimCandidate
where candidateref like 'abj/%'




--11.9

use PRV_BritishCouncil_SecureAssess

WITH cte AS (
	SELECT
		ROW_NUMBER() OVER(PARTITION BY UserId ORDER BY ID DESC) N
		,WUT.UserId CandidateKey
		,WUT.[ID] LatestWHCandidateKey
		,WUT.[version] [Version]
		,WUT.[Forename]
		,WUT.[Surname]
		,WUT.[EthnicOrigin] EthnicOrigin
		,WUT.[DOB]
		,WUT.[CandidateRef]
		,CONVERT(nvarchar(10),'') ULN
		,WUT.[Middlename]
		,WUT.[Gender]
		,CONVERT(NVARCHAR(MAX),WUT.[SpecialRequirements]) SpecialRequirements
		,WUT.[AddressLine1]
		,WUT.[AddressLine2]
		,WUT.[Town]
		,WUT.[County]
		,WUT.[Country]
		,WUT.[PostCode]
		,WUT.[Telephone]
		,CONVERT(NVARCHAR(100),WUT.[Email]) Email
		,CONVERT(BIT, 0) IsExaminer
		,UserName
	FROM PRV_BritishCouncil_SecureAssess.[dbo].[WAREHOUSE_UserTable] WUT
		where id in (
	
	SELECT sa.id
	FROM PRV_BritishCouncil_SurpassDataWarehouse..DimCandidate SDW
	INNER JOIN PRV_BritishCouncil_SecureAssess..WAREHOUSE_UserTable SA
	on Sa.UserId = sdw.CandidateKey
	where sa.CandidateRef <> sdw.CandidateRef
	)
)
SELECT CandidateKey
	,  LatestWHCandidateKey
	, [version]
	, Forename
	, Surname
	, EthnicOrigin
	, DOB
	, CandidateRef
	, ULN	
	, Middlename
	, Gender
	, SpecialRequirements
	, AddressLine1
	, AddressLine2
	,Town
	, County
	, Country
	,PostCode
	, Telephone
	,Email
	, IsExaminer
	into #temp
	FROM cte WHERE N=1


begin tran

select *
from PRV_BritishCouncil_SurpassDataWarehouse.dbo.DimCandidate
where  candidateKey in (select candidateKey from #temp)


MERGE PRV_BritishCouncil_SurpassDataWarehouse.dbo.DimCandidate Trg
USING  #temp Src
on src.candidateKey = trg.candidatekey
when matched then 
	UPDATE
		SET 

	CandidateKey		=	src.CandidateKey
	,LatestWHCandidateKey		=	 src.LatestWHCandidateKey
	,[version]		=	 src.[version]
	,Forename		=	src.Forename
	,Surname		=	src.Surname
	--,EthnicOriginKey		=	 src.EthnicOrigin
	,DOB		=	 src.DOB
	,CandidateRef		=	 src.CandidateRef
	,ULN		=	 src.ULN
	,Middlename		=	 src.Middlename
	,Gender		=	 src.Gender
	,SpecialRequirements		=	 src.SpecialRequirements
	,AddressLine1		=	 src.AddressLine1
	,AddressLine2		=	 src.AddressLine2
	,Town		=	src.Town
	--,CountyKey		=	 src.County
	--,CountryKey		=	 src.Country
	,PostCode		=	src.PostCode
	,Telephone		=	 src.Telephone
	,Email		=	src.Email
	,IsExaminer		=	 src.IsExaminer;

select *
from PRV_BritishCouncil_SurpassDataWarehouse.dbo.DimCandidate
where  candidateKey in (select candidateKey from #temp)


rollback


