--and ExamSessionKey in (2479838,2428778)

declare @StartDate datetime = '2017-01-01'
declare @EndDate datetime = '2017-04-01'

declare @examStatesTable table (
	ID tinyint
)

INSERT @examStatesTable
SELECT 6
UNION
SELECT 9

DECLARE @ColBuilder nvarchar(MAX) = (SUBSTRING(
	(
	select ',', QUOTENAME(cast(ID as tinyint))
	from @examStatesTable
	for xml path ('')
	), 2,100)
	)

DECLARE @ExamStateList nvarchar(MAX) = SUBSTRING((

	SELECT ',', cast(ID as tinyint)
	FROM @examStatesTable
	for xml path('')
), 2,1000)

if OBJECT_ID('tempdb..##sdw') is not null drop table ##sdw;

DECLARE @DynamicString nvarchar(MAX) = ''
SELECT @DynamicString = '

SELECT *
into ##sdw
FROM (		
	SELECT A.ExamSessionKey
		, ExamStateId
		, StateChangeDate
	FROM (

	SELECT ROW_NUMBER() OVER (PARTITION BY ExamsessionKey, examStateID ORDER BY seqno) R
		, *
	FROM [430327-AAT-SQL2\SQL2].[AAT_SurpassDataWarehouse].[dbo].[FactExamSessionsStateAudit]
		where ExamStateID IN ('+@ExamStateList+')
		and ExamSessionKey in (2479838,2428778)
		CAST(StateChangeDate AS DATE) BETWEEN CAST('+@StartDate=' AS DATE) AND CAST('+@EndDate+' AS DATE)
		) A 
		where R = 1
) AS [S]
PIVOT (
MAX(StateChangeDate)
FOR examStateid IN ('+@ColBuilder+')
) AS [P]

'

exec(@DynamicString)

CREATE CLUSTERED INDEX [IX_ExamSessionKey] ON ##sdw (ExamSessionKey);

select *
from ##sdw






/*

select Exams.ExamSessionKey
	FROM (
		SELECT ExamSessionKey
			,min(StateChangeDate) StateChangeDate
		FROM [430327-AAT-SQL2\SQL2].[AAT_SurpassDataWarehouse].[dbo].[FactExamSessionsStateAudit]
			where ExamStateID IN ((SELECT ID FROM @examStatesTable))
			group by ExamSessionKey
	) Exams
	where CAST(StateChangeDate AS DATE) BETWEEN CAST(@StartDate AS DATE) AND CAST(@EndDate AS DATE)
		and Exams.ExamSessionKey in (2479838,2428778)

*/
