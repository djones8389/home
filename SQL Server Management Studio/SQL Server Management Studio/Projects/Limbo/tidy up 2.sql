use TCNTechnicalAudit_SecureAssess

SELECT EST.ID
	, EST.KeyCode
	, EST.examState
	, UT.Forename
	, UT.Surname
	, CT.CentreName
	, IB.QualificationName
	, SCET.examName
	, scet.id [schedule]
	, esirt.*  
  FROM ExamSessionTable as EST (NOLOCK)

  Inner Join ScheduledExamsTable as SCET (NOLOCK)  on SCET.ID = EST.ScheduledExamID   
  Inner Join UserTable as UT (NOLOCK)  on UT.ID = EST.UserID
  Inner Join CentreTable as CT (NOLOCK)  on CT.ID = SCET.CentreID
  Inner Join IB3QualificationLookup as IB (NOLOCK) on IB.ID = scet.qualificationId

	left join ExamSessionItemResponseTable esirt (NOLOCK) on esirt.ExamSessionID = EST.id
--left join ExamSessionDocumentTable esdt	(NOLOCK) on esdt.ExamSessionID = EST.id

where  examState = 99

select id, scheduledexamid, examstate
from examsessiontable
where scheduledexamid in (882)
order by scheduledexamid

delete from examsessiontable where id in (6079)
delete from ScheduledExamsTable where id in (244,176,245)
