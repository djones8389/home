CREATE TABLE ##ExamsToVoid (
	 ID INT
	, Thread tinyint NULL 
)

INSERT ##ExamsToVoid
SELECT EST.ID, NULL--, AwaitingUploadStateChangingTime, attemptAutoSubmitForAwaitingUpload, AwaitingUploadGracePeriodInDays
FROM ExamSessionTable as EST (NOLOCK)
 
  Inner join ExamStateChangeAuditTable as ESCAT (NOLOCK)
  on ESCAT.ExamSessionID = est.ID
	and NewState = examState
     
where examState = 18
	and StateChangeDate < DATEADD(Month, -6, GETDATE())
		ORDER BY EST.ID;
		
CREATE INDEX [IX_Temp] ON [##ExamsToVoid] (
	 ID
	, Thread
)

DECLARE @rows INT
DECLARE @ids TABLE (id INT)

BEGIN TRANSACTION

        UPDATE TOP (500) ##ExamsToVoid
        SET thread = 1
        OUTPUT deleted.id
        INTO @ids
        WHERE thread IS NULL

        SET @rows = @@ROWCOUNT

COMMIT TRANSACTION


WHILE @rows > 0
BEGIN
        BEGIN TRANSACTION

		UPDATE ExamSessionTable
		SET attemptAutoSubmitForAwaitingUpload = 0
		WHERE ID IN (
						SELECT ID
						FROM @ids
						)

        COMMIT TRANSACTION

        BEGIN TRANSACTION

            UPDATE TOP (500) ##ExamsToVoid
            SET thread = 1
            OUTPUT deleted.id
            INTO @ids
            WHERE thread IS NULL

            SET @rows = @@ROWCOUNT

        COMMIT TRANSACTION
END