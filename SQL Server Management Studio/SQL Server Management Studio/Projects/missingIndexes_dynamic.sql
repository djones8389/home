use master

IF OBJECT_ID('tempdb..#MissingIndexes') IS NOT NULL DROP TABLE #MissingIndexes;

CREATE TABLE #MissingIndexes (
	[ID] uniqueIdentifier,
	[Instance] [nvarchar](500),
	[database_name] sysname,
	[ImprovementMeasure] [float] NULL,
	[Statement] [nvarchar](max) NULL,
	[equality_columns] [nvarchar](max) NULL,
	[inequality_columns] [nvarchar](max) NULL,
	[included_columns] [nvarchar](max) NULL
	)

DECLARE @dynamic nvarchar(MAX)='';

select @dynamic +=CHAR(13)+ '
use '+QUOTENAME([NAME])+'


BEGIN
	SELECT 
		NewID()
		, @@SERVERNAME
		, DB_NAME() 
		, A.[Improvement Measure]	
		,A.TableName
		, REPLACE(A.Main, ''_'','','') [Equality Columns]
		, ISNULL([Include], '''') [Inequality Columns]
		, A.[Create Statement]
	FROM (
		SELECT TOP 10 
			dm_migs.avg_total_user_cost * (dm_migs.avg_user_impact / 100.0) * (dm_migs.user_seeks + dm_migs.user_scans) AS [Improvement Measure]
			, quotename(OBJECT_NAME(dm_mid.OBJECT_ID, dm_mid.database_id))  AS [TableName]
				,'''' + REPLACE(REPLACE(REPLACE(ISNULL(dm_mid.equality_columns, ''''), '', '', ''_''), ''['', ''''), '']'', '''') + CASE 
				WHEN dm_mid.equality_columns IS NOT NULL
					AND dm_mid.inequality_columns IS NOT NULL
					THEN ''_''
				ELSE ''''
				END + REPLACE(REPLACE(REPLACE(ISNULL(dm_mid.inequality_columns, ''''), '', '', ''_''), ''['', ''''), '']'', '''') [Main]
			,(''('' + dm_mid.included_columns + '')'') [Include]
			,''CREATE INDEX [IX_'' + OBJECT_NAME(dm_mid.OBJECT_ID, dm_mid.database_id) + ''_'' + REPLACE(REPLACE(REPLACE(ISNULL(dm_mid.equality_columns, ''''), '', '', ''_''), ''['', ''''), '']'', '''') + CASE 
				WHEN dm_mid.equality_columns IS NOT NULL
					AND dm_mid.inequality_columns IS NOT NULL
					THEN ''_''
				ELSE ''''
				END + REPLACE(REPLACE(REPLACE(ISNULL(dm_mid.inequality_columns, ''''), '', '', ''_''), ''['', ''''), '']'', '''') + '']'' + '' ON '' + dm_mid.statement + '' ('' + ISNULL(dm_mid.equality_columns, '''') + CASE 
				WHEN dm_mid.equality_columns IS NOT NULL
					AND dm_mid.inequality_columns IS NOT NULL
					THEN '',''
				ELSE ''''
				END + ISNULL(dm_mid.inequality_columns, '''') + '')'' + ISNULL('' INCLUDE ('' + dm_mid.included_columns + '')'', '''') AS [Create Statement]

		FROM sys.dm_db_missing_index_groups dm_mig
		INNER JOIN sys.dm_db_missing_index_group_stats dm_migs ON dm_migs.group_handle = dm_mig.index_group_handle
		INNER JOIN sys.dm_db_missing_index_details dm_mid ON dm_mig.index_handle = dm_mid.index_handle
		WHERE dm_mid.database_ID = DB_ID()
		ORDER BY dm_migs.avg_total_user_cost * dm_migs.avg_user_impact * (dm_migs.user_seeks + dm_migs.user_scans) DESC
	) A

END
'
from sys.databases
where state_desc = 'ONLINE'
	AND database_id > 4

INSERT #MissingIndexes
exec(@dynamic);


SELECT *
FROM #MissingIndexes