--ALTER LOGIN Mary5 ENABLE;

USE MASTER

select 'ALTER LOGIN ' + QUOTENAME(NAME) + ' DISABLE;'
from sys.sql_logins 
where name like '%.%'
	order by 1

ALTER LOGIN [Andy.Walker] ENABLE;
ALTER LOGIN [Artem.Fedorov] ENABLE;
ALTER LOGIN [James.Cook] ENABLE;
ALTER LOGIN [Khanh.Tu] ENABLE;
ALTER LOGIN [Martin.Bialachowski] ENABLE;
ALTER LOGIN [Matt.Harris] ENABLE;
ALTER LOGIN [Nabil.Yousfi] ENABLE;
ALTER LOGIN [Peter.Hughes] ENABLE;
ALTER LOGIN [Robert.Barnes] ENABLE;
ALTER LOGIN [Sandeep.Modaboyina] ENABLE;
ALTER LOGIN [Shirley.Pinto] ENABLE;
ALTER LOGIN [Tiberiu.Bucovei] ENABLE;
ALTER LOGIN [Tom.Hatton] ENABLE;
ALTER LOGIN [Umair.Rafique] ENABLE;
ALTER LOGIN [Viet.Tran] ENABLE;
ALTER LOGIN [Wayne.Crossley] ENABLE;


ALTER LOGIN [Andy.Walker] DISABLE;
ALTER LOGIN [Artem.Fedorov] DISABLE;
ALTER LOGIN [James.Cook] DISABLE;
ALTER LOGIN [Khanh.Tu] DISABLE;
ALTER LOGIN [Martin.Bialachowski] DISABLE;
ALTER LOGIN [Matt.Harris] DISABLE;
ALTER LOGIN [Nabil.Yousfi] DISABLE;
ALTER LOGIN [Peter.Hughes] DISABLE;
ALTER LOGIN [Robert.Barnes] DISABLE;
ALTER LOGIN [Sandeep.Modaboyina] DISABLE;
ALTER LOGIN [Shirley.Pinto] DISABLE;
ALTER LOGIN [Tiberiu.Bucovei] DISABLE;
ALTER LOGIN [Tom.Hatton] DISABLE;
ALTER LOGIN [Umair.Rafique] DISABLE;
ALTER LOGIN [Viet.Tran] DISABLE;
ALTER LOGIN [Wayne.Crossley] DISABLE;
