IF OBJECT_ID('tempdb..##HumanMarkedItems') IS NOT NULL DROP TABLE ##CandidateCount;

CREATE TABLE ##CandidateCount (
	client nvarchar(1000)
	, CandidateCount int
);

DECLARE @dynamic nvarchar(MAX)='';

select @dynamic +=CHAR(13)+ '
use '+QUOTENAME([NAME])+'

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

INSERT ##CandidateCount
select db_name()
	, count(ID)
from usertable (NOLOCK)
where Retired = 0		
	and IsCandidate = 1
	and AccountExpiryDate > getDATE()  
'
from sys.databases
where state_desc = 'ONLINE'
	and name like '%[_]SecureAssess';

exec(@dynamic);


select *
from ##CandidateCount
order by 1