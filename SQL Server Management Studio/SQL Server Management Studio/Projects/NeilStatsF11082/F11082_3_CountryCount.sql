IF OBJECT_ID('tempdb..##CentreCount') IS NOT NULL DROP TABLE ##CentreCount;

CREATE TABLE ##CentreCount (
	client nvarchar(1000)
	, CentreCount int
);

DECLARE @dynamic nvarchar(MAX)='';

select @dynamic +=CHAR(13)+ '
use '+QUOTENAME([NAME])+'

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

INSERT ##CentreCount
SELECT db_name()
	, count(centrekey)	
FROM '+QUOTENAME(NAME)+'.dbo.DimCentres  DC
'
from sys.databases
where state_desc = 'ONLINE'
	and name like '%[_]SurpassDataWarehouse';

exec(@dynamic);


select *
from ##CentreCount
order by 1