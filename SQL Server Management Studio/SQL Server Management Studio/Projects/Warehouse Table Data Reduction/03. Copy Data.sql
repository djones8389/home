SET IDENTITY_INSERT [temp_ScheduledPackageCandidateExamItems]  ON 

INSERT [temp_ScheduledPackageCandidateExamItems](ScheduledPackageCandidateExamItemId, ExternalItemId, CandidateExam_ScheduledPackageCandidateExamId, OriginalMark, Mark, OriginalTotalMark, TotalMark, CreateDate)
SELECT SPCEI.* 
FROM [ScheduledPackageCandidateExamItems] SPCEI
INNER JOIN ScheduledPackageCandidateExams SPCE
on SPCE.ScheduledPackageCandidateExamId = SPCEI.CandidateExam_ScheduledPackageCandidateExamId
where SPCE.ScheduledExamRef in (
								SELECT ScheduledExamRef 
								from [PPD_BCIntegration_TestPackage].dbo.DATATOKEEP
	)

SET IDENTITY_INSERT [temp_ScheduledPackageCandidateExamItems]  OFF 


SET IDENTITY_INSERT [temp_ScheduledPackageCandidateExams] ON 

INSERT [temp_ScheduledPackageCandidateExams](ScheduledPackageCandidateExamId, ScheduledExamRef, PackageExamId, IsCompleted, DateCompleted, Score, Candidate_ScheduledPackageCandidateId, IsVoided, ShouldIncludeScale, ShouldIncludeCEFR, ShouldIncludeFinalScore, IsRescheduled, ExamVersionRef, IsLocalScan, ResultXml, ExamCompletionDate)
SELECT SPCE.*
FROM ScheduledPackageCandidateExams SPCE
where ScheduledExamRef in (
								SELECT ScheduledExamRef 
								from [PPD_BCIntegration_TestPackage].dbo.DATATOKEEP
	)

SET IDENTITY_INSERT [temp_ScheduledPackageCandidateExams] OFF

SET IDENTITY_INSERT [temp_ScheduledPackageCandidates]  ON

INSERT [temp_ScheduledPackageCandidates](ScheduledPackageCandidateId, SurpassCandidateId, SurpassCandidateRef, FirstName, LastName, MiddleName, BirthDate, IsCompleted, DateCompleted, PackageScore, ScheduledPackage_ScheduledPackageId, IsVoided, IsLocalScan, TestPackageCompletionDate)
SELECT SPC.*
FROM [ScheduledPackageCandidates] SPC
INNER JOIN ScheduledPackageCandidateExams SPCE 
on SPCE.Candidate_ScheduledPackageCandidateId = SPC.ScheduledPackageCandidateId
where ScheduledExamRef in (
								SELECT ScheduledExamRef 
								from [PPD_BCIntegration_TestPackage].dbo.DATATOKEEP
	)

SET IDENTITY_INSERT [temp_ScheduledPackageCandidates]  OFF


SET IDENTITY_INSERT [temp_ScheduledPackages]  ON

INSERT [temp_ScheduledPackages](ScheduledPackageId, CenterId, CenterRef, CenterName, QualificationId, QualificationRef, QualificationName, StartDate, EndDate, StartTime, EndTime, DateCreated, CreatedBy, StatusValue, PackageId, PackageDate)
SELECT SP.* 
FROM ScheduledPackages SP
INNER JOIN ScheduledPackageCandidates SPC
ON SPC.ScheduledPackage_ScheduledPackageId = SP.ScheduledPackageID
INNER JOIN ScheduledPackageCandidateExams SPCE 
on SPCE.Candidate_ScheduledPackageCandidateId = SPC.ScheduledPackageCandidateId
where ScheduledExamRef in (
								SELECT ScheduledExamRef 
								from [PPD_BCIntegration_TestPackage].dbo.DATATOKEEP
	)

SET IDENTITY_INSERT [temp_ScheduledPackages]  OFF