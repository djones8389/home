USE PPD_BCIntegration_SecureAssess


ALTER TABLE [dbo].[WAREHOUSE_ExamSessionAvailableItemsTable]  WITH CHECK ADD  CONSTRAINT [FK_WAREHOUSE_ExamSessionAvailableItemsTable_WAREHOUSE_ExamSessionTable] FOREIGN KEY([ExamSessionID])
REFERENCES [dbo].[WAREHOUSE_ExamSessionTable] ([ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[WAREHOUSE_ExamSessionAvailableItemsTable] CHECK CONSTRAINT [FK_WAREHOUSE_ExamSessionAvailableItemsTable_WAREHOUSE_ExamSessionTable]
GO
ALTER TABLE [dbo].[WAREHOUSE_ExamSessionDocumentTable]  WITH CHECK ADD  CONSTRAINT [FK_WAREHOUSE_ExamSessionDocumentTable_WAREHOUSE_ExamSessionTable] FOREIGN KEY([warehouseExamSessionID])
REFERENCES [dbo].[WAREHOUSE_ExamSessionTable] ([ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[WAREHOUSE_ExamSessionDocumentTable] CHECK CONSTRAINT [FK_WAREHOUSE_ExamSessionDocumentTable_WAREHOUSE_ExamSessionTable]
GO
ALTER TABLE [dbo].[WAREHOUSE_ExamSessionItemResponseTable]  WITH CHECK ADD  CONSTRAINT [FK_WAREHOUSE_ExamSessionItemResponseTable_WAREHOUSE_ExamSessionTable] FOREIGN KEY([WAREHOUSEExamSessionID])
REFERENCES [dbo].[WAREHOUSE_ExamSessionTable] ([ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[WAREHOUSE_ExamSessionItemResponseTable] CHECK CONSTRAINT [FK_WAREHOUSE_ExamSessionItemResponseTable_WAREHOUSE_ExamSessionTable]
GO
ALTER TABLE [dbo].[WAREHOUSE_ExamSessionResultHistoryTable]  WITH CHECK ADD  CONSTRAINT [FK_WAREHOUSE_ExamSessionResultHistoryTable_WAREHOUSE_ExamSessionTable] FOREIGN KEY([WareHouseExamSessionTableID])
REFERENCES [dbo].[WAREHOUSE_ExamSessionTable] ([ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[WAREHOUSE_ExamSessionResultHistoryTable] CHECK CONSTRAINT [FK_WAREHOUSE_ExamSessionResultHistoryTable_WAREHOUSE_ExamSessionTable]
GO
ALTER TABLE [dbo].[WAREHOUSE_ExamSessionTable]  WITH CHECK ADD  CONSTRAINT [FK_WAREHOUSE_ExamSessionTable_WAREHOUSE_ExamStateLookupTable] FOREIGN KEY([WarehouseExamState])
REFERENCES [dbo].[WAREHOUSE_ExamStateLookupTable] ([ID])
GO
ALTER TABLE [dbo].[WAREHOUSE_ExamSessionTable] CHECK CONSTRAINT [FK_WAREHOUSE_ExamSessionTable_WAREHOUSE_ExamStateLookupTable]
GO
ALTER TABLE [dbo].[WAREHOUSE_ExamSessionTable]  WITH CHECK ADD  CONSTRAINT [FK_WAREHOUSE_ExamSessionTable_WAREHOUSE_ScheduledExamsTable] FOREIGN KEY([WAREHOUSEScheduledExamID])
REFERENCES [dbo].[WAREHOUSE_ScheduledExamsTable] ([ID])
GO
ALTER TABLE [dbo].[WAREHOUSE_ExamSessionTable] CHECK CONSTRAINT [FK_WAREHOUSE_ExamSessionTable_WAREHOUSE_ScheduledExamsTable]
GO
ALTER TABLE [dbo].[WAREHOUSE_ExamSessionTable]  WITH CHECK ADD  CONSTRAINT [FK_WAREHOUSE_ExamSessionTable_WAREHOUSE_UserTable] FOREIGN KEY([WAREHOUSEUserID])
REFERENCES [dbo].[WAREHOUSE_UserTable] ([ID])
GO
ALTER TABLE [dbo].[WAREHOUSE_ExamSessionTable] CHECK CONSTRAINT [FK_WAREHOUSE_ExamSessionTable_WAREHOUSE_UserTable]
GO
ALTER TABLE [dbo].[WAREHOUSE_ExamSessionTable_Shreded]  WITH CHECK ADD  CONSTRAINT [FK_WAREHOUSE_ExamSessionTable_Shreded_WAREHOUSE_ExamSessionTable] FOREIGN KEY([examSessionId])
REFERENCES [dbo].[WAREHOUSE_ExamSessionTable] ([ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[WAREHOUSE_ExamSessionTable_Shreded] CHECK CONSTRAINT [FK_WAREHOUSE_ExamSessionTable_Shreded_WAREHOUSE_ExamSessionTable]
GO
ALTER TABLE [dbo].[WAREHOUSE_ExamSessionTable_ShrededItems]  WITH CHECK ADD  CONSTRAINT [FK_WAREHOUSE_ExamSessionTable_ShrededItems_WAREHOUSE_ExamSessionTable_Shreded] FOREIGN KEY([examSessionId])
REFERENCES [dbo].[WAREHOUSE_ExamSessionTable_Shreded] ([examSessionId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[WAREHOUSE_ExamSessionTable_ShrededItems] CHECK CONSTRAINT [FK_WAREHOUSE_ExamSessionTable_ShrededItems_WAREHOUSE_ExamSessionTable_Shreded]
GO
ALTER TABLE [dbo].[WAREHOUSE_ExamStateAuditTable]  WITH CHECK ADD  CONSTRAINT [FK_WAREHOUSE_ExamStateAuditTable_WAREHOUSE_ExamSessionTable] FOREIGN KEY([WarehouseExamSessionID])
REFERENCES [dbo].[WAREHOUSE_ExamSessionTable] ([ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[WAREHOUSE_ExamStateAuditTable] CHECK CONSTRAINT [FK_WAREHOUSE_ExamStateAuditTable_WAREHOUSE_ExamSessionTable]
GO
ALTER TABLE [dbo].[WAREHOUSE_ExamStateAuditTable]  WITH CHECK ADD  CONSTRAINT [FK_WAREHOUSE_ExamStateAuditTable_WAREHOUSE_ExamStateLookupTable] FOREIGN KEY([ExamState])
REFERENCES [dbo].[WAREHOUSE_ExamStateLookupTable] ([ID])
GO
ALTER TABLE [dbo].[WAREHOUSE_ExamStateAuditTable] CHECK CONSTRAINT [FK_WAREHOUSE_ExamStateAuditTable_WAREHOUSE_ExamStateLookupTable]
GO
ALTER TABLE [dbo].[WAREHOUSE_ScheduledExamsTable]  WITH CHECK ADD  CONSTRAINT [FK_WAREHOUSE_ScheduledExamsTable_WAREHOUSE_CentreTable] FOREIGN KEY([WAREHOUSECentreID])
REFERENCES [dbo].[WAREHOUSE_CentreTable] ([ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[WAREHOUSE_ScheduledExamsTable] CHECK CONSTRAINT [FK_WAREHOUSE_ScheduledExamsTable_WAREHOUSE_CentreTable]
GO
ALTER TABLE [dbo].[WAREHOUSE_ScheduledExamsTable]  WITH NOCHECK ADD  CONSTRAINT [FK_WAREHOUSE_ScheduledExamsTable_WAREHOUSE_UserTable] FOREIGN KEY([WAREHOUSECreatedBy])
REFERENCES [dbo].[WAREHOUSE_UserTable] ([ID])
GO
ALTER TABLE [dbo].[WAREHOUSE_ScheduledExamsTable] CHECK CONSTRAINT [FK_WAREHOUSE_ScheduledExamsTable_WAREHOUSE_UserTable]
GO
