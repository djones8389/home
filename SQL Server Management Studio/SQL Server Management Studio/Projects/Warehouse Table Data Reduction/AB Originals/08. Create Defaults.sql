/*Notes: This script creates defaults on the newly 
    renamed tables to bring them inline with the originals.*/

/*Create defaults*/
ALTER TABLE [dbo].[WAREHOUSE_ExamSessionItemResponseTable]
    ADD CONSTRAINT [DF_WAREHOUSE_ExamSessionItemResponseTable_MarkingIgnored] 
    DEFAULT ((0)) 
    FOR [MarkingIgnored];

ALTER TABLE [dbo].[WAREHOUSE_ExamSessionTable]
    ADD CONSTRAINT [DF_WAREHOUSE_ExamSessionTable_AllowPackageDelivery] 
    DEFAULT ((0)) 
    FOR [AllowPackageDelivery];

ALTER TABLE [dbo].[WAREHOUSE_ExamSessionTable]
    ADD CONSTRAINT [DF_WAREHOUSE_ExamSessionTable_appeal] 
    DEFAULT ((0)) 
    FOR [appeal];

ALTER TABLE [dbo].[WAREHOUSE_ExamSessionTable]
    ADD CONSTRAINT [DF_WAREHOUSE_ExamSessionTable_availableForCentreReview] 
    DEFAULT ((0)) 
    FOR [availableForCentreReview];

ALTER TABLE [dbo].[WAREHOUSE_ExamSessionTable]
    ADD CONSTRAINT [DF_WAREHOUSE_ExamSessionTable_ContainsBTLOffice] 
    DEFAULT ((0)) 
    FOR [ContainsBTLOffice];

ALTER TABLE [dbo].[WAREHOUSE_ExamSessionTable]
    ADD CONSTRAINT [DF_WAREHOUSE_ExamSessionTable_EnableOverrideMarking] 
    DEFAULT ((0)) 
    FOR [EnableOverrideMarking];

ALTER TABLE [dbo].[WAREHOUSE_ExamSessionTable]
    ADD CONSTRAINT [DF_WAREHOUSE_ExamSessionTable_ExportedToIntegration] 
    DEFAULT ((0)) 
    FOR [ExportedToIntegration];

ALTER TABLE [dbo].[WAREHOUSE_ExamSessionTable]
    ADD CONSTRAINT [DF_WAREHOUSE_ExamSessionTable_ExportToSecureMarker] 
    DEFAULT ((0)) 
    FOR [ExportToSecureMarker];

ALTER TABLE [dbo].[WAREHOUSE_ExamSessionTable]
    ADD CONSTRAINT [DF_WAREHOUSE_ExamSessionTable_remarkStatus] 
    DEFAULT ((0)) 
    FOR [reMarkStatus];

ALTER TABLE [dbo].[WAREHOUSE_ExamSessionTable]
    ADD CONSTRAINT [DF_WAREHOUSE_ExamSessionTable_submissionExported] 
    DEFAULT ((0)) 
    FOR [submissionExported];

ALTER TABLE [dbo].[WAREHOUSE_ExamSessionTable]
    ADD CONSTRAINT [DF_WAREHOUSE_ExamSessionTable_WarehouseExamState] 
    DEFAULT ((1)) 
    FOR [WarehouseExamState];

ALTER TABLE [dbo].[WAREHOUSE_ExamSessionTable_ShrededItems]
    ADD CONSTRAINT [DF__WAREHOUSE__Total__28A2FA0E] 
    DEFAULT ((0)) 
    FOR [TotalMark];

ALTER TABLE [dbo].[WAREHOUSE_ScheduledExamsTable]
    ADD CONSTRAINT [DF_WAREHOUSE_ScheduledExamsTable_IsExternal] 
    DEFAULT ((0)) 
    FOR [IsExternal];

ALTER TABLE [dbo].[WAREHOUSE_ExamSessionTable_Shreded]
    ADD CONSTRAINT [DF_WAREHOUSE_ExamSessionTable_Shreded_AllowPackageDelivery] 
    DEFAULT ((0)) 
    FOR [AllowPackageDelivery];

ALTER TABLE [dbo].[WAREHOUSE_ExamSessionTable_Shreded]
    ADD CONSTRAINT [DF_WAREHOUSE_ExamSessionTable_Shreded_ContainsBTLOffice] 
    DEFAULT ((0)) 
    FOR [ContainsBTLOffice];

ALTER TABLE [dbo].[WAREHOUSE_ExamSessionTable_Shreded]
    ADD CONSTRAINT [DF_WAREHOUSE_ExamSessionTable_Shreded_EnableOverrideMarking] 
    DEFAULT ((0)) 
    FOR [EnableOverrideMarking];

ALTER TABLE [dbo].[WAREHOUSE_ExamSessionTable_Shreded]
    ADD CONSTRAINT [DF_WAREHOUSE_ExamSessionTable_Shreded_ExportedToIntegration] 
    DEFAULT ((0)) 
    FOR [ExportedToIntegration];

ALTER TABLE [dbo].[WAREHOUSE_ExamSessionTable_Shreded]
    ADD CONSTRAINT [DF_WAREHOUSE_ExamSessionTable_Shreded_ExportToSecureMarker] 
    DEFAULT ((0)) 
    FOR [ExportToSecureMarker];

ALTER TABLE [dbo].[WAREHOUSE_ExamSessionTable_Shreded]
    ADD CONSTRAINT [DF_WAREHOUSE_ExamSessionTable_Shreded_IsExternal] 
    DEFAULT ((0)) 
    FOR [IsExternal];

ALTER TABLE [dbo].[WAREHOUSE_ExamSessionTable_Shreded]
    ADD CONSTRAINT [DF_WAREHOUSE_ExamSessionTable_Shreded_KeyCode] 
    DEFAULT ('') 
    FOR [KeyCode];

ALTER TABLE [dbo].[WAREHOUSE_ExamSessionTable_Shreded]
    ADD CONSTRAINT [DF_WAREHOUSE_ExamSessionTable_Shreded_qualificationLevel] 
    DEFAULT ((1)) 
    FOR [qualificationLevel];

ALTER TABLE [dbo].[WAREHOUSE_ExamSessionTable_Shreded]
    ADD CONSTRAINT [DF_WAREHOUSE_ExamSessionTable_Shreded_WarehouseExamState] 
    DEFAULT ((1)) 
    FOR [WarehouseExamState];

