USE [PPD_BCIntegration_TestPackage]

ALTER TABLE [ScheduledPackageCandidateExams] DROP CONSTRAINT [DF__Scheduled__IsVoi__1A14E395];
ALTER TABLE [ScheduledPackageCandidateExams] DROP CONSTRAINT [DF__Scheduled__Shoul__1DE57479];
ALTER TABLE [ScheduledPackageCandidateExams] DROP CONSTRAINT [DF__Scheduled__Shoul__1ED998B2];
ALTER TABLE [ScheduledPackageCandidateExams] DROP CONSTRAINT [DF__Scheduled__Shoul__1FCDBCEB];
ALTER TABLE [ScheduledPackageCandidateExams] DROP CONSTRAINT [DF_ScheduledPackageCandidateExams_IsLocalScan];
ALTER TABLE [ScheduledPackageCandidateExams] DROP CONSTRAINT [DF__Scheduled__IsRes__32E0915F];
ALTER TABLE [ScheduledPackageCandidates] DROP CONSTRAINT [DF_ScheduledPackageCandidates_IsLocalScan];
ALTER TABLE [ScheduledPackageCandidates] DROP CONSTRAINT [DF__Scheduled__IsVoi__1920BF5C];
ALTER TABLE [ScheduledPackages] DROP CONSTRAINT [DF__Scheduled__Packa__1CF15040];


/*select  
	t.name
  , dc.name [DCNAME]
  , 'ALTER TABLE ' + QUOTENAME(t.name) + ' DROP CONSTRAINT ' + quotename(dc.name) + ';'
--, kc.name [KCNAME]
from sys.tables t
inner join sys.objects o
on o.object_id = t.object_id
inner join sys.default_constraints dc
on dc.parent_object_id = o.object_id
--inner join sys.key_constraints kc
--on kc.parent_object_id = o.object_id
order by 1
*/