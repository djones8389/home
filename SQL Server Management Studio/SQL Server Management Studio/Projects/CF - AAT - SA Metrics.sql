SELECT  
	 a.database_name
	, SUM([TotalKB])/1024 [Total_MB]
	, SUM([TotalKB])/1024/1024  [Total_GB]
	, [StartDate]
FROM (
SELECT 
	 [database_name]
      ,cast(replace([data_kb], 'KB','') as float) + cast(replace([index_size], 'KB','') as float) [TotalKB]
      ,cast(CollectionDate as date) [StartDate]
  FROM [PSCollector].[dbo].[xxDBGrowthMetrics]
  where database_name = 'aat_SecureAssess'
	AND cast(CollectionDate as date) > '2017-01-02'
) a
GROUP BY   a.database_name,  [StartDate]
--ORDER BY [StartDate]

UNION

SELECT  
	 a.database_name
	, SUM([TotalKB])/1024 [Total_MB]
	, SUM([TotalKB])/1024/1024  [Total_GB]
	, [StartDate]
FROM (
SELECT 
	[Instance]
      , [database_name]
	  , table_name
      , CAST(replace([data_kb], 'KB','') as float) + cast(replace([index_size], 'KB','') as float) [TotalKB]
	  , ROW_NUMBER() OVER(PARTITION BY [database_name],table_name, cast([StartDate] as date) ORDER BY [StartDate]) R
      ,[StartDate] [StartDate]
  FROM [PSCollector].[dbo].[DBGrowthMetrics]
  where database_name = 'aat_SecureAssess'
	--AND cast([StartDate] as date) between '2017-04-12' AND '2017-04-24'
	--ORDER BY [Instance]
	--	  ,[database_name]
	--	  ,table_name,[StartDate]
) A
WHERE  R = 1

GROUP BY   a.database_name,  [StartDate]
ORDER BY [StartDate]







/*
SELECT
	a.database_name
	, SUM([TotalKB])/1024 [Total_MB]
	, SUM([TotalKB])/1024/1024  [Total_GB]
	, [StartDate]
FROM (
SELECT 
	[Instance]
      ,[database_name]
	  --,table_name
	  --, rows
      ,cast(replace([data_kb], 'KB','') as float) + cast(replace([index_size], 'KB','') as float) [TotalKB]
      ,cast([StartDate] as date) [StartDate]
  FROM [PSCollector].[dbo].[DBGrowthMetrics]
  where database_name = 'aat_SecureAssess'
	--AND cast([StartDate] as date) > '2017-06-01'
	--AND table_name IN ('ExamSessionDocumentTable','WAREHOUSE_ExamSessionDocumentTable')
) a
GROUP BY a.Instance, a.database_name,  [StartDate]
ORDER BY [StartDate]
--ORDER BY cast([StartDate] as date), table_name
*/
