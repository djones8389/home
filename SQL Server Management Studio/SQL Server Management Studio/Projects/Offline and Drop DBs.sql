select Name
from sys.databases
where sys.databases.database_id > 4
	AND state_desc = 'ONLINE'
	order by NAME


select 'ALTER DATABASE [' + Name + '] SET ONLINE;'
from sys.databases
where sys.databases.database_id > 4
	--AND name like '%SQA2%'
	order by NAME

select 'DROP DATABASE [' + Name + '];'
from sys.databases
where sys.databases.database_id > 4
	AND state_desc = 'ONLINE'
	order by NAME

--ALTER DATABASE [AAT_ContentProducer] SET ONLINE;
--ALTER DATABASE [AAT_ItemBank] SET ONLINE;
--ALTER DATABASE [AAT_SecureAssess] SET ONLINE;
--ALTER DATABASE [AAT_SecureMarker] SET ONLINE;
--ALTER DATABASE [AAT_SurpassDataWarehouse] SET ONLINE;

--ALTER DATABASE [UAT_BC2_SecureMarker_READONLY] SET OFFLINE;

--ALTER DATABASE [AAT_SecureMarker_READONLY] SET OFFLINE;
--ALTER DATABASE [UAT_SQA_ContentProducer] SET OFFLINE;
--ALTER DATABASE [UAT_SQA_ItemBank] SET OFFLINE;
--ALTER DATABASE [UAT_SQA_OpenAssess] SET OFFLINE;
--ALTER DATABASE [UAT_SQA_SecureAssess] SET OFFLINE;
--ALTER DATABASE [UAT_SQA_SurpassDataWarehouse] SET OFFLINE;
--ALTER DATABASE [UAT_CandG_SecureAssess] SET OFFLINE;
--ALTER DATABASE [DONOTUSE_UAT_BC2_TestPackageManager] SET OFFLINE;
--ALTER DATABASE [SecureAssessLocal_AQA] SET OFFLINE;
--ALTER DATABASE [UAT_AQA_SurpassDataWarehouse_11_7_ENT] SET OFFLINE;

--ALTER DATABASE [UAT_AQA_SurpassDataWarehouse_11_7_ENT] SET ONLINE;

--ALTER DATABASE [UAT_EAL_ContentProducer] SET OFFLINE;
--ALTER DATABASE [UAT_EAL_ItemBank] SET OFFLINE;
--ALTER DATABASE [UAT_EAL_SecureAssess] SET OFFLINE;
--ALTER DATABASE [UAT_EAL_SurpassDataWarehouse] SET OFFLINE;

--sp_whoisactive

--ALTER DATABASE [AAT_ContentProducer] SET OFFLINE;
--ALTER DATABASE [AAT_ItemBank] SET OFFLINE;
--ALTER DATABASE [AAT_SecureAssess] SET OFFLINE;
--ALTER DATABASE [AAT_SecureMarker] SET OFFLINE;
--ALTER DATABASE [AAT_SurpassDataWarehouse] SET OFFLINE;


--ALTER DATABASE [UAT_SQA2_ContentProducer] SET OFFLINE;
--ALTER DATABASE [UAT_SQA2_ItemBank] SET OFFLINE;
--ALTER DATABASE [UAT_SQA2_OpenAssess] SET OFFLINE;
--ALTER DATABASE [UAT_SQA2_SecureAssess] SET OFFLINE;
--ALTER DATABASE [UAT_SQA2_SurpassDataWarehouse] SET OFFLINE;