use rcpch_secureassess

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

SELECT
	EST.KeyCode
	, EST.examState
	, UT.Forename
	, UT.Surname
	, CT.CentreName
	, ct.CentreCode
	, SCET.examName
	, downloadInformation.value('data(downloads/information/machineName/text())[1]','nvarchar(max)')  machineName
	, downloadInformation.value('data(downloads/information/clientIP/text())[1]','nvarchar(max)')  clientIP
	, downloadInformation.value('data(downloads/information/date/text())[1]','nvarchar(MAX)') [date]
  FROM ExamSessionTable as EST

  Inner Join ScheduledExamsTable as SCET  on SCET.ID = EST.ScheduledExamID   
  Inner Join UserTable as UT  on UT.ID = EST.UserID
  Inner Join CentreTable as CT on CT.ID = SCET.CentreID

where centrename = 'Jeddah Female CL036F'