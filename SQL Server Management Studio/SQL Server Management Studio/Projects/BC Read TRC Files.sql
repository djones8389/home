USE SPDurationMetrics

--DROP TABLE [BCTraceFiles]

--select
--	ServerName
--	, ObjectName
--	, Duration
--	, 'AQA_CPProjectAdmin' [DB]
--INTO [BCTraceFiles]
--from ::fn_trace_gettable ('D:\New Folder\AQA_CPProjectAdmin.trc',1000)
----where Duration > 0

--DROP TABLE [BCTraceFiles]

INSERT [BCTraceFiles]
select 
	ServerName
	, ObjectName
	, Duration
	, 'AQA_CPProjectAdmin' [DB]
from ::fn_trace_gettable ('D:\New Folder\AQA_CPProjectAdmin.trc',1000)

INSERT [BCTraceFiles]
select 
	ServerName
	, ObjectName
	, Duration
	, 'AQA_ItemBank' [DB]
from ::fn_trace_gettable ('D:\New Folder\AQA_ItemBank.trc',31)

INSERT [BCTraceFiles]
select 
	ServerName
	, ObjectName
	, Duration
	, 'AQA_ReportServer' [DB]
from ::fn_trace_gettable ('D:\New Folder\AQA_ReportServer.trc',31)

INSERT [BCTraceFiles]
select 
	ServerName
	, ObjectName
	, Duration
	, 'AQA_ReportServerTempDB' [DB]
from ::fn_trace_gettable ('D:\New Folder\AQA_ReportServerTempDB.trc',31)

INSERT [BCTraceFiles]
select 
	ServerName
	, ObjectName
	, Duration
	, 'AQA_SecureAssess' [DB]
from ::fn_trace_gettable ('D:\New Folder\AQA_SecureAssess.trc',31)

INSERT [BCTraceFiles]
select 
	ServerName
	, ObjectName
	, Duration
	, 'AQA_SecureMarker_10.0' [DB]
from ::fn_trace_gettable ('D:\New Folder\AQA_SecureMarker_10.0.trc',31)


INSERT [BCTraceFiles]
select 
	ServerName
	, ObjectName
	, Duration
	, 'AQA_SurpassDataWarehouse' [DB]
from ::fn_trace_gettable ('D:\New Folder\AQA_SurpassDataWarehouse.trc',31)


INSERT [BCTraceFiles]
select 
	ServerName
	, ObjectName
	, Duration
	, 'BTLSURPASSAUDIT_AQA.' [DB]
from ::fn_trace_gettable ('D:\New Folder\BTLSURPASSAUDIT_AQA.trc',31)