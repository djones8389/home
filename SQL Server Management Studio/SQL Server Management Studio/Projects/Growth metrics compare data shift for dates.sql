--SELECT a.*
--from (
--SELECT [server_name]
--      ,[database_name]
--      ,[table_name]
--      ,cast([CollectionDate] as date) [CollectionDate]
--	  ,cast(replace([data_kb], 'kb','')  as int) +  cast(replace([index_size], 'kb','') as int) [size]
--  FROM [PSCollector].[dbo].[DBGrowthMetrics]
--  where server_name =  '335657-APP1'
--	and cast([CollectionDate] as date)  in ('2017-01-16')
--) a

--select b.*
--from (
--SELECT [server_name]
--      ,[database_name]
--      ,[table_name]
--      ,cast([CollectionDate] as date) [CollectionDate]
--	  ,cast(replace([data_kb], 'kb','')  as int) +  cast(replace([index_size], 'kb','') as int) [size]
--  FROM [PSCollector].[dbo].[DBGrowthMetrics]
--  where server_name =  '335657-APP1'
--	and cast([CollectionDate] as date)  in ('2017-01-23')
--) b

select a.[server_name]
	, a.[database_name]
	--, a.table_name
	, sum(a.size)
	, sum(b.size)
	--,(b.size-a.size) [Diff]
from (
SELECT [server_name]
      ,[database_name]
      ,[table_name]
      ,cast([CollectionDate] as date) [CollectionDate]
	  ,cast(replace([data_kb], 'kb','')  as int) +  cast(replace([index_size], 'kb','') as int) [size]
  FROM [PSCollector].[dbo].[DBGrowthMetrics]
  where server_name =  '335657-APP1'
	and cast([CollectionDate] as date)  in ('2017-01-16')
)	a
inner join (
SELECT [server_name]
      ,[database_name]
      ,[table_name]
      ,cast([CollectionDate] as date) [CollectionDate]
	  ,cast(replace([data_kb], 'kb','')  as int) +  cast(replace([index_size], 'kb','') as int) [size]
  FROM [PSCollector].[dbo].[DBGrowthMetrics]
  where server_name =  '335657-APP1'
	and cast([CollectionDate] as date)  in ('2017-01-23')
	) b
on a.[server_name] = b.[server_name]
	and a.[database_name] = b.[database_name]
	and a.[table_name] = b.[table_name]

--where a.size <> b.size
group by a.[server_name]
	, a.[database_name]
	--, a.table_name
	--order by (b.size-a.size) desc