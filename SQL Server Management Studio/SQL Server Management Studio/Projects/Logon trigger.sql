drop table  master.dbo.Audit_XML

CREATE TABLE master.dbo.Audit_XML
(
      event_type  varchar(100)
	  , loginTime datetime
	  , ServerName varchar(100)
	  , LoginName varchar(100)
	  , LoginType varchar(100)
	  , ClientHost varchar(100)
	  , fullXML xml
);



CREATE TRIGGER Log_Table_DDL
ON  ALL SERVER 
FOR LOGON
AS

 
BEGIN

	INSERT INTO  master.dbo.Audit_XML (event_type, loginTime, ServerName, LoginType, ClientHost,fullXML)
	select 
		eventData().value('data(EVENT_INSTANCE/EventType)[1]','varchar(100)')
		,eventData().value('data(EVENT_INSTANCE/PostTime)[1]','datetime')
		,eventData().value('data(EVENT_INSTANCE/ServerName)[1]','varchar(100)')
		,eventData().value('data(EVENT_INSTANCE/LoginType)[1]','varchar(100)')
		,eventData().value('data(EVENT_INSTANCE/ClientHost)[1]','varchar(100)')
		,eventData()

END
GO

DISABLE TRIGGER Log_Table_DDL ON  ALL SERVER 
GO

--drop TRIGGER Log_Table_DDL
--ON  ALL SERVER 

/*
use master;

declare @eventInfo xml = 
'<EVENT_INSTANCE>
	  <EventType>LOGON</EventType>
	  <PostTime>2017-01-23T10:50:11.513</PostTime>
	  <SPID>55</SPID>
	  <ServerName>BTL144\SQL2014</ServerName>
	  <LoginName>SALTSWHARF\DaveJ</LoginName>
	  <LoginType>Windows (NT) Login</LoginType>
	  <SID>AQUAAAAAAAUVAAAAxx9wxT3ZsF9sE6FNHREAAA==</SID>
	  <ClientHost>&lt;local machine&gt;</ClientHost>
	  <IsPooled>1</IsPooled>
</EVENT_INSTANCE>'


select 
	@eventInfo.value('data(EVENT_INSTANCE/EventType)[1]','varchar(100)')
	,@eventInfo.value('data(EVENT_INSTANCE/PostTime)[1]','datetime')
	,@eventInfo.value('data(EVENT_INSTANCE/ServerName)[1]','varchar(100)')
	,@eventInfo.value('data(EVENT_INSTANCE/LoginType)[1]','varchar(100)')
	,@eventInfo.value('data(EVENT_INSTANCE/ClientHost)[1]','varchar(100)')

*/