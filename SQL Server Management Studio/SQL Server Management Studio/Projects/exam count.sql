USE connect_Secureassess

select a.DBName
	, examState
	, count(ID) [Count]
FROM (
select db_name() DBName
	, ID
	, examState
from ExamSessionTable NOLOCK
) a
group by DBName,examState
order by 1,2



use master

IF OBJECT_ID('tempdb..##DATALENGTH') IS NOT NULL DROP TABLE ##DATALENGTH;

CREATE TABLE ##DATALENGTH (
	client nvarchar(1000)
	, examState tinyint
	, [count] int
);

DECLARE @dynamic nvarchar(MAX)='';

select @dynamic +=CHAR(13)+ '
use '+QUOTENAME([NAME])+'

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

INSERT ##DATALENGTH
select a.DBName
	, examState
	, count(ID) [Count]
FROM (
select db_name() DBName
	, ID
	, examState
from ExamSessionTable NOLOCK
) a
group by DBName,examState
order by 1,2

'
from sys.databases
where state_desc = 'ONLINE'
	and name like '%[_]SecureAssess';

exec(@dynamic);



select *
from ##DATALENGTH
where examstate in (9,10,11,12)
order by 1






