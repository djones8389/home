--IF(SUSER_ID('SHA_QEYADAH_CPProjectAdmin_10.0_Login') IS NULL) BEGIN CREATE LOGIN [SHA_QEYADAH_CPProjectAdmin_10.0_Login] WITH PASSWORD = 0x0100CEAA298722EFD3F0D8FBDE4986E1B26775DCC1978E50A134 HASHED,CHECK_POLICY = OFF,CHECK_EXPIRATION = OFF,DEFAULT_DATABASE = [master] END;
--IF(SUSER_ID('SHA_QEYADAH_ItemBank_10.0_Login') IS NULL) BEGIN CREATE LOGIN [SHA_QEYADAH_ItemBank_10.0_Login] WITH PASSWORD = 0x0100733C909A4CACB353E5D59B6C60FAFAEE3B103B84215F8019 HASHED,CHECK_POLICY = OFF,CHECK_EXPIRATION = OFF,DEFAULT_DATABASE = [master] END;
--IF(SUSER_ID('SHA_QEYADAH_SecureAssess_10.0_Login') IS NULL) BEGIN CREATE LOGIN [SHA_QEYADAH_SecureAssess_10.0_Login] WITH PASSWORD = 0x010087186CC8E769CC7679DA0C9C61842ADDB0DB04DE259EC94A HASHED,CHECK_POLICY = OFF,CHECK_EXPIRATION = OFF,DEFAULT_DATABASE = [master] END;
--IF(SUSER_ID('SHA_BIS_CPProjectAdmin_11.0_Login') IS NULL) BEGIN CREATE LOGIN [SHA_BIS_CPProjectAdmin_11.0_Login] WITH PASSWORD = 0x0100E57C0E711D8834D6E49C4C79C57E65E8050C1B2B3C320BEB HASHED,CHECK_POLICY = OFF,CHECK_EXPIRATION = OFF,DEFAULT_DATABASE = [master] END;
--IF(SUSER_ID('SHA_BIS_ItemBank_11.0_Login') IS NULL) BEGIN CREATE LOGIN [SHA_BIS_ItemBank_11.0_Login] WITH PASSWORD = 0x010064A8652C6A91BE357963A17A7E57A93CC28878A4E1769F0B HASHED,CHECK_POLICY = OFF,CHECK_EXPIRATION = OFF,DEFAULT_DATABASE = [master] END;
--IF(SUSER_ID('SHA_BIS_SecureAssess_11.0_Login') IS NULL) BEGIN CREATE LOGIN [SHA_BIS_SecureAssess_11.0_Login] WITH PASSWORD = 0x010042C09F4EF598A032414F4880332C14EB2CD367F9C13BE3C1 HASHED,CHECK_POLICY = OFF,CHECK_EXPIRATION = OFF,DEFAULT_DATABASE = [master] END;
--IF(SUSER_ID('SHA_CCEA_ContentProducer_Login') IS NULL) BEGIN CREATE LOGIN [SHA_CCEA_ContentProducer_Login] WITH PASSWORD = 0x010058829D2EC0D73C2B1FD7FF752EB3966C00DCE62BFB7D72FD HASHED,CHECK_POLICY = OFF,CHECK_EXPIRATION = OFF,DEFAULT_DATABASE = [master] END;
--IF(SUSER_ID('SHA_CCEA_ItemBank_Login') IS NULL) BEGIN CREATE LOGIN [SHA_CCEA_ItemBank_Login] WITH PASSWORD = 0x0100B4B8F3FD3F94CE207F253F650C6C5BAC7A5CB1E3C66B14B9 HASHED,CHECK_POLICY = OFF,CHECK_EXPIRATION = OFF,DEFAULT_DATABASE = [master] END;

--use [SHA_BIS_CPProjectAdmin_11.0] exec sp_change_users_login 'auto_fix','SHA_BIS_CPProjectAdmin_11.0_User', NULL,NULL;
--use [SHA_BIS_ItemBank_11.0] exec sp_change_users_login 'auto_fix','SHA_BIS_ItemBank_11.0_User', NULL,NULL;
--use [SHA_BIS_SecureAssess_11.0] exec sp_change_users_login 'auto_fix','SHA_BIS_SecureAssess_11.0_User', NULL,NULL;
--use [SHA_BTLGCC_CP_BTL_Arabic_Project_Copy] exec sp_change_users_login 'auto_fix','SHA_BTLGCC_CP_BTL_Arabic_Project_Copy_User', NULL,NULL;
--use [SHA_BTLGCC_CP_BTL_Test_Project] exec sp_change_users_login 'auto_fix','SHA_BTLGCC_CP_BTL_Test_Project_User', NULL,NULL;
--use [SHA_BTLGCC_CP_TESTcb20062013Onsc] exec sp_change_users_login 'auto_fix','SHA_BTLGCC_CP_TESTcb20062013Onsc_User', NULL,NULL;
--use [SHA_BTLGCC_CP_TESTCB20062013Pap] exec sp_change_users_login 'auto_fix','SHA_BTLGCC_CP_TESTCB20062013Pap_User', NULL,NULL;
--use [SHA_CCEA_ContentProducer] exec sp_change_users_login 'auto_fix','SHA_CCEA_ContentProducer_User', NULL,NULL;
--use [SHA_CCEA_ItemBank] exec sp_change_users_login 'auto_fix','SHA_CCEA_ItemBank_User', NULL,NULL;
--use [SHA_DEMO_CP_BTL_Arabic_Project] exec sp_change_users_login 'auto_fix','SHA_DEMO_CP_BTL_Arabic_Project_User', NULL,NULL;
--use [SHA_QEYADAH_CP_abc1234565789] exec sp_change_users_login 'auto_fix','SHA_QEYADAH_CP_abc1234565789_User', NULL,NULL;
--use [SHA_QEYADAH_CP_BTLTestOnscreen1109] exec sp_change_users_login 'auto_fix','SHA_QEYADAH_CP_BTLTestOnscreen1109_User', NULL,NULL;
--use [SHA_QEYADAH_CP_BTLTestPaper1109] exec sp_change_users_login 'auto_fix','SHA_QEYADAH_CP_BTLTestPaper1109_User', NULL,NULL;
--use [SHA_QEYADAH_CP_cbonscreen22032013testlivedeploy] exec sp_change_users_login 'auto_fix','SHA_QEYADAH_CP_cbonscreen22032013testlivedeploy_User', NULL,NULL;
--use [SHA_QEYADAH_CP_cbpaperdeploytest22032013] exec sp_change_users_login 'auto_fix','SHA_QEYADAH_CP_cbpaperdeploytest22032013_User', NULL,NULL;
--use [SHA_QEYADAH_CP_christest] exec sp_change_users_login 'auto_fix','SHA_QEYADAH_CP_christest_User', NULL,NULL;
--use [SHA_QEYADAH_CP_grfrggfg] exec sp_change_users_login 'auto_fix','SHA_QEYADAH_CP_grfrggfg_User', NULL,NULL;
--use [SHA_QEYADAH_CP_HB_10Sep2012] exec sp_change_users_login 'auto_fix','SHA_QEYADAH_CP_HB_10Sep2012_User', NULL,NULL;
--use [SHA_QEYADAH_CP_testdeploy22032013cb] exec sp_change_users_login 'auto_fix','SHA_QEYADAH_CP_testdeploy22032013cb_User', NULL,NULL;
--use [SHA_QEYADAH_CP_Tuesdayaft11092012] exec sp_change_users_login 'auto_fix','SHA_QEYADAH_CP_Tuesdayaft11092012_User', NULL,NULL;
--use [SHA_QEYADAH_CPProjectAdmin_10.0] exec sp_change_users_login 'auto_fix','SHA_QEYADAH_CPProjectAdmin_10.0_User', NULL,NULL;
--use [SHA_QEYADAH_ItemBank_10.0] exec sp_change_users_login 'auto_fix','SHA_QEYADAH_ItemBank_10.0_User', NULL,NULL;
--use [SHA_QEYADAH_SecureAssess_10.0] exec sp_change_users_login 'auto_fix','SHA_QEYADAH_CPProjectAdmin_10.0_User', NULL,NULL;
--use [SHA_QEYADAH_SecureAssess_10.0] exec sp_change_users_login 'auto_fix','SHA_QEYADAH_SecureAssess_10.0_User', NULL,NULL;




SELECT 'IF(SUSER_ID('+QUOTENAME(p.name,'''')+') IS NULL) BEGIN CREATE LOGIN '+QUOTENAME(p.name)+
       CASE WHEN p.type_desc = 'SQL_LOGIN'
            THEN ' WITH PASSWORD = '+CONVERT(NVARCHAR(MAX),L.password_hash,1)+' HASHED,'
            ELSE ' FROM WINDOWS'
       END  + 
	   'SID = ' + --+ L.sid +
       CASE WHEN is_policy_checked = '1'
		   THEN 'CHECK_POLICY = ON,'
		   ELSE 'CHECK_POLICY = OFF,'
       END +
       CASE WHEN is_expiration_checked = '1'
		   THEN 'CHECK_EXPIRATION = ON,'
		   ELSE 'CHECK_EXPIRATION = OFF,'
       END +
       'DEFAULT_DATABASE = ' +  quotename(l.default_database_name) + ' END;' 
       
       COLLATE SQL_Latin1_General_CP1_CI_AS      
  FROM sys.server_principals AS p
  LEFT JOIN sys.sql_logins AS L
    ON p.principal_id = L.principal_id
WHERE p.type_desc IN ('SQL_LOGIN','WINDOWS_GROUP','WINDOWS_LOGIN')
   AND p.name NOT IN ('SA')
  AND p.name NOT LIKE '##%##'
  and l.name like 'sha_%'

  select * from sys.sql_logins

--DECLARE @command1 nvarchar(1000) = '

--USE [?];

--select ''?''  as DBName
--		, u.name as ''User''
--		, (select SUSER_SNAME (u.sid)) as ''Login''
--		, r.name  as RoleName
--		--, u.*
--from sys.database_role_members RM 
--	right join sys.database_principals U on U.principal_id = RM.member_principal_id
--	left join sys.database_principals R on R.principal_id = RM.role_principal_id
--where u.type<>''R''
--	and U.type = ''S''
--	and r.name IS NOT NULL
--	and ''?'' not in (''msdb'',''tempdb'',''model'',''master'')
--'

--create TABLE #UserLogins (DBName nvarchar(150), userName nvarchar(150), loginName nvarchar(150), RoleName nvarchar(50))
--INSERT #UserLogins
--EXEC sp_MSforeachdb  @command1

--select  'use ' +QUOTENAME(DBName)+ ' exec sp_change_users_login ' + '''auto_fix'',''' + username + ''', NULL,NULL;'
--from #UserLogins
--where dbname like 'sha_%'
--	and username not in ('dbo','powershell')
--	order by 1;