USE UAT_BC_SecureMarker

SELECT 
	ExternalItemName
	, UGR.ID
	, c.i.value('.[1]', 'nvarchar(max)') as userData	
	, Keycode
FROM CandidateExamVersions as CEV

INNER JOIN CandidateResponses as CR
ON CR.CandidateExamVersionID = CEV.ID

INNER JOIN UniqueResponses as UR
ON UR.ID = CR.UniqueResponseID 

INNER JOIN UniqueGroupResponseLinks as UGRL
on UGRL.UniqueResponseId = UR.id

INNER JOIN UniqueGroupResponses as UGR
on UGR.ID = UGRL.UniqueGroupResponseID

INNER JOIN Items as I
on I.ID = UR.itemId

cross apply UR.responseData.nodes('/p/s/c/i') c(i)

where ugr.ID in ('539646','539658','539746','539750','540501','541502','541547','541579','541587','548770','548790','550231','550239','559918','559927','559979','560772','560824','560880','560936','561077','562666','562856','562876')
	--and ExternalItemName = 'Teens_W_Ver_3_Task_4_WIP'

	
	order by UGR.ID;
	
	
	
USE UAT_BC_SecureAssess	


select
	structureXml
	, ExternalReference
from WAREHOUSE_ExamSessionTable_Shreded 


left join WAREHOUSE_ExamSessionItemResponseTable 
on WAREHOUSE_ExamSessionItemResponseTable.WAREHOUSEExamSessionID = WAREHOUSE_ExamSessionTable_Shreded.examSessionId

left join WAREHOUSE_ExamSessionTable_ShrededItems
on WAREHOUSE_ExamSessionTable_ShrededItems.examSessionId = WAREHOUSE_ExamSessionTable_Shreded.examSessionId

cross apply ItemResponseData.nodes('/p') ItemResponseData(p)
cross apply ItemResponseData.nodes('/p/s/c/i') c(i)

where KeyCode in ('UANNKY01','5N86RD01','7SGKLF01','4WJDP901','GVN7UN01','EE5EV401','6XPWYS01','3N7L8H01','R96KLW01','T7QGDY01','4WCXAX01','5XB6ZW01','9GCPDD01','3WM2VN01','HW8BYJ01','LNWZSA01','69NZ6U01','2LECST01','L4K8ZW01','A4548E01','BMQKBM01','5YU58201','SYJQH601','6E67TK01')
and ItemName = 'Teens_W_Ver_3_Task_4_WIP'
	--and WAREHOUSE_ExamSessionTable_ShrededItems.ItemRef = ItemResponseData.p.value('@id','nvarchar(max)')