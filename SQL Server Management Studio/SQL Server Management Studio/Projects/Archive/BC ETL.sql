SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

WITH cte AS (
 SELECT
  ROW_NUMBER() OVER(PARTITION BY UserId ORDER BY ID DESC) N
  ,WUT.UserId CandidateKey
  ,WUT.[ID] LatestWHCandidateKey
  ,WUT.[version] [Version]
  ,WUT.[Forename]
  ,WUT.[Surname]
  ,WUT.[EthnicOrigin] EthnicOrigin
  ,WUT.[DOB]
  ,WUT.[CandidateRef]
  ,CONVERT(nvarchar(10),'') ULN
  ,WUT.[Middlename]
  ,WUT.[Gender]
  ,CONVERT(NVARCHAR(MAX),WUT.[SpecialRequirements]) SpecialRequirements
  ,WUT.[AddressLine1]
  ,WUT.[AddressLine2]
  ,WUT.[Town]
  ,WUT.[County]
  ,WUT.[Country]
  ,WUT.[PostCode]
  ,WUT.[Telephone]
  ,CONVERT(NVARCHAR(100),WUT.[Email]) Email
  ,CONVERT(BIT, 0) IsExaminer
  ,UserName
 FROM [dbo].[WAREHOUSE_UserTable] WUT
 WHERE ID > 0
  AND ID <= 2147483647
)

SELECT * FROM 
cte
WHERE N=1


select candidateKey
from UAT_BC2_SurpassDataWarehouse..ETL_factExamSessions
where candidateKey NOT IN (select candidateKey from UAT_BC2_SurpassDataWarehouse..DimCandidate)



