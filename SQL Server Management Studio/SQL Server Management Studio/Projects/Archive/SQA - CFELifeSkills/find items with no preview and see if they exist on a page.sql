USE SQA_CPProjectAdmin_DI

DECLARE @mySharedLibs TABLE(projectID INT, structure XML)

INSERT INTO @mySharedLibs
SELECT projectID, CONVERT(XML, structureXML)
FROM SharedLibraryTable;

WITH mySharedLibItems AS (
SELECT projectID, t.p.value('data(@id)', 'int') sharedLibID, t.p.value('data(@name)', 'NVARCHAR(50)') name
FROM @mySharedLibs
CROSS APPLY structure.nodes('//item') t(p))

SELECT ProjectListTable.Name as ProjectName
	, mySharedLibItems.name as SharedLibName
	, ProjectManifestTable.ID as aliID
	, ProjectListTable.ID as projectID
	, DATALENGTH(ProjectManifestTable.Image) as 'ImageLength'
	, Items.ID
FROM mySharedLibItems
INNER JOIN ProjectListTable
ON ProjectListTable.ID = mySharedLibItems.projectID
LEFT JOIN ProjectManifestTable
ON ProjectManifestTable.ID = sharedLibID AND ProjectManifestTable.ProjectId = mySharedLibItems.projectID
LEFT JOIN 
(
      SELECT ID, ali
      FROM ItemCustomQuestionTable
      UNION
      SELECT ID, ali
      FROM ItemGraphicTable
      UNION
      SELECT ID, ali
      FROM ItemVideoTable
      UNION
      SELECT ID, ali
      FROM ItemHotSpotTable
) Items
ON 
      Items.ID LIKE CONVERT(NVARCHAR(10), ProjectManifestTable.ProjectId) + 'P%' 
      AND 
      (
            CONVERT(NVARCHAR(50), sharedLibID) = ali
            OR
            SUBSTRING(mySharedLibItems.name, 0, LEN(mySharedLibItems.name) - 3) = ali
      )
WHERE DATALENGTH(ProjectManifestTable.Image) IS NULL and Items.id IS NULL and ProjectManifestTable.ProjectID = 942
ORDER BY ProjectListTable.Name

/*

select id, name, location from ProjectManifestTable where projectID = 942 and DATALENGTH(Image) IS NULL and location not like '%background%'

delete from ProjectManifestTable 
where projectID = 942 and DATALENGTH(Image) IS NULL and location not like '%background%'

*/


--select id, name
--from ProjectManifestTable
--where projectid = 942;

--SELECT SharedLibraryTable.ProjectID
--	,CAST(SharedLibraryTable.structureXML AS XML) AS [StructureXML]
--FROM dbo.SharedLibraryTable
--where projectid = 942;