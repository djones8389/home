IF OBJECT_ID('tempdb..#changesmade') IS NOT NULL DROP TABLE #changesmade

create table #changesmade
(
	statement nvarchar(MAX)
	, userID int
	, qualID int
	, CQN nvarchar(10)
)

DECLARE @QualTable TABLE (QualID INT)
DECLARE @UserTable TABLE (UserID INT)

INSERT @QualTable
select ID
from IB3QualificationLookup
--where QualificationName in ()

INSERT @UserTable
select ID
From UserTable
--Where ID IN (1)

BEGIN TRY
    BEGIN TRANSACTION 
		merge into UserQualificationsTable as TGT
		using (SELECT UserID, QualID
				FROM @UserTable
				CROSS APPLY @QualTable
				) as SRC
			on TGT.QualificationID = SRC.QualID
				and TGT.Userid = SRC.UserID
		WHEN NOT MATCHED BY TARGET THEN
		insert(UserID, QualificationID)
		values(SRC.UserID, SRC.QualID)
		
		OUTPUT $action, inserted.* INTO #changesmade;
		
    COMMIT TRANSACTION
END TRY

BEGIN CATCH
    SELECT 
        ERROR_NUMBER() AS ErrorNumber
        ,ERROR_SEVERITY() AS ErrorSeverity
        ,ERROR_STATE() AS ErrorState
        ,ERROR_PROCEDURE() AS ErrorProcedure
        ,ERROR_LINE() AS ErrorLine
        ,ERROR_MESSAGE() AS ErrorMessage;

    IF @@TRANCOUNT > 0
        ROLLBACK TRANSACTION;
END CATCH;
IF @@TRANCOUNT > 0
    COMMIT TRANSACTION;
GO





--SELECT * FROM #changesmade




--SELECT * FROM UserQualificationsTable where UserID = 1


--DECLARE @UsersAndQuals TABLE(UserID INT, QualID INT)

--INSERT @UsersAndQuals(QualID, UserID)
--select ID, 1
--from IB3QualificationLookup


--select
-- 'INSERT INTO UserQualificationsTable(UserID, QualificationID)
--	VALUES ('+ convert(nvarchar(max),A.UserID) + ',' + convert(nvarchar(max),QualID) + '' + ')
--	'
--FROM @UserTable A
--Cross Apply @QualTable B
--LEFT JOIN UserQualificationsTable as UQT
--on UQT.UserID = A.UserID
--	and UQT.QualificationID = B.QualID
--WHERE UQT.UserID IS NULL
--	and UQT.QualificationID IS NULL








/*
select 
 UserQualificationsTable.UserID
	, UserQualificationsTable.QualificationID
	, IB3QualificationLookup.ID
	, IB3QualificationLookup.QualificationName
	from IB3QualificationLookup
cross apply  UserQualificationsTable
where --UserQualificationsTable.QualificationID < 78
	UserID = 1


--Insert Qual ID's 1-77 into [IB3QualificationLookup] for userID = 1

select 
 	 'INSERT INTO UserQualificationsTable VALUES(' + UserID + ' '
	from IB3QualificationLookup
cross apply  UserQualificationsTable
where --UserQualificationsTable.QualificationID < 78
	UserID = 1


select  * 
from UserQualificationsTable
where UserID = 1
	and UserQualificationsTable.QualificationID < 78
*/