/****** Script for SelectTopNRows command from SSMS  ******/
SELECT distinct CentreID
  FROM [AAT_SecureAssess].[dbo].[CentreQualificationsTable]
    where QualificationID != 15
  
  --MERGE--
  
  
create table #DJTable
(

	--id int identity(1,1),
	CentreID int,
	QualificationID int
)


with cte  as (

SELECT distinct CentreID as 'CentreID'
	--, '15' as '15'
  FROM [AAT_SecureAssess].[dbo].[CentreQualificationsTable]
    where QualificationID != 15
)

--select * from cte

insert into #DJTable(CentreID, QualificationID)

SELECT distinct cte.CentreID
		, '15'

  from cte
  
  right join CentreQualificationsTable
  on cte.CentreID = [AAT_SecureAssess].[dbo].[CentreQualificationsTable].CentreID
  
    
--select *   FROM [AAT_SecureAssess].[dbo].[CentreQualificationsTable]
--    where CentreID = 62 and QualificationID != 15
--		order by QualificationID
    
select * from #DJTable

--SELECT distinct CentreID
--  FROM [AAT_SecureAssess].[dbo].[CentreQualificationsTable]
--    where QualificationID != 15

--drop table #DJTable

begin tran

merge into CentreQualificationsTable as TGT
using #DJTable as SRC
	on 1 = 2 -- TGT.CentreID = SRC.CentreID
when not matched then 
	insert (CentreID, QualificationID)
	Values(SRC.CentreID,  SRC.QualificationID);
	
rollback
	

