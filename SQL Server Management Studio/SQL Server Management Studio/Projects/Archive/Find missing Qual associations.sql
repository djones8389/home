USE Rcpch_SurpassManagement

SELECT  ItemBankSubjectId, UserCentreSubjectPermissions.CentreId as 'EditionsCentreID' , SecureAssessCentreId as 'SACentreID'
	, QualificationName
  FROM UserCentreSubjectPermissions
  
  inner join Centres
  on Centres.Id = UserCentreSubjectPermissions.CentreId
  
  inner join Subjects
  on Subjects.Id = UserCentreSubjectPermissions.SubjectId
  
  inner join Rcpch_ItemBank..QualificationTable
  on Rcpch_ItemBank..QualificationTable.ID = Subjects.ItemBankSubjectId
  
  inner join Rcpch_SecureAssess..CentreQualificationsTable
  on Rcpch_SecureAssess..CentreQualificationsTable.CentreID =  Centres.SecureAssessCentreId
  
  where Status = 0 and IsValid = 1 
  and centres.Name like 'Aberdeen%'
  
  
  
use Rcpch_SecureAssess
  
  
  select * from CentreQualificationsTable
  
  inner join CentreTable
  on CentreTable.ID = CentreQualificationsTable.CentreID
  
  inner join Rcpch_ItemBank..QualificationTable
  on Rcpch_ItemBank..QualificationTable.ID = CentreQualificationsTable.QualificationID
  
  where CentreTable.CentreName like 'Aberdeen%'
  
  
  
  
  
  
  
  
  
  
  /*
SELECT  distinct CentreId, SubjectId
  FROM UserCentreSubjectPermissions
  
  inner join Centres
  on Centres.Id = UserCentreSubjectPermissions.CentreId
  
  inner join Subjects
  on Subjects.Id = UserCentreSubjectPermissions.SubjectId
  
  
  
  
  
  
  
  
  SELECT distinct SecureAssessCentreId as 'SACentreID', Rcpch_SecureAssess..CentreQualificationsTable.QualificationID
  FROM UserCentreSubjectPermissions
  
  inner join Centres
  on Centres.Id = UserCentreSubjectPermissions.CentreId
  
  inner join Subjects
  on Subjects.Id = UserCentreSubjectPermissions.SubjectId
  
  inner join Rcpch_ItemBank..QualificationTable
  on Rcpch_ItemBank..QualificationTable.ID = Subjects.ItemBankSubjectId
  
  inner join Rcpch_SecureAssess..CentreQualificationsTable
  on Rcpch_SecureAssess..CentreQualificationsTable.CentreID =  Centres.SecureAssessCentreId
  */