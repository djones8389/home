DECLARE @@BigTable TABLE (

		Schema_DatabaseName nvarchar(MAX)
	  , SchemaName nvarchar(50)
	  , XMLSchemaCollection_DatabaseName nvarchar(MAX)
	  , XMLSchemaName nvarchar(50)
	  , Role_DatabaseName nvarchar(MAX)
	  , RoleName nvarchar(50)
)

INSERT @@BigTable(Schema_DatabaseName, SchemaName)
exec sp_MSforeachdb '

use [?];

select ''?''
	, Name 
from sys.schemas 
where 
	''?'' not in (''msdb'',''master'', ''SurpassReportServer'', ''SurpassReportServerTempDB'')
and name not in 
	(select name 
		from sysusers 
			where issqlrole = 1
			)
and name not in (''guest'',''INFORMATION_SCHEMA'',''sys'')
		and ''?'' in (select name from sys.databases where state_desc = ''ONLINE'')
'  

/*Write DB names and DB XMLSchemaCollections into a Table Variable*/

INSERT @@BigTable(XMLSchemaCollection_DatabaseName, XMLSchemaName)
EXEC sp_MSforeachdb '

	use [?];
	
	select ''?'', name 	
	from sys.xml_schema_collections
	
	where ''?'' not in (''master'', ''msdb'', ''tempdb'', ''model'')
	and name != ''sys''

'

INSERT @@BigTable(Role_DatabaseName, RoleName)
EXEC sp_MSforeachdb  '
USE [?];

--insert into #DatabaseRoleMemberShip
SELECT  ''?'', roleprinc.[name]
FROM sys.database_role_members members

RIGHT JOIN sys.database_principals roleprinc
ON roleprinc.[principal_id] = members.[role_principal_id]

where is_fixed_role  = 0
	and type = ''R''
	and name != ''public''
	and name in (''btl_developer_access'',''btl_developer_full_access'')
	--and roleprinc.[name] in (
		
	--	--Find user Associations

	--	SELECT  roleprinc.[name]
	--	FROM sys.database_role_members members

	--	INNER JOIN sys.database_principals roleprinc
	--	ON roleprinc.[principal_id] = members.[role_principal_id]

	--	where is_fixed_role  = 0

	--)
'

-- @Schema_DatabaseName nvarchar(100), @SchemaName nvarchar(100), 


select distinct Role_DatabaseName
			, RoleName
	from @@BigTable
	
select distinct Schema_DatabaseName
						, SchemaName
		from @@BigTable

--select distinct Role_DatabaseName
--		, RoleName
--from @@BigTable
--where Role_DatabaseName is  null

--SELECT * FROM @@BigTable

DECLARE @UserName nvarchar(100) = 'DJones'
DECLARE @DBName nvarchar(100) = 'AdventureWorks2008R2'
DECLARE @ReadRole nvarchar(100) = 'btl_developer_access'
DECLARE @WriteRole nvarchar(100) = 'btl_developer_full_access'

/*Check to see if Role exists.  If so, simply assign user to it*/

	DECLARE AssignROLES CURSOR FOR

	select distinct Role_DatabaseName
			, RoleName
	from @@BigTable
	
	where Role_DatabaseName is not null


	DECLARE @Role_DatabaseName nvarchar(100), @RoleName nvarchar(100)

	OPEN AssignROLES

	FETCH NEXT FROM AssignROLES INTO @Role_DatabaseName, @RoleName

	WHILE @@FETCH_STATUS = 0

	BEGIN
	
		SELECT 'use ' +  Role_DatabaseName + '; ' + 'EXEC sp_addrolemember ' + '''' + @ReadRole + ''''  + ',' + '''' + @UserName + ''''
		from @@BigTable
		where Role_DatabaseName = @DBName 
					
	FETCH NEXT FROM AssignROLES INTO @Role_DatabaseName, @RoleName

	END
	CLOSE AssignROLES
	DEALLOCATE AssignROLES



--DECLARE CreateROLES CURSOR FOR

--select distinct Role_DatabaseName
--		, RoleName
--from @@BigTable
--where Role_DatabaseName is null


--DECLARE @Role_DatabaseName nvarchar(100), @RoleName nvarchar(100)

--OPEN CreateROLES

--FETCH NEXT FROM CreateROLES INTO @Role_DatabaseName, @RoleName

--WHILE @@FETCH_STATUS = 0

--BEGIN
--IF LEN(@DBNAME) > 1
--BEGIN
--	SELECT 'use ' +  name + '; ' + 'CREATE ROLE ' + @ReadRole +';'
--	from sys.databases 
--	where name = @DBName
--END
--ELSE
--BEGIN
--	SELECT 'use ' +  name + '; ' + 'CREATE ROLE ' + @ReadRole +';'
--	from sys.databases 
--	where name not in ('master', 'msdb', 'tempdb', 'model')
--	and state_desc = 'ONLINE'
--END

--FETCH NEXT FROM CreateROLES INTO @Role_DatabaseName, @RoleName



--		DECLARE BiggerCursor CURSOR FOR
--		select distinct SchemaName
--		from @@BigTable
--		where Schema_DatabaseName is not null

--		DECLARE @SchemaName nvarchar(100)

--		OPEN BiggerCursor

--		FETCH NEXT FROM BiggerCursor INTO @SchemaName

--		WHILE @@FETCH_STATUS = 0
			
			
--		BEGIN

--			--SELECT 'use ' +  name + '; ' + 'CREATE ROLE ' + @ReadRole +';'
--			--from sys.databases 
--			--where name = @DBName
			
--		--SELECT 'use ' +  @DBName + '; ' + 'CREATE ROLE ' + @ReadRole +';'

--		FETCH NEXT FROM BiggerCursor INTO @SchemaName

--		END
--		CLOSE BiggerCursor
--		DEALLOCATE BiggerCursor	




--END
--CLOSE CreateROLES
--DEALLOCATE CreateROLES
























/*

	( 	
				
	SELECT @CreateAndGrantRole += CHAR(13) +
	
	
	'use [' + A.name + ']; ' 	+
	
	
   Case When @AccessRequired = 'Read' and @ReadRole IN (select C.RoleName from @@BigTable where C.Role_DatabaseName = @DatabaseName)
			then 
			
			' EXEC sp_addrolemember '''  + @ReadRole + ''', ''' + @Username + '''; '
			
		When @AccessRequired = 'Read' and @ReadRole NOT IN (select C.RoleName from @@BigTable where C.Role_DatabaseName = @DatabaseName)
			then
			'CREATE ROLE '  + @ReadRole +  ';'  
			
			--+ ' GRANT SELECT, VIEW DEFINITION ON SCHEMA::' + B.SchemaName + ' TO ' + @ReadRole +';' 				

			--+ ' EXEC sp_addrolemember '''  + @ReadRole + ''', ''' + @Username + '''; '

		
		When @AccessRequired = 'Write'  and @WriteRole IN (select C.RoleName from @@BigTable where C.Role_DatabaseName = @DatabaseName)
			then 
		 
			' EXEC sp_addrolemember ''' +	 @WriteRole + ''', ''' + @Username + '''; '
		
		 When @AccessRequired = 'Write'  and @WriteRole NOT IN (select C.RoleName from @@BigTable where C.Role_DatabaseName = @DatabaseName)
			then	
		 
				'CREATE ROLE '  + @WriteRole +  ';'  
	
				--+ ' GRANT ALTER, DELETE, EXECUTE, INSERT, SELECT, UPDATE, VIEW DEFINITION ON SCHEMA::' +B.SchemaName + ' TO ' + @WriteRole +';' 		   
						
				--+ ' EXEC sp_addrolemember ''' +	 @WriteRole + ''', ''' + @Username + '''; '
									   
		end

	from sys.databases A
	
	LEFT join @@BigTable  B
	on B.Schema_DatabaseName COLLATE Latin1_General_CI_AS = a.name
	LEFT join @@BigTable C
	on C.Role_DatabaseName COLLATE Latin1_General_CI_AS = A.name
		
	where  a.name COLLATE Latin1_General_CI_AS  = @DatabaseName
	)
	
*/