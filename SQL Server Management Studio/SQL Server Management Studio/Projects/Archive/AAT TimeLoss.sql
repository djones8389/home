select top 10

StructureXML
, a.b.value('(.)[1]','int') as offlineMode
, ScheduledStartDateTime
, ScheduledEndDateTime
, clientInformation
, downloadInformation
, west.PreviousExamState
FROM WAREHOUSE_ExamSessionTable as WEST

  Inner Join WAREHOUSE_ScheduledExamsTable as WSCET
  on WSCET.ID = WEST.WAREHOUSEScheduledExamID
  
  Inner Join WAREHOUSE_UserTable as WUT
  on WUT.ID = WEST.WAREHOUSEUserID

  Inner Join WAREHOUSE_CentreTable as WCT
  on WCT.ID = WSCET.WAREHOUSECentreID

cross apply structurexml.nodes ('/assessmentDetails/offlineMode') a(b)

where clientInformation is not null 
	and downloadInformation is not null
	and west.PreviousExamState != 10
order by WEST.ID DESC