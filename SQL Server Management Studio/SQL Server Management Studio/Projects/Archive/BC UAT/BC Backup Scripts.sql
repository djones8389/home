--DECLARE @Location nvarchar(100) = '\\524778-sql\DATASHARE'

--SELECT N'BACKUP DATABASE ['+name+N'] TO DISK = N''' +  @Location + '\' + 'ICAEW.ALL' + CONVERT(VARCHAR(10), GETDATE(), 102) + '.bak''WITH NAME = N'''+name+N'- Database Backup'', COPY_ONLY, COMPRESSION, NOFORMAT, NOINIT, SKIP, COMPRESSION, NOREWIND, NOUNLOAD, STATS = 10;'  
--FROM sys.databases 
--where name like 'ICAEW%'


DECLARE @BackupPath nvarchar(max) = '''G:\UAT_DATABASE_BACKUPS\UAT.BC.Pre.Migration.bak'''
--DECLARE @RestorePath nvarchar(max) = '''S:\DATA\'

DECLARE @Table table
        (BackupName  nvarchar(128),
        BackupDescription  nvarchar(255) ,
        BackupType  smallint ,
        ExpirationDate  datetime ,
        Compressed  bit ,
        Position  smallint ,
        DeviceType  tinyint ,
        UserName  nvarchar(128) ,
        ServerName  nvarchar(128) ,
        DatabaseName  nvarchar(128) ,
        DatabaseVersion  int ,
        DatabaseCreationDate  datetime ,
        BackupSize  numeric(20,0) ,
        FirstLSN  numeric(25,0) ,
        LastLSN  numeric(25,0) ,
        CheckpointLSN  numeric(25,0) ,
        DatabaseBackupLSN  numeric(25,0) ,
        BackupStartDate  datetime ,
        BackupFinishDate  datetime ,
        SortOrder  smallint ,
        CodePage  smallint ,
        UnicodeLocaleId  int ,
        UnicodeComparisonStyle  int ,
        CompatibilityLevel  tinyint ,
        SoftwareVendorId  int ,
        SoftwareVersionMajor  int ,
        SoftwareVersionMinor  int ,
        SoftwareVersionBuild  int ,
        MachineName  nvarchar(128) ,
        Flags  int ,
        BindingID  uniqueidentifier ,
        RecoveryForkID  uniqueidentifier ,
        Collation  nvarchar(128) ,
        FamilyGUID  uniqueidentifier ,
        HasBulkLoggedData  bit ,
        IsSnapshot  bit ,
        IsReadOnly  bit ,
        IsSingleUser  bit ,
        HasBackupChecksums  bit ,
        IsDamaged  bit ,
        BeginsLogChain  bit ,
        HasIncompleteMetaData  bit ,
        IsForceOffline  bit ,
        IsCopyOnly  bit ,
        FirstRecoveryForkID  uniqueidentifier ,
        ForkPointLSN  numeric(25,0) NULL,
        RecoveryModel  nvarchar(60) ,
        DifferentialBaseLSN  numeric(25,0) NULL,
        DifferentialBaseGUID  uniqueidentifier ,
        BackupTypeDescription  nvarchar(60) ,
        BackupSetGUID  uniqueidentifier NULL,
        CompressedBackupSize bigint)

    INSERT INTO @Table EXEC('RESTORE HEADERONLY FROM DISK = '  + @BackupPath)

SELECT BackupName, CompressedBackupSize FROM @Table
