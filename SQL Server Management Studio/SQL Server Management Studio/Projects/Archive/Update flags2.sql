begin tran

declare @NewValue int = 0
declare @myDecimal decimal(10,3) = '0.000'
declare @esid int = 107839

select StructureXML, resultData, resultDataFull from WAREHOUSE_ExamSessionTable where ID = @esid

--WEST.ResultData--

	--SectionLevel--
	/*markerUserMark + actualUserMark +  userMark + viewingTime + userAttempted*/

	update WAREHOUSE_ExamSessionTable
	set ResultData.modify('replace value of (/exam/@userMark)[1] with sql:variable("@NewValue") ')    
	where id = @esid

	delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid

	update WAREHOUSE_ExamSessionTable
	set ResultData.modify('replace value of (/exam/@userPercentage)[1] with sql:variable("@NewValue") ')    
	where id = @esid
	
	delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid

	--ItemLevel--
	
	update WAREHOUSE_ExamSessionTable
	set ResultData.modify('replace value of (/exam/section/item/@markerUserMark)[1] with sql:variable("@NewValue") ')    
	where id = @esid
	
	delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
	
	update WAREHOUSE_ExamSessionTable
	set ResultData.modify('replace value of (/exam/section/item/@actualUserMark)[1] with sql:variable("@NewValue") ')    
	where id = @esid
	
	delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
	
	update WAREHOUSE_ExamSessionTable
	set ResultData.modify('replace value of (/exam/section/item/@userMark)[1] with sql:variable("@NewValue") ')    
	where id = @esid
	
	delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
	
	update WAREHOUSE_ExamSessionTable
	set ResultData.modify('replace value of (/exam/section/item/@viewingTime)[1] with sql:variable("@NewValue") ')    
	where id = @esid
	
	delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
	
	update WAREHOUSE_ExamSessionTable
	set ResultData.modify('replace value of (/exam/section/item/@userAttempted)[1] with sql:variable("@NewValue") ')    
	where id = @esid

--WEST.ResultDataFull--

	--SectionLevel--

	update WAREHOUSE_ExamSessionTable
	set resultDataFull.modify('replace value of (/assessmentDetails/assessment/@userMark)[1] with sql:variable("@NewValue") ')    
	where id = @esid
	
	delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
	
	update WAREHOUSE_ExamSessionTable
	set resultDataFull.modify('replace value of (/assessmentDetails/assessment/@userPercentage)[1] with sql:variable("@NewValue") ')    
	where id = @esid
	
	delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
	
	update WAREHOUSE_ExamSessionTable
	set resultDataFull.modify('replace value of (/assessmentDetails/assessment/section/@userMark)[1] with sql:variable("@NewValue") ')    
	where id = @esid
	
	delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid

	update WAREHOUSE_ExamSessionTable
	set resultDataFull.modify('replace value of (/assessmentDetails/assessment/section/@userPercentage)[1] with sql:variable("@NewValue") ')    
	where id = @esid

	--ItemLevel--
	--UserMark--
	update WAREHOUSE_ExamSessionTable
	set resultDataFull.modify('replace value of (/assessmentDetails/assessment/section/item/@userMark)[1] with sql:variable("@NewValue") ')    
	where id = @esid
	
	update WAREHOUSE_ExamSessionTable
	set resultDataFull.modify('replace value of (/assessmentDetails/assessment/section/item/@userMark)[2] with sql:variable("@NewValue") ')    
	where id = @esid

	update WAREHOUSE_ExamSessionTable
	set resultDataFull.modify('replace value of (/assessmentDetails/assessment/section/item/@userMark)[3] with sql:variable("@NewValue") ')    
	where id = @esid
	
	update WAREHOUSE_ExamSessionTable
	set resultDataFull.modify('replace value of (/assessmentDetails/assessment/section/item/@userMark)[4] with sql:variable("@NewValue") ')    
	where id = @esid

	update WAREHOUSE_ExamSessionTable
	set resultDataFull.modify('replace value of (/assessmentDetails/assessment/section/item/@userMark)[5] with sql:variable("@NewValue") ')    
	where id = @esid
	
	update WAREHOUSE_ExamSessionTable
	set resultDataFull.modify('replace value of (/assessmentDetails/assessment/section/item/@userMark)[6] with sql:variable("@NewValue") ')    
	where id = @esid

	update WAREHOUSE_ExamSessionTable
	set resultDataFull.modify('replace value of (/assessmentDetails/assessment/section/item/@userMark)[7] with sql:variable("@NewValue") ')    
	where id = @esid
	
	update WAREHOUSE_ExamSessionTable
	set resultDataFull.modify('replace value of (/assessmentDetails/assessment/section/item/@userMark)[8] with sql:variable("@NewValue") ')    
	where id = @esid

	update WAREHOUSE_ExamSessionTable
	set resultDataFull.modify('replace value of (/assessmentDetails/assessment/section/item/@userMark)[9] with sql:variable("@NewValue") ')    
	where id = @esid
	
	update WAREHOUSE_ExamSessionTable
	set resultDataFull.modify('replace value of (/assessmentDetails/assessment/section/item/@userMark)[10] with sql:variable("@NewValue") ')    
	where id = @esid

	update WAREHOUSE_ExamSessionTable
	set resultDataFull.modify('replace value of (/assessmentDetails/assessment/section/item/@userAttempted)[1] with sql:variable("@NewValue") ')    
	where id = @esid
	
	update WAREHOUSE_ExamSessionTable
	set resultDataFull.modify('replace value of (/assessmentDetails/assessment/section/item/@userAttempted)[2] with sql:variable("@NewValue") ')    
	where id = @esid
	
	update WAREHOUSE_ExamSessionTable
	set resultDataFull.modify('replace value of (/assessmentDetails/assessment/section/item/@userAttempted)[3] with sql:variable("@NewValue") ')    
	where id = @esid
	
	update WAREHOUSE_ExamSessionTable
	set resultDataFull.modify('replace value of (/assessmentDetails/assessment/section/item/@userAttempted)[4] with sql:variable("@NewValue") ')    
	where id = @esid
	
	update WAREHOUSE_ExamSessionTable
	set resultDataFull.modify('replace value of (/assessmentDetails/assessment/section/item/@userAttempted)[5] with sql:variable("@NewValue") ')    
	where id = @esid
	
	update WAREHOUSE_ExamSessionTable
	set resultDataFull.modify('replace value of (/assessmentDetails/assessment/section/item/@userAttempted)[6] with sql:variable("@NewValue") ')    
	where id = @esid
	
	update WAREHOUSE_ExamSessionTable
	set resultDataFull.modify('replace value of (/assessmentDetails/assessment/section/item/@userAttempted)[7] with sql:variable("@NewValue") ')    
	where id = @esid
	
	update WAREHOUSE_ExamSessionTable
	set resultDataFull.modify('replace value of (/assessmentDetails/assessment/section/item/@userAttempted)[8] with sql:variable("@NewValue") ')    
	where id = @esid
	
	update WAREHOUSE_ExamSessionTable
	set resultDataFull.modify('replace value of (/assessmentDetails/assessment/section/item/@userAttempted)[9] with sql:variable("@NewValue") ')    
	where id = @esid
	
	update WAREHOUSE_ExamSessionTable
	set resultDataFull.modify('replace value of (/assessmentDetails/assessment/section/item/@userAttempted)[10] with sql:variable("@NewValue") ')    
	where id = @esid

	update WAREHOUSE_ExamSessionTable
	set resultDataFull.modify('replace value of (/assessmentDetails/assessment/section/item/@viewingTime)[1] with sql:variable("@NewValue") ')    
	where id = @esid
	
	update WAREHOUSE_ExamSessionTable
	set resultDataFull.modify('replace value of (/assessmentDetails/assessment/section/item/@viewingTime)[2] with sql:variable("@NewValue") ')    
	where id = @esid
	
	update WAREHOUSE_ExamSessionTable
	set resultDataFull.modify('replace value of (/assessmentDetails/assessment/section/item/@viewingTime)[3] with sql:variable("@NewValue") ')    
	where id = @esid

	update WAREHOUSE_ExamSessionTable
	set resultDataFull.modify('replace value of (/assessmentDetails/assessment/section/item/@viewingTime)[4] with sql:variable("@NewValue") ')    
	where id = @esid
	
	update WAREHOUSE_ExamSessionTable
	set resultDataFull.modify('replace value of (/assessmentDetails/assessment/section/item/@viewingTime)[5] with sql:variable("@NewValue") ')    
	where id = @esid
	
	update WAREHOUSE_ExamSessionTable
	set resultDataFull.modify('replace value of (/assessmentDetails/assessment/section/item/@viewingTime)[6] with sql:variable("@NewValue") ')    
	where id = @esid
	
		update WAREHOUSE_ExamSessionTable
	set resultDataFull.modify('replace value of (/assessmentDetails/assessment/section/item/@viewingTime)[7] with sql:variable("@NewValue") ')    
	where id = @esid
	
	update WAREHOUSE_ExamSessionTable
	set resultDataFull.modify('replace value of (/assessmentDetails/assessment/section/item/@viewingTime)[8] with sql:variable("@NewValue") ')    
	where id = @esid
	
	update WAREHOUSE_ExamSessionTable
	set resultDataFull.modify('replace value of (/assessmentDetails/assessment/section/item/@viewingTime)[9] with sql:variable("@NewValue") ')    
	where id = @esid
	
	update WAREHOUSE_ExamSessionTable
	set resultDataFull.modify('replace value of (/assessmentDetails/assessment/section/item/@viewingTime)[10] with sql:variable("@NewValue") ')    
	where id = @esid
	
select StructureXML, resultData, resultDataFull from WAREHOUSE_ExamSessionTable where ID = @esid

Rollback












--WEST.StructureXML--

	--update WAREHOUSE_ExamSessionTable
	--set StructureXML.modify('replace value of (//item/@totalMark)[1] with sql:variable("@myDecimal") ')    
	--where id = @esid
	
	--update WAREHOUSE_ExamSessionTable
	--set StructureXML.modify('replace value of (//item/@userAttempted)[1] with sql:variable("@NewValue") ')    
	--where id = @esid
	
	--update WAREHOUSE_ExamSessionTable
	--set StructureXML.modify('replace value of (//item/@markerUserMark)[1] with sql:variable("@NewValue") ')    
	--where id = @esid

	--update WAREHOUSE_ExamSessionTable
	--set StructureXML.modify('replace value of (//item/@viewingTime)[1] with sql:variable("@NewValue") ')    
	--where id = @esid