--USE master
--GO

--SET NOCOUNT ON

--DECLARE @Kb float
--DECLARE @PageSize float
--DECLARE @SQL varchar(2000)

--SELECT @Kb = 1024.0
--SELECT @PageSize=v.low/@Kb FROM master..spt_values v WHERE v.number=1 AND v.type='E'


--IF OBJECT_ID('tempdb.dbo.#FileSize') IS NOT NULL DROP TABLE #FileSize

--CREATE TABLE #FileSize (
--	DatabaseName sysname,
--	FileName sysname,
--	FileSize int,
--	FileGroupName sysname,
--	LogicalName sysname
--)

--IF OBJECT_ID('tempdb.dbo.#FileStats') IS NOT NULL  DROP TABLE #FileStats

--CREATE TABLE #FileStats (
--	FileID int,
--	FileGroup int,
--	TotalExtents int,
--	UsedExtents int,
--	LogicalName sysname,
--	FileName nchar(520)
--)

--IF OBJECT_ID('tempdb.dbo.#LogSpace') IS NOT NULL DROP TABLE #LogSpace

--CREATE TABLE #LogSpace (
--	DatabaseName sysname,
--	LogSize float,
--	SpaceUsedPercent float,
--	Status bit
--)

--INSERT #LogSpace EXEC ('DBCC sqlperf(logspace)')

--DECLARE @DatabaseName sysname

--DECLARE cur_Databases CURSOR FAST_FORWARD FOR
--	SELECT DatabaseName = [name] FROM dbo.sysdatabases ORDER BY DatabaseName
--OPEN cur_Databases
--FETCH NEXT FROM cur_Databases INTO @DatabaseName
--WHILE @@FETCH_STATUS = 0
--  BEGIN
--	SET @SQL = '
--USE [' + @DatabaseName + '];
--DBCC showfilestats;
--INSERT #FileSize (DatabaseName, FileName, FileSize, FileGroupName, LogicalName)
--SELECT ''' +@DatabaseName + ''', filename, size, ISNULL(FILEGROUP_NAME(groupid),''LOG''), [name]
--FROM dbo.sysfiles sf;
--'

--	INSERT #FileStats EXECUTE (@SQL)
--	FETCH NEXT FROM cur_Databases INTO @DatabaseName
--  END

--CLOSE cur_Databases
--DEALLOCATE cur_Databases

--SELECT
--	DatabaseName = fsi.DatabaseName,
--	FileGroupName = fsi.FileGroupName,
--	LogicalName = RTRIM(fsi.LogicalName),
--	FileName = RTRIM(fsi.FileName),
--	FileSize = CAST(fsi.FileSize*@PageSize/@Kb as decimal(15,2)),
--	UsedSpace = CAST(ISNULL((fs.UsedExtents*@PageSize*8.0/@Kb), fsi.FileSize*@PageSize/@Kb * ls.SpaceUsedPercent/100.0) as decimal(15,2)),
--	FreeSpace = CAST(ISNULL(((fsi.FileSize - UsedExtents*8.0)*@PageSize/@Kb), (100.0-ls.SpaceUsedPercent)/100.0 * fsi.FileSize*@PageSize/@Kb) as decimal(15,2)),
--	[FreeSpace %] = CAST(ISNULL(((fsi.FileSize - UsedExtents*8.0) / fsi.FileSize * 100.0), 100-ls.SpaceUsedPercent) as decimal(15,2))
--FROM #FileSize fsi
--LEFT JOIN #FileStats fs
--	ON fs.FileName = fsi.FileName
--LEFT JOIN #LogSpace ls
--	ON ls.DatabaseName = fsi.DatabaseName
--ORDER BY 1,3



--DBCC showfilestats;
--select NAME from  dbo.sysfiles



--select 'USE [' + Name + ']; select NAME, fileID, fileName from  dbo.sysfiles;' 
--from sys.databases


--USE [MarketDev]; select NAME, fileID, fileName, * from  dbo.sysfiles;


/*
EXEC sp_attach_db @dbname = N'AdventureWorks2012', 
    @filename1 = N'C:\Program Files\Microsoft SQL Server\MSSQL13.MSSQLSERVER\MSSQL\Data\AdventureWorks2012_Data.mdf', 
    @filename2 = N'C:\Program Files\Microsoft SQL Server\MSSQL13.MSSQLSERVER\MSSQL\Data\AdventureWorks2012_log.ldf';
*/

DECLARE @DBLocation nvarchar(200) = 'S:\Data\'
DECLARE @LogLocation nvarchar(200) = 'L:\Logs\'

DECLARE @SQL NVARCHAR(MAX);

DECLARE @@DataHolder TABLE (DBName nvarchar(max), LogicalFileName nvarchar(max), fileID tinyint, CurrentLocation nvarchar(max))
INSERT @@DataHolder
exec sp_MSforeachdb '

use [?];

select ''?'' as DBName, NAME as LogicalFileName, fileID, fileName as CurrentLocation
from  dbo.sysfiles;

'

DECLARE @@DynamicSQLData TABLE (DBname nvarchar(200), MYSQL nvarchar(MAX))
DECLARE @@DynamicSQLLog TABLE (DBname nvarchar(200), MYSQL nvarchar(MAX))

DECLARE myDataCursor CURSOR FOR
SELECT DBname, LogicalFileName, fileID, CurrentLocation
from @@DataHolder
where fileID = 1

OPEN myDataCursor;

DECLARE @DBName nvarchar(max), @LogicalFileName nvarchar(max), @fileID tinyint, @CurrentLocation nvarchar(max)

FETCH NEXT FROM myDataCursor INTO @DBName, @LogicalFileName, @fileID, @CurrentLocation

WHILE @@FETCH_STATUS = 0

BEGIN
	
	INSERT @@DynamicSQLData(DBname, MYSQL)
	SELECT @DBname, 'EXEC sp_attach_db @dbname = N''' + @DBName + ''',  @filename1 = N''' + @DBLocation + @LogicalFileName  + '.mdf'''

FETCH NEXT FROM myDataCursor INTO @DBName, @LogicalFileName, @fileID, @CurrentLocation

END

CLOSE myDataCursor
DEALLOCATE myDataCursor

DECLARE myLogCursor CURSOR FOR
SELECT DBname, LogicalFileName, fileID, CurrentLocation
from @@DataHolder
where fileID = 2

OPEN myLogCursor;

DECLARE @DBName1 nvarchar(max), @LogicalFileName1 nvarchar(max), @fileID1 tinyint, @CurrentLocation1 nvarchar(max)

FETCH NEXT FROM myLogCursor INTO @DBName1, @LogicalFileName1, @fileID1, @CurrentLocation1

WHILE @@FETCH_STATUS = 0

BEGIN
	
	INSERT @@DynamicSQLLog(DBname,MYSQL)
	SELECT @DBname1, ',  @filename2 = N''' + @DBLocation + @LogicalFileName1  + '.mdf'''

FETCH NEXT FROM myLogCursor INTO @DBName1, @LogicalFileName1, @fileID1, @CurrentLocation1

END

CLOSE myLogCursor
DEALLOCATE myLogCursor


SELECT A.MYSQL + B.MYSQL 
from @@DynamicSQLData  A

INNER JOIN @@DynamicSQLLog B
on A.DBname = B.DBname



--SELECT * from @@DynamicSQLData 
--SELECT * FROM @@DynamicSQLLog
--SELECT 'EXEC sp_attach_db @dbname = N''' + DBName + ''',  @filename1 = N''' + @DBLocation --+ CASE LogicalFileName when ''1'' then ''hi''  '
--FROM @@DataHolder




