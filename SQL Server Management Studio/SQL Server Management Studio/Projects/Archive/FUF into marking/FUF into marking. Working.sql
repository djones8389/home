USE UAT_EAL_SecureAssess

declare @Keycode nvarchar(10) = 'XDDLWTA5';
declare @resultData xml;
declare @resultDataFull xml;
declare @structureXML xml;

declare @ID int = (Select ID from ExamSessionTable where Keycode = @Keycode);
declare @ItemESID int;

/*Find Exam in Live and check itemResponses  >  Compare to FUF*/

select EST.id, keycode, examstate, CandidateRef, Forename, Surname, StructureXML,ESIRT.ItemID,  ESIRT.ItemResponseData
  from  ExamSessionTable as EST WITH (NOLOCK)

	inner join UserTable as UT WITH (NOLOCK) on UT.id = EST.UserID
	left Join ExamSessionItemResponseTable as ESIRT WITH (NOLOCK) on ESIRT.ExamSessionID = EST.ID

where EST.id =  @ID;


/*Void exam for FUF,  if needed*/

--update ExamSessionTable
--set previousExamState = examState, examState = 10
--where ID = @ID;


/*Find Exam in Warehouse*/


select id, ExamSessionID, keycode, StructureXML, resultData, resultDataFull  
      from  Warehouse_ExamSessionTable WITH (NOLOCK)
           where ExamSessionID =  @ID;

/*Run FUF Tool*/

SET @ResultDataFull = (Select resultDataFull from Warehouse_ExamSessionTable WITH (NOLOCK) where ExamSessionID =  @ID);
SET @resultData = (Select resultData from Warehouse_ExamSessionTable WITH (NOLOCK)  where ExamSessionID =  @ID);
SET @structureXML = (Select structureXML from Warehouse_ExamSessionTable WITH (NOLOCK) where ExamSessionID =  @ID);

--/*Run FUF,  then replace XML's*/

--update ExamSessionTable
--set StructureXML = @structureXML
--      , resultData = @resultData
--      , resultDataFull = @resultDataFull
--where ID = @ID;


select EST.id, keycode, examstate, ResultData, StructureXML, ResultDataFull
  from  ExamSessionTable as EST WITH (NOLOCK)

	inner join UserTable as UT WITH (NOLOCK)on UT.id = EST.UserID
	left Join ExamSessionItemResponseTable as ESIRT WITH (NOLOCK) on ESIRT.ExamSessionID = EST.ID

where EST.ID = @ID;


/*Put exam to State 9 > will go to State 15*/

--update ExamSessionTable
--set previousExamState = examState, examState = 9
--where ID = @ID;


/*Need to do an ItemResponseCheck > Compare Live to Warehouse*/

select ItemID, ItemResponseData 
	from ExamSessionItemResponseTable WITH (NOLOCK) 
where ExamSessionID = @ID;

select ItemID, ItemResponseData 
	from WAREHOUSE_ExamSessionTable as WEST WITH (NOLOCK)
	inner join WAREHOUSE_ExamSessionItemResponseTable as WESIRT WITH (NOLOCK)
	on WESIRT.WarehouseExamSessionID = WEST.ID
where WEST.ExamSessionID = @ID;


/*If Warehouse has more responses, then delete from Live Tables*/

--DELETE FROM ExamSessionItemResponseTable where ExamSessionID = @ID

/*Ready to insert WH responses into Live*/

declare @ItemResponses table (ExamSessionID int, ItemID  nvarchar(15), ItemVersion tinyint, ItemResponseData xml, MarkerResponseData xml, ItemMark tinyint, MarkingIgnored bit)
INSERT @ItemResponses(ExamSessionID, ItemID, ItemVersion, ItemResponseData, MarkerResponseData, ItemMark, MarkingIgnored)
SELECT	WEST.ExamSessionID, ItemID, ItemVersion, ItemResponseData, MarkerResponseData, ItemMark, MarkingIgnored
	from WAREHOUSE_ExamSessionItemResponseTable	as WESIRT WITH (NOLOCK)
	
	inner join WAREHOUSE_ExamSessionTable as WEST WITH (NOLOCK)
	on WEST.ID = WESIRT.WarehouseExamSessionID
	
	where WEST.ExamSessionID = @ID;


insert into ExamSessionItemResponseTable (ExamSessionID, ItemID, ItemVersion, ItemResponseData, MarkerResponseData, ItemMark, MarkingIgnored)
SELECT	ExamSessionID, ItemID, ItemVersion, ItemResponseData, MarkerResponseData, ItemMark, MarkingIgnored from @ItemResponses

select * from ExamSessionItemResponseTable where ExamSessionID = @ID;