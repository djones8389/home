select  
	ProjectListTable.id as ProjectID
	, pro.pag.value('@ID','nvarchar(100)') as PageID
			
from ProjectListTable 

	cross apply ProjectStructureXml.nodes('//Pag') pro(pag)
	
		left join PageTable
		on PageTable.ID = cast(ProjectListTable.ID as nvarchar(100)) + N'P' + pro.pag.value('@ID','nvarchar(100)')
					AND ProjectListTable.ID = PageTable.ParentID
					
WHERE pagetable.id is null;
	
	
	
	/*
872	1194
872	1402
872	1403
872	1404
872	1305
872	1358
872	1353
*/
	

select ProjectID + 'P' + PageID as Combined 
		into #jointable
	from #myTemp
	
select * from #jointable order by Combined 

;with cte as (

	select cast(id as nvarchar(100)) as CTEID
		 from PageTable 
		where ID like '4114%'
)

select * from cte order by CTEID



drop table #myTemp;
drop table #jointable;