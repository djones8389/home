/****** Script for SelectTopNRows command from SSMS  ******/
SELECT TOP 1000 [key]
      ,[token]
      ,[lastAccessed]
      ,[userID]
      ,[examSessionID]
      , forename
      ,surname
      , Username
      ,CandidateRef
  FROM [AAT_SecureAssess].[dbo].[TokenStoreTable]
  
  inner join usertable 
  on usertable.id = userID
  
  order by userID