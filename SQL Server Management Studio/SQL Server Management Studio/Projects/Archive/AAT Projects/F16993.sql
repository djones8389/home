declare @Keycode nvarchar(10) = '2SN4SQA6'

SELECT WEST.ID, WEST.KeyCode, WUT.Forename, WUT.Surname, WUT.CandidateRef, WCT.CentreName, WSCET.examName, WEST.completionDate
	, WESTS.ExternalReference
	, west.duration, wests.actualDuration, wests.defaultDuration, west.StructureXML
FROM WAREHOUSE_ExamSessionTable as WEST

  Inner Join WAREHOUSE_ScheduledExamsTable as WSCET
  on WSCET.ID = WEST.WAREHOUSEScheduledExamID
  
  Inner Join WAREHOUSE_UserTable as WUT
  on WUT.ID = WEST.WAREHOUSEUserID

  Inner Join WAREHOUSE_CentreTable as WCT
  on WCT.ID = WSCET.WAREHOUSECentreID

  Inner Join WAREHOUSE_ExamSessionTable_Shreded as WESTS
  on WESTS.examSessionId = WEST.ID

where west.KeyCode = @Keycode


SELECT top 100 WEST.ID, WEST.KeyCode, WUT.Forename, WUT.Surname, WUT.CandidateRef, WCT.CentreName, WSCET.examName, WEST.completionDate
	, WESTS.ExternalReference
	, west.duration, wests.actualDuration, wests.defaultDuration
FROM WAREHOUSE_ExamSessionTable as WEST

  Inner Join WAREHOUSE_ScheduledExamsTable as WSCET
  on WSCET.ID = WEST.WAREHOUSEScheduledExamID
  
  Inner Join WAREHOUSE_UserTable as WUT
  on WUT.ID = WEST.WAREHOUSEUserID

  Inner Join WAREHOUSE_CentreTable as WCT
  on WCT.ID = WSCET.WAREHOUSECentreID

  Inner Join WAREHOUSE_ExamSessionTable_Shreded as WESTS
  on WESTS.examSessionId = WEST.ID

order by WEST.ID desc

where WCT.CentreName = 'Peterborough Regional College' and duration is null

