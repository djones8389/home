SELECT EST.ID, EST.KeyCode, EST.examState, UT.Forename, UT.Surname, CT.CentreName, SCET.examName
  FROM ExamSessionTable as EST

  Inner Join ScheduledExamsTable as SCET
  on SCET.ID = EST.ScheduledExamID
  
  Inner Join UserTable as UT
  on UT.ID = EST.UserID

  Inner Join CentreTable as CT
  on CT.ID = SCET.CentreID



where candidateref in ('20001977','10438128','20003332','20002253','20049212','10407492','10357422','20046402','10403379','20007666','10454009','20002606','10457003','10453040','10435170','10431775','10379054','10426617','10435170','10431775')
	and examstate = 16
	
begin tran

update ExamSessionTable
set previousExamState = examState, examState = 15
where ID in ('1405665','1405807','1406282','1405394','1405959','1405876','1406650','1405825','1405379','1406606','1406844','1406835','1405751','1406379','1405861','1405332','1405666','1405264')

delete from ExamSessionTable_Shredded where examSessionId in ('1405665','1405807','1406282','1405394','1405959','1405876','1406650','1405825','1405379','1406606','1406844','1406835','1405751','1406379','1405861','1405332','1405666','1405264')
delete from ExamSessionTable_ShrededItems where examSessionId in ('1405665','1405807','1406282','1405394','1405959','1405876','1406650','1405825','1405379','1406606','1406844','1406835','1405751','1406379','1405861','1405332','1405666','1405264')

rollback	