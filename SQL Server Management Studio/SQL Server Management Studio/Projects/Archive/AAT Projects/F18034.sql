SELECT EST.ID, EST.KeyCode, EST.examState, UT.Forename, UT.Surname, CT.CentreName, SCET.examName
  FROM ExamSessionTable as EST

  Inner Join ScheduledExamsTable as SCET
  on SCET.ID = EST.ScheduledExamID
  
  Inner Join UserTable as UT
  on UT.ID = EST.UserID

  Inner Join CentreTable as CT
  on CT.ID = SCET.CentreID

where est.ID in (1394286,1393934)


update ExamSessionTable
set ExportToSecureMarker = 1
where ID in (1394286,1393934)

update ExamSessionTable
set previousExamState = examState, examState = 9
where ID in (1394286,1393934)