SELECT ProjectManifestTable.[ID]
      ,ProjectManifestTable.[name]
      ,[version]
      , ProjectListTable.name
      ,[location]
      ,[source]
      ,SharedLibraryTable.[ProjectId]
      ,[Image]     
      , SharedLibraryTable.structureXML
      , delCol
  FROM [AAT_CPProjectAdmin].[dbo].[ProjectManifestTable]
  
  inner join ProjectListTable
  on ProjectListTable.ID = ProjectManifestTable.ProjectId
  
  inner join SharedLibraryTable
  on SharedLibraryTable.ProjectId = ProjectListTable.ID
    
  where SharedLibraryTable.structureXML not like '%table_highlight_table.swf%'
	and ProjectManifestTable.name = 'table_highlight_table.swf'
	order by SharedLibraryTable.ProjectId
  
  
  