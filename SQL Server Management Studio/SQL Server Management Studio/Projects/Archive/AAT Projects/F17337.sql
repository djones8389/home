SELECT EST.ID, EST.KeyCode, EST.examState, UT.Forename, UT.Surname, CT.CentreName, SCET.examName, est.StructureXML
  FROM ExamSessionTable as EST

  Inner Join ScheduledExamsTable as SCET
  on SCET.ID = EST.ScheduledExamID
  
  Inner Join UserTable as UT
  on UT.ID = EST.UserID

  Inner Join CentreTable as CT
  on CT.ID = SCET.CentreID

where ut.surname in ('Kewn','Fortune','O''Regan')
	and ExamState= 15
	
	
select 

	ID
	,s.i.value('(@viewingTime)[1]', 'int') as viewingTime

	into #viewingTime

from ExamSessionTable

	cross apply StructureXML.nodes('/assessmentDetails/assessment/section/item') s(i)

	where ID in (1309895,1309897,1309900)


select sum(viewingTime)
  from #viewingTime 
  
where ID in (1309895) 

select sum(viewingTime)
  from #viewingTime 

where ID in (1309897) 

select  sum(viewingTime)
  from #viewingTime 

where ID in (1309900) 


drop table #viewingTime



select * from ExamStateChangeAuditTable  where ExamSessionID in (1309895,1309897,1309900)


1309895	6	2014-05-28 17:55:41.027
1309895	9	2014-05-28 19:59:35.030

1309897	6	2014-05-28 17:55:31.047
1309897	9	2014-05-28 19:46:36.070

1309900	6	2014-05-28 17:55:50.053
1309900	9	2014-05-28 19:59:34.040



