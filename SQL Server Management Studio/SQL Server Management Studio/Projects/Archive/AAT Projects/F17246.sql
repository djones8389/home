select --top 10

--	UT.ID
	Forename
	,Surname
	,CentreName
--	,RT.ID as RoleID
	,RT.Name as RoleName
	,PT.Name as PermissionName

from UserTable as UT

inner join AssignedUserRolesTable as AURT
on AURT.UserID = UT.ID

inner join CentreTable as CT
on CT.ID = AURT.CentreID

inner join RolesTable as RT
on RT.ID = AURT.RoleID

inner join RolePermissionsTable as RPT
on RPT.RoleID = RT.ID

inner join PermissionsTable as PT
on PT.ID = RPT.PermissionID

where CT.CentreName like '%Kaplan%'
	and rt.Name != 'Base'
	and pt.Name != 'Base'
	and CandidateRef = ''
	and Email not like '%@btl.com'
	and UT.Retired = 0
order by CentreName asc