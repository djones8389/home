SELECT EST.ID, EST.KeyCode, EST.examState, UT.Forename, UT.Surname, CT.CentreName, SCET.examName, StructureXML, resultData
  FROM ExamSessionTable as EST

  Inner Join ScheduledExamsTable as SCET
  on SCET.ID = EST.ScheduledExamID
  
  Inner Join UserTable as UT
  on UT.ID = EST.UserID

  Inner Join CentreTable as CT
  on CT.ID = SCET.CentreID

where ut.CandidateRef in ('20052111','20045001','20045079')

select COUNT(examsessionid),ExamSessionID from ExamSessionItemResponseTable where ExamSessionID in (1424434,1424436,1425142) group by ExamSessionID
select * from ExamSessionItemResponseTable where ExamSessionID =1425142