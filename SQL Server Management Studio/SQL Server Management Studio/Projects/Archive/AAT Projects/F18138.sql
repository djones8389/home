SELECT WEST.ID, WEST.KeyCode, WUT.Forename, WUT.Surname, WUT.CandidateRef, WCT.CentreName, WSCET.examName, WEST.completionDate
	----, WESTS.ExternalReference
	, west.resultData, west.StructureXML, west.resultDataFull
FROM WAREHOUSE_ExamSessionTable as WEST

  Inner Join WAREHOUSE_ScheduledExamsTable as WSCET
  on WSCET.ID = WEST.WAREHOUSEScheduledExamID
  
  Inner Join WAREHOUSE_UserTable as WUT
  on WUT.ID = WEST.WAREHOUSEUserID

  Inner Join WAREHOUSE_CentreTable as WCT
  on WCT.ID = WSCET.WAREHOUSECentreID

  --Inner Join WAREHOUSE_ExamSessionTable_Shreded as WESTS
  --on WESTS.examSessionId = WEST.ID

where west.ID = 1396838



select * from ReMarkExamSessionTable where WarehouseID = 1396838


/*
update ReMarkExamSessionTable
set StructureXML = '<assessmentDetails xmlns="">
  <assessmentID>1774</assessmentID>
  <qualificationID>85</qualificationID>
  <qualificationName>Budgeting (AQ2013)</qualificationName>
  <qualificationReference>AATQCF</qualificationReference>
  <assessmentGroupName>BDGT (AQ2013)</assessmentGroupName>
  <assessmentGroupID>1671</assessmentGroupID>
  <assessmentGroupReference>CABDGT</assessmentGroupReference>
  <assessmentName>Budgeting (AQ2013)</assessmentName>
  <validFromDate>01 Oct 2013</validFromDate>
  <expiryDate>31 Jul 2014</expiryDate>
  <startTime>00:00:00</startTime>
  <endTime>23:59:59</endTime>
  <duration>150</duration>
  <defaultDuration>150</defaultDuration>
  <scheduledDuration>
    <value>150</value>
    <reason></reason>
  </scheduledDuration>
  <sRBonus>0</sRBonus>
  <sRBonusMaximum>50</sRBonusMaximum>
  <externalReference>CABDGT</externalReference>
  <passLevelValue>70</passLevelValue>
  <passLevelType>1</passLevelType>
  <status>2</status>
  <testFeedbackType>
    <passFail>0</passFail>
    <percentageMark>0</percentageMark>
    <allowSummaryFeedback>0</allowSummaryFeedback>
    <summaryFeedbackType>1</summaryFeedbackType>
    <itemSummary>0</itemSummary>
    <itemReview>0</itemReview>
    <itemReviewCorrectAnswer>0</itemReviewCorrectAnswer>
    <itemFeedback>0</itemFeedback>
    <printableSummary>0</printableSummary>
    <candidateDetails>0</candidateDetails>
    <feedbackByReference>0</feedbackByReference>
    <allowFeedbackDuringExam>0</allowFeedbackDuringExam>
    <allowFeedbackDuringSummary>0</allowFeedbackDuringSummary>
  </testFeedbackType>
  <itemFeedback>0</itemFeedback>
  <allowCalculator>0</allowCalculator>
  <lastInUseDate>17 Sep 2013 11:44:10</lastInUseDate>
  <completedScriptReview>0</completedScriptReview>
  <assessment currentSection="0" totalTime="0" currentTime="0" originalPassMark="70" originalPassType="1" passMark="70" passType="1" totalMark="144" userMark="0" userPercentage="0" passValue="0" originalGrade="Fail">
    <intro id="0" name="Introduction" currentItem="">
      <item id="912P1093" name="Copy of Copy of Copy of Copy of BGT practice assessment intro" totalMark="1" version="8" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" unit="Budgeting" />
    </intro>
    <outro id="0" name="Finish" currentItem="" />
    <tools id="0" name="Tools" currentItem="" />
    <section id="1" name="1" passLevelValue="70" passLevelType="1" itemsToMark="0" currentItem="" fixed="0" totalMark="144" userMark="0" userPercentage="0" passValue="0">
      <item id="912P992" name="01 BDGT TS 003" totalMark="16" version="33" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" LO="1. Organisation and coding" unit="Budgeting" quT="12,16,20" />
      <item id="912P1038" name="02 BDGT TS 006" totalMark="19" version="22" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" LO="2. Production resources" unit="Budgeting" quT="11,20" />
      <item id="912P1034" name="03 BDGT TS 007" totalMark="18" version="21" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" LO="3. Operating budget" unit="Budgeting" quT="20" />
      <item id="912P1028" name="04 BDGT TS 005" totalMark="17" version="26" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" LO="4. Forecasting" unit="Budgeting" quT="11" />
      <item id="912P985" name="05 BDGT TS 002" totalMark="20" version="31" markingType="1" markingState="0" userMark="0" markerUserMark="" userAttempted="1" viewingTime="" flagged="0" LO="5. Present a budget for approval" unit="Budgeting" quT="11" />
      <item id="912P983" name="06 BDGT TS 002" totalMark="18" version="32" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" LO="6. Standard costing and budget revision" unit="Budgeting" quT="20" />
      <item id="912P1018" name="07 BDGT TS 007" totalMark="16" version="22" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" LO="7. Operating statement" unit="Budgeting" quT="20" />
      <item id="912P1048" name="08 BDGT SW009" totalMark="20" version="26" markingType="1" markingState="0" userMark="0" markerUserMark="" userAttempted="1" viewingTime="" flagged="0" LO="8. Performance report" unit="Budgeting" quT="11" />
    </section>
  </assessment>
  <isValid>1</isValid>
  <isPrintable>0</isPrintable>
  <qualityReview>0</qualityReview>
  <numberOfGenerations>1</numberOfGenerations>
  <requiresInvigilation>1</requiresInvigilation>
  <advanceContentDownloadTimespan>60</advanceContentDownloadTimespan>
  <automaticVerification>0</automaticVerification>
  <offlineMode>2</offlineMode>
  <requiresValidation>0</requiresValidation>
  <requiresSecureClient>1</requiresSecureClient>
  <examType>0</examType>
  <advanceDownload>0</advanceDownload>
  <gradeBoundaryData>
    <gradeBoundaries passLevelType="1" adjustedGradeBoundaries="0">
      <grade modifier="lt" value="70" description="Fail" />
      <grade modifier="gt" value="70" description="Pass" />
    </gradeBoundaries>
  </gradeBoundaryData>
  <autoViewExam>0</autoViewExam>
  <autoProgressItems>0</autoProgressItems>
  <confirmationText>
    <confirmationText></confirmationText>
  </confirmationText>
  <requiresConfirmationCheck>0</requiresConfirmationCheck>
  <allowCloseWithoutSubmit>0</allowCloseWithoutSubmit>
  <useSecureMarker>0</useSecureMarker>
  <annotationVersion>0</annotationVersion>
  <allowPackagingDelivery>1</allowPackagingDelivery>
  <scoreBoundaryData>
    <scoreBoundaries showScoreBoundaryColumn="1">
      <score modifier="lt" value="36" description="Significantly below" higherBoundarySet="1" />
      <score modifier="lt" value="36" description="Significantly below" higherBoundarySet="0" />
      <score modifier="gt" value="36" description="Below requirement" higherBoundarySet="1" />
      <score modifier="gt" value="60" description="Borderline" higherBoundarySet="1" />
      <score modifier="gt" value="70" description="Met" higherBoundarySet="0" />
      <score modifier="gt" value="70" description="Met" higherBoundarySet="1" />
      <score modifier="gt" value="36" description="Below requirement" higherBoundarySet="0" userCreated="1" />
      <score modifier="gt" value="60" description="Borderline" higherBoundarySet="0" userCreated="1" />
      <score modifier="gt" value="85" description="Exceeded" higherBoundarySet="1" userCreated="1" />
    </scoreBoundaries>
  </scoreBoundaryData>
  <showCandidateReportResultColumn>0</showCandidateReportResultColumn>
  <showCandidateReportPercentageColumn>1</showCandidateReportPercentageColumn>
  <certifiedAccessible>0</certifiedAccessible>
  <markingProgressVisible>1</markingProgressVisible>
  <markingProgressMode>0</markingProgressMode>
  <secureClientOperationMode>1</secureClientOperationMode>
  <examDeliverySystemStyleType>delivery</examDeliverySystemStyleType>
  <markingAutoVoidPeriod>365</markingAutoVoidPeriod>
  <showPageRequiresScrollingAlert>0</showPageRequiresScrollingAlert>
  <project ID="912" />
</assessmentDetails>', ResultData = '<exam passMark="70" passType="1" originalPassMark="70" originalPassType="1" totalMark="144" userMark="0" userPercentage="0" passValue="0" originalPassValue="0" totalTimeSpent="0" closeBoundaryType="0" closeBoundaryValue="0" grade="Fail" originalGrade="Fail">
  <gradeBoundaryData>
    <gradeBoundaries passLevelType="1" adjustedGradeBoundaries="0">
      <grade modifier="lt" value="70" description="Fail" />
      <grade modifier="gt" value="70" description="Pass" />
    </gradeBoundaries>
  </gradeBoundaryData>
  <section id="1" passMark="70" passType="1" name="1" totalMark="144" userMark="0" userPercentage="0" passValue="0" totalTimeSpent="0" itemsToMark="0">
    <item id="912P992" name="01 BDGT TS 003" totalMark="16" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="33" />
    <item id="912P1038" name="02 BDGT TS 006" totalMark="19" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="22" />
    <item id="912P1034" name="03 BDGT TS 007" totalMark="18" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="21" />
    <item id="912P1028" name="04 BDGT TS 005" totalMark="17" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="26" />
    <item id="912P985" name="05 BDGT TS 002" totalMark="20" userMark="0" actualUserMark="0" markingType="1" markerUserMark="" viewingTime="0" userAttempted="1" version="31" />
    <item id="912P983" name="06 BDGT TS 002" totalMark="18" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="32" />
    <item id="912P1018" name="07 BDGT TS 007" totalMark="16" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="22" />
    <item id="912P1048" name="08 BDGT SW009" totalMark="20" userMark="0" actualUserMark="0" markingType="1" markerUserMark="" viewingTime="0" userAttempted="1" version="26" />
  </section>
</exam>', ResultDataFull = '<assessmentDetails xmlns="">
  <assessmentID>1774</assessmentID>
  <qualificationID>85</qualificationID>
  <qualificationName>Budgeting (AQ2013)</qualificationName>
  <qualificationReference>AATQCF</qualificationReference>
  <assessmentGroupName>BDGT (AQ2013)</assessmentGroupName>
  <assessmentGroupID>1671</assessmentGroupID>
  <assessmentGroupReference>CABDGT</assessmentGroupReference>
  <assessmentName>Budgeting (AQ2013)</assessmentName>
  <validFromDate>01 Oct 2013</validFromDate>
  <expiryDate>31 Jul 2014</expiryDate>
  <startTime>00:00:00</startTime>
  <endTime>23:59:59</endTime>
  <duration>150</duration>
  <defaultDuration>150</defaultDuration>
  <scheduledDuration>
    <value>150</value>
    <reason />
  </scheduledDuration>
  <sRBonus>0</sRBonus>
  <sRBonusMaximum>50</sRBonusMaximum>
  <externalReference>CABDGT</externalReference>
  <passLevelValue>70</passLevelValue>
  <passLevelType>1</passLevelType>
  <status>2</status>
  <testFeedbackType>
    <passFail>0</passFail>
    <percentageMark>0</percentageMark>
    <allowSummaryFeedback>0</allowSummaryFeedback>
    <summaryFeedbackType>1</summaryFeedbackType>
    <itemSummary>0</itemSummary>
    <itemReview>0</itemReview>
    <itemReviewCorrectAnswer>0</itemReviewCorrectAnswer>
    <itemFeedback>0</itemFeedback>
    <printableSummary>0</printableSummary>
    <candidateDetails>0</candidateDetails>
    <feedbackByReference>0</feedbackByReference>
    <allowFeedbackDuringExam>0</allowFeedbackDuringExam>
    <allowFeedbackDuringSummary>0</allowFeedbackDuringSummary>
  </testFeedbackType>
  <itemFeedback>0</itemFeedback>
  <allowCalculator>0</allowCalculator>
  <lastInUseDate>Aug 22 2014 12:20PM</lastInUseDate>
  <completedScriptReview>0</completedScriptReview>
  <assessment currentSection="0" totalTime="0" currentTime="0" originalPassMark="70" originalPassType="1" passMark="70" passType="1" totalMark="144" userMark="0" userPercentage="0" passValue="0" originalGrade="Fail">
    <intro id="0" name="Introduction" currentItem="">
      <item id="912P1093" name="Copy of Copy of Copy of Copy of BGT practice assessment intro" totalMark="1" version="8" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" unit="Budgeting" />
    </intro>
    <outro id="0" name="Finish" currentItem="" />
    <tools id="0" name="Tools" currentItem="" />
    <section id="1" name="1" passLevelValue="70" passLevelType="1" itemsToMark="0" currentItem="" fixed="0" totalMark="144" userMark="0" userPercentage="0" passValue="0">
      <item id="912P992" name="01 BDGT TS 003" totalMark="16" version="33" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" LO="1. Organisation and coding" unit="Budgeting" quT="12,16,20">
        <p um="0.8125" cs="1" ua="1" id="912P992">
          <s ua="1" um="0.33" id="1">
            <c wei="3" maS="0" ie="1" typ="20" rv="0" mv="0" ua="1" um="0.33" id="35">
              <i um="0.333333333333333" ia="1" id="1">2񧊓</i>
            </c>
          </s>
          <s ua="1" um="1" id="2">
            <c wei="3" maS="0" ie="1" typ="20" rv="0" mv="0" ua="1" um="1" id="4">
              <i um="1" ia="1" id="1">4񦶧</i>
            </c>
          </s>
          <s ua="1" um="1" id="3">
            <c ua="1" um="1" typ="16" id="1">
              <z id="15" />
              <z id="13">
                <i y="1559" x="550" ID="3" />
              </z>
              <z id="12" />
              <z id="11" />
              <z id="10" />
              <z id="9" />
              <z id="8">
                <i y="1394" x="695" ID="4" />
                <i y="1419" x="695" ID="11" />
              </z>
              <z id="7">
                <i y="1394" x="415" ID="9" />
              </z>
              <z id="6">
                <i y="1229" x="695" ID="2" />
                <i y="1254" x="695" ID="12" />
              </z>
              <z id="5">
                <i y="1229" x="415" ID="1" />
                <i y="1254" x="415" ID="10" />
              </z>
              <z id="4" />
              <z id="3" />
              <z id="2" />
              <z id="1" />
            </c>
          </s>
          <s ua="1" um="0.88" id="4">
            <c wei="4" maS="0" ie="1" typ="12" rv="0" mv="0" ua="1" um="0.88" id="2">
              <i sl="7" id="10" />
              <i sl="8" id="3" />
              <i sl="4" id="4" />
              <i sl="4" id="5" />
              <i sl="5" id="6" />
              <i sl="5" id="7" />
              <i sl="3" id="8" />
              <i sl="6" id="9" />
            </c>
          </s>
          <s ua="1" um="0.75" id="5">
            <c wei="2" maS="0" ie="1" typ="12" rv="0" mv="0" ua="1" um="0.75" id="13">
              <i sl="4" id="3" />
              <i sl="8" id="4" />
              <i sl="5" id="5" />
              <i sl="5" id="6" />
            </c>
          </s>
        </p>
      </item>
      <item id="912P1038" name="02 BDGT TS 006" totalMark="19" version="22" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" LO="2. Production resources" unit="Budgeting" quT="11,20">
        <p um="1" cs="1" ua="1" id="912P1038">
          <s ua="1" um="1" id="1">
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="3">
              <i ca="1250" id="1">1250</i>
              <i ca="1250" id="2">1250</i>
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="5">
              <i ca="" id="1">2638</i>
              <i ca="" id="2">2508</i>
              <i ca="" id="3">2563</i>
              <i ca="" id="4">2575</i>
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="6">
              <i ca="1280" id="1">1280</i>
              <i ca="1280" id="2">1280</i>
            </c>
            <c wei="8" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="7">
              <i ca="" id="1">10550</i>
              <i ca="" id="2">10030</i>
              <i ca="" id="3">10250</i>
              <i ca="" id="4">10300</i>
              <i ca="" id="5">13188</i>
              <i ca="" id="6">12538</i>
              <i ca="" id="7">12813</i>
              <i ca="" id="8">12875</i>
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="10">
              <i ca="1290" id="1">1290</i>
              <i ca="1290" id="2">1290</i>
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="11">
              <i ca="1270" id="1">1270</i>
            </c>
          </s>
          <s ua="1" um="1" id="3">
            <c wei="2" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="10">
              <i ca="7600" id="1">7600</i>
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="12">
              <i ca="7620" id="1">7620</i>
            </c>
          </s>
          <s ua="1" um="1" id="4">
            <c wei="2" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="13">
              <i ca="255" id="1">255</i>
            </c>
          </s>
          <s ua="1" um="1" id="5">
            <c wei="2" maS="0" ie="1" typ="20" rv="0" mv="0" ua="1" um="1" id="18">
              <i um="1" ia="1" id="1">&lt;table extensionName="protean"&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="0" cor=""&gt;1200&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="0" cor=""&gt;738&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="0" cor=""&gt;750&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="2688"&gt;2688&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="96"&gt;96&lt;/c&gt;&lt;/r&gt;&lt;/table&gt;</i>
            </c>
          </s>
        </p>
      </item>
      <item id="912P1034" name="03 BDGT TS 007" totalMark="18" version="21" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" LO="3. Operating budget" unit="Budgeting" quT="20">
        <p um="0.583333333333333" cs="1" ua="1" id="912P1034">
          <s ua="1" um="0.73" id="1">
            <c wei="11" maS="0" ie="1" typ="20" rv="0" mv="0" ua="1" um="0.73" id="16">
              <i um="0.727272727272727" ia="1" id="1">&lt;table extensionName="protean"&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="256000"&gt;256000&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="843400-[c3r6]~o"&gt;818440&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="263800-[c2r5]~o"&gt;7800&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="[c2r6]*3.2~o"&gt;24960&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="5600"&gt;5600&lt;/c&gt;&lt;c h="0" tp="1" mark="0.5" cor="[c2r9]*14~o"&gt;78400&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="[c2r11]-[c2r9]~o"&gt;800&lt;/c&gt;&lt;c h="0" tp="1" mark="0.5" cor="[c2r10]*17.5~o"&gt;14000&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="6400"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="0.5" cor="[c3r9]+[c3r10]~o"&gt;92400&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="[c2r11]~o"&gt;6400&lt;/c&gt;&lt;c h="0" tp="1" mark="0.5" cor="[c2r14]*6~o"&gt;38400&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="[c3r14]+10760~o"&gt;49160&lt;/c&gt;&lt;/r&gt;&lt;/table&gt;</i>
            </c>
          </s>
          <s ua="1" um="0.36" id="2">
            <c wei="7" maS="0" ie="1" typ="20" rv="0" mv="0" ua="1" um="0.36" id="9">
              <i um="0.357142857142857" ia="1" id="1">&lt;table extensionName="protean"&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="0.5" cor="66000"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="0.5" cor="[c2r2]*25~o"&gt;1650000&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="0.5" cor="[s1c16i1c3r5]~o"&gt;818440&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="0.5" cor="[s1c16i1c3r11]~o"&gt;92400&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="0.5" cor="[s1c16i1c3r16]~o"&gt;49160&lt;/c&gt;&lt;c h="0" tp="1" mark="0.5" cor="[c3r6]+[c3r7]+[c3r8]~o"&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="[c4r8]/64000*8000~o"&gt;120000&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="140000+[c4r8]-[c4r9]~o"&gt;980000&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="[c4r2]-[c4r10]~o"&gt;670000&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="0.5" cor="180000"&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="0.5" cor="[c4r11]-[c4r14]~o"&gt;490000&lt;/c&gt;&lt;/r&gt;&lt;/table&gt;</i>
            </c>
          </s>
        </p>
      </item>
      <item id="912P1028" name="04 BDGT TS 005" totalMark="17" version="26" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" LO="4. Forecasting" unit="Budgeting" quT="11">
        <p um="1" cs="1" ua="1" id="912P1028">
          <s ua="1" um="1" id="1">
            <c wei="5" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="43">
              <i ca="215" id="1">215</i>
              <i ca="431" id="2">431</i>
              <i ca="200" id="3">200</i>
              <i ca="154" id="4">154</i>
              <i ca="" id="5">1000</i>
            </c>
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="44">
              <i ca="" id="1">19500</i>
            </c>
          </s>
          <s ua="1" um="1" id="2">
            <c wei="4" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="13">
              <i ca="213400" id="1">213400</i>
              <i ca="88200" id="2">88200</i>
              <i ca="37800" id="4">37800</i>
              <i ca="1750" id="5">1750</i>
            </c>
            <c wei="2" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="14">
              <i ca="75250" id="1">75250</i>
            </c>
          </s>
          <s ua="1" um="1" id="3">
            <c wei="6" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="25">
              <i ca="122200" id="1">122200</i>
              <i ca="50400" id="2">50400</i>
              <i ca="27400" id="3">27400</i>
              <i ca="19300" id="4">19300</i>
              <i ca="" id="5">97100</i>
              <i ca="" id="6">25100</i>
            </c>
          </s>
        </p>
      </item>
      <item id="912P985" name="05 BDGT TS 002" totalMark="20" version="31" markingType="1" markingState="0" userMark="0" markerUserMark="" userAttempted="1" viewingTime="" flagged="0" LO="5. Present a budget for approval" unit="Budgeting" quT="11">
        <p um="0" cs="1" ua="1" id="912P985">
          <s ua="1" um="0" id="2">
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="3">
              <i ca="" id="1">I ATTACH THE PROPOSED LABOUR BUDGET FOR THE NEXT YEAR FOR YOU TO CONSIDER FOR APPROVAL. I HAVE SHOWN THIS YEARS RESULTS FOR THE PURPOSE OF COMPARISION.#!#MY THIS DRAFT BUDGET IS BASED ON THE PRODUCTION LEVEL OF 84925 UNIT, THIS IS APPROXIMATELY 8% HIGHER THAN THIS YEAR''S. #!#THE PRODUCTIVITY LEVEL OF STAFF IS EXPECTED TO IMPROVE FROM  15MINUTES PER UNIT TO ONLY 12MINUTES PER UNIT..HOWEVER, THE TOTAL NUMBER OF LABOUR HOURS IS EXPECTED TO FALL DUE TO THE IMPROVED PRODUCTIVITY LEVEL..ALSO OVERTIME PAYMENT IS ALSO EXPECTED TO FALL FROM 2250HOURS TO 1235HOURS.#!#THE LABOUR RATE PER HOUR IS TO INCREASE FROM 16 TO16.80 POUNDS PER HOUR.#!#  #!#OVERALL THE DIRECT LABOUR BUDGET SHOWS A DECREASE OF 11% COMPARED TO WITH THIS YEAR.</i>
            </c>
          </s>
          <s ua="1" um="0" id="3">
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="3">
              <i ca="" id="1">PRODUCTIVITY PER UNIT. THIS YEAR, IT TAKES THE LABOUR FORCE 15MINUTES TO PRODUCE ONE UNIT WHERE AS IT IS EXPECTED TO TAKE ONLY 12MINUTES INDICATING THAT PRODUCTIVITY IS EXPECTED TO IMPROVE.#!#THE BUDGETED NUMBER OF HOURS IS LESS THAN THIS YEAR''S ACTUAL HOURS SUGESSTING THAT THERE IS A REDUCTION IN THE NUMBER OF STAFF OR AN IMPROVEMENT IN THE PRODUCTIVITY OF THE WORKFORCE AS SHOWN IN THE INFORMATION ABOVE.</i>
            </c>
          </s>
          <s ua="1" um="0" id="4">
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="3">
              <i ca="" id="1">PROPER SUPERVISION OF WORKFORCE#!##!#SET UP PROPER INCENTIVE SCHEMES</i>
            </c>
          </s>
        </p>
      </item>
      <item id="912P983" name="06 BDGT TS 002" totalMark="18" version="32" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" LO="6. Standard costing and budget revision" unit="Budgeting" quT="20">
        <p um="1" cs="1" ua="1" id="912P983">
          <s ua="1" um="1" id="1">
            <c wei="10" maS="0" ie="1" typ="20" rv="0" mv="0" ua="1" um="1" id="32">
              <i um="1" ia="1" id="1">&lt;table extensionName="protean"&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="7.68"&gt;7.68&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="62350"&gt;62350&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="[c3r2]*[c3r3]~o"&gt;478848&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="3.8*[c3r3]~o"&gt;236930&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="1.8*[c3r3]~o"&gt;112230&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1.5" cor="15120"&gt;15120&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1.5" cor="12600"&gt;12600&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="0.5" cor="[c3r8]+[c3r9]+[c3r10]+[c3r11]~o"&gt;376880&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="0.5" cor="[c3r5]-[c3r12]~o"&gt;101968&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="[c3r14]-111900~o"&gt;-9932&lt;/c&gt;&lt;/r&gt;&lt;/table&gt;</i>
            </c>
          </s>
          <s ua="1" um="1" id="2">
            <c wei="8" maS="0" ie="1" typ="20" rv="0" mv="0" ua="1" um="1" id="6">
              <i um="1" ia="1" id="1">&lt;table extensionName="protean"&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="2" cor="30400"&gt;30400&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="2" cor="2000"&gt;2000&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="2" cor="-1600"&gt;(1600)&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="2" cor="400"&gt;400&lt;/c&gt;&lt;/r&gt;&lt;/table&gt;</i>
            </c>
          </s>
        </p>
      </item>
      <item id="912P1018" name="07 BDGT TS 007" totalMark="16" version="22" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" LO="7. Operating statement" unit="Budgeting" quT="20">
        <p um="1" cs="1" ua="1" id="912P1018">
          <s ua="1" um="1" id="2">
            <c wei="16" maS="0" ie="1" typ="20" rv="0" mv="0" ua="1" um="1" id="1">
              <i um="1" ia="1" id="1">&lt;table extensionName="protean"&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="745000"&gt;745000&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="0.5" cor="782250-[c3r4]~o"&gt;37250&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="171350"&gt;171350&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="0.5" cor="[c3r6]-178800~o"&gt;(7450)&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="119200"&gt;119200&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="0.5" cor="[c3r7]-134100~o"&gt;(14900)&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="8940"&gt;8940&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="0.5" cor="[c3r8]-10430~o"&gt;(1490)&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="2" cor="34800"&gt;34800&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="0.5" cor="[c3r9]-33310~o"&gt;1490&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="2" cor="20000"&gt;20000&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="0.5" cor="[c3r10]-17500~o"&gt;2500&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="0.5" cor="13000"&gt;13000&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="0.5" cor="[c3r11]-14500~o"&gt;(1500)&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="0.5" cor="37600"&gt;37600&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="0.5" cor="[c3r12]-39000~o"&gt;(1400)&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="0.5" cor="38500"&gt;38500&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="0.5" cor="[c3r13]-36250~o"&gt;2250&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="0.5" cor="[c3r6]+[c3r7]+[c3r8]+[c3r9]+[c3r10]+[c3r11]+[c3r12]+[c3r13]~o"&gt;443390&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="0.5" cor="[c3r14]-463890~o"&gt;(20500)&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="0.5" cor="[c3r4]-[c3r14]~o"&gt;301610&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="0.5" cor="[c5r4]+[c5r14]~o"&gt;16750&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;/table&gt;</i>
            </c>
          </s>
        </p>
      </item>
      <item id="912P1048" name="08 BDGT SW009" totalMark="20" version="26" markingType="1" markingState="0" userMark="0" markerUserMark="" userAttempted="1" viewingTime="" flagged="0" LO="8. Performance report" unit="Budgeting" quT="11">
        <p um="0" cs="1" ua="1" id="912P1048">
          <s ua="1" um="0" id="2">
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="23">
              <i ca="" id="1">SALES REVENUE#!##!#THE MAIN REASON FOR THE ADVERSE SALES REVENUE VARIANCE CAN BE ATTRIBUTED TO THE LOWER SELLING PRICE THAN EXPECTED. THIS LOWER SELLING PRICE CAN BE DUE AS ARESULT OF THE UNANTICIPATED INCREASE IN SALES IN THE FAR EAST AS MENTIONED IN THE INFORMATION ABOVE.#!##!#MATERIAL VARIANCE#!##!#THIS ADVERSE MATERIAL VARIANCE CAN BE ATTRIBUTED TO THE EXTRA MATERIALS PURCHASED FROM THE NEW SUPPLIER WHICH WAS MORE EXPENSIVE. ALSO THE MATERIALS PROVED TO BE OF POOR QUALITY AND AS A RESULT, LED TO A LOT OF WASTAGE WHICH COULD ALSO CONTRIBUTE TO THE ADVERSE MATERIAL VARIANCE.#!##!#LABOUR VARIANCE#!##!#THIS ADVERSE VARIANCE CAN ALSO BE EXPLAINED BY THE INCRESED OVERTIME PAYMENTS REQUIRED TO PRODUCE ADDITIONAL UNITS TO REPLACE THE SPOILED ONES#!##!#</i>
            </c>
          </s>
          <s ua="1" um="0" id="3">
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="5">
              <i ca="" id="1">(1)  MANAGEMENT SHOULD HAVE MAINTAINED A HIGHER CLOSING INVENTORY OF FINISH GOODS FROM THE PREVIOS PERIOD TO MAKE UP FOR ANY POSSIBLE SHORTFALL#!##!#(2)  MANAGEMENT SHOULD HAVE PURCHASED EXTRA RAW MATERIALS FROM THEIR ORIGINAL SUPPLIER WITH THE GOOD QUALITY TO MEET ANY UNUSUAL DEMAND.#!##!#(3) FINALLY, MANAGEMENT COULD HAVE ALSO INCREASE THE NUMBER OF SHIFTS INORDER TO BE ABLE TO PRODUCE THE EXTRA UNITS REQUIRED TO MEET THE ADDITIONAL DEMAND.</i>
            </c>
          </s>
          <s ua="1" um="0" id="4">
            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="3">
              <i ca="" id="1">BY INTRODUCING STANDARD COSTING, MANAGEMENT WOULD BE ABLE TO SET A REALISTIC STANDARDS AND AS A RESULT, COMPARE ACTUAL PERFORMANCE WITH THESE STANDARDS SET INORDER TO IDENTIFY ANY VARIANCES. WHEN VARIANCES ARE IDENTIFIED, THE REASONS FOR THE VARIANCES ARE INVESTIGATED AND CORRECTIVE ACTIONS TAKEN.</i>
            </c>
          </s>
        </p>
      </item>
    </section>
  </assessment>
  <isValid>1</isValid>
  <isPrintable>0</isPrintable>
  <qualityReview>0</qualityReview>
  <numberOfGenerations>1</numberOfGenerations>
  <requiresInvigilation>1</requiresInvigilation>
  <advanceContentDownloadTimespan>60</advanceContentDownloadTimespan>
  <automaticVerification>0</automaticVerification>
  <offlineMode>2</offlineMode>
  <requiresValidation>0</requiresValidation>
  <requiresSecureClient>1</requiresSecureClient>
  <examType>0</examType>
  <advanceDownload>0</advanceDownload>
  <gradeBoundaryData>
    <gradeBoundaries passLevelType="1" adjustedGradeBoundaries="0">
      <grade modifier="lt" value="70" description="Fail" />
      <grade modifier="gt" value="70" description="Pass" />
    </gradeBoundaries>
  </gradeBoundaryData>
  <autoViewExam>0</autoViewExam>
  <autoProgressItems>0</autoProgressItems>
  <confirmationText>
    <confirmationText />
  </confirmationText>
  <requiresConfirmationCheck>0</requiresConfirmationCheck>
  <allowCloseWithoutSubmit>0</allowCloseWithoutSubmit>
  <useSecureMarker>0</useSecureMarker>
  <annotationVersion>0</annotationVersion>
  <allowPackagingDelivery>1</allowPackagingDelivery>
  <scoreBoundaryData>
    <scoreBoundaries showScoreBoundaryColumn="1">
      <score modifier="lt" value="36" description="Significantly below" higherBoundarySet="1" />
      <score modifier="lt" value="36" description="Significantly below" higherBoundarySet="0" />
      <score modifier="gt" value="36" description="Below requirement" higherBoundarySet="1" />
      <score modifier="gt" value="60" description="Borderline" higherBoundarySet="1" />
      <score modifier="gt" value="70" description="Met" higherBoundarySet="0" />
      <score modifier="gt" value="70" description="Met" higherBoundarySet="1" />
      <score modifier="gt" value="36" description="Below requirement" higherBoundarySet="0" userCreated="1" />
      <score modifier="gt" value="60" description="Borderline" higherBoundarySet="0" userCreated="1" />
      <score modifier="gt" value="85" description="Exceeded" higherBoundarySet="1" userCreated="1" />
    </scoreBoundaries>
  </scoreBoundaryData>
  <showCandidateReportResultColumn>0</showCandidateReportResultColumn>
  <showCandidateReportPercentageColumn>1</showCandidateReportPercentageColumn>
  <certifiedAccessible>0</certifiedAccessible>
  <markingProgressVisible>1</markingProgressVisible>
  <markingProgressMode>0</markingProgressMode>
  <secureClientOperationMode>1</secureClientOperationMode>
  <examDeliverySystemStyleType>delivery</examDeliverySystemStyleType>
  <markingAutoVoidPeriod>365</markingAutoVoidPeriod>
  <showPageRequiresScrollingAlert>0</showPageRequiresScrollingAlert>
  <project ID="912" />
</assessmentDetails>'
where WarehouseID = 1396838
	*/