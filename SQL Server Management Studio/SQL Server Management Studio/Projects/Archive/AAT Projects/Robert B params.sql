SELECT  --1350896--
				   WAREHOUSE_ExamSessionTable_Shreded.examSessionId ,
                  [examVersionID],
                  [examName],
                  [ExamRef],
                  [examVersionName],
                  [ExamVersionRef],
                  [qualificationId],
                  [QualificationName],
                  [QualificationRef]
                  FROM WAREHOUSE_ExamSessionTable_Shreded

inner join WAREHOUSE_ExamSessionTable_ShrededItems 
ON WAREHOUSE_ExamSessionTable_ShrededItems.examSessionId = WAREHOUSE_ExamSessionTable_Shreded.examSessionId 

where examVersionId = '2035'
and examName = 'BTAX FA 2013'
and examRef = 'CABTAX'
and examVersionName = 'BTAX FA 2013'
and examVersionRef = 'CBTAAT'
and qualificationid = '96'
and qualificationName = 'Business Taxation (AQ2013)'
and qualificationRef = 'AATQCF'

--examVersionID                                 examName        ExamRef                              examVersionName         ExamVersionRef                qualificationId                   QualificationName                                          QualificationRef
--2035                                           BTAX FA 2013     CABTAX                                BTAX FA 2013                     CBTAAT                                96                                                Business Taxation (AQ2013)                        AATQCF
