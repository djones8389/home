--Report 1--

select
	foreName
	,surName
	,candidateRef
	,centreName	
	,s.i.value('@id','nvarchar(100)') as 'Item 3'
	,t.u.value('@id','nvarchar(100)') as 'Item 5'
	,centreCode
	,qualificationName
	,examName
	,case grade when 'Fail' then 'Not yet compentent' else 'Competent' end as [Grade]
	,userMark
	,userPercentage
	,submittedDate
	,'Warehoused' as [ExamState]
 from WAREHOUSE_ExamSessionTable_Shreded 
	
  inner join WAREHOUSE_ExamSessionTable on WAREHOUSE_ExamSessionTable.id = WAREHOUSE_ExamSessionTable_Shreded.examSessionId
			
	cross apply WAREHOUSE_ExamSessionTable_Shreded.ResultData.nodes('/exam/section/item[3]') as s(i)
	cross apply WAREHOUSE_ExamSessionTable_Shreded.ResultData.nodes('/exam/section/item[5]') as t(u)
	
	where qualificationName = 'Processing bookkeeping transactions (AQ2013)'
		and examName like '%PBKT%' 
		and userPercentage >= '68'
		and grade <> 'Pass'
	order by foreName, surName;
	
--Report 2--
	
select
	foreName
	,surName
	,candidateRef
	,centreName	
	--,s.i.value('@id','nvarchar(100)') as 'Item 3'
	--,t.u.value('@id','nvarchar(100)') as 'Item 5'
	,centreCode
	,qualificationName
	,examName
	,case grade when 'Fail' then 'Not yet compentent' else 'Competent' end as [Grade]
	,userMark
	,userPercentage
	,submittedDate
	,'Warehoused' as [ExamState]
 from WAREHOUSE_ExamSessionTable_Shreded 
	
  inner join WAREHOUSE_ExamSessionTable on WAREHOUSE_ExamSessionTable.id = WAREHOUSE_ExamSessionTable_Shreded.examSessionId
			
	--cross apply WAREHOUSE_ExamSessionTable_Shreded.ResultData.nodes('/exam/section/item[3]') as s(i)
	--cross apply WAREHOUSE_ExamSessionTable_Shreded.ResultData.nodes('/exam/section/item[5]') as t(u)
	
	where qualificationName = 'Business taxation (AQ2013)'
		and examName like '%BTAX%' 
		and userPercentage >= '68.9'
		and grade <> 'Pass'
	order by foreName, surName;
		
	
--Report 3--
	
	
select
	foreName
	,surName
	,candidateRef
	,centreName	
	--,s.i.value('@id','nvarchar(100)') as 'Item 3'
	--,t.u.value('@id','nvarchar(100)') as 'Item 5'
	,centreCode
	,qualificationName
	,examName
	,case grade when 'Fail' then 'Not yet compentent' else 'Competent' end as [Grade]
	,userMark
	,userPercentage
	,submittedDate
	,'Warehoused' as [ExamState]
 from WAREHOUSE_ExamSessionTable_Shreded 
	
  inner join WAREHOUSE_ExamSessionTable on WAREHOUSE_ExamSessionTable.id = WAREHOUSE_ExamSessionTable_Shreded.examSessionId
			
	--cross apply WAREHOUSE_ExamSessionTable_Shreded.ResultData.nodes('/exam/section/item[3]') as s(i)
	--cross apply WAREHOUSE_ExamSessionTable_Shreded.ResultData.nodes('/exam/section/item[5]') as t(u)
	
	where qualificationName = 'Business taxation (AQ2010)'
		and examName like '%BTX%' 
		and userPercentage >= '67.9'
		and grade <> 'Pass'
	order by foreName, surName;