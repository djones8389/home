SELECT 

	--sum(section.item.value('@viewingTime[1]', 'int')) as viewingtimeTotalled
--StructureXml

duration, StructureXML
  FROM ExamSessionTable as EST

  Inner Join ScheduledExamsTable as SCET
  on SCET.ID = EST.ScheduledExamID
  
  Inner Join UserTable as UT
  on UT.ID = EST.UserID

  Inner Join CentreTable as CT
  on CT.ID = SCET.CentreID

cross apply StructureXml.nodes('assessmentDetails/assessment/section/item') section(item)

where est.ID = 1374173
	