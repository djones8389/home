SELECT top 1
	wests.ExamSessionId,
	wests.QualificationName,
	wests.ExamName,
	LTRIM(RTRIM(wests.Forename)) + ' ' + LTRIM(RTRIM(wests.Surname)) AS CandidateName,
	wests.candidateRef AS MembershipNo,
	wests.CentreId,
	wests.CentreCode,
	wests.CentreName,
	west.ExamStateChangeAuditXml.value('(/exam/stateChange[newStateID=9]/changeDate)[1]', 'datetime')  AS DateComplete,
	west.ExamStateChangeAuditXml.value('(/exam/stateChange[newStateID=11]/changeDate)[1]', 'datetime') AS DateAssessed,
	west.ExamStateChangeAuditXml.value('(/exam/stateChange[newStateID=12]/changeDate)[1]', 'datetime') AS DateVerified,
	LTRIM(RTRIM(assessor.Forename)) + ' ' + LTRIM(RTRIM(assessor.Surname)) AS Assessor,
	LTRIM(RTRIM(verifier.Forename)) + ' ' + LTRIM(RTRIM(verifier.Surname)) AS InternalVerifier, 	
	ev.EV,
	scco.SCCO
	
FROM 
	dbo.WAREHOUSE_ExamSessionTable_Shreded wests
INNER JOIN
	dbo.WAREHOUSE_ExamSessionTable west ON west.ID = wests.ExamSessionID
LEFT OUTER JOIN
 dbo.WAREHOUSE_UserTable assessor ON wests.AssignedMarkerUserID = assessor.ID
LEFT OUTER JOIN
 dbo.WAREHOUSE_UserTable verifier ON wests.AssignedModeratorUserID = verifier.ID

LEFT OUTER JOIN
( SELECT 
	ur.CentreID,
	STUFF((SELECT '|'+ LTRIM(RTRIM(ut.Forename)) + ' ' + LTRIM(RTRIM(ut.Surname))
			FROM 
				AssignedUserRolesTable ur2
			INNER JOIN UserTable ut ON ur2.UserID = ut.ID
			WHERE 
				ur2.RoleID = ur.RoleID and ur2.centreID = ur.centreID
            ORDER BY userID
            FOR XML PATH(''), TYPE).value('.', 'nvarchar(max)'),1,1,'') AS EV
FROM 
	AssignedUserRolesTable ur
WHERE 
	RoleID = 112
GROUP BY 
	RoleID, CentreID ) ev
ON ev.CentreID = wests.CentreID
LEFT OUTER JOIN
( SELECT 
	ur.CentreID,
	STUFF((SELECT '|'+ LTRIM(RTRIM(ut.Forename)) + ' ' + LTRIM(RTRIM(ut.Surname))
			FROM 
				AssignedUserRolesTable ur2
			INNER JOIN UserTable ut ON ur2.UserID = ut.ID
			WHERE 
				ur2.RoleID = ur.RoleID and ur2.centreID = ur.centreID
            ORDER BY userID
            FOR XML PATH(''), TYPE).value('.', 'nvarchar(max)'),1,1,'') AS SCCO
FROM 
	AssignedUserRolesTable ur
WHERE 
	RoleID = 104
GROUP BY 
	RoleID, CentreID ) scco
ON scco.CentreID = wests.CentreID
WHERE 
	wests.SubmittedDate &gt;= @SearchDate
AND 
	wests.CentreId IN (@CentreName)
AND
	(wests.CentreCode LIKE ('%' + @CentreCode + '%')  OR @CentreCode IS NULL)
AND 
	wests.ExamName IN (@Exam)