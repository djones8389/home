USE OCR_SecureAssess_11_2

DECLARE @BatchName nvarchar(10)
SET @BatchName = 'FSUM'
DECLARE @SessionId nvarchar(10)
SET @SessionId = 'ONDEMAND'
DECLARE @SessionYear nvarchar(10)
SET @SessionYear = '0000';

select OCR_SecureAssess_11_2..ExamSessionTable.ID, KeyCode from OCR_SecureAssess_11_2..ExamSessionTable

	inner join OCR_SecureAssess_11_2..ScheduledExamsTable SCET on SCET.ID = OCR_SecureAssess_11_2..ExamSessionTable.ScheduledExamID

	where examState = 13
	--and OCR_SecureAssess_11_2..ExamSessionTable.ID not in (select ExamSessionId from OcrDataManagement..ExportDetails)
	and OCR_SecureAssess_11_2..ExamSessionTable.ID in (86067,82999,82221,82509,87749,87026,86680,86969,86831,84960,83295,86890,86118,88147,86826,86688,83434,83206,81925,83575,82147,82889,82501,81928,81927,82218,85140,82323,87751,83440,86073,82417,87027,86687,91452,83173,83576,86825,86691,86677,88146,85340,88190,81930,87043,82455,82104,86673,82329,82831,82927,83143,83360,82106,91454,82130,86489,88312,83000,82295,83146,86493,86967,88388,86968,88152,82322,82138,84973,83478,86068,86679,83320,87750,86671,84947,87754,84534,83480,82287,88116,85072,82123,86670,88245,87982,87764,84943,85212,86692,86895,82668,87744,86689,82224,86123,83476,83477,84130,85059,88647,87745,88145,85073,83147,88153,86926,82553,87741,86945,83137,84308,82285,82299,82430,86066,87742,92219,82371,84165,87168,86071,88120,88104,87746,88108,84535,81750,87756,84429,88187,88643,82292,82303,87103,88114,82427,87969,89420,88144,85339,85203,88135,82146,86119,82294,87086,82362,88156,86678,86115,82131,85094,84366,87761,86894,84941,83323,82998,89419,88164,84536,85007,84128,87357,82122,87762,86116,87763,83437,87748,88138,87759,86065,82248,87747,85202,86683,91455,88122,88539,83016,82428,87337,86120,84934,84942,82286,82372,85716,86070,82630,85095,81929,83577,87752,82100,82107) 
	--and OCR_SecureAssess_11_2..ExamSessionTable.ID not in (select ExamSessionId from OcrDataManagement..ExportDetails)
	and previousExamState <> 10
	AND
			SCET.examVersionRef LIKE @BatchName + '_' + @SessionId + '_' + @SessionYear + '_%';

USE OcrDataManagement			

select ExamSessionId from ExportDetails inner join Exports on Exports.ExportId = ExportDetails.ExportId 
  
  inner join OCR_SecureAssess_11_2..ExamSessionTable
  on OCR_SecureAssess_11_2..ExamSessionTable.ID = ExportDetails.ExamSessionId
  
  where BatchName = 'FSUM'
  and SessionId = 'ONDEMAND'
  and SessionYear = '0000'
  and SessionStatus = 16
  and OCR_SecureAssess_11_2..ExamSessionTable.ID in (86067,82999,82221,82509,87749,87026,86680,86969,86831,84960,83295,86890,86118,88147,86826,86688,83434,83206,81925,83575,82147,82889,82501,81928,81927,82218,85140,82323,87751,83440,86073,82417,87027,86687,91452,83173,83576,86825,86691,86677,88146,85340,88190,81930,87043,82455,82104,86673,82329,82831,82927,83143,83360,82106,91454,82130,86489,88312,83000,82295,83146,86493,86967,88388,86968,88152,82322,82138,84973,83478,86068,86679,83320,87750,86671,84947,87754,84534,83480,82287,88116,85072,82123,86670,88245,87982,87764,84943,85212,86692,86895,82668,87744,86689,82224,86123,83476,83477,84130,85059,88647,87745,88145,85073,83147,88153,86926,82553,87741,86945,83137,84308,82285,82299,82430,86066,87742,92219,82371,84165,87168,86071,88120,88104,87746,88108,84535,81750,87756,84429,88187,88643,82292,82303,87103,88114,82427,87969,89420,88144,85339,85203,88135,82146,86119,82294,87086,82362,88156,86678,86115,82131,85094,84366,87761,86894,84941,83323,82998,89419,88164,84536,85007,84128,87357,82122,87762,86116,87763,83437,87748,88138,87759,86065,82248,87747,85202,86683,91455,88122,88539,83016,82428,87337,86120,84934,84942,82286,82372,85716,86070,82630,85095,81929,83577,87752,82100,82107) 
  
  
  --select ExportedToIntegration, * from OCR_SecureAssess_11_2..WAREHOUSE_ExamSessionTable where ExamSessionID = 86067
  
  
 
 
 
 
 
use OCR_SecureAssess_11_2
  
  select ExamSessionTable.ID, KeyCode, examState, examVersionRef from ExamSessionTable
	inner join ScheduledExamsTable on ScheduledExamsTable.ID = ExamSessionTable.ScheduledExamID
	where examState = 13
	and ExamSessionTable.ID not in (select ExamSessionId from OcrDataManagement..ExportDetails)
	and previousExamState <> 10
	--and examVersionRef = 'Installation_Test_01/Practice'
	and examVersionRef = 'PQXX_18CBT_2014_m_05743_2'
	
	

	
-- Query to pull out required data for Unnamed Marksheet export

-- 3 parameters that calling user will pass in
DECLARE @BatchName nvarchar(10)
SET @BatchName = 'PQXX'
DECLARE @SessionId nvarchar(10)
SET @SessionId = '18CBT'
DECLARE @SessionYear nvarchar(10)
SET @SessionYear = '2014';

WITH temp AS (
	SELECT 
		row_number() over (partition by [ExamSessionTable].[ID] ORDER BY [ExamSessionTable].[ID]) as rownum
		,[ExamSessionTable].[ID] AS 'ExamSessionId'
		,[CentreTable].CentreCode AS 'CentreCode'
		,[CentreTable].CentreName AS 'CentreName'
		,[ScheduledExamsTable].examVersionRef AS 'ExamVersionRef'
		,[ScheduledExamsTable].examVersionName AS 'ExamVersionName'
		,[ExamSessionItemResponseTable].MarkerResponseData.query('//userId[1]/text()').value('.', 'int') AS ExaminerUserId
		,REPLACE(CONVERT(varchar(8),[UserTable].DOB,3), '/', '') AS CandidateDOB
		,[ExamSessionTable].ResultData.query('data(/exam/@userMark)').value('.', 'varchar(3)') as TotalMark
		,[ExamSessionTable].ResultData
		,[UserTable].Forename
		,[UserTable].Surname
		,SUBSTRING(Usertable.Forename, 1, 1) + ISNULL(SUBSTRING(Usertable.Middlename, 1, 1),'') + SUBSTRING(Usertable.Surname, 1, 1) AS Initials
		,[UserTable].Gender
		,REPLACE(CONVERT(varchar(8),[ExamStateChangeAuditTable].StateChangeDate,3), '/', '') AS TestDate
		,[UserTable].ULN AS ULN
		, StateChangeDate
		, NewState
		FROM [ExamSessionTable]
			INNER JOIN [ScheduledExamsTable]
				ON ScheduledExamsTable.ID = [ExamSessionTable].ScheduledExamID
			INNER JOIN CentreTable
				ON CentreTable.ID = [ScheduledExamsTable].CentreID
			INNER JOIN ExamSessionItemResponseTable
				ON ExamSessionItemResponseTable.ExamSessionID = [ExamSessionTable].ID
				AND ExamSessionItemResponseTable.ItemID = [ExamSessionTable].resultData.query('data(//item[1]/@id)').value('.', 'varchar(15)')
			INNER JOIN UserTable
				ON UserTable.ID = ExamSessionTable.UserID
			INNER JOIN [ExamStateChangeAuditTable]
				ON ExamStateChangeAuditTable.ExamSessionId = ExamSessionTable.Id
				AND ExamStateChangeAuditTable.NewState = 9
							
		WHERE
			[examState] = 13
			AND
			[ScheduledExamsTable].examVersionRef LIKE @BatchName + '_' + @SessionId + '_' + @SessionYear + '_%'
			and
			examsessiontable.Id in (82705,83057,82698,81983,82521)
	)

SELECT TOP 1000 

temp.ExamSessionId,
temp.CentreCode,
temp.CentreName,
temp.ExamVersionRef,
temp.ExamVersionName,
ISNULL(SUBSTRING(UserTable.Username,3,4), '0000') AS ExaminerNo,
temp.CandidateDOB,
temp.TotalMark,
temp.resultData,
temp.Forename, 
temp.Surname,
temp.Initials,
temp.Gender,
temp.TestDate, 
temp.ULN,
temp.StateChangeDate,
temp.NewState
 
FROM temp
	LEFT JOIN UserTable
ON UserTable.id = temp.ExaminerUserId 
WHERE rownum = 1
ORDER BY CentreCode, ExamVersionRef	

select exports.* from ExportDetails inner join exports on exports.exportid = ExportDetails.exportid where ExportDetails.exportid  = 947


--2014-12-02 11:23:14.213--
--1900-01-01 00:00:00.000--