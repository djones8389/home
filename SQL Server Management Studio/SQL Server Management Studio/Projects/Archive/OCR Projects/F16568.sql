use OCR_SecureAssess_11_2

SELECT EST.ID, EST.KeyCode, EST.examState, UT.Forename, UT.Surname, CT.CentreName, SCET.examName
	, est.Structurexml, scet.*
  FROM ExamSessionTable as EST

  Inner Join ScheduledExamsTable as SCET
  on SCET.ID = EST.ScheduledExamID
  
  Inner Join UserTable as UT
  on UT.ID = EST.UserID

  Inner Join CentreTable as CT
  on CT.ID = SCET.CentreID

where Forename = 'Daniel' and surname = 'Brown'




SELECT WEST.ExamSessionID, WEST.KeyCode, WUT.Forename, WUT.Surname, WUT.CandidateRef, WCT.CentreName, WSCET.examName, WEST.completionDate
	, WESTS.ExternalReference, West.structurexml, west.ExamStateChangeAuditXml
 FROM WAREHOUSE_ExamSessionTable as WEST

  Inner Join WAREHOUSE_ScheduledExamsTable as WSCET
  on WSCET.ID = WEST.WAREHOUSEScheduledExamID
  
  Inner Join WAREHOUSE_UserTable as WUT
  on WUT.ID = WEST.WAREHOUSEUserID

  Inner Join WAREHOUSE_CentreTable as WCT
  on WCT.ID = WSCET.WAREHOUSECentreID

  Inner Join WAREHOUSE_ExamSessionTable_Shreded as WESTS
  on WESTS.examSessionId = WEST.ID

--where wests.Forename = 'Daniel' and wests.surName = 'Brown' and wests.ExternalReference = 'NQEL_Annual_2014_a_04438_202_3'
	where WEST.KeyCode = '6HVW76B8'


--27238
--select * from UserTable as wests where wests.Forename = 'Daniel' and wests.surName = 'Brown'


select * from UserExamHistoryCompletionDateTrackerTable where userID = 11361 and examID = 225
Delete from UserExamHistoryVersionTrackerTable where userID = 11361 and examID = 225 and versionID = 1052