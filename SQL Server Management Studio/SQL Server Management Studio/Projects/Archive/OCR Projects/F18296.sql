--select examVersionRef,examName, ExternalReference
--	from WAREHOUSE_ExamSessionTable_Shreded
--		where KeyCode = '8ZFACTB8'

-- Query to pull out required data for Unnamed Marksheet export

-- 3 parameters that calling user will pass in
DECLARE @BatchName nvarchar(10)
SET @BatchName = 'FSUM'
DECLARE @SessionId nvarchar(10)
SET @SessionId = 'ONDEMAND'
DECLARE @SessionYear nvarchar(10)
SET @SessionYear = '0000';

WITH temp AS (
	SELECT 
		row_number() over (partition by [ExamSessionTable].[ID] ORDER BY [ExamSessionTable].[ID]) as rownum
		,[ExamSessionTable].[ID] AS 'ExamSessionId'
		,[CentreTable].CentreCode AS 'CentreCode'
		,[CentreTable].CentreName AS 'CentreName'
		,[ScheduledExamsTable].examVersionRef AS 'ExamVersionRef'
		,[ScheduledExamsTable].examVersionName AS 'ExamVersionName'
		,[ExamSessionItemResponseTable].MarkerResponseData.query('//userId[1]/text()').value('.', 'int') AS ExaminerUserId
		,REPLACE(CONVERT(varchar(8),[UserTable].DOB,3), '/', '') AS CandidateDOB
		,[ExamSessionTable].ResultData.query('data(/exam/@userMark)').value('.', 'varchar(3)') as TotalMark
		,[ExamSessionTable].ResultData
		,[UserTable].Forename
		,[UserTable].Surname
		,SUBSTRING(Usertable.Forename, 1, 1) + ISNULL(SUBSTRING(Usertable.Middlename, 1, 1),'') + SUBSTRING(Usertable.Surname, 1, 1) AS Initials
		,[UserTable].Gender
		,REPLACE(CONVERT(varchar(8),[ExamStateChangeAuditTable].StateChangeDate,3), '/', '') AS TestDate
		,[UserTable].ULN AS ULN

		FROM [ExamSessionTable]
			INNER JOIN [ScheduledExamsTable]
				ON ScheduledExamsTable.ID = [ExamSessionTable].ScheduledExamID
			INNER JOIN CentreTabl\e
				ON CentreTable.ID = [ScheduledExamsTable].CentreID
			INNER JOIN ExamSessionItemResponseTable
				ON ExamSessionItemResponseTable.ExamSessionID = [ExamSessionTable].ID
				AND ExamSessionItemResponseTable.ItemID = [ExamSessionTable].resultData.query('data(//item[1]/@id)').value('.', 'varchar(15)')
			INNER JOIN UserTable
				ON UserTable.ID = ExamSessionTable.UserID
			INNER JOIN [ExamStateChangeAuditTable]
				ON ExamStateChangeAuditTable.ExamSessionId = ExamSessionTable.Id
				AND ExamStateChangeAuditTable.NewState = 9
							
		WHERE
			[examState] = 13
			AND
			[ScheduledExamsTable].examVersionRef LIKE @BatchName + '_' + @SessionId + '_' + @SessionYear + '_%'
			AND
			KeyCode = '8ZFACTB8'
	)

SELECT TOP 1000 

temp.ExamSessionId,
temp.CentreCode,
temp.CentreName,
temp.ExamVersionRef,
temp.ExamVersionName,
ISNULL(SUBSTRING(UserTable.Username,3,4), '0000') AS ExaminerNo,
temp.CandidateDOB,
temp.TotalMark,
temp.resultData,
temp.Forename, 
temp.Surname,
temp.Initials,
temp.Gender,
temp.TestDate, 
temp.ULN
 
FROM temp
	LEFT JOIN UserTable
ON UserTable.id = temp.ExaminerUserId 
WHERE rownum = 1
ORDER BY CentreCode, ExamVersionRef