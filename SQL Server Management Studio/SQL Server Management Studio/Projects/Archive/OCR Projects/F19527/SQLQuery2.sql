select uct.*, Forename, Surname
 from UserExamHistoryCompletionDateTrackerTable as UCT

	inner join UserTable as UT
	on UT.ID = UCT.userID
	
where CandidateRef = 'AUTOd619af7aefc8455f89064a1c963677cd'
	and examID = 226
	
	
	
SELECT  U.UserID
		,u.CandidateRef as [CandidateRef]
        --,S.ExamID
        ,S.ExamVersionID
        ,s.ExternalReference
        ,s.examName
        ----,E.ExamSessionID
        --, AGT.AllowVersionRecycling
        --, AGT.MinResitTimeInDays
FROM  WAREHOUSE_ExamSessionTable E
	INNER JOIN WAREHOUSE_ScheduledExamsTable S
	ON E.WAREHOUSEScheduledExamID = S.ID
	INNER JOIN WAREHOUSE_UserTable U
	ON E.WAREHOUSEUserID = U.ID
	Inner Join [OCR_ItemBank_11.2]..AssessmentTable as AT
	on AT.ID = examVersionId
	Inner join [OCR_ItemBank_11.2]..AssessmentGroupTable as AGT
	on AGT.ID = AT.AssessmentGroupID
	
WHERE E.ExamSessionID  IN
                (
                SELECT  ExamSessionID
                FROM  UserExamHistoryVersionTrackerTable
                )
AND	E.ExamStateChangeAuditXML.exist('/exam/stateChange[newStateID = 6]') = 1	--Where exam has definitely been presented to a candidate--
and AGT.AllowVersionRecycling = 0	--Where IB does not allow duplicates to be sat--
and E.warehouseTime > '20 Feb 2014' --The date on which I started the job running again--
and U.CandidateRef <> ''			--Omit Users--
--and U.CandidateRef = 'AUTOd619af7aefc8455f89064a1c963677cd'
	group by userID, S.ExamVersionID, CandidateRef , s.ExternalReference,s.examName
		having COUNT(*) > 1 
		
order by UserId
	