exec sp_executesql N';WITH ES AS (
	SELECT 
		FES.ExamSessionKey
		,DT.FullDateAlternateKey + FES.CompletionTime [dateCompleted]
	FROM [dbo].[FactExamSessions] FES
		JOIN [dbo].[DimTime] DT ON DT.TimeKey = FES.CompletionDateKey
	WHERE 
	(DT.FullDateAlternateKey + FES.CompletionTime) BETWEEN @dateStartRange
			AND @dateEndRange
		AND (FES.ExamKey = @exam AND (CAST(@examVersion AS int) = 0 OR FES.ExamVersionKey = @examVersion))
		AND FES.QualificationKey IN (
			SELECT Value
			FROM [dbo].[fn_ParamsToList](@subjects)
			)
		AND FES.CentreKey IN (
			SELECT Value
			FROM [dbo].[fn_ParamsToList](@centres)
			)
), items AS
(
	SELECT FQR.ExamSessionKey
		,FQR.CPID
		,FQR.CPVersion
		,CONVERT(NVARCHAR(4000), DQ.QuestionName) [Name]
		,CONVERT(NVARCHAR(4000), FQR.Mark) [Actual Mark]
		,CONVERT(NVARCHAR(4000), DQ.TotalMark) [Total Mark]
		,CONVERT(NVARCHAR(4000), ISNULL(CASE DQ.QuestionTypeKey
					WHEN 11 THEN dbo.fn_CLR_StripHTML(FQR.DerivedResponse)
					ELSE FQR.ShortDerivedResponse
					END, '''')) [Response]
		,CONVERT(NVARCHAR(4000), ISNULL(CASE DQ.QuestionTypeKey
					WHEN 11 THEN dbo.fn_CLR_StripHTML(DQ.DerivedCorrectAnswer)
					ELSE DQ.ShortDerivedCorrectAnswer
					END, '''')) [Key]
		,CONVERT(NVARCHAR(4000), FQR.ViewingTime/1000) [ViewingTime]
		,CONVERT(NVARCHAR(4000), FQR.ItemPresentationOrder) [Presented Order]
		,(SELECT dbo.ag_CLR_Concat(attribval, '' | '') FROM DimQuestionMetaData DQM WHERE CPID = FQR.CPID AND CPVersion = FQR.CPVersion AND attrib = ''LO'') [LO]
		,(SELECT dbo.ag_CLR_Concat(attribval, '' | '') FROM DimQuestionMetaData DQM WHERE CPID = FQR.CPID AND CPVersion = FQR.CPVersion AND attrib = ''Unit'') [Unit] --As of 2014-05-21 there may be only one unit, but DB structure allows multiple units.
	FROM 
		ES
		JOIN [dbo].[FactQuestionResponses] FQR ON ES.ExamSessionKey = FQR.ExamSessionKey
		JOIN [dbo].[DimQuestions] DQ ON DQ.CPID = FQR.CPID AND DQ.CPVersion = FQR.CPVersion
	
), Metadata AS (
	SELECT 
		I.ExamSessionKey
		,DQM.CPID
		,attrib
		,attribType
		,attribval
		,externalId
		,deleted
	FROM 
		Items I
		JOIN DimQuestionMetaData DQM 
			ON I.CPID = DQM.CPID 
			AND I.CPVersion = DQM.CPVersion
	WHERE 
		attribType IS NOT NULL
)
SELECT 
	1 [Tag]
	,NULL [Parent]
	,NULL [report!1]
	,NULL [examSession!2!id]
	,NULL [examSession!2!dateCompleted]
	,NULL [examSession!2!keyCode]
	,NULL [examSession!2!forename]
	,NULL [examSession!2!surname]
	,NULL [examSession!2!candidateRef]
	,NULL [examSession!2!gender]
	,NULL [examSession!2!DOB]
	,NULL [examSession!2!subject]
	,NULL [examSession!2!centre]
	,NULL [examSession!2!test]
	,NULL [examSession!2!testForm]
	,NULL [examSession!2!actualMark]
	,NULL [examSession!2!totalMark]
	,NULL [examSession!2!grade]	
	,NULL [items!3]
	,NULL [item!5!id]
	,NULL [item!5!name]
	,NULL [item!5!actualMark]
	,NULL [item!5!totalMark]
	,NULL [item!5!response]
	,NULL [item!5!key]
	,NULL [item!5!viewingTime]
	,NULL [item!5!presentedOrder]
	,NULL [item!5!lo]
	,NULL [item!5!unit]
	,NULL [metadata!7]
	,NULL [tag!8!name]	
	,NULL [tag!8!type]
	,NULL [tag!8!id]
	,NULL [tag!8!deleted]
	,NULL [value!9]
	
UNION ALL

SELECT 
	2 [Tag]
	,1 [Parent]
	,NULL [report!1]
	,FES.ExamSessionKey ''@id''
	,ES.dateCompleted ''@dateCompleted''
	,FES.KeyCode ''@keyCode''
	,DC.Forename ''@forename''
	,DC.Surname ''@surname''
	,DC.CandidateRef ''@candidateRef''
	,DC.Gender ''@gender''
	,DC.DOB ''@DOB''
	,DQu.QualificationName ''@subject''
	,DCe.CentreName ''@centre''
	,DE.ExamName ''@test''
	,DEV.ExamVersionName ''@testForm''
	,FES.UserMarks ''@actualMark''
	,FES.TotalMarksAvailable ''@totalMark''
	,DG.Grade ''@grade''	
	,NULL [items!3]
	,NULL [item!5!id]
	,NULL [item!5!name]
	,NULL [item!5!actualMark]
	,NULL [item!5!totalMark]
	,NULL [item!5!response]
	,NULL [item!5!key]
	,NULL [item!5!viewingTime]
	,NULL [item!5!presentedOrder]
	,NULL [item!5!lo]
	,NULL [item!5!unit]
	,NULL [metadata!7]
	,NULL [tag!8!name]	
	,NULL [tag!8!type]
	,NULL [tag!8!id]
	,NULL [tag!8!deleted]
	,NULL [value!9]
FROM ES
	JOIN FactExamSessions FES ON ES.ExamSessionKey = FES.ExamSessionKey
	LEFT JOIN [dbo].[DimCandidate] DC ON DC.CandidateKey = FES.CandidateKey
	LEFT JOIN [dbo].[DimQualifications] DQu ON DQu.QualificationKey = FES.QualificationKey
	LEFT JOIN [dbo].[DimCentres] DCe ON DCe.CentreKey = FES.CentreKey
	LEFT JOIN [dbo].[DimExams] DE ON DE.ExamKey = FES.ExamKey
	LEFT JOIN [dbo].[DimExamVersions] DEV ON DEV.ExamKey = FES.ExamKey
		AND DEV.ExamVersionKey = FES.ExamVersionKey
	LEFT JOIN [dbo].[DimGrades] DG ON DG.GradeKey = FES.GradeKey

UNION ALL

SELECT 
	3 [Tag]
	,2 [Parent]
	,NULL [report!1]
	,ExamSessionKey [examSession!2!id]
	,NULL [examSession!2!dateCompleted]
	,NULL [examSession!2!keyCode]
	,NULL [examSession!2!forename]
	,NULL [examSession!2!surname]
	,NULL [examSession!2!candidateRef]
	,NULL [examSession!2!gender]
	,NULL [examSession!2!DOB]
	,NULL [examSession!2!subject]
	,NULL [examSession!2!centre]
	,NULL [examSession!2!test]
	,NULL [examSession!2!testForm]
	,NULL [examSession!2!actualMark]
	,NULL [examSession!2!totalMark]
	,NULL [examSession!2!grade'']	
	,'''' [items!3]
	,NULL [item!5!id]
	,NULL [item!5!name]
	,NULL [item!5!actualMark]
	,NULL [item!5!totalMark]
	,NULL [item!5!response]
	,NULL [item!5!key]
	,NULL [item!5!viewingTime]
	,NULL [item!5!presentedOrder]
	,NULL [item!5!lo]
	,NULL [item!5!unit]
	,NULL [metadata!7]
	,NULL [tag!8!name]	
	,NULL [tag!8!type]
	,NULL [tag!8!id]
	,NULL [tag!8!deleted]
	,NULL [value!9]
FROM ES

UNION ALL

SELECT 
	5
	,3
	,NULL [report!1]
	,ExamSessionKey [examSession!2!id]
	,NULL [examSession!2!dateCompleted]
	,NULL [examSession!2!keyCode]
	,NULL [examSession!2!forename]
	,NULL [examSession!2!surname]
	,NULL [examSession!2!candidateRef]
	,NULL [examSession!2!gender]
	,NULL [examSession!2!DOB]
	,NULL [examSession!2!subject]
	,NULL [examSession!2!centre]
	,NULL [examSession!2!test]
	,NULL [examSession!2!testForm]
	,NULL [examSession!2!actualMark]
	,NULL [examSession!2!totalMark]
	,NULL [examSession!2!grade'']
	,NULL
	,CPID ''item/@id''
	,[Name] ''item/@name''
	,CAST([Actual Mark] AS FLOAT)*CAST([Total Mark] AS FLOAT) ''item/@actualMark''
	,CAST([Total Mark] AS FLOAT) ''item/@totalMark''
	,[Response] ''item/@response''
	,[Key] ''item/@key''
	,CAST([ViewingTime] AS FLOAT) ''item/@viewingTime''
	,[Presented Order] ''item/@presentedOrder''
	,[LO] ''item/@lo''
	,[Unit] ''item/@unit''
	,NULL [metadata!7]
	,NULL [tag!8!name]	
	,NULL [tag!8!type]
	,NULL [tag!8!id]
	,NULL [tag!8!deleted]
	,NULL [value!9]
FROM items

UNION ALL

SELECT DISTINCT
	7 [Tag]
	,5 [Parent]
	,NULL [report!1]
	,ExamSessionKey [examSession!2!id]
	,NULL [examSession!2!dateCompleted]
	,NULL [examSession!2!keyCode]
	,NULL [examSession!2!forename]
	,NULL [examSession!2!surname]
	,NULL [examSession!2!candidateRef]
	,NULL [examSession!2!gender]
	,NULL [examSession!2!DOB]
	,NULL [examSession!2!subject]
	,NULL [examSession!2!centre]
	,NULL [examSession!2!test]
	,NULL [examSession!2!testForm]
	,NULL [examSession!2!actualMark]
	,NULL [examSession!2!totalMark]
	,NULL [examSession!2!grade'']
	,NULL
	,CPID [item!2!id]
	,NULL [item!2!name]
	,NULL [item!2!actualMark]
	,NULL [item!2!totalMark]
	,NULL [item!2!response]
	,NULL [item!2!key]
	,NULL [item!2!viewingTime]
	,NULL [item!2!presentedOrder]
	,NULL [item!2!lo]
	,NULL [item!2!unit]
	,'''' [metadata!7]
	,NULL [tag!8!name]	
	,NULL [tag!8!type]
	,NULL [tag!8!id]
	,NULL [tag!8!deleted]
	,NULL [value!9]
FROM Metadata

UNION ALL

SELECT DISTINCT
	8 [Tag]
	,7 [Parent]
	,NULL [report!1]
	,ExamSessionKey [examSession!2!id]
	,NULL [examSession!2!dateCompleted]
	,NULL [examSession!2!keyCode]
	,NULL [examSession!2!forename]
	,NULL [examSession!2!surname]
	,NULL [examSession!2!candidateRef]
	,NULL [examSession!2!gender]
	,NULL [examSession!2!DOB]
	,NULL [examSession!2!subject]
	,NULL [examSession!2!centre]
	,NULL [examSession!2!test]
	,NULL [examSession!2!testForm]
	,NULL [examSession!2!actualMark]
	,NULL [examSession!2!totalMark]
	,NULL [examSession!2!grade'']
	,NULL
	,CPID [item!2!id]
	,NULL [item!2!name]
	,NULL [item!2!actualMark]
	,NULL [item!2!totalMark]
	,NULL [item!2!response]
	,NULL [item!2!key]
	,NULL [item!2!viewingTime]
	,NULL [item!2!presentedOrder]
	,NULL [item!2!lo]
	,NULL [item!2!unit]
	,''''
	,attrib AS [tag!8!name]
	,attribType AS [tag!8!type]
	,externalId AS [tag!8!id]
	,deleted AS [tag!8!deleted]
	,NULL [value!9]
FROM Metadata

UNION ALL

SELECT DISTINCT
	9 [Tag]
	,8 [Parent]
	,NULL [report!1]
	,ExamSessionKey [examSession!2!id]
	,NULL [examSession!2!dateCompleted]
	,NULL [examSession!2!keyCode]
	,NULL [examSession!2!forename]
	,NULL [examSession!2!surname]
	,NULL [examSession!2!candidateRef]
	,NULL [examSession!2!gender]
	,NULL [examSession!2!DOB]
	,NULL [examSession!2!subject]
	,NULL [examSession!2!centre]
	,NULL [examSession!2!test]
	,NULL [examSession!2!testForm]
	,NULL [examSession!2!actualMark]
	,NULL [examSession!2!totalMark]
	,NULL [examSession!2!grade'']
	,NULL
	,CPID [item!2!id]
	,NULL [item!2!name]
	,NULL [item!2!actualMark]
	,NULL [item!2!totalMark]
	,NULL [item!2!response]
	,NULL [item!2!key]
	,NULL [item!2!viewingTime]
	,NULL [item!2!presentedOrder]
	,NULL [item!2!lo]
	,NULL [item!2!unit]
	,''''
	,attrib AS [tag!8!name]
	,NULL AS [tag!8!type]
	,NULL AS [tag!8!id]
	,NULL AS [tag!8!deleted]
	,attribval AS [value!9]
FROM Metadata

ORDER BY [examSession!2!id],[item!5!id], [tag!8!name], [tag]
FOR XML EXPLICIT
OPTION (MAXRECURSION 0)',N'@subjects nvarchar(3),@centres nvarchar(2),@dateStartRange nvarchar(19),@dateEndRange nvarchar(19),@exam nvarchar(3),@examVersion nvarchar(1)',@subjects=N'511',@centres=N'47',@dateStartRange=N'1994-10-29 00:00:00',@dateEndRange=N'2014-11-01 00:00:00',@exam=N'476',@examVersion=N'0'