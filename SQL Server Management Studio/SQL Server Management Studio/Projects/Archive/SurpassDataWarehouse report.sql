declare @centreCode nvarchar(50) = 'C001032'
declare @warehouseTime datetime = '01 May 2014'
--I need a list of all exams sat by the centres PERA TRAINING Retail (C000952) and NEXT Retail Academy (C001032) since 01/05/2014 until today.--
select 
	QualificationName
	, ExamName
	, ExamReference
	, dev.ExamVersionName
	, dev.ExamVersionReference
	, KeyCode
	, Forename
	, Surname
	, case FinalExamState when 10 then 'Voided' else cast(Pass as nvarchar(10)) end as [Grade]
	, Grade
	, CandidateRef
	, ULN
	, Duration
	, FES.Pass
	, UserMarks
	, CentreName
	, CentreCode
	, CompletionTime

 from FactExamSessions as FES

	inner join DimCentres as DC
	on DC.CentreKey = FES.CentreKey
	
	inner join DimExams as DE
	on DE.ExamKey = FES.ExamKey
	
	inner join DimQualifications as DQ
	on DQ.QualificationKey = FES.QualificationKey
	
	inner join DimCandidate as DCan
	on  DCan.CandidateKey = FES.CandidateKey 
	
	inner join DimExamVersions as DEV
	on DEV.ExamVersionKey = FES.ExamVersionKey
	
	inner join DimGrades as DG
	on DG.GradeKey = FES.GradeKey
	
where CentreCode = @centreCode
	and WarehouseTime >= @warehouseTime
	
--	order by UserMarks