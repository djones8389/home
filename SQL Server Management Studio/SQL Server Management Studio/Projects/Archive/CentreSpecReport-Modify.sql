--select top 1 clientinformation from ExamSessionTable order by id desc

--USE TCNTechnicalAudit_SecureAssess

DECLARE @CentreName nvarchar(150) = 'Peterborough PM'

select 
	CentreName
	,downloadInformation.value('data(/downloads/information/machineName)[1]', 'nvarchar(max)') as 'Computer Name'
	,KeyCode
	,clientInformation.value('data(/clientInformation/systemConfiguration/dotNet)[1]', 'nvarchar(max)') as 'FULL.Net Version'
	,clientInformation.value('data(/clientInformation/systemConfiguration/dotNet/version)[last()]', 'nvarchar(max)') as '.Net Version'
	,clientInformation.value('data(/clientInformation/systemConfiguration/flashPlayer/version)[1]', 'nvarchar(max)') as 'Flash Version'
	,clientInformation.value('data(/clientInformation/systemConfiguration/secureClient/CurrentVersion)[1]', 'nvarchar(max)') as 'SecureClient Version'
	,clientInformation.value('data(/clientInformation/systemConfiguration/environment/operatingSystem/version)[1]', 'nvarchar(max)') as 'Operating System'
	,clientInformation.value('data(/clientInformation/systemConfiguration/environment/operatingSystem/servicePack)[1]', 'nvarchar(max)') as 'O/S Service Pack'
	,clientInformation.value('data(/clientInformation/systemConfiguration/environment/processor/name)[1]', 'nvarchar(max)') as 'CPU'
	,clientInformation.value('data(/clientInformation/systemConfiguration/environment/memory/installedPhysicalMemory)[1]', 'nvarchar(max)') as 'RAM'
	,clientInformation.value('data(/clientInformation/systemConfiguration/environment/hardDisk/drives/drive/freeSpace)[1]', 'nvarchar(max)') as 'Free HD Space'
	,clientInformation.query('data(/clientInformation/systemConfiguration/windowsservices)[1]') as 'windowsservices'
  from WAREHOUSE_ExamSessionTable as WEST

	inner join WAREHOUSE_ScheduledExamsTable as WSCET
	on WSCET.ID = WEST.WAREHOUSEScheduledExamID
	
	inner join WAREHOUSE_CentreTable as WCT
	on WCT.ID = WSCET.WAREHOUSECentreID

	Where CentreName != @CentreName
		
	order by CentreName asc;
