drop database DJTempDB;

drop table DJTempDB.dbo.DJTempTable;

use master;

create database DJTempDB;

create table DJTempDB.dbo.DJTempTable
(
	name nvarchar(1000)
	,pageTableID nvarchar(1000)
	,supportingFiles image

);

insert into DJTempDB.dbo.DJTempTable

select 
	 Name
	, PageTable.ID
	, PageTable.SupportingFiles
	
	
	
	from SQA_CPProjectAdmin..ItemGraphicTable
		
	inner join SQA_CPProjectAdmin..ProjectListTable
	on ProjectListTable.ID = SUBSTRING(ItemGraphicTable.ParentID, 0 , CHARINDEX(N'P', ItemGraphicTable.ParentID)) 

	inner join SQA_CPProjectAdmin..PageTable
	on PageTable.id = SUBSTRING(ItemGraphicTable.ID, 0, CHARINDEX('S',ItemGraphicTable.ID))

where orF = ''
	and ali = ''
	and Name not like '%btl%';
	
	
select * from DJTempDB.dbo.DJTempTable;
