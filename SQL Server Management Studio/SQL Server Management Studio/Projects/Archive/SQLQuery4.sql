----------------WAREHOUSE_ExamSessionTable - ResultData----------------

DECLARE @ExamUserPercentage[decimal](6, 3),
		@NewValue int = 190,
		@examSessionID int = 107841;
		
select ID, resultData, resultDataFull from WAREHOUSE_ExamSessionTable
where id = @examSessionID

Delete FROM [SADB].[dbo].[WAREHOUSE_ExamStateAuditTable]
  Where WarehouseExamSessionID = @examSessionID  

update WAREHOUSE_ExamSessionTable
set resultData.modify('replace value of (/exam/@totalMark)[1] with sql:variable("@NewValue") ')    
where ID = @examSessionID

Delete FROM [SADB].[dbo].[WAREHOUSE_ExamStateAuditTable]
  Where WarehouseExamSessionID = @examSessionID  

update WAREHOUSE_ExamSessionTable
set resultData.modify('replace value of (/exam/section/@totalMark)[1] with sql:variable("@NewValue") ')    
where ID = @examSessionID	

 Delete FROM [SADB].[dbo].[WAREHOUSE_ExamStateAuditTable]
  Where WarehouseExamSessionID = @examSessionID    

update WAREHOUSE_ExamSessionTable
SET @ExamUserPercentage = CAST(((resultdata.value('(/exam/@userMark)[1]', 'decimal(13, 10)') / resultdata.value('(/exam/@totalMark)[1]', 'decimal(13, 10)')) * 100) AS [decimal](6, 3))
where ID = @examSessionID

 Delete FROM [SADB].[dbo].[WAREHOUSE_ExamStateAuditTable]
  Where WarehouseExamSessionID = @examSessionID  

update WAREHOUSE_ExamSessionTable
SET  resultdata.modify('replace value of (/exam/@userPercentage)[1] with sql:variable("@ExamUserPercentage")')
where ID = @examSessionID

 Delete FROM [SADB].[dbo].[WAREHOUSE_ExamStateAuditTable]
  Where WarehouseExamSessionID = @examSessionID  

update WAREHOUSE_ExamSessionTable
SET  resultdata.modify('replace value of (/exam/section/@userPercentage)[1] with sql:variable("@ExamUserPercentage")')
where ID = @examSessionID
 
Select *
FROM [SADB].[dbo].[WAREHOUSE_ExamStateAuditTable]
  Where WarehouseExamSessionID = @examSessionID



--Lets lower the totalMark for Q1--


update WAREHOUSE_ExamSessionTable
SET  resultdata.modify('replace value of (/exam/section/item[@id = "365P451" and @version = "16"]/@totalMark)[1] with 3')
where ID = @examSessionID

--Lower it for the section--

update WAREHOUSE_ExamSessionTable
SET  resultdata.modify('replace value of (/exam/section/@totalMark)[1] with 108')
where ID = @examSessionID

select ID, resultData, resultDataFull from WAREHOUSE_ExamSessionTable
where id = @examSessionID