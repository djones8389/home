----select * from ScheduledPackageCandidateExams where ScheduledExamRef in ('HSPKQQ99','TRD5AE99','TTEPHU99')



--west.id(607378,6073800)


begin tran

DECLARE @ExamUserPercentage [decimal] (6, 3);
DECLARE @ESID int = 607378

select ID, resultData, resultDatafull from WAREHOUSE_ExamSessionTable	where id = @ESID;

	delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @ESID
	SELECT @ExamUserPercentage = CAST(((resultdata.value('(/exam/@userMark)[1]', 'decimal(13, 10)') / resultdata.value('(/exam/@totalMark)[1]', 'decimal(13, 10)')) * 10) AS [decimal](6, 3))
	FROM WAREHOUSE_ExamSessionTable_Shreded
	WHERE examSessionId = @ESID;	
	
	UPDATE WAREHOUSE_ExamSessionTable_Shreded
	SET resultdata.modify('replace value of (/exam/@userPercentage)[1] with sql:variable("@ExamUserPercentage")')
	WHERE examSessionId = @ESID;

	UPDATE WAREHOUSE_ExamSessionTable
	SET resultdata.modify('replace value of (/exam/@userPercentage)[1] with sql:variable("@ExamUserPercentage")')
	WHERE Id = @ESID;
	
	UPDATE WAREHOUSE_ExamSessionTable
	SET resultdata.modify('replace value of (/exam/@userPercentage)[1] with sql:variable("@ExamUserPercentage")')
	WHERE Id = @ESID;

	UPDATE WAREHOUSE_ExamSessionTable
	SET resultDataFull.modify('replace value of (/assessmentDetails/assessment/@userPercentage)[1] with sql:variable("@ExamUserPercentage")')
	WHERE ID = @ESID;

select ID, resultData, resultDatafull from WAREHOUSE_ExamSessionTable	where id = @ESID;

rollback