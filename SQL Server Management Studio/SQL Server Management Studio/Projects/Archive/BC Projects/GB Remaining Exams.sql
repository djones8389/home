USE BRITISHCOUNCIL_SecureAssess

select west.ID, wests.KeyCode, qualificationID, examId, candidateRef, west.warehouseTime, examName
	from WAREHOUSE_ExamSessionTable_Shreded as wests
		
		inner join WAREHOUSE_ExamSessionTable as west
		on west.id =  wests.examSessionId
	
where --wests.candidateRef = 'TUTOR027-CORTE23' and
	wests.KeyCode in ('T5CEQ599 ','3JGC6K99 ','Z5HS2V99 ','HQGQWE99 ','4BDR9Q99 ','BLNDA599 ','EAFN8W99 ','VQPYFN99 ','FQ526T99 ','YWNZYF99 ')
	 --and examId in (272,271,330,274,273)
	order by candidateRef;

USE BritishCouncil_TestPackage

select * from ScheduledPackageCandidateExams as SPCE

	inner join ScheduledPackageCandidates as SPC
	on SPC.ScheduledPackageCandidateId =  SPCE.Candidate_ScheduledPackageCandidateId

where ScheduledExamRef in ('T5CEQ599 ','3JGC6K99 ','Z5HS2V99 ','HQGQWE99 ','4BDR9Q99 ','BLNDA599 ','EAFN8W99 ','VQPYFN99 ','FQ526T99 ','YWNZYF99 ')
