declare @esid int = 684420;

select ID, examState, KeyCode, ExportToSecureMarker
	from ExamSessionTable
		where examState = 99
		 and LEN(keycode) = 8
		 and ID = @esid;		
		
Select * from StateManagementException where ExamSessionID = @esid;
Select * from ExamStateChangeAuditTable where ExamSessionID = @esid;
select * from WAREHOUSE_ExamSessionTable where ExamSessionID = @esid;
select * from WAREHOUSE_ExamSessionDocumentTable where warehouseExamSessionID = 607408;

--update ExamSessionTable
--set previousExamState = examState, examState = 9 
--where id = @esid;


SELECT ExamSessionDocumentTable.ID
              ,ExamSessionDocumentTable.ExamSessionID
              ,ExamSessionDocumentTable.ItemID
              ,ExamSessionDocumentTable.DocumentName
              ,ExamSessionDocumentTable.Document
              ,ExamSessionDocumentTable.UploadDate
              ,NULL
       FROM ExamSessionDocumentTable
       INNER JOIN ExamSessionTable
       ON ExamSessionID = ExamSessionTable.ID
       WHERE ExamSessionDocumentTable.ID IN
              (SELECT ID
              FROM WAREHOUSE_ExamSessionDocumentTable)
       AND ExamState <> 13;
