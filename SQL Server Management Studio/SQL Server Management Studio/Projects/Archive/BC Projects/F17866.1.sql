/*
3524
3520
3521
3519
3536
*/

select id, name, image, ProjectId from  ProjectManifestTable where name in (

	(	
	select name from ProjectManifestTable where ProjectId = 3537 and Image is null
	)

)
AND image IS NOT NULL
order by ProjectId


--select
-- ID, Name, ProjectId, Image 
--	into #3524test
--		from ProjectManifestTable 
--			where projectid= 3524

--select
-- ID, Name, ProjectId, Image 
--	into #3522test
--		from ProjectManifestTable 
--			where projectid= 3522
			
--select
-- ID, Name, ProjectId, Image 
--	into #3520test
--		from ProjectManifestTable 
--			where projectid= 3520

--select
-- ID, Name, ProjectId, Image 
--	into #3521test
--		from ProjectManifestTable 
--			where projectid= 3521
			
--select
-- ID, Name, ProjectId, Image 
--	into #3519test
--		from ProjectManifestTable 
--			where projectid= 3519
			
--select
-- ID, Name, ProjectId, Image 
--	into #3536test
--		from ProjectManifestTable 
--			where projectid= 3536
			
select
 ID, Name, ProjectId, Image 
	into #3521test
		from ProjectManifestTable 
			where projectid= 3521


drop table #3524test
drop table #3520test
drop table #3521test
drop table #3519test
drop table #3536test
drop table #3522test
drop table #3521test



begin tran

select image from ProjectManifestTable where image is null	and ProjectId = 3537
--select ID, name, image from ProjectManifestTable where ProjectId = 3537

update ProjectManifestTable
set ProjectManifestTable.Image = #3521test.Image
from ProjectManifestTable
	
	inner join #3521test
	on #3521test.name = ProjectManifestTable.name

where ProjectManifestTable.ProjectId = 3537
	and #3521test.name is not null
	and ProjectManifestTable.Image is null

--select ID, name, image from ProjectManifestTable where ProjectId = 3537
select * from ProjectManifestTable where image is null	and ProjectId = 3537
rollback