SELECT UniqueResponses.id AS URID, *
FROM CandidateGroupResponses AS Original
INNER JOIN UniqueGroupResponses
ON Original.UniqueGroupResponseID = UniqueGroupResponses.ID
INNER JOIN UniqueGroupResponseLinks
ON UniqueGroupResponseLinks.UniqueGroupResponseID = UniqueGroupResponses.ID
INNER JOIN UniqueResponses
ON UniqueGroupResponseLinks.UniqueResponseId = UniqueResponses.ID
LEFT JOIN AssignedGroupMarks
ON AssignedGroupMarks.UniqueGroupResponseId = UniqueGroupResponses.ID

WHERE Original.CandidateExamVersionID = 13868



SELECT *
FROM UniqueGroupResponses
WHERE 
NOT EXISTS (SELECT ID FROM AssignedGroupMarks WHERE AssignedGroupMarks.UniqueGroupResponseId = UniqueGroupResponses.ID) AND 
[CHECKSUM] = N'ᪧ獈ఙ竺ଋ꬈喝�㕁'

SELECT *
FROM UniqueGroupResponses AS Main
LEFT JOIN AssignedGroupMarks
ON AssignedGroupMarks.UniqueGroupResponseId = Main.ID
WHERE EXISTS(
	SELECT *
	FROM UniqueGroupResponses AS Sub
	WHERE Main.CheckSum = Sub.CheckSum
	AND Main.ID <> Sub.ID
	AND Main.GroupDefinitionID = Sub.GroupDefinitionID
)
ORDER BY Main.GroupDefinitionID, Main.CheckSum, AssignedGroupMarks.ID DESC