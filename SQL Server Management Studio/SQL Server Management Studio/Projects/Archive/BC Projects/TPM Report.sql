select distinct
	SPC.IsVoided
	, P.Name
	, FirstName
	, LastName
	, SurpassCandidateRef
	, PackageScore
	--, WESTS.centreName
	, SP.CenterName
	, SPC.DateCompleted
	--,*
from ScheduledPackageCandidates as SPC

	left join ScheduledPackageCandidateExams as SPCE
	on SPCE.Candidate_ScheduledPackageCandidateId = SPC.ScheduledPackageCandidateId

	left join PackageExams as PE
	on PE.PackageExamId = SPCE.PackageExamId
	
	left join Packages as P
	on P.PackageId = PE.PackageId	

	inner join ScheduledPackages as SP
	on SP.ScheduledPackageId = SPC.ScheduledPackage_ScheduledPackageId

where SPC.DateCompleted >= '01 May 2014'
and SPC.DateCompleted <= '31 May 2014'
	order by SurpassCandidateRef
