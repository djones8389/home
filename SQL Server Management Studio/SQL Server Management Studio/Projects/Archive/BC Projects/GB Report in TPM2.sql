select distinct
	SPC.IsVoided
	, P.Name
	, FirstName
	, LastName
	, SurpassCandidateRef
	, PackageScore
	, WESTS.centreName
	--,CentreName?
	, SPC.DateCompleted
	--,*
 from ScheduledPackageCandidates as SPC
 
	left join ScheduledPackageCandidateExams as SPCE
	on SPCE.Candidate_ScheduledPackageCandidateId = SPC.ScheduledPackageCandidateId
 
	inner join PackageExams as PE
	on PE.PackageExamId = SPCE.PackageExamId
	
	inner join Packages as P
	on P.PackageId = PE.PackageId
	
	inner join BRITISHCOUNCIL_SecureAssess..WAREHOUSE_ExamSessionTable_Shreded as WESTS
	--on WESTS.KeyCode collate Latin1_General_CI_AS = SurpassExamRef
	on wests.candidateRef collate Latin1_General_CI_AS = SurpassCandidateRef
	
where SPC.DateCompleted >= '01 Jan 2012'