use BritishCouncil_TestPackage

declare @incorrectname nvarchar(30) = 'SDtomel',
	  @correctname nvarchar(30) = 'Strobel'

select * from ScheduledPackageCandidates where LastName = @incorrectname --or FirstName = 'Radoniqui'

begin tran

update ScheduledPackageCandidates 
set LastName = @correctname
where LastName = @incorrectname

rollback

select * from ScheduledPackageCandidates where LastName = @correctname --or FirstName = 'Radoniqui'


select * from [BRITISHCOUNCIL_SecureAssess].[dbo].WAREHOUSE_ExamSessionTable_Shreded  where surName = @incorrectname

begin tran

update [BRITISHCOUNCIL_SecureAssess].[dbo].WAREHOUSE_ExamSessionTable_Shreded 
set surName = @correctname
where surName = @incorrectname

rollback

select * from [BRITISHCOUNCIL_SecureAssess].[dbo].WAREHOUSE_ExamSessionTable_Shreded  where surName = @correctname


select * from [BRITISHCOUNCIL_SecureAssess].[dbo].UserTable  where surName = @incorrectname

begin tran

update [BRITISHCOUNCIL_SecureAssess].[dbo].UserTable 
set surName = @correctname
where surName = @incorrectname

rollback

select * from [BRITISHCOUNCIL_SecureAssess].[dbo].UserTable  where surName = @correctname


select * from [BRITISHCOUNCIL_SecureAssess].[dbo].WAREHOUSE_UserTable where surName = @incorrectname

begin tran

update [BRITISHCOUNCIL_SecureAssess].[dbo].WAREHOUSE_UserTable
set surName = @correctname
where surName = @incorrectname

rollback

select * from [BRITISHCOUNCIL_SecureAssess].[dbo].WAREHOUSE_UserTable where surName = @correctname




select * from BRITISHCOUNCIL_SecureAssess..UserTable where forename = 'SDtomel' 
select * from BRITISHCOUNCIL_SecureAssess..UserTable where surname = 'SDtomel' 

select * from BRITISHCOUNCIL_SecureAssess..WAREHOUSE_UserTable where forename = 'SDtomel' 
select * from BRITISHCOUNCIL_SecureAssess..WAREHOUSE_UserTable where surname = 'SDtomel' 

select * from [BRITISHCOUNCIL_SecureAssess].[dbo].WAREHOUSE_ExamSessionTable_Shreded  where forename  = 'SDtomel' 
select * from [BRITISHCOUNCIL_SecureAssess].[dbo].WAREHOUSE_ExamSessionTable_Shreded  where surName  = 'SDtomel' 

select * from [BRITISHCOUNCIL_SecureAssess].[dbo].WAREHOUSE_ExamSessionTable_Shreded  where forename = 'Sabine' and surname = 'SDtomel' 
--select * from [BRITISHCOUNCIL_SecureAssess].[dbo].WAREHOUSE_ExamSessionTable_Shreded  where surName = 'Sabine'


select * from ScheduledPackageCandidates where LastName = 'Sabine'  --or FirstName = 'Radoniqui'
select * from ScheduledPackageCandidates where FirstName = 'Sabine' and LastName = 'Sabine'

select * from ScheduledPackageCandidates where LastName =  'SDtomel'  --or FirstName = 'Radoniqui'
select * from ScheduledPackageCandidates where LastName  = 'SDtomel'  --or FirstName = 'Radoniqui'