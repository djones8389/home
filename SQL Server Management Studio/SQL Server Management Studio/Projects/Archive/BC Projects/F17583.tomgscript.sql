BEGIN TRAN

DECLARE @myUniqueResponses TABLE (ID INT)

/*
100192
469535
469538
469536
469537

*/

--insert each of the unique response IDs here
INSERT INTO @myUniqueResponses
VALUES (100192)

INSERT INTO @myUniqueResponses
VALUES (469535)

INSERT INTO @myUniqueResponses
VALUES (469538)

INSERT INTO @myUniqueResponses
VALUES (469536)

INSERT INTO @myUniqueResponses
VALUES (469537)

DECLARE @myUniqueResponseDocuments TABLE 
(
      ID                            INT,
      Name                    NVARCHAR(100),
      Blob                    VARBINARY(MAX),
      UniqueResponseID  BIGINT,
      BlobType                NVARCHAR(20),
      ExternalDocID           INT,
      UploadedDate            DATETIME,
      [FileName]              NVARCHAR(200)
)

PRINT 'Delete documents'
DELETE FROM UniqueResponseDocuments
OUTPUT DELETED.* INTO @myUniqueResponseDocuments
WHERE UniqueResponseID IN (SELECT ID
FROM @myUniqueResponses)

SELECT * FROM @myUniqueResponseDocuments

DECLARE @myAssignedItemMarks TABLE
(
      GroupMarkId                   INT,
      UniqueResponseId        BIGINT,
      AnnotationData                XML,
      AssignedMark                  DECIMAL(18,10),
      MarkingDeviation        DECIMAL(18,10),
      MarkedMetadataResponse  XML
)

PRINT 'Delete assigned item marks'
DELETE AssignedItemMarks
OUTPUT DELETED.* INTO @myAssignedItemMarks
FROM AssignedItemMarks
INNER JOIN @myUniqueResponses UR
ON UR.ID = AssignedItemMarks.UniqueResponseId

SELECT * FROM @myAssignedItemMarks

DECLARE @myAssignedGroupMarks TABLE
(
      ID                                  INT,
      UserId                              INT,
      UniqueGroupResponseID   BIGINT,
      [Timestamp]                   DATETIME,
      AssignedMark                  DECIMAL(18,10),
      IsConfirmedMark               BIT,
      UserName                      NVARCHAR(50),
      FullName                      NVARCHAR(101),
      Surname                             NVARCHAR(50),
      Disregarded                   BIT,
      MarkingDeviation        DECIMAL(18,10),
      MarkingMethodId               INT,
      GroupDefinitionID				INT,
      IsEscalated                   BIT

)

PRINT 'Delete assigned group marks'
DELETE AssignedGroupMarks
OUTPUT DELETED.* INTO @myAssignedGroupMarks
FROM AssignedGroupMarks
INNER JOIN UniqueGroupResponses
ON UniqueGroupResponses.ID = AssignedGroupMarks.UniqueGroupResponseId
INNER JOIN UniqueGroupResponseLinks
ON UniqueGroupResponses.ID = UniqueGroupResponseLinks.UniqueGroupResponseID
WHERE UniqueGroupResponseLinks.UniqueResponseID IN (SELECT ID
FROM @myUniqueResponses)

SELECT * FROM @myAssignedGroupMarks

DECLARE @myCandidateExamVersionIDs TABLE
(
      ID    INT
)

INSERT INTO @myCandidateExamVersionIDs
SELECT DISTINCT CandidateExamVersions.ID
FROM CandidateExamVersions
INNER JOIN CandidateGroupResponses
ON CandidateGroupResponses.CandidateExamVersionID = CandidateExamVersions.ID
INNER JOIN UniqueGroupResponses
ON UniqueGroupResponses.ID = CandidateGroupResponses.UniqueGroupResponseID
INNER JOIN UniqueGroupResponseLinks
ON UniqueGroupResponses.ID = UniqueGroupResponseLinks.UniqueGroupResponseID
WHERE UniqueGroupResponseLinks.UniqueResponseID IN (SELECT ID
FROM @myUniqueResponses)

DECLARE @myCandidateResponses TABLE
(
      CandidateExamVersionID  INT,
      UniqueResponseID        BIGINT
)

PRINT 'Delete candidate responses'
DELETE CandidateResponses
OUTPUT DELETED.* INTO @myCandidateResponses
FROM CandidateResponses
INNER JOIN @myCandidateExamVersionIDs CEV
ON CEV.ID = CandidateResponses.CandidateExamVersionID

SELECT * FROM @myCandidateResponses

DECLARE @myCandidateGroupResponses TABLE
(
      CandidateExamVersionID  INT,
      UniqueGroupResponseID   BIGINT
)

PRINT 'Delete candidate group responses'
DELETE CandidateGroupResponses
OUTPUT DELETED.* INTO @myCandidateGroupResponses
FROM CandidateGroupResponses
INNER JOIN @myCandidateExamVersionIDs CEV
ON CEV.ID = CandidateGroupResponses.CandidateExamVersionID

SELECT * FROM @myCandidateGroupResponses

DECLARE @myExamVersionItems TABLE
(
      ID                                  INT,
      CandidateExamVersionID  INT,
      ItemID                              INT
)

PRINT 'Delete Exam version items'
DELETE ExamVersionItems
OUTPUT DELETED.* INTO @myExamVersionItems
FROM ExamVersionItems
INNER JOIN CandidateExamVersions
ON CandidateExamVersions.ID = ExamVersionItems.CandidateExamVersionID
INNER JOIN @myCandidateExamVersionIDs CEV
ON CEV.ID = CandidateExamVersions.ID

SELECT * FROM @myExamVersionItems

DECLARE @myCandidateExamVersions TABLE 
(
      ID                                        INT,
      ExternalSessionID             INT,
      ExamVersionID                       INT,
      DateSubmitted                       DATETIME,
      Keycode                                   NVARCHAR(12),
      ExamSessionState              INT,
      VIP                                       BIT,
      lastExportDate                      DATETIME,
      PercentageMarkingComplete     INT,
      CandidateForename             NVARCHAR(50),
      CandidateSurname              NVARCHAR(50),
      CandidateRef                        NVARCHAR(300),
      CentreName                          NVARCHAR(100),
      CentreCode                          NVARCHAR(50),
      CountryName                         NVARCHAR(50),
      CompletionDate                      DATETIME,
      ScriptType                          SMALLINT,
      ExamVersionStructureID        INT,
      Mark                                DECIMAL(18,10),
      TotalMark                           DECIMAL(18,10),
      IsFlagged                           BIT
     
)

PRINT 'Delete candidate exam versions'
DELETE CandidateExamVersions
OUTPUT DELETED.* INTO @myCandidateExamVersions
FROM CandidateExamVersions
INNER JOIN @myCandidateExamVersionIDs CEV
ON CEV.ID = CandidateExamVersions.ID

SELECT * FROM @myCandidateExamVersions

DECLARE @myUniqueGroupResponses TABLE 
(
      UniqueGroupResponseID   INT,
      UniqueResposneId        BIGINT,
      confirmedMark                 DECIMAL(9,5),
      markedMetadataResponse  XML
)

PRINT 'Delete unique group response links'
DELETE FROM UniqueGroupResponseLinks
OUTPUT DELETED.* INTO @myUniqueGroupResponses
WHERE UniqueResponseId IN (SELECT ID
FROM @myUniqueResponses)

SELECT * FROM @myUniqueGroupResponses

DECLARE @myUniqueGroupResponsesComplete   TABLE
(
      ID                            BIGINT,
      GroupDefinitionID INT,
      CheckSum                NVARCHAR(50),
      InsertionDate           DATETIME,
      CI_Review               BIT,
      CI                            BIT,
      VIP                           BIT,
      ConfirmedMark           DECIMAL(9,5),
      ParkedAnnotation  NVARCHAR(MAX),
      ParkedDate              DATETIME,
      TokenId                       INT,
      ParkedUserID            INT,
      ScriptType              SMALLINT
      
)

PRINT 'Delete unique group reponses'
DELETE UniqueGroupResponses
OUTPUT DELETED.* INTO @myUniqueGroupResponsesComplete
FROM UniqueGroupResponses
WHERE ID IN (SELECT UniqueGroupResponseID
FROM @myUniqueGroupResponses)

SELECT * FROM @myUniqueGroupResponsesComplete

DECLARE @myUniqueResponsesComplete TABLE
(
      ID                                                    INT,
      itemId                                                INT,
      responseData                                    XML,
      checksum                                        NCHAR(10),
      confirmedMark                                   DECIMAL(9,5),
      insertionDate                                   DATETIME,
      markSchemeId                                    INT,
      latestMarkSchemeIdAtTimeOfImport    INT,
      markedMetadataResponse                    XML
)

PRINT 'Delete unique responses'
DELETE FROM UniqueResponses
OUTPUT DELETED.* INTO @myUniqueResponsesComplete
WHERE ID IN (SELECT ID
FROM @myUniqueResponses)

SELECT * FROM @myUniqueResponsesComplete

ROLLBACK TRAN
