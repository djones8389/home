

SELECT AssignedGroupMarks.Timestamp, MarkingMethodId, IsConfirmedMark, AssignedItemMarks.AssignedMark, *
FROM AssignedItemMarks
INNER JOIN AssignedGroupMarks
ON AssignedGroupMarks.ID = AssignedItemMarks.GroupMarkId
INNER JOIN UniqueGroupResponses
ON UniqueGroupResponses.ID = UniqueGroupResponseId
WHERE UniqueResponseId = 266914 
ORDER BY AssignedGroupMarks.Timestamp

