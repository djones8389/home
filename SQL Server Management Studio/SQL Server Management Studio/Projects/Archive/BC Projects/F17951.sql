select distinct examSessionId
	from WAREHOUSE_ExamSessionTable_Shreded
		
	cross apply resultdata.nodes('exam/section/item') section(item)	
	
	where keycode not like '%[a-z]%'
		and section.item.value('@userAttempted', 'int') = 1
		and section.item.value('@markingType', 'int') = 0
		and len(keycode) > 8
	order by examSessionId desc
	
	
	
select examSessionId, userMark, userPercentage, structureXml, resultData, KeyCode, examName, warehousetime
	from WAREHOUSE_ExamSessionTable_Shreded
		
where examSessionId in (577447,576931,573665,567476)
	
	
select
	west.ID 
	, wests. examSessionId
	, userMark
	, userPercentage
	, wests.structureXml
	, wests.resultData
	, wests.KeyCode
	, examName
	, wests.warehousetime
	, ExamStateChangeAuditXml
	from WAREHOUSE_ExamSessionTable_Shreded as wests
		
		inner join WAREHOUSE_ExamSessionTable as west
		on west.ID = wests.examSessionId
		
where wests.examSessionId in (577447,576931,573665,567476)



select
	* from WAREHOUSE_ExamSessionItemResponseTable
		where WAREHOUSEExamSessionID in (576931)