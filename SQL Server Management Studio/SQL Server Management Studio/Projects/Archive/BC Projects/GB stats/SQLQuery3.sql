--select * from ExamStatsTable 

--where ClientID = 54 
--and CentreName = 'BEST' 
--and QualificationName = 'AustriaAMS' 
--and ExamName = 'Reading AMS'
--and CentreCode = 'Austria - BEST'
--and CompletionDate < '01 May 2014'

--order by CompletionDate asc




select COUNT(examsessionkey) from FactExamSessions

inner join DimCentres
on DimCentres.CentreKey = FactExamSessions.CentreKey

inner join DimExams
on DimExams.ExamKey = FactExamSessions.ExamKey

where CentreName = 'BEST'
	and FactExamSessions.ExamKey = '255'