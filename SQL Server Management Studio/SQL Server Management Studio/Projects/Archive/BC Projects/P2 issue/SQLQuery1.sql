SELECT WEST.ID, WEST.KeyCode, WUT.Forename, WUT.Surname, WUT.CandidateRef, WCT.CentreName, WSCET.examName, WEST.completionDate
	, WESTS.ExternalReference, west.ExportToSecureMarker
 FROM WAREHOUSE_ExamSessionTable as WEST

  Inner Join WAREHOUSE_ScheduledExamsTable as WSCET
  on WSCET.ID = WEST.WAREHOUSEScheduledExamID
  
  Inner Join WAREHOUSE_UserTable as WUT
  on WUT.ID = WEST.WAREHOUSEUserID

  Inner Join WAREHOUSE_CentreTable as WCT
  on WCT.ID = WSCET.WAREHOUSECentreID

  Inner Join WAREHOUSE_ExamSessionTable_Shreded as WESTS
  on WESTS.examSessionId = WEST.ID


where west.KeyCode in ('6KQH5U01','S6EZ3L01','F692Y201', 'QJKQ5L01','Q78EVA01')

select *
		from [BritishCouncil_TestPackage]..ScheduledPackageCandidateExams
		--where DateCompleted is null
		where  ScheduledExamRef in ('6KQH5U01','S6EZ3L01','F692Y201', 'QJKQ5L01','Q78EVA01')