SELECT WEST.ID
	,WEST.KeyCode
	,WUT.Forename
	,WUT.Surname
	,WUT.CandidateRef
	,WCT.CentreName
	,WSCET.examName
	,WEST.completionDate
	,WESTS.ExternalReference
	,west.ExportToSecureMarker
	,wests.userMark
	,WESTS.userPercentage
	,west.warehouseTime
	,wests.warehousetime
--,wests.WarehouseExamState, west.WarehouseExamState
FROM WAREHOUSE_ExamSessionTable AS WEST
INNER JOIN WAREHOUSE_ScheduledExamsTable AS WSCET ON WSCET.ID = WEST.WAREHOUSEScheduledExamID
INNER JOIN WAREHOUSE_UserTable AS WUT ON WUT.ID = WEST.WAREHOUSEUserID
INNER JOIN WAREHOUSE_CentreTable AS WCT ON WCT.ID = WSCET.WAREHOUSECentreID
INNER JOIN WAREHOUSE_ExamSessionTable_Shreded AS WESTS ON WESTS.examSessionId = WEST.ID
WHERE west.ExportToSecureMarker = 1
	AND west.KeyCode collate Latin1_General_CI_AS IN (
		SELECT ScheduledExamRef
		FROM [BritishCouncil_TestPackage]..ScheduledPackageCandidateExams
		WHERE IsCompleted = 0
			AND Score IS NULL
		)
	AND west.KeyCode collate Latin1_General_CI_AS IN (
		SELECT KeyCode
		FROM [430326-BC-SQL2\SQL2].BRITISHCOUNCIL_SecureMarker.dbo.CandidateExamVersions
		WHERE ExamSessionState = 7
			AND PercentageMarkingComplete = '100'
		)
	AND wests.warehouseTime > '2014-01-01'
	--and wests.warehousetime <> west.warehouseTime
	AND wests.warehousetime = west.warehouseTime
ORDER BY wests.warehousetime		
		
	--	[BRITISHCOUNCIL_SecureMarker].[dbo].[CandidateExamVersions] 
	
	
	--select * from BritishCouncil_TestPackage..CommonSettings	
	
	--select*  from [430326-BC-SQL2\SQL2].BRITISHCOUNCIL_SecureMarker.dbo.CandidateExamVersions where KeyCode in ('BLXFUY01','XKD3XT01','RMTVNF01', '4NXSB901','7YN6A201')
	
	
	--west.KeyCode in ('BLXFUY01','XKD3XT01','RMTVNF01', '4NXSB901','7YN6A201')
	
	
	--2,767 this year
	
	--Spot Test--
	/*
	declare @myKeycode nvarchar(8) = 'BLXFUY01'
	
	select * from WAREHOUSE_ExamSessionTable_Shreded where KeyCode =  @myKeycode
	select *  from [430326-BC-SQL2\SQL2].BRITISHCOUNCIL_SecureMarker.dbo.CandidateExamVersions where KeyCode = @myKeycode
	select * from BritishCouncil_TestPackage..ScheduledPackageCandidateExams where ScheduledExamRef = @myKeycode
	
	
	
	*/