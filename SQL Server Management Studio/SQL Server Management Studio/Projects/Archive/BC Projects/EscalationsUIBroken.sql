WITH T AS ( SELECT ROW_NUMBER() OVER( ORDER BY DateEscalated DESC, ID ASC ) AS RowNum, ID, Qualification, QualificationId, Exam, ExamId, ExamVersion, ExamVersionId, ItemName, ItemId, Examiner, ExaminerId, DateEscalated, Issues, ReasonIds, Centres, CentresCodes, CountOfScripts, ScriptId, CandidateName, CandidateRef, CentreName, CentreCode, IsAllowViewWholeScript FROM sm_EscalatedUGRs_fn(118) WHERE (ID > 0) ) SELECT *  FROM T ORDER BY DateEscalated DESC, ID ASC



select --Items.ExternalItemID, ExamVersions.*, E.*, Qualifications.*
  --UGR.ID [ugrID], UGR.InsertionDate [ugrInsertDate], UR.id  [urID],  UR.insertionDate [urInsertDate],
	--responseData, ExternalItemID, Version, ExternalItemName, E.Name, ExternalExamReference, Keycode
		--, ParkedDate
		AssignedGroupMarks.*
		
from UniqueGroupResponses as UGR
	
	inner join UniqueGroupResponseLinks as UGRL
	on UGRL.UniqueGroupResponseID = UGR.ID
	
	inner join UniqueResponses as UR
	on UR.id = UGRL.UniqueResponseId
	
	INNER JOIN AssignedGroupMarks
	ON AssignedGroupMarks.UniqueGroupResponseId = UGR.ID


	left join CandidateGroupResponses
	on CandidateGroupResponses.UniqueGroupResponseID = UGR.ID
	
	INNER join Items
	on items.ID = itemId
	
	inner join ExamVersions 
	on ExamVersions.ID = Items.ExamVersionID

	INNER JOIN Exams as E
	ON E.ID = ExamVersions.ExamID

	INNER JOIN Qualifications
	ON E.QualificationID = Qualifications.ID

	left join CandidateResponses as CR
	on CR.UniqueResponseID = UR.id
	
	left JOIN AssignedItemMarks
	ON AssignedItemMarks.UniqueResponseId = CR.UniqueResponseID

	
	left join CandidateExamVersions as CEV
	on CEV.ID = CR.CandidateExamVersionID
	

where ugr.ID in (693094)
	or Keycode in ('KG82PA99','NMPQJF99','TZXYEY99','UXSXDG99')
	--or cast(responseData as nvarchar(max)) = ''
	order by Keycode asc;

--select * from GroupDefinitions where ID = 1343


--update UniqueGroupResponses
--set IsEscalated = 0
--where id = 693094


select * from UniqueGroupResponses where UniqueGroupResponses.ID = 693094
select * from UniqueResponses where UniqueResponses.id = 613471