SELECT *
FROM AssignedGroupMarks
INNER JOIN UniqueGroupResponses
ON UniqueGroupResponseId = UniqueGroupResponses.ID
INNER JOIN UniqueGroupResponseLinks
ON UniqueGroupResponseLinks.UniqueGroupResponseID = UniqueGroupResponses.ID
INNER JOIN UniqueResponses
ON UniqueGroupResponseLinks.UniqueResponseId = UniqueResponses.ID
INNER JOIN Items
ON UniqueResponses.itemId = Items.ID
INNER JOIN ExamVersions
ON ExamVersions.ID = Items.ExamVersionID
INNER JOIN Exams
ON Exams.ID = ExamVersions.ExamID
WHERE UserId = 73 AND ExternalItemName = 'W_Ver_7_Task_4' AND [Timestamp] >= '2014-03-01 00:00:00' AND [Timestamp] < '2014-04-30 00:00:00'



--select * from Users where Username = 'meghnagandhi'