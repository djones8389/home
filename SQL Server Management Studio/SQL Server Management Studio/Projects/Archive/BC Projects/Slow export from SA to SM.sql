select WAREHOUSE_ExamStateAuditTable.* 
	 , ExamStateChangeAuditXml.query('data(//stateChange[newStateID=9]/changeDate)[1]') as 'State_9 from ExamStateChangeAuditXml'
	 , ExamStateChangeAuditXml
	 from WAREHOUSE_ExamStateAuditTable 

	inner join WAREHOUSE_ExamSessionTable on WAREHOUSE_ExamSessionTable.ID  = WAREHOUSE_ExamStateAuditTable.WarehouseExamSessionID

where WarehouseExamSessionID in (
	select ID
		from WAREHOUSE_ExamSessionTable
			where WarehouseExamState in (3,4)
)
	and WarehouseExamSessionID = 633837
	order by DATE desc