SELECT ExternalSessionID, Keycode
FROM CandidateExamVersions
WHERE NOT EXISTS(
      SELECT CandidateResponses.CandidateExamVersionID
      FROM CandidateResponses
      INNER JOIN UniqueResponses
      ON UniqueResponses.id = CandidateResponses.UniqueResponseID
      WHERE CandidateResponses.CandidateExamVersionID = CandidateExamVersions.ID
            AND
            CONVERT(NVARCHAR(MAX), responseData) <> ''
)