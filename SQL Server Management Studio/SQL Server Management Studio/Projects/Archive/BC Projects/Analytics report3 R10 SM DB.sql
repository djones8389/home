--From Analytics--

SELECT	 S.Username
		,S.Surname
		,S.Forename 
		,S.ExamName
		,S.QualificationName
		,S.ExamVersionsName
		,S.ExamVersionsRef
		,S.ExternalItemID
		,S.ExternalItemName
		,SUM(S.NumResponsesMarked) AS NumResponsesMarked
 FROM	(
	SELECT	 Users.Username
			,Users.Surname
			,Users.Forename 
			,Exams.Name AS ExamName
			,Qualifications.Name AS QualificationName
			,ExamVersions.Name AS ExamVersionsName
			,ExamVersions.ExternalExamReference AS ExamVersionsRef
			,Items.ExternalItemID
			,Items.[Version]
			,Items.ExternalItemName
			,COUNT(AssignedItemMarks.ID)                  AS NumResponsesMarked
	 FROM	 AssignedItemMarks
	 INNER JOIN UniqueResponses
			 ON UniqueResponses.id = AssignedItemMarks.uniqueResponseId
	 INNER JOIN Items
		     ON Items.ID = UniqueResponses.itemId
	 INNER JOIN ExamVersions
			 ON ExamVersions.ID = Items.ExamVersionID
	 INNER JOIN Exams
			 ON ExamVersions.ExamID = Exams.ID
	 INNER JOIN Qualifications
			 ON Exams.QualificationID = Qualifications.ID
	 INNER JOIN Users
			 ON Users.ID = AssignedItemMarks.userId
	WHERE	 AssignedItemMarks.markingMethodId IN (2, 3, 4, 5, 6, 7, 8, 9, 10, 11)
	 AND	 AssignedItemMarks.userId IS NOT NULL
	 AND	 AssignedItemMarks.[timestamp] >= '2014-03-01 23:00:00'
	 AND	 AssignedItemMarks.[timestamp] <= '2014-04-30 23:59:59'
	 --AND	(@myExamVersionRef = ''
		--OR	 ExamVersions.ExternalExamReference LIKE @myExamVersionRef)
	 --AND	(@myItemId = ''
		--OR	 Items.ExternalItemID LIKE @myItemId)
	 --AND	(@myUserName = ''
		--OR	 Users.Username LIKE @myUserName)
		And Users.ID = 73
	GROUP BY Items.ID
			,Users.ID
			,Users.Username
			,Users.Surname
			,Users.Forename
			,Exams.Name
			,Qualifications.Name
			,ExamVersions.ID
			,ExamVersions.Name
			,ExamVersions.ExternalExamReference
			,Items.ID
			,Items.ExternalItemID
			,Items.Version
			,Items.ExternalItemName
		) S
GROUP BY S.Username
		,S.Surname
		,S.Forename 
		,S.ExamName
		,S.QualificationName
		,S.ExamVersionsName
		,S.ExamVersionsRef
		,S.ExternalItemID
		,S.ExternalItemName
		
		--select *from Users where Username = 'Meghnagandhi'