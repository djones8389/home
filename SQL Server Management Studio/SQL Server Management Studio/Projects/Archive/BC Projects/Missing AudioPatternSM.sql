SELECT CEV.ExternalSessionID as 'CEV.ExternalSessionID', URD.ID as 'URD.ID', urd.Name, urd.UniqueResponseID, urd.ExternalDocID, urd.UploadedDate
	,  E.Name, E.Reference, EV.ExternalExamReference, EV.Name
FROM CandidateExamVersions as CEV

INNER JOIN CandidateResponses as CR
ON CR.CandidateExamVersionID = CEV.ID

INNER JOIN UniqueResponses as UR
ON UR.ID = UniqueResponseID 

left join UniqueResponseDocuments as URD
ON URD.UniqueResponseID = UR.id 
	
Inner Join Items as I
on I.ID = ur.itemId

inner join ExamVersions as EV
on EV.ID = I.ExamVersionID

INNER JOIN Exams as E
ON E.ID = EV.ExamID

INNER JOIN Qualifications
ON E.QualificationID = Qualifications.ID

where keycode in ('7756UW01','T3GM2E01','75RFQF01','QZWAU601','QGUHCQ01','Q8UQUV01','SXGS4R01','WNPJTB01','G3TB8B01','Z5ZG2D01','3L8AQ701','HK3XUA01','DJAX5501','8PDKPV01','KN5RWB01','TN2MAN01','RQXBLZ01','AV8VG201','T8RN2U99')
	order by CEV.ExternalSessionID ASC