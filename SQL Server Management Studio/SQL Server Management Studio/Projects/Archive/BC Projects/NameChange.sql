use BritishCouncil_TestPackage

declare @incorrectname nvarchar(30) = 'KGEETHAMALA',
	  @correctname nvarchar(30) = 'K GEETHAMALA'

select * from ScheduledPackageCandidates where FirstName = @incorrectname --or FirstName = 'Radoniqui'

begin tran

--update ScheduledPackageCandidates 
set FirstName = @correctname
where FirstName = @incorrectname

--rollback

select * from ScheduledPackageCandidates where FirstName = @correctname --or FirstName = 'Radoniqui'


select * from [BRITISHCOUNCIL_SecureAssess].[dbo].WAREHOUSE_ExamSessionTable_Shreded  where forename = @incorrectname

begin tran

update [BRITISHCOUNCIL_SecureAssess].[dbo].WAREHOUSE_ExamSessionTable_Shreded 
set forename = @correctname
where forename = @incorrectname

rollback

select * from [BRITISHCOUNCIL_SecureAssess].[dbo].WAREHOUSE_ExamSessionTable_Shreded  where forename = @correctname


select * from [BRITISHCOUNCIL_SecureAssess].[dbo].UserTable  where forename = @incorrectname

begin tran

update [BRITISHCOUNCIL_SecureAssess].[dbo].UserTable 
set forename = @correctname
where forename = @incorrectname

rollback

select * from [BRITISHCOUNCIL_SecureAssess].[dbo].UserTable  where forename = @correctname


select * from [BRITISHCOUNCIL_SecureAssess].[dbo].WAREHOUSE_UserTable where forename = @incorrectname

begin tran

update [BRITISHCOUNCIL_SecureAssess].[dbo].WAREHOUSE_UserTable
set forename = @correctname
where forename = @incorrectname

rollback

select * from [BRITISHCOUNCIL_SecureAssess].[dbo].WAREHOUSE_UserTable where forename = @correctname


