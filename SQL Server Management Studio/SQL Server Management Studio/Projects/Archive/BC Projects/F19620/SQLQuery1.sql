USE BRITISHCOUNCIL_SecureAssess

SELECT EST.ID, EST.KeyCode, EST.examState, UT.Forename, UT.Surname, CT.CentreName, SCET.examName
  FROM ExamSessionTable as EST

  Inner Join ScheduledExamsTable as SCET
  on SCET.ID = EST.ScheduledExamID
  
  Inner Join UserTable as UT
  on UT.ID = EST.UserID

  Inner Join CentreTable as CT
  on CT.ID = SCET.CentreID

where ut.Surname in ('Newbery','Pico')
	or CandidateRef in ('UST002','UST001')	
	or examState = 99

/*
select ScheduledExamsTable.ID, CentreID, ScheduledExamsTable.CreatedDateTime
	from ScheduledExamsTable 
	
	inner join CentreTable on CentreTable.ID = ScheduledExamsTable.CentreID	
	--where ScheduledExamsTable.ID not in (Select ScheduledExamID from ExamSessionTable)
	where CentreName = 'British Council - Chile'
	order by ScheduledExamsTable.CreatedDateTime desc
--select * from BritishCouncil_TestPackage..ScheduledPackageCandidates where LastName in ('Newbery','Pico')
*/


select ESCAT.* from ExamStateChangeAuditTable as ESCAT

	inner join ExamSessionTable as EST
	on EST.ID = ESCAT.ExamSessionID
	
	inner join UserTable as UT
	on UT.ID = EST.UserID

where CandidateRef in ('UST002','UST001')

Use BritishCouncil_TestPackage

select FirstName, LastName, SurpassCandidateRef, SPC.ScheduledPackageCandidateId, Score, PackageScore, ScheduledExamRef
	 from ScheduledPackageCandidateExams as SPCE

	inner join ScheduledPackageCandidates as SPC
	on SPC.ScheduledPackageCandidateId =  SPCE.Candidate_ScheduledPackageCandidateId

where LastName in ('Newbery','Pico')	 

or ScheduledExamRef is null


select * from ScheduledPackageCandidateExams where Candidate_ScheduledPackageCandidateId in (256246,256247)
select * from ScheduledPackageCandidates where ScheduledPackageCandidateId in (256246,256247)