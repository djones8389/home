IF OBJECT_ID('tempdb..#AffectedPages') IS NOT NULL DROP TABLE #AffectedPages

USE UAT_OCR_ContentProducer_IP_24032015;

DECLARE @ProjectID int = 6024

/*REPORT -  Find pages that:

- Have images
- Are not in the recycle bin
- Are within the publishable state
- Are not BTL Test projects

Store this data,  this is what our cursor will run against


This uses:
	UAT_OCR_ItemBank_IP_24032015
	UAT_OCR_ContentProducer_IP_24032015

*/

	SELECT 
		 A.ProjectName 
		, cast(A.ProjectID as nvarchar(10)) + 'P' +  cast(A.PageID  as nvarchar(10)) as CP_ProjectAndPage
		,  A.Version as CP_Version
		,  cast(B.ProjectID as nvarchar(10)) + 'P' + cast(ItemRef as nvarchar(10)) as IB_ProjectAndPage
		,  B.Version as IB_Version
	INTO #AffectedPages
	FROM 
	(
	select distinct
		  PLT.ID as [ProjectID]
		 , Name as [ProjectName]
		 , SUBSTRING(PT.ID, CHARINDEX('P', PT.ID) + 1, LEN(PT.ID)) as PageID
		 , PT.Ver as Version
		from UAT_OCR_ContentProducer_IP_24032015.dbo.ProjectListTable as PLT with (NOLOCK)
	
	cross apply ProjectStructureXml.nodes('Pro/*[local-name(.)!="Rec"]//Pag') p(r) 
	
	INNER JOIN dbo.ItemGraphicTable IGT with (NOLOCK) on SUBSTRING(IGT.ID, CHARINDEX('P', IGT.ID) + 1, CHARINDEX('S',IGT.ID) - CHARINDEX('P', IGT.ID) - 2 + Len('S')) =  p.r.value('@ID','nvarchar(12)')
	INNER JOIN dbo.ComponentTable CT with (NOLOCK) ON IGT.ParentID = CT.ID
	INNER JOIN dbo.SceneTable ST with (NOLOCK) ON  ST.ID = CT.ParentID
	INNER JOIN dbo.PageTable PT with (NOLOCK) ON PT.ParentID = PLT.ID AND PT.ID = ST.ParentID AND PT.ID = cast(PLT.ID as nvarchar(10)) + 'P' + cast(SUBSTRING(PT.ID, CHARINDEX('P', PT.ID) + 1, LEN(PT.ID)) as nvarchar(10))

	where p.r.value('@sta','int') Between PublishSettingsXML.value('(/PublishConfig/StatusLevelFrom)[1]','int') and PublishSettingsXML.value('(/PublishConfig/StatusLevelTo)[1]','int')
		and PLT.ID = @ProjectID
	) A

	LEFT JOIN (

	select  ProjectID, ItemRef, max(Version) as Version 
		from UAT_OCR_ItemBank_IP_24032015.dbo.ItemTable  with (NOLOCK)
			where projectID = @ProjectID
			group by ProjectID, ItemRef 
	) B

	ON A.ProjectID = B.ProjectID 
		and A.PageID = B.ItemRef		


	Order by 1,2;

	select * from #AffectedPages