SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

SELECT top 5 EST.ID
	,examState
	, QualificationName
	, examName
	, CentreName
	, CentreCode
	, Forename
	, Surname
	, CandidateRef
	, StateChangeDate as [completed]
	,(dbo.fn_GetUserRelationForExamSession(EST.ID, 1)) AS userAssociationMarker
	,(dbo.fn_GetUserRelationForExamSession(EST.ID, 2)) AS userAssociationModerator
    ,(select Email from UserTable where Username = (dbo.fn_GetUserRelationForExamSession(EST.ID, 1))) as userAssociationMarker_Email
    ,(select Email from UserTable where Username = (dbo.fn_GetUserRelationForExamSession(EST.ID, 2))) as userAssociationModerator_Email
  FROM ExamSessionTable as EST

  Inner Join ScheduledExamsTable as SCET
  on SCET.ID = EST.ScheduledExamID
  
  Inner Join UserTable as UT
  on UT.ID = EST.UserID

  Inner Join CentreTable as CT
  on CT.ID = SCET.CentreID

  Inner Join IB3QualificationLookup as IB
  on IB.ID = SCET.qualificationId
  
  left join ExamStateChangeAuditTable as ESCAT
  on ESCAT.ExamSessionID = EST.ID

  left JOIN UserSessionRelationTable AS USR
  on USR.UserId = UT.ID

where  examState in (15,16)
	and NewState = 9
	and Surname = 'Mackinlay'
	order by examState asc;

--select * from ExamStateChangeAuditTable where ExamSessionID = 1405336