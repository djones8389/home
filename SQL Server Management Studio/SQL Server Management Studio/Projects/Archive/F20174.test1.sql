            select 

                        X.name as NullName
                    , S.name as PopulatedName
                    --, S.ProjectId as PopulatedProjectID
                    --, S.ID as PopulatedID
                  into #MyTempTable
            from
            (   --Find my null values, for 1 project only, to start with
                  select distinct  name
                  from ProjectManifestTable 
                        where ProjectId = 942 
                              and Image is null
            ) X

            INNER JOIN ( --Find other instances of these images, where it is not null
                  SELECT distinct name
                  FROM ProjectManifestTable
                  where Image is not null
                  
            ) S
            ON X.name = S.name;


begin tran

select  name, DATALENGTH(Image) from ProjectManifestTable where ProjectId = 942  order by name;  --(325 row(s) affected)

update ProjectManifestTable
set Image = (select image from ProjectManifestTable where Image is not null and #MyTempTable.PopulatedName =  a.name)
from ProjectManifestTable a
inner join #MyTempTable
on #MyTempTable.PopulatedName = a.name

where a.ProjectId = 942
	and #MyTempTable.PopulatedName = a.name;

select  name, DATALENGTH(Image) from ProjectManifestTable where ProjectId = 942 and DATALENGTH(Image) = 46448  order by name; 

rollback
