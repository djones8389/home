--USE [AAT_CPProjectAdmin_11.3]


select 
	--ProjectStructureXml,
	  ID as [ProjectID]
	 , Name as [ProjectName]
	 , p.r.value('@ID','nvarchar(12)') as PageID
	 , p.r.value('@chO2','nvarchar(max)') as CheckedOutTo_ComputerName
	 , p.r.value('@chO','nvarchar(max)') as CheckedOutTo_UserName
	 , Username
	from ProjectListTable
	
cross apply ProjectStructureXml.nodes('Pro//Pag') p(r)
	
	left join UserTable on UserTable.UserID = p.r.value('@chO','nvarchar(max)')
	
	where  (p.r.value('@chO2','nvarchar(max)') is not null 
			or p.r.value('@chO','nvarchar(max)') is not null)

EXCEPT		
		
select 
	--ProjectStructureXml,
	  ID as [ProjectID]
	 , Name as [ProjectName]
	 , p.r.value('@ID','nvarchar(12)') as PageID
	 , p.r.value('@chO2','nvarchar(max)') as CheckedOutTo_ComputerName
	 , p.r.value('@chO','nvarchar(max)') as CheckedOutTo_UserName
	 , Username
	from ProjectListTable
	
cross apply ProjectStructureXml.nodes('Pro/Rec//Pag') p(r)
	
	left join UserTable on UserTable.UserID = p.r.value('@chO','nvarchar(max)')
	
	where (p.r.value('@chO2','nvarchar(max)') is not null 
			or p.r.value('@chO','nvarchar(max)') is not null)
			
	order by Name;
		
	
	
	
	
	
--EXCEPT--
--chO="1846" pMU="1947" pMD="24/06/2014 10:28:36" chO2="BTL144" />--





--Report to find pages that are checked out to people--

--Exclude all Pro/Rec


