USE UAT_OCR_ContentProducer_IP

IF OBJECT_ID('tempdb..#AffectedPages') IS NOT NULL DROP TABLE #AffectedPages

/*REPORT #1  -  Find pages that:

- Have images
- Are not in the recycle bin
- Are within the publishable state
- Are not BTL Test projects

Store this data,  this is what our cursor will run against

*/

	SELECT distinct
		 A.ProjectName 
		, cast(A.ProjectID as nvarchar(10)) + 'P' +  cast(A.PageID  as nvarchar(10)) as CP_ProjectAndPage
		,  A.Version as CP_Version
		,  cast(C.ProjectID as nvarchar(10)) + 'P' + cast(ItemRef as nvarchar(10)) as IB_ProjectAndPage
		,  C.Version as IB_Version
	INTO #AffectedPages
	FROM 
	(
	select distinct
		  PLT.ID as [ProjectID]
		 , Name as [ProjectName]
		 , SUBSTRING(PT.ID, CHARINDEX('P', PT.ID) + 1, LEN(PT.ID)) as PageID
		 , PT.Ver as Version
		from dbo.ProjectListTable as PLT with (NOLOCK)
	
	cross apply ProjectStructureXml.nodes('Pro//Pag') p(r)  
	
	INNER JOIN dbo.ItemGraphicTable IGT with (NOLOCK) on SUBSTRING(IGT.ID, CHARINDEX('P', IGT.ID) + 1, CHARINDEX('S',IGT.ID) - CHARINDEX('P', IGT.ID) - 2 + Len('S')) =  p.r.value('@ID','nvarchar(12)')
	INNER JOIN dbo.ComponentTable CT with (NOLOCK) ON IGT.ParentID = CT.ID
	INNER JOIN dbo.SceneTable ST with (NOLOCK) ON  ST.ID = CT.ParentID
	INNER JOIN dbo.PageTable PT with (NOLOCK) ON PT.ParentID = PLT.ID AND PT.ID = ST.ParentID AND PT.ID = cast(PLT.ID as nvarchar(10)) + 'P' + cast(SUBSTRING(PT.ID, CHARINDEX('P', PT.ID) + 1, LEN(PT.ID)) as nvarchar(10))

	where p.r.value('@sta','int') Between PublishSettingsXML.value('(/PublishConfig/StatusLevelFrom)[1]','int') and PublishSettingsXML.value('(/PublishConfig/StatusLevelTo)[1]','int')

	) A

	LEFT JOIN (
		
	select distinct
		  PLT.ID as [ProjectID]
		 , Name as [ProjectName]
		 , SUBSTRING(PT.ID, CHARINDEX('P', PT.ID) + 1, LEN(PT.ID)) as PageID
		from dbo.ProjectListTable as PLT with (NOLOCK) 
	
	cross apply PLT.ProjectStructureXml.nodes('Pro/Rec//Pag') p(r) 
	
	INNER JOIN dbo.ItemGraphicTable IGT with (NOLOCK) on SUBSTRING(IGT.ID, CHARINDEX('P', IGT.ID) + 1, CHARINDEX('S',IGT.ID) - CHARINDEX('P', IGT.ID) - 2 + Len('S')) =  p.r.value('@ID','nvarchar(12)')
	INNER JOIN dbo.ComponentTable CT with (NOLOCK) ON IGT.ParentID = CT.ID
	INNER JOIN dbo.SceneTable ST with (NOLOCK) ON  ST.ID = CT.ParentID
	INNER JOIN dbo.PageTable PT with (NOLOCK) ON PT.ParentID = PLT.ID AND PT.ID = ST.ParentID AND PT.ID = cast(PLT.ID as nvarchar(10)) + 'P' + cast(SUBSTRING(PT.ID, CHARINDEX('P', PT.ID) + 1, LEN(PT.ID)) as nvarchar(10))

	where p.r.value('@sta','int') Between PublishSettingsXML.value('(/PublishConfig/StatusLevelFrom)[1]','int') and PublishSettingsXML.value('(/PublishConfig/StatusLevelTo)[1]','int')

	) B

	ON A.ProjectID = B.ProjectID
		and A.PageID = B.PageID
		--where B.PageID is null

	LEFT JOIN (

	select  ProjectID, ItemRef, max(Version) as Version 
		from UAT_OCR_ItemBank_IP.dbo.ItemTable  with (NOLOCK)
			group by ProjectID, ItemRef 
	) C

	ON A.ProjectID = C.ProjectID 
		and A.PageID = C.ItemRef
		and A.Version <> C.Version

	where B.PageID is null --Filter Recycle Bin Items---
	and cast(C.ProjectID as nvarchar(10)) + 'P' + cast(ItemRef as nvarchar(10)) is null
	Order by 1,2;
	
SELECT * from #AffectedPages;


--select * from UAT_OCR_ItemBank_IP.dbo.ItemTable
--where cast(ProjectID as nvarchar(10)) + 'P' + cast(ItemRef as nvarchar(10)) in ()

