--SELECT ID as ProjectID
--      --ProjectStructureXML
--	, Name
--FROM ProjectListTable
--WHERE ID not IN (6020,6005,6015,6014,6009,6038,6010,6042,334,327,340,6007,6012,6051,6011,6041,6044,6045,6016, 6072)
--order by ID, Name

SELECT ID ProjectID, 
      ProjectStructureXML
INTO ##Projects
FROM ProjectListTable
WHERE ID NOT IN (6020,6005,6015,6014,6009,6038,6010,6042,334,327,340,6007,6012,6051,6011,6041,6044,6045,6016, 6072)
AND ID IN (
      SELECT PT.ParentID
      FROM dbo.ItemGraphicTable IGT
      INNER JOIN dbo.ComponentTable CT ON IGT.ParentID = CT.ID
      INNER JOIN dbo.SceneTable ST ON ST.ID = CT.ParentID
      INNER JOIN dbo.PageTable PT ON PT.ID = ST.ParentID
      --WHERE IGT.loM IS NULL
)

DECLARE mySQL CURSOR FOR
      SELECT N'UPDATE P SET ProjectStructureXML = REPLACE(CAST(ProjectStructureXML AS NVARCHAR(MAX)), '''+
            --,ProjectID
            CAST(Pro.Pag.query('.') AS NVARCHAR(MAX))+N''','''+
            REPLACE(
                  REPLACE(CAST(Pro.Pag.query('.') AS NVARCHAR(MAX))
                        ,SUBSTRING(CAST(Pro.Pag.query('.') AS NVARCHAR(MAX)),
                              CHARINDEX(N'alF="', CAST(Pro.Pag.query('.') AS NVARCHAR(MAX))),
                              CASE
                                    WHEN CHARINDEX(N' ', CAST(Pro.Pag.query('.') AS NVARCHAR(MAX)), CHARINDEX(N'alF="', CAST(Pro.Pag.query('.') AS NVARCHAR(MAX)))) > 0
                                          THEN CHARINDEX(N' ', CAST(Pro.Pag.query('.') AS NVARCHAR(MAX)), CHARINDEX(N'alF="', CAST(Pro.Pag.query('.') AS NVARCHAR(MAX))))
                                    ELSE CHARINDEX(N'/', CAST(Pro.Pag.query('.') AS NVARCHAR(MAX)), CHARINDEX(N'alF="', CAST(Pro.Pag.query('.') AS NVARCHAR(MAX))))
                              END - CHARINDEX(N'alF="', CAST(Pro.Pag.query('.') AS NVARCHAR(MAX)))
                        )
                  , N'')
            ,N'/>'
            ,N' alF="1" />')+N''') FROM ##Projects P WHERE ProjectID = '+CAST(ProjectID AS NVARCHAR(22))
      --)
      FROM ##Projects P
      CROSS APPLY ProjectStructureXML.nodes('//Pag') Pro(Pag)

OPEN mySQL

DECLARE @mySQL NVARCHAR(MAX);

FETCH NEXT FROM mySQL INTO @mySQL;

WHILE @@FETCH_STATUS = 0
BEGIN
      EXEC(@mySQL);
      FETCH NEXT FROM mySQL INTO @mySQL;
END

CLOSE mySQL;
DEALLOCATE mySQL;

DROP TABLE ##Projects;
