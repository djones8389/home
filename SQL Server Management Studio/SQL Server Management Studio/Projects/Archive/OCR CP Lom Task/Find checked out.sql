USE UAT_OCR_CONTENTPRODUCER_IP

IF OBJECT_ID('tempdb..#TEST') IS NOT NULL DROP TABLE #TEST

select A.*
into #TEST
from

(

select 
	Projectlisttable.id
   , name
   --, ProjectStructureXml
   , project.page.value('@ID','nvarchar(12)') as PageID
   , project.page.value('@alF','int')  as Alf
from Projectlisttable

cross apply ProjectStructureXml.nodes('Pro//Pag') project(page)

inner join PageTable 
on PageTable.ParentID = Projectlisttable.id
	AND PageTable.ID = cast(Projectlisttable.ID as nvarchar(10)) + 'P' + cast(project.page.value('@ID','nvarchar(12)') as nvarchar(10))

where project.page.value('@alF','int') = '1'
	and project.page.value('@sta','int') Between PublishSettingsXML.value('(/PublishConfig/StatusLevelFrom)[1]','int') and PublishSettingsXML.value('(/PublishConfig/StatusLevelTo)[1]','int')

EXCEPT

select 
	Projectlisttable.id
   , name
   --, ProjectStructureXml
   , project.page.value('@ID','nvarchar(12)') as PageID
   , project.page.value('@alF','int')  as Alf
from Projectlisttable

cross apply ProjectStructureXml.nodes('Pro/Rec//Pag') project(page)

inner join PageTable 
on PageTable.ParentID = Projectlisttable.id
	AND PageTable.ID = cast(Projectlisttable.ID as nvarchar(10)) + 'P' + cast(project.page.value('@ID','nvarchar(12)') as nvarchar(10))

where project.page.value('@alF','int') = '1'
	and project.page.value('@sta','int') Between PublishSettingsXML.value('(/PublishConfig/StatusLevelFrom)[1]','int') and PublishSettingsXML.value('(/PublishConfig/StatusLevelTo)[1]','int')
) A


--4507--
select count(PageID) from #Test;

select count(PageID) as 'ct', Name, ID
from #Test
group by ID, Name, ID
order by ct desc;

/*
ct	Name	ID

3527	OCR Cambridge Awards in Mathematics	6026
413	Certificates of Professional Competence	6029
354	OCR Cambridge Awards in English	6025
94	Digital Literacy	6031
66	OCR Functional Skills Mathematics	4208
30	Business and Admin Staging	6027
17	OCR Functional Skills Mathematics L1	6052
3	OCR Cambridge Awards Maths On Demand	6057
1	OCR Functional Skills English Writing L2	6049
1	OCR DA Tool	6033
1	TESTCB2 1912322	6045

*/