Select ID, Name, projectStructureXML, ProjectDefaultXML, PublishSettingsXML
from ProjectListTable 
where ID IN (6014)--327,340,6009,6007,6012,6042,6014,6010,6015)




select distinct
	-- PublishSettingsXML.query('data(/PublishConfig/StatusLevelFrom)[1]') as StatusFrom
	--, PublishSettingsXML.query('data(/PublishConfig/StatusLevelTo)[1]') as StatusTo
	PLT.ID as ProjectID
	, a.b.value('@ID','nvarchar(20)') as ItemRef
	, PT.Ver as Version
	, cast(PLT.ID as nvarchar(10)) + 'P' + cast(a.b.value('@ID','nvarchar(20)') as nvarchar(10)) as ProjectAndPage
	--, a.b.value('@sta','int') as PageStatus
	--, PublishSettingsXML.value('(/PublishConfig/StatusLevelFrom)[1]','int') as StatusFrom
	--, PublishSettingsXML.value('(/PublishConfig/StatusLevelTo)[1]','int') as StatusTo
from ProjectListTable as PLT


cross apply ProjectStructureXML.nodes('Pro//Pag') a(b)
--cross apply PublishSettingsXML.nodes('PublishConfig') c(d)

inner join PageTable as PT
on PT.ParentID = PLT.ID and PT.ID = cast(PLT.ID as nvarchar(10)) + 'P' + cast(a.b.value('@ID','nvarchar(20)') as nvarchar(10))


where PLT.ID = 6014
	--Check Status of the page is within the publishable range
	and a.b.value('@sta','int') Between PublishSettingsXML.value('(/PublishConfig/StatusLevelFrom)[1]','int') and PublishSettingsXML.value('(/PublishConfig/StatusLevelTo)[1]','int');


select  ProjectID, ItemRef, max(Version) as Version from UAT_OCR_ItemBank_IP..ItemTable where ProjectID = 6014 group by ProjectID, ItemRef;





/*

select ID, ver
from PageTable 
where ID IN ('6014P6018','6014P6020','6014P6021','6014P6022','6014P6023','6014P6030','6014P6039','6014P6040','6014P6041','6014P6042','6014P6044');



ProjectID	ItemRef	Version
327			331		20
327			331		21
327			334		20
327			335		16
327			336		34
*/

select top 5 * from UAT_OCR_ItemBank_IP..ItemTable