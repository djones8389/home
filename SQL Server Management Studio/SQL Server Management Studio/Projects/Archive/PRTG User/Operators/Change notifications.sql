DECLARE @NewOperatorName nvarchar(20) = 'DBAdmin';
DECLARE @OldOperatorName nvarchar(20) = (select name from msdb.dbo.sysoperators where name like 'Andy%');
DECLARE @EmailAddress  nvarchar(25) = 'DBAdmin@btl.com';
DECLARE @CreateNewOperator nvarchar(MAX) = '';
DECLARE @AssignOperatorNotifications nvarchar(MAX) = '';
DECLARE @RemoveOperatorNotifications  nvarchar(MAX) = '';
DECLARE @ChangeJobNotification nvarchar(MAX) = '';
DECLARE @ChangeSQLServerAgent nvarchar(MAX) = '';




SELECT @ChangeJobNotification +=CHAR(13) + '

IF (SELECT top 1 MSDB.dbo.sysjobs.name--, notify_email_operator_id
FROM MSDB.dbo.sysjobs
inner join MSDB.dbo.sysoperators
on MSDB.dbo.sysoperators.id = MSDB.dbo.sysjobs.notify_email_operator_id
where MSDB.dbo.sysoperators.name=N'+ ''''+ @OldOperatorName + '''and MSDB.dbo.sysjobs.name=N''' +  name + ''') 
	
IS NOT NULL

      BEGIN
      
      EXEC msdb.dbo.sp_update_job @job_name =N''' + name + ''',
            @notify_level_netsend=2, 
            @notify_level_page=2,
            @notify_email_operator_name =N'+ '''' + @NewOperatorName + ''', 
            @notify_netsend_operator_name =N'+ '''' + @NewOperatorName + '''
      
      PRINT ''Notifications have been updated''
      
      END
      ELSE
      BEGIN
         
         PRINT ''Notifications do not need changing''
      
      END
'
from MSDB.dbo.sysjobs





BEGIN TRY
   BEGIN TRANSACTION 
            --EXEC (@CreateNewOperator)
            --EXEC (@AssignOperatorNotifications);
            --EXEC (@RemoveOperatorNotifications);
            EXEC (@ChangeJobNotification);
            --EXEC (@ChangeSQLServerAgent);
    COMMIT TRANSACTION
END TRY
BEGIN CATCH
    SELECT 
        ERROR_NUMBER() AS ErrorNumber
        ,ERROR_SEVERITY() AS ErrorSeverity
        ,ERROR_STATE() AS ErrorState
        ,ERROR_PROCEDURE() AS ErrorProcedure
        ,ERROR_LINE() AS ErrorLine
        ,ERROR_MESSAGE() AS ErrorMessage;

    IF @@TRANCOUNT > 0
        ROLLBACK TRANSACTION;
END CATCH;

IF @@TRANCOUNT > 0
    COMMIT TRANSACTION;
GO