--Test

BEGIN TRY
DROP TABLE #T1
END TRY
BEGIN CATCH
END CATCH
 
CREATE TABLE #T1
(Val XML)
 
-- Stick in some XML data
 
INSERT INTO #T1 (Val) VALUES ('<root>
  <branch>
    <leaf name="displayname" type="String">cycling</leaf>
    <leaf name="alternativename" type="String">is sometimes fun</leaf>
  </branch>
</root>')

SELECT * FROM #T1
--INSERT INTO #T1 (Val) VALUES ('<root xmlns="http://tim.xsd">
--  <branch>
--    <leaf name="displayname" type="String">running</leaf>
--    <leaf name="alternativename" type="String">is sometimes fun</leaf>
--  </branch>
--</root>')
 
-- Update the cycling row using a SQL variable and some functions (concat and substring)
 
DECLARE @timvar VARCHAR(MAX)
SELECT @timvar = ' is always '
 
UPDATE #T1 SET Val.modify('
  replace value of (/root/branch/leaf[@name="alternativename"]/text())[1]
  with fn:concat(
    "Everybody knows that ",
    (/root/branch/leaf[@name="displayname"]/text())[1],
    sql:variable("@timvar"),
    fn:substring((/root/branch/leaf[@name="alternativename"]/text())[1], 14, 3))
')
WHERE Val.query('(root/branch[1]/leaf[@name="displayname"]/text())[1]').value('.', 'NVARCHAR(MAX)') = 'cycling'
 
-- Check the result
 
SELECT * FROM #T1