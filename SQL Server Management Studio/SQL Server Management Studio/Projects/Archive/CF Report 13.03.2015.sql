select COUNT(ID)
From ExamSessionTable
Where examState = 6	



SELECT A.*
	 , B.StateChangeDate
FROM
(
SELECT
	EST.ID
	,EST.examState
	,pinNumber
	,examName
	,EST.KeyCode
	,CT.CentreName
	,QualificationName
	,cast(ScheduledStartDateTime AS DATE) AS [Start Date]
	,cast(ScheduledEndDateTime AS DATE) AS [End Date]
	,UT.Forename + ' ' + UT.Surname AS [Candidate]
	,ut.CandidateRef
	,invigilated
	,cast(DATEADD(minute, ActiveStartTime, 0) AS TIME(0)) AS [StartTime]
	,cast(DATEADD(minute, ActiveEndTime, 0) AS TIME(0)) AS [EndTime]
	,CASE IsProjectBased WHEN '0' then 'No' else 'Yes' end as IsProjectBased
FROM ExamSessionTable AS EST 
INNER JOIN ScheduledExamsTable AS SCET WITH (NOLOCK) ON SCET.ID = EST.ScheduledExamID
INNER JOIN UserTable AS UT WITH (NOLOCK) ON UT.ID = EST.UserID
INNER JOIN CentreTable AS CT WITH (NOLOCK) ON CT.ID = SCET.CentreID
INNER JOIN IB3QualificationLookup AS IB WITH (NOLOCK) ON IB.ID = qualificationId
)  A

INNER JOIN	(			

	select est.ID
			,	MIN(escat.StateChangeDate) as StateChangeDate
			, escat.NewState 
	from ExamSessionTable as est
	
	left join ExamStateChangeAuditTable as escat WITH (NOLOCK)on escat.ExamSessionID = est.ID
	
	where est.examState = 6
		and escat.NewState = 6
		
		 group by est.ID, escat.NewState

) B
On A.ID = B.ID and A.examState = B.NewState

WHERE A.examstate = 6
	 AND B.StateChangeDate > '01 Sep 2014';