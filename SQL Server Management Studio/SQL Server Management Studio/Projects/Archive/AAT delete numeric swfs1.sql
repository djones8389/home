--Look for items with numeric SWF

SELECT B.*, PLT.Name
FROM
	(
	select ID, name, ProjectId
		from ProjectManifestTable
			where (name like '[%0-9%]' + '.swf' or name like '[%0-9%][%0-9%]' + '.swf' or name like '[%0-9%][%0-9%][%0-9%]' + '.swf')
	) A


INNER JOIN  (

	Select i.id, i.ver, i.moD, it.ali, i.ParentID, it.ID as ITID
	
	from ItemGraphicTable it 
		INNER JOIN ComponentTable c ON c.ID = it.ParentID 
		INNER JOIN SceneTable s ON s.ID = c.ParentID 
		INNER JOIN PageTable i ON i.id = S.ParentID
	UNION
	Select i.id, i.ver, i.moD, it.aLI, i.ParentID, it.ID as ITID
	from ItemVideoTable it 
		INNER JOIN ComponentTable c ON c.ID = it.ParentID 
		INNER JOIN SceneTable s ON s.ID = c.ParentID 
		INNER JOIN PageTable i ON i.id = S.ParentID
	UNION
	Select i.id, i.ver, i.moD, it.aLI, i.ParentID , it.ID  as ITID
		from ItemHotSpotTable it 
		INNER JOIN ComponentTable c ON c.ID = it.ParentID 
		INNER JOIN SceneTable s ON s.ID = c.ParentID 
		INNER JOIN PageTable i ON i.id = S.ParentID
	UNION
	Select i.id, i.ver, i.moD, it.aLI , i.ParentID, it.ID as ITID
	from ItemCustomQuestionTable it 
		INNER JOIN ComponentTable c ON c.ID = it.ParentID 
		INNER JOIN SceneTable s ON s.ID = c.ParentID 
		INNER JOIN PageTable i ON i.id = S.ParentID

	) as B

  ON B.aLI = SUBSTRING(A.NAME , 0, CHARINDEX('.',A.NAME ))
  AND SUBSTRING (B.ID, 0, CHARINDEX('P', B.ID)) = A.ProjectID

inner join PageTable as PT
on PT.ID = B.ID and PT.ver = B.ver

inner join ProjectListTable as PLT
on PLT.ID = PT.ParentID



--68419







--select * from ProjectManifestTable where ProjectId = 241 and ID= 12

--select top 5 * from ItemGraphicTable