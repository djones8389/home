select ID
	--, structurexml
	, sum(s.i.value('@viewingTime[1]','int')) as ViewingTimes
from examsessiontable

cross apply structurexml.nodes('assessmentDetails/assessment/section/item') s(i)

where examState = 6  
  group by ID
	having (sum(s.i.value('@viewingTime[1]','int'))) = 0

	


--select  ID, structurexml
--from examsessiontable
--where examState = 6
--	and structurexml.value('count(assessmentDetails/assessment/section/item)', 'nvarchar(max)') > 1




/*
update examsessiontable
set structurexml = '<assessmentDetails>
  <assessmentID>1824</assessmentID>
  <qualificationID>79</qualificationID>
  <qualificationName>Processing bookkeeping transactions (AQ2013)</qualificationName>
  <qualificationReference>AATQCF</qualificationReference>
  <assessmentGroupName>PBKT sample assessment 2</assessmentGroupName>
  <assessmentGroupID>1714</assessmentGroupID>
  <assessmentGroupReference>CAPBKT/PRACTICE</assessmentGroupReference>
  <assessmentName>PBKT sample assessment 2</assessmentName>
  <validFromDate>01 Oct 2013</validFromDate>
  <expiryDate>31 Dec 2016</expiryDate>
  <startTime>00:00:00</startTime>
  <endTime>23:59:59</endTime>
  <duration>120</duration>
  <defaultDuration>120</defaultDuration>
  <scheduledDuration>
    <value>120</value>
    <reason></reason>
  </scheduledDuration>
  <sRBonus>0</sRBonus>
  <sRBonusMaximum>40</sRBonusMaximum>
  <externalReference>CAPBKT/PRACTICE</externalReference>
  <passLevelValue>70</passLevelValue>
  <passLevelType>1</passLevelType>
  <status>2</status>
  <testFeedbackType>
    <passFail>1</passFail>
    <percentageMark>0</percentageMark>
    <allowSummaryFeedback>0</allowSummaryFeedback>
    <summaryFeedbackType>1</summaryFeedbackType>
    <itemSummary>0</itemSummary>
    <itemReview>0</itemReview>
    <itemReviewCorrectAnswer>0</itemReviewCorrectAnswer>
    <itemFeedback>0</itemFeedback>
    <printableSummary>0</printableSummary>
    <candidateDetails>0</candidateDetails>
    <feedbackByReference>0</feedbackByReference>
    <allowFeedbackDuringExam>0</allowFeedbackDuringExam>
    <allowFeedbackDuringSummary>0</allowFeedbackDuringSummary>
  </testFeedbackType>
  <itemFeedback>0</itemFeedback>
  <allowCalculator>0</allowCalculator>
  <lastInUseDate>28 Sep 2013 17:26:53</lastInUseDate>
  <completedScriptReview>0</completedScriptReview>
  <assessment currentSection="0" totalTime="0" currentTime="0">
    <intro id="0" name="Introduction" currentItem="">
      <item id="904P3158" name="Copy of Introduction" totalMark="1" version="7" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
    </intro>
    <outro id="0" name="Finish" currentItem="" />
    <tools id="0" name="Tools" currentItem="" />
    <section id="1" name="1" passLevelValue="70" passLevelType="1" itemsToMark="0" currentItem="" fixed="1">
      <item id="904P3096" name="01 PBKT SP003 " totalMark="15" version="71" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="0" flagged="0" LO="1. Make entries in an analysed day-book" unit="Processing Bookkeeping Transactions" quT="11,12" />
      <item id="904P3111" name="02 PBKT CH004" totalMark="15" version="44" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="0" flagged="0" LO="2. Transfer data from day-books to ledgers" unit="Processing Bookkeeping Transactions" quT="20" />
      <item id="904P3410" name="03 PBKT SP003 v2" totalMark="20" version="71" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="0" flagged="0" LO="3. Make entries in a three column cash-book" unit="Processing Bookkeeping Transactions" quT="20" />
      <item id="904P3099" name="04 PBKT SP003" totalMark="15" version="51" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="123" flagged="0" LO="4. Transfer data from a three column cash- book" unit="Processing Bookkeeping Transactions" quT="20" />
      <item id="904P3112" name="05 PBKT CH004" totalMark="20" version="80" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="227" flagged="0" LO="5. Make entries in and transfers from an analysed petty cash-book" unit="Processing Bookkeeping Transactions" quT="10,11,20" />
      <item id="904P3101" name="06 PBKT SP003" totalMark="20" version="48" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="0" flagged="0" LO="6. Prepare an initial trial balance" unit="Processing Bookkeeping Transactions" quT="20" />
      <item id="904P3116" name="07 PBKT CH004" totalMark="15" version="62" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="0" flagged="0" LO="7. Check supplier invoices/credit notes" unit="Processing Bookkeeping Transactions" quT="11,14,20" />
      <item id="904P3115" name="08 PBKT CH004" totalMark="15" version="52" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="0" flagged="0" LO="8. Prepare sales invoice or credit note: Check the accuracy of receipts from customers" unit="Processing Bookkeeping Transactions" quT="10,11,12" />
      <item id="904P3104" name="09 PBKT SP003" totalMark="15" version="57" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="0" flagged="0" LO="9. Prepare a statement of account from an account in the sales ledger" unit="Processing Bookkeeping Transactions" quT="11,16,20" />
      <item id="904P3105" name="10 PBKT SP003" totalMark="15" version="54" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="0" flagged="0" LO="10. Understand the double entry bookkeeping system" unit="Processing Bookkeeping Transactions" quT="11,12,20" />
    </section>
  </assessment>
  <isValid>1</isValid>
  <isPrintable>0</isPrintable>
  <qualityReview>0</qualityReview>
  <numberOfGenerations>1</numberOfGenerations>
  <requiresInvigilation>0</requiresInvigilation>
  <advanceContentDownloadTimespan>60</advanceContentDownloadTimespan>
  <automaticVerification>1</automaticVerification>
  <offlineMode>2</offlineMode>
  <requiresValidation>0</requiresValidation>
  <requiresSecureClient>1</requiresSecureClient>
  <examType>0</examType>
  <advanceDownload>0</advanceDownload>
  <gradeBoundaryData>
    <gradeBoundaries passLevelType="1" adjustedGradeBoundaries="0">
      <grade modifier="lt" value="70" description="Fail" />
      <grade modifier="gt" value="70" description="Pass" />
    </gradeBoundaries>
  </gradeBoundaryData>
  <autoViewExam>0</autoViewExam>
  <autoProgressItems>0</autoProgressItems>
  <confirmationText>
    <confirmationText>Tick this box to confirm the above details are correct, the CBA submission is your own unaided work and you will not distribute, reproduce or circulate AAT assessment material.</confirmationText>
  </confirmationText>
  <requiresConfirmationCheck>1</requiresConfirmationCheck>
  <allowCloseWithoutSubmit>0</allowCloseWithoutSubmit>
  <useSecureMarker>0</useSecureMarker>
  <annotationVersion>0</annotationVersion>
  <allowPackagingDelivery>1</allowPackagingDelivery>
  <scoreBoundaryData>
    <scoreBoundaries showScoreBoundaryColumn="1">
      <score modifier="lt" value="36" description="Significantly below" higherBoundarySet="1" />
      <score modifier="lt" value="36" description="Significantly below" higherBoundarySet="0" />
      <score modifier="gt" value="36" description="Below requirement" higherBoundarySet="1" />
      <score modifier="gt" value="60" description="Borderline" higherBoundarySet="1" />
      <score modifier="gt" value="70" description="Met" higherBoundarySet="0" />
      <score modifier="gt" value="70" description="Met" higherBoundarySet="1" />
      <score modifier="gt" value="36" description="Below requirement" higherBoundarySet="0" userCreated="1" />
      <score modifier="gt" value="60" description="Borderline" higherBoundarySet="0" userCreated="1" />
      <score modifier="gt" value="85" description="Exceeded" higherBoundarySet="1" userCreated="1" />
    </scoreBoundaries>
  </scoreBoundaryData>
  <showCandidateReportResultColumn>0</showCandidateReportResultColumn>
  <showCandidateReportPercentageColumn>1</showCandidateReportPercentageColumn>
  <certifiedAccessible>0</certifiedAccessible>
  <markingProgressVisible>1</markingProgressVisible>
  <markingProgressMode>0</markingProgressMode>
  <secureClientOperationMode>0</secureClientOperationMode>
  <examDeliverySystemStyleType>delivery</examDeliverySystemStyleType>
  <markingAutoVoidPeriod>365</markingAutoVoidPeriod>
  <showPageRequiresScrollingAlert>0</showPageRequiresScrollingAlert>
  <project ID="904" />
</assessmentDetails>'
where id = 1521004
*/