--Create Tables--


/*
select
 ID, Name, ProjectId, Image 
	into #3524test
		from ProjectManifestTable 
			where projectid= 3524


select
 ID, Name, ProjectId, Image 
	into #3537test
		from ProjectManifestTable 
			where projectid= 3537

select
 ID, Name, ProjectId, Image 
	into #3519test
		from ProjectManifestTable 
			where projectid= 3519
			
select
 ID, Name, ProjectId, Image 
	into #3522test
		from ProjectManifestTable 
			where projectid= 3522
			
select
 ID, Name, ProjectId, Image 
	into #3527test
		from ProjectManifestTable 
			where projectid= 3527

select
 ID, Name, ProjectId, Image 
	into #3507test
		from ProjectManifestTable 
			where projectid= 3507							
			
*/



select 
	#3537test.id
	,#3537test.Name
	,#3537test.Projectid
	,#3537test.image as 'broken'
	,#3524test.image as 'working'
 from #3537test

	inner join #3524test
	on #3524test.ID = #3537test.ID

order by #3524test.ID



begin tran

select * from ProjectManifestTable where  ProjectId = 3537 order by ID asc
select * from ProjectManifestTable where  ProjectId = 3524 order by ID asc
--declare @tableName nvarchar(10) = '#3519test'

update ProjectManifestTable
set Image = #3524test.Image
from ProjectManifestTable
	
	inner join #3524test
	on #3524test.name = ProjectManifestTable.name

where ProjectManifestTable.ProjectId = 3537
	and #3524test.name is not null
	and Image is null

select image from ProjectManifestTable where image is null	and ProjectId = 3537
	
rollback


select id, name, image, ProjectId from  ProjectManifestTable where name in (

	(	
	select name from ProjectManifestTable where ProjectId = 3537 and Image is null
	)

)
AND image IS NOT NULL
order by ProjectId





select COUNT(ProjectId) as CT, ProjectId
	from ProjectManifestTable
		group by  ProjectId
			order by CT DESC



select id, name, image, ProjectId from  ProjectManifestTable where ProjectId = 3537 and Image is null
	
--select id, name, image, ProjectId from  ProjectManifestTable where name in ('demo_task_2.jpg','demo_task_3.jpg','reading_header_graphic.swf','reading_intro_graphic.swf')

begin tran

update ProjectManifestTable
set Image = (select Image from ProjectManifestTable where ProjectId = 3522 and ID = 33)
where ProjectId = 3537 and ID = 33

rollback


/*
SYNTAX
      UPDATE WAREHOUSE_ExamSessionTable_Shreded
      SET StructureXML = U.StructureXML, ResultData = U.ResultData
      --SELECT *
      FROM WAREHOUSE_ExamSessionTable_Shreded
      INNER JOIN @UpdatedXML U
      ON WAREHOUSE_ExamSessionTable_Shreded.ExamSessionID = U.ExamSessionID
      where U.ExamSessionID = U.ExamSessionID;
*/


--select * from ProjectListTable where id = 3524

/*

	
	
create table c
(

	id int,
	Name nvarchar(50),
	projectid int,
	image image

)


drop table #3537test
*/


