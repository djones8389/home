select 
	  ProjectListTable.ID as [ProjectID]
	 , Name as [ProjectName]
	 , p.r.value('@ID','nvarchar(12)') as PageID
	 , p.r.value('@markingSchemeFor', 'NVARCHAR(1000)') AS markingSchemeFor  
	 --, p.r.value('@chO2','nvarchar(max)') as CheckedOutTo_ComputerName
     , datalength(PageTable.SupportingFiles) as 'DataLength'
     , case OutputMedium when '0' then 'Computer-Based' else 'Paper-Based' END AS 'ProjectType'
     --, p.r.value('(@chO2)[1]', 'NVARCHAR(12)') as CheckedOut
	from ProjectListTable WITH (NOLOCK)
	
cross apply ProjectStructureXml.nodes('Pro//Pag') p(r)
	
	--left join UserTable on UserTable.UserID = p.r.value('@chO','nvarchar(max)')
	inner join PageTable WITH (NOLOCK) on PageTable.ID = CAST(ProjectListTable.ID as nvarchar(100)) + 'P' + p.r.value('@ID', 'nvarchar(1000)')
	
	where  datalength(PageTable.SupportingFiles) <= 22
     AND PageTable.ver > 1
     AND p.r.value('@markingSchemeFor', 'NVARCHAR(1000)') != '-1'

EXCEPT		
		
select 
	  ProjectListTable.ID as [ProjectID]
	 , Name as [ProjectName]
	 , p.r.value('@ID','nvarchar(12)') as PageID
	 , p.r.value('@markingSchemeFor', 'NVARCHAR(1000)') AS markingSchemeFor  
	-- , p.r.value('@chO2','nvarchar(max)') as CheckedOutTo_ComputerName
     , datalength(PageTable.SupportingFiles) as 'DataLength'
     , case OutputMedium when '0' then 'Computer-Based' else 'Paper-Based' END AS 'ProjectType'
    -- , p.r.value('(@chO2)[1]', 'NVARCHAR(12)') as CheckedOut
	from ProjectListTable WITH (NOLOCK)
	
cross apply ProjectStructureXml.nodes('Pro/Rec//Pag') p(r)
	
	--left join UserTable on UserTable.UserID = p.r.value('@chO','nvarchar(max)')
	inner join PageTable WITH (NOLOCK) on PageTable.ID = CAST(ProjectListTable.ID as nvarchar(100)) + 'P' + p.r.value('@ID', 'nvarchar(1000)')
	
	where  datalength(PageTable.SupportingFiles) <= 22
     AND PageTable.ver > 1
     AND p.r.value('@markingSchemeFor', 'NVARCHAR(1000)') != '-1'; 


