SELECT      WAREHOUSE_ExamSessionTable_Shreded.ExamSessionID
            ,ResultDataFull.value('(/assessmentDetails/assessment/@userMark)[1]', 'decimal(6, 3)') AssessmentUserMark
            ,Assessment.Section.value('(@id)[1]', 'int') SectionID
            ,Assessment.Section.value('(@userMark)[1]', 'decimal(6, 3)') SectionUserMark
            ,Section.Item.value('(@id)[1]', 'nvarchar(50)') ItemID
            ,Section.Item.value('(@userMark)[1]', 'decimal(6, 3)') ItemUserMark
            ,Section.Item.value('(@totalMark)[1]', 'int') ItemTotalMark
            ,Item.Page.value('(@id)[1]', 'nvarchar(50)') PageID
            ,Item.Page.value('(@um)[1]', 'decimal(6, 3)') PageUserMark
            ,Page.Scene.value('(@id)[1]', 'int') SceneID
            ,Page.Scene.value('(@um)[1]', 'decimal(6, 3)') SceneUserMark
            ,Scene.Component.value('(@id)[1]', 'int') ComponentID
            ,Scene.Component.value('(@um)[1]', 'decimal(6, 3)') ComponentUserMark
INTO #UserMarkBreakdown
FROM  WAREHOUSE_ExamSessionTable
INNER JOIN WAREHOUSE_ExamSessionTable_Shreded
ON WAREHOUSE_ExamSessionTable.ID = WAREHOUSE_ExamSessionTable_Shreded.ExamSessionID
CROSS APPLY ResultDataFull.nodes('/assessmentDetails/assessment/section') Assessment(Section)
CROSS APPLY Assessment.Section.nodes('item') Section(Item)
CROSS APPLY Section.Item.nodes('p') Item(Page)
CROSS APPLY Item.Page.nodes('s') Page(Scene)
CROSS APPLY Page.Scene.nodes('c') Scene(Component)
WHERE WAREHOUSE_ExamSessionTable.PreviousExamState <> 10 
AND         UserMark = 0 AND  Scene.Component.value('(@um)[1]', 'nvarchar(50)') <> 'NaN' AND  WAREHOUSE_ExamSessionTable.completionDate > '2014-04-18'
--AND       CAST(CompletionDate AS DATE) = CAST(N'2013-11-29' AS DATE);

/*Update item user marks from page user mark*/
UPDATE      #UserMarkBreakdown
SET         ItemUserMark = (CorrectMarks.ItemUserMark * ItemTotalMark)
FROM  #UserMarkBreakdown
INNER JOIN (
      SELECT      PageUserMarks.ExamSessionID
                  ,PageUserMarks.SectionID
                  ,PageUserMarks.ItemID
                  ,SUM(PageUserMarks.PageUserMark) AS ItemUserMark
      FROM  (
            SELECT      DISTINCT
                        ExamSessionID
                        ,SectionID
                        ,ItemID
                        ,PageID
                        ,PageUserMark
            FROM  #UserMarkBreakdown
                  ) PageUserMarks
      GROUP BY PageUserMarks.ExamSessionID
                  ,PageUserMarks.SectionID
                  ,PageUserMarks.ItemID
            ) CorrectMarks
            ON #UserMarkBreakdown.ExamSessionID = CorrectMarks.ExamSessionID
            AND #UserMarkBreakdown.SectionID = CorrectMarks.SectionID
            AND #UserMarkBreakdown.ItemID = CorrectMarks.ItemID;

/*Update section user mark from item user mark*/
UPDATE      #UserMarkBreakdown
SET         SectionUserMark = CorrectMarks.SectionUserMark
FROM  #UserMarkBreakdown
INNER JOIN (
      SELECT      ItemUserMarks.ExamSessionID
                  ,ItemUserMarks.SectionID
                  ,SUM(ItemUserMarks.ItemUserMark) AS SectionUserMark
      FROM  (
            SELECT      DISTINCT
                        ExamSessionID
                        ,SectionID
                        ,ItemID
                        ,ItemUserMark
            FROM  #UserMarkBreakdown
                  ) ItemUserMarks
      GROUP BY ItemUserMarks.ExamSessionID
                  ,ItemUserMarks.SectionID
            ) CorrectMarks
            ON #UserMarkBreakdown.ExamSessionID = CorrectMarks.ExamSessionID
            AND #UserMarkBreakdown.SectionID = CorrectMarks.SectionID;

/*Update assessment user mark from section user mark*/
UPDATE      #UserMarkBreakdown
SET         AssessmentUserMark = CorrectMarks.AssessmentUserMark
FROM  #UserMarkBreakdown
INNER JOIN (
      SELECT      SectionUserMarks.ExamSessionID
                  ,SUM(SectionUserMarks.SectionUserMark) AS AssessmentUserMark
      FROM  (
            SELECT      DISTINCT
                        ExamSessionID
                        ,SectionID
                        ,SectionUserMark
            FROM  #UserMarkBreakdown
                  ) SectionUserMarks
      GROUP BY ExamSessionID
            ) CorrectMarks
            ON #UserMarkBreakdown.ExamSessionID = CorrectMarks.ExamSessionID;

/*Get different node values*/
SELECT      DISTINCT
            ExamSessionID
            ,AssessmentUserMark
INTO #AssessmentUserMarks
FROM #UserMarkBreakdown
ORDER BY ExamSessionID;

SELECT      DISTINCT
            ExamSessionID
            ,SectionID
            ,SectionUserMark
INTO #SectionUserMarks
FROM #UserMarkBreakdown
ORDER BY ExamSessionID
            ,SectionID;
            
SELECT      DISTINCT
            ExamSessionID
            ,SectionID
            ,ItemID
            ,ItemUserMark
INTO #ItemUserMarks
FROM #UserMarkBreakdown
ORDER BY ExamSessionID
            ,SectionID
            ,ItemID

/*Clean up*/
DROP TABLE #UserMarkBreakdown;

/*To store our end result*/
DECLARE @UpdatedXML TABLE (ExamSessionID INT, StructureXML XML, ResultData XML, ResultDataFull XML);

/*For each assessment*/
DECLARE ExamSessions CURSOR FOR SELECT ExamSessionID FROM #AssessmentUserMarks;
DECLARE @ExamSessionID INT;

OPEN ExamSessions;
FETCH NEXT FROM ExamSessions INTO @ExamSessionID;

WHILE @@FETCH_STATUS = 0
BEGIN
      DECLARE  @StructureXML XML
                  ,@ResultData XML
                  ,@ResultDataFull XML;

      SELECT      @StructureXML = StructureXML
                  ,@ResultData = ResultData
                  ,@ResultDataFull = ResultDataFull
      FROM  WAREHOUSE_ExamSessionTable
      WHERE ID = @ExamSessionID;
      
      DECLARE     @AssessmentUserMark NVARCHAR(10);
      SELECT @AssessmentUserMark = AssessmentUserMark FROM #AssessmentUserMarks WHERE ExamSessionID = @ExamSessionID;
      
      SET @ResultDataFull.modify('replace value of (/assessmentDetails/assessment/@userMark)[1] with sql:variable("@AssessmentUserMark")');
      SET @ResultData.modify('replace value of (/exam/@userMark)[1] with sql:variable("@AssessmentUserMark")');
      
      DECLARE @AssessmentUserPercentage DECIMAL(6, 3);
      
      SET @AssessmentUserPercentage = (CAST(@AssessmentUserMark AS DECIMAL(6, 3)) / @ResultDataFull.value('(/assessmentDetails/assessment/@totalMark)[1]', 'DECIMAL(6, 3)')) * 100;
      
      SET @ResultDataFull.modify('replace value of (/assessmentDetails/assessment/@userPercentage)[1] with sql:variable("@AssessmentUserPercentage")');
      SET @ResultData.modify('replace value of (/exam/@userPercentage)[1] with sql:variable("@AssessmentUserPercentage")');
      
      DECLARE @GradeDesc NVARCHAR(100);
      IF (@AssessmentUserPercentage >= @ResultData.value('(/exam/@passMark)[1]', 'DECIMAL(6, 3)'))
      BEGIN
            SET @GradeDesc = @StructureXML.value('(/assessmentDetails/gradeBoundaryData/gradeBoundaries/grade[@modifier = "gt"]/@description)[1]', 'NVARCHAR(100)');
      
            SET @ResultData.modify('replace value of (/exam/@passValue)[1] with "1"');
            SET @ResultDataFull.modify('replace value of (/assessmentDetails/assessment/@passValue)[1] with "1"');
            SET @ResultData.modify('replace value of (/exam/@originalPassValue)[1] with "1"');
            SET @ResultData.modify('replace value of (/exam/@grade)[1] with sql:variable("@GradeDesc")');
            SET @ResultData.modify('replace value of (/exam/@originalGrade)[1] with sql:variable("@GradeDesc")');
            SET @ResultDataFull.modify('replace value of (/assessmentDetails/assessment/@originalGrade)[1] with sql:variable("@GradeDesc")');
      END
      ELSE
      BEGIN
            SET @GradeDesc = @StructureXML.value('(/assessmentDetails/gradeBoundaryData/gradeBoundaries/grade[@modifier = "lt"]/@description)[1]', 'NVARCHAR(100)');
            
            SET @ResultData.modify('replace value of (/exam/@grade)[1] with sql:variable("@GradeDesc")');
            SET @ResultData.modify('replace value of (/exam/@originalGrade)[1] with sql:variable("@GradeDesc")');
            SET @ResultDataFull.modify('replace value of (/assessmentDetails/assessment/@originalGrade)[1] with sql:variable("@GradeDesc")');
      END
      
      /*For each assessment section*/
      DECLARE ExamSections CURSOR FOR SELECT SectionID, SectionUserMark FROM #SectionUserMarks WHERE ExamSessionID = @ExamSessionID;
      DECLARE @SectionID INT, @SectionUserMark NVARCHAR(10);
      
      OPEN ExamSections;
      FETCH NEXT FROM ExamSections INTO @SectionID, @SectionUserMark;
      
      WHILE @@FETCH_STATUS = 0
      BEGIN
            
            SET @ResultDataFull.modify('replace value of (/assessmentDetails/assessment/section[@id = sql:variable("@SectionID")]/@userMark)[1] with sql:variable("@SectionUserMark")');
            SET @ResultData.modify('replace value of (/exam/section[@id = sql:variable("@SectionID")]/@userMark)[1] with sql:variable("@SectionUserMark")');
            
            DECLARE @SectionUserPercentage DECIMAL(6, 3);
            
            SET @SectionUserPercentage = (CAST(@SectionUserMark AS DECIMAL(6, 3)) / @ResultDataFull.value('(/assessmentDetails/assessment/section[@id = sql:variable("@SectionID")]/@totalMark)[1]', 'DECIMAL(6, 3)')) * 100;
            
            SET @ResultDataFull.modify('replace value of (/assessmentDetails/assessment/section[@id = sql:variable("@SectionID")]/@userPercentage)[1] with sql:variable("@SectionUserPercentage")');
            SET @ResultData.modify('replace value of (/exam/section[@id = sql:variable("@SectionID")]/@userPercentage)[1] with sql:variable("@SectionUserPercentage")');
            
            /*For each assessment section item*/
            DECLARE ExamItems CURSOR FOR SELECT ItemID, ItemUserMark FROM #ItemUserMarks WHERE ExamSessionID = @ExamSessionID AND SectionID = @SectionID;
            DECLARE @ItemID NVARCHAR(50), @ItemUserMark NVARCHAR(10);
            
            OPEN ExamItems;
            FETCH NEXT FROM ExamItems INTO @ItemID, @ItemUserMark;
            
            WHILE @@FETCH_STATUS = 0
            BEGIN
                  SET @ResultDataFull.modify('replace value of (/assessmentDetails/assessment/section[@id = sql:variable("@SectionID")]/item[@id = sql:variable("@ItemID")]/@userMark)[1] with sql:variable("@ItemUserMark")');
                  IF @ResultDataFull.exist('((/assessmentDetails/assessment/section[@id = sql:variable("@SectionID")]/item[@id = sql:variable("@ItemID")]/p[@ua = 1])[1])') = 1
                  BEGIN
                        SET @ResultDataFull.modify('replace value of (/assessmentDetails/assessment/section[@id = sql:variable("@SectionID")]/item[@id = sql:variable("@ItemID")]/@userAttempted)[1] with "1"');
                        SET @ResultData.modify('replace value of (/exam/section[@id = sql:variable("@SectionID")]/item[@id = sql:variable("@ItemID")]/@userAttempted)[1] with "1"');
                        SET @StructureXML.modify('replace value of (/assessmentDetails/assessment/section[@id = sql:variable("@SectionID")]/item[@id = sql:variable("@ItemID")]/@userAttempted)[1] with "1"');
                  END
                  SET @ResultData.modify('replace value of (/exam/section[@id = sql:variable("@SectionID")]/item[@id = sql:variable("@ItemID")]/@userMark)[1] with sql:variable("@ItemUserMark")');
                  SET @StructureXML.modify('replace value of (/assessmentDetails/assessment/section[@id = sql:variable("@SectionID")]/item[@id = sql:variable("@ItemID")]/@userMark)[1] with sql:variable("@ItemUserMark")');
            
                  FETCH NEXT FROM ExamItems INTO @ItemID, @ItemUserMark;
            END
            
            CLOSE ExamItems;
            DEALLOCATE ExamItems;
            
            FETCH NEXT FROM ExamSections INTO @SectionID, @SectionUserMark
      END
      
      CLOSE ExamSections;
      DEALLOCATE ExamSections;
      
      INSERT INTO @UpdatedXML
      VALUES(@ExamSessionID, @StructureXML, @ResultData, @ResultDataFull);
      
      FETCH NEXT FROM ExamSessions INTO @ExamSessionID;
END

/*Clean up*/
CLOSE ExamSessions;
DEALLOCATE ExamSessions;

DROP TABLE #AssessmentUserMarks;
DROP TABLE #SectionUserMarks;
DROP TABLE #ItemUserMarks;

/*Final results*/
--SELECT * FROM @UpdatedXML


/*Update source*/
BEGIN TRANSACTION;
      UPDATE WAREHOUSE_ExamSessionTable
      SET StructureXML = U.StructureXML, ResultData = U.ResultData, ResultDataFull = U.ResultDataFull
      --SELECT o.StructureXML, U.StructureXML, o.ResultData, U.ResultData, o.ResultDataFull, U.ResultDataFull
      FROM WAREHOUSE_ExamSessionTable o
      INNER JOIN @UpdatedXML U
      ON ID = U.ExamSessionID
      where ID = U.ExamSessionID;

      UPDATE WAREHOUSE_ExamSessionTable_Shreded
      SET StructureXML = U.StructureXML, ResultData = U.ResultData
      --SELECT o.StructureXML,U.StructureXML, o.ResultData , U.ResultData
      FROM WAREHOUSE_ExamSessionTable_Shreded o
      INNER JOIN @UpdatedXML U
      ON o.ExamSessionID = U.ExamSessionID
      where U.ExamSessionID = U.ExamSessionID;
COMMIT TRAN     

      BEGIN TRAN

      DECLARE IDs CURSOR FOR
      
      select  ID
      from WAREHOUSE_ExamSessionTable     
            INNER JOIN @UpdatedXML U
            ON ID = U.ExamSessionID
      where ID = U.examsessionid    
      and u.ResultData.exist('/exam/section/item[@markingType="1" and @userAttempted = "1"]') = 1
      and reMarkStatus = 0
      and PreviousExamState <> 10
      order by ID desc;

      OPEN IDs;

      DECLARE @ID int;

      FETCH NEXT FROM IDs INTO @ID;

      WHILE @@FETCH_STATUS = 0
      BEGIN

            
            exec sa_EXAMAUDITSERVICE_SetReMarkStatusForExamSessions_sp @ID, 1
            
                  
      FETCH NEXT FROM IDs INTO @ID;
      END

      CLOSE IDs;
      DEALLOCATE IDs;
