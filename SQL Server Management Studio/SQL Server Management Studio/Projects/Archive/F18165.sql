create table #AffectedAssessments

(
	ID int,
	AssessmentID int,
	Assessment xml

);

insert into #AffectedAssessments
SELECT ID, AssessmentID, Assessment
	FROM AssessmentAuditTable
		WHERE Assessment.exist('Assessment/ProjectDuration') = 0;


select * from #AffectedAssessments;

declare @ItemXML xml = 
 '<ProjectDuration>90</ProjectDuration>
  <ProjectSRBonus>0</ProjectSRBonus>
  <ProjectSRBonusMaximum>5</ProjectSRBonusMaximum>';

	
	UPDATE #AffectedAssessments 
	SET Assessment.modify('insert sql:variable("@ItemXML") as last into ((/Assessment)[1])')
	
	From #AffectedAssessments as A
	Inner join AssessmentAuditTable as B
	on B.ID = A.ID
		and B.AssessmentID = A.AssessmentID
		
	WHERE B.ID = A.ID;
	
	/*

	Update AssessmentAuditTable
	set Assessment = A.Assessment
	from AssessmentAuditTable
	inner join #AffectedAssessments A
	on A.AssessmentID = A.AssessmentID	
		and A.ID = A.ID
	where A.ID = AssessmentAuditTable.ID;

	*/

select * from #AffectedAssessments;
drop table #AffectedAssessments;