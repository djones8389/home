SELECT CentreID
  FROM [AAT_SecureAssess].[dbo].[CentreQualificationsTable]
    where QualificationID = 15
  
  --MERGE--
  
  
create table #DJTable
(
	CentreID int,
	QualificationID int
)


with cte  as (

SELECT distinct CentreID as 'CentreID'
  FROM [AAT_SecureAssess].[dbo].[CentreQualificationsTable]
      --where CentreID not IN (62,142,258,261,280,678,693,735,746,839,853)
)
    
--select * from cte

insert into #DJTable(CentreID, QualificationID)

SELECT cte.CentreID
	    , '15'
  from cte
  

select * from #DJTable

begin tran

merge into CentreQualificationsTable as TGT
using #DJTable as SRC
	on 1 = 2 -- TGT.CentreID = SRC.CentreID
when not matched then 
	insert (CentreID, QualificationID)
	Values (SRC.CentreID,  SRC.QualificationID);
	
rollback

drop table #DJTable
	


