DECLARE @OldStatus nvarchar(100) = 'Published';
DECLARE @NewStatus nvarchar(100) = 'Published In Live Use';

select ID
	, Name
	--, ProjectDefaultXml
	, PublishSettingsXml --,ProjectStructureXml
	--, a.b.query('data(StatusLevelFrom)') as StatusLevelFrom
	--, a.b.query('data(StatusLevelTo)') as ProjectPublish_StatusLevelTo
	, a.b.value('(.)[1]','nvarchar(100)') as ProjectPublish_StatusLevelTo
	--, c.d.query('data(.)')
--	, c.d.value('(.)[1]','nvarchar(100)')
	, c.d.value('@id[1]','nvarchar(100)') as Project_	
from ProjectListTable as PLT

CROSS APPLY PublishSettingsXml.nodes('PublishConfig/StatusLevelTo') a(b)
CROSS APPLY ProjectDefaultXml.nodes('Defaults/StatusList/Status') c(d)

where ID IN (959,960,961,962,963,964,965,967,975,980,992,1002,1005,1007,1008,1009,1010,1011,1012,1014,1015,1018,1019,1026,1027,1029)
		and c.d.value('(.)[1]','nvarchar(100)') = @OldStatus
			
		
		
		
		
		
		
		and c.d.value('@id[1]','nvarchar(100)')	= a.b.query('data(StatusLevelTo)'
		
		
		
		--and c.d.value('(.)[1]','nvarchar(100)') = @OldStatus
		
		and a.b.query('data(StatusLevelTo)') = c.d.value('@id/@OldStatus[1]','nvarchar(100)')
		
		
		
--and a.b.query('data(StatusLevelTo)') = 

--select
--from ProjectListTable
--where ID = 959
--	and 

	
--where name like '%National%' and IsAppointeeManagement = 1

/*
<Defaults>
  <GridSize>5</GridSize>
  <ContentWidth>1010</ContentWidth>
  <ContentHeight>680</ContentHeight>
  <DefaultFontSize>14</DefaultFontSize>
  <Fonts Embedded="1" XLIFF="0">
    <Font>Arial</Font>
    <Font>Arial SubScript</Font>
    <Font>Arial SuperScript</Font>
    <Font>Arial_Symbols</Font>
    <Font>Comic Sans MS</Font>
    <Font>ComicSans_Symbols</Font>
    <Font>SassoonSans</Font>
    <Font>Times New Roman</Font>
    <Font>Trebuchet MS</Font>
    <Font>Verdana</Font>
    <Font>BTL_Symbols</Font>
    <Font>Surpass Greek</Font>
    <Font>Surpass Maths</Font>
    <Font>Surpass Symbols</Font>
  </Fonts>
  <ToolbarFontRange>
    <from>10</from>
    <to>72</to>
  </ToolbarFontRange>
  <AdditionalPageAttributes />
  <StatusList advW="1" val="-1" nId="6">
    <Status val="-2" id="-2">Guest<users><user id="836" /><user id="827" /><user id="700" /><user id="434" /><user id="890" /><user id="888" /><user id="889" /><user id="894" /><user id="895" /><user id="904" /><user id="899" /><user id="898" /><user id="108" /><user id="900" /><user id="903" /><user id="901" /><user id="887" /><user id="817" /><user id="896" /><user id="906" /><user id="908" /><user id="909" /><user id="935" /><user id="922" /><user id="975" /><user id="938" /><user id="942" /><user id="939" /><user id="940" /><user id="941" /><user id="970" /><user id="972" /><user id="971" /><user id="925" /><user id="924" /><user id="981" /><user id="923" /><user id="934" /><user id="933" /><user id="932" /><user id="927" /><user id="1027" /><user id="1001" /><user id="1030" /><user id="977" /><user id="1105" /><user id="800" /><user id="848" /><user id="803" /><user id="1224" /><user id="1225" /><user id="1227" /><user id="1228" /><user id="1013" /><user id="1017" /><user id="1018" /><user id="1306" /><user id="974" /><user id="1390" /><user id="1399" /><user id="1401" /><user id="1340" /><user id="72" /><user id="1403" /><user id="1177" /><user id="1700" /><user id="1903" /><user id="1947" /><user id="2004" /><user id="2046" /><user id="2058" /><user id="2119" /><user id="1530" /><user id="982" /><user id="2205" /><user id="737" /><user id="2364" /><user id="1845" /><user id="2461" /><user id="2462" /><user id="2485" /><user id="2373" /><user id="2474" /><user id="2492" /><user id="2504" /><user id="1367" /><user id="2509" /></users><roles><role name="Itembank leader" /><role name="PA" /><role name="Writer" /><role name="CP Admin" /><role name="QO" /><role name="QM" /><role name="Itembank leader/PA" /><role name="Graphic artist" /><role name="Checker2" /><role name="Checker1" /></roles></Status>
    <Status id="0" val="0">Draft<roles><role name="Writer" noinherit="true" /><role name="PrincipalAssessor" noinherit="true" /></roles><users><user id="972" /><user id="970" /><user id="971" /><user id="889" noinherit="true" /><user id="1403" noinherit="true" /><user id="2119" noinherit="true" /><user id="1030" noinherit="true" /><user id="982" noinherit="true" /><user id="1340" noinherit="true" /><user id="887" noinherit="true" /><user id="935" noinherit="true" /><user id="2474" noinherit="true" /><user id="2492" noinherit="true" /><user id="1367" noinherit="true" /><user id="1018" noinherit="true" /><user id="2509" noinherit="true" /></users></Status>
    <Status id="6" val="1">Graphic Required<roles><role name="Graphic artist" noinherit="true" /><role name="Writer" noinherit="true" /><role name="PrincipalAssessor" noinherit="true" /></roles><users><user id="972" /><user id="970" /><user id="971" /><user id="925" noinherit="true" /><user id="889" noinherit="true" /><user id="1700" noinherit="true" /><user id="924" noinherit="true" /><user id="1403" noinherit="true" /><user id="981" noinherit="true" /><user id="2119" noinherit="true" /><user id="1030" noinherit="true" /><user id="982" noinherit="true" /><user id="1340" noinherit="true" /><user id="887" noinherit="true" /><user id="935" noinherit="true" /><user id="906" noinherit="true" /><user id="1306" noinherit="true" /><user id="1401" noinherit="true" /><user id="2474" noinherit="true" /><user id="2492" noinherit="true" /><user id="1367" noinherit="true" /><user id="1018" noinherit="true" /><user id="2509" noinherit="true" /></users></Status>
    <Status id="7" val="2">Graphic Produced<roles><role name="Graphic artist" noinherit="true" /><role name="Writer" noinherit="true" /><role name="PrincipalAssessor" noinherit="true" /></roles><users><user id="972" /><user id="970" /><user id="971" /><user id="925" noinherit="true" /><user id="889" noinherit="true" /><user id="1700" noinherit="true" /><user id="924" noinherit="true" /><user id="1403" noinherit="true" /><user id="981" noinherit="true" /><user id="2119" noinherit="true" /><user id="1030" noinherit="true" /><user id="982" noinherit="true" /><user id="1340" noinherit="true" /><user id="887" noinherit="true" /><user id="935" noinherit="true" /><user id="906" noinherit="true" /><user id="1306" noinherit="true" /><user id="1401" noinherit="true" /><user id="2474" noinherit="true" /><user id="2492" noinherit="true" /><user id="1367" noinherit="true" /><user id="1018" noinherit="true" /><user id="2509" noinherit="true" /></users></Status>
    <Status id="9" val="3">Graphic Amendments Required<users><user id="925" noinherit="true" /><user id="889" noinherit="true" /><user id="1700" noinherit="true" /><user id="924" noinherit="true" /><user id="972" noinherit="true" /><user id="1403" noinherit="true" /><user id="981" noinherit="true" /><user id="2119" noinherit="true" /><user id="1030" noinherit="true" /><user id="970" noinherit="true" /><user id="982" noinherit="true" /><user id="1340" noinherit="true" /><user id="887" noinherit="true" /><user id="935" noinherit="true" /><user id="971" noinherit="true" /><user id="906" noinherit="true" /><user id="1306" noinherit="true" /><user id="1401" noinherit="true" /><user id="2474" noinherit="true" /><user id="2492" noinherit="true" /><user id="1367" noinherit="true" /><user id="1018" noinherit="true" /><user id="2509" noinherit="true" /></users><roles><role name="PrincipalAssessor" noinherit="true" /><role name="Graphic artist" noinherit="true" /><role name="Writer" noinherit="true" /></roles></Status>
    <Status id="1" val="4">Requires Rework<users><user id="972" /><user id="970" /><user id="971" /><user id="982" noinherit="true" /><user id="1340" noinherit="true" /><user id="887" noinherit="true" /><user id="935" noinherit="true" /><user id="2474" noinherit="true" /><user id="1367" noinherit="true" /><user id="1018" noinherit="true" /></users><roles><role name="Itembank leader/PA" noinherit="true" /><role name="Writer" noinherit="true" /><role name="PrincipalAssessor" noinherit="true" /><role name="Checker" noinherit="true" /></roles></Status>
    <Status id="2" val="5">Ready for Check<roles><role name="Checker1" noinherit="true" /><role name="Writer" noinherit="true" /><role name="Itembank leader/PA" noinherit="true" /><role name="QM" noinherit="true" /><role name="QO" noinherit="true" /><role name="PrincipalAssessor" noinherit="true" /><role name="Checker" noinherit="true" /></roles><users><user id="935" noinherit="true" /><user id="972" /><user id="971" /><user id="887" noinherit="true" /><user id="982" noinherit="true" /><user id="1340" noinherit="true" /><user id="970" noinherit="true" /><user id="2474" noinherit="true" /><user id="1367" noinherit="true" /><user id="1018" noinherit="true" /></users></Status>
    <Status id="3" val="6">Published<users>



<PublishConfig DoNotPublishFlag="False">
  <Type>3</Type>
  <TimeType>Specific Interval</TimeType>
  <TimeValue>10</TimeValue>
  <StatusLevelFrom>3</StatusLevelFrom>
  <StatusLevelTo>3</StatusLevelTo>
  */