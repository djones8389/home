SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

IF OBJECT_ID('tempdb..#StateChanges') IS NOT NULL DROP TABLE #StateChanges;

DECLARE @StateToSearch tinyint = 5;
DECLARE @MinStateChangeDate datetime = '01 May 2015';
DECLARE @MaxStateChangeDate datetime = '15 May 2015';

DECLARE @MyESIDs TABLE
(
	ESID INT
)

CREATE TABLE #StateChanges 
(
	rowNumber tinyint
	, ExamSessionID int
	, NewState tinyint
	, StateChangeDate datetime	
)


INSERT INTO #StateChanges
SELECT

	ROW_NUMBER() OVER (PARTITION BY ExamSessionID
						ORDER BY ExamSessionID, NewState, StateChangeDate) as N
	, ExamSessionID
	, NewState
	, StateChangeDate
FROM 
	(	

SELECT 
	DISTINCT --<< Stops duplicates with same time
                ExamSessionID,
                ExamStateChangeXML.StateChange.value('(newStateID)[1]', 'int') NewState,
                ExamStateChangeXML.StateChange.value('(changeDate)[1]', 'datetime') StateChangeDate
FROM dbo.WAREHOUSE_ExamSessionTable WITH(NOLOCK)
CROSS APPLY ExamStateChangeAuditXml.nodes('exam/stateChange') ExamStateChangeXML(StateChange)

where  completionDate >  '30 April 2015'  --'2015-04-31 00:00:00.000'	
	and ( 
		   cast(ExamStateChangeAuditXml as nvarchar(MAX)) like '%<newStateID>5</newStateID>%'
			or	
		   cast(ExamStateChangeAuditXml as nvarchar(MAX)) like '%<newStateID>14</newStateID>%'
		)

) as X


UNION


SELECT ROW_NUMBER() OVER(
            PARTITION BY ExamStateChangeAuditTable.ExamSessionID
            ORDER BY ExamStateChangeAuditTable.ExamSessionID
                  ,ExamStateChangeAuditTable.StateChangeDate
      ) AS [N]
      ,ExamStateChangeAuditTable.ExamSessionID
      ,ExamStateChangeAuditTable.NewState
      ,ExamStateChangeAuditTable.StateChangeDate
FROM dbo.ExamStateChangeAuditTable
WHERE ExamStateChangeAuditTable.ExamSessionID IN (
      SELECT ExamStateChangeAuditTable.ExamSessionID
      FROM dbo.ExamStateChangeAuditTable
      WHERE YEAR(ExamStateChangeAuditTable.StateChangeDate) = 2015
      AND MONTH(ExamStateChangeAuditTable.StateChangeDate) = 5
      AND ExamStateChangeAuditTable.NewState IN (5, 14)
)

;WITH CTE AS (
      SELECT *,
            CAST(NULL AS DATETIME) AS [NextStateChangeDate]
      FROM #StateChanges
      
      UNION ALL
      
      SELECT StateChange.*
			, CTE.StateChangeDate
      FROM #StateChanges AS [StateChange]
      
      INNER JOIN CTE 
      ON StateChange.ExamSessionID = CTE.ExamSessionID
      AND StateChange.rowNumber + 1 = CTE.rowNumber
)


INSERT @MyESIDs(ESID)
SELECT DISTINCT ExamSessionID
FROM CTE
WHERE CTE.NewState IN (5, 14)
AND NextStateChangeDate IS NOT NULL
AND N'2015-05-31' BETWEEN StateChangeDate AND NextStateChangeDate;


SELECT EST.ID, EST.KeyCode, UT.CandidateRef ,UT.Forename, UT.Surname, CT.CentreName, IB.QualificationName, SCET.examName, examVersionRef
	, NewState, StateChangeDate
  FROM ExamSessionTable as EST

	INNER JOIN ScheduledExamsTable AS SCET  ON SCET.ID = EST.ScheduledExamID
	INNER JOIN UserTable AS UT  ON UT.ID = EST.UserID
	INNER JOIN CentreTable AS CT  ON CT.ID = SCET.CentreID
	INNER JOIN IB3QualificationLookup AS IB  ON IB.ID = qualificationId
	INNER JOIN #StateChanges as TEMP on TEMP.ExamSessionID = EST.ID
	
WHERE EST.ID IN (SELECT ESID From @MyESIDs)
	And StateChangeDate > @MinStateChangeDate
	And StateChangeDate < @MaxStateChangeDate
	AND EXISTS (
		select *
		from #StateChanges
		where #StateChanges.NewState = @StateToSearch
	)
		
	
UNION

SELECT WEST.ExamSessionID, WEST.KeyCode, WESTS.CandidateRef, WUT.Forename, WUT.Surname, WCT.CentreName, wests.qualificationName, WSCET.examName
	, wests.ExternalReference, NewState, StateChangeDate	
  FROM WAREHOUSE_ExamSessionTable as WEST

INNER JOIN WAREHOUSE_ScheduledExamsTable AS WSCET  ON WSCET.ID = WEST.WAREHOUSEScheduledExamID
INNER JOIN WAREHOUSE_UserTable AS WUT  ON WUT.ID = WEST.WAREHOUSEUserID
INNER JOIN WAREHOUSE_CentreTable AS WCT  ON WCT.ID = WSCET.WAREHOUSECentreID
INNER JOIN WAREHOUSE_ExamSessionTable_Shreded AS WESTS  ON WESTS.examSessionId = WEST.ID
INNER JOIN #StateChanges as TEMP on TEMP.ExamSessionID = WEST.ExamSessionID
	
WHERE WEST.ExamSessionID IN (SELECT ESID From @MyESIDs)	
	And StateChangeDate > @MinStateChangeDate
	And StateChangeDate < @MaxStateChangeDate
	AND EXISTS (
		select *
		from #StateChanges
		where #StateChanges.NewState = @StateToSearch
	)
	
	
DROP TABLE #StateChanges;