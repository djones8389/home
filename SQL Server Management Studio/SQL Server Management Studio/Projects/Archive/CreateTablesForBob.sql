create database FacilityValueData;

--select * from FacilityValueData.dbo.WAREHOUSE_ExamSessionTable_Shreded
--select * from FacilityValueData.dbo.WAREHOUSE_ExamSessionTable_ShrededItems	
--drop database FacilityValueData;
--drop table FacilityValueData.dbo.WAREHOUSE_ExamSessionTable_Shreded
--drop table FacilityValueData.dbo.WAREHOUSE_ExamSessionTable_ShrededItems	

create table FacilityValueData.dbo.WAREHOUSE_ExamSessionTable_Shreded(
	[examSessionId] [int] NOT NULL,
	[structureXml] [xml] NULL,
	[examVersionName] [nvarchar](max) NULL,
	[examVersionRef] [nvarchar](max) NULL,
	[examVersionId] [int] NULL,
	[examName] [nvarchar](max) NULL,
	[examRef] [nvarchar](max) NULL,
	[qualificationid] [int] NULL,
	[qualificationName] [nvarchar](max) NULL,
	[qualificationRef] [nvarchar](max) NULL,
	[resultData] [xml] NULL,
	[submittedDate] [datetime] NULL,
	[originatorId] [int] NULL,
	[centreName] [nvarchar](max) NULL,
	[centreCode] [nvarchar](max) NULL,
	[foreName] [nvarchar](max) NULL,
	[dateOfBirth] [smalldatetime] NULL,
	[gender] [char](1) NULL,
	[candidateRef] [nvarchar](max) NULL,
	[surName] [nvarchar](max) NULL,
	[scheduledDurationValue] [int] NULL,
	[previousExamState] [int] NULL,
	[examStateInformation] [xml] NULL,
	[examResult] [float] NULL,
	[passValue] [bit] NULL,
	[closeValue] [bit] NULL,
	[ExternalReference] [nvarchar](100) NULL,
	[examType] [int] NULL,
	[warehouseTime] [datetime] NULL,
	[CQN] [nvarchar](20) NULL,
	[actualDuration] [int] NULL,
	[appeal] [bit] NULL,
	[reMarkStatus] [int] NULL,
	[qualificationLevel] [int] NULL,
	[centreId] [int] NULL,
	[ULN] [nvarchar](10) NULL,
	[AllowPackageDelivery] [bit] NOT NULL,
	[ExportToSecureMarker] [bit] NOT NULL,
	[ContainsBTLOffice] [bit] NULL,
	[ExportedToIntegration] [bit] NULL,
	[WarehouseExamState] [int] NOT NULL,
	[grade]  nvarchar(100) not null,
	[userMark] nvarchar(100) not null,
	[userPercentage] nvarchar(100) not null,
	[adjustedGrade]  nvarchar(100) not null,
	[language] [nchar](10) NULL,
	[KeyCode] [varchar](12) NOT NULL,
	[TargetedForVoid] [xml] NULL,
	[EnableOverrideMarking] [bit] NOT NULL,
	[originalGrade]  nvarchar(100) not null,
	[IsExternal] [bit] NOT NULL,
	[AddressLine1] [nvarchar](100) NULL,
	[AddressLine2] [nvarchar](100) NULL,
	[Town] [nvarchar](100) NULL,
	[County] [nvarchar](70) NULL,
	[Country] [nvarchar](50) NULL,
	[Postcode] [nvarchar](12) NULL,
	[ScoreBoundaryData] [xml] NULL,
	[itemMarksUploaded] [bit] NOT NULL,
	[passMark] [decimal](6, 3) NULL,
	[totalMark] [decimal](6, 3) NULL,
	[passType] [int] NULL,
	[voidJustificationLookupTableId] [int] NULL,
	[automaticVerification] [bit] NULL,
	[resultSampled] [bit] NULL,
	[started] [datetime] NULL,
	[submitted] [datetime] NULL,
	[validFromDate] [datetime] NULL,
	[expiryDate] [datetime] NULL,
	[totalTimeSpent] [int] NULL,
	[defaultDuration] [int] NULL,
	[scheduledDurationReason] [nvarchar](max) NULL,
	[WAREHOUSEScheduledExamID] [int] NULL,
	[WAREHOUSEUserID] [int] NULL,
	[itemDataFull] [xml] NULL,
	[examId] [int] NULL,
	[userId] [int] NULL,
	[TakenThroughLocalScan] [bit] NOT NULL,
	[LocalScanDownloadDate] [datetime] NULL,
	[LocalScanUploadDate] [datetime] NULL,
	[LocalScanNumPages] [int] NULL,
	[CertifiedForTablet] [bit] NOT NULL,
	[IsProjectBased] [bit] NOT NULL,
	[AssignedMarkerUserID] [int] NULL,
	[AssignedModeratorUserID] [int] NULL
	);
	
create table FacilityValueData.dbo.WAREHOUSE_ExamSessionTable_ShrededItems(
	[examSessionId] [int] NOT NULL,
	[ItemRef] [varchar](15) NULL,
	[ItemName] [nvarchar](200) NULL,
	[userMark] [decimal](6, 3) NULL,
	[markerUserMark] [nvarchar](max) NULL,
	[examPercentage] [decimal](6, 3) NULL,
	[candidateId] [int]  NOT NULL,
	[ItemVersion] [int] NULL,
	[responsexml] [xml] NULL,
	[OptionsChosen] [varchar](200) NULL,
	[selectedCount] [int] NULL,
	[correctAnswerCount] [int] NULL,
	[TotalMark] [decimal](6, 3) NOT NULL,
	[userAttempted] [bit] NULL,
	[markingIgnored] [tinyint] NULL,
	[markedMetadataCount] [int] NULL,
	[ItemType] [int] NULL,
	[FriendItems] [nvarchar](max) NULL
	);
	
insert into FacilityValueData.dbo.WAREHOUSE_ExamSessionTable_Shreded (examSessionId, structureXml, examVersionName, examVersionRef, examVersionId, examName, examRef, qualificationid, qualificationName, qualificationRef, resultData, submittedDate, originatorId, centreName, centreCode, foreName, dateOfBirth, gender, candidateRef, surName, scheduledDurationValue, previousExamState, examStateInformation, examResult, passValue, closeValue, ExternalReference, examType, warehouseTime, CQN, actualDuration, appeal, reMarkStatus, qualificationLevel, centreId, ULN, AllowPackageDelivery, ExportToSecureMarker, ContainsBTLOffice, ExportedToIntegration, WarehouseExamState, grade, userMark, userPercentage, adjustedGrade, language, KeyCode, TargetedForVoid, EnableOverrideMarking, originalGrade, IsExternal, AddressLine1, AddressLine2, Town, County, Country, Postcode, ScoreBoundaryData, itemMarksUploaded, passMark, totalMark, passType, voidJustificationLookupTableId, automaticVerification, resultSampled, started, submitted, validFromDate, expiryDate, totalTimeSpent, defaultDuration, scheduledDurationReason, WAREHOUSEScheduledExamID, WAREHOUSEUserID, itemDataFull, examId, userId, TakenThroughLocalScan, LocalScanDownloadDate, LocalScanUploadDate, LocalScanNumPages, CertifiedForTablet, IsProjectBased, AssignedMarkerUserID, AssignedModeratorUserID)
select examSessionId, structureXml, examVersionName, examVersionRef, examVersionId, examName, examRef, qualificationid, qualificationName, qualificationRef, resultData, submittedDate, originatorId, centreName, centreCode, foreName, dateOfBirth, gender, candidateRef, surName, scheduledDurationValue, previousExamState, examStateInformation, examResult, passValue, closeValue, ExternalReference, examType, warehouseTime, CQN, actualDuration, appeal, reMarkStatus, qualificationLevel, centreId, ULN, AllowPackageDelivery, ExportToSecureMarker, ContainsBTLOffice, ExportedToIntegration, WarehouseExamState, grade, userMark, userPercentage, adjustedGrade, language, KeyCode, TargetedForVoid, EnableOverrideMarking, originalGrade, IsExternal, AddressLine1, AddressLine2, Town, County, Country, Postcode, ScoreBoundaryData, itemMarksUploaded, passMark, totalMark, passType, voidJustificationLookupTableId, automaticVerification, resultSampled, started, submitted, validFromDate, expiryDate, totalTimeSpent, defaultDuration, scheduledDurationReason, WAREHOUSEScheduledExamID, WAREHOUSEUserID, itemDataFull, examId, userId, TakenThroughLocalScan, LocalScanDownloadDate, LocalScanUploadDate, LocalScanNumPages, CertifiedForTablet, IsProjectBased, AssignedMarkerUserID, AssignedModeratorUserID
 from AAT_SecureAssess..WAREHOUSE_ExamSessionTable_Shreded
	where examSessionId in (1090109,1090109,1090109,1090109,1090109,1090109,1090109,1090109,1090109,1090109,1090109,1090109,1113536,1113536,1113536,1113536,1113536,1113536,1113536,1113536,1113536,1113536,1113536,1113536,1316515,1316515,1316515,1316515,1316515,1316515,1316515,1316515,1316515,1316515,1316515,1316515,1348214,1348214,1348214,1348214,1348214,1348214,1348214,1348214,1348214,1348214,1348214,1348214,1348215,1348215,1348215,1348215,1348215,1348215,1348215,1348215,1348215,1348215,1348215,1348215,1348216,1348216,1348216,1348216,1348216,1348216,1348216,1348216,1348216,1348216,1348216,1348216,1348217,1348217,1348217,1348217,1348217,1348217,1348217,1348217,1348217,1348217,1348217,1348217,1348218,1348218,1348218,1348218,1348218,1348218,1348218,1348218,1348218,1348218,1348218,1348218,1348219,1348219,1348219,1348219,1348219,1348219,1348219,1348219,1348219,1348219,1348219,1348219,1348220,1348220,1348220,1348220,1348220,1348220,1348220,1348220,1348220,1348220,1348220,1348220,1348221,1348221,1348221,1348221,1348221,1348221,1348221,1348221,1348221,1348221,1348221,1348221,1348222,1348222,1348222,1348222,1348222,1348222,1348222,1348222,1348222,1348222,1348222,1348222,1348223,1348223,1348223,1348223,1348223,1348223,1348223,1348223,1348223,1348223,1348223,1348223,1348224,1348224,1348224,1348224,1348224,1348224,1348224,1348224,1348224,1348224,1348224,1348224,1348225,1348225,1348225,1348225,1348225,1348225,1348225,1348225,1348225,1348225,1348225,1348225,1348226,1348226,1348226,1348226,1348226,1348226,1348226,1348226,1348226,1348226,1348226,1348226,1348227,1348227,1348227,1348227,1348227,1348227,1348227,1348227,1348227,1348227,1348227,1348227,1348228,1348228,1348228,1348228,1348228,1348228,1348228,1348228,1348228,1348228,1348228,1348228,1348229,1348229,1348229,1348229,1348229,1348229,1348229,1348229,1348229,1348229,1348229,1348229,1348230,1348230,1348230,1348230,1348230,1348230,1348230,1348230,1348230,1348230,1348230,1348230,1348231,1348231,1348231,1348231,1348231,1348231,1348231,1348231,1348231,1348231,1348231,1348231,1348232,1348232,1348232,1348232,1348232,1348232,1348232,1348232,1348232,1348232,1348232,1348232,1348233,1348233,1348233,1348233,1348233,1348233,1348233,1348233,1348233,1348233,1348233,1348233,1350318,1350318,1350318,1350318,1350318,1350318,1350318,1350318,1350318,1350318,1350318,1350318,1350895,1350895,1350895,1350895,1350895,1350895,1350895,1350895,1350895,1350895,1350895,1350895,1350896,1350896,1350896,1350896,1350896,1350896,1350896,1350896,1350896,1350896,1350896,1350896);


insert into FacilityValueData.dbo.WAREHOUSE_ExamSessionTable_ShrededItems (examSessionId, ItemRef, ItemName, userMark, markerUserMark, examPercentage, candidateId, ItemVersion, responsexml, OptionsChosen, selectedCount, correctAnswerCount, TotalMark, userAttempted, markingIgnored, markedMetadataCount, ItemType, FriendItems)
	select examSessionId, ItemRef, ItemName, userMark, markerUserMark, examPercentage, candidateId, ItemVersion, responsexml, OptionsChosen, selectedCount, correctAnswerCount, TotalMark, userAttempted, markingIgnored, markedMetadataCount, ItemType, FriendItems
		 from AAT_SecureAssess..WAREHOUSE_ExamSessionTable_ShrededItems
		where examSessionId in (1090109,1090109,1090109,1090109,1090109,1090109,1090109,1090109,1090109,1090109,1090109,1090109,1113536,1113536,1113536,1113536,1113536,1113536,1113536,1113536,1113536,1113536,1113536,1113536,1316515,1316515,1316515,1316515,1316515,1316515,1316515,1316515,1316515,1316515,1316515,1316515,1348214,1348214,1348214,1348214,1348214,1348214,1348214,1348214,1348214,1348214,1348214,1348214,1348215,1348215,1348215,1348215,1348215,1348215,1348215,1348215,1348215,1348215,1348215,1348215,1348216,1348216,1348216,1348216,1348216,1348216,1348216,1348216,1348216,1348216,1348216,1348216,1348217,1348217,1348217,1348217,1348217,1348217,1348217,1348217,1348217,1348217,1348217,1348217,1348218,1348218,1348218,1348218,1348218,1348218,1348218,1348218,1348218,1348218,1348218,1348218,1348219,1348219,1348219,1348219,1348219,1348219,1348219,1348219,1348219,1348219,1348219,1348219,1348220,1348220,1348220,1348220,1348220,1348220,1348220,1348220,1348220,1348220,1348220,1348220,1348221,1348221,1348221,1348221,1348221,1348221,1348221,1348221,1348221,1348221,1348221,1348221,1348222,1348222,1348222,1348222,1348222,1348222,1348222,1348222,1348222,1348222,1348222,1348222,1348223,1348223,1348223,1348223,1348223,1348223,1348223,1348223,1348223,1348223,1348223,1348223,1348224,1348224,1348224,1348224,1348224,1348224,1348224,1348224,1348224,1348224,1348224,1348224,1348225,1348225,1348225,1348225,1348225,1348225,1348225,1348225,1348225,1348225,1348225,1348225,1348226,1348226,1348226,1348226,1348226,1348226,1348226,1348226,1348226,1348226,1348226,1348226,1348227,1348227,1348227,1348227,1348227,1348227,1348227,1348227,1348227,1348227,1348227,1348227,1348228,1348228,1348228,1348228,1348228,1348228,1348228,1348228,1348228,1348228,1348228,1348228,1348229,1348229,1348229,1348229,1348229,1348229,1348229,1348229,1348229,1348229,1348229,1348229,1348230,1348230,1348230,1348230,1348230,1348230,1348230,1348230,1348230,1348230,1348230,1348230,1348231,1348231,1348231,1348231,1348231,1348231,1348231,1348231,1348231,1348231,1348231,1348231,1348232,1348232,1348232,1348232,1348232,1348232,1348232,1348232,1348232,1348232,1348232,1348232,1348233,1348233,1348233,1348233,1348233,1348233,1348233,1348233,1348233,1348233,1348233,1348233,1350318,1350318,1350318,1350318,1350318,1350318,1350318,1350318,1350318,1350318,1350318,1350318,1350895,1350895,1350895,1350895,1350895,1350895,1350895,1350895,1350895,1350895,1350895,1350895,1350896,1350896,1350896,1350896,1350896,1350896,1350896,1350896,1350896,1350896,1350896,1350896);
		
--drop database FacilityValueData;
--drop table FacilityValueData.dbo.WAREHOUSE_ExamSessionTable_Shreded;
--drop table FacilityValueData.dbo.WAREHOUSE_ExamSessionTable_ShrededItems;
