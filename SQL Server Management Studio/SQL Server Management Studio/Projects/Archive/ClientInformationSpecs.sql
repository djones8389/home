DECLARE @CentreName nvarchar(150) = ''

select 
	downloadInformation.value('data(/downloads/information/machineName)[1]', 'nvarchar(max)') as 'Computer Name'
	,KeyCode
	,clientInformation.value('data(/clientInformation/systemConfiguration/dotNet)[1]', 'nvarchar(max)') as 'FULL.Net Version'
	,clientInformation.value('data(/clientInformation/systemConfiguration/dotNet/version)[last()]', 'nvarchar(max)') as '.Net Version'
	,clientInformation.value('data(/clientInformation/systemConfiguration/flashPlayer/version)[1]', 'nvarchar(max)') as 'Flash Version'
	,clientInformation.value('data(/clientInformation/systemConfiguration/secureClient/Application)[1]', 'nvarchar(max)') as 'SecureClient Version'
	,clientInformation.value('data(/clientInformation/systemConfiguration/environment/operatingSystem/version)[1]', 'nvarchar(max)') as 'Operating System'
	,clientInformation.value('data(/clientInformation/systemConfiguration/environment/operatingSystem/servicePack)[1]', 'nvarchar(max)') as 'O/S Service Pack'
	,clientInformation.value('data(/clientInformation/systemConfiguration/environment/processor/name)[1]', 'nvarchar(max)') as 'CPU'
	,clientInformation.value('data(/clientInformation/systemConfiguration/environment/memory/installedPhysicalMemory)[1]', 'nvarchar(max)') as 'RAM'
	,clientInformation.value('data(/clientInformation/systemConfiguration/environment/hardDisk/drives/drive/freeSpace)[1]', 'nvarchar(max)') as 'Free HD Space'
	
from WAREHOUSE_ExamSessionTable as WEST

	inner join WAREHOUSE_ScheduledExamsTable as WSCET
	on WSCET.ID = WEST.WAREHOUSEScheduledExamID
	
	inner join WAREHOUSE_CentreTable as WCT
	on WCT.ID = WSCET.WAREHOUSECentreID

Where CentreName = @CentreName


--clientInformation is not null and downloadInformation is not null
--	and clientInformation.value('count(/clientInformation/systemConfiguration)', 'int') > 1
--	and downloadInformation.value('count(/downloads/information)', 'int') > 1
/*
<clientInformation>
  <systemConfiguration source="1" web="0">
 
  </systemConfiguration>
</clientInformation>
*/
--secureClient/Application
/*
dotNet
flashPlayer
secureClient

      <operatingSystem>
        <version>Microsoft Windows 7 Enterprise </version>
        <servicePack>Service Pack 1.0</servicePack>
      </operatingSystem>
      
       <memory>
        <installedPhysicalMemory>2 GB</installedPhysicalMemory>
        <totalVisibleMemorySize>1.96 GB</totalVisibleMemorySize>
        <availablePhysicalMemory>1 GB</availablePhysicalMemory>
      </memory>
      
     <hardDisk>
        <drives>
          <drive>
            <name>C:</name>
            <totalSize>225.93 GB</totalSize>
            <freeSpace>182.47 GB</freeSpace>
          </drive>
        </drives>
      </hardDisk>
      
 */