DELETE from ProjectPaperTemplateTable 
where ProjectID in (
                  SELECT ID 
                  from ProjectListTable 
                  where OutputMedium = 1
      );
      
DELETE from ContractPage
where ContractID in (
                  SELECT Contract.ID 
                  from Contract 
                  inner join ProjectListTable
                  on ProjectListTable.ID = Contract.ProjectID
                  where OutputMedium = 1
      );
 
DELETE from Contract 
where ProjectID in (
                  SELECT ID 
                  from ProjectListTable 
                  where OutputMedium = 1
      );
 
 
DELETE
from ProjectListTable
where OutputMedium = 1;
