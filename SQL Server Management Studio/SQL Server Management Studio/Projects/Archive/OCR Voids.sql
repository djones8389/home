SELECT EST.ID, EST.KeyCode, EST.examState, UT.Forename, UT.Surname, CT.CentreName, SCET.examName, examVersionRef
  FROM ExamSessionTable as EST

  Inner Join ScheduledExamsTable as SCET
  on SCET.ID = EST.ScheduledExamID
  
  Inner Join UserTable as UT
  on UT.ID = EST.UserID

  Inner Join CentreTable as CT
  on CT.ID = SCET.CentreID

where  examState = 15
	and examVersionRef like '%PQUM_00CBT%Practice%' 
	or examVersionRef like '%PQXX_02CBT%Practice%'
	or examVersionRef like '%PQXX_03CBT%Practice%' 
	or examVersionRef like '%PQXX_04CBT%Practice%'
	or examVersionRef like '%PQXX_05CBT%Practice%'
	or examVersionRef like '%PQXX_06CBT%Practice%'
	or examVersionRef like '%PQXX_07CBT%Practice%'
	or examVersionRef like '%PQXX_08CBT%Practice%'
	or examVersionRef like '%PQXX_09CBT%Practice%' 
	or examVersionRef like '%PQXX_10CBT%Practice%'
	or examVersionRef like '%PQXX_11CBT%Practice%'
	or examVersionRef like '%PQXX_12CBT%Practice%'
	or examVersionRef like '%PQXX_13CBT%Practice%'
	or examVersionRef like '%PQXX_14CBT%Practice%'
	or examVersionRef like '%PQXX_15CBT%Practice%'
	or examVersionRef like '%PQXX_16CBT%Practice%'
	or examVersionRef like '%PQXX_17CBT%Practice%'
	or examVersionRef like '%PQXX_18CBT%Practice%'
	or examVersionRef like '%PQXX_19CBT%Practice%'
	or examVersionRef like '%PQXX_20CBT%Practice%'
	or examVersionRef like '%PQXX_21CBT%Practice%'
order by EST.ID ASC;	





SELECT EST.ID, EST.KeyCode, EST.examState, UT.Forename, UT.Surname, CT.CentreName, SCET.examName, examVersionRef
  FROM ExamSessionTable as EST

  Inner Join ScheduledExamsTable as SCET
  on SCET.ID = EST.ScheduledExamID
  
  Inner Join UserTable as UT
  on UT.ID = EST.UserID

  Inner Join CentreTable as CT
  on CT.ID = SCET.CentreID

where  examState = 15
	and examVersionRef like '%PQUM_00CBT%'
	or examVersionRef like '%PQXX_02CBT%'
	or examVersionRef like '%PQXX_03CBT%'
	or examVersionRef like '%PQXX_04CBT%'
	or examVersionRef like '%PQXX_05CBT%'
	or examVersionRef like '%PQXX_06CBT%'
	or examVersionRef like '%PQXX_07CBT%'
	or examVersionRef like '%PQXX_08CBT%'
	or examVersionRef like '%PQXX_09CBT%'
	or examVersionRef like '%PQXX_10CBT%'
	or examVersionRef like '%PQXX_11CBT%'
	or examVersionRef like '%PQXX_12CBT%'
	or examVersionRef like '%PQXX_13CBT%'
	or examVersionRef like '%PQXX_14CBT%'
	or examVersionRef like '%PQXX_15CBT%'
	or examVersionRef like '%PQXX_16CBT%'
	or examVersionRef like '%PQXX_17CBT%'
	or examVersionRef like '%PQXX_18CBT%'
	or examVersionRef like '%PQXX_19CBT%'
	or examVersionRef like '%PQXX_20CBT%'
	or examVersionRef like '%PQXX_21CBT%'
order by EST.ID ASC;	