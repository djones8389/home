select Result.Item.value('@userAttempted', 'bit') AS [userAttempted]
,	 resultData

from WAREHOUSE_ExamSessionTable 

CROSS APPLY WAREHOUSE_ExamSessionTable.ResultData.nodes('exam/section/item') Result(Item)

where ID = '160938'



select 
	   result.item.value ('@userAttempted', 'bit') as 'UA From ResultData'
	 	, WAREHOUSE_ExamSessionTable_ShrededItems.userAttempted as 'UA from ShrededItems'
	 	, WAREHOUSE_ExamSessionTable.ID
	 	, WAREHOUSE_ExamSessionTable.KeyCode
	from WAREHOUSE_ExamSessionTable 

CROSS APPLY WAREHOUSE_ExamSessionTable.ResultData.nodes('//item') Result(Item)
	 	
    inner join WAREHOUSE_ExamSessionTable_ShrededItems
	on WAREHOUSE_ExamSessionTable_ShrededItems.examSessionId = WAREHOUSE_ExamSessionTable.ID
	 
where WAREHOUSE_ExamSessionTable_ShrededItems.ItemRef = result.item.value('@id','nvarchar(100)')
		and result.item.value ('@userAttempted', 'bit') <> WAREHOUSE_ExamSessionTable_ShrededItems.userAttempted
		--and WAREHOUSE_ExamSessionTable.ID = 160938 


select * from WAREHOUSE_ExamSessionTable_ShrededItems where examSessionId = 160938