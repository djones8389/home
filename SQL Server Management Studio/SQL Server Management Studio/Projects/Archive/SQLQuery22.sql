select top 10 

ExamStateChangeAuditXml.value('data(/exam/stateChange[newStateID=1]/changeDate)[1]', 'nvarchar(max)') as 'State 1'
,ExamStateChangeAuditXml.value('data(/exam/stateChange[newStateID=2]/changeDate)[1]', 'nvarchar(max)') as 'First State 2'
, ExamStateChangeAuditXml.value('count(/exam/stateChange[newStateID=2])', 'int') as 'CountOfState2s'
, ExamStateChangeAuditXml
, DATALENGTH(ExamStateChangeAuditXml)
, warehouseTime
from WAREHOUSE_ExamSessionTable 

where warehouseTime > '01 Nov 2014' and ExamStateChangeAuditXml.value('count(/exam/stateChange[newStateID=2])', 'int') >  1

order by warehouseTime desc