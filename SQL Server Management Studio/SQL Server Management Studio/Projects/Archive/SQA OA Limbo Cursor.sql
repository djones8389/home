DECLARE Limbo CURSOR FOR
	select ID
	FROM ExamSessionTable
	where ExamState = 99


DECLARE @ESID int;

OPEN Limbo;

FETCH NEXT FROM Limbo INTO @ESID;

WHILE @@FETCH_STATUS = 0

BEGIN

WAITFOR DELAY '000:00:10'

update ExamSessionTable
set previousExamState = examState, examState = 9
where ID = @ESID;

--PRINT @ESID;
--PRINT GETDATE();

FETCH NEXT FROM Limbo INTO @ESID;

END
CLOSE Limbo;
DEALLOCATE Limbo;






--WAITFOR DELAY '000:00:10'
--select top 1 ID from ExamSessionTable
--PRINT GETDATE()
--GO

--WAITFOR DELAY '000:00:10'
--select top 5 ID from ExamSessionTable
--PRINT GETDATE()
--GO