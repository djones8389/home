use SADB
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[BTLDaveJ_WhoScheduledThisExam_Warehouse]

@examSessionID int
AS
BEGIN


create table #CreatedByTempTable
(
	userID int,
	Forename nvarchar(20),
	Surname nvarchar(20)
)

SELECT WAREHOUSECreatedBy, WUT.Forename, WUT.Surname
 FROM WAREHOUSE_ExamSessionTable as WEST

  Inner Join WAREHOUSE_ScheduledExamsTable as WSCET
  on WSCET.ID = WEST.WAREHOUSEScheduledExamID
  
  Inner Join WAREHOUSE_UserTable as WUT
  on WUT.ID = WSCET.WAREHOUSECreatedBy

where WEST.ID = @examSessionID



drop table #CreatedByTempTable
--drop procedure [dbo].[BTLDaveJ_WhoScheduledThisExam_Warehouse]
END