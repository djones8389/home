/*DECLARE @myKeycode NVARCHAR(12)
SET @myKeycode = 'ZC7MGU01'


select COUNT(UserID) from AssignedUserRoles where RoleID = 30

select UserID, Username, RoleID from AssignedUserRoles 

	inner join Users On Users.ID = AssignedUserRoles.UserID

where RoleID = 30 or username = 'jacobchandy'
*/

--SELECT Items.ExternalItemName, Users.Forename, Users.Surname, AssignedGroupMarks.Timestamp, MarkingMethodId, MarkingMethodLookup.markingMethod
select Users.UserName, Keycode, DateSubmitted, markingMethod

	FROM CandidateExamVersions
	INNER JOIN CandidateResponses
	ON CandidateResponses.CandidateExamVersionID = CandidateExamVersions.ID
	INNER JOIN AssignedItemMarks
	ON AssignedItemMarks.UniqueResponseId = CandidateResponses.UniqueResponseID
	INNER JOIN AssignedGroupMarks
	ON AssignedGroupMarks.ID = AssignedItemMarks.GroupMarkId AND AssignedGroupMarks.IsConfirmedMark = 1
	INNER JOIN Users
	ON Users.ID = AssignedGroupMarks.UserId
	INNER JOIN UniqueResponses
	ON UniqueResponses.ID = CandidateResponses.UniqueResponseID
	INNER JOIN Items
	ON Items.ID = UniqueResponses.itemId
	INNER JOIN MarkingMethodLookup
	ON MarkingMethodLookup.id = MarkingMethodId
	inner join ExamVersions
	on ExamVersions.ID = Items.ExamVersionID

WHERE Users.UserName != 'automarker'
	and users.ID not in 
	(	
		select UserID
			from AssignedUserRoles as AUR
			
			inner join Roles as R
			on R.ID = AUR.RoleID
			
			inner join RolePermissions as RP
			on RP.RoleID = R.ID
			
			inner join Permissions as P
			on P.ID = RP.PermissionID
				
				where RP.PermissionID = 42
				 --permid42
		)
	and IsMarkOneItemPerScript = 1
	AND AssignedGroupMarks.[Timestamp] > '01-01-2015'
	group by Users.Username, Keycode, DateSubmitted, markingMethod
	
	having COUNT(*) > 1
	order by DateSubmitted desc;
	
	
	--68024--
	--59368--
	--6943--
	--select * from AssignedUserRoles 
	--where userID not in (26,31,126,126,150,170,172,172,173,174,175,176,183,190,192,196)
	--and RoleID != 21