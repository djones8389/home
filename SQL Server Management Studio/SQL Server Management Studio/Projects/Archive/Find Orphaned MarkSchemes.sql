USE SQA_CPProjectAdmin

IF OBJECT_ID('tempdb..#Temp') IS NOT NULL DROP TABLE #Temp;
IF OBJECT_ID('tempdb..#ProjectStructure') IS NOT NULL DROP TABLE #ProjectStructure;

select  
	ProjectListTable.id as ProjectID
	, pro.pag.value('@ID','nvarchar(100)') as PageID
	, Name
	, pro.pag.value('@markingSchemeFor','nvarchar(100)')  as markingSchemeFor
	into #Temp
from ProjectListTable (NOLOCK)

	cross apply ProjectStructureXml.nodes('Pro/*[local-name(.)!="Rec"]//Pag') pro(pag);
		
	--Find your orphaned users

DECLARE @HoldingTable TABLE (ProjectID int, PageID int)	
INSERT INTO @HoldingTable(ProjectID, PageID)

	select ProjectID, markingSchemeFor from #Temp
	EXCEPT
	select ProjectID, PageID from #Temp

	--Write ProjectStructureXml into TempTable for the cursor

select ID, ProjectStructureXml							
	into #ProjectStructure
	from ProjectListTable
 	where ID IN (
		select distinct projectid  from @HoldingTable
			where PageID is not null
			and PageID != -1
	)

	--Create a cursor and narrow it down to the pages we need
	
DECLARE DeleteProjectStructure 
CURSOR FOR 

SELECT top 100 ProjectID, PageID		
FROM @HoldingTable
	 
	left join PageTable as PT
	on PT.ID = cast(ProjectID as nvarchar(6)) + 'P' + cast(PageID as nvarchar(6))
	
where PageID is not null		
	and PageID != -1
	and PT.ID IS NULL

--select * from @HoldingTable

Open DeleteProjectStructure;	

DECLARE @ProjectID int, @PageID int;

FETCH NEXT FROM DeleteProjectStructure INTO @ProjectID, @PageID;

WHILE @@FETCH_STATUS = 0


--select * from #ProjectStructure

BEGIN

--PRINT 'ProjectID =' + cast(@ProjectID as nvarchar(MAX))
--PRINT 'PageID = ' +  cast(@PAGEID as nvarchar(MAX))

--PRINT 'Update #ProjectStructure set ProjectStructureXml.modify(''delete(/Pro[@ID=sql:variable("' + cast(@ProjectID as nvarchar(MAX)) +'")] [@markingSchemeFor=sql:variable("' + cast(@PAGEID as nvarchar(MAX)) + '")])'') where ID = ' + cast(@ProjectID as nvarchar(MAX))
Update #ProjectStructure
set ProjectStructureXml.modify('delete(/Pro[@ID=sql:variable("@ProjectID")] [@markingSchemeFor=sql:variable("@PageID")])')
where ID = @ProjectID

FETCH NEXT FROM DeleteProjectStructure INTO @ProjectID, @PageID;

END

CLOSE DeleteProjectStructure;
DEALLOCATE DeleteProjectStructure;


--select * from #ProjectStructure



--Be sure to target the PageID node where markingSchemeFor is in my query



--DROP TABLE #Temp;
--DROP TABLE #ProjectStructure;



/*
Update #ProjectStructure
set ProjectStructureXml.modify('delete(/Pro[@ID=sql:variable("@ProjectID")] [@markingSchemeFor=sql:variable("@PageID")])')
where ID = @ProjectID
--set Structurexml.modify('delete(/sharedLibrary/item[@id=sql:variable("@ID")]  [@name=sql:variable("@Name")])')
*/

--Begin tran

--Update #ProjectStructure set ProjectStructureXml.modify('delete(/Pro[@ID=("946")] [@markingSchemeFor=("967")])') where ID =946

--rollback

--select ProjectStructureXml from #ProjectStructure where ID = 946
--select ProjectStructureXml from ProjectListTable where ID = 946
--begin tran
--select ProjectStructureXml from #ProjectStructure where ID = 946
--Update #ProjectStructure set ProjectStructureXml.modify('delete(/Pro//Pag[@markingSchemeFor=("967")])') where ID = 946
--select ProjectStructureXml from #ProjectStructure where ID = 946
--rollback