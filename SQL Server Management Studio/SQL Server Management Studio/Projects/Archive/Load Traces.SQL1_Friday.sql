select X.* 
	into #FULL_LIST
from
(
--1--
select 
	  CAST(TextData as nvarchar(max)) TextData
	, SPID
	, Duration
	, StartTime
	, EndTime
	, Reads
	, Writes
	, CPU
	, EventClass
--into #SQL1Friday
from sys.fn_trace_gettable('C:\Users\DaveJ\Desktop\TomG_Traces\SQL1_Friday.trc', default)
	--into #SQL1_Friday.trc
	where DatabaseName = 'AAT_SecureAssess'
	and ApplicationName = 'Microsoft SQL Server Management Studio - Query'

UNION ALL

--2--
select 
	  CAST(TextData as nvarchar(max)) TextData
	, SPID
	, Duration
	, StartTime
	, EndTime
	, Reads
	, Writes
	, CPU
	, EventClass
--into #SQL1Friday
from sys.fn_trace_gettable('C:\Users\DaveJ\Desktop\TomG_Traces\SQL1_Monday.trc', default)
	--into #SQL1_Friday.trc
	where DatabaseName = 'AAT_SecureAssess'
	and ApplicationName = 'Microsoft SQL Server Management Studio - Query'


UNION ALL
--3--
select 
	  CAST(TextData as nvarchar(max)) TextData
	, SPID
	, Duration
	, StartTime
	, EndTime
	, Reads
	, Writes
	, CPU
	, EventClass
--into #SQL1Friday
from sys.fn_trace_gettable('C:\Users\DaveJ\Desktop\TomG_Traces\SQL1_Wednesday.trc', default)
	--into #SQL1_Friday.trc
	where DatabaseName = 'AAT_SecureAssess'
	and ApplicationName = 'Microsoft SQL Server Management Studio - Query'


UNION ALL
--4--
select 
	  CAST(TextData as nvarchar(max)) TextData
	, SPID
	, Duration
	, StartTime
	, EndTime
	, Reads
	, Writes
	, CPU
	, EventClass
--into #SQL1Friday
from sys.fn_trace_gettable('C:\Users\DaveJ\Desktop\TomG_Traces\SQL2_Friday.trc', default)
	--into #SQL1_Friday.trc
	where DatabaseName = 'AAT_SecureAssess'
	and ApplicationName = 'Microsoft SQL Server Management Studio - Query'

UNION ALL
--5--
select 
	  CAST(TextData as nvarchar(max)) TextData
	, SPID
	, Duration
	, StartTime
	, EndTime
	, Reads
	, Writes
	, CPU
	, EventClass
--into #SQL1Friday
from sys.fn_trace_gettable('C:\Users\DaveJ\Desktop\TomG_Traces\SQL2_Monday.trc', default)
	--into #SQL1_Friday.trc
	where DatabaseName = 'AAT_SecureAssess'
	and ApplicationName = 'Microsoft SQL Server Management Studio - Query'


UNION ALL
--6--
select 
	  CAST(TextData as nvarchar(max)) TextData
	, SPID
	, Duration
	, StartTime
	, EndTime
	, Reads
	, Writes
	, CPU
	, EventClass
--into #SQL1Friday
from sys.fn_trace_gettable('C:\Users\DaveJ\Desktop\TomG_Traces\SQL2_Tuesday.trc', default)
	--into #SQL1_Friday.trc
	where DatabaseName = 'AAT_SecureAssess'
	and ApplicationName = 'Microsoft SQL Server Management Studio - Query'

UNION ALL
--7--
select 
	  CAST(TextData as nvarchar(max)) TextData
	, SPID
	, Duration
	, StartTime
	, EndTime
	, Reads
	, Writes
	, CPU
	, EventClass
--into #SQL1Friday
from sys.fn_trace_gettable('C:\Users\DaveJ\Desktop\TomG_Traces\SQL2_Wednesday.trc', default)
	--into #SQL1_Friday.trc
	where DatabaseName = 'AAT_SecureAssess'
	and ApplicationName = 'Microsoft SQL Server Management Studio - Query'


) X


select * from #FULL_LIST 


/*

select distinct TextData
--, SPID 
from #SQL1Friday 


*/