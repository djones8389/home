USE SQA_CPProjectAdmin_DI

--Step #1 - Run AndyB script to write in the missing entries
--Need to set DelCol = 0 - Only for items that need it
--Need to set visible = 0 - Only for items that need it
--Need to set ItemGraphicTable.ali to be = to ProjectManifest.ID

--1 hr 8 mins to run...
	   
    Select 
		DISTINCT
		  A.Name
		 , A.ProjectID
		 , A.PageID
		 , A.ID
		 , A.Ali as 'ItemGraphic ref'
		 , B.ID as 'PMT ali ref'
	into #FROM_ItemGraphicTable
	FROM (
	SELECT
           PLT.Name
		   , SUBSTRING(IT.ID, 0, CHARINDEX('P', IT.ID)) as ProjectID
		   , SUBSTRING(IT.ID, CHARINDEX('P', IT.ID) + 1, CHARINDEX('S',IT.ID) - CHARINDEX('P', IT.ID) - 2 + Len('S')) as PageID
		   , IT.ID
		   , IT.ALI
      from ItemGraphicTable it 
            INNER JOIN ComponentTable c ON c.ID = it.ParentID 
            INNER JOIN SceneTable s ON s.ID = c.ParentID 
            INNER JOIN PageTable i ON i.id = S.ParentID
			INNER JOIN ProjectListTable PLT on PLT.ID = I.ParentID
	   where ali not like '%[0-9]%' and ali != '' and ali != 'placeholder'

	   )  A

	INNER JOIN		--Find where the Ali is in the ProjectManifestTable, and obtain the ID reference
	(
		select  ID
				, ProjectID
				, SUBSTRING(Name,0,CHARINDEX('.', Name)) as Name
				, sharedLib
		from ProjectManifestTable 
		where name not like '%.fla'
				and SharedLib = 1
				--and DelCol = 1
	)   B

On A.ProjectID = B.ProjectId	
	and A.ali = B.name

	INNER JOIN			--Has to exist in SharedLibrary AND ProjectManifestTable
	(
		SELECT X.ProjectID
			  ,sl.i.value('@id', 'int') ItemID
			  ,sl.i.value('@name', 'nvarchar(max)') ItemName
		FROM (
			SELECT  SharedLibraryTable.ProjectID		
				,  CAST(SharedLibraryTable.structureXML AS XML) AS [StructureXML]
			FROM dbo.SharedLibraryTable
			) X
		CROSS APPLY X.StructureXML.nodes('//item') sl(i)
	)  X 

ON B.ProjectId = X.ProjectID
	AND B.ID = X.ItemID;




update ProjectManifestTable
set DelCol = 0
From ProjectManifestTable a

inner join #FROM_ItemGraphicTable b
on b.projectid = a.ProjectId
	and a.id = b.[PMT ali ref];




DROP TABLE #FROM_ItemGraphicTable







select *
from ProjectManifestTable
where ProjectID = 410 and id = 55


select cast(Structurexml as xml), Structurexml
from SharedLibraryTable
where ProjectID = 888 

/*
SELECT 
		top 100 *
FROM
(
      Select  IT.ID, IT.ALI, SUBSTRING(
	  --into #FROM_ItemGraphicTable
      from ItemGraphicTable it 
            INNER JOIN ComponentTable c ON c.ID = it.ParentID 
            INNER JOIN SceneTable s ON s.ID = c.ParentID 
            INNER JOIN PageTable i ON i.id = S.ParentID
	   where ali not like '%[0-9]%' and ali != '' and ali != 'placeholder'




      UNION
      Select i.id, i.ver, i.moD, it.aLI as 'FROM ItemVideoTable'  
      from ItemVideoTable it 
            INNER JOIN ComponentTable c ON c.ID = it.ParentID 
            INNER JOIN SceneTable s ON s.ID = c.ParentID 
            INNER JOIN PageTable i ON i.id = S.ParentID
      UNION
      Select i.id, i.ver, i.moD, it.aLI as 'FROM ItemHotSpotTable'  
            from ItemHotSpotTable it 
            INNER JOIN ComponentTable c ON c.ID = it.ParentID 
            INNER JOIN SceneTable s ON s.ID = c.ParentID 
            INNER JOIN PageTable i ON i.id = S.ParentID
      UNION
      Select i.id, i.ver, i.moD, it.aLI as 'FROM ItemCustomQuestionTable'   
      from ItemCustomQuestionTable it 
            INNER JOIN ComponentTable c ON c.ID = it.ParentID 
            INNER JOIN SceneTable s ON s.ID = c.ParentID 
            INNER JOIN PageTable i ON i.id = S.ParentID
) AS Items
INNER JOIN PageTable as PT
        on PT.ID = Items.ID and PT.ver = Items.ver   

*/


select *
from ProjectManifestTable
where ProjectID = 888 and name like '%dartboard_darts%'


select cast(Structurexml as xml), Structurexml
from SharedLibraryTable
where ProjectID = 888 

--update ProjectManifestTable
--set delCol = 0
--where ProjectID = 888 and ID = 8

--update SharedLibraryTable
--set Structurexml ='<sharedLibrary><item id="5" name="caloriesburned_table.fla" visible="0" type="fla" source="" /><item id="6" name="caloriesburned_table.swf" visible="1" type="swf" source="5" /><item id="7" name="dartboard_darts.fla" visible="0" type="fla" source="" /><item id="8" name="dartboard_darts.swf" visible="1" type="swf" source="7" /><item id="9" name="nutrition_table.fla" visible="0" type="fla" source="" /><item id="15" name="1558016_business_hours_sign.jpg" visible="1" type="jpg" source="" /><item id="29" name="istock_000003457130_man_counting_xsmall.jpg" visible="1" type="jpg" source="" /><item id="30" name="6590293_clock_five_to_tweleve.jpg" visible="1" type="jpg" source="" /><item id="33" name="eleven_fifteen.jpg" visible="1" type="jpg" source="" /><item id="35" name="7814909_baby_toes.jpg" visible="1" type="jpg" source="" /><item id="67" name="istock_000004701496morfcalculatorxsmall.jpg" visible="1" type="jpg" source="" /><item id="70" name="6922169_motorway.jpg" visible="1" type="jpg" source="" /><item id="93" name="table_2_x_9.fla" visible="0" type="fla" source="" /><item id="94" name="table_2_x_9.swf" visible="1" type="swf" source="93" /><item id="95" name="table_2_x_10.fla" visible="0" type="fla" source="" /><item id="96" name="table_2_x_10.swf" visible="1" type="swf" source="95" /><item id="97" name="table_3option.fla" visible="0" type="fla" source="" /><item id="98" name="table_3option.swf" visible="1" type="swf" source="97" /><item id="99" name="table_4option.fla" visible="0" type="fla" source="" /><item id="100" name="table_4option.swf" visible="1" type="swf" source="99" /><item id="101" name="hint.fla" visible="0" type="fla" source="" /><item id="102" name="hint.swf" visible="1" type="swf" source="101" /><item id="103" name="ni_information.fla" visible="0" type="fla" source="" /><item id="104" name="ni_information.swf" visible="1" type="swf" source="103" /><item id="105" name="return.fla" visible="0" type="fla" source="" /><item id="106" name="return.swf" visible="1" type="swf" source="105" /><item id="107" name="nmw_rates.fla" visible="0" type="fla" source="" /><item id="108" name="nmw_rates.swf" visible="1" type="swf" source="107" /><item id="109" name="cb_inform.fla" visible="0" type="fla" source="" /><item id="110" name="cb_inform.swf" visible="1" type="swf" source="109" /><item id="111" name="itemcosttable.fla" visible="0" type="fla" source="" /><item id="112" name="itemcosttable.swf" visible="1" type="swf" source="111" /><item id="113" name="cal_newbtnold.fla" visible="0" type="fla" source="" /><item id="114" name="cal_newbtnold.swf" visible="1" type="swf" source="113" /><item id="115" name="nnpv11feed.fla" visible="0" type="fla" source="" /><item id="116" name="nnpv11feed.swf" visible="1" type="swf" source="115" /><item id="117" name="nnpv7feed.fla" visible="0" type="fla" source="" /><item id="118" name="nnpv7feed.swf" visible="1" type="swf" source="117" /><item id="119" name="1over4.fla" visible="0" type="fla" source="" /><item id="120" name="1over4.swf" visible="1" type="swf" source="119" /><item id="121" name="4over10.fla" visible="0" type="fla" source="" /><item id="122" name="4over10.swf" visible="1" type="swf" source="121" /><item id="123" name="14over50.fla" visible="0" type="fla" source="" /><item id="124" name="14over50.swf" visible="1" type="swf" source="123" /><item id="125" name="7over50.fla" visible="0" type="fla" source="" /><item id="126" name="7over50.swf" visible="1" type="swf" source="125" /><item id="127" name="14over20.fla" visible="0" type="fla" source="" /><item id="128" name="14over20.swf" visible="1" type="swf" source="127" /><item id="129" name="materialstable.fla" visible="0" type="fla" source="" /><item id="130" name="materialstable.swf" visible="1" type="swf" source="129" /><item id="131" name="ad54mrqtable.fla" visible="0" type="fla" source="" /><item id="132" name="ad54mrqtable.swf" visible="1" type="swf" source="131" /><item id="133" name="ad34mrqtable.fla" visible="0" type="fla" source="" /><item id="134" name="ad34mrqtable.swf" visible="1" type="swf" source="133" /><item id="135" name="ad14mrqtable.fla" visible="0" type="fla" source="" /><item id="136" name="ad14mrqtable.swf" visible="1" type="swf" source="135" /><item id="137" name="nnpv9table.fla" visible="0" type="fla" source="" /><item id="138" name="nnpv9table.swf" visible="1" type="swf" source="137" /><item id="139" name="asq14mcqtable.fla" visible="0" type="fla" source="" /><item id="140" name="asq14mcqtable.swf" visible="1" type="swf" source="139" /><item id="141" name="asq12table4.fla" visible="0" type="fla" source="" /><item id="142" name="asq12table4.swf" visible="1" type="swf" source="141" /><item id="143" name="asq12table3.fla" visible="0" type="fla" source="" /><item id="144" name="asq12table3.swf" visible="1" type="swf" source="143" /><item id="145" name="asq12table2.fla" visible="0" type="fla" source="" /><item id="146" name="asq12table2.swf" visible="1" type="swf" source="145" /><item id="147" name="asq12table1.fla" visible="0" type="fla" source="" /><item id="148" name="asq12table1.swf" visible="1" type="swf" source="147" /><item id="149" name="jb2honeybutton.fla" visible="0" type="fla" source="" /><item id="150" name="jb2honeybutton.swf" visible="1" type="swf" source="149" /><item id="151" name="tablebutton.fla" visible="0" type="fla" source="" /><item id="152" name="tablebutton.swf" visible="1" type="swf" source="151" /><item id="153" name="jh224mrqtable.fla" visible="0" type="fla" source="" /><item id="155" name="ad2dndtable.fla" visible="0" type="fla" source="" /><item id="156" name="ad2dndtable.swf" visible="1" type="swf" source="155" /><item id="157" name="asq35table.fla" visible="0" type="fla" source="" /><item id="158" name="asq35table.swf" visible="1" type="swf" source="157" /><item id="159" name="asq41table.fla" visible="0" type="fla" source="" /><item id="160" name="asq41table.swf" visible="1" type="swf" source="159" /><item id="161" name="aveprob5table.fla" visible="0" type="fla" source="" /><item id="163" name="aveprob4table.fla" visible="0" type="fla" source="" /><item id="164" name="aveprob4table.swf" visible="1" type="swf" source="163" /><item id="165" name="aveprob1table.fla" visible="0" type="fla" source="" /><item id="166" name="aveprob1table.swf" visible="1" type="swf" source="165" /><item id="167" name="eatwell_plate.jpg" visible="1" type="jpg" source="" /><item id="168" name="jlg12_brown.fla" visible="0" type="fla" source="" /><item id="169" name="jlg12_brown.swf" visible="1" type="swf" source="168" /><item id="170" name="jlg12_smith.fla" visible="0" type="fla" source="" /><item id="171" name="jlg12_smith.swf" visible="1" type="swf" source="170" /><item id="172" name="jlg12_thompson.fla" visible="0" type="fla" source="" /><item id="173" name="jlg12_thompson.swf" visible="1" type="swf" source="172" /><item id="174" name="calculator.fla" visible="0" type="fla" source="" /><item id="175" name="calculator.swf" visible="1" type="swf" source="174" /><item id="176" name="jlq12link3.fla" visible="0" type="fla" source="" /><item id="177" name="jlq12link3.swf" visible="1" type="swf" source="176" /><item id="178" name="jlq12link2.fla" visible="0" type="fla" source="" /><item id="179" name="jlq12link2.swf" visible="1" type="swf" source="178" /><item id="180" name="jlq12link1.fla" visible="0" type="fla" source="" /><item id="181" name="jlq12link1.swf" visible="1" type="swf" source="180" /><updateHistory><item original="caloriesburned_table" Type="fla" id="5" /><item original="caloriesburned_table" Type="swf" id="6" /><item original="dartboard_darts" Type="fla" id="7" /><item original="nutrition_table" Type="fla" id="9" /><item original="1558016_business_hours_sign" Type="jpg" id="15" /><item original="istock_000003457130_man_counting_xsmall" Type="jpg" id="29" /><item original="6590293_clock_five_to_tweleve" Type="jpg" id="30" /><item original="eleven_fifteen" Type="jpg" id="33" /><item original="7814909_baby_toes" Type="jpg" id="35" /><item original="istock_000004701496morfcalculatorxsmall" Type="jpg" id="67" /><item original="6922169_motorway" Type="jpg" id="70" /><item original="table_2_x_9" Type="fla" id="93" /><item original="table_2_x_9" Type="swf" id="94" /><item original="table_2_x_10" Type="fla" id="95" /><item original="table_2_x_10" Type="swf" id="96" /><item original="table_3option" Type="fla" id="97" /><item original="table_3option" Type="swf" id="98" /><item original="table_4option" Type="fla" id="99" /><item original="table_4option" Type="swf" id="100" /><item original="hint" Type="fla" id="101" /><item original="hint" Type="swf" id="102" /><item original="ni_information" Type="fla" id="103" /><item original="ni_information" Type="swf" id="104" /><item original="return" Type="fla" id="105" /><item original="return" Type="swf" id="106" /><item original="nmw_rates" Type="fla" id="107" /><item original="nmw_rates" Type="swf" id="108" /><item original="cb_inform" Type="fla" id="109" /><item original="cb_inform" Type="swf" id="110" /><item original="itemcosttable" Type="fla" id="111" /><item original="itemcosttable" Type="swf" id="112" /><item original="cal_newbtnold" Type="fla" id="113" /><item original="cal_newbtnold" Type="swf" id="114" /><item original="nnpv11feed" Type="fla" id="115" /><item original="nnpv11feed" Type="swf" id="116" /><item original="nnpv7feed" Type="fla" id="117" /><item original="nnpv7feed" Type="swf" id="118" /><item original="1over4" Type="fla" id="119" /><item original="1over4" Type="swf" id="120" /><item original="4over10" Type="fla" id="121" /><item original="4over10" Type="swf" id="122" /><item original="14over50" Type="fla" id="123" /><item original="14over50" Type="swf" id="124" /><item original="7over50" Type="fla" id="125" /><item original="7over50" Type="swf" id="126" /><item original="14over20" Type="fla" id="127" /><item original="14over20" Type="swf" id="128" /><item original="materialstable" Type="fla" id="129" /><item original="materialstable" Type="swf" id="130" /><item original="ad54mrqtable" Type="fla" id="131" /><item original="ad54mrqtable" Type="swf" id="132" /><item original="ad34mrqtable" Type="fla" id="133" /><item original="ad34mrqtable" Type="swf" id="134" /><item original="ad14mrqtable" Type="fla" id="135" /><item original="ad14mrqtable" Type="swf" id="136" /><item original="nnpv9table" Type="fla" id="137" /><item original="nnpv9table" Type="swf" id="138" /><item original="asq14mcqtable" Type="fla" id="139" /><item original="asq14mcqtable" Type="swf" id="140" /><item original="asq12table4" Type="fla" id="141" /><item original="asq12table4" Type="swf" id="142" /><item original="asq12table3" Type="fla" id="143" /><item original="asq12table3" Type="swf" id="144" /><item original="asq12table2" Type="fla" id="145" /><item original="asq12table2" Type="swf" id="146" /><item original="asq12table1" Type="fla" id="147" /><item original="asq12table1" Type="swf" id="148" /><item original="jb2honeybutton" Type="fla" id="149" /><item original="jb2honeybutton" Type="swf" id="150" /><item original="tablebutton" Type="fla" id="151" /><item original="tablebutton" Type="swf" id="152" /><item original="jh224mrqtable" Type="fla" id="153" /><item original="jh224mrqtable" Type="swf" id="154" /><item original="ad2dndtable" Type="fla" id="155" /><item original="ad2dndtable" Type="swf" id="156" /><item original="asq35table" Type="fla" id="157" /><item original="asq35table" Type="swf" id="158" /><item original="asq41table" Type="fla" id="159" /><item original="asq41table" Type="swf" id="160" /><item original="aveprob5table" Type="fla" id="161" /><item original="aveprob4table" Type="fla" id="163" /><item original="aveprob4table" Type="swf" id="164" /><item original="aveprob1table" Type="fla" id="165" /><item original="aveprob1table" Type="swf" id="166" /><item original="eatwell_plate" Type="jpg" id="167" /><item original="jlg12_brown" Type="fla" id="168" /><item original="jlg12_brown" Type="swf" id="169" /><item original="jlg12_smith" Type="fla" id="170" /><item original="jlg12_smith" Type="swf" id="171" /><item original="jlg12_thompson" Type="fla" id="172" /><item original="jlg12_thompson" Type="swf" id="173" /><item original="calculator" Type="fla" id="174" /><item original="calculator" Type="swf" id="175" /><item original="jlq12link3" Type="fla" id="176" /><item original="jlq12link3" Type="swf" id="177" /><item original="jlq12link2" Type="fla" id="178" /><item original="jlq12link2" Type="swf" id="179" /><item original="jlq12link1" Type="fla" id="180" /><item original="jlq12link1" Type="swf" id="181" /></updateHistory><fol name="EditedLJMay2013" id="1"><fol name="Jackie Howie" id="3"><item id="202" name="jh37_5mrq_tt_money_pounds.swf" visible="1" type="swf" source="201" /><item id="200" name="jh36_5mrq_tt_money.swf" visible="1" type="swf" source="" /><item id="199" name="jh394_mcq_tt_bank_building.swf" visible="1" type="swf" source="198" /><item id="197" name="jh40_4_mcq_tt_temperaturesicicles.swf" visible="1" type="swf" source="195" /><item id="195" name="jh41_reorder_tt_moneypig.swf" visible="1" type="swf" source="194" /><item id="193" name="jh16_5mrq_tt_womanshopping.swf" visible="1" type="swf" source="191" /><item id="191" name="jh21_5mrq_tt_paint.swf" visible="1" type="swf" source="190" /><item id="189" name="jh22_4mrq_tt_shoppinglist.swf" visible="1" type="swf" source="188" /><item id="187" name="jh84mcqtt_digitalclock.swf" visible="1" type="swf" source="185" /><item id="185" name="jh7_4mcq_tt_timesheet_blank.swf" visible="1" type="swf" source="184" /><item id="183" name="jh14mcqtt_smartcar.swf" visible="1" type="swf" source="182" /></fol><fol name="Alan Deans" id="4"><item id="204" name="ad14_4mrq_tt_small_people.swf" visible="1" type="swf" source="203" /><item id="206" name="ad15_4mrq_tt_house_plan.swf" visible="1" type="swf" source="205" /><item id="208" name="ad4_4mrq_tt_petrolpumpmoney.swf" visible="1" type="swf" source="207" /><item id="210" name="ad17_4mcq_tt_chef.swf" visible="1" type="swf" source="209" /></fol><fol name="Nadia" id="5"><item id="214" name="nnpv_10_gapfill_lottery.swf" visible="1" type="swf" source="213" /><item id="216" name="nne9_4mcq_tt_planeflightpath.swf" visible="1" type="swf" source="215" /><item id="218" name="nne9_4mcq_ttl_hollandbuildings.swf" visible="1" type="swf" source="217" /></fol><fol name="AngelaSmith" id="6"><item id="220" name="asc40_picklist_tt_measurement.swf" visible="1" type="swf" source="219" /><item id="222" name="asq29_gapfill_tt_mancalculator.swf" visible="1" type="swf" source="220" /><item id="224" name="asq11_matching_tt_timesheet.swf" visible="1" type="swf" source="222" /><item id="226" name="asq10_matching_tt_cheffemale.swf" visible="1" type="swf" source="225" /><item id="228" name="asq2_reorder_tt_salesman.swf" visible="1" type="swf" source="227" /><item id="230" name="asq13_4mcq_tt_mumkids.swf" visible="1" type="swf" source="229" /><item id="232" name="asq5_4mcq_tt_newspapers.swf" visible="1" type="swf" source="231" /><item id="234" name="asq1_4mcq_tt_timesheet.swf" visible="1" type="swf" source="232" /><item id="267" name="asc38_picklist_route2.swf" visible="1" type="swf" source="266" /><item id="269" name="asc38_picklist_route3.swf" visible="1" type="swf" source="268" /><item id="271" name="asc38_picklist_route1.swf" visible="1" type="swf" source="270" /><item id="273" name="asc38_picklist_route4.swf" visible="1" type="swf" source="272" /><item id="275" name="asq34_5mcq_tt_pastas.swf" visible="1" type="swf" source="273" /></fol><fol name="Jill Brown" id="7"><item id="238" name="jb9_gc_eatwellplate.swf" visible="1" type="swf" source="237" /><item id="240" name="aveprob9_tt_commuters.swf" visible="1" type="swf" source="239" /><item id="242" name="aveprob7_fruitjuggling.swf" visible="1" type="swf" source="241" /><item id="244" name="aveprob6_peoplelying.swf" visible="1" type="swf" source="243" /><item id="246" name="aveprob5_tt_tablecreditcard.swf" visible="1" type="swf" source="245" /><item id="248" name="aveprob2_tt_deliveryman.swf" visible="1" type="swf" source="247" /><item id="250" name="aveprob1_tt_students.swf" visible="1" type="swf" source="249" /><item id="263" name="aveprob10_tt_rain_cloud.swf" visible="1" type="swf" source="262" /></fol><fol name="Jill Little" id="8"><item id="257" name="jlq14_pizza.swf" visible="1" type="swf" source="256" /><item id="254" name="jlq15_sale_clearance.swf" visible="1" type="swf" source="253" /><item id="252" name="jlq16_sale.swf" visible="1" type="swf" source="250" /><item id="259" name="jlq11_clock.swf" visible="1" type="swf" source="258" /><item id="261" name="jlq12_timesheet.swf" visible="1" type="swf" source="260" /><item id="265" name="jlq18_table_fishfingers.swf" visible="1" type="swf" source="264" /></fol></fol><fol name="old images" id="2"><item id="154" name="jh224mrqtable.swf" visible="1" type="swf" source="153" /></fol></sharedLibrary>'
--where ProjectID = 888 