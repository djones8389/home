SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

IF OBJECT_ID('tempdb..#StateChanges') IS NOT NULL DROP TABLE #StateChanges;

DECLARE @StateToSearch	nvarchar(MAX) = '5,14'		-- tinyint = 5;
DECLARE @MinStateChangeDate datetime = '01 May 2015';
DECLARE @MaxStateChangeDate datetime = '15 May 2015';
DECLARE @ExecLiveAndWHAudit nvarchar(MAX) = ''
DECLARE @ESIDsFromCTE nvarchar(MAX) = ''
DECLARE @sql NVARCHAR(MAX) = ''

--DECLARE @CTE  TABLE
--(
--	rowNumber tinyint
--	, ExamSessionID int
--	, NewState tinyint
--	, StateChangeDate datetime	
--	, NextStateChangeDate datetime	
--)

DECLARE @MyESIDs TABLE
(
	ESID INT
)

CREATE TABLE #StateChanges 
(
	rowNumber tinyint
	, ExamSessionID int
	, NewState tinyint
	, StateChangeDate datetime	
)


SELECT @ExecLiveAndWHAudit += CHAR(13) + 

'

SELECT

	ROW_NUMBER() OVER (PARTITION BY ExamSessionID
						ORDER BY ExamSessionID, NewState, StateChangeDate) as N
	, ExamSessionID
	, NewState
	, StateChangeDate
FROM 
	(	

SELECT 
	DISTINCT --<< Stops duplicates with same time
                ExamSessionID,
                ExamStateChangeXML.StateChange.value(''(newStateID)[1]'', ''int'') NewState,
                ExamStateChangeXML.StateChange.value(''(changeDate)[1]'', ''datetime'') StateChangeDate
FROM dbo.WAREHOUSE_ExamSessionTable WITH(NOLOCK)
CROSS APPLY ExamStateChangeAuditXml.nodes(''exam/stateChange'') ExamStateChangeXML(StateChange)

where  completionDate >  ''30 April 2015'' 
	and ( 
		   cast(ExamStateChangeAuditXml as nvarchar(MAX)) like ''%<newStateID>[' +  @StateToSearch +']</newStateID>%''
				
		)

) as X


UNION


SELECT ROW_NUMBER() OVER(
            PARTITION BY ExamStateChangeAuditTable.ExamSessionID
            ORDER BY ExamStateChangeAuditTable.ExamSessionID
                  ,ExamStateChangeAuditTable.StateChangeDate
      ) AS [N]
      ,ExamStateChangeAuditTable.ExamSessionID
      ,ExamStateChangeAuditTable.NewState
      ,ExamStateChangeAuditTable.StateChangeDate
FROM dbo.ExamStateChangeAuditTable
WHERE ExamStateChangeAuditTable.ExamSessionID IN (
      SELECT ExamStateChangeAuditTable.ExamSessionID
      FROM dbo.ExamStateChangeAuditTable
      WHERE YEAR(ExamStateChangeAuditTable.StateChangeDate) = 2015
      AND MONTH(ExamStateChangeAuditTable.StateChangeDate) = 5
      AND ExamStateChangeAuditTable.NewState in (' + @StateToSearch + ')
)
'

INSERT INTO #StateChanges
EXEC(@ExecLiveAndWHAudit)






SET @sql =
'

DECLARE @CTE  TABLE
(
	rowNumber tinyint
	, ExamSessionID int
	, NewState tinyint
	, StateChangeDate datetime	
	, NextStateChangeDate datetime	
)


;WITH CTE AS (
      SELECT #StateChanges.rowNumber
			, #StateChanges.ExamSessionID
			, #StateChanges.NewState
			, #StateChanges.StateChangeDate
            , CAST(NULL AS DATETIME) AS [NextStateChangeDate]
      FROM #StateChanges
      
      UNION ALL
      
      SELECT [StateChange].rowNumber
			, [StateChange].ExamSessionID
			, [StateChange].NewState
			, [StateChange].StateChangeDate
			, CTE.StateChangeDate
      FROM #StateChanges AS [StateChange]
      
      INNER JOIN CTE 
      ON  CTE.ExamSessionID = StateChange.ExamSessionID
		AND StateChange.rowNumber + 1 = CTE.rowNumber

)
	SELECT DISTINCT ExamSessionID
	FROM @CTE C
	WHERE C.NewState IN (' + @StateToSearch + ')
	AND NextStateChangeDate IS NOT NULL
	AND N''2015-05-31'' BETWEEN StateChangeDate AND NextStateChangeDate;

'
SELECT @SQL
--INSERT @CTE
EXEC sp_executesql @sql

SELECT * FROM @CTE


--SELECT @ESIDsFromCTE +=CHAR(13) + '

--SELECT DISTINCT ExamSessionID
--FROM @CTE C
--WHERE C.NewState IN (' + @StateToSearch + ')
--AND NextStateChangeDate IS NOT NULL
--AND N''2015-05-31'' BETWEEN StateChangeDate AND NextStateChangeDate;
--'

--PRINT (@ESIDsFromCTE)

INSERT @MyESIDs(ESID)
EXEC (@ESIDsFromCTE)

SELECT  ExamSessionTable.ID
		, ExamSessionTable.KeyCode
		, UserTable.CandidateRef
		, UserTable.Forename
		, UserTable.Surname
		, CentreTable.CentreName
		, IB3QualificationLookup.QualificationName
		, ScheduledExamsTable.examName
		, examVersionRef
		, NewState
		, StateChangeDate
  FROM ExamSessionTable

	INNER JOIN ScheduledExamsTable ON ScheduledExamsTable.ID = ExamSessionTable.ScheduledExamID
	INNER JOIN UserTable ON UserTable.ID = ExamSessionTable.UserID
	INNER JOIN CentreTable ON CentreTable.ID = ScheduledExamsTable.CentreID
	INNER JOIN IB3QualificationLookup ON IB3QualificationLookup.ID = ScheduledExamsTable.qualificationId
	INNER JOIN #StateChanges on #StateChanges.ExamSessionID = ExamSessionTable.ID
	
WHERE ExamSessionTable.ID IN (SELECT ESID From @MyESIDs)
	And StateChangeDate > @MinStateChangeDate
	And StateChangeDate < @MaxStateChangeDate
	--AND EXISTS (
	--	select *
	--	from #StateChanges
	--	where #StateChanges.NewState = @StateToSearch
	--)
	
UNION

SELECT WAREHOUSE_ExamSessionTable.ExamSessionID
		, WAREHOUSE_ExamSessionTable.KeyCode
		, WAREHOUSE_UserTable.CandidateRef
		, WAREHOUSE_UserTable.Forename
		, WAREHOUSE_UserTable.Surname
		, WAREHOUSE_CentreTable.CentreName
		, WAREHOUSE_ExamSessionTable_Shreded.qualificationName
		, WAREHOUSE_ScheduledExamsTable.examName
		, WAREHOUSE_ExamSessionTable_Shreded.ExternalReference
		, NewState
		, StateChangeDate	
  FROM WAREHOUSE_ExamSessionTable

INNER JOIN WAREHOUSE_ScheduledExamsTable ON WAREHOUSE_ScheduledExamsTable.ID = WAREHOUSE_ExamSessionTable.WAREHOUSEScheduledExamID
INNER JOIN WAREHOUSE_UserTable ON WAREHOUSE_UserTable.ID = WAREHOUSE_ExamSessionTable.WAREHOUSEUserID
INNER JOIN WAREHOUSE_CentreTable  ON WAREHOUSE_CentreTable.ID = WAREHOUSE_ScheduledExamsTable.WAREHOUSECentreID
INNER JOIN WAREHOUSE_ExamSessionTable_Shreded ON WAREHOUSE_ExamSessionTable_Shreded.examSessionId = WAREHOUSE_ExamSessionTable.ID
INNER JOIN #StateChanges on #StateChanges.ExamSessionID = WAREHOUSE_ExamSessionTable.ExamSessionID
	
WHERE WAREHOUSE_ExamSessionTable.ExamSessionID IN (SELECT ESID From @MyESIDs)	
	And StateChangeDate > @MinStateChangeDate
	And StateChangeDate < @MaxStateChangeDate
	--AND EXISTS (
	--	select *
	--	from #StateChanges
	--	where #StateChanges.NewState = @StateToSearch
	--)
	
	
DROP TABLE #StateChanges;