USE [BIS_SecureAssess_11.0]

select examsessionid
	--, userMark
	--, userPercentage
	--, resultData
	--, resultData.value('(@userMark)','tinyint') as total
	--, SUM(a.b.value('(@actualUserMark)','tinyint')) as totalActual
	--, SUM(a.b.value('(@userMark)','decimal(6,3)')) as totalUser
	,a.b.value('(@userMark)','decimal(6,3)')
	,a.b.value('(@totalMark)','decimal(6,3)')
from WAREHOUSE_ExamSessionTable_Shreded (NOLOCK)
cross apply resultdata.nodes('/exam//section') a(b)
where examSessionID IN (9125,9122,9121,9119,9117,9116,9114,9113,9112,9111,9110,9109,9108,9103,9098,9097,9091,9088,9087,9086,9085,9084,9078,9077,9076,9070,9069,9064,9063,9062,9060,9059,9057,9053,9052,9051,9050,9049,9048,9047,8974,8583,8561,8499,8484,8446,8445,8444,8443,8442,8440,8439,8438,8437,8436,8434,8431,8430,8408,8407,8406,8405,8404,8403,8402,8401,8400,8388,8359,8358,8201,8200,8199,8197,8196,8195,8194,8193,8192,8191,8190,8189,8188,8139,8122,8119,8117,8116,8109,8108,8107,8106,8061,8060,7995,7984,7925,7924,7916,7914,7885,7840,7593,7382,7380,7266,7258,6539,6536,6535,6534,6533,6532,6529,6528,6026,6025,5698,5532,5449,4014,4013,3778,3771,3766,3729)
	--AND a.b.value('(@userMark)','decimal(6,3)') > a.b.value('(@totalMark)','decimal(6,3)')
   --GROUP BY examSessionID
   

select examSessionId, resultData
from WAREHOUSE_ExamSessionTable_Shreded   
where examSessionID IN (3771,8122,8403,8407,8444)







select top 100 examSessionId, resultData
from WAREHOUSE_ExamSessionTable_Shreded   
order by 1 desc



SELECT  examSessionID
	--,a.b.value('@actualUserMark','decimal(6,3)') as userMark
	--, a.b.value('@totalMark','decimal(6,3)') as totalMark
	, resultData
	, userMark
	, userPercentage
from WAREHOUSE_ExamSessionTable_Shreded (NOLOCK)
--cross apply resultdata.nodes('//item') a(b)  
where examSessionID IN (9125,9122,9121,9119,9117,9116,9114,9113,9112,9111,9110,9109,9108,9103,9098,9097,9091,9088,9087,9086,9085,9084,9078,9077,9076,9070,9069,9064,9063,9062,9060,9059,9057,9053,9052,9051,9050,9049,9048,9047,8974,8583,8561,8499,8484,8446,8445,8444,8443,8442,8440,8439,8438,8437,8436,8434,8431,8430,8408,8407,8406,8405,8404,8403,8402,8401,8400,8388,8359,8358,8201,8200,8199,8197,8196,8195,8194,8193,8192,8191,8190,8189,8188,8139,8122,8119,8117,8116,8109,8108,8107,8106,8061,8060,7995,7984,7925,7924,7916,7914,7885,7840,7593,7382,7380,7266,7258,6539,6536,6535,6534,6533,6532,6529,6528,6026,6025,5698,5532,5449,4014,4013,3778,3771,3766,3729)	
	and a.b.value('@actualUserMark','decimal(6,3)') > a.b.value('@totalMark','decimal(6,3)')