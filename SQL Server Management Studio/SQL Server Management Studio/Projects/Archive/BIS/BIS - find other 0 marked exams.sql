DECLARE @Temp TABLE (
	ESID INT
	, TotalMark DECIMAL(6,3)
)

INSERT @Temp
SELECT 
	WAREHOUSEExamSessionID
,   SUM(MarkerResponseData.value('(entries/entry[last()]/assignedMark)[1]','decimal(6,3)')) as MyMark
FROM WAREHOUSE_ExamSessionItemResponseTable (NOLOCK)
	group by WAREHOUSEExamSessionID
	
SELECT examSessionId, candidateRef, userMark, userPercentage, qualificationName, examName, KeyCode, warehouseTime
from WAREHOUSE_ExamSessionTable_Shreded WESTS (NOLOCK)
INNER JOIN @Temp T
On T.ESID = WESTS.examSessionId
where userMark = 0
	AND TotalMark > 0
ORDER BY warehouseTime DESC		
	
	
/*1 5 6 */
	