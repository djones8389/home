RESTORE DATABASE [BIS_SecureAssess_11.0_restore] FROM DISK = N'T:\Backup\BIS_SecureAssess_11.02015.10.13.bak' 
	WITH FILE = 1, NOUNLOAD, NOREWIND, REPLACE, STATS = 10
	, MOVE 'BIS_SecureAssess_11.0' TO N'S:\DATA\BIS_SecureAssess_11.0_restore.mdf'
	, MOVE 'BIS_SecureAssess_11.0_log' TO N'L:\LOGS\BIS_SecureAssess_11.0_restore.ldf';