USE SQA_CPProjectAdmin	
	
SET NOCOUNT ON;

CREATE TABLE #BadMarkingType (ProjectID int, PageID int, MarkingType nvarchar(100), OriginalPageXML XML, NewPageXML XML, NewMarkingType INT);
INSERT INTO #BadMarkingType
	SELECT [ID]
		,Project.Page.value('@ID', 'int')
		,Project.Page.value('@MarkingType', 'nvarchar(100)')
		,Project.Page.query('.')
		,Project.Page.query('.')
		,0
	FROM [ProjectListTable]
	CROSS APPLY ProjectStructureXml.nodes('//Pag') Project(Page)
	WHERE Project.Page.value('@MarkingType', 'nvarchar(100)') LIKE N'%[a-zA-Z]%';

select * from #BadMarkingType

DECLARE @MarkingTypes TABLE (ProjectID int, MarkingType nvarchar(100), MarkingTypeValue int);

INSERT INTO @MarkingTypes
	SELECT DISTINCT
		 ProjectID
		,Def.MarkingType.value('@display', 'nvarchar(100)')
		,Def.MarkingType.value('.', 'int')
	FROM #BadMarkingType
	INNER JOIN ProjectListTable
	ON #BadMarkingType.ProjectID = ProjectListTable.ID
	CROSS APPLY ProjectDefaultXml.nodes('/Defaults/StructureAttributes/Attribute[@Name = "MarkingType"]/Value') Def(MarkingType);

UPDATE #BadMarkingType
SET NewMarkingType = MarkingType.MarkingTypeValue
FROM #BadMarkingType
LEFT JOIN @MarkingTypes MarkingType
ON #BadMarkingType.ProjectID = MarkingType.ProjectID
WHERE (#BadMarkingType.MarkingType = MarkingType.MarkingType COLLATE Latin1_General_CI_AS
	OR #BadMarkingType.MarkingType = MarkingType.MarkingType+N'ed' COLLATE Latin1_General_CI_AS)
AND MarkingType.MarkingType IS NOT NULL;

UPDATE #BadMarkingType
SET NewPageXML.modify('replace value of (/Pag/@MarkingType)[1] with sql:column("NewMarkingType")');

SELECT ID AS ProjectID
	,CAST(ProjectStructureXML AS NVARCHAR(MAX)) ProjectStructureXML
INTO #ProjectStructure
FROM ProjectListTable
WHERE ID IN (SELECT ProjectID FROM #BadMarkingType);

/*SELECT * FROM #BadMarkingType
	inner join ProjectListTable as plt
	on plt.ID = #BadMarkingType.ProjectID
order by #BadMarkingType.ProjectID */
/*RUN TO HERE*/

DECLARE PageReplacement CURSOR FOR
	SELECT ProjectID, CAST(OriginalPageXML AS NVARCHAR(MAX)), CAST(NewPageXML AS NVARCHAR(MAX)) FROM #BadMarkingType;

OPEN PageReplacement;

DECLARE @ProjectID INT, @OriginalPageXML NVARCHAR(MAX), @NewPageXML NVARCHAR(MAX);

FETCH NEXT FROM PageReplacement INTO @ProjectID, @OriginalPageXML, @NewPageXML;

WHILE @@FETCH_STATUS = 0
BEGIN
	UPDATE #ProjectStructure
	SET ProjectStructureXML = REPLACE(ProjectStructureXML, @OriginalPageXML, @NewPageXML)
	WHERE ProjectID = @ProjectID;
	
	FETCH NEXT FROM PageReplacement INTO @ProjectID, @OriginalPageXML, @NewPageXML;
END

CLOSE PageReplacement;
DEALLOCATE PageReplacement;

SELECT ProjectID AS ID, 
CAST(ProjectStructureXML AS XML) AS ProjectStructureXML 
FROM #ProjectStructure;



update projectlisttable
set ProjectStructureXML = PS.ProjectStructureXML 
from projectlisttable

	inner join #ProjectStructure as PS
	on PS.ProjectID = projectlisttable.ID
	
where PS.ProjectID = projectlisttable.ID


DROP TABLE #BadMarkingType;
DROP TABLE #ProjectStructure;