BEGIN TRAN

CREATE TABLE #StructureXmlFolder(ExamSessionId int, myXml xml)
INSERT INTO #StructureXmlFolder
SELECT  ID, StructureXML
  FROM ExamSessionTable	where examState in (3,4)

SELECT  #StructureXmlFolder.ExamSessionId,
c.p.value('@id', 'nvarchar(20)') '@id',
c.p.value('@markingType', 'nvarchar(20)') '@markingType',
c.p.value('@totalMark', 'int') '@totalMark'
FROM
    #StructureXmlFolder
    CROSS APPLY myXml.nodes('//item') c(p)
WHERE  --ExamSessionTable.examState in (3,4)
  c.p.value('@markingType', 'nvarchar(20)') like '%[a-z]%'

	DROP TABLE	#StructureXmlFolder


ROLLBACK
--31181--
--8218-

sp_lock