SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

select examSessionId
	--, resultData.query('exam/gradeBoundaryData/gradeBoundaries/grade[last()]') as GradeFromBoundaries
	, a.b.value('@value','int') as PassValue
	, a.b.value('@description','nvarchar(20)') as Description
	, resultData
	, usermark
	, grade
	, reMarkStatus
	, KeyCode
	from WAREHOUSE_ExamSessionTable_Shreded with(NOLOCK)
	
	cross apply resultData.nodes('exam/gradeBoundaryData[last()]/gradeBoundaries[last()]/grade[last()]') a(b)
	
	where --resultData.exist('exam/section/item/mark[@mark]') = 1
		--and reMarkStatus != 0
		 a.b.value('@description','nvarchar(20)') = 'Pass'
		and a.b.value('@value','nvarchar(20)') <= userMark
		and grade != 'Pass' and grade != 'Voided'
		 --examSessionID = 82186
		 
		 USE UAT_OCR_SecureAssess