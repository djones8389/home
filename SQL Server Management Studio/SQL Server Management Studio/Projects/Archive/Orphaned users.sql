CREATE PROCEDURE ##GenPass(@Len INT = 32, @Password NVARCHAR(MAX) OUTPUT)
AS
BEGIN
SET @Password = (
SELECT XChar AS [text()]
FROM  (
              SELECT TOP(@Len) XChar
              FROM  (
                     VALUES  ('A'),('B'),('C'),('D'),('E'),('F'),('G'),('H'),('I'),('J'),('K'),('L'),('M'),('N'),('O'),('P'),('Q'),('R'),('S'),('T'),('U'),('V'),('W'),('X'),('Y'),('Z'),
                                  ('a'),('b'),('c'),('d'),('e'),('f'),('g'),('h'),('i'),('j'),('k'),('l'),('m'),('n'),('o'),('p'),('q'),('r'),('s'),('t'),('u'),('v'),('w'),('x'),('y'),('z'),
                                  ('0'),('1'),('2'),('3'),('4'),('5'),('6'),('7'),('8'),('9'),
                                  ('!'),('�'),('$'),('%'),('^'),('&'),('*'),('('),(')'),('-'),('_'),('='),('+'),('@'),('~'),('#'),('?')
                           ) AS XChars(XChar)
              ORDER BY ABS(CHECKSUM(NEWID()))
              ) AS [XChars]
FOR XML PATH('')
)
END;
GO

create table ##DaveJTempTable  (LoginName nvarchar(100), ID nvarchar(max), password nvarchar(max));

EXEC sp_MSForEachDB '
USE [?];

insert into ##DaveJTempTable(LoginName, ID)

EXEC sp_change_users_login ''Report'';

';



DECLARE Logins CURSOR 
FOR
select LoginName
  from ##DaveJTempTable


DECLARE @LoginName nvarchar(max)
Open Logins

FETCH NEXT FROM Logins INTO @LoginName

WHILE @@FETCH_STATUS = 0
BEGIN
DECLARE	@return_value int,
		@Password nvarchar(max)

	EXEC	##GenPass
			@Len = 32,
			@Password = @Password OUTPUT
		
		PRINT 'EXEC [dbo].[sp_change_users_login] ''Auto_Fix'',''' + @LoginName + ''', NULL,''' + @Password + ''';'
		
FETCH NEXT FROM Logins INTO @LoginName

END

CLOSE Logins;
DEALLOCATE Logins;


Drop table ##DaveJTempTable;








/*
--EXEC sp_change_users_login 'Report';

DECLARE	@return_value int,
		@Password nvarchar(max)

EXEC	@return_value = [dbo].[##GenPass]
		@Len = 32,
		@Password = @Password OUTPUT

--insert into ##DaveJTempTable(password)
SELECT	@Password --as N'@Password'


EXEC [dbo].[sp_change_users_login] 'Auto_Fix','IntegrationUser', NULL,V1!J7?yI+H#Y$~*RO&amp;kf%sE9rC8S)-Xn;


Generate a list of Users without logins
Store these, so you can run them in a cursor, against "sp_change_users_login"




EXEC [dbo].[sp_change_users_login] ''Auto_Fix'', UserName1, 'Null', 'B3r12-3x$098f6'


--create table #TT  (LoginName nvarchar(100), ID nvarchar(max))

--insert into #TT(LoginName, ID)
--EXEC sp_change_users_login ''Report'';



EXEC sp_MSForEachDB '
USE [?];
if(''?'' LIKE N''%BritishCouncil_ContentProducer'')
--SELECT DB_NAME(); 
EXEC [dbo].[sp_change_users_login] ''Auto_Fix'', ''cp_britishcouncil'',NULL ,''B3r12-3x$098f6'';

';

EXEC [dbo].[sp_change_users_login] 'Auto_Fix', 'cp_britishcouncil',NULL ,'B3r12-3x$098f6';



--cp_britishcouncil--

Barring a conflict, the row for user 'cp_britishcouncil' will be fixed by updating its link to a new login.
The number of orphaned users fixed by updating users was 0.
The number of orphaned users fixed by adding new logins and then updating users was 1.

*/
