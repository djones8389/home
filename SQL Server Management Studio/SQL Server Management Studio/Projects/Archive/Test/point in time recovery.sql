--USE [BRITISHCOUNCIL_CPProjectAdmin_10.0]

--CREATE TABLE [dbo].[ProjectListTable_Backup](
--	[ID] [int] NOT NULL,
--	[Name] [nvarchar](100) NOT NULL,
--	[DatabaseName] [varchar](103) NOT NULL,
--	[CreatedByUserID] [int] NOT NULL,
--	[ClientName] [nvarchar](50) NOT NULL,
--	[CreationDate] [varchar](20) NOT NULL,
--	[Status] [int] NOT NULL,
--	[OutputMedium] [int] NOT NULL,
--	[UseAsTemplate] [bit] NULL
--	)


--INSERT INTO [ProjectListTable_Backup](ID, Name, DatabaseName, CreatedByUserID, ClientName, CreationDate, Status, OutputMedium, UseAsTemplate)
--SELECT ID, Name, DatabaseName, CreatedByUserID, ClientName, CreationDate, Status, OutputMedium, UseAsTemplate FROM [ProjectListTable]

--ALTER DATABASE [BRITISHCOUNCIL_CPProjectAdmin_10.0] SET RECOVERY FULL

--SELECT name, physical_name AS current_file_location
--FROM sys.master_files 
--where name like 'CPMaster_%'

DELETE FROM [BRITISHCOUNCIL_CPProjectAdmin_10.0]..[ProjectListTable]

--BACKUP DATABASE [BRITISHCOUNCIL_CPProjectAdmin_10.0] TO DISK = N'C:\Batch\myDB1.bak' WITH NOINIT, NOSKIP, NO_COMPRESSION, COPY_ONLY
--BACKUP LOG [BRITISHCOUNCIL_CPProjectAdmin_10.0] TO DISK = N'C:\Batch\myDB1.bak' WITH NORECOVERY, NOINIT, NOSKIP, NO_COMPRESSION, COPY_ONLY


RESTORE DATABASE [BRITISHCOUNCIL_CPProjectAdmin_10.0] 
FROM DISK = N'C:\Batch\myDB.bak'
    WITH FILE = 1, NORECOVERY, REPLACE, NOUNLOAD, NOREWIND,  STATS = 10;

RESTORE LOG [BRITISHCOUNCIL_CPProjectAdmin_10.0] FROM DISK = N'C:\Batch\myDB.bak'
	 WITH FILE = 1, RECOVERY, NOUNLOAD, NOREWIND,  STATS = 10, STOPAT = N'2015-07-15T11:40:00';

RESTORE DATABASE  [BRITISHCOUNCIL_CPProjectAdmin_10.0] WITH RECOVERY




