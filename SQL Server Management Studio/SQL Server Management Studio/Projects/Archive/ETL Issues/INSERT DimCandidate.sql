USE UAT_SQA2_SecureAssess

DECLARE @UsersToInsert TABLE
(

    CandidateKey int
	, LatestWHCandidateKey int
	, Version tinyint
	, Forename nvarchar(100)
	, Surname nvarchar(100)
	, EthnicOrigin nvarchar(70)
	, DOB smalldatetime
	, CandidateRef nvarchar(300)
	, ULN nvarchar(10)
	, MiddleName nvarchar(50)
	, Gender char(1)
	, SpecialRequirements NVARCHAR(MAX)--XML
	, Addressline1 nvarchar(50)
	, AddressLine2 nvarchar(50)
	, Town nvarchar(100)
	, County nvarchar(100)
	, Country nvarchar(100)
	, Postcode nvarchar(12)
	, Telephone nvarchar(15)
	, Email nvarchar(max)
	, IsExaminer bit
	, UserName nvarchar(50)
)

INSERT @UsersToInsert
	SELECT
		WUT.UserId CandidateKey
		,WUT.[ID] LatestWHCandidateKey
		,WUT.[version] [Version]
		,WUT.[Forename]
		,WUT.[Surname]
		,(SELECT [EthnicOriginKey] FROM [UAT_SQA2_SurpassDataWarehouse].[dbo].[DimEthnicOrigins] where EthnicOrigin = WUT.[EthnicOrigin])
		,WUT.[DOB]
		,WUT.[CandidateRef]
		,CONVERT(nvarchar(10),'') ULN
		,WUT.[Middlename]
		,WUT.[Gender]
		,CONVERT(NVARCHAR(MAX),WUT.[SpecialRequirements]) SpecialRequirements
		,WUT.[AddressLine1]
		,WUT.[AddressLine2]
		,WUT.[Town]
		,(Select CountyKey from [UAT_SQA2_SurpassDataWarehouse].[dbo].[DimCounty] where County = WUT.[County])
		,(Select CountryKey from [UAT_SQA2_SurpassDataWarehouse].[dbo].[DimCountry] where Country = WUT.[Country])
		,WUT.[PostCode]
		,WUT.[Telephone]
		,CONVERT(NVARCHAR(100),WUT.[Email]) Email
		,CONVERT(BIT, 0) IsExaminer
		,UserName
	FROM [dbo].[WAREHOUSE_UserTable] WUT
	WHERE ID > 0
		AND ID <= 2147483647
		AND WUT.UserId IN (select DISTINCT CandidateKey from UAT_SQA2_SurpassDataWarehouse.ETL.stg_FactExamSessions
							WHERE CandidateKey NOT IN 
								(SELECT CandidateKey FROM UAT_SQA2_SurpassDataWarehouse..DimCandidate)
				)
		

INSERT UAT_SQA2_SurpassDataWarehouse..DimCandidate(CandidateKey, LatestWHCandidateKey, Version, Forename, Surname, EthnicOriginKey, DOB, CandidateRef, Username, ULN, Middlename, Gender, SpecialRequirements, AddressLine1, AddressLine2, Town, PostCode, Telephone, Email, CountryKey, CountyKey, IsExaminer)
SELECT CandidateKey, LatestWHCandidateKey, Version, Forename, Surname, EthnicOrigin, DOB, CandidateRef,  Username, ULN,  Middlename, Gender, SpecialRequirements, AddressLine1, AddressLine2, Town, PostCode, Telephone, Email, Country, County, IsExaminer   FROM @UsersToInsert

