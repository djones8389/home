
--; WITH cte AS (
--select examsessionid as ESID	
--	from WAREHOUSE_ExamSessionTable_Shreded 
--		where started = '1900-01-01 00:00:00.000'

--)


create table #DJTEST
(
	ESID int
)


insert into #DJTEST
select examsessionid as ESID	
	from WAREHOUSE_ExamSessionTable_Shreded 
		where started = '1900-01-01 00:00:00.000'

--31--

begin tran

select wests.examsessionid, started--, ExamStateChangeAuditXml, CAST(west.ExamStateChangeAuditXml AS XML).query('data(//stateChange[newStateID=6][1]/changeDate/text())').value('.', 'DATETIME')
	from WAREHOUSE_ExamSessionTable_Shreded  as wests
	
	inner join WAREHOUSE_ExamSessionTable as west
	on west.ID = wests.examsessionid
		where wests.examSessionId = 83998

update WAREHOUSE_ExamSessionTable_Shreded

SET WAREHOUSE_ExamSessionTable_Shreded.[started] = CAST(WAREHOUSE_ExamSessionTable.ExamStateChangeAuditXml AS XML).query('data(//stateChange[newStateID=6][1]/changeDate/text())').value('.', 'DATETIME')
from WAREHOUSE_ExamSessionTable_Shreded o

inner join #DJTEST
on #DJTEST.ESID = o.examSessionId

inner join WAREHOUSE_ExamSessionTable
on WAREHOUSE_ExamSessionTable.ID = o.examSessionId

where o.examSessionId = 83998

--where #DJTEST.ESID = o.examSessionId
select examsessionid, started 
	from WAREHOUSE_ExamSessionTable_Shreded 
		where examSessionId = 83998
		
rollback



select WAREHOUSE_ExamSessionTable_Shreded.examSessionId
	,  started as [Currently]
	,  CAST(WAREHOUSE_ExamSessionTable.ExamStateChangeAuditXml AS XML).query('data(//stateChange[newStateID=6][1]/changeDate/text())').value('.', 'DATETIME') as [ShouldBe]  	
	from WAREHOUSE_ExamSessionTable_Shreded 

	inner join WAREHOUSE_ExamSessionTable
	on WAREHOUSE_ExamSessionTable.id = WAREHOUSE_ExamSessionTable_Shreded.examSessionId

		where started = '1900-01-01 00:00:00.000'
		
		
		
	





; WITH cte AS (
select examsessionid as ESID	
	from WAREHOUSE_ExamSessionTable_Shreded 
		where started = '1900-01-01 00:00:00.000'

)


--create table #DJTEST2
--(
--	ID int,
--	CorrectState6Date datetime
--)


--insert into #DJTEST2

--select 	
--	ID
--	,CAST(WAREHOUSE_ExamSessionTable.ExamStateChangeAuditXml AS XML).query('data(//stateChange[newStateID=6][1]/changeDate/text())').value('.', 'DATETIME') as [ShouldBe]  	
--from WAREHOUSE_ExamSessionTable
		
		
select * from CTE
	
	inner join #DJTEST2	
	on #DJTEST2.ID = 	CTE.ESID

	