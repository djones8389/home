use AAT_CPProjectAdmin

ALTER view Datepicker
as
select DISTINCT
	PLT.Name [ProjectName]
	, PT.ID [ItemID]
	,  a.b.value('@Nam','nvarchar(200)') [ItemName]
	,  StatusChecker.[StatusName]
from ItemCustomQuestionTable CQT (READUNCOMMITTED)

INNER JOIN ProjectListTable PLT (READUNCOMMITTED)
on PLT.id = SUBSTRING(CQT.ID, 0, CHARINDEX('P',CQT.ID))

CROSS APPLY ProjectStructurexml.nodes('//Pro//Pag') a(b)

INNER JOIN (
	SELECT ID
		, c.d.value('.','nvarchar(100)') [StatusName]
		, c.d.value('@id','smallint') [StatusID]
	FROM ProjectListTable (READUNCOMMITTED)
	CROSS APPLY ProjectDefaultXml.nodes('Defaults/StatusList/Status') c(d)
) StatusChecker
on PLT.ID = StatusChecker.ID
	and StatusChecker.[StatusID] =  a.b.value('@sta','smallint')
inner join PageTable PT (READUNCOMMITTED)
on PT.ParentID = SUBSTRING(CQT.ID, 0, CHARINDEX('P',CQT.ID))
	and PT.ID = cast(PT.ParentID as nvarchar(20)) + 'P' + cast(a.b.value('@ID','nvarchar(20)') as nvarchar(20))

where CONVERT(xml, CQT.itemValue).exist('ItemValue/extension/t/r/c[@dt]') = 1



SELECT *
FROM Datepicker

	/*
select *
from ProjectListTable
where id = 918



SELECT ID
	, c.d.value('.','nvarchar(100)') [Status]
	,c.d.value('@id','smallint')
FROM ProjectListTable
CROSS APPLY ProjectDefaultXml.nodes('Defaults/StatusList/Status') c(d)
where id = 868
*/