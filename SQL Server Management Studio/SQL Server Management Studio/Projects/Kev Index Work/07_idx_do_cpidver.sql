USE [Demo_SDWH_Perf2]
GO

/****** Object:  Index [04_idx_dc_w]    Script Date: 02/19/2016 08:50:31 ******/
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[DimOptions]') AND name = N'07_idx_do_cpidver')
DROP INDEX [07_idx_do_cpidver] ON [dbo].[DimOptions] WITH ( ONLINE = OFF )
GO

USE [Demo_SDWH_Perf2]
GO
CREATE NONCLUSTERED INDEX [07_idx_do_cpidver]
ON [dbo].[DimOptions] ([CPID],[CPVersion])

GO


