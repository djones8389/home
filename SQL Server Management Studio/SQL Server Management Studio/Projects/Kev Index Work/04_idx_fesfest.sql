USE [Demo_SDWH_Perf2]
GO

/****** Object:  Index [04_idx_fesfest]    Script Date: 02/19/2016 08:49:16 ******/
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[FactExamSessions]') AND name = N'04_idx_fesfest')
DROP INDEX [04_idx_fesfest] ON [dbo].[FactExamSessions] WITH ( ONLINE = OFF )
GO

USE [Demo_SDWH_Perf2]
GO

/****** Object:  Index [04_idx_fesfest]    Script Date: 02/19/2016 08:49:16 ******/
CREATE NONCLUSTERED INDEX [04_idx_fesfest] ON [dbo].[FactExamSessions] 
(	[FinalExamState] ASC,
	[CompletionDateKey] ASC	
)

INCLUDE ( [CentreKey],
[CompletionTime],
[ExamKey],
[ExamSessionKey],
[ExamVersionKey],
[Pass],
[QualificationKey],
[UserMarks]) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO


