use master

IF OBJECT_ID('tempdb..##ItemBank') IS NOT NULL DROP TABLE ##ItemBank;

CREATE TABLE ##ItemBank (
	Client nvarchar(1000)
	,QualificationName nvarchar(200)
	,QualStatus nvarchar(50)
	,TestName  nvarchar(1000)
	,TestFormName nvarchar(1000)
	,AssessmentStatus  nvarchar(50)
	,TestFormReference nvarchar(1000)
	,LastTimeTaken datetime null
);

DECLARE @dynamic nvarchar(MAX)='';

select @dynamic +=CHAR(13)+ '
use '+QUOTENAME([NAME])+'

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

INSERT ##ItemBank(Client,QualificationName,QualStatus,TestName,TestFormName,AssessmentStatus,TestFormReference)
SELECT 
	db_Name() 
	, QT.QualificationName
	, QL.[Name] QualStatus
	, AGT.Name [TestName]
	, AssessmentName [TestFormName]
	, AL.[Name] AssessmentStatus
	, externalreference [TestFormReference]
FROM AssessmentTable AT
INNER JOIN AssessmentGroupTable AGT ON AGT.ID = AT.AssessmentGroupID
INNER JOIN QualificationTable QT on QT.ID = AGT.QualificationID
INNER JOIN QualificationStatusLookupTable QL on QL.ID = QT.QualificationStatus
INNER JOIN AssessmentStatusLookupTable AL on AL.ID = AT.AssessmentStatus
WHERE EnableCandidateBreak = 1 
	AND CandidateBreakStyle = 1 
'
from sys.databases
where state_desc = 'ONLINE'
	and name like '%[_]ItemBank';

exec(@dynamic);



/*
	
	�	Last time the exam was taken
	�	Date/s the exam is scheduled for
	�	How many candidates have taken the exam
	�	How many candidates are due to take it in the future
	�	How many candidates used any of the unscheduled break time

*/


USE master

	DECLARE myCursor CURSOR FOR
	SELECT 
		substring(client,0,charindex('_',client)) [Client]
		, QualificationName
		, TestName
		, TestFormName
		, TestFormReference
	FROM ##ItemBank
	WHERE substring(client,0,charindex('_',client))  = 'Demo';

	DECLARE @Client nvarchar(100), @QualificationName nvarchar(100), @TestName nvarchar(100), @TestFormName nvarchar(100), @TestFormReference nvarchar(100);

	OPEN myCursor

	FETCH NEXT FROM myCursor INTO @Client,@QualificationName,@TestName,@TestFormName,@TestFormReference

	WHILE @@FETCH_STATUS = 0

	BEGIN

			DECLARE @LastTime TABLE (LastTime datetime)
			DECLARE @SQL NVARCHAR(MAX) = ''

				SELECT @SQL = '	

				SELECT MIN(StateChangeDate)
				FROM '+@Client+'_SecureAssess.dbo.ScheduledExamsTable SCET
				INNER JOIN '+@Client+'_SecureAssess.dbo.ExamSessionTable EST ON EST.ScheduledExamID = SCET.ID
				INNER JOIN '+@Client+'_SecureAssess.dbo.IB3QualificationLookup IB3 on IB3.ID = SCET.qualificationID
				LEFT JOIN (
					SELECT examSessionID
						, max(StateChangeDate) StateChangeDate
					FROM '+@Client+'_SecureAssess.dbo.ExamStateChangeAuditTable
					where newState in (6,9)
					group by examSessionID

				) ESCAT
				ON ESCAT.ExamSessionID = EST.ID	
				where ib3.QualificationName = '''+@QualificationName+'''
					and examName = '''+@TestName+'''
					and examVersionName= '''+@TestFormName+'''
					and examVersionRef = '''+@TestFormReference+'''
				'
				INSERT @LastTime
				EXEC(@SQL)


				UPDATE ##ItemBank
				SET LastTimeTaken = (SELECT TOP 1 LastTime from @LastTime)
				where QualificationName = @QualificationName
					and @TestName = @TestName
					and TestFormName = TestFormName
				and TestFormReference = @TestFormReference

				DELETE @LastTime

		FETCH NEXT FROM myCursor INTO @Client,@QualificationName,@TestName,@TestFormName,@TestFormReference

	END 
	CLOSE myCursor
	DEALLOCATE myCursor


	SELECT * FROM ##ItemBank


	USE Demo_SecureAssess

	;with minCTE as (
	SELECT est.id
		, MIN(StateChangeDate) StateChangeDate
		--QualificationName
		-- , examName
		-- , examVersionName
		-- , examVersionRef
	from ScheduledExamsTable SCET
	INNER JOIN ExamSessionTable EST ON EST.ScheduledExamID = SCET.ID
	INNER JOIN IB3QualificationLookup IB3 on IB3.ID = SCET.qualificationID
	LEFT JOIN (
		SELECT examSessionID
			, max(StateChangeDate) StateChangeDate
		FROM ExamStateChangeAuditTable
		where newState in (6,9)
		group by examSessionID

	) ESCAT
	ON ESCAT.ExamSessionID = EST.ID	
	where ib3.QualificationName = 'Demonstration'
		and examName = 'Demonstration Test'
		and examVersionName= 'Demonstration Test Form'
		and examVersionRef = 'Demo Form - SF'
	group by est.id

	UNION


	SELECT examsessionid
		, min(started) StateChangeDate
	FROM WAREHOUSE_ExamSessionTable_Shreded WESTS
	where QualificationName = 'Demonstration'
		and examName = 'Demonstration Test'
		and examVersionName= 'Demonstration Test Form'
		and examVersionRef = 'Demo Form - SF'
		and [started] != '1900-01-01 00:00:00.000'
	group by examsessionid
	)

	select min(StateChangeDate)
	from minCTE


/*



SELECT A.*
FROM (
	select 
		substring(client,0,charindex('_',client)) [Client]
		, QualificationName
		, QualStatus
		, TestName
		, AssessmentStatus
		, TestFormName
		, TestFormReference
	from ##ItemBank
) A



USE Demo_SecureAssess;

select DISTINCT IB.*
from ScheduledExamsTable SCET
INNER JOIN ExamSessionTable EST ON EST.ScheduledExamID = SCET.ID
INNER JOIN IB3QualificationLookup IB3 on IB3.ID = SCET.qualificationID
INNER JOIN ##ItemBank IB ON IB.QualificationName = IB3.QualificationName
						AND IB.TestName = SCET.examName
LEFT JOIN (
	SELECT MAX(StateChangeDate) StateChangeDate
		,	ExamSessionID
	FROM (
		SELECT MAX(StateChangeDate) StateChangeDate
			,ExamSessionID
		  FROM ExamStateChangeAuditTable
		  where newState in (6,9)
		  GROUP by ExamSessionID
	) B
	GROUP by ExamSessionID
) ESCAT  
	ON ESCAT.ExamSessionID = EST.ID  



	
	SELECT --MIN(StateChangeDate)
		QualificationName
		 , examName
		 , examVersionName
		 , examVersionRef
	from ScheduledExamsTable SCET
	INNER JOIN ExamSessionTable EST ON EST.ScheduledExamID = SCET.ID
	INNER JOIN IB3QualificationLookup IB3 on IB3.ID = SCET.qualificationID
	LEFT JOIN (
		SELECT examSessionID
			, max(StateChangeDate) StateChangeDate
		FROM ExamStateChangeAuditTable
		where newState in (6,9)
		group by examSessionID

	) ESCAT
	ON ESCAT.ExamSessionID = EST.ID	
	where ib3.QualificationName = 'Demonstration!^%'
		and examName = 'Demonstration Test'
		and examVersionName= 'Demonstration Test Form'
		and examVersionRef = 'Demo Form - SF'










*/