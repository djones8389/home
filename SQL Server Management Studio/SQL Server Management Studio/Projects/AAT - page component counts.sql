SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;


SELECT ID
	, ISNULL(ItemVideoTable,0) ItemVideoTable
	, ISNULL(ItemTextSelectorTable,0) ItemTextSelectorTable
	, ISNULL(ItemTextBoxTable,0) ItemTextBoxTable
	, ISNULL(ItemSceneConditionTable,0) ItemSceneConditionTable
	, ISNULL(ItemReorderingTable,0) ItemReorderingTable
	, ISNULL(ItemPicklistTable,0) ItemPicklistTable
	, ISNULL(ItemMultipleChoiceTable,0) ItemMultipleChoiceTable
	, ISNULL(ItemImageMapTable,0) ItemImageMapTable
	, ISNULL(ItemHotSpotTable,0) ItemHotSpotTable
	, ISNULL(ItemGraphicTable,0) ItemGraphicTable
	, ISNULL(ItemGapfillTable,0) ItemGapfillTable
	, ISNULL(ItemDragAndDropTable,0) ItemDragAndDropTable
	, ISNULL(ItemDocumentTable,0) ItemDocumentTable
	, ISNULL(ItemCustomQuestionTable,0) ItemCustomQuestionTable
	, ISNULL(ItemAutoShapeTable,0) ItemAutoShapeTable
	, ISNULL(ItemAutoAudio,0) ItemAutoAudio
	, ISNULL(ItemAudioRecorderTable,0) ItemAudioRecorderTable
FROM PageTable 
LEFT JOIN (
	SELECT A.PageID
		,Count(A.Component) ItemVideoTable
	FROM (
		SELECT SUBSTRING(ID, 0, CHARINDEX('S', ID)) PageID
			,SUBSTRING(ID, CHARINDEX('C', ID) + 1, CHARINDEX('I', ID) - CHARINDEX('C', ID) - 2 + Len('I')) 'Component'
		FROM dbo.[ItemVideoTable](READUNCOMMITTED)
		) A
	GROUP BY PageID
	) ItemVideoTable ON ItemVideoTable.PageID = PageTable.ID
LEFT JOIN (
	SELECT A.PageID
		,Count(A.Component) ItemTextSelectorTable
	FROM (
		SELECT SUBSTRING(ID, 0, CHARINDEX('S', ID)) PageID
			,SUBSTRING(ID, CHARINDEX('C', ID) + 1, CHARINDEX('I', ID) - CHARINDEX('C', ID) - 2 + Len('I')) 'Component'
		FROM dbo.[ItemTextSelectorTable](READUNCOMMITTED)
		) A
	GROUP BY PageID
	) ItemTextSelectorTable ON ItemTextSelectorTable.PageID = PageTable.ID
LEFT JOIN (
	SELECT A.PageID
		,Count(A.Component) ItemTextBoxTable
	FROM (
		SELECT SUBSTRING(ID, 0, CHARINDEX('S', ID)) PageID
			,SUBSTRING(ID, CHARINDEX('C', ID) + 1, CHARINDEX('I', ID) - CHARINDEX('C', ID) - 2 + Len('I')) 'Component'
		FROM dbo.[ItemTextBoxTable](READUNCOMMITTED)
		) A
	GROUP BY PageID
	) ItemTextBoxTable ON ItemTextBoxTable.PageID = PageTable.ID
LEFT JOIN (
	SELECT A.PageID
		,Count(A.Component) ItemSceneConditionTable
	FROM (
		SELECT SUBSTRING(ID, 0, CHARINDEX('S', ID)) PageID
			,SUBSTRING(ID, CHARINDEX('C', ID) + 1, CHARINDEX('I', ID) - CHARINDEX('C', ID) - 2 + Len('I')) 'Component'
		FROM dbo.[ItemSceneConditionTable](READUNCOMMITTED)
		) A
	GROUP BY PageID
	) ItemSceneConditionTable ON ItemSceneConditionTable.PageID = PageTable.ID
LEFT JOIN (
	SELECT A.PageID
		,Count(A.Component) ItemReorderingTable
	FROM (
		SELECT SUBSTRING(ID, 0, CHARINDEX('S', ID)) PageID
			,SUBSTRING(ID, CHARINDEX('C', ID) + 1, CHARINDEX('I', ID) - CHARINDEX('C', ID) - 2 + Len('I')) 'Component'
		FROM dbo.[ItemReorderingTable](READUNCOMMITTED)
		) A
	GROUP BY PageID
	) ItemReorderingTable ON ItemReorderingTable.PageID = PageTable.ID
LEFT JOIN (
	SELECT A.PageID
		,Count(A.Component) ItemPicklistTable
	FROM (
		SELECT SUBSTRING(ID, 0, CHARINDEX('S', ID)) PageID
			,SUBSTRING(ID, CHARINDEX('C', ID) + 1, CHARINDEX('I', ID) - CHARINDEX('C', ID) - 2 + Len('I')) 'Component'
		FROM dbo.[ItemPicklistTable](READUNCOMMITTED)
		) A
	GROUP BY PageID
	) ItemPicklistTable ON ItemPicklistTable.PageID = PageTable.ID
LEFT JOIN (
	SELECT A.PageID
		,Count(A.Component) ItemMultipleChoiceTable
	FROM (
		SELECT SUBSTRING(ID, 0, CHARINDEX('S', ID)) PageID
			,SUBSTRING(ID, CHARINDEX('C', ID) + 1, CHARINDEX('I', ID) - CHARINDEX('C', ID) - 2 + Len('I')) 'Component'
		FROM dbo.[ItemMultipleChoiceTable](READUNCOMMITTED)
		) A
	GROUP BY PageID
	) ItemMultipleChoiceTable ON ItemMultipleChoiceTable.PageID = PageTable.ID
LEFT JOIN (
	SELECT A.PageID
		,Count(A.Component) ItemImageMapTable
	FROM (
		SELECT SUBSTRING(ID, 0, CHARINDEX('S', ID)) PageID
			,SUBSTRING(ID, CHARINDEX('C', ID) + 1, CHARINDEX('I', ID) - CHARINDEX('C', ID) - 2 + Len('I')) 'Component'
		FROM dbo.[ItemImageMapTable](READUNCOMMITTED)
		) A
	GROUP BY PageID
	) ItemImageMapTable ON ItemImageMapTable.PageID = PageTable.ID
LEFT JOIN (
	SELECT A.PageID
		,Count(A.Component) ItemHotSpotTable
	FROM (
		SELECT SUBSTRING(ID, 0, CHARINDEX('S', ID)) PageID
			,SUBSTRING(ID, CHARINDEX('C', ID) + 1, CHARINDEX('I', ID) - CHARINDEX('C', ID) - 2 + Len('I')) 'Component'
		FROM dbo.[ItemHotSpotTable](READUNCOMMITTED)
		) A
	GROUP BY PageID
	) ItemHotSpotTable ON ItemHotSpotTable.PageID = PageTable.ID
LEFT JOIN (
	SELECT A.PageID
		,Count(A.Component) ItemGraphicTable
	FROM (
		SELECT SUBSTRING(ID, 0, CHARINDEX('S', ID)) PageID
			,SUBSTRING(ID, CHARINDEX('C', ID) + 1, CHARINDEX('I', ID) - CHARINDEX('C', ID) - 2 + Len('I')) 'Component'
		FROM dbo.[ItemGraphicTable](READUNCOMMITTED)
		) A
	GROUP BY PageID
	) ItemGraphicTable ON ItemGraphicTable.PageID = PageTable.ID
LEFT JOIN (
	SELECT A.PageID
		,Count(A.Component) ItemGapfillTable
	FROM (
		SELECT SUBSTRING(ID, 0, CHARINDEX('S', ID)) PageID
			,SUBSTRING(ID, CHARINDEX('C', ID) + 1, CHARINDEX('I', ID) - CHARINDEX('C', ID) - 2 + Len('I')) 'Component'
		FROM dbo.[ItemGapfillTable](READUNCOMMITTED)
		) A
	GROUP BY PageID
	) ItemGapfillTable ON ItemGapfillTable.PageID = PageTable.ID
LEFT JOIN (
	SELECT A.PageID
		,Count(A.Component) ItemDragAndDropTable
	FROM (
		SELECT SUBSTRING(ID, 0, CHARINDEX('S', ID)) PageID
			,SUBSTRING(ID, CHARINDEX('C', ID) + 1, CHARINDEX('I', ID) - CHARINDEX('C', ID) - 2 + Len('I')) 'Component'
		FROM dbo.[ItemDragAndDropTable](READUNCOMMITTED)
		) A
	GROUP BY PageID
	) ItemDragAndDropTable ON ItemDragAndDropTable.PageID = PageTable.ID
LEFT JOIN (
	SELECT A.PageID
		,Count(A.Component) ItemDocumentTable
	FROM (
		SELECT SUBSTRING(ID, 0, CHARINDEX('S', ID)) PageID
			,SUBSTRING(ID, CHARINDEX('C', ID) + 1, CHARINDEX('I', ID) - CHARINDEX('C', ID) - 2 + Len('I')) 'Component'
		FROM dbo.[ItemDocumentTable](READUNCOMMITTED)
		) A
	GROUP BY PageID
	) ItemDocumentTable ON ItemDocumentTable.PageID = PageTable.ID
LEFT JOIN (
	SELECT A.PageID
		,Count(A.Component) ItemCustomQuestionTable
	FROM (
		SELECT SUBSTRING(ID, 0, CHARINDEX('S', ID)) PageID
			,SUBSTRING(ID, CHARINDEX('C', ID) + 1, CHARINDEX('I', ID) - CHARINDEX('C', ID) - 2 + Len('I')) 'Component'
		FROM dbo.[ItemCustomQuestionTable](READUNCOMMITTED)
		) A
	GROUP BY PageID
	) ItemCustomQuestionTable ON ItemCustomQuestionTable.PageID = PageTable.ID
LEFT JOIN (
	SELECT A.PageID
		,Count(A.Component) ItemAutoShapeTable
	FROM (
		SELECT SUBSTRING(ID, 0, CHARINDEX('S', ID)) PageID
			,SUBSTRING(ID, CHARINDEX('C', ID) + 1, CHARINDEX('I', ID) - CHARINDEX('C', ID) - 2 + Len('I')) 'Component'
		FROM dbo.[ItemAutoShapeTable](READUNCOMMITTED)
		) A
	GROUP BY PageID
	) ItemAutoShapeTable ON ItemAutoShapeTable.PageID = PageTable.ID
LEFT JOIN (
	SELECT A.PageID
		,Count(A.Component) ItemAutoAudio
	FROM (
		SELECT SUBSTRING(ID, 0, CHARINDEX('S', ID)) PageID
			,SUBSTRING(ID, CHARINDEX('C', ID) + 1, CHARINDEX('I', ID) - CHARINDEX('C', ID) - 2 + Len('I')) 'Component'
		FROM dbo.[ItemAutoAudio](READUNCOMMITTED)
		) A
	GROUP BY PageID
	) ItemAutoAudio ON ItemAutoAudio.PageID = PageTable.ID
LEFT JOIN (
	SELECT A.PageID
		,Count(A.Component) ItemAudioRecorderTable
	FROM (
		SELECT SUBSTRING(ID, 0, CHARINDEX('S', ID)) PageID
			,SUBSTRING(ID, CHARINDEX('C', ID) + 1, CHARINDEX('I', ID) - CHARINDEX('C', ID) - 2 + Len('I')) 'Component'
		FROM dbo.[ItemAudioRecorderTable](READUNCOMMITTED)
		) A
	GROUP BY PageID
	) ItemAudioRecorderTable ON ItemAudioRecorderTable.PageID = PageTable.ID

order by cast(substring(PageTable.ID, 0, CHARINDEX('P',PageTable.ID)) as float) ASC;


/*Dynamic SQL

SELECT 'LEFT JOIN (

       SELECT A.PageID
              , Count(A.Component) ComponentCount_'+name+'
       FROM (
       SELECT SUBSTRING(ID, 0, CHARINDEX(''S'',ID)) PageID
              , SUBSTRING(ID, CHARINDEX(''C'', ID) + 1, CHARINDEX(''I'',ID) - CHARINDEX(''C'', ID) - 2 + Len(''I'')) ''Component''
       From ' + 'dbo.'+ QUOTENAME(name) + ' (READUNCOMMITTED)
       ) A
       group by PageID
) '+name+'
on '+name+'.PageID = PageTable.ID

'      
FROM (
       SELECT name
       FROM sys.tables
       WHERE name LIKE 'Item%'
              AND name NOT IN ('ItemSpecificationLookup','ItemType')
) a


*/