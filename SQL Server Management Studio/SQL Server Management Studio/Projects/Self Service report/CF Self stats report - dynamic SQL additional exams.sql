SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;


DECLARE @StartDate datetime = '01 Oct 2016'
	,  @EndDate datetime = '31 Oct 2016'
	,  @ExamStates nvarchar(100)= '6,10'

IF OBJECT_ID('tempdb..#Params') IS NOT NULL DROP TABLE #Params;

CREATE TABLE #Params (
	States nvarchar(50)
	, startDate datetime
	, endDate datetime
);

INSERT #Params(States, startDate, endDate)
select @ExamStates, @StartDate, @EndDate;

CREATE TABLE #AdditionalIDs (
	ESID int
);


DECLARE @Dynamic nvarchar(max)=''

select @Dynamic = '
	SELECT 
		WAREHOUSE_ExamSessionTable.ID
	FROM WAREHOUSE_ExamSessionTable 
	where  EXAMSTATECHANGEAUDITXML.value(''data(exam/stateChange[newStateID=('+States+')]/changeDate)[1]'',''datetime'')  between '''
		+CONVERT(VARCHAR(10),StartDate, 120)+''' and '''+CONVERT(VARCHAR(10),EndDate, 120)+'''
		and WarehouseExamState != 1;
		'
from #Params

--INSERT #AdditionalIDs
print(@Dynamic)


CREATE CLUSTERED INDEX [IX_ID] ON #AdditionalIDs (ESID);