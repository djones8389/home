--original

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

declare @State6FromDate datetime = '01 Oct 2016 00:00:01'
declare @State6ToDate datetime = '10 Nov 2016 23:59:59'

select ID
, examsessionid
,ExamStateChangeAuditXml.value('(/exam/stateChange[newStateID=("6", "10")]/changeDate)[1]', 'date')
, ExamStateChangeAuditXml
FROM WAREHOUSE_ExamSessionTable WEST
where KeyCode = '2RVEBPA6'
 AND ExamStateChangeAuditXml.value('(/exam/stateChange[newStateID=("6", "10")]/changeDate)[1]', 'date') BETWEEN @State6FromDate AND @State6ToDate


 --new

 select ID
	, examsessionid
	--, ExamStateChangeAuditXml.value('(/exam/stateChange[newStateID=("6", "10")]/changeDate)[1]', 'date')
	, ExamStateChangeAuditXml
	, a.b.value('.[1]','datetime')
FROM WAREHOUSE_ExamSessionTable WEST
cross apply ExamStateChangeAuditXml.nodes('exam/stateChange[newStateID=(6,10)]/changeDate') a(b)
where KeyCode = '2RVEBPA6'
	and a.b.value('.[1]','datetime') BETWEEN @State6FromDate AND @State6ToDate