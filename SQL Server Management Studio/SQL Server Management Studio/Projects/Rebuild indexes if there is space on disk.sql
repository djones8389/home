if OBJECT_ID('tempdb..##DiskSpace') IS NOT NULL DROP TABLE ##DiskSpace;

CREATE TABLE ##DiskSpace (
	Drive CHAR(1) 
	,Space INT
	)
	
INSERT ##DiskSpace
EXEC master..xp_fixeddrives

exec sp_MSforeachdb '

use [?];

if(DB_ID() > 4)


BEGIN

Declare @Dynamic NVARCHAR(MAX) = '''';


	SELECT @Dynamic +=CHAR(13) + 
	
	''ALTER INDEX '' + C.NAME + '' ON '' + ''dbo'' + ''.'' + B.NAME + '' REBUILD;''
		FROM sys.dm_db_index_physical_stats(DB_ID(), NULL, NULL, NULL, NULL) A
		INNER JOIN sys.objects B ON A.object_id = B.object_id
		INNER JOIN sys.indexes C ON B.object_id = C.object_id
			AND A.index_id = C.index_id
		INNER JOIN sys.partitions D ON B.object_id = D.object_id
			AND A.index_id = D.index_id
		INNER JOIN sys.dm_db_partition_stats E ON E.object_id = b.object_id
			AND e.index_id = c.index_id
		WHERE C.index_id > 0
			AND A.avg_fragmentation_in_percent > 30
			AND ((E.used_page_count * 8)/1024) < (
				(
				
					select [Space]
					from ##DiskSpace
					where Drive COLLATE Latin1_General_CI_AS in (
				
						select substring(physical_name, 0, charindex('':'', physical_name))
						from sys.database_files
						where type_desc = ''ROWS''
					 )
				) - 5120 /*5GB buffer*/
		
			)
	PRINT(@Dynamic);

END

'

