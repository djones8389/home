/*
Item type key
Introduction = 0,
       MultipleChoice = 2,
        MultipleResponse = 3,
        EitherOr = 4,
        NumericalEntry = 5,
        ShortAnswer = 7,
        Essay = 8,
        InformationPage = 13,
        FinishPage = 20,
        MultipleChoiceSurvey = 23,
        MultipleResponseSurvey = 24,
        EssaySurvey = 25,
*/

IF OBJECT_ID('tempdb..##ResultHolder') IS NOT NULL DROP TABLE ##ResultHolder;

CREATE TABLE ##ResultHolder (
	Client sysname
	, [SubjectName] nvarchar(200)
	, itemID nvarchar(20)
	, itemType tinyint
);

SELECT '

use '+quotename(name)+'

set transaction isolation level read uncommitted;

declare @updateDate'+SUBSTRING(Name,0,CHARINDEX('_',Name))+' datetime

declare @itemIds'+SUBSTRING(Name,0,CHARINDEX('_',Name))+' table (title nvarchar(200), ID nvarchar(21), Type int)

SET @updateDate'+SUBSTRING(Name,0,CHARINDEX('_',Name))+' = ''2016/08/12 00:00:00'' 
insert into @itemIds'+SUBSTRING(Name,0,CHARINDEX('_',Name))+'
select S.title, convert(nvarchar(10), S.ProjectId) + ''P'' + convert(nvarchar(10), I.id),  I.Type
from Items I
inner join Subjects S
on S.Id = I.SubjectId
where I.[Type] in (0,2,3,4,13,20) and I.UpdateDate < @updateDate'+SUBSTRING(Name,0,CHARINDEX('_',Name))+' and ContentUpdateDate < @updateDate'+SUBSTRING(Name,0,CHARINDEX('_',Name))+'

SET @updateDate'+SUBSTRING(Name,0,CHARINDEX('_',Name))+' = ''2017/01/12 00:00:00''
insert into @itemIds'+SUBSTRING(Name,0,CHARINDEX('_',Name))+'
select S.title,convert(nvarchar(10), S.ProjectId) + ''P'' + convert(nvarchar(10), I.id), I.Type
from Items I
inner join Subjects S
on S.Id = I.SubjectId
where I.[Type] in (8,23,34,25) and I.UpdateDate < @updateDate'+SUBSTRING(Name,0,CHARINDEX('_',Name))+' and ContentUpdateDate < @updateDate'+SUBSTRING(Name,0,CHARINDEX('_',Name))+'

SET @updateDate'+SUBSTRING(Name,0,CHARINDEX('_',Name))+' = ''2017/07/05 00:00:00'' 
insert into @itemIds'+SUBSTRING(Name,0,CHARINDEX('_',Name))+'
select S.title,convert(nvarchar(10), S.ProjectId) + ''P'' + convert(nvarchar(10), I.id),  I.Type
from Items I
inner join Subjects S
on S.Id = I.SubjectId
where I.[Type] in (5,7) and I.UpdateDate < @updateDate'+SUBSTRING(Name,0,CHARINDEX('_',Name))+' and ContentUpdateDate < @updateDate'+SUBSTRING(Name,0,CHARINDEX('_',Name))+'

INSERT ##ResultHolder
SELECT SUBSTRING(db_Name(),0,CHARINDEX(''_'',db_Name()))
	, *
FROM @itemIds'+SUBSTRING(Name,0,CHARINDEX('_',Name))+'

'
FROM SYS.Databases
where name like '%ContentAuthor%'
