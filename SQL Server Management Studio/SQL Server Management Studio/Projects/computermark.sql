IF OBJECT_ID('tempdb..##ComputerMarked') IS NOT NULL DROP TABLE ##ComputerMarked;

CREATE TABLE ##ComputerMarked (
	client nvarchar(1000)
	, ComputerMarked int
);

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

;with items as (
select est.id
	, esirt.ItemID
from AAT_SecureAssess.dbo.ExamSessionTable est
cross apply structurexml.nodes('assessmentDetails/assessment/section/item') a(b)

inner join AAT_SecureAssess.dbo.ExamSessionItemResponseTable esirt
on est.id = esirt.examsessionid
	and esirt.ItemID = a.b.value('data(@id)[1]','nvarchar(10)')

where ItemResponseData.exist('p[@ua=1]') = 1
	and a.b.value('data(@markingType)[1]','tinyint') = 0
	and examState != 13

union

select est.ExamSessionID
	, esirt.ItemID
from AAT_SecureAssess.dbo.warehouse_ExamSessionTable est
cross apply structurexml.nodes('assessmentDetails/assessment/section/item') a(b)

inner join AAT_SecureAssess.dbo.warehouse_ExamSessionItemResponseTable esirt
on est.id = esirt.warehouseexamsessionid
	and esirt.ItemID = a.b.value('data(@id)[1]','nvarchar(10)')

where est.id in (
	select SA.ID
	from AAT_SecureAssess.dbo.Warehouse_ExamSessionTable SA
	left join [430327-AAT-SQL2\SQL2].AAT_SurpassDataWarehouse.dbo.FactExamSessions FES
	on FES.ExamSessionKey = SA.ID
	where FES.ExamSessionKey IS NULL
)
	and ItemResponseData.exist('p[@ua=1]') = 1
	and a.b.value('data(@markingType)[1]','tinyint') = 0
	
union

SELECT  FCR.ExamSessionKey  
	,  fcr.CPID collate SQL_Latin1_General_CP1_CI_AS
FROM [430327-AAT-SQL2\SQL2].AAT_SurpassDataWarehouse.dbo.ComputerMarked FCR
)

INSERT ##ComputerMarked
	SELECT 'AAT'
	, count(*)
FROM Items


SELECT *
FROM ##ComputerMarked
