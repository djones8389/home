USE PRV_BritishCouncil_SecureAssess

select qualificationName, examName, examVersionName, previousExamState, warehouseTime, KeyCode
from WAREHOUSE_ExamSessionTable_Shreded
where centrename = 'Aptis Control'
	and qualificationName = 'UAT April 2016'
	and examName = 'Listening'
 order by warehouseTime


 USE PRV_BritishCouncil_SecureAssess

select count(examsessionid), qualificationName, examName, examVersionName
from WAREHOUSE_ExamSessionTable_Shreded
where centrename = 'Aptis Control'
	and qualificationName = 'UAT April 2016'
	and examName = 'Listening'
	and warehouseTime > '22/03/2016'
 group by qualificationName, examName, examVersionName


/*
If you look at the two attached reported you will see that the number of test takers is reported at 18 for V1 and 24 for V2
However this should be 42 and 39 respectively.

select count(*)
from PRV_BritishCouncil_SecureAssess..WAREHOUSE_ExamSessionTable
where warehouseTime > '01/04/2016'

select count(*)
from PRV_BritishCouncil_SurpassDataWarehouse.ETL.stg_FactExamSessions
where warehouseTime > '01/04/2016'


--SELECT	*
--FROM FactQuestionResponses
--where ExamSessionKey = 1450734
--	and CPID = '3521P3594'



*/

/*
Source: MERGE stgFactQuestionResponses      Description: Violation of PRIMARY KEY constraint 'PK_FactQuestionResponses_1'.
Cannot insert duplicate key in object 'dbo.FactQuestionResponses'. The duplicate key value is (1450734, 3521P3594).

 Violation of PRIMARY KEY constraint 'PK_FactQuestionResponses_1'. 
 Cannot insert duplicate key in object 'dbo.FactQuestionResponses'. The duplicate key value is (1450833, 3519P3801).
*/

USE PRV_BritishCouncil_SurpassDataWarehouse


SELECT FQR.* 
, fes.*
FROM FactExamSessions FES
INNER JOIN FactQuestionResponses FQR
ON FES.ExamSessionKey = FQR.ExamSessionKey
where FQR.ExamSessionKey = 1450833
	--and CPID = '3521P3594'


SELECT FQR.*
, fes.* 
FROM ETL.stg_FactExamSessions FES
INNER JOIN ETL.stg_FactQuestionResponses FQR
on FES.ExamSessionKey = FQR.ExamSessionKey
where fes.ExamSessionKey = 1450833
	--and CPID = '3521P3594'

select  WAREHOUSEExamSessionID, ItemID, ItemVersion, ItemResponseData, MarkerResponseData, ItemMark, MarkingIgnored, DerivedResponse, ShortDerivedResponse, resultData, warehouseTime
from PRV_BritishCouncil_SecureAssess..WAREHOUSE_ExamSessionTable WEST
LEFT JOIN PRV_BritishCouncil_SecureAssess..WAREHOUSE_ExamSessionItemResponseTable WESIRT
on WESIRT.WAREHOUSEExamSessionID = west.ID
where west.id = 1450833



SELECT *
FROM PRV_BritishCouncil_ItemBank.[dbo].[ItemTable]
where ProjectID = 3521
	and itemref = 3594




Select * from etl.ExecutionLog
























 --USE PRV_BritishCouncil_SecureAssess

select Keycode
from PRV_BritishCouncil_SecureAssess.dbo.WAREHOUSE_ExamSessionTable_Shreded
where centrename = 'Aptis Control'
	and qualificationName = 'UAT April 2016'
	and examName = 'Listening'
	and warehouseTime > '22/03/2016'
	and (
		KeyCode not in (select KeyCode from PRV_BritishCouncil_SurpassDataWarehouse.dbo.FactExamSessions)
		or
		KeyCode not in (select KeyCode from PRV_BritishCouncil_SurpassDataWarehouse.etl.stg_FactExamSessions)
		)

