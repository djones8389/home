--USE [BritishCouncil_SecureAssess]

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

IF OBJECT_ID('tempdb..#test') IS NOT NULL DROP TABLE #test;

SELECT --TOP (8000)
	DISTINCT
	 wesdt.ID [DocID]
	, wests.Country
    , wests.Section
	, wests.examVersionName		
	, wesdt.itemId
	, wesirt.ItemMark
	--, tpm.CEFR	
	, wests.examSessionId
	
INTO #test
	--SUM(CAST(DATALENGTH(wesdt.Document) AS BIGINT))/1024/1024 [MB]
FROM (

	SELECT
		examSessionId
		,examVersionName
		,KeyCode
		,country
		, userMark		
		, section.item.value('@id','nvarchar(20)') Item
		, exam.section.value('@id','tinyint') Section
	FROM WAREHOUSE_ExamSessionTable_Shreded 

		cross apply resultData.nodes('exam/section') exam(section)
		cross apply exam.section.nodes('item') section(item)
		
	WHERE qualificationName = 'International Comparisons Research'
		AND examName = 'Speaking'
		and previousExamState != 10
 ) wests
INNER JOIN [WAREHOUSE_ExamSessionDocumentTable] wesdt
ON wesdt.warehouseExamSessionID = wests.examSessionId
	and substring(wesdt.itemId, 0, charindex('S',wesdt.itemId)) = wests.Item
LEFT JOIN dbo.WAREHOUSE_ExamSessionItemResponseTable wesirt
ON wesirt.WAREHOUSEExamSessionID = wesdt.warehouseExamSessionID
	AND wesirt.ItemID = SUBSTRING(wesdt.itemId, 0, CHARINDEX('S',wesdt.itemId))
--LEFT JOIN [BritishCouncil_TestPackage]..[CEFR] TPM
--on TPM.Keycode = wests.KeyCode collate SQL_Latin1_General_CP1_CI_AS

--SELECT SUM(cast(DATALENGTH(b.document) as bigint))/1024/1024/1024 [GB]
--from #test a
--inner join WAREHOUSE_ExamSessionDocumentTable b (READUNCOMMITTED)
--on a.DocID = b.ID



DECLARE @FilePath NVARCHAR(200) = '\\430091-bc-file\contentshare\';
DECLARE @Servername sysname = (SELECT @@SERVERNAME)


SELECT distinct
	'bcp "select document from [britishcouncil_secureassess].dbo.[WAREHOUSE_ExamSessionDocumentTable] where id = ' +CAST([DocID] AS NVARCHAR(8)) +'" queryout "' + @FilePath 
	+ 'International Comparisons Research - Speaking2\' + a.Country + '_T' +
		 CAST(Section AS NVARCHAR(8)) + '_' + a.examVersionName +  '_' + CAST(ItemMark AS NVARCHAR(8)) + '_'  + '_' + CAST([DocID] AS NVARCHAR(8))
			+ '.mp3" -T -c -S ' + @Servername collate SQL_Latin1_General_CP1_CI_AS
			--,ItemMark
			, DATALENGTH(b.Document)
FROM #test a
inner join WAREHOUSE_ExamSessionDocumentTable b (READUNCOMMITTED)
on a.DocID = b.ID
order by 2 desc;


SELECT
	keycode
	, b.itemId
	, DATALENGTH(b.Document)
	, warehouseTime
	, b.id
FROM #test a
inner join WAREHOUSE_ExamSessionDocumentTable b (READUNCOMMITTED)
on b.id = a.DocID
inner join WAREHOUSE_ExamSessionTable c
on c.id = b.warehouseExamSessionID

order by warehousetime desc