USE [Ocr_SurpassManagement];
 
SELECT distinct u.UserName  
	  , c.Name [Centre]
	  , s.Name [Subject]	  
	  , r.Name [Role]
	  ,[Timestamp] [DeletedTime]
  FROM [Ocr_SurpassManagement].[dbo].[UserCentreSubjectRoles_DeleteAudit] A (READUNCOMMITTED)
  left join Users u
  on u.id = a.userid
  left join Centres c
  on c.id = CentreId
  left join roles r
  on r.id = RoleId
  left join Subjects s
  on s.id = a.subjectid

  where Timestamp > '2017-09-15 09:59:01.880' --'2017-08-31 15:16:22.183' --'2017-08-31 15:03:20.060' --'2017-08-31 14:55:31.001'
	order by Timestamp asc;

/*


USE [Ocr_SurpassManagement];
 
SELECT u.UserName  
	  , c.Name [Centre]
	  , s.Name [Subject]	  
	  , r.Name [Role]
	  ,[Timestamp] [DeletedTime]
  FROM [Ocr_SurpassManagement].[dbo].[UserCentreSubjectRoles_DeleteAudit] A (READUNCOMMITTED)
  left join Users u
  on u.id = a.userid
  left join Centres c
  on c.id = CentreId
  left join roles r
  on r.id = RoleId
  left join Subjects s
  on s.id = a.subjectid

  where Timestamp BETWEEN  '2017-08-31 15:27:29.550' AND
  
   --'2017-08-31 15:16:22.183'   
   --'2017-08-31 15:27:29.550'  
   --'2017-08-31 15:03:20.060'  
   --'2017-08-31 14:55:31.001'
	order by Timestamp asc;

	*/