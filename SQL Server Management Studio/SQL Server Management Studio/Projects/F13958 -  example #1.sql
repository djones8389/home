DECLARE @subject nvarchar(3)
	,@datefrom nvarchar(19)
	,@dateto nvarchar(19)
	,@folder nvarchar(2)
	,@status nvarchar(2)

Set @subject= (	
	select QualificationKey
	from DimQualifications
	where QualificationName = 'The Association of Taxation Technicians'
	)
Set @datefrom=N'1998-03-29 00:00:00'
Set @dateto=N'2018-03-31 00:00:00'
Set @folder=N'-2'
Set @status=N'-2'


IF @folder = -2 SET @folder = NULL
IF @status = -2 SET @status = NULL


IF OBJECT_ID('tempdb..#Items') IS NOT NULL DROP TABLE #Items
CREATE TABLE #Items(
	CPID nvarchar(50)
	,CPVersion int
	,ItemsetId int
	,ItemsetName nvarchar(100)

)

;WITH folders AS (
	SELECT ExternalId
	FROM DimQuestions  DQ
	WHERE DQ.ExternalId = @folder
	
	UNION ALL
	
	SELECT DQ.ExternalId
	FROM DimQuestions  DQ
		JOIN folders F ON F.ExternalId = DQ.ItemParentKey
	WHERE 
		(DQ.CAQuestionTypeKey=1 OR DQ.CAQuestionTypeKey=26)
		AND DQ.Source=2
),Items AS (
	SELECT 
		DQ.CPID
		,DQ.CPVersion
		,CASE WHEN DQP.CAQuestionTypeKey = 26 THEN DQP.ItemKey ELSE NULL END ItemsetId
		,CASE WHEN DQP.CAQuestionTypeKey = 26 THEN DQP.QuestionName ELSE NULL END ItemsetName
	FROM 
		DimQuestions  DQ
		JOIN DimProjects DP ON DP.ProjectKey = DQ.ProjectKey
		JOIN DimWorkflowStatuses DWS ON DQ.WorkflowStatusKey = DWS.WorkflowStatusKey
		LEFT JOIN DimQuestions DQP ON DQ.ItemParentKey = DQP.ItemKey AND DQP.CPVersion = DQP.LatestVersion AND DQP.ProjectKey = DQ.ProjectKey
	WHERE
		(DP.QualificationKey = @subject)
		AND (DQ.CAQuestionTypeKey<>1) AND (DQ.CAQuestionTypeKey<>26)
		AND DQ.Deleted<>1
		AND (DWS.ExternalId = @status OR @status IS NULL)
		AND DQ.CPVersion = DQ.LatestVersion
		AND DQ.LastModifiedDate BETWEEN @datefrom AND @dateto
		AND DQ.Source=2
		AND (
			EXISTS (SELECT * FROM folders F WHERE F.ExternalId = DQ.ItemParentKey) --subfolders
			OR @folder IS NULL -- all items
			OR (DQ.ItemParentKey = 0 AND @folder = 0) --not in folder
		)
)
INSERT INTO #Items
SELECT * FROM Items


SELECT 
		DQM.CPID
		,attrib
		,attribType
		,attribval
		,DQM.externalId
		,deleted
	FROM 
		#Items I
		JOIN DimQuestionMetaData DQM 
			ON I.CPID = DQM.CPID 
			AND I.CPVersion = DQM.CPVersion --Show tags only for latest item version
	WHERE 
		attribType IS NOT NULL

