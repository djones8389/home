select t.name
	, c.name
	, type_name(c.system_type_id) [type]
from sys.columns c
inner join sys.tables t
on t.object_id = c.object_id
where t.name in ('WAREHOUSE_ExamSessionAvailableItemsTable','WAREHOUSE_ExamSessionItemResponseTable','WAREHOUSE_ExamSessionTable_ShrededItems','WAREHOUSE_ExamSessionTable_ShreddedItems_Mark')
	--and type_name(c.system_type_id) not in ('int','tinyint','bit', 'decimal')
order by 1;