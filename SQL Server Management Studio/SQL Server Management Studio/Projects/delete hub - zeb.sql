use CMS

DECLARE @HubID tinyint = 4;

DELETE FROM ServersAudit
where [DatabaseServerId] = (
	select ID
	FROM [CMS].[dbo].[DatabaseServers] where HubId = @HubID
);

DELETE FROM ClientPasswords
where clientid in (
	select Id from Clients
	where [DatabaseServerId] = (
		select ID
		FROM [CMS].[dbo].[DatabaseServers] where HubId = @HubID
	)
);

DELETE FROM FeatureClientParameters
where featureclientid in (
	select Id
	from FeatureClients
	where clientid in (
		select Id from Clients
		where [DatabaseServerId] = (
			select ID
			FROM [CMS].[dbo].[DatabaseServers] where HubId = @HubID
		)
	)
);

DELETE FROM  FeatureClients
where clientid in (
	select Id from Clients
	where [DatabaseServerId] = (
		select ID
		FROM [CMS].[dbo].[DatabaseServers] where HubId = @HubID
	)
);

DELETE FROM DatabasePasswords WHERE   ClientId IN (
	 select Id from Clients
	 where [DatabaseServerId] = (
		select ID
		FROM [CMS].[dbo].[DatabaseServers] where HubId = @HubID
	) 
);

DELETE FROM DeploymentStatuses
where DeploymentId IN (
	select id from Deployments
	where ClientID IN (
		 select Id from Clients
		 where [DatabaseServerId] = (
			select ID
			FROM [CMS].[dbo].[DatabaseServers] where HubId = @HubID
		)
	) 
);

DELETE FROM Deployments
where ClientID IN (
	 select Id from Clients
	 where [DatabaseServerId] = (
		select ID
		FROM [CMS].[dbo].[DatabaseServers] where HubId = @HubID
	) 
)

DELETE FROM Clients
where [DatabaseServerId] = (
	select ID
	FROM [CMS].[dbo].[DatabaseServers] where HubId = @HubID
)


DELETE FROM JobStatistics
where [ServerId] = (
	select ID
	FROM [CMS].[dbo].[DatabaseServers] where HubId = @HubID
)


DELETE FROM [CMS].[dbo].[DatabaseServers] where HubId = @HubID


