USE cms

SELECT c.Name [Client]
	, a.Name [Application]
	, d.createddate
	, case cast(d.deploymenttype as varchar(2))
		when cast(2 as varchar(2)) then 'Update'
		when cast(13 as varchar(2)) then 'Clone'
		when cast(9 as varchar(2)) then 'Apply Features'
		else cast(DeploymentType as varchar(2)) 
		end as DeploymentType
	, d.Error
	, V.Version
	, V.IntVersion
from Clients C
inner join Deployments D
on D.ClientId = C.Id
inner join DeploymentsVersions DV
on DV.DeploymentId = D.Id
inner join Versions V
on V.Id = dv.VersionId
inner join Applications A
on A.Id = v.ApplicationId
where c.url in (
	select substring(name, 0, charindex('_',name))
	from sys.databases
	where substring(name, 0, charindex('_',name)) != ''
	group by substring(name, 0, charindex('_',name))
	having count(*)>4
)
	and createdDate > DATEADD(WEEK, -2, GETDATE())
	order by client, Application