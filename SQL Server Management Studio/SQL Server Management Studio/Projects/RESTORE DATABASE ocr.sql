RESTORE DATABASE [Ocr_ContentAuthor]
FROM DISK = N'D:\Backup\OCR R12.5.2017.06.23.bak'
WITH FILE = 1
	,NOUNLOAD
	,NOREWIND
	,REPLACE
	,STATS = 10
	,MOVE 'OCR_DRY_ContentAuthor' TO N'E:\Data\2008R2\Ocr_ContentAuthor.mdf'
	,MOVE 'OCR_DRY_ContentAuthor_log' TO N'E:\Log\2008R2\Ocr_ContentAuthor.ldf';

RESTORE DATABASE [Ocr_SurpassDataWarehouse]
FROM DISK = N'D:\Backup\OCR R12.5.2017.06.23.bak'
WITH FILE = 2
	,NOUNLOAD
	,NOREWIND
	,REPLACE
	,STATS = 10
	,MOVE 'OCR_DRY_SurpassDataWarehouse' TO N'E:\Data\2008R2\Ocr_SurpassDataWarehouse.mdf'
	,MOVE 'OCR_DRY_SurpassDataWarehouse_log' TO N'E:\Log\2008R2\Ocr_SurpassDataWarehouse.ldf';

RESTORE DATABASE [Ocr_SurpassManagement]
FROM DISK = N'D:\Backup\OCR R12.5.2017.06.23.bak'
WITH FILE = 3
	,NOUNLOAD
	,NOREWIND
	,REPLACE
	,STATS = 10
	,MOVE 'OCR_DRY_SurpassManagement' TO N'E:\Data\2008R2\Ocr_SurpassManagement.mdf'
	,MOVE 'OCR_DRY_SurpassManagement_log' TO N'E:\Log\2008R2\Ocr_SurpassManagement.ldf';

RESTORE DATABASE [Ocr_AnalyticsManagement]
FROM DISK = N'D:\Backup\OCR R12.5.2017.06.23.bak'
WITH FILE = 4
	,NOUNLOAD
	,NOREWIND
	,REPLACE
	,STATS = 10
	,MOVE 'ocrdryrun' TO N'E:\Data\2008R2\Ocr_AnalyticsManagement.mdf'
	,MOVE 'ocrdryrun_log' TO N'E:\Log\2008R2\Ocr_AnalyticsManagement.ldf';