use SandboxWelshGov_ContentAuthor

--DROP TABLE IF EXISTS tempdb..#fv;
DROP TABLE IF EXISTS tempdb..#lnf;
DROP TABLE IF EXISTS tempdb..#BeforePublish;

CREATE TABLE #lnf (

	itemname nvarchar(100)
	, itemid nvarchar(100)
	, LNFYear2 varchar(20)
	, LNFYear3 varchar(20)
	, LNFYear4 varchar(20)
	, LNFYear5 varchar(20)
	, LNFYear6 varchar(20)
	, LNFYear7 varchar(20)
	, LNFYear8 varchar(20)
	, LNFYear9 varchar(20)
);


BULK INSERT #lnf  
FROM 'F:\WelshGov\LNF - copy.csv'
WITH (fieldterminator=',',rowterminator='\n');

DELETE FROM #lnf WHERE ISNUMERIC(itemID) = 0;

DELETE stv
FROM  Subjects s 
INNER JOIN Items i on  I.SubjectId = s.id		
LEFT JOIN  SubjectTagValueItems stvi on stvi.Item_Id = i.Id
LEFT JOIN SubjectTagValues stv on stv.id = stvi.SubjectTagValue_Id
LEFT JOIN SubjectTagTypes stt on stt.Id = stv.SubjectTagTypeId
where TagTypeName in ('lnf');

DELETE
FROM SubjectTagTypes
WHERE TagTypeName in ('LNF');

INSERT SubjectTagTypes
VALUES(1,1,4,4,'LNF',0,getdate(),NULL,0)

DECLARE @Scope int = (SELECT SCOPE_IDENTITY());

INSERT SubjectTagCategoryGroups([CategoryId], [GroupId])
VALUES (3,@Scope) 

INSERT SubjectTagValues([SubjectTagTypeId], [Text], [Deleted])
SELECT C.*
FROM (
	SELECT @Scope as [SubjectTagTypeId], SUBSTRING(a.LNF, 0, CHARINDEX('|', lnf)) as [Text], 0 as [Deleted]
	FROM (
	SELECT itemid
			,  COALESCE(b.LNFYear2, b.LNFYear3,b.LNFYear4,b.LNFYear5,b.LNFYear6,b.LNFYear7,b.LNFYear8,b.LNFYear9) LNF
	FROM #lnf b
	) a

	UNION

	SELECT @Scope as [SubjectTagTypeId], ltrim(SUBSTRING(a.LNF, CHARINDEX('|', lnf)+1, 10)) as [Text], 0 as [Deleted]
	FROM (
	SELECT itemid
			,  COALESCE(b.LNFYear2, b.LNFYear3,b.LNFYear4,b.LNFYear5,b.LNFYear6,b.LNFYear7,b.LNFYear8,b.LNFYear9) LNF
	FROM #lnf b
	) a
) C
WHERE [Text] is not null and [Text] <> '';



INSERT SubjectTagValueItems([SubjectTagValue_Id], [Item_Id])
SELECT DISTINCT stv.Id
	, itemid
FROM (

	SELECT cast(itemid as float) itemid
		, SUBSTRING(a.LNF, 0, CHARINDEX('|', lnf)) as [Text], 0 as [Deleted]
	FROM (
	SELECT itemid
			,  COALESCE(b.LNFYear2, b.LNFYear3,b.LNFYear4,b.LNFYear5,b.LNFYear6,b.LNFYear7,b.LNFYear8,b.LNFYear9) LNF
	FROM #lnf b
	) a
	where  SUBSTRING(a.LNF, 0, CHARINDEX('|', lnf)) <> ''

	UNION

	SELECT cast(itemid as float) itemid
		, ltrim(SUBSTRING(a.LNF, CHARINDEX('|', lnf)+1, 10)) as [Text], 0 as [Deleted]
	FROM (
	SELECT itemid
			,  COALESCE(b.LNFYear2, b.LNFYear3,b.LNFYear4,b.LNFYear5,b.LNFYear6,b.LNFYear7,b.LNFYear8,b.LNFYear9) LNF
	FROM #lnf b
	) a

) lnf
LEFT JOIN SubjectTagValues STV
ON stv.Text collate SQL_Latin1_General_CP1_CI_AS = lnf.Text
LEFT JOIN SubjectTagTypes stt 
ON stt.Id = stv.SubjectTagTypeId
LEFT JOIN  SubjectTagValueItems stvi 
ON stvi.Item_Id = lnf.itemid
WHERE TagTypeName in ('LNF');












--;with cte as (
--	select cast(itemid as float) itemid
--		, SUBSTRING(a.LNF, 0, CHARINDEX('|', lnf)) as [Text], 0 as [Deleted]
--	from (
--	SELECT itemid
--			,  COALESCE(b.LNFYear2, b.LNFYear3,b.LNFYear4,b.LNFYear5,b.LNFYear6,b.LNFYear7,b.LNFYear8,b.LNFYear9) LNF
--	FROM #lnf b
--	) a
--	where  SUBSTRING(a.LNF, 0, CHARINDEX('|', lnf)) <> ''

--	UNION

--	select cast(itemid as float) itemid
--		, ltrim(SUBSTRING(a.LNF, CHARINDEX('|', lnf)+1, 10)) as [Text], 0 as [Deleted]
--	from (
--	SELECT itemid
--			,  COALESCE(b.LNFYear2, b.LNFYear3,b.LNFYear4,b.LNFYear5,b.LNFYear6,b.LNFYear7,b.LNFYear8,b.LNFYear9) LNF
--	FROM #lnf b
--	) a
--	order by itemid, len(text)
--)
--select *
--from cte
--order by itemid, len(text)
/*


select itemid
	, SUBSTRING(a.LNF, 0, CHARINDEX('|', lnf)) as [Text], 0 as [Deleted]
from (
SELECT itemid
		,  COALESCE(b.LNFYear2, b.LNFYear3,b.LNFYear4,b.LNFYear5,b.LNFYear6,b.LNFYear7,b.LNFYear8,b.LNFYear9) LNF
FROM #lnf b
) a
where  SUBSTRING(a.LNF, 0, CHARINDEX('|', lnf)) <> ''

UNION

select itemid
	, ltrim(SUBSTRING(a.LNF, CHARINDEX('|', lnf)+1, 10)) as [Text], 0 as [Deleted]
from (
SELECT itemid
		,  COALESCE(b.LNFYear2, b.LNFYear3,b.LNFYear4,b.LNFYear5,b.LNFYear6,b.LNFYear7,b.LNFYear8,b.LNFYear9) LNF
FROM #lnf b
) a



select * from SubjectTagValueItems
	
select stv.*
	--, stt.*
	--, stvi.*
FROM  Subjects s 
inner join Items i on  I.SubjectId = s.id		
LEFT JOIN  SubjectTagValueItems stvi on stvi.Item_Id = i.Id
LEFT JOIN SubjectTagValues stv on stv.id = stvi.SubjectTagValue_Id
LEFT JOIN SubjectTagTypes stt on stt.Id = stv.SubjectTagTypeId
where i.id = 10840

select * 
from SubjectTagValues STV
order by 1 desc

select * 
from SubjectTagTypes
where TagTypeName in ('LNF')


SELECT itemid
	,  COALESCE(b.LNFYear2, b.LNFYear3,b.LNFYear4,b.LNFYear5,b.LNFYear6,b.LNFYear7,b.LNFYear8,b.LNFYear9) LNF
	, stv.*
	, stt.*
FROM #lnf b
inner join items i on i.Id = b.itemid
LEFT JOIN  SubjectTagValueItems stvi on stvi.Item_Id = i.Id
LEFT JOIN SubjectTagValues stv on stv.id = stvi.SubjectTagValue_Id
LEFT JOIN SubjectTagTypes stt on stt.Id = stv.SubjectTagTypeId 
where itemid = '10840'
	
select *
from SubjectTagTypes
where TagTypeName in ('LNF')


SELECT itemid
	,  COALESCE(b.LNFYear2, b.LNFYear3,b.LNFYear4,b.LNFYear5,b.LNFYear6,b.LNFYear7,b.LNFYear8,b.LNFYear9) LNF
FROM #lnf b
where itemid = '10840'

--9269	85	hello	0

	--22602
	--10840


	select distinct itemid
		, itemname
		, COALESCE(b.LNFYear2, b.LNFYear3,b.LNFYear4,b.LNFYear5,b.LNFYear6,b.LNFYear7,b.LNFYear8,b.LNFYear9)
		, stvi.*
	from SubjectTagValues a
	inner join #lnf b
	on a.Text collate SQL_Latin1_General_CP1_CI_AS = COALESCE(b.LNFYear2, b.LNFYear3,b.LNFYear4,b.LNFYear5,b.LNFYear6,b.LNFYear7,b.LNFYear8,b.LNFYear9)
	inner join SubjectTagValueItems stvi
	on stvi.Item_Id = b.itemid
	inner join SubjectTagValues STV
	on stv.Text collate SQL_Latin1_General_CP1_CI_AS = COALESCE(b.LNFYear2, b.LNFYear3,b.LNFYear4,b.LNFYear5,b.LNFYear6,b.LNFYear7,b.LNFYear8,b.LNFYear9)
	inner join SubjectTagTypes stt 
	on stt.Id = stv.SubjectTagTypeId
	where COALESCE(b.LNFYear2, b.LNFYear3,b.LNFYear4,b.LNFYear5,b.LNFYear6,b.LNFYear7,b.LNFYear8,b.LNFYear9)= '96ii'
		and itemid = '16213'
		and TagTypeName in ('LNF')
*/