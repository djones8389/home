use master;

declare @dynamic nvarchar(MAX) = '';

select @dynamic += CHAR(13) + 'kill ' + convert(varchar(5),spid)
from sys.sysprocesses s
where dbid > 4
	and  db_name(dbid) = 'SandboxWelshGov_ContentAuthor'
EXEC (@dynamic)
GO

RESTORE DATABASE [SandboxWelshGov_ContentAuthor]
FROM DISK = N'F:\Backup\Asesu.ALL.2018.04.05.bak'
WITH FILE = 2
	,NOUNLOAD
	,NOREWIND
	,REPLACE
	,STATS = 10
	,MOVE 'Data' TO N'F:\Data\SandboxWelshGov_ContentAuthor.mdf'
	,MOVE 'Log' TO N'F:\Log\SandboxWelshGov_ContentAuthor.ldf';


--declare @dynamic nvarchar(MAX) = '';

--select @dynamic += CHAR(13) + 'kill ' + convert(varchar(5),spid)
--from sys.sysprocesses s
--where dbid > 4
--	and  db_name(dbid) = 'SandboxWelshGov_AnalyticsManagement'
--EXEC (@dynamic)
--GO

--RESTORE DATABASE [SandboxWelshGov_AnalyticsManagement]
--FROM DISK = N'F:\Backup\Asesu.ALL.2018.04.05.bak'
--WITH FILE = 1
--	,NOUNLOAD
--	,NOREWIND
--	,REPLACE
--	,STATS = 10
--	,MOVE 'Data' TO N'F:\Data\SandboxWelshGov_AnalyticsManagement.mdf'
--	,MOVE 'Log' TO N'F:\Log\SandboxWelshGov_AnalyticsManagement.ldf';

--declare @dynamic nvarchar(MAX) = '';

--select @dynamic += CHAR(13) + 'kill ' + convert(varchar(5),spid)
--from sys.sysprocesses s
--where dbid > 4
--	and  db_name(dbid) = 'SandboxWelshGov_ItemBank'
--EXEC (@dynamic)
--GO

--RESTORE DATABASE [SandboxWelshGov_ItemBank]
--FROM DISK = N'F:\Backup\Asesu.ALL.2018.04.05.bak'
--WITH FILE = 3
--	,NOUNLOAD
--	,NOREWIND
--	,REPLACE
--	,STATS = 10
--	,MOVE 'Data' TO N'F:\Data\SandboxWelshGov_ItemBank.mdf'
--	,MOVE 'Log' TO N'F:\Log\SandboxWelshGov_ItemBank.ldf';

--declare @dynamic nvarchar(MAX) = '';

--select @dynamic += CHAR(13) + 'kill ' + convert(varchar(5),spid)
--from sys.sysprocesses s
--where dbid > 4
--	and  db_name(dbid) = 'SandboxWelshGov_SecureAssess'
--EXEC (@dynamic)
--GO

--RESTORE DATABASE [SandboxWelshGov_SecureAssess]
--FROM DISK = N'F:\Backup\Asesu.ALL.2018.04.05.bak'
--WITH FILE = 4
--	,NOUNLOAD
--	,NOREWIND
--	,REPLACE
--	,STATS = 10
--	,MOVE 'Data' TO N'F:\Data\SandboxWelshGov_SecureAssess.mdf'
--	,MOVE 'Log' TO N'F:\Log\SandboxWelshGov_SecureAssess.ldf';

--declare @dynamic nvarchar(MAX) = '';

--select @dynamic += CHAR(13) + 'kill ' + convert(varchar(5),spid)
--from sys.sysprocesses s
--where dbid > 4
--	and  db_name(dbid) = 'SandboxWelshGov_SurpassDataWarehouse'
--EXEC (@dynamic)
--GO

--RESTORE DATABASE [SandboxWelshGov_SurpassDataWarehouse]
--FROM DISK = N'F:\Backup\Asesu.ALL.2018.04.05.bak'
--WITH FILE = 5
--	,NOUNLOAD
--	,NOREWIND
--	,REPLACE
--	,STATS = 10
--	,MOVE 'Data' TO N'F:\Data\SandboxWelshGov_SurpassDataWarehouse.mdf'
--	,MOVE 'Log' TO N'F:\Log\SandboxWelshGov_SurpassDataWarehouse.ldf';

--declare @dynamic nvarchar(MAX) = '';

--select @dynamic += CHAR(13) + 'kill ' + convert(varchar(5),spid)
--from sys.sysprocesses s
--where dbid > 4
--	and  db_name(dbid) = 'SandboxWelshGov_SurpassManagement'
--EXEC (@dynamic)
--GO

--RESTORE DATABASE [SandboxWelshGov_SurpassManagement]
--FROM DISK = N'F:\Backup\Asesu.ALL.2018.04.05.bak'
--WITH FILE = 6
--	,NOUNLOAD
--	,NOREWIND
--	,REPLACE
--	,STATS = 10
--	,MOVE 'Data' TO N'F:\Data\SandboxWelshGov_SurpassManagement.mdf'
--	,MOVE 'Log' TO N'F:\Log\SandboxWelshGov_SurpassManagement.ldf';
--GO

--USE [SandboxWelshGov_SecureAssess]; IF NOT EXISTS (SELECT 1 FROM sys.database_principals where name = 'SandboxWelshGov_SecureAssessUser') CREATE USER [SandboxWelshGov_SecureAssessUser]FOR LOGIN [SandboxWelshGov_SecureAssessUser]; exec sp_addrolemember'db_owner', 'SandboxWelshGov_SecureAssessUser' 
--USE [SandboxWelshGov_SecureAssess]; IF NOT EXISTS (SELECT 1 FROM sys.database_principals where name = 'SecureAssessUser') CREATE USER [SecureAssessUser]FOR LOGIN [SecureAssessUser]; exec sp_addrolemember'db_owner', 'SecureAssessUser' 
--USE [SandboxWelshGov_SecureAssess]; IF NOT EXISTS (SELECT 1 FROM sys.database_principals where name = 'IntegrationUser') CREATE USER [IntegrationUser]FOR LOGIN [IntegrationUser]; exec sp_addrolemember'db_owner', 'IntegrationUser' 
--USE [SandboxWelshGov_SecureAssess]; IF NOT EXISTS (SELECT 1 FROM sys.database_principals where name = 'SandboxWelshGov_ETLUser') CREATE USER [SandboxWelshGov_ETLUser]FOR LOGIN [SandboxWelshGov_ETLUser]; exec sp_addrolemember'db_datareader', 'SandboxWelshGov_ETLUser' 
--USE [SandboxWelshGov_ItemBank]; IF NOT EXISTS (SELECT 1 FROM sys.database_principals where name = 'SandboxWelshGov_ETLUser') CREATE USER [SandboxWelshGov_ETLUser]FOR LOGIN [SandboxWelshGov_ETLUser]; exec sp_addrolemember'ETLRole', 'SandboxWelshGov_ETLUser' 
--USE [SandboxWelshGov_ItemBank]; IF NOT EXISTS (SELECT 1 FROM sys.database_principals where name = 'SandboxWelshGov_ItemBankUser') CREATE USER [SandboxWelshGov_ItemBankUser]FOR LOGIN [SandboxWelshGov_ItemBankUser]; exec sp_addrolemember'db_owner', 'SandboxWelshGov_ItemBankUser' 
--USE [SandboxWelshGov_ItemBank]; IF NOT EXISTS (SELECT 1 FROM sys.database_principals where name = 'ItemBank') CREATE USER [ItemBank]FOR LOGIN [ItemBank]; exec sp_addrolemember'db_owner', 'ItemBank' 
USE [SandboxWelshGov_ContentAuthor]; IF NOT EXISTS (SELECT 1 FROM sys.database_principals where name = 'SandboxWelshGov_ContentAuthorUser') CREATE USER [SandboxWelshGov_ContentAuthorUser]FOR LOGIN [SandboxWelshGov_ContentAuthorUser]; exec sp_addrolemember'db_owner', 'SandboxWelshGov_ContentAuthorUser' 
USE [SandboxWelshGov_ContentAuthor]; IF NOT EXISTS (SELECT 1 FROM sys.database_principals where name = 'SandboxWelshGov_ETLUser') CREATE USER [SandboxWelshGov_ETLUser]FOR LOGIN [SandboxWelshGov_ETLUser]; exec sp_addrolemember'db_datareader', 'SandboxWelshGov_ETLUser' 
--USE [SandboxWelshGov_SurpassManagement]; IF NOT EXISTS (SELECT 1 FROM sys.database_principals where name = 'SandboxWelshGov_SurpassManagementUser') CREATE USER [SandboxWelshGov_SurpassManagementUser]FOR LOGIN [SandboxWelshGov_SurpassManagementUser]; exec sp_addrolemember'db_owner', 'SandboxWelshGov_SurpassManagementUser' 
--USE [SandboxWelshGov_SurpassManagement]; IF NOT EXISTS (SELECT 1 FROM sys.database_principals where name = 'SandboxWelshGov_ETLUser') CREATE USER [SandboxWelshGov_ETLUser]FOR LOGIN [SandboxWelshGov_ETLUser]; exec sp_addrolemember'db_datareader', 'SandboxWelshGov_ETLUser' 
--USE [SandboxWelshGov_AnalyticsManagement]; IF NOT EXISTS (SELECT 1 FROM sys.database_principals where name = 'SandboxWelshGov_AnalyticsManagementUser') CREATE USER [SandboxWelshGov_AnalyticsManagementUser]FOR LOGIN [SandboxWelshGov_AnalyticsManagementUser]; exec sp_addrolemember'db_owner', 'SandboxWelshGov_AnalyticsManagementUser' 
--USE [SandboxWelshGov_SurpassDataWarehouse]; IF NOT EXISTS (SELECT 1 FROM sys.database_principals where name = 'SandboxWelshGov_ETLUser') CREATE USER [SandboxWelshGov_ETLUser]FOR LOGIN [SandboxWelshGov_ETLUser]; exec sp_addrolemember'db_owner', 'SandboxWelshGov_ETLUser' 
--GO

--USE [SandboxWelshGov_AnalyticsManagement]DROP USER [Asesu_AnalyticsManagementUser];
--USE [SandboxWelshGov_ContentAuthor]DROP USER [Asesu_ContentAuthorUser];
--USE [SandboxWelshGov_ContentAuthor]DROP USER [Asesu_ETLUser];
--USE [SandboxWelshGov_ItemBank]DROP USER [Asesu_ETLUser];
--USE [SandboxWelshGov_ItemBank]DROP USER [Asesu_ItemBankUser];
--USE [SandboxWelshGov_SecureAssess]DROP USER [Asesu_ETLUser];
--USE [SandboxWelshGov_SecureAssess]DROP USER [Asesu_SecureAssessUser];
--USE [SandboxWelshGov_SurpassDataWarehouse]DROP USER [Asesu_ETLUser];
--USE [SandboxWelshGov_SurpassManagement]DROP USER [Asesu_ETLUser];
--USE [SandboxWelshGov_SurpassManagement]DROP USER [Asesu_SurpassManagementUser];USE [SandboxWelshGov_AnalyticsManagement]DROP USER [Asesu_AnalyticsManagementUser];
USE [SandboxWelshGov_ContentAuthor]DROP USER [Asesu_ContentAuthorUser];
USE [SandboxWelshGov_ContentAuthor]DROP USER [Asesu_ETLUser];
--USE [SandboxWelshGov_ItemBank]DROP USER [Asesu_ETLUser];
--USE [SandboxWelshGov_ItemBank]DROP USER [Asesu_ItemBankUser];
--USE [SandboxWelshGov_SecureAssess]DROP USER [Asesu_ETLUser];
--USE [SandboxWelshGov_SecureAssess]DROP USER [Asesu_SecureAssessUser];
--USE [SandboxWelshGov_SurpassDataWarehouse]DROP USER [Asesu_ETLUser];
--USE [SandboxWelshGov_SurpassManagement]DROP USER [Asesu_ETLUser];
--USE [SandboxWelshGov_SurpassManagement]DROP USER [Asesu_SurpassManagementUser];