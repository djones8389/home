IF OBJECT_ID('tempdb..#BCStats') IS NOT NULL DROP TABLE #BCStats;


CREATE TABLE #BCStats (
	server_name nvarchar(100)
	,database_name nvarchar(100)
	,schema_name nvarchar(100)
	,table_name nvarchar(100)
	,row_count nvarchar(100)
	,reserved_kb int
	,data_kb int
	,index_kb int
	,unused_kb int
	,collection_date datetime
) 

BULK INSERT #BCStats
from 'C:\BCData.csv'
with (fieldterminator = ',', rowterminator = '\n')
go


SELECT * 
FROM #BCStats 
where server_name = '"430088-BC-SQL\SQL1'
	and database_name in ('BRITISHCOUNCIL_SecureAssess', 'BritishCouncil_TestPackage')
	and table_name like 'Warehouse%'
	order by 1,2,4, collection_date


select 
	Date
	,SUM(totalkb)/1024/1024 GB
from (
SELECT
	SUBSTRING(convert(nvarchar(100),collection_date), 0, 7) as [Date]
	,SUM(data_kb) + SUM(index_kb) [Totalkb]
	, collection_date
FROM #BCStats 
where database_name in ('BRITISHCOUNCIL_SecureAssess', 'BritishCouncil_TestPackage')	
	and table_name not in ('WAREHOUSE_ExamSessionDocumentTable')
 group by collection_date
	
	) A
	group by Date




/*20,000 Audio*/

USE [PRV_BritishCouncil_SecureAssess];

SELECT SUM(convert(decimal(8,2),[DL-KB]))/1024 [DL-MB]
	, SUM(convert(decimal(8,2),[DL-KB]))/1024/1024 [DL-GB]
FROM (
select top 20000 [warehouseExamSessionID]
	, SUM(DATALENGTH([Document]))/1024 [DL-KB]
FROM [dbo].[WAREHOUSE_ExamSessionDocumentTable]
--where GETDATE() < DATEADD(MONTH, 3, uploadDate)
 group by [warehouseExamSessionID]
	order by  [warehouseExamSessionID] desc
 ) A


 --DECLARE @ESIDs TABLE (ESID INT)
 --INSERT @ESIDs(ESID)
 --select top 20000 [warehouseExamSessionID]
 --FROM [dbo].[WAREHOUSE_ExamSessionDocumentTable]
 --order by  [warehouseExamSessionID] desc