DECLARE @ESIDS TABLE (ESID INT)

INSERT @ESIDS 
select examSessionId
from WAREHOUSE_ExamSessionTable_Shreded
	where examName like '%Speaking%'
		and previousexamstate != 10


select b.Y [Year]
	,  b.M [Month]
	,  c.ExamCount
	,  b.MinDL
	,  convert(decimal(12,2),b.MaxDL)/1024/1024 [Max.DocumentSizePerExamInMB]
	,  convert(decimal(12,2),b.AvDL)/1024/1024 [Avg.DocumentSizePerExamInMB]
FROM (

select	
	 Y
	, M
	, MIN(DL) MinDL
	 , max(DL) MaxDL
	 , CONVERT(BIGINT,avg(DL)) AvDL
FROM (

	select	
		year(uploadDate) Y
		, month(uploaddate) M
		, CONVERT(BIGINT,SUM(DATALENGTH(Document))) DL
		,warehouseexamsessionid
	from WAREHOUSE_ExamSessionDocumentTable
	INNER JOIN @ESIDS F
	ON F.ESID = WAREHOUSE_ExamSessionDocumentTable.warehouseexamsessionid
	where DATALENGTH(DOCUMENT)>0
		group by 
			warehouseexamsessionid
			, year(uploadDate) 
			, month(uploaddate)
	) A

group by Y
		, M

) B

INNER JOIN (

	SELECT count(distinct warehouseexamsessionid) 'examCount'
		,year(uploadDate) Y
		, month(uploaddate) M
	FROM WAREHOUSE_ExamSessionDocumentTable
	INNER JOIN @ESIDS F
	ON F.ESID = WAREHOUSE_ExamSessionDocumentTable.warehouseexamsessionid
	 group by year(uploadDate) 
		, month(uploaddate)
) C

ON B.y = c.y
	and b.m = c.m

ORDER BY 1,2;





/*


select --count(id)
	 COUNT(distinct warehouseexamsessionid)
	--, uploaddate
FROM WAREHOUSE_ExamSessionDocumentTable
where month(uploadDate) = 7
		and year(uploadDate) = 2012


	SELECT count(warehouseexamsessionid) 'examCount'
		,year(uploadDate) Y
		, month(uploaddate) M
	FROM WAREHOUSE_ExamSessionDocumentTable
	 group by warehouseexamsessionid
		,year(uploadDate) 
		, month(uploaddate)
		order by 2,3


select 
		 CONVERT(BIGINT,SUM(DATALENGTH(Document))) [DataLength]
		, MIN(DATALENGTH(Document)) [Min. Doc Size Per Exam]
		, MAX(DATALENGTH(Document)) [Max. Doc Size Per Exam]
		, AVG(DATALENGTH(Document)) [Avg. Doc Size Per Exam]
		, MONTH(uploadDate) MonthOfUpload
		, YEAR(uploadDate) YearOfUpload
		, warehouseExamSessionID
	FROM WAREHOUSE_ExamSessionDocumentTable
	group by warehouseExamSessionID
	, MONTH(uploadDate)
		, YEAR(uploadDate)

order by YearOfUpload, MonthOfUpload,2

*/