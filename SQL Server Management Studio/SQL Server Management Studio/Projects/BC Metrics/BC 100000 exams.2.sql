--select
--	MONTH(uploadDate) MonthOfUpload
--	,YEAR(uploadDate) YearOfUpload
--	,count(ID) CountOfDocuments
--	,min(DATALENGTH(Document))
--	,MAX(DATALENGTH(Document))
--from WAREHOUSE_ExamSessionDocumentTable
--group by MONTH(uploadDate),YEAR(uploadDate)
--	order by 2, 1

--select top 100 uploadDate, ID, warehouseExamSessionID, itemId, documentName
--from WAREHOUSE_ExamSessionDocumentTable
--order by uploadDate asc;



SELECT 
	A.YearOfUpload
	, A.MonthOfUpload
	, CountOfDocs
	, B.[Min. Doc Size Per Exam]
	, b.[Max. Doc Size Per Exam]
	, b.[Avg. Doc Size Per Exam]
	--, CONVERT(BIGINT,SUM([DATALENGTH])) [SizeOfData]
	--, CONVERT(BIGINT,SUM([DATALENGTH]))
FROM (
select
	COUNT(ID) CountOfDocs
	,MONTH(uploadDate) MonthOfUpload
	,YEAR(uploadDate) YearOfUpload
from WAREHOUSE_ExamSessionDocumentTable
group by MONTH(uploadDate),YEAR(uploadDate)
)  A
	
INNER JOIN (
	select 
		 CONVERT(BIGINT,SUM(DATALENGTH(Document))) [DataLength]
		, MIN(DATALENGTH(Document)) [Min. Doc Size Per Exam]
		, MAX(DATALENGTH(Document)) [Max. Doc Size Per Exam]
		, AVG(DATALENGTH(Document)) [Avg. Doc Size Per Exam]
		, MONTH(uploadDate) MonthOfUpload
		, YEAR(uploadDate) YearOfUpload
		, warehouseExamSessionID
	FROM WAREHOUSE_ExamSessionDocumentTable
	group by warehouseExamSessionID
	, MONTH(uploadDate)
		, YEAR(uploadDate)
) B

ON A.MonthOfUpload = B.MonthOfUpload
	and A.YearOfUpload = B.YearOfUpload

GROUP BY 	A.YearOfUpload
			, A.MonthOfUpload
			, CountOfDocs
			, B.[Min. Doc Size Per Exam]
			, b.[Max. Doc Size Per Exam]
			, b.[Avg. Doc Size Per Exam]
	order by 1,2