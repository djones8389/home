use master

if OBJECT_ID('tempdb..#IBDynamicRules') is not null drop table #IBDynamicRules;

create table #IBDynamicRules (
	Instance sysname
	, Client sysname
	, qualificationName nvarchar(200)
	, AssessmentName nvarchar(200)
	, ExternalReference nvarchar(200)
	, RuleName nvarchar(500)
)

INSERT #IBDynamicRules
exec sp_msforeachdb '

use [?];

SET QUOTED_IDENTIFIER ON;

if (''?'' like ''%Itembank%'')

begin

	select  @@Servername [Instance]
		,''?'' [Client]
		, QT.qualificationName
		, at.AssessmentName
		, at.ExternalReference
		, a.b.value(''@Name'',''nvarchar(500)'') RuleName 
	from AssessmentTable at (READUNCOMMITTED)
	inner join assessmentgrouptable agt (READUNCOMMITTED) on agt.id = at.AssessmentGroupID
	inner join qualificationtable qt (READUNCOMMITTED) on qt.id = agt.qualificationid
	cross apply AssessmentRules.nodes(''PaperRules/Section[@Fixed=0]/Rule'') a(b)

end
'


SELECT top 100 *
	,LEN(rulename)
FROM #IBDynamicRules
order by LEN(rulename) desc







/*
select  a.b.value('@Name','nvarchar(100)') name 
from assessmenttable

cross apply AssessmentRules.nodes('PaperRules/Section[@Fixed=0]/Rule') a(b)

where externalreference = 'Windesheimtest'
*/