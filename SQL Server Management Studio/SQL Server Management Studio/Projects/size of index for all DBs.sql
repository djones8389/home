IF OBJECT_ID('tempdb..##IndexSize') IS NOT NULL DROP TABLE ##IndexSize;

CREATE TABLE ##IndexSize (
	client nvarchar(1000)
	, IndexSizeKB int
);

DECLARE @dynamic nvarchar(MAX)='';

select @dynamic +=CHAR(13)+ '
use '+QUOTENAME([NAME])+'

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

if exists(select 1 from sys.indexes where name = ''idx_WAREHOUSE_ExamSessionItemResponseTable_WAREHOUSEExamSessionID'')
	
BEGIN
	INSERT ##IndexSize
	SELECT  
		db_name() [Client]
		,[index_size-kb] = cast(LTRIM (STR ((CASE WHEN usedpages > pages THEN (usedpages - pages) ELSE 0 END) * 8, 15, 0)) as int)
	FROM (

		SELECT 
			OBJECT_NAME(s.object_id) [TableName]
			, (used_page_count) [usedpages]
			,  (
				CASE
					WHEN (s.index_id < 2) THEN (in_row_data_page_count + lob_used_page_count + row_overflow_used_page_count)
					ELSE lob_used_page_count + row_overflow_used_page_count
				END
			) [pages]
			, i.name
		FROM sys.dm_db_partition_stats AS s
			INNER JOIN sys.indexes AS i
				ON s.[object_id] = i.[object_id]
				AND s.[index_id] = i.[index_id]
			INNER JOIN sys.tables t
			on t.object_id = s.object_id
	) A
	where [TableName] = ''WAREHOUSE_ExamSessionItemResponseTable''
		and name = ''idx_WAREHOUSE_ExamSessionItemResponseTable_WAREHOUSEExamSessionID''
END
'
from sys.databases
where state_desc = 'ONLINE'
	and name like '%[_]SecureAssess';

exec(@dynamic);

select *
from ##IndexSize
order by 1



