if OBJECT_ID ('tempdb..#checker') is not null drop table #checker;

create table #checker (
	keycode nvarchar(10)
	, correctMark float
	, assignedMark float
);


bulk insert #checker
from 'C:\Users\977496-davej2\Desktop\AAT_Combined - Copy.csv'
with (fieldterminator=',',rowterminator='\n')

SELECT B.*
FROM (
SELECT A.Keycode
	, forename + ' ' + surName [Candidate Name]
	, candidateRef [Membership No.]
	, centreName
	, a.correctMark
	, a.assignedMark
	, wests.userMark
	,(wests.userMark-(a.assignedMark * WESTI.TotalMark))+(correctMark*WESTI.TotalMark) [userMark_shouldbe]
	, userPercentage
	, (((wests.userMark-(a.assignedMark * WESTI.TotalMark))+(correctMark*WESTI.TotalMark))/west.resultData.value('data(exam/@totalMark)[1]','tinyint'))*100 [userPercent_shouldbe]
	, cast(WESTI.TotalMark as float) [ItemTotalMark]
	, west.resultData.value('data(exam/@passMark)[1]','tinyint') [ExamPassMark]
	, west.resultData.value('data(exam/@totalMark)[1]','tinyint') [ExamTotalMark]
	, WESTI.ItemRef 
	, originalGrade
	--, west.resultData
	, case when
		((((wests.userMark-(a.assignedMark * WESTI.TotalMark))+(correctMark*WESTI.TotalMark))/west.resultData.value('data(exam/@totalMark)[1]','tinyint'))*100 < west.resultData.value('data(exam/@passMark)[1]','tinyint')) then 'Fail'
			ELSE 'Pass'
			END AS [result_shouldbe]
	, clientInformation.value('data(clientInformation/systemConfiguration/flashPlayer/version)[1]','nvarchar(200)') [Flash]
	, downloadInformation.value('data(downloads/information/date)[1]','nvarchar(MAX)') [DownloadedToPC]
	, clientInformation.value('data(clientInformation/systemConfiguration/timeZone/timeZoneName)[1]','nvarchar(200)') [Timezone]
	, clientInformation.value('data(clientInformation/systemConfiguration/timeZone/daylightSavingTime)[1]','bit') [DaylightSavingTime]
	, [started] as [State 6]
	, ROW_NUMBER() over (partition by west.Keycode order by wesirt.itemid) R
FROM #checker A
INNER JOIN WAREHOUSE_ExamSessionTable_Shreded wests WITH (NOLOCK)
on wests.keycode = a.keycode
inner join WAREHOUSE_ExamSessionTable west WITH (NOLOCK)
on west.id = wests.examSessionId
INNER JOIN WAREHOUSE_ExamSessionItemResponseTable WESIRT WITH (NOLOCK)
ON WESIRT.WAREHOUSEExamSessionID = WEST.ID	
	and assignedMark = WESIRT.ItemResponseData.value('data(p/@um)[1]','float')
INNER JOIN WAREHOUSE_ExamSessionTable_ShrededItems WESTI WITH (NOLOCK)
on WESTI.examSessionId = west.ID
	and WESTI.ItemRef = WESIRT.ItemID
	and wesirt.itemid in ('974P1252','974P1225','974P1180','974P1169','974P1132','974P1064')
--where west.Keycode in ('3W7PK6A6','6CRRCRA6','73YPBYA6','7FKGBYA6','7J7GDPA6','8FRV76A6','8TV9KMA6','9J4K3RA6','9K34T4A6','BNLKF6A6','BWLNP7A6','DPGTC9A6','FYDWBLA6','HHVBLGA6','J4K4PTA6','J6G4XWA6','HM8WRTA6','H6T4WWA6','KR3TF9A6','GP9XLWA6','GNKQNVA6','NG74CFA6','FNPXKYA6','PNJKPCA6','PRGGN6A6','F9Y6N3A6','F6WNFFA6','DR6JGJA6','DQP4MYA6','QCKM9BA6','QDBNFHA6','DKK9W8A6','CDPCL4A6','C3FMH3A6','VDGXBXA6','VPV7GJA6','BNRD9DA6','WMPNGDA6','9VKJ7VA6','8RYNQMA6','XF78BGA6','Y4D44BA6','6YH9T9A6','6P8JTDA6','6MR3NMA6','4N4DL7A6','4CTYLHA6','3XHPJRA6','3MWC7BA6','3FPJ3PA6','33L4VPA6','K4HRFKA6','KKDRN4A6','L68DHWA6','M8VYLRA6','PHWXDWA6','PVWT6DA6','Q6M9PGA6','Q766KYA6','Q87KLYA6','QQTK99A6','TQ7PCWA6','TTCGLLA6','VQTC9QA6','WNYJFKA6','WRJ8WWA6','YTTM7HA6')
	--and west.KeyCode in ('7FKGBYA6','GP9XLWA6','J4K4PTA6','7J7GDPA6','QCKM9BA6','4N4DL7A6','YTTM7HA6','6MR3NMA6','73YPBYA6','BWLNP7A6','PRGGN6A6','FNPXKYA6','C3FMH3A6','DQP4MYA6')
) B
--where R > 1
--order by keycode, WESIRT.ItemID
where [result_shouldbe] <> originalGrade


select KeyCode 
from WAREHOUSE_ExamSessionTable west WITH (NOLOCK)
--left JOIN WAREHOUSE_ExamSessionItemResponseTable WESIRT WITH (NOLOCK)
--ON WESIRT.WAREHOUSEExamSessionID = WEST.ID
where west.KeyCode in ('7FKGBYA6','GP9XLWA6','J4K4PTA6','7J7GDPA6','QCKM9BA6','4N4DL7A6','YTTM7HA6','6MR3NMA6','73YPBYA6','BWLNP7A6','PRGGN6A6','FNPXKYA6','C3FMH3A6','DQP4MYA6')
	and wesirt.itemid in ('974P1252','974P1125','974P1180','974P1169','974P1132','974P1064')

	

select KeyCode 
from WAREHOUSE_ExamSessionTable_Shreded west WITH (NOLOCK)
--left JOIN WAREHOUSE_ExamSessionItemResponseTable WESIRT WITH (NOLOCK)
--ON WESIRT.WAREHOUSEExamSessionID = WEST.ID
where west.KeyCode in ('7FKGBYA6','GP9XLWA6','J4K4PTA6','7J7GDPA6','QCKM9BA6','4N4DL7A6','YTTM7HA6','6MR3NMA6','73YPBYA6','BWLNP7A6','PRGGN6A6','FNPXKYA6','C3FMH3A6','DQP4MYA6')