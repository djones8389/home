--SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

--SELECT A.projectID
--	, x.y.value('@id', 'int') [ali]
--	, x.y.value('@name', 'nvarchar(100)')
--FROM (
--select ProjectId
--	, cast(structurexml as xml) structurexml
--from SharedLibraryTable
--) A
--CROSS APPLY structurexml.nodes('sharedLibrary/item') x(y)
--INNER JOIN ProjectManifestTable PMT
--	ON PMT.ProjectId = A.ProjectId
--		and PMT.ID = y.value('@id', 'int')

--where PMT.name like '%ledgerincomplete%' 
--	and PMT.name  not like '%.fla'



select * from [ledgerincomplete] with (NOLOCK)

CREATE VIEW [ledgerincomplete] 
AS (
	
SELECT 
	b.name as [Project Name]
	, cast(b.id as nvarchar(10)) + 'P' + SUBSTRING(A.ID, CHARINDEX('P', A.ID) + 1, CHARINDEX('P', A.ID)) [ItemID]
	, PT.ver [ItemVersion]
	, [AQT]
FROM
(
      Select i.id, i.ver, i.moD, it.aLI
      from ItemGraphicTable it 
            INNER JOIN ComponentTable c ON c.ID = it.ParentID 
            INNER JOIN SceneTable s ON s.ID = c.ParentID 
            INNER JOIN PageTable i ON i.id = S.ParentID
      UNION
      Select i.id, i.ver, i.moD, it.aLI  
      from ItemVideoTable it 
            INNER JOIN ComponentTable c ON c.ID = it.ParentID 
            INNER JOIN SceneTable s ON s.ID = c.ParentID 
            INNER JOIN PageTable i ON i.id = S.ParentID
      UNION
      Select i.id, i.ver, i.moD, it.aLI  
            from ItemHotSpotTable it 
            INNER JOIN ComponentTable c ON c.ID = it.ParentID 
            INNER JOIN SceneTable s ON s.ID = c.ParentID 
            INNER JOIN PageTable i ON i.id = S.ParentID
      UNION
      Select i.id, i.ver, i.moD, it.aLI 
      from ItemCustomQuestionTable it 
            INNER JOIN ComponentTable c ON c.ID = it.ParentID 
            INNER JOIN SceneTable s ON s.ID = c.ParentID 
            INNER JOIN PageTable i ON i.id = S.ParentID
) A

INNER JOIN ProjectListTable B
on SUBSTRING(A.ID, 0,CHARINDEX('P', A.ID)) = B.ID

INNER JOIN (
	SELECT A.projectID
		, x.y.value('@id', 'int') [ali]
		, x.y.value('@name', 'nvarchar(100)') [AQT]
	FROM (
	select ProjectId
		, cast(structurexml as xml) structurexml
	from SharedLibraryTable
	) A
	CROSS APPLY structurexml.nodes('sharedLibrary/item') x(y)
	INNER JOIN ProjectManifestTable PMT
		ON PMT.ProjectId = A.ProjectId
			and PMT.ID = y.value('@id', 'int')

	where PMT.name like '%ledgerincomplete%' 
		and PMT.name  not like '%.fla'
) C
on B.ID = C.ProjectId
	and c.ali = a.ali

INNER JOIN PageTable PT
on PT.ParentID = B.ID
	and PT.ID = cast(b.id as nvarchar(10)) + 'P' + SUBSTRING(A.ID, CHARINDEX('P', A.ID) + 1, CHARINDEX('P', A.ID))

where isnumeric(a.ali) = 1
	and a.ali <> ''
)