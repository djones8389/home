use ICAEW_SecureAssess

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;


--broken 6PWVQWF6
--working 7F4QVKF6

select distinct
	KeyCode
	,	 ItemID
	--,	itemversion
	,    warehouseTime
--into #broken
FROM (

	select west.ID
		, wesirt.itemid
		--, wesirt.itemversion
		, KeyCode
		, itemresponsedata
		, ItemResponseData.query('p/s/c/i/t/r/c/text') [query]
		, a.b.value('.', 'nvarchar(MAX)') [value]
		, west.warehouseTime
	from WAREHOUSE_ExamSessionTable west
	inner join WAREHOUSE_ExamSessionItemResponseTable wesirt
	on west.id = wesirt.warehouseexamsessionid

	CROSS APPLY itemresponsedata.nodes('p/s/c/i/t/r/c/text') a(b)

	where 
		 --( keycode = '6PWVQWF6' 	and itemid = '368P936')
	 
		 --or
		 --( keycode = '7F4QVKF6' 	and itemid = '368P938')
		 --and 
		 itemresponsedata.exist('p/s/c/i/t/r/c/text') = 1
) A
	where LEFT([value], 7) = '%253CP%'
	order by a.warehouseTime desc;



SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;


--broken 6PWVQWF6
--working 7F4QVKF6

select 
	 west.KeyCode
	,	wesirt.ItemID
	,	wesirt.itemversion
	,    west.warehouseTime
	,  ItemResponseData.query('p/s/c/i/t/r/c/text')  [query]
from WAREHOUSE_ExamSessionTable west
inner join WAREHOUSE_ExamSessionItemResponseTable wesirt
on west.id = wesirt.warehouseexamsessionid
inner join #broken a
on a.ItemID = wesirt.ItemID
	and a.ItemVersion = wesirt.ItemVersion
	and a.KeyCode != west.KeyCode
order by west.KeyCode

--select count(ID)
--from WAREHOUSE_ExamSessionItemResponseTable (READUNCOMMITTED)
--where itemid = '368P936'
--	and ItemVersion = '131'

--select count(ID)
--from WAREHOUSE_ExamSessionItemResponseTable (READUNCOMMITTED)
--where itemid = '368P937'
--	and ItemVersion = '131'