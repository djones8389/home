use ICAEW_SecureAssess

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

select west.ID
	,wesirt.itemid
	, KeyCode
	, itemresponsedata
	--, ItemResponseData.query('p/s/c/i/t/r/c/text')
	, a.b.value('.', 'nvarchar(MAX)')
	, west.warehouseTime
from WAREHOUSE_ExamSessionTable west
inner join WAREHOUSE_ExamSessionItemResponseTable wesirt
on west.id = wesirt.warehouseexamsessionid

CROSS APPLY itemresponsedata.nodes('p/s/c/i/t/r/c/text') a(b)

where  a.b.value('.', 'nvarchar(MAX)') like '%%20%'
	--and keycode = '6PWVQWF6'
	order by west.warehouseTime desc;

	/*
	 keycode = '6PWVQWF6'
	and itemid = '368P936'
	and cast(ItemResponseData as nvarchar(MAX)) like '%%20%'
	*/