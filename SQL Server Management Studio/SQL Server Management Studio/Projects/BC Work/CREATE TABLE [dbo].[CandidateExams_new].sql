USE [BC_TPM_Extract]
GO

/****** Object:  Table [dbo].[CandidateExams]    Script Date: 12/04/2017 14:43:29 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[CandidateExams_new] (
	[CandidateScheduleExamID] [int] NOT NULL,
	[CandidateScheduleID] [int] NOT NULL,
	[PackageExamId] [int] NOT NULL,
	[Keycode] [varchar](100) NULL,
	[IsCompleted] [bit] NOT NULL,
	[DateCompleted] [datetime] NULL,
	[Score] [decimal](18, 2) NULL,
	[IsVoided] [bit] NOT NULL,
	[ShouldIncludeScale] [bit] NOT NULL,
	[ShouldIncludeCEFR] [bit] NOT NULL,
	[ShouldIncludeFinalScore] [bit] NOT NULL,
	[IsRescheduled] [bit] NOT NULL,
	[ExamVersionRef] [nvarchar](100) NULL,
	[IsLocalScan] [bit] NOT NULL,
	[ResultXml] [nvarchar](max) NULL,
	[ExamCompletionDate] [datetimeoffset](7) NULL,
	[QualificationId]  int null,
	[QualificationRef] nvarchar(4000) null,
	[QualificationName] nvarchar(4000) null
) 

GO

INSERT [CandidateExams_new]
SELECT CE.CandidateScheduleExamID, CE.CandidateScheduleID, CE.PackageExamId, CE.Keycode, CE.IsCompleted
	, CE.DateCompleted, CE.Score, CE.IsVoided, CE.ShouldIncludeScale, CE.ShouldIncludeCEFR, CE.ShouldIncludeFinalScore
	, CE.IsRescheduled, CE.ExamVersionRef, CE.IsLocalScan, CE.ResultXml, CE.ExamCompletionDate
	, Q.QualificationID, Q.QualificationRef, Q.QualificationName
FROM CandidateExams CE
left join [dbo].[QualCorrectInfo_Full] Q
on Q.[CandidateScheduleID] = CE.[CandidateScheduleID]
	AND Q.Keycode = CE.Keycode
left join [CandidateExams_new] C
on C.CandidateScheduleExamID = CE.CandidateScheduleExamID
	and C.[CandidateScheduleID] = ce.[CandidateScheduleID]
where c.CandidateScheduleID is null


/*
select ce.Keycode, ce.CandidateScheduleID, c.CandidateScheduleID
FROM CandidateExams CE (READUNCOMMITTED)
left join [CandidateExams_new] C
on C.Keycode = CE.Keycode
where C.Keycode IS NULL --79631

select ce.Keycode, ce.CandidateScheduleID, c.CandidateScheduleID
FROM CandidateExams CE (READUNCOMMITTED)
left join [CandidateExams_new] C
on C.CandidateScheduleExamID = CE.CandidateScheduleExamID
	and C.[CandidateScheduleID] = ce.[CandidateScheduleID]
where c.CandidateScheduleID is null  --79631
*/