USE BC_TPM_Extract 

SELECT TOP (1000) [ScheduledPackageCandidateID]
      ,[CandidateID]
      ,[CandidateRef]
      ,[FirstName]
      ,[MiddleName]
      ,[LastName]
      ,[DOB]
  FROM [BC_TPM_Extract].[dbo].[Candidates]
  where CandidateID = 306417
  
  SELECT ID
	, userid 
	,[CandidateRef]
	,uln
	,forename [FirstName]
	,middlename [MiddleName]
	,surname [LastName]
	,Gender
	,AddressLine1
	,AddressLine2
	,Town
	,County
	,Country
	,PostCode
	,email
	,Telephone
	,0 [retiredTBD]
	,[DOB]
	,AccountExpiryDate --'ExpiryDate'
INTO [dbo].[Candidates_full]
FROM [dbo].[WAREHOUSE_UserTable]

exec sp_rename 'Candidates_full.userID', 'CandidateID', 'COLUMN';  

DELETE
FROm [Candidates_full]
where ID IN (  
	select a.id
	from (

	SELECT c.id
		,CandidateID
		, C.CandidateRef
		, C.FirstName
		, C.lastname
		, ROW_NUMBER() OVER(partition by CandidateID
		, C.CandidateRef
		, C.FirstName
		, C.lastname order by c.id) r
	FROM [Candidates_full] C

	) a
	where R <> 1
)


--DELETE
--FROm [Candidates_full]
--where CandidateID NOT IN (
--  select candidateID
--  from Candidates
--)

SELECT *
INTO Candidates_full2
FROM Candidates_full
where CandidateID  IN (
  select candidateID
  from Candidates
)

drop table Candidates_full;
exec sp_rename 'Candidates_full2', 'Candidates_full'