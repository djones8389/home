/****** Script for SelectTopNRows command from SSMS  ******/
SELECT   [QualificationID]
      ,count( [QualificationID])
  FROM [BC_TPM_Extract].[dbo].[Qualifications]
  group by  [QualificationID]
  having count( [QualificationID]) > 1

  select distinct [QualificationID]
  FROM [BC_TPM_Extract].[dbo].[Qualifications]
  /*
  •	The Qualification table has different records with the same Qualification Id
  •	Cannot enforce referential integrity between Qualification and CentreQualLookup

*/


--•	Some Candidate records were blank rows
--•	CandidateSchedules has corrupted data – with rows with blank CandidateIds



select  CS.[CandidateScheduleID]	
	,Keycode
	, Q.QualificationId
	, QualificationName
	, QualificationRef

from [CandidateExams] ce
inner join [CandidateSchedules] cs on cs.CandidateScheduleID = ce.CandidateScheduleID
inner join TestPackages TP on TP.ScheduledPackageID = CS.ScheduledPackageId
inner join Candidates ca on ca.CandidateID = cs.CandidateID
inner join Centres C on C.CentreID = TP.CentreID
inner join Packages P on P.PackageID = TP.PackageID
inner join PackageExams PE ON PE.PackageExamID = CE.PackageExamId and PE.PackageID = P.PackageID
INNER JOIN Qualifications Q ON Q.QualificationID = tp.QualificationID
--where Keycode in ('QSWC4701','DYTU3U01','8VZMYD01','REZ3Q401','2DPDNF01','BJNVRG01','HEA2M201','43L97Y01','HWA6CE01')
where Keycode = 'PXTDXH99'


select 
	Keycode
	, q.*
	,tp.QualificationID
from [CandidateExams] ce
inner join [CandidateSchedules] cs on cs.CandidateScheduleID = ce.CandidateScheduleID
inner join TestPackages TP on TP.ScheduledPackageID = CS.ScheduledPackageId
inner join Qualifications q on q.QualificationID = tp.QualificationID
where q.qualificationid in (122,136,167,172,235)
	and Keycode = '3VKR4H99'
	
	order by Keycode ASC
--inner join [dbo].[QualCorrectInfo] QQ 
--	on QQ.QualificationId = q.QualificationID
--	and qq.QualificationName != q.QualificationName
--	and qq.qualificationref != q.QualificationRef
--where tp.QualificationID in (122,136,167,172,235)


UPDATE Qualifications

SELECT *
FROM Qualifications Q
INNER JOIN [dbo].[QualCorrectInfo] QQ
on qq.QualificationName = q.QualificationName
	and qq.qualificationref = q.qualificationref
	and q.QualificationID != qq.QUalificationid

where q.QualificationID = 119 
	or qq.qualificationid = 119


select 
	Keycode
	, q.*
	,tp.QualificationID
from [CandidateExams] ce
inner join [CandidateSchedules] cs on cs.CandidateScheduleID = ce.CandidateScheduleID
inner join TestPackages TP on TP.ScheduledPackageID = CS.ScheduledPackageId
inner join Qualifications q on q.QualificationID = tp.QualificationID
where   keycode = '3VKR4H99'




----UPDATE [dbo].[TestPackages]
----set QualificationID =
select  Keycode
	,qf.QualificationName
	, q.QualificationName
	, qf.QualificationRef
	,q.QualificationRef
FROM [dbo].[TestPackages] TP
inner join [CandidateSchedules] cs 
on cs.ScheduledPackageId = tp.ScheduledPackageID
inner join [CandidateExams] ce
on cs.CandidateScheduleID = ce.CandidateScheduleID 
inner join [FULL_TEXT_TEST].[dbo].[QualCorrectInfo_full] qf
on qf.ScheduledExamRef = Keycode
inner join  Qualifications q 
on q.QualificationID = qf.QualificationID
where  (
	   qf.QualificationName != q.QualificationName
		or 
		qf.qualificationref != q.QualificationRef
		)


begin tran
UPDATE Qualifications
set QualificationName = qf.QualificationName
	, QualificationRef = qf.QualificationRef
FROM Qualifications Q
inner join [FULL_TEXT_TEST].[dbo].[QualCorrectInfo_full] qf
on q.QualificationID = qf.QualificationID
inner join [CandidateExams] ce
on qf.ScheduledExamRef = Keycode
inner join [CandidateSchedules] cs 
on cs.CandidateScheduleID = ce.CandidateScheduleID 
inner join [TestPackages] TP
on cs.ScheduledPackageId = tp.ScheduledPackageID

where  (
	   qf.QualificationName != q.QualificationName
		or 
		qf.qualificationref != q.QualificationRef
		)
rollback


/*
begin tran

select *
from [QualCorrectInfo] QQ
where ScheduledExamRef = '3VKR4H99'

select *
from [QualCorrectInfo_backup] QQ
where ScheduledExamRef = '3VKR4H99'

select *
from Qualifications
where qualificationID = 172

UPDATE [QualCorrectInfo]
set qualificationName = q.qualificationName
	, QualificationRef = q.QualificationRef
from [QualCorrectInfo] QQ
inner join Qualifications Q 
on qq.qualificationID = q.qualificationID
where q.qualificationid in (122,136,167,172,235)
	--and Scheduledexamref = '9LYNMW99'
	and
		(
		qq.QualificationName != q.QualificationName
		or 
		qq.qualificationref != q.QualificationRef
		)

select QQ.*	 
from [QualCorrectInfo] QQ
inner join Qualifications Q 
on qq.qualificationID = q.qualificationID
where ScheduledExamRef = 'PXTDXH99'


rollback

*/

select *	 
from [QualCorrectInfo] QQ
inner join Qualifications Q 
on qq.qualificationID = q.qualificationID
where qq.qualificationid in (122,136,167,172,235)
	

	--and Scheduledexamref = '9LYNMW99'
	and
		(
		qq.QualificationName != q.QualificationName
		or 
		qq.qualificationref != q.QualificationRef
		)
		and ScheduledExamRef = 'PXTDXH99'


begin tran

select * from Qualifications where qualificationid in (122,136,167,172,235)

UPDATE Qualifications
set qualificationName = qq.qualificationName
	, QualificationRef = qq.QualificationRef
from Qualifications Q
inner join  [QualCorrectInfo] QQ
on qq.qualificationID = q.qualificationID
where q.qualificationid in (122,136,167,172,235)
	--and Scheduledexamref = '9LYNMW99'
	and
		(
		qq.QualificationName != q.QualificationName
		or 
		qq.qualificationref != q.QualificationRef
		)
select * from Qualifications where qualificationid in (122,136,167,172,235)

rollback




begin tran


	select  CS.[CandidateScheduleID]	
		,Keycode
		, Q.QualificationId
		, Q.QualificationName
		, Q.QualificationRef
		, qc.*
	from [CandidateExams] ce
	inner join [CandidateSchedules] cs on cs.CandidateScheduleID = ce.CandidateScheduleID
	inner join TestPackages TP on TP.ScheduledPackageID = CS.ScheduledPackageId
	inner join Candidates ca on ca.CandidateID = cs.CandidateID
	inner join Centres C on C.CentreID = TP.CentreID
	inner join Packages P on P.PackageID = TP.PackageID
	inner join PackageExams PE ON PE.PackageExamID = CE.PackageExamId and PE.PackageID = P.PackageID
	INNER JOIN Qualifications Q ON Q.QualificationID = tp.QualificationID
	inner join [dbo].[QualCorrectInfo] qc on qc.ScheduledexamRef = Keycode
	where 
		
			qc.QualificationName != q.QualificationName
			or 
			qc.qualificationref != q.QualificationRef
		



--update TestPackages
--set QualificationID =  q.QualificationID
--from TestPackages tp
--inner join CandidateSchedules cs
--on cs.CandidateScheduleID = tp.ScheduledPackageID
--inner join [dbo].[QualCorrectInfo] qc
--on qc.[CandidateScheduleID] = tp.ScheduledPackageID
--inner join [dbo].[Qualifications] q
--on q.QualificationID = qc.qualificationid

--select *
--into [dbo].[TestPackages_backup]
--from [dbo].[TestPackages]


	select  CS.[CandidateScheduleID]	
		,Keycode
		, Q.QualificationId
		, Q.QualificationName
		, Q.QualificationRef
		, qc.*
	from [CandidateExams] ce
	inner join [CandidateSchedules] cs on cs.CandidateScheduleID = ce.CandidateScheduleID
	inner join TestPackages TP on TP.ScheduledPackageID = CS.ScheduledPackageId
	inner join Candidates ca on ca.CandidateID = cs.CandidateID
	inner join Centres C on C.CentreID = TP.CentreID
	inner join Packages P on P.PackageID = TP.PackageID
	inner join PackageExams PE ON PE.PackageExamID = CE.PackageExamId and PE.PackageID = P.PackageID
	INNER JOIN Qualifications Q ON Q.QualificationID = tp.QualificationID
	inner join [dbo].[QualCorrectInfo] qc on qc.ScheduledexamRef = Keycode
	where 
		
			qc.QualificationName != q.QualificationName
			or 
			qc.qualificationref != q.QualificationRef
		



rollback









select  CS.[CandidateScheduleID]	
	,Keycode
	, Q.QualificationId
	, Q.QualificationName
	, Q.QualificationRef
	, qc.*
from [CandidateExams] ce
inner join [CandidateSchedules] cs on cs.CandidateScheduleID = ce.CandidateScheduleID
inner join TestPackages TP on TP.ScheduledPackageID = CS.ScheduledPackageId
inner join Candidates ca on ca.CandidateID = cs.CandidateID
inner join Centres C on C.CentreID = TP.CentreID
inner join Packages P on P.PackageID = TP.PackageID
inner join PackageExams PE ON PE.PackageExamID = CE.PackageExamId and PE.PackageID = P.PackageID
INNER JOIN Qualifications Q ON Q.QualificationID = tp.QualificationID
inner join [dbo].[QualCorrectInfo] qc on qc.ScheduledexamRef = Keycode
where 
		
		qc.QualificationName != q.QualificationName
		or 
		qc.qualificationref != q.QualificationRef
		
