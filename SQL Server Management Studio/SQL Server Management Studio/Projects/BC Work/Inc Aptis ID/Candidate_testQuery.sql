drop table #missingCandidateID;

select distinct CandidateID
into #missingCandidateID
from [dbo].[CandidateSchedules]
where CandidateID not in (

select N.CandidateID
	--, O.ID [AptisCandidateID]
from [BC_TPM_Extract].[dbo].[Candidates] N  
inner join (
select id	
	, CandidateRef
	, AddressLine1
	, AddressLine2
	, Forename
	, Middlename
	, Surname
	, DOB
--from [BC_TPM_Extract].[dbo].[xxFullUsers]
--where Forename = 'Monika' and Surname = 'Zarat'
--where Forename = 'CAO KIEU DUYEN'
--UNION

--select UserId
--	, CandidateRef	
--	,AddressLine1
--	,AddressLine2
--	, Forename
--	, Middlename
--	, Surname
--	, DOB
from [BC_TPM_Extract].[dbo].[xxFullUsers]
--where Forename = 'Monika' and Surname = 'Zarat'
--where Forename = 'CAO KIEU DUYEN'
)  O
on  
	isnull(N.FirstName,0) =  isnull(O.Forename,0)
	and isnull(N.LastName,0) =  isnull(o.Surname,0)
	and isnull(N.DOB,0) =  isnull(O.DOB,0)
	and isnull(N.Middlename,0) = isnull(O.Middlename,0)
	and isnull(N.CandidateRef,0) = isnull(O.CandidateRef,0)
	and isnull(n.AddressLine1,0) = isnull(o.AddressLine1,0)    --58,885 w/ these
	and isnull(n.AddressLine2,0) = isnull(o.AddressLine2,0)    --58,885 w/ these
	
)


SELECT * --old (8813 row(s) affected)

FROM #missingCandidateID
where CandidateID = 663638

select *
from CandidateSchedules
where CandidateID = 663638

SELECT *
FROM Candidates
where CandidateID = 663638 or FirstName = 'CAO KIEU DUYEN'













--;with cte as (
--select id	
--	, CandidateRef
--	, AddressLine1
--	, AddressLine2
--	, Forename
--	, Middlename
--	, Surname
--	, DOB
--from [BC_TPM_Extract_old].[dbo].[xxUserTable]

--UNION

--select UserId
--	, CandidateRef	
--	,AddressLine1
--	,AddressLine2
--	, Forename
--	, Middlename
--	, Surname
--	, DOB
--from [BC_TPM_Extract_old].[dbo].[xxWAREHOUSE_UserTable]
--) 



--select *
--into #table
--from cte 

