USE [Biglo_SecureAssess]
GO
/****** Object:  StoredProcedure [dbo].[sa_CANDIDATEEXAMSTATEMANAGEMENTSERVICE_GetExternallyDeliveredExams_sp]    Script Date: 15/05/2017 11:31:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================    
-- Author:  Stanislav Gants  
-- Create date: 28/12/16   
-- Description: Selects all exam sessions at state 4 with flag externally delivered to process it for paper marking 
-- =============================================   

ALTER Proc [dbo].[sa_CANDIDATEEXAMSTATEMANAGEMENTSERVICE_GetExternallyDeliveredExams_sp]
AS
BEGIN
    SET NOCOUNT ON;

    BEGIN TRY
               
  SELECT TOP 500 
     E.ID                                  AS id
  FROM ExamSessionTable AS E
  INNER JOIN ScheduledExamsTable AS S
   ON E.ScheduledExamID = S.ID
  WHERE E.examState = 4 and E.ExternallyDelivered = 1  

    END TRY
 BEGIN CATCH
  DECLARE @ErrorMessage NVARCHAR(4000);
  DECLARE @ErrorSeverity INT;

  SELECT @ErrorMessage = ERROR_MESSAGE(),
      @ErrorSeverity = ERROR_SEVERITY();

  -- Use RAISERROR inside the CATCH block to return 
  -- error information about the original error that 
  -- caused execution to jump to the CATCH block.
  RAISERROR (@ErrorMessage, -- Message text.
       @ErrorSeverity, -- Severity.
       1 -- State.
       );
 END CATCH
END

