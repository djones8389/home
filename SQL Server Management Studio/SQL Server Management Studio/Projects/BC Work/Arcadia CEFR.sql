--select * from CandidateExams where Keycode = 'QJTCR499'

WITH EXAMS
AS
(
SELECT
       pe.Name 'ComponentName'
       , SPC.FirstName 'FirstName'
       , SPC.LastName 'LastName'
       , C.CandidateRef 'CandidateRef'
	   , SPC.[CandidateScheduleID]
	   , SPCE.ShouldIncludeCEFR
	   , pe.IsAdjustComponentScoreBoundariesActivated
	   , CASE WHEN SPC.IsCompleted = 1 AND SPC.IsVoided = 0 THEN 1 ELSE 0 END 'IsFinished'
       , CASE WHEN SPC.IsCompleted = 1 AND SPC.IsVoided = 0 AND SPCE.ShouldIncludeScale = 1 AND SPCE.Score IS NOT NULL THEN SPCE.Score else 0 end 'ComponentScaleScore'
       , CASE WHEN SPC.IsCompleted = 1 AND SPC.IsVoided = 0 AND SPCE.ShouldIncludeScale = 1 AND pe.ScaleValue IS NOT NULL THEN pe.ScaleValue else P.DefaultScaleValue end 'ComponentMaxScaleScore'
       , CASE WHEN SPCE.ShouldIncludeFinalScore = 1 AND SPCE.ShouldIncludeScale = 1 THEN 1 ELSE 0 END 'ShouldIncludeFinalScore'
       , CE.CentreName 'Centre'
       , CONVERT(VARCHAR(10),SPCE.DateCompleted,103) 'Completed'
       , CASE WHEN SPC.IsVoided = 1 THEN 'Voided'
				WHEN SPC.IsCompleted = 1 THEN 'Completed'
				else 'InProgress' END 'State'

	   ,SPC.[DateCompleted]
	,TP.[ScheduledPackageId] 
	,P.[PackageId] 
	,(CASE WHEN (SPCE.CandidateScheduleExamID IS NULL) THEN CAST(NULL AS int) ELSE 1 END) 'HasSpce'
	,pe.[PackageExamId]
	,ET.[ExamTypeId] 
	,SPCE.CandidateScheduleExamID
   
FROM [dbo].CandidateExams AS SPCE
INNER JOIN [dbo].CandidateSchedules AS SPC ON SPC.[CandidateScheduleID] = SPCE.[CandidateScheduleID]
--INNER JOIN [dbo].Packages AS SP ON SP.ScheduledPackageId = SPC.ScheduledPackage_ScheduledPackageId
INNER JOIN [dbo].[PackageExams] pe ON pe.PackageExamId = SPCE.[PackageExamId]
INNER JOIN [dbo].[Packages] P ON P.[PackageId] = pe.[PackageId]
INNER JOIN [dbo].ExamTypes ET ON ET.ExamTypeId = pe.ExamTypeId
INNER JOIN Candidates C on C.CandidateID = SPC.CandidateID
INNER JOIn TestPackages TP ON TP.ScheduledPackageID = SPC.ScheduledPackageID 
INNER JOIN Centres CE ON CE.CentreID = TP.CentreID
where C.CandidateRef LIKE '%danka17-3%'
),
Grammas
AS (
SELECT [CandidateScheduleID], ComponentScaleScore, pe.MagicNumber, pe.MagicScaleIncrease
FROM EXAMS 
INNER JOIN [dbo].[PackageExams] pe ON pe.PackageExamId = EXAMS.[PackageExamId]
WHERE EXAMS.ExamTypeId = 5
),
MagicNumberScores
AS (
SELECT
       EXAMS.CandidateScheduleExamID
       , CASE WHEN EXAMS.ExamTypeId = 5 OR Grammas.MagicNumber IS NULL OR Grammas.ComponentScaleScore < Grammas.MagicNumber THEN EXAMS.ComponentScaleScore 
	          WHEN (EXAMS.ComponentScaleScore + EXAMS.ComponentScaleScore * Grammas.MagicScaleIncrease > EXAMS.ComponentMaxScaleScore) THEN EXAMS.ComponentMaxScaleScore
		 ELSE (EXAMS.ComponentScaleScore + EXAMS.ComponentScaleScore * Grammas.MagicScaleIncrease) END 'Score'   
FROM EXAMS
LEFT JOIN Grammas ON Grammas.[CandidateScheduleID] = EXAMS.[CandidateScheduleID]
),
ExamCefrs
AS (
	SELECT CandidateScheduleExamID, Code, Score, DefaultMinScore
	FROM (
			SELECT CandidateScheduleExamID, Code, Score, DefaultMinScore, 
						ROW_NUMBER() OVER(PARTITION BY B.CandidateScheduleExamID ORDER BY B.DefaultMinScore DESC) as num
			FROM (
					SELECT EXAMS.CandidateScheduleExamID, EG.Code, MagicNumberScores.Score, 
						CASE WHEN SB.Value IS NOT NULL THEN SB.Value ELSE EGD.DefaultMinScore END DefaultMinScore
					FROM EXAMS 
					INNER JOIN [dbo].[PackageExams] pe ON pe.PackageExamId = EXAMS.[PackageExamId]
					INNER JOIN [dbo].[PackageGrades] PG ON PG.[PackageId] = pe.[PackageId]
					INNER JOIN [dbo].ExamGrades EG ON EG.Id = PG.ExamGradeId
					INNER JOIN [dbo].ExamGradeDescriptions EGD ON EGD.ExamTypeId = pe.ExamTypeId AND EGD.ExamGradeId = EG.Id
					LEFT JOIN [dbo].PackageExamScoreBoundaries SB ON (SB.PackageExamId = EXAMS.[PackageExamId] AND SB.ExamGradeId = EG.Id AND EXAMS.IsAdjustComponentScoreBoundariesActivated = 1)
					INNER JOIN MagicNumberScores ON MagicNumberScores.CandidateScheduleExamID = EXAMS.CandidateScheduleExamID
					WHERE (SB.Value IS NOT NULL AND MagicNumberScores.Score >= SB.Value) OR (SB.Value IS NULL AND MagicNumberScores.Score >= EGD.DefaultMinScore)
				) B
			
		) A
	WHERE num = 1
),
PACKS
AS (
SELECT
       EXAMS.[CandidateScheduleID]
       , CASE WHEN (SUM(EXAMS.IsFinished) > 0 OR (SPC.IsCompleted = 1 AND SPC.IsVoided = 0)) AND P.ShouldIncludeScale = 1 AND SPC.PackageScore IS NOT NULL THEN SPC.PackageScore else 0 end 'TestPackageScaleScore'
       , CASE WHEN (SUM(EXAMS.IsFinished) > 0  OR (SPC.IsCompleted = 1 AND SPC.IsVoided = 0)) AND P.ShouldIncludeScale = 1 AND SUM(EXAMS.ShouldIncludeFinalScore) > 0 THEN SUM(EXAMS.ComponentMaxScaleScore) else 0 end 'TestPackageMaxScaleScore'

   
FROM EXAMS
INNER JOIN [dbo].CandidateSchedules AS SPC ON SPC.[CandidateScheduleID] = EXAMS.[CandidateScheduleID]
INNER JOIN [dbo].Packages AS SP ON SP.PackageId = SPC.ScheduledPackageId
INNER JOIN [dbo].[Packages] P ON P.[PackageId] = SP.PackageId
GROUP BY EXAMS.[CandidateScheduleID], SPC.IsCompleted, SPC.IsVoided, P.ShouldIncludeScale, SPC.PackageScore
)
,
PACKSGROUPED
AS (
SELECT ComponentName, FirstName, LastName, CandidateRef, 
'' CEFR,
'' ComponentScaleScore, '' ComponentMaxScaleScore, 
CAST(TestPackageScaleScore as varchar(10)) TestPackageScaleScore, 
CAST(TestPackageMaxScaleScore as varchar(10)) TestPackageMaxScaleScore, 
Centre, Completed, [State],
EXAMS.[DateCompleted], 
EXAMS.[PackageId], 
EXAMS.[ScheduledPackageId], 
EXAMS.[CandidateScheduleID],  
0 HasSpce, 
0 [PackageExamId], 
0 [ExamTypeId], 
0 [ScheduledPackageCandidateExamId], 
ROW_NUMBER() OVER(PARTITION BY EXAMS.[CandidateScheduleID] ORDER BY EXAMS.HasSpce ASC, EXAMS.[PackageExamId] ASC, EXAMS.[ExamTypeId] ASC, EXAMS.[CandidateScheduleExamID] ASC) as num

FROM EXAMS
LEFT JOIN ExamCefrs ON ExamCefrs.CandidateScheduleExamID = EXAMS.CandidateScheduleExamID
JOIN PACKS ON PACKS.[CandidateScheduleID] = EXAMS.[CandidateScheduleID]
WHERE EXAMS.[State] = 'Completed'
)
,
Results
AS (
SELECT ComponentName, FirstName, LastName, CandidateRef, CEFR, ComponentScaleScore, ComponentMaxScaleScore, 
TestPackageScaleScore, TestPackageMaxScaleScore, Centre, Completed, [State], [DateCompleted], 
[PackageId], [ScheduledPackageId], [CandidateScheduleID],  HasSpce, [PackageExamId], [ExamTypeId], [ScheduledPackageCandidateExamId] 
FROM PACKSGROUPED WHERE num = 1
UNION ALL
SELECT ComponentName, FirstName, LastName, CandidateRef, 
CASE
	when EXAMS.ShouldIncludeCEFR = 0 then ''
	when EXAMS.ShouldIncludeCEFR = 1 then ExamCefrs.Code
	end CEFR,
CAST(ComponentScaleScore as varchar(10)) ComponentScaleScore, 
CAST(ComponentMaxScaleScore as varchar(10)) ComponentMaxScaleScore, 
'' TestPackageScaleScore, '' TestPackageMaxScaleScore, Centre, Completed, [State],
EXAMS.[DateCompleted], 
EXAMS.[PackageId], 
EXAMS.[ScheduledPackageId], 
EXAMS.[CandidateScheduleID],  
EXAMS.HasSpce, 
EXAMS.[PackageExamId], 
EXAMS.[ExamTypeId], 
EXAMS.CandidateScheduleExamID
FROM EXAMS
LEFT JOIN ExamCefrs ON ExamCefrs.CandidateScheduleExamID = EXAMS.CandidateScheduleExamID
JOIN PACKS ON PACKS.[CandidateScheduleID] = EXAMS.[CandidateScheduleID]
WHERE EXAMS.[State] = 'Completed'
)

SELECT * FROM Results
ORDER BY 
[DateCompleted] DESC, 
[PackageId] ASC, 
[ScheduledPackageId] ASC, 
[CandidateScheduleID] ASC,  
HasSpce ASC, 
[PackageExamId] ASC, 
[ExamTypeId] ASC, 
[ScheduledPackageCandidateExamId] ASC
