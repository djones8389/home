use [BC_TPM_Extract_old]

select id, 	
	CandidateRef
	, Forename
	, MiddleName
	, Surname	
	, Country
	, County
from [dbo].[xxUserTable]
where forename = 'Nuriyya' --and surname = 'Nuriyya'
--union
select userid
	,CandidateRef
	, Forename
	, MiddleName
	, Surname
	, Country
	, County
from [dbo].[xxWAREHOUSE_UserTable]
where forename = 'Nuriyya' --and surname = 'Nuriyya'






select *
from [BC_TPM_Extract]..Candidates N
INNER JOIN (

select 
	candidateref
	, Forename
	, Surname
	, cast(Country as nvarchar(200)) Country
	, cast(County as nvarchar(200)) County
FROM [BC_TPM_Extract_old]..[xxUserTable]
--where forename = 'Nuriyya'
--UNION
--select 
--	candidateref
--	, Country
--	, County
--FROM [BC_TPM_Extract_old]..[xxWarehouse_UserTable]
--where forename = 'Nuriyya'

) O
on o.CandidateRef = n.CandidateRef
		and o.Forename = n.FirstName
		and o.Surname = n.LastName
where (N.Country in ('Unknown','-1')	or N.County in ('Unknown','-1')

		and

       O.Country not in  ('Unknown','-1')	or O.County not in ('Unknown','-1')
)


begin tran

UPDATE [BC_TPM_Extract]..Candidates
set Country = O.Country
--select n.CandidateRef
--	, o.CandidateRef
--	, n.Country
--	, o.Country
--	, Forename
--	, FirstName
FROM [BC_TPM_Extract]..Candidates N
INNER JOIN (
select 
	candidateref
	, Forename
	, Surname
	, cast(Country as nvarchar(200)) Country
	, cast(County as nvarchar(200)) County
FROM [BC_TPM_Extract_old]..[xxUserTable]
--where CandidateRef = ''
	--and CandidateRef = '27JUL16BUEAM158153'
) O
on o.CandidateRef = n.CandidateRef
	and o.Forename = n.FirstName
	and o.Surname = n.LastName
WHERE N.Country in ('Unknown','-1')
	--and O.Country NOT in ('Unknown','-1')

UPDATE [BC_TPM_Extract]..Candidates
set County = O.County
--select n.CandidateRef
--	, o.CandidateRef
--	, n.County
--	, o.County
--	, Forename
--	, FirstName
FROM [BC_TPM_Extract]..Candidates N
INNER JOIN (
select 
	candidateref
	, Forename
	, Surname
	, cast(Country as nvarchar(200)) Country
	, cast(County as nvarchar(200)) County
FROM [BC_TPM_Extract_old]..[xxUserTable]
--where CandidateRef != ''
--	and CandidateRef = '27JUL16BUEAM158153'
) O
on o.CandidateRef = n.CandidateRef
	and o.Forename = n.FirstName
	and o.Surname = n.LastName
WHERE N.County in ('Unknown','-1')
	and O.County NOT in ('Unknown','-1')

rollback


--select *
--from [BC_TPM_Extract]..Candidates N
--where CandidateRef in (

--	select  FirstName, LastName, CandidateRef
--	from [BC_TPM_Extract]..Candidates N
--	where CandidateRef != ''
--	group by FirstName, LastName, CandidateRef
--	having count(*) > 1
--) 
--order by CandidateRef


--select *
--from   [BC_TPM_Extract]..Candidates N
--where candidateref = 'TSG07022017-005'

SELECT  firstname, LastName, n.CandidateRef, n.country
	, o.Forename, o.Surname, o.CandidateRef, o.Country
FROM [BC_TPM_Extract]..Candidates N
inner join [BC_TPM_Extract_old]..[xxUserTable] O
ON  o.CandidateRef = n.CandidateRef
	and o.Forename = n.FirstName
	and o.Surname = n.LastName

where n.country = '-1'
	and o.country != '-1'


select count(country)
	, country
from Candidates
group by country
order by 1 asc

SELECT  forename, Surname, CandidateRef, country
FROM [BC_TPM_Extract_old]..[xxUserTable]
where country != '-1'
	