/****** Script for SelectTopNRows command from SSMS  ******/
SELECT  [ID]
      ,[County]
  FROM [BC_TPM_Extract].[dbo].[Candidates_Full]
  WHERE isnumeric(CAST([County] AS NVARCHAR(MAX))) = 0

/****** Script for SelectTopNRows command from SSMS  ******/
SELECT  [ID]
      ,[Country]
  FROM [BC_TPM_Extract].[dbo].[Candidates_Full]
  WHERE isnumeric(CAST([Country] AS NVARCHAR(MAX))) = 0



UPDATE [Candidates_Full]
SET country = clt.id
--SELECT c.*
FROM [BC_TPM_Extract].[dbo].[Candidates_Full] c
INNER JOIN [dbo].[CountryLookup] clt
ON CAST(clt.[Country] AS NVARCHAR(MAX)) = CAST(c.[Country] AS NVARCHAR(MAX))
WHERE isnumeric(CAST(c.[Country] AS NVARCHAR(MAX))) = 0
  

UPDATE [Candidates_Full]
SET county = clt.id
--SELECT c.*
FROM [BC_TPM_Extract].[dbo].[Candidates_Full] c
INNER JOIN [dbo].[CountyLookup] clt
ON CAST(clt.[County] AS NVARCHAR(MAX)) = CAST(c.[County] AS NVARCHAR(MAX))
WHERE isnumeric(CAST(c.[County] AS NVARCHAR(MAX))) = 0


  SELECT  [ID]
      ,[Country]
  FROM [BC_TPM_Extract].[dbo].[Candidates_Full]
  WHERE isnumeric(CAST([Country] AS NVARCHAR(MAX))) = 0

  