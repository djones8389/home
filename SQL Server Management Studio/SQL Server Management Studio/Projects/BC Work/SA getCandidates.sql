SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

SELECT ID, CandidateRef, Forename, Surname, Middlename, DOB, Gender, AddressLine1, AddressLine2, CAST(Town AS NVARCHAR(MAX)) Town, CAST(County AS NVARCHAR(MAX)) County, CAST(Country AS NVARCHAR(MAX)) Country, PostCode, Telephone, Email, CAST(EthnicOriginid AS NVARCHAR(MAX)) EthnicOriginid, AccountCreationDate, AccountExpiryDate, Retired
FROM usertable
UNION
SELECT USERID, CandidateRef, Forename, Surname, Middlename, DOB, Gender, AddressLine1, AddressLine2, CAST(Town AS NVARCHAR(MAX)) Town, CAST(County AS NVARCHAR(MAX)) County, CAST(Country AS NVARCHAR(MAX)) Country, PostCode, Telephone, Email, CAST(EthnicOrigin AS NVARCHAR(MAX)) EthnicOrigin, AccountCreationDate, AccountExpiryDate, NULL
FROM warehouse_usertable