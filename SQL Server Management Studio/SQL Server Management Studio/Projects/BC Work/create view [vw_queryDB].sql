inner join [QualificationsNew] Q
on Q.QualificationName = c.QualificationName
	and q.QualificationRef = c.QualificationRef
GO
sp_whoisactive

alter view [vw_queryDB]
as
select 
	Keycode
	, CA.CandidateRef
	, CA.FirstName
	, CA.LastName
	, CentreName
	, CS.PackageScore
	, CE.Score
	, Q.*
	, p.Name [PackageName]
	, PE.Name [PackageExamName]
	, tp.PackageDate
from CandidateSchedules CS
inner join CandidateExams CE on CE.CandidateScheduleID = CS.CandidateScheduleID
inner join TestPackages TP on TP.ScheduledPackageID  = CS.ScheduledPackageID 
inner join Centres C on C.CentreID = TP.CentreID
inner join Packages P on P.PackageID = TP.PackageID
inner join PackageExams PE ON PE.PackageExamID = CE.PackageExamId and PE.PackageID = P.PackageID
INNER JOIN Qualifications Q on Q.QualificationID =  tp.QualificationID
INNER JOIN Candidates CA on CA.CandidateID = CS.CandidateID

--where Keycode = 'ETSQ5M01'
