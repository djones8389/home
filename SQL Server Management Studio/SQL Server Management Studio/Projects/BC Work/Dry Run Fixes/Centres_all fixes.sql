DELETE A
--SELECT A.*
FROM (
SELECT  [ID]
      ,[CentreName]
      ,[CentreCode]
      ,[AddressLine1]
      ,[AddressLine2]
      ,[Town]
      ,[County]
      ,[Country]
      ,[PostCode]
	  , ROW_NUMBER() OVER (
			PARTITION BY [ID]
			,[CentreName]
			,[CentreCode]
		  ORDER BY CAST(County AS NVARCHAR(MAX)) ) R
  FROM [Centres]

) A
WHERE R > 1
GO


DELETE A
--SELECT A.*
FROM (
SELECT  [ID]
      ,[CentreName]
      ,[CentreCode]
      ,[AddressLine1]
      ,[AddressLine2]
      ,[Town]
      ,[County]
      ,[Country]
      ,[PostCode]
	  , ROW_NUMBER () OVER(partition by [ID]
      ,[CentreName]
      ,[CentreCode]
      ,[AddressLine1]
      ,[AddressLine2]
      ,[Town]
      ,[County]
      ,[Country]
      ,[PostCode] order by id) r
  FROM [dbo].[Centres]
) A
where r > 1

UPDATE C
SET Country = cast(D.ID as nvarchar(MAX))
FROM Centres C
INNER JOIN CountryLookUpTable D
On cast(D.Country as nvarchar(MAX)) = cast(C.Country as nvarchar(MAX))
Where ISNUMERIC(cast(C.Country as nvarchar(MAX))) = 0


UPDATE C
SET County = cast(D.ID as nvarchar(MAX))
FROM Centres C
INNER JOIN CountyLookUpTable D
On cast(D.County as nvarchar(MAX)) = cast(C.County as nvarchar(MAX))
Where ISNUMERIC(cast(C.County as nvarchar(MAX))) = 0


UPDATE centres
set AddressLine1 = REPLACE(AddressLine1, ',','')
	, AddressLine2 = REPLACE(AddressLine2, ',','')
	, CentreName = REPLACE(AddressLine1, ',','')

UPDATE centres
set AddressLine1 = REPLACE(AddressLine1, '"','')
	, AddressLine2 = REPLACE(AddressLine2, '"','')
	, CentreName = REPLACE(AddressLine1, '"','')


--BEGIN TRAN
----SELECT *
--UPDATE C
--SET Country = cast(D.ID as nvarchar(MAX))
--FROM Centres C
--INNER JOIN CountryLookUpTable D
--On cast(D.Country as nvarchar(MAX)) = cast(C.Country as nvarchar(MAX))
--Where ISNUMERIC(cast(C.Country as nvarchar(MAX))) = 0


--UPDATE C
--SET County = cast(D.ID as nvarchar(MAX))
--FROM Centres C
--INNER JOIN CountyLookUpTable D
--On cast(D.County as nvarchar(MAX)) = cast(C.County as nvarchar(MAX))
--Where ISNUMERIC(cast(C.County as nvarchar(MAX))) = 0


--ROLLBACK