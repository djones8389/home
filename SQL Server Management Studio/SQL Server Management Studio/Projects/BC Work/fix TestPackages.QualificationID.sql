
	--UPDATE TestPackages
	--set QualificationID = qf.aptisqualid
	--FROM TestPackages TP
	--INNER JOIN Qualifications_import QF
	--on QF.QualificationID = TP.QualificationID
	--where QF.Aptisqualid in (

	--	select distinct 
	--		tp.QualificationID
	--	from TestPackages TP
	--	left join Qualifications Q
	--	on Tp.QualificationID = q.QualificationID
	--	where q.QualificationID is null
	--)


begin tran

	--Select * from TestPackages where QualificationID not in(select QualificationID from Qualifications)
	Select * from TestPackages where ScheduledPackageID = 26

	update TestPackages
	SET QualificationID = qf.[QualificationID]
	from TestPackages TP
	LEFT JOIN Qualifications_import QF
	ON QF.[AptisQualID] = TP.QualificationID
	where TP.QualificationID not in ( select QualificationID from Qualifications )

	--Select * from TestPackages where QualificationID not in(select QualificationID from Qualifications)
	Select * from TestPackages where ScheduledPackageID = 26

rollback

select *
from Qualifications_import