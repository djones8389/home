SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

SELECT b.SurpassExamRef [Component Name]
	, b.[Keycode] 
	, Code [CEFR]
	, round([Score], 0)
	,DefaultScaleValue [Component Max Scale Score]
FROM (
	SELECT a.*
		, ROW_NUMBER () OVER (partition by Keycode, SurpassExamRef order by ScoreDiff) R
	FROM (
		select 
			Keycode
			, SurpassExamRef
			, round(spce.Score,0) Score
			,DefaultMinScore
			,round((Score-DefaultMinScore),0) [ScoreDiff]
			, DefaultScaleValue
			,egd.ExamTypeId,egd.[ExamGradeId],eg.*
		from [dbo].[CandidateExams] as SPCE

			inner join CandidateSchedules as SPC
			on SPC.[CandidateScheduleID] =  SPCE.[CandidateScheduleID]
			--inner join Packages as SP
			-- on SP.ScheduledPackageId =  SPC.ScheduledPackage_ScheduledPackageId
			inner join [dbo].[PackageExams_Full] pe
			on pe.[PackageExamId] = SPCE.[PackageExamId]
			inner join [dbo].[Packages_full] pack
			on pack.[PackageId] = pe.[PackageId]
			inner join PackageExamScoreBoundaries pes
			on pes.PackageExamId = pack.PackageId
				and pes.ExamGradeId = ExamTypeId
			left join ExamGradeDescriptions egd
			on pes.examgradeid = egd.[ExamGradeId]
			 left join [dbo].[ExamGrades] EG
			 on EG.Id in (egd.ExamTypeId,egd.[ExamGradeId])
		
		--where ScheduledExamRef In  ('NY9XYM01','5B48C501','YRDJLN01','ENCUVW01')
		where Keycode In  ('246EW901','TTY2ZZ01')
			and Score > DefaultMinScore
			and eg.id = egd.ExamTypeId
	) a
	where [ScoreDiff] > 0 
) b
where R = 1



--select * from Packages
--select * from Packages_full