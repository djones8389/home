--use Aatpreview_ItemBank

--select top 5 DB_NAME()
--	,ID
--	, AssessmentName
--	, ExternalReference
--	, AssessmentRules.value('data(PaperRules//Section [last()]/@ID)[1]','smallint') [No Of Sections]
--	, cast(REPLACE(AssessmentRules.value('sum(PaperRules//Section/@TotalQuestions)','varchar(100)'), '0.0E0','0') as float) [No Of Questions]
--from [dbo].[AssessmentTable] NOLOCK
--order by 5 desc;

use master

IF OBJECT_ID('tempdb..##DATALENGTH') IS NOT NULL DROP TABLE ##DATALENGTH;

CREATE TABLE ##DATALENGTH (
	client nvarchar(1000)
	, ID smallint
	, AssessmentName nvarchar(200)
	, ExternalReference nvarchar(200)
	, [No Of Sections] smallint
	, [No Of Questions] smallint
);

DECLARE @dynamic nvarchar(MAX)='';

select @dynamic +=CHAR(13)+ '
use '+QUOTENAME([NAME])+'

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

INSERT ##DATALENGTH
select top 5 DB_NAME()
	, ID
	, AssessmentName
	, ExternalReference
	, AssessmentRules.value(''data(PaperRules//Section [last()]/@ID)[1]'',''smallint'') [No Of Sections]
	, cast(REPLACE(AssessmentRules.value(''sum(PaperRules//Section/@TotalQuestions)'',''varchar(100)''), ''0.0E0'',''0'') as float) [No Of Questions]
from [dbo].[AssessmentTable] NOLOCK
order by 6 desc;
'
from sys.databases
where state_desc = 'ONLINE'
	and name like '%[_]ItemBank';

exec(@dynamic);



select substring(client,0,CHARINDEX('_',client)) Client
	, ID
	, AssessmentName
	, ExternalReference
	, [No Of Sections]
	, [No Of Questions]
from ##DATALENGTH
order by 1, [No Of Questions] desc