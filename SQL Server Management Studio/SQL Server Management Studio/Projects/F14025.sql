select Keycode
	, examName
	, west.resultData.value('data(exam/@totalMark)[1]','tinyint') TotalMark
	, west.warehouseTime
from WAREHOUSE_ExamSessionTable_Shreded wests (NOLOCK)
inner join WAREHOUSE_ExamSessionTable west (NOLOCK)
on west.id = wests.examsessionID
where  examName = 'VPK-VPMK3 Verpleegkundige en Medische Kennis 3 Summatief 01'
	and west.warehouseTime > '2017-08-21 13:39:55.323'
	--and surname = 'abdallah'