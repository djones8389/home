SELECT A.*
FROM (

SELECT  
	ROW_NUMBER() OVER(PARTITION BY 
		CONVERT(CHAR(100), SERVERPROPERTY('Servername'))
		,b.database_name
		,b.type
		,device_type
	 ORDER BY	 
		CONVERT(CHAR(100), SERVERPROPERTY('Servername'))
		,b.database_name
		,b.type
		,device_type
	 ) R ,
	CONVERT(CHAR(100), SERVERPROPERTY('Servername')) AS Server, 
	b.database_name,  
	b.backup_start_date,  
	b.backup_finish_date, 
	CASE b.type
	   WHEN 'D' THEN 'Database'  
	   WHEN 'I' then 'Differential Database'
	   WHEN 'L' THEN 'Log'
	   WHEN 'F' THEN 'File of filegroup'
	   when 'G' then 'Differential file'
	   when 'P' then 'Partial'
	   when 'Q' then 'Differential partial'
	END AS backup_type,  
	b.compressed_backup_size / 1000000 AS CompressedBackupSize_MB,
	b.compressed_backup_size / 1000000000 AS CompressedBackupSize_GB,
	mf.device_type,
	mf.physical_device_name
FROM   msdb.dbo.backupmediafamily mf
INNER JOIN msdb.dbo.backupset b ON mf.media_set_id = b.media_set_id 
WHERE  b.backup_start_date >= dateadd(day, -3, GETDATE())
	--and b.type = 'D'
	and database_name like '%SecureAssess%'
	AND b.compressed_backup_size > 776972000
)  A
where A.R = 1



SELECT [JobName] = JOB.NAME
	,[Step] = HIST.step_id
	,[StepName] = HIST.step_name
	,[Message] = HIST.message
	,[Status] = CASE 
		WHEN HIST.run_status = 0
			THEN 'Failed'
		WHEN HIST.run_status = 1
			THEN 'Succeeded'
		WHEN HIST.run_status = 2
			THEN 'Retry'
		WHEN HIST.run_status = 3
			THEN 'Canceled'
		END
	,[RunDate] = HIST.run_date
	,[RunTime] = HIST.run_time
	,[Duration] = HIST.run_duration
FROM sysjobs JOB
INNER JOIN sysjobhistory HIST ON HIST.job_id = JOB.job_id
WHERE JOB.NAME = 'Job1'
ORDER BY HIST.run_date
	,HIST.run_time