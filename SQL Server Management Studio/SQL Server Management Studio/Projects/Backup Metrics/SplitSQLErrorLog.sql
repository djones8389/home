IF OBJECT_ID('tempdb..#Log') IS NOT NULL DROP TABLE #Log

CREATE TABLE #Log (
	LogDate datetime
	, ProcessInfo nvarchar(100)
	, Text Nvarchar(1000)
)

INSERT #Log
EXEC sp_readerrorlog 0, 1, 'Back'


DECLARE @string NVARCHAR(1000) = 'Database backed up. Database: ItemBank, creation date(time): 2015/02/13(09:00:34), pages dumped: 506, first LSN: 77:450:1, last LSN: 77:452:1, number of dump devices: 1, device information: (FILE=1, TYPE=DISK: {''ItemBank''}). This is an informational message only. No user action is required.  '
DECLARE @delimiter char(1) = ','
DECLARE @start INT
DECLARE @end INT 
DECLARE @output TABLE (splitdata NVARCHAR(1000))

    SELECT @start = 1
    SELECT @end = CHARINDEX(@delimiter, @string) 
    
    WHILE @start < LEN(@string) + 1 
    BEGIN 
        IF @end = 0  
            SET @end = LEN(@string) + 1
       
        INSERT INTO @output (splitdata)  
        VALUES(SUBSTRING(@string, @start, @end - @start)) 

		--SELECT * FROM @output

        SET @start = @end + 1 
        SET @end = CHARINDEX(@delimiter, @string, @start)
        
    END 

SELECT top 3 

	 --SUBSTRING(splitdata, 1, CHARINDEX(':', splitData)) as [Action]
	--,SUBSTRING(splitData, CHARINDEX(':', splitData)+2, 100) as [Data]
	--SUBSTRING(SUBSTRING(splitData, CHARINDEX(':', splitData)+2, 100), CHARINDEX('(', SUBSTRING(splitData, CHARINDEX(':', splitData)+2, 100)), 100) as TEST
	 REPLACE(REPLACE(SUBSTRING(SUBSTRING(splitData, CHARINDEX(':', splitData)+2, 100), CHARINDEX('(', SUBSTRING(splitData, CHARINDEX(':', splitData)+2, 100)), 100), '(', ''), ')', '')
	--, SUBSTRING(splitData, CHARINDEX(':', splitData)+2, 100) as [Data]
FROM @output    

		