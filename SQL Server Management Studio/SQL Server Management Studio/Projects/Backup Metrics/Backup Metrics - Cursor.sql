USE MSDB;

DECLARE Maintenance CURSOR FOR
select name 
from msdb.dbo.sysjobs
where name like '%Backup%'

DECLARE @JobName NVARCHAR(128);

OPEN Maintenance

FETCH NEXT FROM Maintenance INTO @JobName

WHILE @@FETCH_STATUS = 0

BEGIN

SELECT	
     sjh.server,
     job_name = sj.name,
     sjh.step_name,
     sjh.run_date,
     sjh.run_time,
     sjh.run_duration, *
  FROM msdb.dbo.sysjobhistory  sjh,    msdb.dbo.sysjobs_view sj
  WHERE sj.name = 'Surpass Suite Maintenance Plan.Data Backup'
	and sjh.run_status = 1
	and run_date = CONVERT(nvarchar(10),GETDATE(),112)
	and step_name like '%Backup%'


FETCH NEXT FROM Maintenance INTO @JobName

END
CLOSE Maintenance;
DEALLOCATE Maintenance;