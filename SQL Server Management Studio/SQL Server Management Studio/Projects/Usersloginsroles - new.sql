use master

select srv.name
	, db.name 
	, *
from sys.database_principals DB
left join sys.database_role_members RoleName
on DB.principal_id in(RoleName.member_principal_id, RoleName.role_principal_id)
left join sys.server_principals Srv
on DB.sid = srv.sid
where db.type = 'S'

--sys.database_principals R on R.principal_id = RM.role_principal_id

select * 
from sys.database_role_members RM 
inner join sys.database_principals DB
on DB.principal_id in (RM.member_principal_id, RM.role_principal_id)
--left join sys.server_principals Srv
--on DB.sid = srv.sid


	right join sys.database_principals U on U.principal_id = RM.member_principal_id
	left join sys.database_principals R on R.principal_id = RM.role_principal_id


use PRV_Evolve_ItemBank

select 
		(select SUSER_SNAME (u.sid)) as 'Login'
		, u.name as 'User'
		, r.name  as RoleName
		, CASE WHEN (select SUSER_SNAME (u.sid)) IS NULL and u.name IS NOT NULL THEN '1' else '0' END AS 'Orphaned User'
from sys.database_role_members RM 
	right join sys.database_principals U on U.principal_id = RM.member_principal_id
	left join sys.database_principals R on R.principal_id = RM.role_principal_id



where u.type<>'R'
	and U.type = 'S'
	and u.principal_id > 4


	