SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

SET STATISTICS IO, TIME ON

declare @TagTypeName1 NVARCHAR(100) = 'Generieke Kennisbasis'
		,@TagTypeName2 NVARCHAR(100) = 'Taxonomie Bloom'
		,@TagTypeName3 NVARCHAR(100) = NULL
		,@StvText NVARCHAR(100) = 'One'
		,@count tinyint

SELECT @count = (count(@TagTypeName1) + count(@TagTypeName2) + count(@TagTypeName3));

SELECT DISTINCT i.subjectId, i.Id 
FROM Items i
WHERE I.ID IN (

		select A.ID
		from (	
			SELECT distinct
					i.Id
					,stt.TagTypeName
			FROM Items i
			INNER JOIN SubjectTagValueItems si ON si.Item_Id = i.id
			INNER JOIN SubjectTagValues stv ON si.SubjectTagValue_Id = stv.Id
			INNER JOIN SubjectTagTypes stt on stv.SubjectTagTypeId = stt.Id AND stt.SubjectId = i.SubjectId
			WHERE STT.TagTypeName in (@TagTypeName1,@TagTypeName2,@TagTypeName3)
				--and stv.Text = @StvText
		) A
		group by A.ID
		having count(A.ID) = @count
	)


/*
SELECT i.subjectId, i.Id 
FROM Items i
INNER JOIN SubjectTagValueItems si ON si.Item_Id = i.id
INNER JOIN SubjectTagValues stv ON si.SubjectTagValue_Id = stv.Id
INNER JOIN SubjectTagTypes stt on stv.SubjectTagTypeId = stt.Id AND stt.SubjectId = i.SubjectId
WHERE I.ID IN (208775,208783)


SELECT DISTINCT i.subjectId, i.Id,stv.Text 
FROM Items i
INNER JOIN SubjectTagValueItems si ON si.Item_Id = i.id
INNER JOIN SubjectTagValues stv ON si.SubjectTagValue_Id = stv.Id
INNER JOIN SubjectTagTypes stt on stv.SubjectTagTypeId = stt.Id AND stt.SubjectId = i.SubjectId
WHERE I.ID IN (208775,208783)

*/