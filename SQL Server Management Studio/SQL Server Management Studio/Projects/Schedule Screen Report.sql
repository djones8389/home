SELECT   distinct

	CentreName as [Centre Name]
	, QualificationName as [Qualification]
	, ExamName as [Assessment Name]
	, ut.Forename + ',' + ut.Surname as [Candidate]
	, ut.CandidateRef as [Membership no.]
	, ut.DOB as [Date of Birth]
	, cast(ScheduledStartDateTime as date) as [Start Date]
	, cast(ScheduledEndDateTime as date) as [End Date] 
	, cast(DATEADD(minute, ActiveStartTime, 0) as time(0)) as [Start Time]
	, cast(DATEADD(minute, ActiveEndTime, 0) as time(0)) as [End Time] 
	, CreatedDateTime as [Date Created]
	, ut.Forename + ',' + ut.Surname as [Created By] 
	, case groupState when '1' then 'Editable' else 'Locked'  END as [Status]
	, case invigilated when '0' then 'No' else 'Yes' END   as [Invigilated]
  FROM ExamSessionTable as EST

  Inner Join ScheduledExamsTable as SCET
  on SCET.ID = EST.ScheduledExamID
  
  Inner Join UserTable as UT
  on UT.ID = EST.UserID
  
  INNER JOIN UserTable as WUT
  on WUT.ID = CreatedBy

  Inner Join CentreTable as CT
  on CT.ID = SCET.CentreID

  inner join IB3QualificationLookup as IB
  on IB.ID = qualificationId

where examState in (1,2,3,4,5,6,7,8,9)
--	and UT.CandidateRef = '10298250'
SELECT * FROM EXAMSESSIONTABLE WHERE USERID IN (SELECT ID FROM USERTABLE WHERE CANDIDATEREF = '20000731')

/*EST.examState, pinNumber, examName, EST.KeyCode, CT.CentreName, QualificationName
	 , cast(ScheduledStartDateTime as date) as [Start Date], cast(ScheduledEndDateTime as date) as [End Date]
	 , UT.Forename + ' ' + UT.Surname as [Candidate], ut.CandidateRef, invigilated
	 , cast(DATEADD(minute, ActiveStartTime, 0) as time(0)) as [StartTime]
	 , cast(DATEADD(minute, ActiveEndTime, 0) as time(0)) as [EndTime]

*/