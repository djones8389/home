--Declare @@DBswithwaits table (
--	DBID tinyint
--)

--INSERT @@DBswithwaits
--select database_id
--from sys.dm_exec_requests 
--where status not in ('background','sleeping','running')

exec sp_MSforeachdb '

Declare @@DBswithwaits table (
	DBID tinyint
)

INSERT @@DBswithwaits
select database_id
from sys.dm_exec_requests 
where status not in (''background'',''sleeping'',''running'')

use [?];

if (db_ID() in (select DBID from @@DBswithwaits))
BEGIN

	SELECT A.[Database]
		, t.name [Table Name]
		, text [Query]
		, Status	
		, command
		, wait_type
		, last_wait_type
		, wait_time
		, total_elapsed_time
		, reads
		, writes
	FROM (
		select DB_NAME(database_id) [Database] 
		, session_id
		, status
		, command
		, wait_type
		, last_wait_type
		, wait_time
		, total_elapsed_time
		, reads
		, writes
		, text
		, SUBSTRING(wait_resource, LEN(wait_resource)- CHARINDEX('':'',REVERSE(wait_resource))+2, LEN(wait_resource)- CHARINDEX(''('',wait_resource)+LEN(wait_resource)- CHARINDEX('':'',REVERSE(wait_resource))-3) [WaitResource]
		from sys.dm_exec_requests 
		CROSS APPLY sys.dm_exec_sql_text(sql_handle) st
		where status not in (''background'',''sleeping'',''running'')
	) A
	INNER JOIN sys.partitions P
	on P.partition_id = A.[WaitResource]
	INNER JOIN sys.tables T
	on T.object_id = p.object_id;

END
'



select db_ID()


SELECT A.[Database]
	, t.name [Table Name]
	, text [Query]
	, Status	
	, command
	, wait_type
	, last_wait_type
	, wait_time
	, total_elapsed_time
	, reads
	, writes
FROM (
	select DB_NAME(database_id) [Database] 
	, session_id
	, status
	, command
	, wait_type
	, last_wait_type
	, wait_time
	, total_elapsed_time
	, reads
	, writes
	, text
	, SUBSTRING(wait_resource, LEN(wait_resource)- CHARINDEX(':',REVERSE(wait_resource))+2, LEN(wait_resource)- CHARINDEX('(',wait_resource)+LEN(wait_resource)- CHARINDEX(':',REVERSE(wait_resource))-3) [WaitResource]
	from sys.dm_exec_requests 
	CROSS APPLY sys.dm_exec_sql_text(sql_handle) st
	where status not in ('background','sleeping','running')
) A
INNER JOIN sys.partitions P
on P.partition_id = A.[WaitResource]
INNER JOIN sys.tables T
on T.object_id = p.object_id;


--USE [21BrokenExams]

--select * from sys.tables where name = 'ConditionalJoinExample'

--select * from sys.partitions where object_id = 37575172

--KEY: 11:72057594038910976 (61a06abd401c)


--select * 
--from sys.partitions p
--where partition_id = 72057594038910976


--select * from sys.objects where object_id = 72057594038910976



/*




SELECT CAST(xet.target_data as xml) FROM sys.dm_xe_session_targets xet
JOIN sys.dm_xe_sessions xe
ON (xe.address = xet.event_session_address)
WHERE xe.name = 'system_health'

select * from sys.dm_exec_connections where session_id = 57

select * from sys.dm_exec_sql_text(0x01000B00F6AF321360BF359C030000000000000000000000)



select * from sys.dm_exec_query_stats order by last_execution_time desc

select row_lock_wait_in_ms, t.name, * 
from sys.dm_db_index_operational_stats  (db_id(), null, null, null)	
inner join sys.tables t
on t.object_id = sys.dm_db_index_operational_stats.object_id
order by sys.dm_db_index_operational_stats.row_lock_wait_in_ms desc


select * from sys.dm_tran_locks

Declare @session table (
	sessionID tinyint
)

INSERT @session
select session_id
from sys.dm_exec_requests 
where status not in ('background','sleeping','running')

UNION

select request_session_id
from sys.dm_tran_locks
where request_status != 'Grant'

--select substring(resource_description, CHARINDEX('associatedObjectId',resource_description)+19, 100)
--from sys.dm_os_waiting_tasks 
--where session_id = 58

sp_whoisactive

select * from sys.dm_tran_locks
where request_status != 'Grant'


select * from  sys.dm_tran_locks
where request_status != 'Grant'


select
	 
	, t.name
	, i.name
from sys.partitions P
INNER JOIN sys.tables T
on T.object_id = P.object_id
inner join sys.indexes i 
on i.object_id = p.object_id
where partition_id IN (

)


select *
from sys.dm_os_waiting_tasks 
	where session_id = 58


select * 
from sys.dm_exec_connections

where session_id = 58

select most_recent_sql_handle from sys.dm_exec_connections where session_id = 58

select * from sys.dm_exec_sql_text(0x02000000D4A1ED04A1E451503E925719FE8D6F3C58958373)

select request_session_id  [SPID]
	,	db_Name(DB_ID())   [DBName]
	,  i.name  [Index Name]
	, wait_duration_ms / 1000 / 60 as [Wait time in minutes]
	, st.text
from  sys.dm_tran_locks
inner join sys.partitions P
on p.partition_id = resource_associated_entity_id
inner join sys.tables t
on t.object_id = p.object_id
left join sys.indexes i 
on i.index_id = p.index_id
	and i.object_id = p.object_id
left join sys.dm_os_waiting_tasks  W
on W.session_id = request_session_id
left join sys.dm_exec_connections C
on c.session_id = request_session_id
CROSS APPLY sys.dm_exec_sql_text(most_recent_sql_handle) st
where request_status != 'Grant'



/*



select %%LockRes%%
from ConditionalJoinExample
where %%LockRes%% = '(8194443284a0)'

	select substring(resource_description, CHARINDEX('associatedObjectId',resource_description)+19, 100)
	from sys.dm_os_waiting_tasks 
	where session_id = 58
	*/


SELECT TOP 5 total_worker_time/execution_count AS [Avg CPU Time]
	,
    SUBSTRING(st.text, (qs.statement_start_offset/2)+1, 
        ((CASE qs.statement_end_offset
          WHEN -1 THEN DATALENGTH(st.text)
         ELSE qs.statement_end_offset
         END - qs.statement_start_offset)/2) + 1) AS statement_text
		 ,st.text
		 
FROM sys.dm_exec_query_stats AS qs
CROSS APPLY sys.dm_exec_sql_text(qs.sql_handle) AS st
ORDER BY total_worker_time/execution_count DESC;


*/