/****** Script for SelectTopNRows command from SSMS  ******/
SELECT SUBSTRING([database_Name], 0,CHARINDEX('_',[database_Name])) [database_Name]
,[database_Name]
      , count(user_forename) [No. of Candidates]
  FROM [PSCollector].[dbo].[Darren.UserLogins]
  
  where AccountExpiryDate > getDate()
		and user_disabled = 0
		AND isCandidate = 1
		and (
			user_forename not like '%btl%' 
			or
			user_surname not like '%btl%'
			or
			[user_email] not like '%btl%'
		)
GROUP BY [database_Name]


--SELECT DISTINCT
--	isCandidate
--	  FROM [PSCollector].[dbo].[Darren.UserLogins]