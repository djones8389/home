SET STATISTICS IO ON

SELECT DB_NAME()
	,COUNT(CT.ID) [TotalCentres]
	,(
		SELECT COUNT(CT.ID)
		FROM CentreTable CT
		INNER JOIN CountryLookupTable CLT ON CLT.id = CT.Country
		WHERE CentreName NOT LIKE '%BTL%'
			AND Retired = 0
			AND clt.Country IN (
				'England'
				,'Northern Ireland'
				,'Scotland'
				,'Wales'
				)
		) [UK]
	,(
		SELECT COUNT(CT.ID)
		FROM CentreTable CT
		INNER JOIN CountryLookupTable CLT ON CLT.id = CT.Country
		WHERE CentreName NOT LIKE '%BTL%'
			AND Retired = 0
			AND clt.Country NOT IN (
				'England'
				,'Northern Ireland'
				,'Scotland'
				,'Wales'
				,'Unknown'
				)
		) [Non-UK]
			,(
		SELECT COUNT(CT.ID)
		FROM CentreTable CT
		INNER JOIN CountryLookupTable CLT ON CLT.id = CT.Country
		WHERE CentreName NOT LIKE '%BTL%'
			AND Retired = 0
			AND clt.Country IN ('Unknown')
		) [Unknown]
FROM CentreTable CT
INNER JOIN CountryLookupTable CLT ON CLT.id = CT.Country
WHERE ct.CentreName NOT LIKE '%BTL%'
	AND Retired = 0;
	

/*
	ID	Country
	0	England
	1	Northern Ireland
	2	Scotland
	3	Wales


'England','Northern Ireland','Scotland','Wales'
*/


	--, CASE clt.Country when  'England' THEN 'UK'
	--		when 'Northern Ireland' THEN 'UK' 
	--		when 'Scotland' THEN 'UK'
	--		when 'Wales' THEN 'UK' 
	--		when 'Unknown' THEN 'Unknown' 
	--		ELSE clt.Country END AS 'Non-UK'