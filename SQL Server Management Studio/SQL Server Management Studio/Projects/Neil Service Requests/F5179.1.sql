SELECT [ServerName]
      ,substring([DBName], 0, CHARINDEX('_',[DBName])) [DBName]
      ,[MonthNumberOfYear]
      ,[DayNumberOfMonth] [Day Number in Month]
	  ,sum([ExamCounter]) [ExamCounter]	  
FROM [PSCollector].[dbo].[F5179 - exams by date SDW]
where [MonthNumberOfYear] = 1
	
group by
	  [ServerName]
      ,[DBName]      
      ,[MonthNumberOfYear]
      ,[DayNumberOfMonth]
	
	order by 2



--SELECT *
--FROM [PSCollector].[dbo].[F5179 - exams by date SDW]
--where [MonthNumberOfYear] = 1
--	and servername = '335657-APP1'
--	and [DayNumberOfMonth] = 1