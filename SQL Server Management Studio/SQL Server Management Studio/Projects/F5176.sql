USE [PPD_BCIntegration_SecureAssess]

SELECT TOP 1000 [ID]
      ,[WAREHOUSEExamSessionID]
      ,[ItemID]
      ,[ItemVersion]
      ,[ItemResponseData]
      ,[MarkerResponseData]
      ,[ItemMark]
      ,[MarkingIgnored]
      ,[DerivedResponse]
      ,[ShortDerivedResponse]
  FROM [PPD_BCGuildsIntegration_SecureAssess].[dbo].[WAREHOUSE_ExamSessionItemResponseTable]
  where WAREHOUSEExamSessionID = 825


  WHEN '0' then 'Computer Marking'
		WHEN '1' then 'Human Marking'

  
SELECT 
	DBName
	, YYYY
	, markingType
	, case totalMark when (ISNUMERIC(totalMark) = 1) then '11111' end as '3rr33r'
FROM (  
  
  SELECT 
	 db_NAME() [DBName]
	 , YEAR(WarehouseTime) [YYYY]	 
	 ,  CASE a.b.value('@markingType','nvarchar(10)')  
			when '0' then 'Computer Marking'
			WHen '1' then 'Human Marking'
			ELSE a.b.value('@markingType','nvarchar(10)')  
		END AS markingType
	 ,  CASE a.b.value('@totalMark','nvarchar(10)') 
			WHEN '0' THEN 'Not Marked' 
			ELSE a.b.value('@totalMark','nvarchar(10)')
		END AS  totalMark
  FROM WAREHOUSE_ExamSessionTable
  CROSS APPLY ResultData.nodes('exam/section/item') a(b)
  WHERE ID IN (1788426,1788425,1788424,1788423,1788422)

) C




--	 ,  a.b.value('@markingType','int')  markingType

  select 
		db_NAME()
	  , YEAR(WarehouseTime) [Year]
	  , sum(resultData.value('count(exam/section/item[@markingType=0])[1]','int')) [Computer Marking]
	  , sum(resultData.value('count(exam/section/item[@markingType=1])[1]','int')) [Human Marking]	  
  from WAREHOUSE_ExamSessionTable
  group by YEAR(WarehouseTime)

  /*
  	when DimMarkingTypes.[MarkingType] = ''Computer Marking'' THEN ''Computer Marking''
			when DimMarkingTypes.[MarkingType] = ''Human Marking'' THEN ''Human Marking''
			when DimMarkingTypes.[MarkingType] = ''Not Marked'' THEN ''Not Marked'' 
			when DimMarkingTypes.[MarkingType] = ''Smart Marking'' THEN ''Computer Marking'' 
			*/