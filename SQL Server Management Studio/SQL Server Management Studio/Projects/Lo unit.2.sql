USE SANDBOX_Abcawards_SecureAssess

if OBJECT_ID('tempdb..#ResultHolder') IS NOT NULL DROP TABLE #ResultHolder;

SELECT
	ID [ExamSessionID]
	, KeyCode
	, warehouseTime
	, StructureXML.value('data(assessmentDetails/qualificationName)[1]','nvarchar(255)') [QualificationName]
	, a.b.value('@id','nvarchar(20)') [ItemID]
	, a.b.value('@name','nvarchar(100)') [ItemName]
	, a.b.value('@unit','nvarchar(100)') [unit]
	, a.b.value('@LO','nvarchar(100)') [LO]
	, structureXML
	, resultDataFull
--INTO #ResultHolder
from [SANDBOX_Abcawards_SecureAssess]..WAREHOUSE_ExamSessionTable SA
cross apply structurexml.nodes('assessmentDetails/assessment/section/item') a(b)
where a.b.value('@LO','nvarchar(max)') = ''
	and a.b.value('@unit','nvarchar(max)') != ''
	and KeyCode = 'GK68XBSU'


Select *
from WAREHOUSE_ExamSessionTable_ShrededItems
where examSessionId = 1285
	and ItemRef = '5084p5268'







select 
	i.ExamSessionID
	, i.KeyCode
	, i.warehouseTime
	, i.QualificationName
	, i.ItemID
	, i.ItemName
	, i.unit
	, i.LO
	, [Text]
	, MultipleValuesAllowed
from #ResultHolder I
inner join [SANDBOX_Abcawards_ContentAuthor]..SubjectTagValueItems on SubjectTagValueItems.Item_Id = substring(I.ItemID, CHARINDEX('P',I.ItemID)+1, 10)
inner join [SANDBOX_Abcawards_ContentAuthor]..SubjectTagValues on SubjectTagValues.id = SubjectTagValueItems.SubjectTagValue_Id
inner join [SANDBOX_Abcawards_ContentAuthor]..SubjectTagTypes on SubjectTagTypes.Id = SubjectTagValues.SubjectTagTypeId
inner join [SANDBOX_Abcawards_ContentAuthor]..Subjects on subjects.title = I.QualificationName
where TagTypeKey = 1
	order by ItemID;





