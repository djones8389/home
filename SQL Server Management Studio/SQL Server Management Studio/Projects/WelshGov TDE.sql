-- Enable advanced options.  

	sp_configure 'show advanced options', 1 ;  
	GO  
	RECONFIGURE ;  
	GO  

-- Enable EKM provider  

	sp_configure 'EKM provider enabled', 1 ;  
	GO  
	RECONFIGURE ;  
	GO  

	CREATE CRYPTOGRAPHIC PROVIDER AzureKeyVault_EKM_Prov   
	FROM FILE = 'C:\Program Files\SQL Server Connector for Microsoft Azure Key Vault\Microsoft.AzureKeyVaultService.EKM.dll' ;  
	GO  

	CREATE CREDENTIAL welsh_gov_ekm_credential   
	WITH IDENTITY = 'WelshGov',   	--Whatever you have called it in Azure
	SECRET = 'Application-ID (minus hyphens) + Secret'   
	FOR CRYPTOGRAPHIC PROVIDER AzureKeyVault_EKM_Prov ;  
	GO  

--Create EKM Login

	CREATE LOGIN ASESU_EKM_Login  with password = 'myPassword'
	ADD CREDENTIAL welsh_gov_ekm_credential;  
	GO  

	USE master ;  
	GO  

--Create Asymmetric Key

	CREATE ASYMMETRIC KEY ASESUTDE   
	FROM PROVIDER [AzureKeyVault_EKM_Prov]  
	WITH ALGORITHM = RSA_512,  
		PROVIDER_KEY_NAME = 'WelshGov'   	--Whatever you have called it in Azure
		,CREATION_DISPOSITION = OPEN_EXISTING
	GO  

--Add credential to logins

	ALTER LOGIN Asesu_AnalyticsManagementUser   
	ADD CREDENTIAL welsh_gov_ekm_credential ;  
	GO  

	ALTER LOGIN Asesu_ContentAuthorUser 
	ADD CREDENTIAL welsh_gov_ekm_credential ;  
	GO  

	ALTER LOGIN Asesu_ItemBankUser   
	ADD CREDENTIAL welsh_gov_ekm_credential ;  
	GO  

	ALTER LOGIN Asesu_SecureAssessUser   
	ADD CREDENTIAL welsh_gov_ekm_credential ;  
	GO  

	ALTER LOGIN Asesu_SurpassManagementUser   
	ADD CREDENTIAL welsh_gov_ekm_credential ;  
	GO  

	ALTER LOGIN Asesu_ETLUser   
	ADD CREDENTIAL welsh_gov_ekm_credential ;  
	GO  


-- Create the database encryption key that will be used for TDE.  

	USE [Asesu_AnalyticsManagement] ;  
	GO  
	CREATE DATABASE ENCRYPTION KEY  
	WITH ALGORITHM  = AES_128  
	ENCRYPTION BY SERVER ASYMMETRIC KEY ASESUTDE ;  
	GO  

	USE [Asesu_ContentAuthor] ;  
	GO  
	CREATE DATABASE ENCRYPTION KEY  
	WITH ALGORITHM  = AES_128  
	ENCRYPTION BY SERVER ASYMMETRIC KEY ASESUTDE ;  
	GO  

	USE [Asesu_ItemBank] ;  
	GO  
	CREATE DATABASE ENCRYPTION KEY  
	WITH ALGORITHM  = AES_128  
	ENCRYPTION BY SERVER ASYMMETRIC KEY ASESUTDE ;  
	GO  

	USE [Asesu_SecureAssess] ;  
	GO  
	CREATE DATABASE ENCRYPTION KEY  
	WITH ALGORITHM  = AES_128  
	ENCRYPTION BY SERVER ASYMMETRIC KEY ASESUTDE ;  
	GO  

	USE [Asesu_SurpassDataWarehouse] ;  
	GO  
	CREATE DATABASE ENCRYPTION KEY  
	WITH ALGORITHM  = AES_128  
	ENCRYPTION BY SERVER ASYMMETRIC KEY ASESUTDE ;  
	GO  

	USE [Asesu_SurpassManagement] ;  
	GO  
	CREATE DATABASE ENCRYPTION KEY  
	WITH ALGORITHM  = AES_128  
	ENCRYPTION BY SERVER ASYMMETRIC KEY ASESUTDE ;  
	GO  


-- Alter the database to enable transparent data encryption.  

	ALTER DATABASE [Asesu_AnalyticsManagement]   
	SET ENCRYPTION ON ;  
	GO  

	ALTER DATABASE [Asesu_ContentAuthor]
	SET ENCRYPTION ON ;  
	GO 

	ALTER DATABASE [Asesu_ItemBank]   
	SET ENCRYPTION ON ;  
	GO  

	ALTER DATABASE [Asesu_SecureAssess]
	SET ENCRYPTION ON ;  
	GO 

	ALTER DATABASE [Asesu_SurpassDataWarehouse]   
	SET ENCRYPTION ON ;  
	GO  

	ALTER DATABASE [Asesu_SurpassManagement]
	SET ENCRYPTION ON ;  
	GO 