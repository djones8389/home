USE [STG_SANDBOX_SQA_CPProjectAdmin]

SELECT b.name as 'Project Name'
	--,Items.id
	--, items.*
	,SUBSTRING(Items.ID, CHARINDEX('P', Items.ID) + 1, CHARINDEX('P', Items.ID)) 'Page Number'
FROM
(
      Select i.id, i.ver, i.moD, it.aLI
      from ItemGraphicTable it 
            INNER JOIN ComponentTable c ON c.ID = it.ParentID 
            INNER JOIN SceneTable s ON s.ID = c.ParentID 
            INNER JOIN PageTable i ON i.id = S.ParentID
				where ali = ''
					and orf !=''
      UNION
      Select i.id, i.ver, i.moD, it.aLI 
      from ItemVideoTable it 
            INNER JOIN ComponentTable c ON c.ID = it.ParentID 
            INNER JOIN SceneTable s ON s.ID = c.ParentID 
            INNER JOIN PageTable i ON i.id = S.ParentID
				where ali = ''
      UNION
      Select i.id, i.ver, i.moD, it.aLI
            from ItemHotSpotTable it 
            INNER JOIN ComponentTable c ON c.ID = it.ParentID 
            INNER JOIN SceneTable s ON s.ID = c.ParentID 
            INNER JOIN PageTable i ON i.id = S.ParentID
            where ali = ''
				and grt != ''
      UNION
      Select i.id, i.ver, i.moD, it.aLI
      from ItemCustomQuestionTable it 
            INNER JOIN ComponentTable c ON c.ID = it.ParentID 
            INNER JOIN SceneTable s ON s.ID = c.ParentID 
            INNER JOIN PageTable i ON i.id = S.ParentID
             where ali = ''
				and ext != 'linkingboxes'
) AS Items
INNER JOIN ProjectListTable B
on SUBSTRING(Items.ID, 0,CHARINDEX('P', Items.ID)) = B.ID

where ali = ''
order by 1,2



select *
from ItemGraphicTable
where id like '1057p1171%'

select *
from ItemHotSpotTable
where id like '834p2713%'

select *
from ItemCustomQuestionTable
where id like'%1041P2458%'
	and ext !

Select i.id, i.ver, i.moD, it.aLI, *
from ItemHotSpotTable it 
INNER JOIN ComponentTable c ON c.ID = it.ParentID 
INNER JOIN SceneTable s ON s.ID = c.ParentID 
INNER JOIN PageTable i ON i.id = S.ParentID
where ali = ''
	and grt !=''