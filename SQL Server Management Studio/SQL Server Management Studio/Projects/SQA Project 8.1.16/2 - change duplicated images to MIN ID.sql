SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

SELECT  ROW_NUMBER() OVER (

		  PARTITION BY  A.ProjectId
					,  A.name
					,  DATALENGTH(Image)  				 
		   ORDER BY    A.ProjectId
					,  A.name
				    ,  A.ID
					,  DATALENGTH(Image)  
		) RowNum
	,  A.ID	
 	,  A.ProjectId
	,  A.name
	,  DATALENGTH(Image)  as DL  
	,  delCol
	,  location
INTO #ItemGraphicTableRecords
FROM ProjectManifestTable A
where location not like '%background%'



/*Pages using shared lib items that have duplicates in PMT*/

--SELECT IGT.ID
--	, IGT.ali
--	, A.ID
--	, A.ProjectID
--FROM ItemGraphicTable IGT
--INNER JOIN #ItemGraphicTableRecords A
--ON A.ProjectID = SUBSTRING(IGT.ID, 0, CHARINDEX('P', IGT.ID))
--	AND A.ID = IGT.ALI
	
--WHERE ISNUMERIC(IGT.ALI) = 1
--	AND  RowNum > 1;
	
/*Pages using shared lib items that have duplicates in PMT - and not using the minimum record*/	
	
SELECT IGT.ID
	--, IGT.ali
	, A.ID as 'oldID'
	, b.id as 'newID'
	, A.ProjectID
	, C.Name
	, SUBSTRING(IGT.ID, CHARINDEX('P', IGT.ID) + 1, CHARINDEX('S',IGT.ID) - CHARINDEX('P', IGT.ID) - 2 + Len('S')) 'Page Number'
	, 'UPDATE ItemGraphicTable SET ali = ' + CONVERT(nvarchar(6),B.ID) + ' where ID = ''' + CONVERT(nvarchar(30),IGT.ID) + '''' + ';' as UpdateScript
	, 'UPDATE ItemGraphicTable SET ali = ' + CONVERT(nvarchar(6),IGT.ali) + ' where ID = ''' + CONVERT(nvarchar(30),IGT.ID) + '''' + ';' as RollbackScript
FROM ItemGraphicTable IGT

INNER JOIN #ItemGraphicTableRecords A
ON A.ProjectID = SUBSTRING(IGT.ID, 0, CHARINDEX('P', IGT.ID))
	AND A.ID = IGT.ALI

LEFT JOIN (
	SELECT 
		  ID
		, projectid 
		, name
		, delcol
		, DL
FROM #ItemGraphicTableRecords 
where RowNum = 1
	and delCOL = 0	

) B

ON A.projectID = B.ProjectID
	AND a.Name = B.Name
	
INNER JOIN ProjectListTable as C
on C.ID = A.ProjectID
	
WHERE ISNUMERIC(IGT.ALI) = 1
	AND RowNum > 1
	AND A.DL = B.DL
	AND A.ID != B.ID;	



SELECT  ROW_NUMBER() OVER (

		  PARTITION BY  A.ProjectId
					,  A.name
					,  DATALENGTH(Image)  				 
		   ORDER BY    A.ProjectId
					,  A.name
				    ,  A.ID
					,  DATALENGTH(Image)  
		) RowNum
	,  A.ID	
 	,  A.ProjectId
	,  A.name
	,  DATALENGTH(Image)  as DL  
	,  delCol
	,  location
INTO #ItemCustomQuestionTable
FROM ProjectManifestTable A
where location not like '%background%'



/*Pages using shared lib items that have duplicates in PMT*/

--SELECT IGT.ID
--	, IGT.ali
--	, A.ID
--	, A.ProjectID
--FROM ItemCustomQuestionTable IGT
--INNER JOIN #ItemCustomQuestionTable A
--ON A.ProjectID = SUBSTRING(IGT.ID, 0, CHARINDEX('P', IGT.ID))
--	AND A.ID = IGT.ALI
	
--WHERE ISNUMERIC(IGT.ALI) = 1
--	AND  RowNum > 1;
	
/*Pages using shared lib items that have duplicates in PMT - and not using the minimum record*/	
	
SELECT IGT.ID
	--, IGT.ali
	, A.ID as 'oldID'
	, b.id as 'newID'
	, A.ProjectID
	, C.Name
	, SUBSTRING(IGT.ID, CHARINDEX('P', IGT.ID) + 1, CHARINDEX('S',IGT.ID) - CHARINDEX('P', IGT.ID) - 2 + Len('S')) 'Page Number'
	, 'UPDATE ItemCustomQuestionTable SET ali = ' + CONVERT(nvarchar(6),B.ID) + ' where ID = ''' + CONVERT(nvarchar(30),IGT.ID) + '''' + ';' as UpdateScript
	, 'UPDATE ItemCustomQuestionTable SET ali = ' + CONVERT(nvarchar(6),IGT.ali) + ' where ID = ''' + CONVERT(nvarchar(30),IGT.ID) + '''' + ';' as RollbackScript
FROM ItemCustomQuestionTable IGT

INNER JOIN #ItemCustomQuestionTable A
ON A.ProjectID = SUBSTRING(IGT.ID, 0, CHARINDEX('P', IGT.ID))
	AND A.ID = IGT.ALI

LEFT JOIN (
	SELECT 
		  ID
		, projectid 
		, name
		, delcol
		, DL
FROM #ItemCustomQuestionTable 
where RowNum = 1
	and delCOL = 0	

) B

ON A.projectID = B.ProjectID
	AND a.Name = B.Name
	
INNER JOIN ProjectListTable as C
on C.ID = A.ProjectID
	
WHERE ISNUMERIC(IGT.ALI) = 1
	AND RowNum > 1
	AND A.DL = B.DL
	AND A.ID != B.ID;	









SELECT  ROW_NUMBER() OVER (

		  PARTITION BY  A.ProjectId
					,  A.name
					,  DATALENGTH(Image)  				 
		   ORDER BY    A.ProjectId
					,  A.name
				    ,  A.ID
					,  DATALENGTH(Image)  
		) RowNum
	,  A.ID	
 	,  A.ProjectId
	,  A.name
	,  DATALENGTH(Image)  as DL  
	,  delCol
	,  location
INTO #ItemHotSpotTable
FROM ProjectManifestTable A
where location not like '%background%'



/*Pages using shared lib items that have duplicates in PMT*/

--SELECT IGT.ID
--	, IGT.ali
--	, A.ID
--	, A.ProjectID
--FROM ItemHotSpotTable IGT
--INNER JOIN #ItemHotSpotTable A
--ON A.ProjectID = SUBSTRING(IGT.ID, 0, CHARINDEX('P', IGT.ID))
--	AND A.ID = IGT.ALI
	
--WHERE ISNUMERIC(IGT.ALI) = 1
--	AND  RowNum > 1;
	
/*Pages using shared lib items that have duplicates in PMT - and not using the minimum record*/	
	
SELECT IGT.ID
	--, IGT.ali
	, A.ID as 'oldID'
	, b.id as 'newID'
	, A.ProjectID
	, C.Name
	, SUBSTRING(IGT.ID, CHARINDEX('P', IGT.ID) + 1, CHARINDEX('S',IGT.ID) - CHARINDEX('P', IGT.ID) - 2 + Len('S')) 'Page Number'
	, 'UPDATE ItemHotSpotTable SET ali = ' + CONVERT(nvarchar(6),B.ID) + ' where ID = ''' + CONVERT(nvarchar(30),IGT.ID) + '''' + ';' as UpdateScript
	, 'UPDATE ItemHotSpotTable SET ali = ' + CONVERT(nvarchar(6),IGT.ali) + ' where ID = ''' + CONVERT(nvarchar(30),IGT.ID) + '''' + ';' as RollbackScript
FROM ItemHotSpotTable IGT

INNER JOIN #ItemHotSpotTable A
ON A.ProjectID = SUBSTRING(IGT.ID, 0, CHARINDEX('P', IGT.ID))
	AND A.ID = IGT.ALI

LEFT JOIN (
	SELECT 
		  ID
		, projectid 
		, name
		, delcol
		, DL
FROM #ItemHotSpotTable
where RowNum = 1
	and delCOL = 0	

) B

ON A.projectID = B.ProjectID
	AND a.Name = B.Name
	
INNER JOIN ProjectListTable as C
on C.ID = A.ProjectID
	
WHERE ISNUMERIC(IGT.ALI) = 1
	AND RowNum > 1
	AND A.DL = B.DL
	AND A.ID != B.ID;	





SELECT  ROW_NUMBER() OVER (

		  PARTITION BY  A.ProjectId
					,  A.name
					,  DATALENGTH(Image)  				 
		   ORDER BY    A.ProjectId
					,  A.name
				    ,  A.ID
					,  DATALENGTH(Image)  
		) RowNum
	,  A.ID	
 	,  A.ProjectId
	,  A.name
	,  DATALENGTH(Image)  as DL  
	,  delCol
	,  location
INTO #ItemVideoTable
FROM ProjectManifestTable A
where location not like '%background%'



/*Pages using shared lib items that have duplicates in PMT*/

--SELECT IGT.ID
--	, IGT.ali
--	, A.ID
--	, A.ProjectID
--FROM ItemVideoTable IGT
--INNER JOIN #ItemVideoTable A
--ON A.ProjectID = SUBSTRING(IGT.ID, 0, CHARINDEX('P', IGT.ID))
--	AND A.ID = IGT.ALI
	
--WHERE ISNUMERIC(IGT.ALI) = 1
--	AND  RowNum > 1;
	
/*Pages using shared lib items that have duplicates in PMT - and not using the minimum record*/	
	
SELECT IGT.ID
	--, IGT.ali
	, A.ID as 'oldID'
	, b.id as 'newID'
	, A.ProjectID
	, C.Name
	, SUBSTRING(IGT.ID, CHARINDEX('P', IGT.ID) + 1, CHARINDEX('S',IGT.ID) - CHARINDEX('P', IGT.ID) - 2 + Len('S')) 'Page Number'
	, 'UPDATE ItemVideoTable SET ali = ' + CONVERT(nvarchar(6),B.ID) + ' where ID = ''' + CONVERT(nvarchar(30),IGT.ID) + '''' + ';' as UpdateScript
	, 'UPDATE ItemVideoTable SET ali = ' + CONVERT(nvarchar(6),IGT.ali) + ' where ID = ''' + CONVERT(nvarchar(30),IGT.ID) + '''' + ';' as RollbackScript
FROM ItemVideoTable IGT

INNER JOIN #ItemVideoTable A
ON A.ProjectID = SUBSTRING(IGT.ID, 0, CHARINDEX('P', IGT.ID))
	AND A.ID = IGT.ALI

LEFT JOIN (
	SELECT 
		  ID
		, projectid 
		, name
		, delcol
		, DL
FROM #ItemVideoTable
where RowNum = 1
	and delCOL = 0	

) B

ON A.projectID = B.ProjectID
	AND a.Name = B.Name
	
INNER JOIN ProjectListTable as C
on C.ID = A.ProjectID
	
WHERE ISNUMERIC(IGT.ALI) = 1
	AND RowNum > 1
	AND A.DL = B.DL
	AND A.ID != B.ID;	
	
DROP TABLE #ItemCustomQuestionTable;
DROP TABLE #ItemHotSpotTable;
DROP TABLE #ItemGraphicTableRecords;
DROP TABLE #ItemVideoTable;