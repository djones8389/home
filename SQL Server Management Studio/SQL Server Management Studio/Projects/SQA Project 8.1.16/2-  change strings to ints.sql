SET STATISTICS IO ON

Select i.id
	, i.ver
	, i.moD
	, it.aLI
	, name
	, pmt.id
	, 'UPDATE ItemGraphicTable set ali = ''' + CONVERT(nvarchar(10),pmt.id) + ''' where ID = '''+ CONVERT(nvarchar(255),It.ID) +'''' + ';' as UpdateScript
	, 'UPDATE ItemGraphicTable set ali = ''' + it.ali + ''' where ID = '''+ CONVERT(nvarchar(255),It.ID) +'''' + ';' as RollbackScript
from ItemGraphicTable it 
INNER JOIN ComponentTable c ON c.ID = it.ParentID 
INNER JOIN SceneTable s ON s.ID = c.ParentID 
INNER JOIN PageTable i ON i.id = S.ParentID
INNER JOIN ProjectManifestTable PMT
on PMT.ProjectId =  SUBSTRING(it.ID, 0, CHARINDEX('P', it.ID))
where ISNUMERIC(aLI) = 0
	AND PMT.name NOT LIKE '%swf'
	AND aLI != ''
	AND ali = SUBSTRING(Name, 0, CHARINDEX('.', Name));

Select i.id
	, i.ver
	, i.moD
	, it.aLI
	, name
	, pmt.id
	, 'UPDATE ItemVideoTable set ali = ''' + CONVERT(nvarchar(10),pmt.id) + ''' where ID = '''+ CONVERT(nvarchar(255),It.ID) +'''' + ';' as UpdateScript
	, 'UPDATE ItemVideoTable set ali = ''' + it.ali + ''' where ID = '''+ CONVERT(nvarchar(255),It.ID) +'''' + ';' as RollbackScript
from ItemVideoTable it 
INNER JOIN ComponentTable c ON c.ID = it.ParentID 
INNER JOIN SceneTable s ON s.ID = c.ParentID 
INNER JOIN PageTable i ON i.id = S.ParentID
INNER JOIN ProjectManifestTable PMT
on PMT.ProjectId =  SUBSTRING(it.ID, 0, CHARINDEX('P', it.ID))
where ISNUMERIC(aLI) = 0
	AND PMT.name NOT LIKE '%swf'
	AND aLI != ''
	AND ali = SUBSTRING(Name, 0, CHARINDEX('.', Name));

Select i.id
	, i.ver
	, i.moD
	, it.aLI
	, name
	, pmt.id
	, 'UPDATE ItemHotSpotTable set ali = ''' + CONVERT(nvarchar(10),pmt.id) + ''' where ID = '''+ CONVERT(nvarchar(255),It.ID) +'''' + ';' as UpdateScript
	, 'UPDATE ItemHotSpotTable set ali = ''' + it.ali + ''' where ID = '''+ CONVERT(nvarchar(255),It.ID) +'''' + ';' as RollbackScript
from ItemHotSpotTable it 
INNER JOIN ComponentTable c ON c.ID = it.ParentID 
INNER JOIN SceneTable s ON s.ID = c.ParentID 
INNER JOIN PageTable i ON i.id = S.ParentID
INNER JOIN ProjectManifestTable PMT
on PMT.ProjectId =  SUBSTRING(it.ID, 0, CHARINDEX('P', it.ID))
where ISNUMERIC(aLI) = 0
	AND PMT.name NOT LIKE '%swf'
	AND aLI != ''
	AND ali = SUBSTRING(Name, 0, CHARINDEX('.', Name));


Select i.id
	, i.ver
	, i.moD
	, it.aLI
	, name
	, pmt.id
	, 'UPDATE ItemCustomQuestionTable set ali = ''' + CONVERT(nvarchar(10),pmt.id) + ''' where ID = '''+ CONVERT(nvarchar(255),It.ID) +'''' + ';' as UpdateScript
	, 'UPDATE ItemCustomQuestionTable set ali = ''' + it.ali + ''' where ID = '''+ CONVERT(nvarchar(255),It.ID) +'''' + ';' as RollbackScript
from ItemCustomQuestionTable it 
INNER JOIN ComponentTable c ON c.ID = it.ParentID 
INNER JOIN SceneTable s ON s.ID = c.ParentID 
INNER JOIN PageTable i ON i.id = S.ParentID
INNER JOIN ProjectManifestTable PMT
on PMT.ProjectId =  SUBSTRING(it.ID, 0, CHARINDEX('P', it.ID))
where ISNUMERIC(aLI) = 0
	AND PMT.name NOT LIKE '%swf'
	AND aLI != ''
	AND ali = SUBSTRING(Name, 0, CHARINDEX('.', Name));


