use Saxion_ItemBank

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

--Breakdown

select AssessmentName
	, ExternalReference
	, a.b.value('data(@ID)[1]','tinyint') SectionNumber
	, a.b.value('data(@TotalQuestions)[1]','tinyint') TotalQuestions
	, IsValid [Valid]
from AssessmentTable 
cross apply assessmentrules.nodes('PaperRules/Section') a(b)
where AssessmentStatus = 2
order by 1;	



--Summary--

select AssessmentName
	, ExternalReference
	, assessmentrules.value('count(PaperRules/Section)','int') NumberOfSections
	, assessmentrules.value('sum(PaperRules/Section/@TotalQuestions)','varchar(10)') NumberOfQuestions
	, IsValid [Valid]
from AssessmentTable 

where AssessmentStatus = 2
order by 1;	
	
