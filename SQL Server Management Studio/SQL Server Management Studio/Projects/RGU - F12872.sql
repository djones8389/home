use rgu_Secureassess

if object_id('tempdb..#rgu') is not null drop table #rgu;
if object_id('tempdb..#ids') is not null drop table #ids;

create table #rgu (
	lastname nvarchar(100)
	,firstname nvarchar(100)
	, candidateref nvarchar(100)
);

create table #ids (
	id int
)

bulk insert #rgu
from 'T:\rgu.csv'
with(fieldterminator=',',rowterminator='\n')

begin tran

delete
from usertable 
where id in (
		select id
		from  #ids
)


rollback






/*
insert #ids
select --a.*, 'select id,  surname, forename, candidateref [Matric], dob from usertable where forename = ''' + a.firstname + ''' and surname = ''' + a.lastname+ '''  union '
	ut.id
	--,'select id from usertable where forename = ''' + a.firstname + ''' and surname = ''' + a.lastname+ '''  union '
from #rgu a
left join usertable ut
on a.candidateref = ut.candidateref
where ut.id is not null

select * from #ids where id in (1112,1157,1196,1321,1335,1390)


;with cte as (

select id, surname, forename, candidateref from usertable where forename = 'Richard' and surname = 'Brown'  union 
select id, surname, forename, candidateref from usertable where forename = 'Debbie' and surname = 'Fraser'  union 
select id, surname, forename, candidateref from usertable where forename = 'Christina' and surname = 'Orbell'  union 
select id, surname, forename, candidateref from usertable where forename = 'Katie' and surname = 'Reilly'  union 
select id, surname, forename, candidateref from usertable where forename = 'Stuart' and surname = 'Russell'  union 
select id, surname, forename, candidateref from usertable where forename = 'Anna' and surname = 'Webster'  
)
insert #ids
select *
from cte


select  id
from #ids


select id, surname, forename, candidateref [Matric], dob
from usertable
where candidateref in ('2337','204461','912520','905250','914841','403080')
order by surname

*/