SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

--where itemid = '368P1048' and keycode = 'M489VCF6'
--where itemid = '368P1037' and keycode = 'LFW4HWF6'

use ICAEW_SecureAssess

--34 mins

IF OBJECT_ID('tempdb..#itemids') is not null DROP TABLE #itemids;
IF OBJECT_ID('tempdb..#RowSplit') is not null DROP TABLE #RowSplit;

	
	--Store all records

	SELECT WESIRT.ID	
		, WESIRT.WAREHOUSEExamSessionID
		, Keycode
		, ItemID
		, itemresponsedata
	INTO [DJ_ItemIDs]
	FROM WAREHOUSE_ExamSessionItemResponseTable wesirt (READUNCOMMITTED)
	inner join WAREHOUSE_ExamSessionTable west (READUNCOMMITTED)
	on west.id = wesirt.warehouseexamsessionid
	where warehouseTime > '2017-09-10'
		and  ItemResponseData.exist('p/s/c[@typ=20]') = 1;       

	CREATE CLUSTERED INDEX [PK]	on [DJ_ItemIDs] (ID);



	--Create Col-Width lookup

		
	select ID 
		, a.b.value('@w','int') [ColWidth]
		, ROW_NUMBER() over (partition by ID, WAREHOUSEExamSessionID, itemid order by (SELECT 1)) ColNo
	INTO [DJ_ColWidthLookup]
	FROM  #itemids

	CROSS APPLY ItemResponseData.nodes('p/s/c/i/t/r/c[@w]') a(b)

	CREATE CLUSTERED INDEX [ColWidth] on [DJ_ColWidthLookup](ID);
	CREATE NONCLUSTERED INDEX [ColWidth2] on [DJ_ColWidthLookup](ID, ColNo,ColWidth);


	--Split out Merge Cells


	CREATE TABLE DJ_MergeExcluder (id int, colnum int, rownum int);

	DECLARE CommaSplit CURSOR FOR	

	SELECT  [ID]
		,  a.b.value('@cols','nvarchar(100)') [MergeCol] --+1		
		,  a.b.value('@rows','nvarchar(100)') [MergeRow] --+1
	FROM  #itemids
	CROSS APPLY itemresponsedata.nodes('//merge/m') a(b)

	DECLARE @ID int, @MergeColString nvarchar(max), @MergeRowString nvarchar(max)

	OPEN CommaSplit

	FETCH NEXT FROM CommaSplit INTO @ID, @MergeColString,@MergeRowString
	WHILE @@FETCH_STATUS = 0

		BEGIN
	
		INSERT DJ_MergeExcluder(ID, colnum,rownum)
		SELECT @ID,a.Value, b.value+1
		FROM ParmsToList(@MergeColString) a,ParmsToList(@MergeRowString) b

		FETCH NEXT FROM CommaSplit INTO @ID, @MergeColString, @MergeRowString

		END
	CLOSE CommaSplit
	DEALLOCATE CommaSplit



	CREATE CLUSTERED INDEX [IX_ID] on DJ_MergeExcluder (ID);


	





use ICAEW_SecureAssess

select keycode, wesirt.*
from WAREHOUSE_ExamSessionTable west
inner join WAREHOUSE_ExamSessionItemResponseTable wesirt
on west.id = wesirt.WAREHOUSEExamSessionID
where wesirt.id = 91212
--where KeyCode = '64BKP8F6' and itemid = '368P1047'

where itemid = '368P1037' and keycode = 'LFW4HWF6'


