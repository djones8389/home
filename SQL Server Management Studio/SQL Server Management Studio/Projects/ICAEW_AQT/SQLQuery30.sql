SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

--select * from [ColLookup_trim] where id = 88107
--select * from [ColLookup_full] where id = 88107

use ICAEW_SecureAssess

	SELECT D.ID
		, ColText
		, CL.ColWidth
	FROM (
		SELECT WESIRT.ID	
			, WESIRT.WAREHOUSEExamSessionID
			, WESIRT.ItemID
			, c.d.value('.','nvarchar(max)') [ColText]
		    , ROW_NUMBER() OVER(partition by WESIRT.ID, WESIRT.WAREHOUSEExamSessionID, WESIRT.itemid,CAST(c.d.query('..') as nvarchar(MAX)) ORDER BY CAST(c.d.query('..') as nvarchar(MAX))) [ColNo] 
		FROM  ItemResponseTable WESIRT (NOLOCK)
	
		CROSS APPLY  WESIRT.ItemResponseData.nodes('p/s/c/i/t/r/c') c(d)

			WHERE ISNULL(c.d.value('./@typ', 'tinyint'), 3) = 3 --filters WP
				and c.d.value('.','nvarchar(max)') <> ''	
				--and WESIRT.itemid = '368p1037'
				--and WESIRT.keycode = '9RPXW8F6'
				 --and  WESIRT.id = 88107
	) D			 
	INNER JOIN [ColLookup_trim] CL 
	ON CL.ID = D.ID
		and CL.ColNo = D.ColNo