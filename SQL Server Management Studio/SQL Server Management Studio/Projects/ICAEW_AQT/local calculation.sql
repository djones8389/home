/*
select COUNT(*)
from [ICAEW_SecureAssess].[dbo].[DJ_QueriedData] READUNCOMMITTED
where  Response like '%[^a-z. )0-9]%'
	and id = 91540
*/


USE [ICAEW_SecureAssess]
select a.*
	--, ResponseLength * 4
	,  cast([ResponseLength] as float)/68 [NoOfRowsRequired]
FROM (
	select *
		, LEN(cast(RESPONSE as nvarchar(MAX))) [ResponseLength]
		, cast(RowHeight as nvarchar(max))/22 [NoOfRowsAvailable]
	from [ICAEW_SecureAssess].[dbo].[DJ_QueriedData] READUNCOMMITTED
	--where (
	--		KeyCode = '3C39DGF6' and itemid = '368P1037'
	--	)
	--	or (
	--		KeyCode = 'LFW4HWF6' and itemid = '368P1037' 
	--	)
) A 
where (ResponseLength*4) > ColWidth
	and (cast([ResponseLength] as float)/68) > NoOfRowsAvailable
ORDER BY 1 desc,2,3,4

--where cast([ResponseLength] as float)/68 > [NoOfRowsAvailable]
	

	--72 was this

--and (itemid = '368P1048' and keycode = 'M489VCF6')
--or (itemid = '368P1037' and keycode = 'LFW4HWF6')




/*anomaly

--UPDATE [ICAEW_SecureAssess].[dbo].[DJ_QueriedData]
--SET Response = REPLACE(cast(Response as nvarchar(MAX)),'%20',' ')

--select Response
--from [ICAEW_SecureAssess].[dbo].[DJ_QueriedData] READUNCOMMITTED
--where  Response like '%20%'
#
*/