SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

use ICAEW_SecureAssess

IF OBJECT_ID('tempdb..#itemids') is not null DROP TABLE #itemids;

--31 secs to populate

SELECT WESIRT.ID	
	, WESIRT.WAREHOUSEExamSessionID
	, Keycode
	, ItemID
	, itemresponsedata
INTO #itemids
FROM WAREHOUSE_ExamSessionItemResponseTable wesirt (READUNCOMMITTED)
inner join WAREHOUSE_ExamSessionTable west (READUNCOMMITTED)
on west.id = wesirt.warehouseexamsessionid
where warehouseTime > '2017-09-10'
	and  ItemResponseData.exist('p/s/c[@typ=20]') = 1;       

CREATE CLUSTERED INDEX [PK]	on #itemids (ID);


SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

SELECT [FULL].ID
	, KeyCode
	, ColCheck.ItemID
	, ColCheck.ColNo
	, ColText
	
FROM (

SELECT ID	
	, c.d.value('.','nvarchar(max)') [ColText]
	, len(c.d.value('.','nvarchar(max)')) [ColTextLength]
	, ROW_NUMBER() OVER(partition by ID, WAREHOUSEExamSessionID, itemid,CAST(c.d.query('..') as nvarchar(MAX)) ORDER BY CAST(c.d.query('..') as nvarchar(MAX))) [ColNo] 
	--, ItemResponseData
FROM #itemids

CROSS APPLY ItemResponseData.nodes('p/s/c/i/t/r/c') c(d)

	WHERE ISNULL(c.d.value('./@typ', 'tinyint'), 3) = 3 --filters WP
		and c.d.value('.','nvarchar(max)') <> ''	
		and itemid = '368P1037' and keycode = 'LFW4HWF6'
) [FULL]

INNER JOIN (

SELECT ID
	   , warehouseexamsessionid		
	   , keycode
	   , ItemID
	   , a.b.value('@w','int') [ColWidth]
	   , ROW_NUMBER() over (partition by ID, WAREHOUSEExamSessionID, itemid order by (SELECT 1)) ColNo
FROM  #itemids

CROSS APPLY ItemResponseData.nodes('p/s/c/i/t/r/c[@w]') a(b)

where itemid = '368P1037' and keycode = 'LFW4HWF6'

) ColCheck

on [FULL].ID = ColCheck.ID
	and [FULL].ColNo = ColCheck.ColNo
where ColTextLength > (ColWidth/4)*2;






	   /*
SELECT [FULL].*
	, ColCheck.ItemID
	, ColCheck.ColNo
	, ColCheck.ColWidth
	
FROM (

SELECT WESIRT.ID	
	, c.d.value('.','nvarchar(max)') [ColText]
	, len(c.d.value('.','nvarchar(max)')) [ColTextLength]
	, ROW_NUMBER() OVER(partition by WESIRT.ID, WESIRT.WAREHOUSEExamSessionID, WESIRT.itemid,CAST(c.d.query('..') as nvarchar(MAX)) ORDER BY CAST(c.d.query('..') as nvarchar(MAX))) [ColNo] 
	--, ItemResponseData
FROM  warehouse_examsessionItemResponseTable WESIRT (NOLOCK)
	
inner join WAREHOUSE_ExamSessionTable west on west.id = wesirt.warehouseexamsessionid

CROSS APPLY  WESIRT.ItemResponseData.nodes('p/s/c/i/t/r/c') c(d)

	WHERE ISNULL(c.d.value('./@typ', 'tinyint'), 3) = 3 --filters WP
		and c.d.value('.','nvarchar(max)') <> ''	
		and WESIRT.itemid = '368P1037'
		and WEST.keycode = 'LFW4HWF6'
) [FULL]

INNER JOIN (

SELECT WESIRT.ID
	   , wesirt.warehouseexamsessionid		
	    , ItemID
		, a.b.value('@w','int') [ColWidth]
		, ROW_NUMBER() over (order by (SELECT 1)) ColNo
FROM  warehouse_examsessionItemResponseTable WESIRT (NOLOCK)
	
inner join WAREHOUSE_ExamSessionTable west on west.id = wesirt.warehouseexamsessionid
	
CROSS APPLY WESIRT.ItemResponseData.nodes('p/s/c/i/t/r/c[@w]') a(b)

WHERE WESIRT.itemid = '368P1037'
		and WEST.keycode = 'LFW4HWF6'
) ColCheck
on [FULL].ID = ColCheck.ID

*/






