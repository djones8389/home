use master

if OBJECT_ID('tempdb..#statechange') is not null drop table #statechange;

create table #statechange (

	client sysname
	, NoOfSecondsToChangeState bigint
)

insert #statechange
exec sp_MSforeachdb '

use [?];

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

if (''?'' like ''%SecureAssess'')

BEGIN
	SELECT ''?''
	, AVG((DATEDIFF(SECOND,isnull(STATE1.StateChangeDate,0), isnull(STATE2.StateChangeDate,0)))) [NoOfSecondsToChangeState]
	FROM (
	SELECT [ExamSessionID]
		  ,[NewState]
		  ,[StateChangeDate]
	FROM [?].[dbo].[ExamStateChangeAuditTable]
	where NewState = 1
	) STATE1
	INNER JOIN (
	SELECT  [ExamSessionID]
		  ,[NewState]
		  ,[StateChangeDate]
	FROM [?].[dbo].[ExamStateChangeAuditTable]
	where NewState = 2
	) STATE2
	on State1.ExamSessionID = State2.ExamSessionID
END	
'

select *
from #statechange
where NoOfSecondsToChangeState is not null
ORDER BY 1