SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
SET NOCOUNT ON;

IF OBJECT_ID('tempdb..#Questions') IS NOT NULL DROP TABLE #Questions;
IF OBJECT_ID('tempdb..#Answers') IS NOT NULL DROP TABLE #Answers;

CREATE TABLE #Questions (
	ItemID VARCHAR(20)
	, Question VARCHAR(max)
	, Answers VARCHAR(max)
);

INSERT #Questions(ItemID,Question)
select distinct 
	itt.itemID
	, itt.Question
FROM ItemMultipleChoiceTable MCQ
INNER JOIN  (
	select a.itemID
		, a.Question
	from (
		select distinct substring(ID, 0, charindex('S',ID)) [itemID]	
			, cast(ITT.ItemValue as xml).value('data(ItemValue/TEXTFORMAT/P/FONT)[1]','nvarchar(1000)') Question
			, ROW_NUMBER() OVER(PARTITION BY substring(ITT.ID, 0, charindex('S',ITT.ID)) ORDER BY ITT.Y ASC) R
		from [ItemTextBoxTable] ITT
	) a 
	where r = 1
) ITT
on ITT.itemID = substring(MCQ.ID, 0, charindex('S',MCQ.ID))


SELECT 
	substring(ID, 0, charindex('S',ID)) [itemID]
	,cast(mcq.ItemValue as xml).value('data(ItemValue/TEXTFORMAT/P/FONT)[1]','nvarchar(1000)') Answers
INTO #Answers
FROM [dbo].[ItemMultipleChoiceTable] mcq

CREATE CLUSTERED INDEX [IX_ItemID] on #Questions (itemid);
CREATE CLUSTERED INDEX [IX_ItemID] on #Answers(itemid);

DECLARE myCursor CURSOR FOR
SELECT DISTINCT itemid
FROM #Questions

DECLARE @itemid varchar(200)

OPEN myCursor

FETCH NEXT FROM myCursor INTO @itemid

WHILE @@FETCH_STATUS = 0

BEGIN

	UPDATE #Questions
	SET Answers = (
		
		SELECT SUBSTRING(
		(
		SELECT DISTINCT N', '+QUOTENAME([answers])
		FROM #Answers
		where itemid = @itemid	
		FOR XML PATH('')
		), 2, 10000)
	)
	WHERE ItemID = @itemid;

FETCH NEXT FROM myCursor INTO @itemid

END
CLOSE myCursor
DEALLOCATE myCursor




/*




SELECT left(id,7)
FROM ItemMultipleChoiceTable mcq
inner join ##Questions q
on q.ITEMID collate Latin1_General_CI_AS = left(mcq.id,7)



--<ItemValue><TEXTFORMAT LEADING="2"><P ALIGN="LEFT"><FONT FACE="Verdana" SIZE="14" COLOR="#000000" LETTERSPACING="0" KERNING="0">Question text</FONT></P></TEXTFORMAT></ItemValue>

SELECT itemID
	, question
FROM (
	SELECT  substring(ID, 0, charindex('S',ID)) [itemID]
		, cast(ItemValue as xml).value('data(ItemValue/TEXTFORMAT/P/FONT)[1]','nvarchar(1000)') Question
		, ROW_NUMBER() OVER(PARTITION BY substring(ID, 0, charindex('S',ID)) ORDER BY Y ASC) R
	FROM [dbo].[ItemTextBoxTable] ITT
	INNER JOIN (
		SELECT substring(ID, 0, charindex('S',ID)) [itemID]
		FROM ItemMultipleChoiceTable
		where id like '822P2189%'
	) MCQ
	ON MCQ.itemID = substring(ID, 0, charindex('S',ID))
	WHERE   cast(ItemValue as xml).value('data(ItemValue/TEXTFORMAT/P/FONT)[1]','nvarchar(1000)') != '•'
		and  id like '822P2189%'
) a 
WHERE r = 1

select *
from ##Questions where answers is null
where itemid = '822P2189'

where answer is null
	order by ItemID

--where itemid = '334P341'

--This question is about a cook baking cakes for sale.
select *
from ItemTextBoxTable
where id like '822P2189%'

select *
from ItemMultipleChoiceTable
where id like '822P2189%'



SELECT SUBSTRING(
		(
		SELECT DISTINCT N', '+QUOTENAME([answer])
		FROM 	
			(
			SELECT 
				substring(ID, 0, charindex('S',ID)) [itemID]
				,cast(mcq.ItemValue as xml).value('data(ItemValue/TEXTFORMAT/P/FONT)[1]','nvarchar(1000)') Answer
			FROM [dbo].[ItemMultipleChoiceTable] mcq
			WHERE substring(ID, 0, charindex('S',ID)) =  '822P2189'
			) a
		FOR XML PATH('')
		), 2, 10000)

*/