USE [PRV_SADB]

--/*Find exams with 2 x State 9 nodes*/
IF OBJECT_ID('tempdb..#IDs') IS NOT NULL DROP TABLE #IDs;

CREATE TABLE #IDs (
	ESID INT
)

INSERT #IDs
SELECT TOP 1000 ID 
FROM WAREHOUSE_ExamSessionTable;


SELECT ID, examStatechangeauditxml
FROM WAREHOUSE_ExamSessionTable
WHERE ID IN (
			  SELECT ESID 
				FROM #IDs
			 )

SET STATISTICS IO ON

/*First try,  lets find where we have 2 of these nodes*/

SELECT  COUNT(ID)
FROM WAREHOUSE_ExamSessionTable
WHERE ID IN (
			  SELECT ESID 
				FROM #IDs
			 )
AND ExamStateChangeAuditXML.value('count(exam/stateChange[newStateID=9])','tinyint') = 2;


SELECT  COUNT(ID)
FROM WAREHOUSE_ExamSessionTable
WHERE ID IN (
			  SELECT ESID 
				FROM #IDs
			 )
AND ExamStateChangeAuditXML.value('count(exam[1]/stateChange[newStateID=9])','tinyint') = 2;




--Though this is inefficient.  It has to look at all 1000 XML's,  go through all the nodes, and count them
--Can you think of a way to make it better













/*More efficient*/

SELECT COUNT(ID) 
FROM WAREHOUSE_ExamSessionTable
WHERE ID IN (
			  SELECT ESID 
				FROM #IDs
			 )
AND ExamStateChangeAuditXML.exist('exam/stateChange[newStateID=9][2]') = 1;


SELECT COUNT(ID) 
FROM WAREHOUSE_ExamSessionTable
WHERE ID IN (
			  SELECT ESID 
				FROM #IDs
			 )
AND ExamStateChangeAuditXML.exist('exam[1]/stateChange[newStateID=9][2]') = 1;
















/*Better or worse?*/

SELECT  COUNT(ID)
FROM WAREHOUSE_ExamSessionTable
WHERE ID IN (
			  SELECT ESID 
				FROM #IDs
			 )
AND ExamStateChangeAuditXML.value('count(exam/stateChange[newStateID=9])','tinyint') > 1;
