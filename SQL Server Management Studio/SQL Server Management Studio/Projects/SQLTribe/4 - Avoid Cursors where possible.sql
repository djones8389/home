USE [SurpassAnalytics_11.7]

SET STATISTICS IO ON

DECLARE @tbl nvarchar(4000)
	  , @sch nvarchar(4000)
	 , @query nvarchar(max)

DECLARE T_cur INSENSITIVE CURSOR FOR

SELECT TABLE_SCHEMA, TABLE_NAME 
FROM INFORMATION_SCHEMA.TABLES
WHERE TABLE_NAME NOT IN (
		'DimTime'
		,'DimMarkingTypes'
		,'DimQuestionTypes'
		,'DimExamState'
		,'DimMediaTypes'
		,'DimCAQuestionTypes'
		,'Packages'
	)
OPEN T_cur
FETCH NEXT FROM T_cur INTO @sch, @tbl
WHILE @@FETCH_STATUS = 0 
	BEGIN
		SET @query = 'TRUNCATE TABLE [' + @sch + '].[' + @tbl + ']'
		PRINT(@query)
		FETCH NEXT FROM T_cur INTO @sch, @tbl
	END
CLOSE T_cur
DEALLOCATE T_cur





DECLARE @Dynamic nvarchar(MAX)='';

SELECT @Dynamic+=CHAR(13) + 'TRUNCATE TABLE ' + quotename(TABLE_SCHEMA) + '.' + quotename(TABLE_NAME) + ';'
	FROM INFORMATION_SCHEMA.TABLES
	WHERE TABLE_NAME NOT IN (
		'DimTime'
		,'DimMarkingTypes'
		,'DimQuestionTypes'
		,'DimExamState'
		,'DimMediaTypes'
		,'DimCAQuestionTypes'
		,'Packages'
	);
PRINT(@Dynamic);

