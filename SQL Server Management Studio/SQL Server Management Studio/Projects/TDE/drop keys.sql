	DROP MASTER KEY
	
	use TDE_Database
	DROP DATABASE ENCRYPTION KEY  

	SELECT db_name(database_id), encryption_state,   percent_complete, key_algorithm, key_length
FROM sys.dm_database_encryption_keys
WHERE db_name(database_id) not in('tempdb')

alter database TDE_Database set  ENCRYPTION OFF

USE master
GO
DROP CERTIFICATE MyServerCert
DROP MASTER KEY