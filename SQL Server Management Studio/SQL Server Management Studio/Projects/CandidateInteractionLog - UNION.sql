SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

SELECT
	ExamSessionID
	,Keycode --ExamSessionID
	,ItemID
	--,A.EventType
	,B.Description [EventType]
	,cast(Data.query('/data/text()') as nvarchar(MAX)) [Descriptive]
	,TIMESTAMP
FROM [dbo].[ExamSessionCandidateInteractionLogsTable] A 
INNER JOIN ExamSessionCandidateInteractionEventsLookupTable B
ON A.EventType= B.EventType
inner join ExamSessionTable est
on est.id = a.ExamSessionID
where B.Description != 'Dirty Data Sent to The Server'

UNION

SELECT c.ExamSessionID
	,Keycode -- c.ExamSessionID
	--,LogsEntryID
	,ItemID
	,B.Description [EventType]
	,cast(Data.query('/data/text()') as nvarchar(MAX)) [Descriptive]
	,TIMESTAMP
FROM [dbo].[WAREHOUSE_ExamSessionCandidateInteractionLogsTable]  A 
INNER JOIN ExamSessionCandidateInteractionEventsLookupTable B
ON A.EventType= B.EventType
inner join WAREHOUSE_ExamSessionTable c
on c.id = a.ExamSessionID
where B.Description != 'Dirty Data Sent to The Server'

order by ExamSessionID;

