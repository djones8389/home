--These exams are wanting to getStructureXML's from IB, and go state 1 ---> 2

	SELECT 
			E.ID				AS examInstanceID,
			E.examState		AS examStatus,
			1 as [Limbo'd]
	INTO [ESIDsToLimbo]
	FROM ExamSessionTable	AS E
		INNER JOIN ScheduledExamsTable AS S ON E.ScheduledExamID = S.ID
	WHERE 
	(E.examState = 1 OR E.examState = 2)
		AND (( DATEDIFF(MINUTE,GETDATE(),DATEADD(DAY,-S.daysInAdvanceToUnlock,DATEADD(MINUTE,S.ActiveStartTime,S.ScheduledStartDateTime))) < ( S.AdvanceContentDownloadTimespanInHours * 60 )
			AND S.unlockForWholeDay = 0)
				OR ( DATEDIFF(MINUTE,GETDATE(),DATEADD(DAY,-S.daysInAdvanceToUnlock,S.ScheduledStartDateTime)) < ( S.AdvanceContentDownloadTimespanInHours * 60 )
					AND S.unlockForWholeDay = 1
					))
	ORDER BY 
				E.InvalidRulesCount,
				DATEADD(MINUTE,S.ActiveStartTime,S.ScheduledStartDateTime),
				E.examState
	GO

	
--Limbo these exams

	UPDATE ExamSessionTable
	set previousExamState = examState
		, examState = 99
	FROM ExamSessionTable
	INNER JOIN [ESIDsToLimbo]
	on [ESIDsToLimbo].examInstanceID = ExamSessionTable.id
	GO

--Query to see if exams are at the states we expect.  If today's date we'd expect > state 1
--We may have just limbo'd all of todays exams though

	SELECT EST.ID
		,EST.KeyCode
		,EST.examState
		,CT.CentreName
		,SCET.examName
		,SCET.ScheduledStartDateTime
		,SCET.ScheduledEndDateTime
		,AdvanceContentDownloadTimespanInHours
	FROM ExamSessionTable AS EST  
	INNER JOIN ScheduledExamsTable AS SCET ON SCET.ID = EST.ScheduledExamID
	INNER JOIN UserTable AS UT ON UT.ID = EST.UserID
	INNER JOIN CentreTable AS CT ON CT.ID = SCET.CentreID
	WHERE examState < 2
	ORDER BY SCET.ScheduledStartDateTime

--Un-limbo exams one at a time, or in batches, this would do them all:

	UPDATE ExamSessionTable
	set previousExamState = examState
		, examState = examStatus
	FROM ExamSessionTable
	INNER JOIN [ESIDsToLimbo]
	on [ESIDsToLimbo].examInstanceID = ExamSessionTable.id
	GO