USE MASTER;

IF DATABASEPROPERTY (N'pagesplittest', 'Version') > 0 DROP DATABASE pagesplittest;

CREATE DATABASE pagesplittest

USE pagesplittest;


CREATE TABLE t1 (c1 INT, c2 VARCHAR (1000));

CREATE CLUSTERED INDEX t1c1 ON t1 (c1);

GO

INSERT INTO t1 VALUES (1, REPLICATE ('a', 900));
INSERT INTO t1 VALUES (2, REPLICATE ('b', 900));
INSERT INTO t1 VALUES (3, REPLICATE ('c', 900));
INSERT INTO t1 VALUES (4, REPLICATE ('d', 900));
--� leave a gap at 5
INSERT INTO t1 VALUES (6, REPLICATE ('f', 900));
INSERT INTO t1 VALUES (7, REPLICATE ('g', 900));
INSERT INTO t1 VALUES (8, REPLICATE ('h', 900));
INSERT INTO t1 VALUES (9, REPLICATE ('i', 900));


select * from pagesplittest..t1

DBCC IND ('pagesplittest', 't1', 1);


DBCC TRACEON (3604);
DBCC PAGE (pagesplittest, 1, 78, 3);



--BEGIN TRAN
INSERT INTO t1 VALUES (5, REPLICATE ('a', 900));
DBCC IND ('pagesplittest', 't1', 1);

DBCC PAGE (pagesplittest, 1, 78, 3);
DBCC PAGE (pagesplittest, 1, 89, 3);
--ROLLBACK TRAN;


--ALTER INDEX [t1c1] ON [t1]  REORGANIZE;