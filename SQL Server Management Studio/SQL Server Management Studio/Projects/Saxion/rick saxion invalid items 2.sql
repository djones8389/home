;with Assessments as (
select ID	
	, REPLACE(REPLACE(cast(a.b.query('.') as NVARCHAR(MAX)), '<XPath>@ID="',''), '"</XPath>','') ItemID
from AssessmentTable  (READUNCOMMITTED)
		
CROSS APPLY AssessmentRules.nodes('PaperRules//XPath') a(b)

where IsValid = 0
	and REPLACE(REPLACE(cast(a.b.query('.') as NVARCHAR(MAX)), '<XPath>@ID="',''), '"</XPath>','') not like '<XPath>%'
), Projects as (
select DISTINCT a.b.value('data(@ID)[1]','nvarchar(20)') ItemID
from ProjectTable (READUNCOMMITTED)
cross apply ProjectStructure.nodes ('P//I') a(b)
)



SELECT 
	--QualificationName
	AT.id	
	, AT.AssessmentName
	, AT.ExternalReference
	, count(Assessments.ItemID) [Count]
FROM Assessments

INNER JOIN AssessmentTable AT
on AT.id = Assessments.ID

--INNER JOIN AssessmentGroupTable AGT 
--on AGT.ID = AGT.AssessmentGroupID

--INNER JOIN QualificationTable QT
--ON QT.ID = AGT.QualificationID

LEFT JOIN Projects
on Assessments.ItemID = Projects.ItemID

where Projects.ItemID IS NULL

group by AT.id
	, AT.AssessmentName
	, AT.ExternalReference
order by 4 desc;