SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

use Saxion_ItemBank

SELECT 		
	QualificationName
	, AssessmentName
	, ExternalReference
	--, AssessmentRules
	, ASL.Name
	--, assessmentrules.value('data(PaperRules/Section/Rule//Param/@Type)[1]','tinyint') [Type]
FROM (
	select  [current].*
	from (
	select AssessmentName, AssessmentGroupID,ExternalReference, isvalid, AssessmentStatus, AssessmentRules
	from saxion_itembank..assessmenttable (READUNCOMMITTED)
	where isvalid = 0
		and AssessmentStatus in (2,3)		
	) [current]
	inner join (
	select AssessmentName, AssessmentGroupID,ExternalReference, isvalid, AssessmentStatus, AssessmentRules
	from [Saxion_Itembank_2017-11-08]..assessmenttable (READUNCOMMITTED)
	where isvalid = 1
		and AssessmentStatus in (2,3)	
	) old
	on old.AssessmentName = [current].AssessmentName
	 and old.ExternalReference = [current].ExternalReference
	 and old.IsValid <> [current].IsValid	
) AT
INNER JOIN AssessmentGroupTable AGT  (READUNCOMMITTED)
on AGT.ID = AT.AssessmentGroupID

INNER JOIN QualificationTable QT (READUNCOMMITTED)
ON QT.ID = AGT.QualificationID

INNER JOIN AssessmentStatusLookupTable ASL
ON ASL.ID = AT.AssessmentStatus

where AssessmentRules.exist('PaperRules/Section/Rule//Param[@Type="7"]') = 1


order by 1,2,3


