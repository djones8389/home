SELECT A.[Date]
	, Sum(B) B
	, Sum(KB) KB
	, Sum(MB) MB
FROM (
SELECT	cast(uploadDate as date) [Date]
	, SUM(cast(DATALENGTH(Document) as bigint)) [B]
	, SUM(cast(DATALENGTH(Document) as bigint))/1024 [KB]
	, SUM(cast(DATALENGTH(Document) as bigint))/1024/1024 [MB]
FROM [dbo].[ExamSessionDocumentTable] (NOLOCK)
where uploadDate > '2018-04-01'
group by cast(uploadDate as date) 

UNION

SELECT	cast(uploadDate as date) [Date]
	, SUM(cast(DATALENGTH(Document) as bigint)) [B]
	, SUM(cast(DATALENGTH(Document) as bigint))/1024 [KB]
	, SUM(cast(DATALENGTH(Document) as bigint))/1024/1024 [MB]
FROM [dbo].[WAREHOUSE_ExamSessionDocumentTable] (NOLOCK)
where uploadDate > '2018-04-01'
group by cast(uploadDate as date) 
) A
GROUP BY A.[Date]
order by 1;


