USE PPD_AAT_SecureAssess

--sp_linkedservers
exec sp_executesql N'SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

if object_ID(''tempdb..##ESIDs'') is not null drop table ##ESIDs;
if object_ID(''tempdb..#LiveExamData'') is not null drop table #LiveExamData;

DECLARE @examStatesTable TABLE (ID int);
DECLARE @examStatepart nvarchar(max);
DECLARE @examStateinput nvarchar(max) =  @ExamStates;

WHILE LEN(@examStateinput) > 0
BEGIN
		IF PATINDEX(''%,%'', @examStateinput) > 0
		BEGIN
			SELECT @examStatepart = SUBSTRING(@examStateinput, 0, PATINDEX(''%,%'', @examStateinput));
			SET @examStateinput = SUBSTRING(@examStateinput, LEN(@examStatepart + N'','') + 1, LEN(@examStateinput))
		END
		ELSE
		BEGIN
			SET @examStatepart = @examStateinput;
			SET @examStateinput = NULL;
		END
		INSERT INTO @examStatesTable
		SELECT CAST(@examStatepart AS int);
END


BEGIN

	declare @singleState varchar(50) = @examStates;
	declare @myStartDate datetime = (select @StartDate);
	declare @myEndDate datetime = (select @EndDate);	

		declare @myString varchar(max) = ''

			CREATE TABLE ##ESIDs (ID int);
			INSERT INTO ##ESIDs
			SELECT ID
			FROM WAREHOUSE_ExamSessionTable 
			where  WarehouseExamState != 1
			and cast(EXAMSTATECHANGEAUDITXML.value(''''data(exam/stateChange[newStateID=(@examStates)]/changeDate)[1]'''',''''datetime'''') as DATE)  between ''''@StartDate'''' and ''''@EndDate''''
			''

			select @myString  = replace(replace(replace(@myString , ''@examStates'', @singleState), ''@StartDate'',@myStartDate), ''@EndDate'',@myEndDate)
			exec(@myString)
	
END

BEGIN

	DECLARE @ColBuilder nvarchar(MAX) = (SUBSTRING(
		(
		select '','', QUOTENAME(cast(ID as tinyint))
		from @examStatesTable
		for xml path ('''')
		), 2,100)
		)

	DECLARE @ExamStateList nvarchar(MAX) = SUBSTRING((

		SELECT '','', cast(ID as tinyint)
		FROM @examStatesTable
		for xml path('''')
	), 2,1000)


	DECLARE @DynamicString nvarchar(MAX)
	SELECT @DynamicString = ''

	SELECT *
	into ##sdw
	FROM (		
		SELECT A.ExamSessionKey
			, ExamStateId
			, StateChangeDate
		FROM (

		SELECT ROW_NUMBER() OVER (PARTITION BY ExamsessionKey, examStateID ORDER BY seqno) R
			, *
		FROM [PPD_AAT_SurpassDataWarehouse].[dbo].[FactExamSessionsStateAudit]
			where ExamStateID IN (''+@ExamStateList+'')
			AND
			CAST(StateChangeDate AS DATE) BETWEEN CAST(''+@StartDate+'' AS DATE) AND CAST(''+@EndDate+'' AS DATE)
			) A 
			where R = 1
	) AS [S]
	PIVOT (
	MAX(StateChangeDate)
	FOR examStateid IN (''+@ColBuilder+'')
	) AS [P]

	''
		
	print(@DynamicString)

	
	

END

if object_ID(''tempdb..##ESIDs'') is not null drop table ##ESIDs;
if object_ID(''tempdb..#LiveExamData'') is not null drop table #LiveExamData;',N'@ExamStates nvarchar(3),@StartDate datetime,@EndDate datetime',@ExamStates=N'6,9',@StartDate='2017-01-01 00:00:00',@EndDate='2017-01-01 00:00:00'