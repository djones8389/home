SELECT
	CentreName
	, CentreCode
	, examName  
	, createdBy
	, UT.Forename + ' ' + UT.Surname AS [Candidate]
	, scet.CreatedDateTime [ScheduledDate]
	, DATEADD(MINUTE, ActiveStartTime, ScheduledStartDateTime) AssessmentStartTime
	, DATEADD(MINUTE, ActiveStartTime, ScheduledStartDateTime) AssessmentEndTime
FROM ScheduledExamsTable SCET
INNER JOIN CentreTable CT ON CT.ID = SCET.CentreID
INNER JOIN ExamSessionTable EST on EST.ScheduledExamID = SCET.ID
INNER JOIN UserTable UT ON UT.ID = EST.UserID



SELECT  ID as [Value]
 , CentreName as  [Label]
FROM CentreTable
where ID IN (
	SELECT CentreID
	FROM ScheduledExamsTable
)
order by CentreName


SELECT ID as [Value]
  ,  Forename + ' ' + Surname [Label]
FROM UserTable
where ID IN (
	SELECT CreatedBy
	FROM ScheduledExamsTable
)
order by [Label]