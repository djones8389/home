SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

USE NHTV_SurpassDataWarehouse

exec sp_executesql N'SELECT 
    [Project1].[ExamSessionKey] AS [ExamSessionKey], 
    [Project1].[ExamSessionKey1] AS [ExamSessionKey1], 
    [Project1].[OriginatorKey] AS [OriginatorKey], 
    [Project1].[WarehouseTime] AS [WarehouseTime], 
    [Project1].[CompletionDateKey] AS [CompletionDateKey], 
    [Project1].[CompletionTime] AS [CompletionTime], 
    [Project1].[KeyCode] AS [KeyCode], 
    [Project1].[ExamKey] AS [ExamKey], 
    [Project1].[QualificationKey] AS [QualificationKey], 
    [Project1].[CentreKey] AS [CentreKey], 
    [Project1].[CandidateKey] AS [CandidateKey], 
    [Project1].[GradeKey] AS [GradeKey], 
    [Project1].[Pass] AS [Pass], 
    [Project1].[TimeTaken] AS [TimeTaken], 
    [Project1].[Duration] AS [Duration], 
    [Project1].[UserMarks] AS [UserMarks], 
    [Project1].[TotalMarksAvailable] AS [TotalMarksAvailable], 
    [Project1].[PassMark] AS [PassMark], 
    [Project1].[Invigilated] AS [Invigilated], 
    [Project1].[ClientMachineKey] AS [ClientMachineKey], 
    [Project1].[FinalExamState] AS [FinalExamState], 
    [Project1].[VoidReasonKey] AS [VoidReasonKey], 
    [Project1].[VoidReason] AS [VoidReason], 
    [Project1].[ExamVersionKey] AS [ExamVersionKey], 
    [Project1].[IsMysteryShopper] AS [IsMysteryShopper], 
    [Project1].[ExamStatus] AS [ExamStatus], 
    [Project1].[AdjustedGradeKey] AS [AdjustedGradeKey], 
    [Project1].[ScaleScoreKey] AS [ScaleScoreKey], 
    [Project1].[ScaledScore] AS [ScaledScore], 
    [Project1].[ExamVersionVersion] AS [ExamVersionVersion], 
    [Project1].[startedDate] AS [startedDate], 
    [Project1].[resultsReleased] AS [resultsReleased], 
    [Project1].[resultsSampled] AS [resultsSampled], 
    [Project1].[SubmittedDate] AS [SubmittedDate], 
    [Project1].[ScaleScoreId] AS [ScaleScoreId], 
    [Project1].[GradeBoundaryId] AS [GradeBoundaryId], 
    [Project1].[ScoreBoundaryId] AS [ScoreBoundaryId], 
    [Project1].[ExcludeFromReporting] AS [ExcludeFromReporting], 
    [Project1].[ResultDataFull] AS [ResultDataFull], 
    [Project1].[LOResults] AS [LOResults], 
    [Project1].[C2] AS [C1], 
    [Project1].[ExamSessionKey2] AS [ExamSessionKey2], 
    [Project1].[CPID] AS [CPID], 
    [Project1].[CPVersion] AS [CPVersion], 
    [Project1].[Section] AS [Section], 
    [Project1].[Mark] AS [Mark], 
    [Project1].[ViewingTime] AS [ViewingTime], 
    [Project1].[RawResponse] AS [RawResponse], 
    [Project1].[DerivedResponse] AS [DerivedResponse], 
    [Project1].[ShortDerivedResponse] AS [ShortDerivedResponse], 
    [Project1].[Attempted] AS [Attempted], 
    [Project1].[ItemPresentationOrder] AS [ItemPresentationOrder], 
    [Project1].[Scored] AS [Scored], 
    [Project1].[CPID1] AS [CPID1], 
    [Project1].[CPVersion1] AS [CPVersion1], 
    [Project1].[ItemXML] AS [ItemXML], 
    [Project1].[C1] AS [C2], 
    [Project1].[Id] AS [Id], 
    [Project1].[ExamSessionKey3] AS [ExamSessionKey3], 
    [Project1].[CPID2] AS [CPID2], 
    [Project1].[RescoringHistoryId] AS [RescoringHistoryId], 
    [Project1].[FactExamSessionAuditId] AS [FactExamSessionAuditId], 
    [Project1].[Mark1] AS [Mark1], 
    [Project1].[IsTemp] AS [IsTemp], 
    [Project1].[Scored1] AS [Scored1], 
    [Project1].[IsOriginal] AS [IsOriginal], 
    [Project1].[RawResponse1] AS [RawResponse1]
    FROM ( SELECT 
        [Filter1].[ExamSessionKey1] AS [ExamSessionKey], 
        [Filter1].[OriginatorKey] AS [OriginatorKey], 
        [Filter1].[WarehouseTime] AS [WarehouseTime], 
        [Filter1].[CompletionDateKey] AS [CompletionDateKey], 
        [Filter1].[CompletionTime] AS [CompletionTime], 
        [Filter1].[KeyCode] AS [KeyCode], 
        [Filter1].[ExamKey] AS [ExamKey], 
        [Filter1].[QualificationKey] AS [QualificationKey], 
        [Filter1].[CentreKey] AS [CentreKey], 
        [Filter1].[CandidateKey] AS [CandidateKey], 
        [Filter1].[GradeKey] AS [GradeKey], 
        [Filter1].[Pass] AS [Pass], 
        [Filter1].[TimeTaken] AS [TimeTaken], 
        [Filter1].[Duration] AS [Duration], 
        [Filter1].[UserMarks] AS [UserMarks], 
        [Filter1].[TotalMarksAvailable] AS [TotalMarksAvailable], 
        [Filter1].[PassMark] AS [PassMark], 
        [Filter1].[Invigilated] AS [Invigilated], 
        [Filter1].[ClientMachineKey] AS [ClientMachineKey], 
        [Filter1].[FinalExamState] AS [FinalExamState], 
        [Filter1].[VoidReasonKey] AS [VoidReasonKey], 
        [Filter1].[VoidReason] AS [VoidReason], 
        [Filter1].[ExamVersionKey] AS [ExamVersionKey], 
        [Filter1].[IsMysteryShopper] AS [IsMysteryShopper], 
        [Filter1].[ExamStatus] AS [ExamStatus], 
        [Filter1].[AdjustedGradeKey] AS [AdjustedGradeKey], 
        [Filter1].[ScaleScoreKey] AS [ScaleScoreKey], 
        [Filter1].[ScaledScore] AS [ScaledScore], 
        [Filter1].[ExamVersionVersion] AS [ExamVersionVersion], 
        [Filter1].[startedDate] AS [startedDate], 
        [Filter1].[resultsReleased] AS [resultsReleased], 
        [Filter1].[resultsSampled] AS [resultsSampled], 
        [Filter1].[SubmittedDate] AS [SubmittedDate], 
        [Filter1].[ScaleScoreId] AS [ScaleScoreId], 
        [Filter1].[GradeBoundaryId] AS [GradeBoundaryId], 
        [Filter1].[ScoreBoundaryId] AS [ScoreBoundaryId], 
        [Filter1].[ExcludeFromReporting] AS [ExcludeFromReporting], 
        [Filter1].[ExamSessionKey2] AS [ExamSessionKey1], 
        [Filter1].[ResultDataFull] AS [ResultDataFull], 
        [Filter1].[LOResults] AS [LOResults], 
        [Join3].[ExamSessionKey3] AS [ExamSessionKey2], 
        [Join3].[CPID1] AS [CPID], 
        [Join3].[CPVersion1] AS [CPVersion], 
        [Join3].[Section] AS [Section], 
        [Join3].[Mark1] AS [Mark], 
        [Join3].[ViewingTime] AS [ViewingTime], 
        [Join3].[RawResponse1] AS [RawResponse], 
        [Join3].[DerivedResponse] AS [DerivedResponse], 
        [Join3].[ShortDerivedResponse] AS [ShortDerivedResponse], 
        [Join3].[Attempted] AS [Attempted], 
        [Join3].[ItemPresentationOrder] AS [ItemPresentationOrder], 
        [Join3].[Scored1] AS [Scored], 
        [Join3].[CPID2] AS [CPID1], 
        [Join3].[CPVersion2] AS [CPVersion1], 
        [Join3].[ItemXML] AS [ItemXML], 
        [Join3].[Id] AS [Id], 
        [Join3].[ExamSessionKey4] AS [ExamSessionKey3], 
        [Join3].[CPID3] AS [CPID2], 
        [Join3].[RescoringHistoryId] AS [RescoringHistoryId], 
        [Join3].[FactExamSessionAuditId] AS [FactExamSessionAuditId], 
        [Join3].[Mark2] AS [Mark1], 
        [Join3].[IsTemp] AS [IsTemp], 
        [Join3].[Scored2] AS [Scored1], 
        [Join3].[IsOriginal] AS [IsOriginal], 
        [Join3].[RawResponse2] AS [RawResponse1], 
        CASE WHEN ([Join3].[ExamSessionKey3] IS NULL) THEN CAST(NULL AS int) WHEN ([Join3].[ExamSessionKey4] IS NULL) THEN CAST(NULL AS int) ELSE 1 END AS [C1], 
        CASE WHEN ([Join3].[ExamSessionKey3] IS NULL) THEN CAST(NULL AS int) ELSE 1 END AS [C2]
        FROM   (
		SELECT [Extent1].[ExamSessionKey] AS [ExamSessionKey1]
			,[Extent1].[OriginatorKey] AS [OriginatorKey]
			,[Extent1].[WarehouseTime] AS [WarehouseTime]
			,[Extent1].[CompletionDateKey] AS [CompletionDateKey]
			,[Extent1].[CompletionTime] AS [CompletionTime]
			,[Extent1].[KeyCode] AS [KeyCode]
			,[Extent1].[ExamKey] AS [ExamKey]
			,[Extent1].[QualificationKey] AS [QualificationKey]
			,[Extent1].[CentreKey] AS [CentreKey]
			,[Extent1].[CandidateKey] AS [CandidateKey]
			,[Extent1].[GradeKey] AS [GradeKey]
			,[Extent1].[Pass] AS [Pass]
			,[Extent1].[TimeTaken] AS [TimeTaken]
			,[Extent1].[Duration] AS [Duration]
			,[Extent1].[UserMarks] AS [UserMarks]
			,[Extent1].[TotalMarksAvailable] AS [TotalMarksAvailable]
			,[Extent1].[PassMark] AS [PassMark]
			,[Extent1].[Invigilated] AS [Invigilated]
			,[Extent1].[ClientMachineKey] AS [ClientMachineKey]
			,[Extent1].[FinalExamState] AS [FinalExamState]
			,[Extent1].[VoidReasonKey] AS [VoidReasonKey]
			,[Extent1].[VoidReason] AS [VoidReason]
			,[Extent1].[ExamVersionKey] AS [ExamVersionKey]
			,[Extent1].[IsMysteryShopper] AS [IsMysteryShopper]
			,[Extent1].[ExamStatus] AS [ExamStatus]
			,[Extent1].[AdjustedGradeKey] AS [AdjustedGradeKey]
			,[Extent1].[ScaleScoreKey] AS [ScaleScoreKey]
			,[Extent1].[ScaledScore] AS [ScaledScore]
			,[Extent1].[ExamVersionVersion] AS [ExamVersionVersion]
			,[Extent1].[startedDate] AS [startedDate]
			,[Extent1].[resultsReleased] AS [resultsReleased]
			,[Extent1].[resultsSampled] AS [resultsSampled]
			,[Extent1].[SubmittedDate] AS [SubmittedDate]
			,[Extent1].[ScaleScoreId] AS [ScaleScoreId]
			,[Extent1].[GradeBoundaryId] AS [GradeBoundaryId]
			,[Extent1].[ScoreBoundaryId] AS [ScoreBoundaryId]
			,[Extent1].[ExcludeFromReporting] AS [ExcludeFromReporting]
			,[Extent2].[ExamSessionKey] AS [ExamSessionKey2]
			,[Extent2].[ResultDataFull] AS [ResultDataFull]
			,[Extent2].[LOResults] AS [LOResults]
		FROM [dbo].[FactExamSessions] AS [Extent1]
            LEFT OUTER JOIN [dbo].[FactExamSessionXML] AS [Extent2] ON [Extent1].[ExamSessionKey] = [Extent2].[ExamSessionKey]
            WHERE ( NOT ((10 = [Extent1].[FinalExamState]) AND ([Extent1].[FinalExamState] IS NOT NULL))) AND ([Extent1].[ExcludeFromReporting] <> 1) ) AS [Filter1]
        LEFT OUTER JOIN  (
			SELECT [Extent3].[ExamSessionKey] AS [ExamSessionKey3]
							,[Extent3].[CPID] AS [CPID1]
							,[Extent3].[CPVersion] AS [CPVersion1]
							,[Extent3].[Section] AS [Section]
							,[Extent3].[Mark] AS [Mark1]
							,[Extent3].[ViewingTime] AS [ViewingTime]
							,[Extent3].[RawResponse] AS [RawResponse1]
							,[Extent3].[DerivedResponse] AS [DerivedResponse]
							,[Extent3].[ShortDerivedResponse] AS [ShortDerivedResponse]
							,[Extent3].[Attempted] AS [Attempted]
							,[Extent3].[ItemPresentationOrder] AS [ItemPresentationOrder]
							,[Extent3].[Scored] AS [Scored1]
							,[Extent4].[CPID] AS [CPID2]
							,[Extent4].[CPVersion] AS [CPVersion2]
							,[Extent4].[ItemXML] AS [ItemXML]
							,[Extent5].[Id] AS [Id]
							,[Extent5].[ExamSessionKey] AS [ExamSessionKey4]
							,[Extent5].[CPID] AS [CPID3]
							,[Extent5].[RescoringHistoryId] AS [RescoringHistoryId]
							,[Extent5].[FactExamSessionAuditId] AS [FactExamSessionAuditId]
							,[Extent5].[Mark] AS [Mark2]
							,[Extent5].[IsTemp] AS [IsTemp]
							,[Extent5].[Scored] AS [Scored2]
							,[Extent5].[IsOriginal] AS [IsOriginal]
							,[Extent5].[RawResponse] AS [RawResponse2]
			FROM [dbo].[FactQuestionResponses] AS [Extent3]
            INNER JOIN [dbo].[DimQuestionXML] AS [Extent4] 
			ON ([Extent3].[CPVersion] = [Extent4].[CPVersion]) AND ([Extent3].[CPID] = [Extent4].[CPID])
            LEFT OUTER JOIN [dbo].[FactQuestionResponseAudits] AS [Extent5] 
			ON ([Extent3].[CPID] = [Extent5].[CPID]) AND ([Extent3].[ExamSessionKey] = [Extent5].[ExamSessionKey]) ) 
				AS [Join3] 
				ON [Filter1].[ExamSessionKey1] = [Join3].[ExamSessionKey3]
        WHERE ([Filter1].[SubmittedDate] >= @p__linq__0) 
			AND ([Filter1].[SubmittedDate] <= @p__linq__1) AND ([Filter1].[ExamVersionKey] = @p__linq__2)
    )  AS [Project1]
    ORDER BY [Project1].[ExamSessionKey] DESC, [Project1].[ExamSessionKey1] ASC, [Project1].[C2] ASC, [Project1].[ExamSessionKey2] ASC, [Project1].[CPID] ASC, [Project1].[CPID1] ASC, [Project1].[CPVersion1] ASC, [Project1].[C1] ASC',N'@p__linq__0 datetime2(7),@p__linq__1 datetime2(7),@p__linq__2 int',@p__linq__0='2017-06-09 00:00:00',@p__linq__1='2017-06-13 23:59:59',@p__linq__2=68