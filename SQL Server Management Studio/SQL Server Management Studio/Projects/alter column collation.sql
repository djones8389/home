use Performance3_SecureAssess

--alter table [dbo].[AssessmentStatusLookupTable]
--alter column [name] nvarchar(50) collate SQL_Latin1_General_CP1_CI_AS

select t.name [TableName]
       , c.name [ColumnName]
       , collation_name
       , is_nullable
       , max_length
	   , type_name(C.system_type_id)
       , 'ALTER TABLE ' + SCHEMA_NAME(T.schema_id) + '.' + QUOTENAME(T.NAME) +
              '  ALTER COLUMN ' + QUOTENAME(C.NAME) + ' ' + type_name(C.system_type_id) 
              + 
                     CASE
                       WHEN type_name(C.system_type_id) = 'varchar'  and max_length > -1  THEN + '(' + cast(max_length as varchar(100)) + ')' 
                       WHEN type_name(C.system_type_id) = 'varchar'  and max_length = -1  THEN + '(MAX)'                                  
                     ELSE
                     CASE
                       WHEN type_name(C.system_type_id) = 'nvarchar' and max_length > -1 THEN '(' +  cast(max_length/2 as varchar(100)) + ')' 
                       WHEN type_name(C.system_type_id) = 'nvarchar' and max_length = -1 THEN '(MAX)' 
                           ELSE + '(' + cast(max_length as varchar(100)) + ')'
                           END
						   + ' collate SQL_Latin1_General_CP1_CI_AS'

                        end as [Alter Column]
from sys.columns c
inner join sys.tables t
on t.object_id = c.object_id
where collation_name = 'Latin1_General_CI_AS'
order by 1,2

ALTER TABLE dbo.[CandidateDemographicsTable]  ALTER COLUMN [DemographicName] nvarchar(50) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[CandidateDemographicsTable]  ALTER COLUMN [DemographicValue] nvarchar(MAX) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[CandidateExternalCache]  ALTER COLUMN [candidateRefStatusList] nvarchar(MAX) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[CandidateExternalCache]  ALTER COLUMN [CentreCodeExamRef] nvarchar(150) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[CentreCommsKeyAuditTable]  ALTER COLUMN [CommsKey] char(13) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[CentreTable]  ALTER COLUMN [AddressLine1] nvarchar(100) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[CentreTable]  ALTER COLUMN [AddressLine2] nvarchar(100) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[CentreTable]  ALTER COLUMN [CentreCode] nvarchar(50) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[CentreTable]  ALTER COLUMN [CentreName] nvarchar(100) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[CentreTable]  ALTER COLUMN [CommsKey] char(13) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[CentreTable]  ALTER COLUMN [InstallKey] char(13) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[CentreTable]  ALTER COLUMN [PostCode] nvarchar(12) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[CentreTable]  ALTER COLUMN [town] nvarchar(100) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[CohortTable]  ALTER COLUMN [description] nvarchar(MAX) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[CohortTable]  ALTER COLUMN [Name] nvarchar(100) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[ConfigurationDataTypeLookupTable]  ALTER COLUMN [Name] nvarchar(50) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[ConfigurationTable]  ALTER COLUMN [Description] nvarchar(500) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[ConfigurationTable]  ALTER COLUMN [Name] nvarchar(200) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[ConfigurationTable]  ALTER COLUMN [Value] nvarchar(500) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[CountryLookupTable]  ALTER COLUMN [Country] nvarchar(50) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[CountyLookupTable]  ALTER COLUMN [County] nvarchar(50) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[CPAuditTable]  ALTER COLUMN [CorrectAnswers] nvarchar(MAX) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[CPAuditTable]  ALTER COLUMN [ItemID] nvarchar(50) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[CPAuditTable]  ALTER COLUMN [QuestionTypes] nvarchar(4000) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[CPAuditTable]  ALTER COLUMN [ShortCorrectAnswers] nvarchar(500) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[DeletedTypeTable]  ALTER COLUMN [DeletedType] nvarchar(50) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[DiagnosticWriteTestTable]  ALTER COLUMN [TestToWrite] varchar(50)
ALTER TABLE dbo.[DocumentExtensionTypeLookupTable]  ALTER COLUMN [ExtensionName] nvarchar(10) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[EnthnicOriginLookupTable]  ALTER COLUMN [Name] nvarchar(70) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[ExamSectionSelectorTable]  ALTER COLUMN [Description] nvarchar(50) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[ExamSectionSelectorTable]  ALTER COLUMN [Name] nvarchar(100) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[ExamSectionsTable]  ALTER COLUMN [Description] nvarchar(50) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[ExamSectionsTable]  ALTER COLUMN [Name] nvarchar(100) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[ExamSectionTypeLookupTable]  ALTER COLUMN [Name] nvarchar(50) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[ExamSessionAuditTrail]  ALTER COLUMN [IPAddress] varchar(50)
ALTER TABLE dbo.[ExamSessionAuditTrail]  ALTER COLUMN [methodCalled] varchar(200)
ALTER TABLE dbo.[ExamSessionCandidateInteractionEventsLookupTable]  ALTER COLUMN [Description] nvarchar(50) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[ExamSessionCandidateInteractionLogsTable]  ALTER COLUMN [ItemID] nvarchar(50) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[ExamSessionDeliveryMechanismLookupTable]  ALTER COLUMN [DeliveryMechanism] nvarchar(10) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[ExamSessionDeliveryPlatformLookupTable]  ALTER COLUMN [Platform] varchar(25)
ALTER TABLE dbo.[ExamSessionDocumentInfoTable]  ALTER COLUMN [Reason] nvarchar(MAX) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[ExamSessionDocumentTable]  ALTER COLUMN [documentName] nvarchar(200) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[ExamSessionDocumentTable]  ALTER COLUMN [itemId] nvarchar(20) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[ExamSessionDurationAuditTable]  ALTER COLUMN [Reason] nvarchar(MAX) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[ExamSessionItemResponseTable]  ALTER COLUMN [ItemID] varchar(15)
ALTER TABLE dbo.[ExamSessionTable]  ALTER COLUMN [adjustedGrade] nvarchar(50) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[ExamSessionTable]  ALTER COLUMN [CandidateBreakExtraDurationReasonOther] nvarchar(250) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[ExamSessionTable]  ALTER COLUMN [CandidateBreakExtraDurationReasonText] nvarchar(100) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[ExamSessionTable]  ALTER COLUMN [grade] nvarchar(50) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[ExamSessionTable]  ALTER COLUMN [isPrintable] varchar(1)
ALTER TABLE dbo.[ExamSessionTable]  ALTER COLUMN [KeyCode] varchar(12)
ALTER TABLE dbo.[ExamSessionTable]  ALTER COLUMN [originalGrade] nvarchar(50) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[ExamSessionTable]  ALTER COLUMN [pinNumber] nvarchar(15) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[ExamSessionTable_Shredded]  ALTER COLUMN [candidateRef] nvarchar(300) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[ExamSessionTable_Shredded]  ALTER COLUMN [centreName] nvarchar(100) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[ExamSessionTable_Shredded]  ALTER COLUMN [examName] nvarchar(200) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[ExamSessionTable_Shredded]  ALTER COLUMN [examRef] nvarchar(100) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[ExamSessionTable_Shredded]  ALTER COLUMN [examVersionName] nvarchar(100) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[ExamSessionTable_Shredded]  ALTER COLUMN [examVersionRef] nvarchar(100) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[ExamSessionTable_Shredded]  ALTER COLUMN [foreName] nvarchar(50) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[ExamSessionTable_Shredded]  ALTER COLUMN [QualificationName] nvarchar(100) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[ExamSessionTable_Shredded]  ALTER COLUMN [surName] nvarchar(50) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[ExamSessionTable_Shredded]  ALTER COLUMN [ULN] nvarchar(10) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[ExamSessionTable_ShrededItems]  ALTER COLUMN [ItemName] varchar(MAX)
ALTER TABLE dbo.[ExamSessionTable_ShrededItems]  ALTER COLUMN [ItemRef] varchar(4000)
ALTER TABLE dbo.[ExamSessionTable_ShrededItems]  ALTER COLUMN [markerUserMark] nvarchar(MAX) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[ExamSessionTable_ShrededItems]  ALTER COLUMN [OptionsChosen] varchar(MAX)
ALTER TABLE dbo.[ExamSessionTagLookupTable]  ALTER COLUMN [Tag] nvarchar(250) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[ExamStateLookupTable]  ALTER COLUMN [Description] nvarchar(MAX) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[ExamStateLookupTable]  ALTER COLUMN [StateName] nvarchar(50) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[FailedResultUploadTable]  ALTER COLUMN [ProblemDescription] nvarchar(MAX) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[FilterMappingTable]  ALTER COLUMN [DatabaseVariable] nvarchar(50) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[FilterMappingTable]  ALTER COLUMN [FilterContext] nvarchar(50) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[FilterMappingTable]  ALTER COLUMN [FilterVariableName] nvarchar(50) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[FilterMappingTable]  ALTER COLUMN [MappingType] varchar(20)
ALTER TABLE dbo.[GenericLookup]  ALTER COLUMN [Description] nvarchar(50) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[GenericLookup]  ALTER COLUMN [Type] nvarchar(10) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[GroupStateLookupTable]  ALTER COLUMN [Description] nvarchar(MAX) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[GroupStateLookupTable]  ALTER COLUMN [StateName] nvarchar(50) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[IB3QualificationLookup]  ALTER COLUMN [ProgrammeName] nvarchar(50) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[IB3QualificationLookup]  ALTER COLUMN [QualificationName] nvarchar(100) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[IB3QualificationLookup]  ALTER COLUMN [QualificationRef] nvarchar(100) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[InteractionAudit]  ALTER COLUMN [AuditKey] nvarchar(50) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[InteractionAudit]  ALTER COLUMN [UserName] nvarchar(100) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[LearningOutcomeResultsTable]  ALTER COLUMN [LOName] nvarchar(300) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[LiveItemSessionTable]  ALTER COLUMN [ItemID] varchar(15)
ALTER TABLE dbo.[LiveItemSessionTable]  ALTER COLUMN [markerUserMark] varchar(10)
ALTER TABLE dbo.[LiveItemSessionTable]  ALTER COLUMN [name] nvarchar(255) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[LiveItemSessionTable]  ALTER COLUMN [userMark] varchar(10)
ALTER TABLE dbo.[LiveSectionSessionTable]  ALTER COLUMN [currentItem] varchar(15)
ALTER TABLE dbo.[LiveSectionSessionTable]  ALTER COLUMN [name] nvarchar(255) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[LOBoundaryTable]  ALTER COLUMN [Name] nvarchar(300) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[LocalScanExamStateLookupTable]  ALTER COLUMN [Description] nvarchar(MAX) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[LocalScanExamStateLookupTable]  ALTER COLUMN [StateName] nvarchar(50) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[LocalUpdatesTable]  ALTER COLUMN [Description] nvarchar(50) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[MacAddressTrackingTable]  ALTER COLUMN [MACAddress] nvarchar(MAX) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[MarkSchemeDocumentTable]  ALTER COLUMN [DocName] nvarchar(MAX) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[MarkSchemeDocumentTable]  ALTER COLUMN [ItemRef] varchar(15)
ALTER TABLE dbo.[MarkSchemeDocumentTable]  ALTER COLUMN [OwnerItemRef] varchar(15)
ALTER TABLE dbo.[MarkSchemeDocumentTable]  ALTER COLUMN [Url] nvarchar(MAX) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[PermissionsTable]  ALTER COLUMN [Description] nvarchar(500) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[PermissionsTable]  ALTER COLUMN [Name] nvarchar(50) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[QualificationStatusLookup]  ALTER COLUMN [Name] nvarchar(30) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[RelationTypeLookupTable]  ALTER COLUMN [Name] nvarchar(200) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[ReMarkExamSessionItemTable]  ALTER COLUMN [ItemID] varchar(15)
ALTER TABLE dbo.[ReMarkExamSessionLearningOutcomeResultsTable]  ALTER COLUMN [LOName] nvarchar(300) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[ReMarkExamSessionTable]  ALTER COLUMN [AdjustedGrade] nvarchar(50) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[RolesTable]  ALTER COLUMN [Name] nvarchar(50) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[ScheduledExamsTable]  ALTER COLUMN [examName] nvarchar(200) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[ScheduledExamsTable]  ALTER COLUMN [ExamRef] nvarchar(100) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[ScheduledExamsTable]  ALTER COLUMN [examVersionName] nvarchar(100) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[ScheduledExamsTable]  ALTER COLUMN [examVersionRef] nvarchar(100) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[ScheduledExamsTable]  ALTER COLUMN [language] nchar(20) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[ScheduledExamsTable]  ALTER COLUMN [purchaseOrder] nvarchar(100) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[StateManagementException]  ALTER COLUMN [ErrorMessage] nvarchar(500) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[StyleProfileTable]  ALTER COLUMN [ZipFileName] varchar(50)
ALTER TABLE dbo.[TagRestrictionsTable]  ALTER COLUMN [Description] nvarchar(MAX) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[TagRestrictionsTable]  ALTER COLUMN [Name] nvarchar(MAX) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[TagTypeTable]  ALTER COLUMN [TagTypeName] nvarchar(256) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[TagValuesTable]  ALTER COLUMN [Text] nvarchar(500) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[TokenStoreTable]  ALTER COLUMN [key] char(24) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[UsedKeyCodeTable]  ALTER COLUMN [KeyCode] varchar(12)
ALTER TABLE dbo.[UsersCentreReferenceLookup]  ALTER COLUMN [UsersCentreReference] nvarchar(50) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[UserTable]  ALTER COLUMN [AddressLine1] nvarchar(50) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[UserTable]  ALTER COLUMN [AddressLine2] nvarchar(50) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[UserTable]  ALTER COLUMN [CandidateRef] nvarchar(300) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[UserTable]  ALTER COLUMN [Email] nvarchar(MAX) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[UserTable]  ALTER COLUMN [Forename] nvarchar(50) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[UserTable]  ALTER COLUMN [Gender] char(1) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[UserTable]  ALTER COLUMN [Middlename] nvarchar(50) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[UserTable]  ALTER COLUMN [Password] nvarchar(50) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[UserTable]  ALTER COLUMN [PostCode] nvarchar(12) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[UserTable]  ALTER COLUMN [SecurityAnswer] nvarchar(50) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[UserTable]  ALTER COLUMN [SecurityQuestion] nvarchar(50) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[UserTable]  ALTER COLUMN [Surname] nvarchar(50) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[UserTable]  ALTER COLUMN [Telephone] nvarchar(15) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[UserTable]  ALTER COLUMN [Town] nvarchar(100) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[UserTable]  ALTER COLUMN [ULN] nvarchar(10) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[UserTable]  ALTER COLUMN [Username] nvarchar(50) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[VoidJustificationLookupTable]  ALTER COLUMN [name] nvarchar(100) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[WAREHOUSE_CandidateReviewAuditTable]  ALTER COLUMN [Comment] nvarchar(500) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[WAREHOUSE_CandidateReviewAuditTable]  ALTER COLUMN [EditedByForename] nvarchar(50) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[WAREHOUSE_CandidateReviewAuditTable]  ALTER COLUMN [EditedBySurname] nvarchar(50) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[WAREHOUSE_CandidateReviewBookingTable]  ALTER COLUMN [CentreName] nvarchar(100) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[WAREHOUSE_CandidateReviewBookingTable]  ALTER COLUMN [ExamName] nvarchar(200) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[WAREHOUSE_CandidateReviewBookingTable]  ALTER COLUMN [PIN] nvarchar(10) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[WAREHOUSE_CentreTable]  ALTER COLUMN [AddressLine1] nvarchar(100) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[WAREHOUSE_CentreTable]  ALTER COLUMN [AddressLine2] nvarchar(100) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[WAREHOUSE_CentreTable]  ALTER COLUMN [CentreCode] nvarchar(50) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[WAREHOUSE_CentreTable]  ALTER COLUMN [CentreName] nvarchar(100) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[WAREHOUSE_CentreTable]  ALTER COLUMN [Country] nvarchar(50) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[WAREHOUSE_CentreTable]  ALTER COLUMN [County] nvarchar(70) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[WAREHOUSE_CentreTable]  ALTER COLUMN [PostCode] nvarchar(12) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[WAREHOUSE_CentreTable]  ALTER COLUMN [Town] nvarchar(100) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[WAREHOUSE_DataLookupTable]  ALTER COLUMN [DataRefString] nvarchar(100) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[WAREHOUSE_DataLookupType]  ALTER COLUMN [LookupType] nvarchar(100) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[WAREHOUSE_ExamSectionSelectorTable]  ALTER COLUMN [Description] nvarchar(50) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[WAREHOUSE_ExamSectionSelectorTable]  ALTER COLUMN [Name] nvarchar(100) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[WAREHOUSE_ExamSectionsTable]  ALTER COLUMN [Description] nvarchar(50) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[WAREHOUSE_ExamSectionsTable]  ALTER COLUMN [Name] nvarchar(100) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[WAREHOUSE_ExamSessionAvailableItemsTable]  ALTER COLUMN [ItemID] nvarchar(50) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[WAREHOUSE_ExamSessionCandidateInteractionLogsTable]  ALTER COLUMN [ItemID] nvarchar(50) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[WAREHOUSE_ExamSessionDocumentInfoTable]  ALTER COLUMN [Reason] nvarchar(MAX) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[WAREHOUSE_ExamSessionDocumentTable]  ALTER COLUMN [documentName] nvarchar(200) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[WAREHOUSE_ExamSessionDocumentTable]  ALTER COLUMN [itemId] nvarchar(20) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[WAREHOUSE_ExamSessionDurationAuditTable]  ALTER COLUMN [Reason] nvarchar(MAX) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[WAREHOUSE_ExamSessionItemResponseTable]  ALTER COLUMN [Comments] nvarchar(300) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[WAREHOUSE_ExamSessionItemResponseTable]  ALTER COLUMN [DerivedResponse] nvarchar(MAX) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[WAREHOUSE_ExamSessionItemResponseTable]  ALTER COLUMN [ItemID] nvarchar(15) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[WAREHOUSE_ExamSessionItemResponseTable]  ALTER COLUMN [ShortDerivedResponse] nvarchar(500) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[WAREHOUSE_ExamSessionTable]  ALTER COLUMN [CandidateBreakExtraDurationReasonOther] nvarchar(250) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[WAREHOUSE_ExamSessionTable]  ALTER COLUMN [CandidateBreakExtraDurationReasonText] nvarchar(100) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[WAREHOUSE_ExamSessionTable]  ALTER COLUMN [CQN] nvarchar(20) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[WAREHOUSE_ExamSessionTable]  ALTER COLUMN [KeyCode] varchar(12)
ALTER TABLE dbo.[WAREHOUSE_ExamSessionTable]  ALTER COLUMN [pinNumber] nvarchar(15) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[WAREHOUSE_ExamSessionTable_ShreddedItems_Mark]  ALTER COLUMN [DisplayText] nvarchar(MAX) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[WAREHOUSE_ExamSessionTable_ShreddedItems_Mark]  ALTER COLUMN [ItemID] varchar(15)
ALTER TABLE dbo.[WAREHOUSE_ExamSessionTable_Shreded]  ALTER COLUMN [adjustedGrade] nvarchar(50) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[WAREHOUSE_ExamSessionTable_Shreded]  ALTER COLUMN [centreName] nvarchar(100) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[WAREHOUSE_ExamSessionTable_Shreded]  ALTER COLUMN [examName] nvarchar(200) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[WAREHOUSE_ExamSessionTable_Shreded]  ALTER COLUMN [examRef] nvarchar(100) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[WAREHOUSE_ExamSessionTable_Shreded]  ALTER COLUMN [examVersionName] nvarchar(100) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[WAREHOUSE_ExamSessionTable_Shreded]  ALTER COLUMN [examVersionRef] nvarchar(100) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[WAREHOUSE_ExamSessionTable_Shreded]  ALTER COLUMN [ExternalReference] nvarchar(100) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[WAREHOUSE_ExamSessionTable_Shreded]  ALTER COLUMN [foreName] nvarchar(50) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[WAREHOUSE_ExamSessionTable_Shreded]  ALTER COLUMN [grade] nvarchar(50) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[WAREHOUSE_ExamSessionTable_Shreded]  ALTER COLUMN [language] nchar(20) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[WAREHOUSE_ExamSessionTable_Shreded]  ALTER COLUMN [originalGrade] nvarchar(50) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[WAREHOUSE_ExamSessionTable_Shreded]  ALTER COLUMN [originalGradewithVoidReason] nvarchar(400) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[WAREHOUSE_ExamSessionTable_Shreded]  ALTER COLUMN [qualificationName] nvarchar(100) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[WAREHOUSE_ExamSessionTable_Shreded]  ALTER COLUMN [qualificationRef] nvarchar(100) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[WAREHOUSE_ExamSessionTable_Shreded]  ALTER COLUMN [scheduledDurationReason] nvarchar(MAX) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[WAREHOUSE_ExamSessionTable_Shreded]  ALTER COLUMN [surName] nvarchar(50) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[WAREHOUSE_ExamSessionTable_ShrededItems]  ALTER COLUMN [FriendItems] nvarchar(MAX) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[WAREHOUSE_ExamSessionTable_ShrededItems]  ALTER COLUMN [ItemName] nvarchar(200) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[WAREHOUSE_ExamSessionTable_ShrededItems]  ALTER COLUMN [ItemRef] varchar(15)
ALTER TABLE dbo.[WAREHOUSE_ExamSessionTable_ShrededItems]  ALTER COLUMN [markerUserMark] nvarchar(MAX) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[WAREHOUSE_ExamSessionTable_ShrededItems]  ALTER COLUMN [OptionsChosen] varchar(200)
ALTER TABLE dbo.[WAREHOUSE_ExamStateLookupTable]  ALTER COLUMN [Description] nvarchar(100) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[WAREHOUSE_LearningOutcomeResultsTable]  ALTER COLUMN [LOName] nvarchar(300) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[WAREHOUSE_LOBoundaryTable]  ALTER COLUMN [Name] nvarchar(300) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[WAREHOUSE_QualificationTable]  ALTER COLUMN [QualificationName] nvarchar(100) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[WAREHOUSE_ScheduledExamsTable]  ALTER COLUMN [examName] nvarchar(200) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[WAREHOUSE_ScheduledExamsTable]  ALTER COLUMN [ExternalReference] nvarchar(100) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[WAREHOUSE_ScheduledExamsTable]  ALTER COLUMN [language] nchar(20) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[WAREHOUSE_ScheduledExamsTable]  ALTER COLUMN [purchaseOrder] nvarchar(100) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[WAREHOUSE_ScheduledExamsTable]  ALTER COLUMN [qualificationName] nvarchar(100) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[WAREHOUSE_ScheduledExamsTable]  ALTER COLUMN [QualificationRef] nvarchar(100) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[WAREHOUSE_UserTable]  ALTER COLUMN [AddressLine1] nvarchar(50) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[WAREHOUSE_UserTable]  ALTER COLUMN [AddressLine2] nvarchar(50) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[WAREHOUSE_UserTable]  ALTER COLUMN [CandidateRef] nvarchar(300) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[WAREHOUSE_UserTable]  ALTER COLUMN [Country] nvarchar(50) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[WAREHOUSE_UserTable]  ALTER COLUMN [County] nvarchar(70) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[WAREHOUSE_UserTable]  ALTER COLUMN [Email] nvarchar(MAX) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[WAREHOUSE_UserTable]  ALTER COLUMN [EthnicOrigin] nvarchar(70) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[WAREHOUSE_UserTable]  ALTER COLUMN [Forename] nvarchar(50) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[WAREHOUSE_UserTable]  ALTER COLUMN [Gender] char(1) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[WAREHOUSE_UserTable]  ALTER COLUMN [Middlename] nvarchar(50) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[WAREHOUSE_UserTable]  ALTER COLUMN [PostCode] nvarchar(12) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[WAREHOUSE_UserTable]  ALTER COLUMN [Surname] nvarchar(50) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[WAREHOUSE_UserTable]  ALTER COLUMN [Telephone] nvarchar(15) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[WAREHOUSE_UserTable]  ALTER COLUMN [Town] nvarchar(100) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[WAREHOUSE_UserTable]  ALTER COLUMN [ULN] nvarchar(10) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[WAREHOUSE_UserTable]  ALTER COLUMN [Username] nvarchar(50) collate SQL_Latin1_General_CP1_CI_AS
ALTER TABLE dbo.[WelcomeMessageTable]  ALTER COLUMN [welcomeText] nvarchar(MAX) collate SQL_Latin1_General_CP1_CI_AS