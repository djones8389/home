--BACKUP DATABASE [PRV_BritishCouncil_CPProjectAdmin] 
--	TO DISK = N'T:\BACKUP\PRV.BC.CP.2016.04.21.bak' 
--	WITH NAME = N'PRV_BritishCouncil_CPProjectAdmin- Database Backup', COPY_ONLY, NOFORMAT, NOINIT, SKIP, COMPRESSION, NOREWIND, NOUNLOAD, STATS = 10;


RESTORE DATABASE [PRV_BritishCouncil_CPProjectAdmin_IP] FROM DISK = N'T:\BACKUP\PRV.BC.CP.2016.04.21.bak' 
	WITH FILE = 1, NOUNLOAD, NOREWIND,  STATS = 10
	, MOVE 'BRITISHCOUNCIL_CPProjectAdmin' TO N'S:\DATA\PRV_BritishCouncil_CPProjectAdmin_IP.mdf'
	, MOVE 'BRITISHCOUNCIL_CPProjectAdmin_log' TO N'L:\LOGS\PRV_BritishCouncil_CPProjectAdmin_IP.ldf'; 


exec sp_renamedb 'PPD_BCIntegration_CPProjectAdmin','PPD_BCIntegration_CPProjectAdmin_Old'

exec sp_renamedb 'PRV_BritishCouncil_CPProjectAdmin_IP','PPD_BCIntegration_CPProjectAdmin'


KILL 90



DECLARE @DBName nvarchar(100) = 'PPD_BCIntegration_CPProjectAdmin';
DECLARE @Login nvarchar(100) = 'Davej';


DECLARE @DynamicSpidKiller nvarchar(MAX) = '';

select @DynamicSpidKiller +=CHAR(13) + 
	 'KILL '  + convert(nvarchar(10),spid)
from sys.databases D
INNER JOIN sys.sysprocesses S
on D.database_id = s.dbid
where Name = @DBName
	--or loginame = @Login
PRINT (@DynamicSpidKiller);
