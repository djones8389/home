
/*Constraints*/

--[WAREHOUSE_ExamSessionItemResponseTable]

--ALTER TABLE [dbo].[WAREHOUSE_ExamSessionItemResponseTable]
--	DROP CONSTRAINT [PK_WAREHOUSEExamSessionItemResponseTable_1]
--ALTER TABLE [dbo].[WAREHOUSE_ExamSessionItemResponseTable]
--	DROP CONSTRAINT [FK_WAREHOUSE_ExamSessionItemResponseTable_WAREHOUSE_ExamSessionTable]
--DROP INDEX [IX_WAREHOUSE_ExamSessionItemResponseTable_R11_3PT_script3]
--	on [WAREHOUSE_ExamSessionItemResponseTable]
--DROP INDEX [idx_NC_WAREHOUSE_ExamSessionItemResponseTable_WAREHOUSEExamSessionID_ItemID_ItemVersion]
--	on [WAREHOUSE_ExamSessionItemResponseTable]
--DROP INDEX [examSessionId_NCI]
--	on [WAREHOUSE_ExamSessionItemResponseTable]

ALTER TABLE [dbo].[WAREHOUSE_ExamSessionItemResponseTable]
	ADD CONSTRAINT [PK_WAREHOUSEExamSessionItemResponseTable_1] PRIMARY KEY CLUSTERED (ID)
GO

PRINT 'ADD CONSTRAINT [PK_WAREHOUSEExamSessionItemResponseTable_1] PRIMARY KEY CLUSTERED (ID)'

ALTER TABLE [dbo].[WAREHOUSE_ExamSessionItemResponseTable] ADD  CONSTRAINT [DF_WAREHOUSE_ExamSessionItemResponseTable_MarkingIgnored]  DEFAULT ((0)) FOR [MarkingIgnored]
GO

PRINT 'ADD  CONSTRAINT [DF_WAREHOUSE_ExamSessionItemResponseTable_MarkingIgnored]'

ALTER TABLE [dbo].[WAREHOUSE_ExamSessionItemResponseTable]  WITH CHECK 
ADD  CONSTRAINT [FK_WAREHOUSE_ExamSessionItemResponseTable_WAREHOUSE_ExamSessionTable] FOREIGN KEY([WAREHOUSEExamSessionID])
REFERENCES [dbo].[WAREHOUSE_ExamSessionTable] ([ID])
ON DELETE CASCADE
GO

PRINT 'ADD  CONSTRAINT [FK_WAREHOUSE_ExamSessionItemResponseTable_WAREHOUSE_ExamSessionTable]'

ALTER TABLE [dbo].[WAREHOUSE_ExamSessionItemResponseTable] CHECK CONSTRAINT [FK_WAREHOUSE_ExamSessionItemResponseTable_WAREHOUSE_ExamSessionTable]
GO



/*Indexes*/

PRINT 'START INDEXES'

--[WAREHOUSE_ExamSessionItemResponseTable]

CREATE NONCLUSTERED INDEX [IX_WAREHOUSE_ExamSessionItemResponseTable_R11_3PT_script3] ON [dbo].[WAREHOUSE_ExamSessionItemResponseTable]
(
	[WAREHOUSEExamSessionID] ASC
)
INCLUDE ( 	[ItemID],
	[ItemResponseData]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [WHAItemsWHItemResponses]
GO


CREATE NONCLUSTERED INDEX [idx_NC_WAREHOUSE_ExamSessionItemResponseTable_WAREHOUSEExamSessionID_ItemID_ItemVersion] ON [dbo].[WAREHOUSE_ExamSessionItemResponseTable]
(
	[WAREHOUSEExamSessionID] ASC,
	[ItemID] ASC,
	[ItemVersion] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [WHAItemsWHItemResponses]
GO


CREATE NONCLUSTERED INDEX [examSessionId_NCI] ON [dbo].[WAREHOUSE_ExamSessionItemResponseTable]
(
	[WAREHOUSEExamSessionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [WHAItemsWHItemResponses]
GO