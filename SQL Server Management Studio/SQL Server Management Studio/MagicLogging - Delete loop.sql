Use MagicLogging

DROP TABLE IF EXISTS dbo.Log_ToDelete

select ID
	, 0 [Deleted] 
INTO dbo.Log_ToDelete
FROM [dbo].[Log]
WHERE [Date] < DATEADD(MONTH, -18, GETDATE())
	
CREATE CLUSTERED INDEX [ID_IX] ON Log_ToDelete (id);

declare @total int = (select count(1) from Log_ToDelete)
declare @increment int = 1000
declare @counter int = 0;

while (@counter < @total)

BEGIN

	DELETE TOP (@increment)
	FROM [Log] 
	WHERE ID IN (
		SELECT  ID
		FROM Log_ToDelete
		Where Deleted = 0
	)	 

	UPDATE TOP (@increment) Log_ToDelete
	set [Deleted] = 1
	where [Deleted] = 0
 
 	print @counter + ' ' + (select getdate())
	select @counter += @increment   

	WAITFOR DELAY '00:00:10'

END
