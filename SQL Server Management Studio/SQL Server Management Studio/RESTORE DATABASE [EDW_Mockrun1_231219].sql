--LS0603-21301-00 

	RESTORE DATABASE [EDW_Mockrun1_231219]
	FROM  DISK = N'\\hiscox.com\backup\ReleaseDump\FTP\EDW\EDW_2019-12-23.bak' WITH  FILE = 1,  
	MOVE N'PRIMARY' TO N'E:\SQL_RND_01\SQL_Databases\EDW_Mockrun1_231219.mdf',  
	MOVE N'Current' TO N'E:\SQL_RND_01\SQL_Databases\EDW_Mockrun1_231219_Current.ndf',  
	MOVE N'History' TO N'E:\SQL_RND_01\SQL_Databases\EDW_Mockrun1_231219_History.ndf',  
	MOVE N'Indexes' TO N'E:\SQL_RND_01\SQL_Databases\EDW_Mockrun1_231219_Indexes.ndf',  
	MOVE N'LogFile' TO N'F:\SQL_Logs\EDW_Mockrun1_231219_Log.LDF', 
	NOUNLOAD,  REPLACE,  STATS = 1


	--LS0603-21101-00

	RESTORE DATABASE [EDW_Mockrun1_231219] 
	FROM  DISK = N'\\hiscox.com\backup\ReleaseDump\FTP\EDW\EDW_2019-12-23.bak' WITH  FILE = 1,  
	MOVE N'PRIMARY' TO N'E:\SQL_RND_01\SQL_Databases\EDW_Mockrun1_231219.mdf',  
	MOVE N'Current' TO N'E:\SQL_RND_01\SQL_Databases\EDW_Mockrun1_231219_Current.ndf',  
	MOVE N'History' TO N'E:\SQL_RND_01\SQL_Databases\EDW_Mockrun1_231219_History.ndf',  
	MOVE N'Indexes' TO N'E:\SQL_RND_01\SQL_Databases\EDW_Mockrun1_231219_Indexes.ndf',  
	MOVE N'LogFile' TO N'E:\SQL_SEQ_01\SQL_Logs\EDW_Mockrun1_231219_Log.LDF', 
	NOUNLOAD,  REPLACE,  STATS = 1


	--dv0603-21002-00.hiscox.nonprod\FDWDEV 


	RESTORE DATABASE [EDW_Mockrun1_231219] 
	FROM  DISK = N'E:\SQL_RND_01\SQL_Backups\EDW_2019-12-23.bak' WITH  FILE = 1,  
	MOVE N'PRIMARY' TO N'E:\SQL_RND_01\SQL_Databases\EDW_Mockrun1_231219.mdf',  
	MOVE N'Current' TO N'E:\SQL_RND_01\SQL_Databases\EDW_Mockrun1_231219_Current.ndf',  
	MOVE N'History' TO N'E:\SQL_RND_01\SQL_Databases\EDW_Mockrun1_231219_History.ndf',  
	MOVE N'Indexes' TO N'E:\SQL_RND_01\SQL_Databases\EDW_Mockrun1_231219_Indexes.ndf',  
	MOVE N'LogFile' TO N'E:\SQL_SEQ_01\SQL_Logs\EDW_Mockrun1_231219_Log.LDF', 
	NOUNLOAD,  REPLACE,  STATS = 1