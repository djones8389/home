IF (SELECT hars.role_desc
    FROM sys.DATABASES d
    INNER JOIN sys.dm_hadr_availability_replica_states hars ON d.replica_id = hars.replica_id
    WHERE database_id = DB_ID('MIMService')) = 'PRIMARY'
BEGIN


DECLARE @BackupDirectory NVARCHAR(100)   
EXEC master..xp_instance_regread @rootkey = 'HKEY_LOCAL_MACHINE',  
    @key = 'Software\Microsoft\MSSQLServer\MSSQLServer',  
    @value_name = 'BackupDirectory', @BackupDirectory = @BackupDirectory OUTPUT ;  
 
SELECT @BackupDirectory = @BackupDirectory +'\' 

DECLARE @Dynamic NVARCHAR(MAX)='';

SELECT @Dynamic +=CHAR(13) + 'BACKUP DATABASE [' + name + '] TO  DISK = N'''+@BackupDirectory+ name +'.bak'' WITH  FORMAT,  NAME = N''' + name + '-Full Database Backup'', COMPRESSION,  STATS = 10;'
FROM sys.databases
WHERE name <> 'tempdb'

exec(@Dynamic);

END



IF (SELECT hars.role_desc
    FROM sys.DATABASES d
    INNER JOIN sys.dm_hadr_availability_replica_states hars ON d.replica_id = hars.replica_id
    WHERE database_id = DB_ID('MIMService')) = 'PRIMARY'
BEGIN

DECLARE @Dynamic nvarchar(max) = '';

select @Dynamic+=CHAR(13) + '
BACKUP LOG '+quotename(name)+'
TO  DISK = N''E:\SQL_SEQ_01\ITS1601\SQL_Backups\Logs\'+name+ '_' +(SELECT FORMAT(GETDATE(),'dd.MM.yyyy hh.mm.ss'))+'.trn''
WITH  FORMAT,  NAME = N'''+name+' - Log Backup'', COMPRESSION,  STATS = 10;
'
from sys.databases
where database_id > 4
	and recovery_model_desc = 'FULL'
 
exec(@Dynamic); 

END