--select transferred_size_bytes / (1024 * 1024) as 'transfrerred'
--	, database_size_bytes / (1024 * 1024) as 'total size'
--	, remote_machine_name 
--	, estimate_time_complete_utc 
--from sys.dm_hadr_physical_seeding_stats

 select 
	local_database_name, 
	remote_machine_name, 
	--role_desc as [role], 
	transfer_rate_bytes_per_second / (1024*1024) as transfer_rate_Mb_per_second, 
	transferred_size_bytes / (1024*1024) as transferred_size_Mb, 
	database_size_bytes / (1024*1024) as database_size_Mb, 
	DATEADD(mi, DATEDIFF(mi, GETUTCDATE(), GETDATE()), start_time_utc) as start_time, 
	DATEADD(mi, DATEDIFF(mi, GETUTCDATE(), GETDATE()), estimate_time_complete_utc) as estimate_time_complete, 
	total_disk_io_wait_time_ms, 
	total_network_wait_time_ms, 
	is_compression_enabled 
from sys.dm_hadr_physical_seeding_stats

--use master
--ALTER AVAILABILITY GROUP [availabilitygroup] 
--    MODIFY REPLICA ON 'pr0303qhdeg5-01'   
--    WITH (SEEDING_MODE = MANUAL)
--GO

--SELECT start_time, 
--    completion_time
--    is_source,
--    current_state,
--    failure_state,
--    failure_state_desc
--FROM sys.dm_hadr_automatic_seeding
--order by 1 desc

--SELECT * FROM sys.dm_hadr_physical_seeding_stats;
