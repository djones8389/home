dbcc sqlperf(logspace)

select distinct db_name(a.database_id) DatabaseName
	, physical_name
	, size/128 [Size MB]
	, recovery_model_desc
	, 'backup log '+quotename(db_name(a.database_id))+' to disk = ''F:\SQL_RND_01\TLogBackups\'+db_name(a.database_id)+'.trn'' '
from sys.master_files a
inner join sys.databases b
on b.name = db_name(a.database_id)
where type_desc != 'ROWS'
	and a.database_id > 4
	and recovery_model_desc = 'full'

	--F:\SQL_RND_01\TLogBackups

backup log [DWDataMart] to disk = 'F:\SQL_RND_01\TLogBackups\DWDataMart.trn' with compression, stats = 1
backup log [DWRepository] to disk = 'F:\SQL_RND_01\TLogBackups\DWRepository.trn'  with compression, stats = 1
backup log [OMDWDataMart] to disk = 'F:\SQL_RND_01\TLogBackups\OMDWDataMart.trn' with compression, stats = 1

alter database [DWRepository] set recovery simple with no_wait;
alter database [DWDataMart] set recovery simple with no_wait;
alter database [DWStagingAndConfig] set recovery simple with no_wait;

USE [DWRepository]; checkpoint; DBCC SHRINKFILE ('SM_LOG',1015);/* Space we are creating:  73638.06 MB */
USE [DWDataMart]; checkpoint; DBCC SHRINKFILE ('SM_LOG',765);/* Space we are creating:  9322.69 MB */
USE [DWStagingAndConfig]; checkpoint; DBCC SHRINKFILE ('SM_LOG',205);/* Space we are creating:  1843.00 MB */

