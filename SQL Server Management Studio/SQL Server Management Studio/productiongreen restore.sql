ALTER AVAILABILITY GROUP availabilitygroup REMOVE DATABASE [prodalpha-integration];

declare  @dynamic nvarchar(max) = '';

select @dynamic += CHAR(13) +
		'kill ' + cast(spid as char(10))
       from sys.sysprocesses
where db_name(dbid) = 'prodalpha-integration'

exec(@dynamic);

RESTORE DATABASE [prodalpha-integration] 
FROM DISK = N'E:\SQL_Backups\bborange_prodalpha-integration_2019-09-08.bak' 
WITH FILE = 1, NOUNLOAD, NOREWIND, REPLACE, STATS = 10
, MOVE 'Integration' TO N'E:\SQL_Databases\prodalpha-integration.mdf'
, MOVE 'Integration_log' TO N'T:\SQL_Logs\prodalpha-integration.ldf';