SELECT *
  FROM [BlackBox].[dbo].[tbl_IndexUsage_Aggregate_Daily]
  WHERE   DatabaseName = 'IDIT'
  AND  UsedYear = '2019'
  and IndexName in ('AC_ENTRY_NN20','AC_ENTRY_UN6','AC_ENTRY_NN18','AC_ENTRY_NN21','AC_ENTRY_11NN2','AC_ENTRY_UN15','AC_ENTRY_UN12','P_POL_HEADER_CONTACT_UN2','AC_INSTALLMENT_DETAILS_NN2','AC_INSTALLMENT_DETAILS_NN6','AC_INSTALLMENT_DETAILS_NN3','AC_GL_INTERFACE_NN6','AC_GL_INTERFACE_NN4','AC_GL_INTERFACE_NN3','AC_GL_INTERFACE_INDX8','P_COVER_AGENT_TOT133NN3','P_COVER_AGENT_TOT133NN6','P_COVER_AGENT_TOT133NN4','P_COVER_AGENT_TOT133NN5','P_COVER_NN7','P_COVER_NN8','P_COVER_12NN1','P_COVER_NN3','AC_INSTALLMENT_PERFIDIT_#3','AC_INSTALLMENT_PERFIDIT_#5')--('AC_ENTRY_NN20','AC_ENTRY_UN6','AC_ENTRY_NN18','AC_ENTRY_NN21','AC_ENTRY_11NN2','AC_ENTRY_UN15','AC_ENTRY_UN12')
  ORDER BY 1,2,3,4,5,6

  use idit
  exec sp_spaceused 'AC_ENTRY_NN20'

/*Per Index*/

SELECT 
	[TableName]
	,[index_size-kb] = cast(LTRIM (STR ((CASE WHEN usedpages > pages THEN (usedpages - pages) ELSE 0 END) * 8, 15, 0)) as int)
	, name [index name]	
FROM (

	SELECT 
		OBJECT_NAME(s.object_id) [TableName]
		, (used_page_count) [usedpages]
		, (
			CASE
				WHEN (s.index_id < 2) THEN (in_row_data_page_count + lob_used_page_count + row_overflow_used_page_count)
				ELSE lob_used_page_count + row_overflow_used_page_count
			END
		) [pages]
		, i.name
	FROM sys.dm_db_partition_stats AS s
		INNER JOIN sys.indexes AS i
			ON s.[object_id] = i.[object_id]
			AND s.[index_id] = i.[index_id]
		INNER JOIN sys.tables t
		on t.object_id = s.object_id
		
		where OBJECT_NAME(s.object_id) = 'AC_ENTRY'
			and i.name in ('AC_ENTRY_NN20','AC_ENTRY_UN6','AC_ENTRY_NN18','AC_ENTRY_NN21','AC_ENTRY_11NN2','AC_ENTRY_UN15','AC_ENTRY_UN12')
) A
	order by [index_size-kb] DESC

