select 
	'exec msdb.dbo.sp_update_job @job_name = ''' + name + ''', @enabled = 1'
 from msdb.dbo.sysjobs
where enabled = 1;

--run on secondary

use master;

select 'ALTER AVAILABILITY GROUP '+quotename(name)+' FAILOVER;' 
from sys.availability_groups
order by name 