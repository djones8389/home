-- create job categories for auto-failover jobs.  
USE msdb ;  
GO  

IF NOT EXISTS (SELECT 1 
			   FROM msdb.dbo.syscategories
		       WHERE [name] = 'AG_Enabled' AND [category_class] = 1 AND [category_type] = 1)
	EXEC dbo.sp_add_category  @class=N'JOB',  @type=N'LOCAL',  @name=N'AG_Enabled' ;  
GO 

IF NOT EXISTS (SELECT 1 
			   FROM msdb.dbo.syscategories
		       WHERE [name] = 'AG_Disabled' AND [category_class] = 1 AND [category_type] = 1)
EXEC dbo.sp_add_category  @class=N'JOB', @type=N'LOCAL',  @name=N'AG_Disabled' ;  
GO 
