USE [msdb]
GO

/****** Object:  Job [DBA SQL Agent Jobs Role Change]    Script Date: 08/05/2017 09:31:35 ******/
IF EXISTS (SELECT 1 FROM msdb.dbo.sysjobs WHERE [name] = 'DBA SQL Agent Jobs Role Change')
	EXEC msdb.dbo.sp_delete_job @job_name=N'DBA SQL Agent Jobs Role Change', @delete_unused_schedule=1
GO

/****** Object:  Job [DBA SQL Agent Jobs Role Change]    Script Date: 08/05/2017 09:31:35 ******/
BEGIN TRANSACTION
DECLARE @ReturnCode INT
SELECT @ReturnCode = 0
/****** Object:  JobCategory [[Uncategorized (Local)]]    Script Date: 08/05/2017 09:31:35 ******/
IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'[Uncategorized (Local)]' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'[Uncategorized (Local)]'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=N'DBA SQL Agent Jobs Role Change', 
		@enabled=1, 
		@notify_level_eventlog=0, 
		@notify_level_email=0, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description=N'SUMMARY
Enables/Disables SQL Agent Jobs as a result of a Availability Group failover.

Affects any job with the following category:

AG_Enabled will be enable on Primary, disabled on Secondaries.
DATABASE MAINTENANCE will be enabled on Primary, disabled on Secondaries.
AG_Disabled will be disabled on all.

INTERFACES
Local server / SQL Agent jobs


SUPPORT NOTES
Automatically trigged by failover alert being fired (1480), but can be run manually.
Run in working hours? Y
', 
		@category_name=N'[Uncategorized (Local)]', 
		@owner_login_name=N'sa', @job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Failover]    Script Date: 08/05/2017 09:31:35 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Failover', 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'/**********************************************************************************************
* Script enabled or disabled jobs on the primary and seconday servers based on their Category. 
* 
* AG_Enabled = Enable on Primary, Disable on Secondary.
* DATABASE MAINTENANCE = Enable on Primary, Disable on Secondary.
* AG_Disabled = Disabled on all.
*
* Updating enabled status must be done via sp_update_job as SQL Agent caches msdb.dbo.sysjobs table
* so if you update the table directly it doesn''t take affect until SQL Agent restarts.
**********************************************************************************************/

DECLARE @AGDatabase varchar(20)

DECLARE @isPrimary bit
DECLARE @jobName sysname
DECLARE @jobCategory sysname
DECLARE @jobEnabled bit

SET @AGDatabase = ''SET AS DATABASE IN AG''

/* Decide if we''re the primary server */
IF (sys.fn_hadr_is_primary_replica(@AGDatabase)) = 1
	SET @isPrimary = 1
ELSE
	SET @isPrimary = 0

/* Use a cursor to return all the jobs in the categories we want to control during AG failover. */
DECLARE curJobs cursor FOR
SELECT j.name, c.name, j.enabled
FROM msdb.dbo.sysjobs j
INNER JOIN msdb.dbo.syscategories c on j.category_id = c.category_id
WHERE c.name in (''AG_Enabled'', ''AG_Disabled'', ''DATABASE MAINTENANCE'')

OPEN curJobs
FETCH NEXT FROM curJobs INTO @jobName, @jobCategory, @jobEnabled

WHILE @@FETCH_STATUS = 0
BEGIN
	IF @isPrimary = 1
	BEGIN
		/* If job should be enabled on Primary but isn''t then enable it. */
		IF (@jobCategory = ''AG_Enabled'' OR @jobCategory = ''DATABASE MAINTENANCE'') AND @jobEnabled = 0
		BEGIN
			EXEC msdb.dbo.sp_update_job @job_name = @jobName,@enabled = 1
			PRINT @jobName + '' enabled on Primary.''
		END
		/* If job should be disabled on Primary, but isn''t then disable it. */
		IF @jobCategory = ''AG_Disabled'' AND @jobEnabled = 1
		BEGIN
			EXEC msdb.dbo.sp_update_job @job_name = @jobName,@enabled = 0
			PRINT @jobName + '' disabled on Primary.''
		END
	END
	ELSE
	BEGIN
		/* Running on seconday, make sure all AG jobs are disabled. */
		IF @jobEnabled = 1
		BEGIN
			EXEC msdb.dbo.sp_update_job @job_name = @jobName,@enabled = 0
			PRINT @jobName + '' disabled on Primary.''
		END

	END
	FETCH NEXT FROM curJobs INTO @jobName, @jobCategory, @jobEnabled
END
CLOSE curJobs
DEALLOCATE curJobs

GO
', 
		@database_name=N'msdb', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:

GO


