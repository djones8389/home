USE [msdb]
GO

-- Delete Alert if it already exists.
IF EXISTS (SELECT 1 FROM msdb.dbo.sysalerts WHERE name = 'AG Role Change')
	EXEC msdb.dbo.sp_delete_alert @name=N'AG Role Change'
GO

-- Create alert to trigger job.
EXEC msdb.dbo.sp_add_alert @name=N'AG Role Change', 
		@message_id=1480, 
		@severity=0, 
		@enabled=1, 
		@delay_between_responses=0, 
		@include_event_description_in=0, 
		@category_name=N'[Uncategorized]', 
		@job_name = 'DBA SQL Agent Jobs Role Change'
GO


