select
	db_name(a.database_id) [DBName]
	, cast((size/128/1024) as decimal(6,2))/1024
	, recovery_model_desc
	, physical_name
from sys.master_files a
inner join sys.databases b 
on a.database_id = b.database_id
where type_desc = 'rows'
	and  a.database_id > 4
  --group by 	db_name(a.database_id) 	, recovery_model_desc	, physical_name
  order by 1;
   