USE [msdb]
GO 


declare @drop nvarchar(max)='';
select @drop+=char(13) + '
EXEC msdb.dbo.sp_delete_job @job_id=N'''+cast(job_id as nvarchar(max))+''', @delete_unused_schedule=1
'
from msdb.dbo.sysjobs 
where name = 'DBA - SQL_Magic_Deploy - Permissions'
exec(@drop);

/****** Object:  Job [DBA - SQL_Magic_Deploy - Permissions]    Script Date: 05/06/2019 12:12:43 ******/
BEGIN TRANSACTION
DECLARE @ReturnCode INT
SELECT @ReturnCode = 0
/****** Object:  JobCategory [[Application (MAGIC)]]    Script Date: 05/06/2019 12:12:43 ******/
IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'[Application (MAGIC)]' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'[Application (MAGIC)]'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=N'DBA - SQL_Magic_Deploy - Permissions', 
		@enabled=1, 
		@notify_level_eventlog=0, 
		@notify_level_email=0, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description=N'No description available.', 
		@category_name=N'[Application (MAGIC)]', 
		@owner_login_name=N'dbs', @job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Permissions]    Script Date: 05/06/2019 12:12:43 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Permissions', 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'use master

if not exists (select 1 from sys.syslogins where name = ''NONPROD\SQL_Magic_Deploy'')

	CREATE LOGIN [NONPROD\SQL_Magic_Deploy] FROM WINDOWS WITH DEFAULT_DATABASE=[master]
	GO

ALTER SERVER ROLE [processadmin] ADD MEMBER [NONPROD\SQL_Magic_Deploy]
GO
ALTER SERVER ROLE [securityadmin] ADD MEMBER [NONPROD\SQL_Magic_Deploy]
GO
ALTER SERVER ROLE [sysadmin] ADD MEMBER [NONPROD\SQL_Magic_Deploy]
GO
GRANT VIEW ANY DEFINITION TO [NONPROD\SQL_Magic_Deploy]
GO
use [master]
GO
GRANT VIEW SERVER STATE TO [NONPROD\SQL_Magic_Deploy]
GO


exec sp_MSforeachdb ''

use [?];

if (db_id() in (select database_id
from sys.databases
where database_id > 4
	and is_read_only = 0
	and user_access_desc = ''''MULTI_USER''''
	and name <> ''''CDR_Local''''))

begin
	
	IF NOT EXISTS(SELECT 1 FROM sys.database_principals WHERE name = ''''NONPROD\SQL_Magic_Deploy'''')
	begin
		CREATE USER [NONPROD\SQL_Magic_Deploy] FOR LOGIN [NONPROD\SQL_Magic_Deploy]
	end
		exec sp_addrolemember ''''db_datareader'''', [NONPROD\SQL_Magic_Deploy];
		exec sp_addrolemember ''''db_datawriter'''', [NONPROD\SQL_Magic_Deploy];
		exec sp_addrolemember ''''db_ddladmin'''', [NONPROD\SQL_Magic_Deploy];

end
''', 
		@database_name=N'master', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'Every 5 mins', 
		@enabled=1, 
		@freq_type=4, 
		@freq_interval=1, 
		@freq_subday_type=4, 
		@freq_subday_interval=5, 
		@freq_relative_interval=0, 
		@freq_recurrence_factor=0, 
		@active_start_date=20190329, 
		@active_end_date=99991231, 
		@active_start_time=0, 
		@active_end_time=235959, 
		@schedule_uid=N'c6c471ec-6640-436e-b43b-b86cf1b1fdff'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:
GO



exec sp_start_job @job_name = 'DBA - SQL_Magic_Deploy - Permissions';