select destination_database_name
	, restore_date
	,a.user_name
from msdb.dbo.restorehistory a
where restore_type = 'D'
	and restore_date > (select cast(getdate() as date))
	--and destination_database_name = 'magiclogging'
order by a.restore_date desc 

