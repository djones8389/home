use IDIT

IF OBJECT_ID('tempdb..#ClientTableMetrics') IS NOT NULL DROP TABLE #ClientTableMetrics;

CREATE TABLE #ClientTableMetrics (
    table_name nvarchar(100)
	, rows int
	, reserved_kb nvarchar(20)
	, data_kb nvarchar(20)
	, index_kb nvarchar(20)
	, unused_kb nvarchar(20)
);

declare @dynamic nvarchar(MAX) = '';

select @dynamic +=CHAR(13) +
	'exec sp_spaceused @objname = '''+s.name+'.'+t.name + ''' '
from sys.tables T
inner join sys.schemas S
on s.schema_id = t.schema_id
where type = 'u'
order by t.name;

INSERT #ClientTableMetrics([table_name], [rows], [reserved_kb], [data_kb], [index_kb], [unused_kb])
exec(@dynamic);

INSERT CDR_local.dbo.ClientTableMetrics([table_name], [rows], [reserved_kb], [data_kb], [index_kb], [unused_kb])
SELECT   
	 table_name
	, rows
	, cast(replace(reserved_kb,'kb','') as int) reserved_kb
	, cast(replace(data_kb,'kb','') as int)  data_kb
	, cast(replace(index_kb ,'kb','') as int) index_kb
	, cast(replace(unused_kb ,'kb','') as int)  unused_kb
FROM #ClientTableMetrics

DROP TABLE #ClientTableMetrics;


