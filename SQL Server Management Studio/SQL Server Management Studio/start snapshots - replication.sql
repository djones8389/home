--select 'use '+quotename(name)+' EXEC sp_startpublication_snapshot N''' + 'SQLNative-' + name + ''''
--from sys.databases
--where is_published = 1
--	and name not in ('maghh','magum','magpi','magww','magmp','magut','magur','magup','magtm')

 
-- Check snapshot job status

USE [msdb];

DECLARE @JobActivity TABLE (
	session_id int,
	job_id uniqueidentifier,
	job_name nvarchar(128),
	run_requested_date datetime,
	run_requested_source nvarchar(128),
	queued_date datetime,
	start_execution_date datetime,
	last_executed_step_id int,
	last_executed_step_date datetime,
	stop_execution_date datetime,
	next_scheduled_run_date datetime,
	job_history_id int,
	[message] nvarchar(1024),
	run_status int,
	operator_id_emailed int,
	operator_id_netsent int,
	operator_id_paged int
	);
	
INSERT INTO @JobActivity
EXEC msdb.dbo.sp_help_jobactivity;

select a.*
from (
SELECT JA.job_name,
	CASE 
		WHEN JA.run_requested_date IS NULL THEN 'Never run'
		ELSE 
		CASE ISNULL(JA.run_status,999) 
			WHEN 0 THEN 'Failed' 
			WHEN 1 THEN 'Succeeded'
			WHEN 3 THEN 'Cancelled'
			WHEN 5 THEN 'Unknown'
			WHEN 999 THEN 'In Progress'
			ELSE NULL
		END
	END AS [current_status],
	JA.run_requested_date,
	CONVERT(varchar(20), DATEADD(second, DATEDIFF(second,JA.start_execution_date,ISNULL(JA.stop_execution_date,GETDATE())), 0), 108) AS [run_time]
FROM @JobActivity JA
	JOIN msdb.dbo.sysjobs j 
	ON JA.job_id = j.job_id
	JOIN msdb.dbo.syscategories c 
	ON j.category_id = c.category_id
WHERE c.name = 'REPL-Snapshot'
) a
where current_status <> 'succeeded'
--ORDER BY JA.job_name;

GO