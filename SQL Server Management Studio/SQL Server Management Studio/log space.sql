----sp_whoisactive

--dbcc sqlperf(logspace)

declare @logspace table (
	[dbname] sysname
	, [Log Size (MB)] decimal (8,2)
	, [Log Space Used (%)] decimal (8,2)
	, [status] bit
)

insert @logspace
exec('dbcc sqlperf(logspace)')

select *
from @logspace
order by [Log Space Used (%)] desc;