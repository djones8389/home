use IDIT_PROD_MI  

declare @date datetime = (
select top 1 UPDATE_DATE
from [dbo].[AC_ACCOUNT_TRANSACTION]
)


declare @dynamic nvarchar(max)='';

select @dynamic +=CHAR(13) +'

EXECUTE [msdb].[dbo].[sp_send_dbmail]
    @profile_name = ''SMTP''
    ,@recipients  = ''dave.jones@hiscox.com''
    ,@body        = ''This is to show that the last date we have is '+cast(@date as nvarchar(max))+'''
    ,@subject     =  ''Data''
'

exec(@dynamic)


select top 1 update_date from P_POLICY [Latest Updated P_POLICY Record]
select top 1 update_date from AC_TRANSACTION [Latest Updated AC_TRANSACTION Record]


select max(update_date)  
from P_POLICY NOLOCK