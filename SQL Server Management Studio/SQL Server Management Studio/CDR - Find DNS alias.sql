USE [SQL_Inventory]


SELECT A.* 
--INTO #OSPatching
FROM (

SELECT
			h.[hostname] as [connection],
			s.[serverID],
			s.[instanceName],
			d.[Application],
			extras.DNSAlias as [DNS / ClusterName]
		FROM  [dbo].[Servers] s
			INNER JOIN [dbo].[Hosts] h ON s.hostid = h.[hostID]
			LEFT JOIN [dbo].[Extras_Instance] extras ON s.serverID = extras.ServerID
			LEFT JOIN [dbo].[Databases] d ON s.serverID = d.serverID AND d.databaseName = 'model'
		WHERE     s.serverState <> 'Decommissioned'
UNION ALL 
	
		SELECT 
			c.[sqlclustername] as [connection], 
			s.[serverID],
			s.[instanceName],
			d.[Application],		
			extras.DNSAlias as [DNS / ClusterName]
		FROM [dbo].[Servers] s 
			INNER JOIN [dbo].[Clusters] c ON s.clusterid = c.clusterid
			LEFT JOIN [dbo].[Extras_Instance] extras ON s.serverID = extras.ServerID
			LEFT JOIN [dbo].[Databases] d ON s.serverID = d.serverID AND d.databaseName = 'model'
		WHERE     s.serverState <> 'Decommissioned'
) A 
 where --[DNS / ClusterName] like '%back%'
	[connection] in ('dv0304vri5od-00.azure.hiscox.com','dv0302kjuvrf-00.azure.hiscox.com','dv0302k5to8g-00.azure.hiscox.com','dv030201bmji-00.azure.hiscox.com','dv03025bg2n8-00.azure.hiscox.com','dv0303lurtvx-00.azure.hiscox.com','dv0303jnhj8o-00.azure.hiscox.com','dv03024g5ebv-00.azure.hiscox.com','dv0302nbdggk-00.azure.hiscox.com','dv03021debaf-00.azure.hiscox.com','dv0302zqjus7-00.azure.hiscox.com','dv0302xutydb-00.azure.hiscox.com','dv0302x4zx9t-00.azure.hiscox.com','dv0302a7jjzl-00.azure.hiscox.com','dv030212kbu2-00.azure.hiscox.com','dv0302i7hrvs-00.azure.hiscox.com','dv0302a34ek0-00.azure.hiscox.com','dv0302g6g1zb-00.azure.hiscox.com','dv0302rlfw76-00.azure.hiscox.com','dv0304et1nlk-00.azure.hiscox.com','dv03038595vb-00.azure.hiscox.com','dv0302c1d22e-00.azure.hiscox.com','dv0304vhbgos-00.azure.hiscox.com','dv0303l1ngwe-00.azure.hiscox.com','dv0302pah5bk-00.azure.hiscox.com','dv03029ra5ri-00.azure.hiscox.com','dv03040u5cvs-00.azure.hiscox.com','dv0304sdd90g-00.azure.hiscox.com','dv0302v6mmxa-00.azure.hiscox.com','dv03026xvhgw-00.azure.hiscox.com','dv0302dy936a-00.azure.hiscox.com','dv0302ze0vm4-00.azure.hiscox.com','dv03026ij480-00.azure.hiscox.com','dv0302x05uxv-00.azure.hiscox.com','dv03022h8hl3-00.azure.hiscox.com','dv0302uygp5r-00.azure.hiscox.com','dv030372z74u-00.azure.hiscox.com','dv03028br5tk-00.azure.hiscox.com','dv0302cahisa-00.azure.hiscox.com','dv0303zsfj6i-00.azure.hiscox.com','dv0302njdnwp-00.azure.hiscox.com','dv0304elhwum-00.azure.hiscox.com','dv0303z9li2c-00.azure.hiscox.com','dv0303o4fssw-00.azure.hiscox.com','dv0303yd4olj-00.azure.hiscox.com','dv0304n673jy-00.azure.hiscox.com','dv0302auxa8x-00.azure.hiscox.com','dv0302m406xz-00.azure.hiscox.com','dv0302yske8a-00.azure.hiscox.com','dv0303rd53pi-00.azure.hiscox.com','dv0302gnvljr-00.azure.hiscox.com','dv03023m5qg5-00.azure.hiscox.com','dv0302wez3wr-00.azure.hiscox.com','dv0303wkd94w-00.azure.hiscox.com','dv0304py1erq-00.azure.hiscox.com','dv0304py1erq-01.azure.hiscox.com','dv0303i7r3vy-00.azure.hiscox.com','dv030289zwg5-00.azure.hiscox.com','dv0304r26ej5-00.azure.hiscox.com','dv0303btj04e-00.azure.hiscox.com','dv0304ct8iff-00.azure.hiscox.com')