declare @test table (
	[account name] sysname
	,[type] sysname
	,[privilege] sysname
	,[mapped login name] sysname
	,[permission path] sysname
)

insert @test
exec xp_logininfo 'hiscox\zsqladmin', 'members'
insert @test
exec xp_logininfo 'HISCOX\SQL_DBA_Admin', 'members'

select *
from @test
where [mapped login name] not like '%achungo%'
	and [mapped login name] not like '%chick%'
	and [mapped login name] not like '%sarti%'
	and [mapped login name] not like '%holden%'
	and [mapped login name] not like '%millard%'
	and [mapped login name] not like '%page%'
	and [mapped login name] not like '%holden%'
	and [mapped login name] not like '%meadows%'
	and [mapped login name] not like '%alamedd%'
	and [mapped login name] not like '%kachiwa%'
	and [mapped login name] not like '%jones%'
	and [mapped login name] not like '%silver%'
	and [mapped login name] not like '%bryant%'
	and [mapped login name] not like '%webber%'
	and [mapped login name] not like '%HISCOX\SQL_Service_Cluster%'
	and [mapped login name] not like '%HISCOX\zSQLService%'