select  group_id
	, synchronization_health_desc
	, last_redone_time
	, log_send_queue_size
	, log_send_rate
	, redo_queue_size
	, redo_rate
from sys.dm_hadr_database_replica_states 
where replica_id not in ('48A61BE4-A998-4568-AE57-426ACD4B749A','D3EE27D7-2CC1-4966-9800-A4147BF09637')
	and synchronization_state_desc = 'SYNCHRONIZED'