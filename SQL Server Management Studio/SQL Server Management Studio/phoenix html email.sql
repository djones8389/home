--Monday to Friday, every 5 hrs:  7am, 12 noon, 5pm

USE master;

DROP TABLE IF EXISTS ##results;

	CREATE TABLE ##results  (
		[Region] char(2)
		,ProductCode nvarchar(10)
		,ProductDescription nvarchar(100)
		,NextPolicyNumber int
		--,NewRangeEnd int
	)

	INSERT ##results
	SELECT 'BE' [Region]
	,ProductCode
	,ProductDescription
	,NextPolicyNumber
	--,NewRangeEnd [PolicyNumberEnd]
	FROM PhoenixBelgium.dbo.P$Prod WITH (NOLOCK)
	--WHERE ISNULL(NextPolicyNumber, 0) + 50 >= NewRangeEnd
	--AND NewRangeEnd IS NOT NULL

	UNION ALL

	SELECT 'DE' [Region]
	,ProductCode
	,ProductDescription
	,NextPolicyNumber
	--,NewRangeEnd [PolicyNumberEnd]
	FROM PhoenixGermany.dbo.P$Prod WITH (NOLOCK)
	--WHERE ISNULL(NextPolicyNumber, 0) + 50 >= NewRangeEnd
	--AND NewRangeEnd IS NOT NULL

	UNION ALL

	SELECT 'FR' [Region]
	,ProductCode
	,ProductDescription
	,NextPolicyNumber
	--,NewRangeEnd [PolicyNumberEnd]
	FROM PhoenixFrance.dbo.P$Prod WITH (NOLOCK)
	--WHERE ISNULL(NextPolicyNumber, 0) + 50 >= NewRangeEnd
	--AND NewRangeEnd IS NOT NULL

	UNION ALL

	SELECT 'NL' [Region]
	,ProductCode
	,ProductDescription
	,NextPolicyNumber
	--,NewRangeEnd [PolicyNumberEnd]
	FROM PhoenixHolland.dbo.P$Prod WITH (NOLOCK)
	--WHERE ISNULL(NextPolicyNumber, 0) + 50 >= NewRangeEnd
	--AND NewRangeEnd IS NOT NULL
	 
if((select count(1) from ##results) > 0)  

begin

DECLARE @tableHTML  NVARCHAR(MAX) ;  
  
SET @tableHTML =  'The following products has almost exhausted the Policy Range Limits allocated in i90. Please contact BAs for respective region and i90 team for extending this <br><br>

Regionwise Products that exceeded limit
' +
    N'<table border="1">' +  
    N'<tr><th bgcolor = "#b3b300">Region</th><th bgcolor = "#b3b300">ProductCode</th>' +  
    N'<th bgcolor = "#b3b300">ProductDescription</th><th bgcolor = "#b3b300">NextPolicyNumber</th>' +  
    N'<th bgcolor = "#b3b300">PolicyNumberEnd</th></tr>' +  
    CAST ( ( SELECT td = Region,       '',    
                    td = ProductCode, '',  
                    td = ProductDescription, '',  
                    td = NextPolicyNumber--, '' ,
					--td = NewRangeEnd
              FROM ##results 
              FOR XML PATH('tr'), TYPE   
    ) AS NVARCHAR(MAX) ) +  
    N'</table>' ;  
	 
	select @tableHTML = replace(@tableHTML,'<td>','<td bgcolor = "#ffffe6">');

	EXEC msdb.dbo.sp_send_dbmail 
		 @profile_name = 'DefaultMailAccount'
		,@recipients  = '!PhoenixSupport@hiscox.com'
		,@subject     = 'Monitoring - AMBER - Phoenix Products nearing MAX PolicyNumber Limit'
		,@body = @tableHTML 
		,@body_format = 'HTML' ;

end