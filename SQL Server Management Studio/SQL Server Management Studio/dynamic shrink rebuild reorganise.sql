USE master
GO

SET NOCOUNT ON

DECLARE @SQL varchar(2000);
DECLARE @Kb float = 1024.0;
DECLARE @PageSize float = (SELECT v.low/@Kb FROM master..spt_values v WHERE v.number=1 AND v.type='E');
DECLARE @DataType varchar(5) = '.mdf';
DECLARE @PercentageToKeepAvailable decimal(3,2) = '1.01';


IF OBJECT_ID('tempdb.dbo.#FileSize') IS NOT NULL DROP TABLE #FileSize;
IF OBJECT_ID('tempdb.dbo.#FileStats') IS NOT NULL DROP TABLE #FileStats;
IF OBJECT_ID('tempdb.dbo.#LogSpace') IS NOT NULL DROP TABLE #LogSpace;

CREATE TABLE #FileSize (
	DatabaseName sysname,
	FileName sysname,
	FileSize int,
	FileGroupName sysname,
	LogicalName sysname
);

CREATE TABLE #FileStats (
	FileID int,
	FileGroup int,
	TotalExtents int,
	UsedExtents int,
	LogicalName sysname,
	FileName nchar(520)
);

CREATE TABLE #LogSpace (
	DatabaseName sysname,
	LogSize float,
	SpaceUsedPercent float,
	Status bit
);

INSERT #LogSpace EXEC ('DBCC sqlperf(logspace) with no_infomsgs');


DECLARE cur_Databases CURSOR FAST_FORWARD FOR

SELECT DatabaseName = [name] 
FROM sys.databases 
where state_desc = 'ONLINE' 
	and database_id > 4 
ORDER BY DatabaseName

DECLARE @DatabaseName sysname;

OPEN cur_Databases;

FETCH NEXT FROM cur_Databases INTO @DatabaseName;

WHILE @@FETCH_STATUS = 0
BEGIN
	SET @SQL = '

		USE [' + @DatabaseName + '];
		DBCC showfilestats with no_infomsgs;
		INSERT #FileSize (DatabaseName, FileName, FileSize, FileGroupName, LogicalName)
		SELECT ''' +@DatabaseName + ''', filename, size, ISNULL(FILEGROUP_NAME(groupid),''LOG''), [name]
		FROM dbo.sysfiles sf;

	'
	INSERT #FileStats
	EXECUTE (@SQL);

	FETCH NEXT FROM cur_Databases INTO @DatabaseName
END

CLOSE cur_Databases
DEALLOCATE cur_Databases


DECLARE MyCursor CURSOR FOR

	select  top 5
				DBCCStatement + '  /* Space we are creating:  ' + cast((FileSize - NewUsedSpace) as nvarchar(10)) + ' MB (Shrinking from: '+cast(a.FileSize as varchar(10))+' to: '+cast(a.NewUsedSpace as varchar(10))+')*/' 
				+ 
'

DECLARE @'+DatabaseName+' nvarchar(MAX) = '''';

SELECT @'+DatabaseName+' +=CHAR(13) +
		''ALTER INDEX '' + quotename(C.name) +  '' ON '' + quotename(SCHEMA_NAME(SCHEMA_ID)) + ''.'' + quotename(B.name) 
			+ CASE
				WHEN avg_fragmentation_in_percent BETWEEN 5 AND 30 THEN '' REORGANIZE''
				ELSE '' REBUILD''
			END + '';''
FROM sys.dm_db_index_physical_stats(DB_ID(),NULL,NULL,NULL,NULL) A
INNER JOIN sys.objects B
ON A.object_id = B.object_id
INNER JOIN sys.indexes C
ON B.object_id = C.object_id AND A.index_id = C.index_id
INNER JOIN sys.partitions D
ON B.object_id = D.object_id AND A.index_id = D.index_id
WHERE C.index_id > 0
	and A.avg_fragmentation_in_percent > 30

EXEC(@'+DatabaseName+');

EXEC sp_updatestats;
		' 
		from (
			SELECT
				fsi.DatabaseName AS [DatabaseName],
				RTRIM(fsi.LogicalName) as [LogicalName],
				RTRIM(fsi.FileName) as [FileName] ,
				CAST(fsi.FileSize*@PageSize/@Kb as decimal(15,2)) AS [FileSize],
				CAST(ISNULL((fs.UsedExtents*@PageSize*8.0/@Kb), fsi.FileSize*@PageSize/@Kb * ls.SpaceUsedPercent/100.0) as decimal(15,2)) AS [UsedSpace],
				CASE WHEN RTRIM(fsi.FileName) like '%.mdf%' 
					THEN	CONVERT(Nvarchar(MAX), CONVERT(DECIMAL(7,0), ROUND(CAST(ISNULL((fs.UsedExtents*@PageSize*8.0/@Kb), fsi.FileSize*@PageSize/@Kb * ls.SpaceUsedPercent/100.0) as decimal(15)) *@PercentageToKeepAvailable, 0)))
					ELSE CONVERT(Nvarchar(MAX), CONVERT(DECIMAL(7,0), ROUND(CAST(ISNULL((fs.UsedExtents*@PageSize*8.0/@Kb), fsi.FileSize*@PageSize/@Kb * ls.SpaceUsedPercent/100.0) as decimal(15)) +100, 0)))		
				END AS [NewUsedSpace],
				CAST(ISNULL(((fsi.FileSize - UsedExtents*8.0)*@PageSize/@Kb), (100.0-ls.SpaceUsedPercent)/100.0 * fsi.FileSize*@PageSize/@Kb) as decimal(15,2)) [FreeSpace],
				CAST(ISNULL(((fsi.FileSize - UsedExtents*8.0) / fsi.FileSize * 100.0), 100-ls.SpaceUsedPercent) as decimal(15,2)) [FreeSpace %],
				CASE WHEN RTRIM(fsi.FileName) like '%.ldf%' 
				THEN 'USE ' + quotename(fsi.DatabaseName) + '; 

DBCC SHRINKFILE (''' + RTRIM(fsi.LogicalName) + + '''' + ',' + CONVERT(Nvarchar(MAX), CONVERT(DECIMAL(7,0), ROUND(CAST(ISNULL((fs.UsedExtents*@PageSize*8.0/@Kb), fsi.FileSize*@PageSize/@Kb * ls.SpaceUsedPercent/100.0) as decimal(15))+ 100, 0))) + ');'
				ELSE 'USE ' + quotename(fsi.DatabaseName) + '; 

DBCC SHRINKFILE (''' + RTRIM(fsi.LogicalName) + + '''' + ',' + CONVERT(Nvarchar(MAX), CONVERT(DECIMAL(7,0), ROUND(CAST(ISNULL((fs.UsedExtents*@PageSize*8.0/@Kb), fsi.FileSize*@PageSize/@Kb * ls.SpaceUsedPercent/100.0) as decimal(15))*@PercentageToKeepAvailable, 0))) + ');'
				END AS 	[DBCCStatement]

			FROM #FileSize fsi
			LEFT JOIN #FileStats fs
			ON fs.FileName = fsi.FileName
			LEFT JOIN #LogSpace ls
			ON ls.DatabaseName = fsi.DatabaseName

			where RTRIM(fsi.FileName) like '%'+@DataType+'%'
				
		) a
		order by (FileSize - NewUsedSpace) desc;


DECLARE @TSQL NVARCHAR(MAX)

OPEN MyCursor

FETCH NEXT FROM MyCursor INTO @TSQL
WHILE @@FETCH_STATUS = 0

BEGIN

	 print(@TSQL);
	 FETCH NEXT FROM MyCursor INTO @TSQL

END
CLOSE MyCursor
DEALLOCATE  MyCursor

