declare @dynamic nvarchar(MAX)='';

declare @dt varchar(100) = (SELECT CONVERT(varchar, getdate(), 121))

select @dynamic+=CHAR(13) + 'BACKUP LOG '+quotename(name)+' TO  DISK = N''\\hiscox.com\backup\SQL-Log\HXPSQLWM20201\'+name+'\'+name+'_'+(select replace(@dt,':','.'))+'.trn'' 
	WITH NOFORMAT, NOINIT,  NAME = N'''+name+'_'+(select replace(@dt,':','.'))+''', SKIP, REWIND, NOUNLOAD,  STATS = 10'
from sys.databases
where recovery_model_desc = 'full'
	and database_id > 4
  
PRINT(@dynamic);

