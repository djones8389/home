declare @dropjob nvarchar(max)='';

select @dropjob+=char(13) + 'EXEC msdb.dbo.sp_delete_job @job_name = N'''+name+''' , @delete_unused_schedule=1' 
from msdb.dbo.sysjobs
where name in (
'CDR: General Maintenance',
'CDR: Check - All DBs',
'DBA - Plan Cache Bloating Maintenance',
'DBA: IndexOptimize',
'CDR: UploadBackupStatus',
'CDR: Optimise - All DBs'
)


print(@dropjob)