use master

declare @login sysname = 'NONPROD\SQL_US_Data_QA_ALL_RO'
declare @dynamic nvarchar(max)='';
select @dynamic +=CHAR(13) +  '

use ['+name+']; 

if not exists (select 1 from sys.syslogins where name = '''+@login+''')
	CREATE LOGIN '+quotename(@login)+' FROM WINDOWS WITH DEFAULT_DATABASE=[master]
	
	IF NOT EXISTS(SELECT 1 FROM sys.database_principals WHERE name = '''+@login+''')
	begin
		CREATE USER '+quotename(@login)+' FOR LOGIN '+quotename(@login)+'
	end
		exec sp_addrolemember ''db_datareader'', '+quotename(@login)+';
	 
'
from sys.databases
where database_id > 4
	and is_read_only = 0 
	and user_access_desc = 'MULTI_USER'
	
exec(@dynamic);

use [ArchiveShredderSIT]
GO
 
GRANT select ON SCHEMA::[MustangData] TO [NONPROD\SQL_US_Data_QA_ALL_RO]
GO
GRANT select ON SCHEMA::[Mustang] TO [NONPROD\SQL_US_Data_QA_ALL_RO]
GO
GRANT select ON SCHEMA::Prototype TO [NONPROD\SQL_US_Data_QA_ALL_RO]
GO
GRANT select ON SCHEMA::Shredder TO [NONPROD\SQL_US_Data_QA_ALL_RO]
GO
GRANT view definition ON SCHEMA::[MustangData] TO [NONPROD\SQL_US_Data_QA_ALL_RO]
GO
GRANT view definition ON SCHEMA::[Mustang] TO [NONPROD\SQL_US_Data_QA_ALL_RO]
GO
GRANT view definition ON SCHEMA::Prototype TO [NONPROD\SQL_US_Data_QA_ALL_RO]
GO
GRANT view definition ON SCHEMA::Shredder TO [NONPROD\SQL_US_Data_QA_ALL_RO]
GO


use ArchiveShredderDev
GO
 
GRANT select ON SCHEMA::[MustangData] TO [NONPROD\SQL_US_Data_QA_ALL_RO]
GO
GRANT select ON SCHEMA::[Mustang] TO [NONPROD\SQL_US_Data_QA_ALL_RO]
GO
GRANT select ON SCHEMA::Prototype TO [NONPROD\SQL_US_Data_QA_ALL_RO]
GO
GRANT select ON SCHEMA::Shredder TO [NONPROD\SQL_US_Data_QA_ALL_RO]
GO
GRANT view definition ON SCHEMA::[MustangData] TO [NONPROD\SQL_US_Data_QA_ALL_RO]
GO
GRANT view definition ON SCHEMA::[Mustang] TO [NONPROD\SQL_US_Data_QA_ALL_RO]
GO
GRANT view definition ON SCHEMA::Prototype TO [NONPROD\SQL_US_Data_QA_ALL_RO]
GO
GRANT view definition ON SCHEMA::Shredder TO [NONPROD\SQL_US_Data_QA_ALL_RO]
GO
