use SQL_Inventory

	DECLARE  @dt AS DATETIME
		, @ScriptName VARCHAR(128) = 'UpsertFailedAgentJobs.ps1'
		, @HideReplication bit

	IF DATENAME(dw,GETDATE()) = 'Monday'
		SELECT @dt = CONVERT(DATETIME,DATEADD(hh,-72,GETDATE()));
	ELSE
		SELECT @dt = CONVERT(DATETIME,DATEADD(hh,-24,GETDATE()));
	
	SELECT cast(@dt as date)
		
		SELECT top 100
			cast(faj.lastRunDate as date)
			,tb.instanceName AS InstanceName
			,tb.hostname AS HostName
			,tb.Environment AS [Environment]
			,tb.lastUpdated
			,saj.jobName AS [Job]
			,faj.errorMessage AS [Error]
			,CONVERT(DATETIME,faj.lastRunDate,120) AS [LastErrorDate]
			,faj.Alerted
			,lfc.[LastFailedComment]
			,tb.[Sorting]
		FROM [vw_FailedAgentJobs_SSRSReport_GetCommissionedInstances] tb

		INNER JOIN dbo.FailedAgentJobs faj ON faj.serverID = tb.serverid
		INNER JOIN dbo.ServerAgentJobs saj ON faj.jobID = saj.jobID
		LEFT JOIN [vw_FailedAgentJobs_SSRSReport_GetDBAComments] lfc 
		on lfc.hostName = tb.hostName
			and lfc.instanceName = tb.instanceName
			and lfc.job = saj.jobName
		WHERE    
			errorMessage not like '%replication agent%'
			and cast(faj.lastRunDate as date) >=  cast(@dt as date)
			--AND faj.lastRunDate = (SELECT MAX(lastRunDate)FROM [SQL_Inventory].[dbo].vw_FailedAgentJobs where jobID = faj.jobID)   
			AND tb.status IN ('E','F','P','R')
			order by 1 desc
