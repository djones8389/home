select schema_name(schema_id)
	, name
	, 'drop table ' + quotename(schema_name(schema_id)) + '.' + quotename(name) + ';'
	, 'truncate table ' + quotename(schema_name(schema_id)) + '.' + quotename(name) + ';'
	, 'select count(1) from ' + quotename(schema_name(schema_id)) + '.' + quotename(name) + ' as '+quotename(name)+'' + '--truncate table ' + quotename(schema_name(schema_id)) + '.' + quotename(name) + ';'
from sys.tables
where schema_name(schema_id) = 'dw'
	and name in ('PremiumType','LineOfBusinessGroup','SALESUNITGROUP','Party','DistributionChannel','SubIndustry','PartyRole','PolicyVersionSectionLondonMarket','ClaimEvent','PartyRoleType','PolicyVersionSectionRiskCarrier','PolicyVersionType','TerritorialScopeGroup','TransactionQualifierType','ExchangeRate','ClaimLondonMarket','Policy','ClaimOSFee','ExchangeRateType','TrustFund','PolicyAdditionalCharge','ClaimOSIndem','ExpertType','UnderwritingAgency','PolicyEventType','ISOCountry','BPUBusinessUnit','LineOfBusiness','PolicyLimit','ReservingClass','BPUDistributionChannel','PolicyPremium','ReservingClassGroup','PolicyPremiumDeduction','Broker','LloydsCatastrophe','BrokerGroup','CoverageClass','RiskCarrier','PolicySection','BusinessCategory','LloydsRiskCode','PolicySigningTransaction','PolicySigningTransactionType','LloydsSection','SalesUnit','PolicyStatus','PolicyVersion','SIILineOfBusiness','Claim','DeclineClaimReason','ClaimBasisType','SourceMethodOfPlacement','SourceSystem','DeductionType','SourceSystemGroup','PolicyVersionSection','NAICAnnualStatementLine','PolicyAdditionalChargeType','ClaimPaymentFee','FILType','PolicyAdditionalChargeTypeCategory','ClaimPaymentIndem','Address','GLLineOfBusiness','PolicyCoverage','ClaimSection','PolicyCoverageReservingClass','PolicyCoverageRiskCode','IndemPaymentType','ClaimStatus','Industry','IndustryClass','PolicyEvent','BPUBusinessClass','InitialRiskCarrierType')
 
   truncate table [DW].[Address];
truncate table [DW].[BPUBusinessClass];
truncate table [DW].[BPUBusinessUnit];
truncate table [DW].[BPUDistributionChannel];
truncate table [DW].[Broker];
truncate table [DW].[BrokerGroup];
truncate table [DW].[BusinessCategory];
truncate table [DW].[Claim];
truncate table [DW].[ClaimBasisType];
truncate table [DW].[ClaimEvent];
truncate table [DW].[ClaimLondonMarket];
truncate table [DW].[ClaimOSFee];
truncate table [DW].[ClaimOSIndem];
truncate table [DW].[ClaimPaymentFee];
truncate table [DW].[ClaimPaymentIndem];
truncate table [DW].[ClaimSection];
truncate table [DW].[ClaimStatus];
truncate table [DW].[CoverageClass];
truncate table [DW].[DeclineClaimReason];
truncate table [DW].[DeductionType];
truncate table [DW].[DistributionChannel];
truncate table [DW].[ExchangeRate];
truncate table [DW].[ExchangeRateType];
truncate table [DW].[ExpertType];
truncate table [DW].[FILType];
truncate table [DW].[GLLineOfBusiness];
truncate table [DW].[IndemPaymentType];
truncate table [DW].[Industry];
truncate table [DW].[IndustryClass];
truncate table [DW].[InitialRiskCarrierType];
truncate table [DW].[ISOCountry];
truncate table [DW].[LineOfBusiness];
truncate table [DW].[LineOfBusinessGroup];
truncate table [DW].[LloydsCatastrophe];
truncate table [DW].[LloydsRiskCode];
truncate table [DW].[LloydsSection];
truncate table [DW].[TransactionQualifierType];
truncate table [DW].[NAICAnnualStatementLine];
truncate table [DW].[Party];
truncate table [DW].[PartyRole];
truncate table [DW].[PartyRoleType];
truncate table [DW].[Policy];
truncate table [DW].[PolicyAdditionalCharge];
truncate table [DW].[PolicyAdditionalChargeType];
truncate table [DW].[PolicyAdditionalChargeTypeCategory];
truncate table [DW].[PolicyCoverage];
truncate table [DW].[PolicyCoverageReservingClass];
truncate table [DW].[PolicyCoverageRiskCode];
truncate table [DW].[PolicyEvent];
truncate table [DW].[PolicyEventType];
truncate table [DW].[PolicyLimit];
truncate table [DW].[PolicyPremium];
truncate table [DW].[PolicyPremiumDeduction];
truncate table [DW].[PolicySection];
truncate table [DW].[PolicySigningTransaction];
truncate table [DW].[PolicySigningTransactionType];
truncate table [DW].[PolicyStatus];
truncate table [DW].[PolicyVersion];
truncate table [DW].[PolicyVersionSection];
truncate table [DW].[PolicyVersionSectionLondonMarket];
truncate table [DW].[PolicyVersionSectionRiskCarrier];
truncate table [DW].[PolicyVersionType];
truncate table [DW].[PremiumType];
truncate table [DW].[ReservingClass];
truncate table [DW].[ReservingClassGroup];
truncate table [DW].[RiskCarrier];
truncate table [DW].[SalesUnit];
truncate table [DW].[SalesUnitGroup];
truncate table [DW].[SIILineOfBusiness];
truncate table [DW].[SourceMethodOfPlacement];
truncate table [DW].[SourceSystem];
truncate table [DW].[SourceSystemGroup];
truncate table [DW].[SubIndustry];
truncate table [DW].[TerritorialScopeGroup];
truncate table [DW].[TrustFund];
truncate table [DW].[UnderwritingAgency];