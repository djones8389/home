select  @@SERVERNAME
	, backup_start_date
	, database_name
	, type
	 , user_name
	 , name	
	 , DATEDIFF(minute, backup_start_date,backup_finish_date) [Duration (min)]
	 , is_copy_only
from msdb.dbo.backupset  a
where database_name not in ('model','msdb','master','CDR_local')
     and type  not  in ('L','i', 'l')
	-- and backup_start_date > cast(getdate() as date)
	--and database_name = 'IDIT'
	--and name != 'CommVault Galaxy Backup'
order by  backup_start_date desc;

/*
 
declare @log table (
	DatabaseName sysname
	, [Log Size (MB)] decimal (8,2)
	, [Log Space Used (%)] decimal (8,2)
	, [Status] bit
)

insert @log
exec ('dbcc sqlperf(logspace)')

select *
from @log
where DatabaseName not in ('model','tempdb','master','msdb','cdr_local')
order by 2 desc; 

 exec  xp_readerrorlog


*/