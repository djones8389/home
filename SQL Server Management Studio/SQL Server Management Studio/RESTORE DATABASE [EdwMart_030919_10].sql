
RESTORE DATABASE [EdwMart_030919_10] FROM DISK = N'E:\SQL_RND_05\SQL_Backups\EDWMart_2019-10-28.bak' 
WITH FILE = 1, NOUNLOAD, NOREWIND, REPLACE, STATS = 1
, MOVE 'PRIMARY' TO N'E:\SQL_RND_03\SQL_Databases\EdwMart_030919_10.mdf'
, MOVE 'CORE' TO N'E:\SQL_RND_03\SQL_Databases\EdwMart_CORE_030919_10.ndf'
, MOVE 'FactEarnedPremiumMovement' TO N'E:\SQL_RND_03\SQL_Databases\EdwMart_FactEarnedPremiumMovement_030919_10.ndf'
, MOVE 'FactLivePolicyCount' TO N'E:\SQL_RND_03\SQL_Databases\EdwMart_FactLivePolicyCount_030919_10.ndf'
, MOVE 'FactLivePolicyLimit' TO N'E:\SQL_RND_03\SQL_Databases\EdwMart_FactLivePolicyLimit_030919_10.ndf'
, MOVE 'FactLivePolicyPremium' TO N'E:\SQL_RND_03\SQL_Databases\EdwMart_FactLivePolicyPremium_030919_10.ndf'
, MOVE 'FactWrittenPremiumDeduction' TO N'E:\SQL_RND_03\SQL_Databases\EdwMart_FactWrittenPremiumDeduction_030919_10.ndf'
, MOVE 'FactWrittenPremiumMovement' TO N'E:\SQL_RND_03\SQL_Databases\EdwMart_FactWrittenPremiumMovement_030919_10.ndf'
, MOVE 'Indexes' TO N'E:\SQL_RND_03\SQL_Databases\EdwMart_Indexes_030919_10.ndf'
, MOVE 'ReportingCore' TO N'E:\SQL_RND_03\SQL_Databases\EdwMart_ReportingCore_030919_10.ndf'
, MOVE 'ReportingIndexes' TO N'E:\SQL_RND_03\SQL_Databases\EdwMart_ReportingIndexes_030919_10.ndf'
, MOVE 'Retail' TO N'E:\SQL_RND_03\SQL_Databases\EdwMart_Retail_030919_10.ndf'
, MOVE 'LogFile' TO N'E:\SQL_SEQ_01\SQL_Logs\EdwMart_LogFile.ldf'; 
