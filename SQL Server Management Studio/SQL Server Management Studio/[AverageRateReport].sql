USE [PhoenixFrance]
GO

/****** Object:  StoredProcedure [dbo].[AverageRateReport]    Script Date: 21/02/2019 05:19:31 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

--exec AverageRateReport '2018.11.30','10000459'

/****** Object:  StoredProcedure [dbo].[AverageRateReport]    Script Date: 05/10/2018 11:31:47 ******/
CREATE PROCEDURE [dbo].[AverageRateReport]
	--DECLARE
	@ToDate DATETIME
	,@AgentReference VARCHAR(15)
	--WITH RECOMPILE
AS
--SET @ToDate = GETDATE()
--SET @AgentReference = ''
BEGIN
	SET NOCOUNT ON;

	BEGIN TRY
		SET ANSI_WARNINGS OFF;

		IF OBJECT_ID('tempdb..#ARRWORK', 'U') <> 0
			DROP TABLE [#ARRWORK]


		CREATE TABLE [#ARRWORK] (
			[AgentReference] [varchar](8) NULL
			,[AgentName] [varchar](40) NULL
			,[Underwriter] [varchar](40) NULL
			,[CurrencyType] [varchar](3) NULL
			,[ClientReference] [varchar](8) NULL
			,[ClientName] [varchar](62) NULL
			,[DocumentDesc1] [varchar](255) NULL
			,[CompanyRegistrationNumber] [varchar](14) NULL
			,[CompanyAPECategory] [varchar](10) NULL
			,[CGReference] [varchar](50) NULL
			,[PolicyNumber] [varchar](15) NULL
			,[PreviousPolicyNo] [varchar](20) NULL
			,[NoOfInstalments] [int] NULL
			,[PaymentLoad] [float] NULL
			,[Payment] [varchar](20) NULL
			,[ProductCode] [varchar](8) NULL
			,[CommissionRate] [varchar](31) NULL
			,[FixedCommissionRate] [varchar](1) NULL
			,[InceptionDate] [datetime] NULL
			,[RenewalDate] [datetime] NULL
			,[MethodOfPlacement] [varchar](4) NULL
			,[NumberOfRisks] [int] NULL
			,[Address] [varchar](126) NULL
			,[Postcode] [varchar](10) NULL
			,[Description] [varchar](80) NULL
			,[Residence] [varchar](2) NULL
			,[HouseType] [varchar](2) NULL
			,[Residency] [varchar](1) NULL
			,[BldOwnerReconstruction] [varchar](1) NULL
			,[BuildingsPremium] [money] NULL
			,[BuildingSI] [float] NULL
			,[BuildingsAreaPremium] [money] NULL
			,[BuildingsArea] [float] NULL
			,[BldAverageCalculation] [float] NULL
			,[BldAreaCalc] [float] NULL
			,[ContentsPremium] [money] NULL
			,[ContentsSumInsured] [money] NULL
			,[ContAverageCalc] [float] NULL
			,[ArtPremium] [money] NULL
			,[ArtSI] [money] NULL
			,[ArtAverage] [float] NULL
			,[PPSPremium] [money] NULL
			,[PPSSI] [money] NULL
			,[AveragePPSCalc] [float] NULL
			,[LiabilitySIPremium] [float] NULL
			,[LiabilityAreaPremium] [float] NULL
			,[LiabilityAverageSIRate] [float] NULL
			,[LiabilityAverageAreaRate] [float] NULL
			,[LiabilityRenterPremium] [float] NULL
			,[LiabilityRenterArea] [float] NULL
			,[LiabilityRenterAverageRate] [float] NULL
			,[LiabilityPriveePremium] [float] NULL
			,[LiabReduction] [money] NULL
			,[LegalAidPremium] [money] NULL
			,[PAPremium] [money] NULL
			,[PASI] [money] NULL
			,[ADAverageRate] [money] NULL
			,[AnnBusIntPremium] [money] NULL
			,[BusIntSI] [money] NULL
			,[BIAverageRate] [money] NULL
			,[RSPMaterialPremium] [money] NULL
			,[RSPMaterialSI] [money] NULL
			,[RSPMaterialAverageCalc] [float] NULL
			,[RSPMusicPremium] [money] NULL
			,[RSPMusicSI] [money] NULL
			,[RSPMusicAverageCalc] [float] NULL
			,[RSPExhibitPremium] [money] NULL
			,[RSPExhibitSI] [money] NULL
			,[RSPExhibitAverageCalc] [float] NULL
			,[RSPArtPremium] [money] NULL
			,[RSPArtSI] [money] NULL
			,[RSPArtAverageCalc] [float] NULL
			,[RSPGroupPremium] [money] NULL
			,[RSPGroupSI] [money] NULL
			,[RSPGroupAverageCalc] [float] NULL
			,[PILimit] [money] NULL
			,[PIPremium] [money] NULL
			,[PISI] [money] NULL
			,[PIAverageCalc] [float] NULL
			,[TotalSI] [money] NULL
			,[BlankColumn1] [varchar](1) NULL
			,[BlankColumn2] [varchar](1) NULL
			,[BldReduction] [money] NULL
			,[ContReduction] [money] NULL
			,[ArtReduction] [money] NULL
			,[ItemsReduction] [money] NULL
			,[PIReduction] [money] NULL
			,[USPIReduction] [money] NULL
			,[FFBIndex] [int] NULL
			,[Discount] [float] NULL
			,[AnnAddressPremium] [money] NULL
			,[DueAddressPremium] [money] NULL
			,[UnderwriterIndicator] [varchar](1) NULL
			,[GenSectionNo] [varchar](3) NULL
			,[AgentPolicyNumber] [varchar](15) NULL
			,[AnnualPremium] [money] NULL
			,[DuePremium] [money] NULL
			,[AnnualPremiumMinusTax] [money] NULL
			,[DuePremiumMinusTax] [money] NULL
			,[AnnualCommission] [money] NULL
			,[DueCommission] [money] NULL
			,[AnnualTax] [money] NULL
			,[DueTax] [money] NULL
			,[StopCodes] [varchar](255) NULL
			,[Diary] [varchar](100) NULL
			,[PolicyYear] [int] NULL
			,[AddressNumber] [int] NULL
			,[IndustryCode] [varchar](25) NULL
			,[DDAuthorisedDate] [varchar](12) NULL
			,[BrokerContact] [varchar](25) NULL
			,[ContributorCode] [varchar](8) NULL
			,[EndorseInProgress] [varchar](1) NULL
			,[AnnCommercialPremium] [money] NULL
			,[DueCommercialPremium] [money] NULL
			,[AnnFees] [money] NULL
			,[DueFees] [money] NULL
			,[SurveyType] [varchar](80) NULL
			,[SurveyComplete] [varchar](1) NULL
			,[SurveyDate] [varchar](12) NULL
			,[MaterialExcess] [varchar](50) NULL
			,[RSPMusicExcess] [varchar](50) NULL
			,[RSPExhibitExcess] [varchar](50) NULL
			,[RSPArtExcess] [varchar](50) NULL
			,[RSPGroupExcess] [varchar](50) NULL
			,[RCOPremium] [money] NULL
			,[RCOSI] [money] NULL
			,[RCOAverage] [float] NULL
			,[RCEPremium] [money] NULL
			,[RCESI] [money] NULL
			,[RCEAverage] [float] NULL
			,[AdminFeesApply] [varchar](1) NULL
			,[AdminFeeExempt] [varchar](1) NULL
			,[DueAdminFees] [money] NULL
			,[DueAdminFeesTax] [money] NULL
			,[CyberRisk] Char(1)  Default 'N'
			,[HasasReference] VARCHAR(8) NULL
			)

		INSERT #ARRWORK
		SELECT P$AGENT.AgentReference
			,P$AGENT.AgentName
			,P$POLICY.Underwriter
			,P$POLICY.CurrencyType
			,P$POLICY.ClientReference
			,CASE 
				WHEN p$client.IndividualOrCompany = 'C'
					THEN RTRIM(LTRIM(ISNULL(p$client.TradingOrCompanyName, '')))
				ELSE RTRIM(LTRIM(RTRIM(LTRIM(ISNULL(p$client.title, ''))) + ' ' + RTRIM(LTRIM(ISNULL(p$client.forename, ''))) + ' ' + RTRIM(LTRIM(ISNULL(p$client.surname, '')))))
				END AS "ClientName"
			,CASE 
				WHEN P$POLICY.ProductCode = 'RCP-01'
					THEN REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(CONVERT(VARCHAR(100), P$COVRCP.DocumentDesc1), '"', ''), ',', ''), CHAR(10), ''), CHAR(13), ' '), CHAR(9), '')
				WHEN P$POLICY.ProductCode = 'CDH01'
					THEN REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(CONVERT(VARCHAR(100), P$COVCDH.DocumentDesc1), '"', ''), ',', ''), CHAR(10), ''), CHAR(13), ' '), CHAR(9), '')
				WHEN P$POLICY.ProductCode = 'FSC01'
					THEN REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(CONVERT(VARCHAR(100), P$COVFSC2.DocumentDesc1), '"', ''), ',', ''), CHAR(10), ''), CHAR(13), ' '), CHAR(9), '')
				ELSE ''
				END AS "DocumentDesc1"
			,P$CLIENT.CompanyRegistrationNumber
			,P$CLIENT.CompanyAPECategory
			,P$POLICY.CGReference
			,P$POLICY.PolicyNumber
			,P$POLICY.PreviousPolicyNo
			,P$POLICY.NoOfInstalments
			,P$POLICY.PaymentLoad
			,CASE 
				WHEN P$POLICY.Payment = 'PY02'
					THEN 'Client - Prélèvement'
				WHEN P$POLICY.Payment = 'PY03'
					THEN 'Client - Chèque'
				WHEN P$POLICY.Payment = 'PY01'
					THEN 'Courtier - Chèque'
				ELSE ''
				END AS Payment
			,P$PROD.PolicyNumberPrefix AS ProductCode
			,CONVERT(VARCHAR, P$POLICY.CommissionRate) + '%' AS CommissionRate
			,P$POLICY.FixedCommissionRate
			,(
				SELECT MIN(InceptionDate)
				FROM p$policy p WITH (NOLOCK)
				WHERE p.policynumber = LEFT(P$POLICY.PolicyNumber, 10)
					AND PolicyYear = 1
				) AS InceptionDate
			,P$POLICY.RenewalDate
			,P$POLICY.MethodOfPlacement
			
			,CASE WHEN P$POLICY.ProductCode = 'MRP01' Then
			   (
			    SELECT Count(t.OccupantNbr)
			    FROM P$TaxSplitHeader t WITH (NOLOCK)
			    WHERE t.PolicyNumber = P$POLICY.PolicyNumber
	            AND t.Version = P$POLICY.Version and t.Deleted  = 0
			   )				
			 Else
			
			   (
				SELECT COUNT(AddressNumber)
				FROM p$cover c WITH (NOLOCK)
				WHERE c.policynumber = P$POLICY.PolicyNumber
					AND c.Version = P$POLICY.Version
					AND c.Deleted <> 'X'
			   )
				End AS NumberOfRisks
           
			,CASE WHEN P$POLICY.ProductCode = 'MRP01' Then

					RTRIM(LTRIM(ISNULL(P$TaxSplitHeader.Adresse, ''))) 
                ELSE 
      				RTRIM(LTRIM(ISNULL(P$ADDR.addressline1, ''))) + CASE 
					WHEN RTRIM(LTRIM(ISNULL(P$ADDR.addressline2, ''))) <> ''
						THEN ', ' + RTRIM(LTRIM(ISNULL(P$ADDR.addressline2, '')))
					ELSE ''
					END + CASE 
					WHEN RTRIM(LTRIM(ISNULL(P$ADDR.addressline3, ''))) <> ''
						THEN ', ' + RTRIM(LTRIM(ISNULL(P$ADDR.addressline3, '')))
					ELSE ''
					END + CASE 
					WHEN RTRIM(LTRIM(ISNULL(P$ADDR.addressline4, ''))) <> ''
						THEN ', ' + RTRIM(LTRIM(ISNULL(P$ADDR.addressline4, '')))
					ELSE ''
					END
				END AS Address
			
			,CASE WHEN P$POLICY.ProductCode = 'MRP01' Then
				P$TaxSplitHeader.PostalCode
			 ELSE
			 	P$ADDR.Postcode
             END As Postcode
			 
			 
			,P$DESC.Description  
			
		        ,CASE 
				WHEN LEFT(P$POLICY.ProductCode, 5) = 'FRABC'
					THEN P$COVRBD.Residence
				WHEN P$POLICY.ProductCode = 'FRABC-03'
					THEN P$COVRBD.MPHResidence
				WHEN P$POLICY.ProductCode = 'CCC01'
					THEN P$COVRBD.Residence
				WHEN P$POLICY.ProductCode = 'CCS01'
					THEN P$COVCCS.Residence
				WHEN P$POLICY.ProductCode = 'FBX-01'
					THEN P$covfbx.Residence
				WHEN LEFT(P$POLICY.ProductCode, 3) = 'CPX'
					THEN P$COVCPX.OccupationType
				ELSE ''
				END AS Residence
			,CASE 
				WHEN LEFT(P$POLICY.ProductCode, 5) = 'FRABC'
					THEN P$COVRBD.HouseType
				WHEN P$POLICY.ProductCode = 'FRABC-03'
					THEN P$COVRBD.MPHHouseType
				WHEN P$POLICY.ProductCode = 'CCC01'
					THEN P$COVRBD.CCCHouseType
				WHEN P$POLICY.ProductCode = 'CCS01'
					THEN P$COVCCS.HouseType
				WHEN P$POLICY.ProductCode = 'FBX-01'
					THEN P$covfbx.HouseType
				WHEN LEFT(P$POLICY.ProductCode, 3) = 'OBH'
					THEN P$covobh.HouseType
				WHEN LEFT(P$POLICY.ProductCode, 3) = 'CPX'
					THEN P$COVCPX.BuildingType
				WHEN LEFT(P$POLICY.ProductCode, 3) = 'FSC'
					THEN P$COVFSC.HouseType
				ELSE ''
				END AS HouseType
			,CASE 
				WHEN LEFT(P$POLICY.ProductCode, 5) = 'FRABC'
					THEN P$COVRBD.Residency
				WHEN P$POLICY.ProductCode = 'CCC01'
					THEN P$COVRBD.CCCResidency
				WHEN P$POLICY.ProductCode = 'CCS01'
					THEN P$COVCCS.Residency
				WHEN P$POLICY.ProductCode = 'FBX-01'
					THEN P$covfbx.Residency
				WHEN LEFT(P$POLICY.ProductCode, 3) = 'OBH'
					THEN p$covobh.Residency
				WHEN LEFT(P$POLICY.ProductCode, 3) = 'CPX'
					THEN P$COVCPX.OwnerType
				WHEN LEFT(P$POLICY.ProductCode, 3) = 'FSC'
					THEN P$COVFSC.Residency
				WHEN P$POLICY.ProductCode = 'MRP01'
					THEN CASE 
						WHEN P$TaxSplitHeader.Status = '01'
							THEN 'L'
						WHEN P$TaxSplitHeader.Status = '02'
							THEN 'A'
						WHEN P$TaxSplitHeader.Status = '03'
							THEN 'N'
						WHEN P$TaxSplitHeader.Status = '04'
							THEN 'P'
						WHEN P$TaxSplitHeader.Status = '05'
							THEN 'F'
					END		   
				ELSE ''
				END AS Residency
			,CASE 
				WHEN LEFT(P$POLICY.ProductCode, 5) = 'FRABC'
					THEN P$COVRBD.BldOwnerReconstruction
				WHEN P$POLICY.ProductCode = 'CCC01'
					THEN P$COVRBD.BldOwnerReconstruction
				WHEN LEFT(P$POLICY.ProductCode, 3) = 'OBH'
					THEN p$covobh.BldOwnerReconstruction
				WHEN LEFT(P$POLICY.ProductCode, 3) = 'CPX'
					THEN P$COVCPX.BldgAsNew
				ELSE ''
				END AS BldOwnerReconstruction
			,CASE 
				WHEN (LEFT(P$POLICY.ProductCode, 5) = 'FRABC')
					OR (LEFT(P$POLICY.ProductCode, 3) = 'CCC')
					THEN ISNULL(P$COVRBD.AnnBldgPremium, 0)
				WHEN P$POLICY.ProductCode = 'FBX-01'
					THEN ISNULL(P$covfbx.AnnBldgPremium, 0)
				WHEN LEFT(P$POLICY.ProductCode, 3) = 'OBH'
					THEN ISNULL(P$covobh.AnnBldgPremium, 0)
				WHEN LEFT(P$POLICY.ProductCode, 3) = 'CPX'
					THEN ISNULL(P$COVCPX.AnnBldgPremium, 0)
				WHEN P$POLICY.ProductCode = 'CCS01'
					THEN ISNULL(P$COVCCS.AnnBldgPremium, 0)
				WHEN P$POLICY.ProductCode = 'FSC01'
					THEN ISNULL(P$COVFSC.AnnBldgPremium, 0)
                WHEN P$POLICY.ProductCode = 'MRP01'  
				   	THEN ISNULL(P$TaxSplitHeader.AnnDomPremium, 0) 
				ELSE 0
				END AS BuildingsPremium
			,CASE 
				WHEN LEFT(P$POLICY.ProductCode, 5) = 'FRABC'
					THEN CASE 
							WHEN (P$COVRBD.Residency <> 'L')
								THEN CASE 
										WHEN (P$COVRBD.HouseType <> 'A')
											THEN CASE 
													WHEN P$COVRBD.BldOwnerMainSIorAREA = 'A'
														THEN P$COVRBD.BldOwnerMainArea * P$COVRBD.BldOwnerMainRebuilding
													ELSE P$COVRBD.BldOwnerMainSI
													END
										END
							END
				WHEN LEFT(P$POLICY.ProductCode, 3) = 'CCC'
					THEN CASE 
							WHEN (P$COVRBD.CCCResidency <> 'L')
								THEN CASE 
										WHEN (P$COVRBD.CCCHouseType <> 'A')
											THEN CASE 
													WHEN P$COVRBD.BldOwnerMainSIorAREA = 'A'
														THEN P$COVRBD.BldOwnerMainArea * P$COVRBD.BldOwnerMainRebuilding
													ELSE P$COVRBD.BldOwnerMainSI
													END
										END
							END
				WHEN P$POLICY.ProductCode = 'FBX-01'
					THEN P$covfbx.BldSI
				WHEN LEFT(P$POLICY.ProductCode, 3) = 'OBH'
					THEN CASE 
							WHEN (P$COVOBH.Residency <> 'L')
								AND (P$COVOBH.Residency <> 'A')
								THEN CASE 
										WHEN P$COVOBH.BldOwnerMainSIorAREA = 'A'
											THEN P$COVOBH.BldOwnerMainArea * P$COVOBH.BldOwnerMainRebuilding
										ELSE P$COVOBH.BldOwnerMainSI
										END
							END
				WHEN LEFT(P$POLICY.ProductCode, 3) = 'CPX'
					THEN CASE 
							WHEN (ISNULL(P$COVCPX.BldgLCIPercent, 0) = 100)
								AND (ISNULL(P$COVCPX.BldgCover, 'N') = 'Y')
								THEN P$COVCPX.BuildingsSITotal
							WHEN (ISNULL(P$COVCPX.BldgLCIPercent, 0) <> 100)
								AND (ISNULL(P$COVCPX.BldgCover, 'N') = 'Y')
								THEN P$COVCPX.BldgLCIAmount
							ELSE 0
							END
				WHEN P$POLICY.ProductCode = 'CCS01'
					THEN ISNULL(P$COVCCS.BldSI, 0) + ISNULL(P$COVCCS.BuildSIOut, 0)
				WHEN P$POLICY.ProductCode = 'FSC01'
					THEN ISNULL(P$COVFSC.BuildingsSITotal, 0)
				WHEN P$POLICY.ProductCode = 'MRP01' 
				    THEN ISNULL(P$TaxSplitHeader.BuildingSI, 0)
				END AS BuildingSI
			,CASE 
				WHEN (LEFT(P$POLICY.ProductCode, 5) = 'FRABC')
					OR (LEFT(P$POLICY.ProductCode, 3) = 'CCC')
					THEN ISNULL(P$COVRBD.AnnBldgPremium, 0)
				WHEN LEFT(P$POLICY.ProductCode, 3) = 'OBH'
					THEN ISNULL(P$COVOBH.AnnBldgPremium, 0)
				WHEN LEFT(P$POLICY.ProductCode, 3) = 'CPX'
					THEN ISNULL(P$COVCPX.AnnBldgPremium, 0)
				WHEN P$POLICY.ProductCode = 'FSC01'
					THEN ISNULL(P$COVFSC.AnnBldgPremium, 0)
				WHEN P$POLICY.ProductCode = 'MRP01'
					THEN ISNULL(P$TaxSplitHeader.AnnDomPremium, 0)		
				ELSE 0
				END AS BuildingsAreaPremium
			,CASE 
				WHEN LEFT(P$POLICY.ProductCode, 5) = 'FRABC'
					THEN CASE 
							WHEN (P$COVRBD.Residency <> 'L')
								THEN CASE 
										WHEN (P$COVRBD.HouseType <> 'A')
											THEN CASE 
													WHEN P$COVRBD.BldOwnerMainSIorAREA = 'A'
														THEN P$COVRBD.BldOwnerMainArea
													ELSE P$COVRBD.BldCoOwnerPropertyArea
													END
										END
							END
				WHEN LEFT(P$POLICY.ProductCode, 3) = 'CCC'
					THEN CASE 
							WHEN (P$COVRBD.CCCResidency <> 'L')
								THEN CASE 
										WHEN (P$COVRBD.CCCHouseType <> 'A')
											THEN CASE 
													WHEN P$COVRBD.BldOwnerMainSIorAREA = 'A'
														THEN P$COVRBD.BldOwnerMainArea
													ELSE P$COVRBD.BldCoOwnerPropertyArea
													END
										END
							END
				WHEN LEFT(P$POLICY.ProductCode, 3) = 'OBH'
					THEN CASE 
							WHEN (P$COVOBH.Residency <> 'L')
								AND (P$COVOBH.Residency <> 'A')
								THEN CASE 
										WHEN P$COVOBH.BldOwnerMainSIorAREA = 'A'
											THEN ISNULL(P$COVOBH.BldOwnerMainArea, 0)
										END
							END
				WHEN LEFT(P$POLICY.ProductCode, 3) = 'CPX'
					THEN CASE 
							WHEN (P$COVCPX.OwnerType <> 'L')
								THEN CASE 
										WHEN (P$COVCPX.BuildingType <> 'A')
											THEN ISNULL(P$COVCPX.BldgMainArea + P$COVCPX.BldgHabitableArea + P$COVCPX.BldgNonHabitableArea, 0)
										END
							END
				WHEN P$POLICY.ProductCode = 'FSC01'
					THEN ISNULL(P$COVFSC.BldOwnerMainArea, 0) + ISNULL(P$COVFSC.BldgSubGar1Area, 0) + ISNULL(P$COVFSC.BldgSubGar2Area, 0) + ISNULL(P$COVFSC.BldgSubGar3Area, 0) + ISNULL(P$COVFSC.BldgSubGar4Area, 0) + ISNULL(P$COVFSC.BldgSubGar5Area, 0)
                WHEN P$POLICY.ProductCode = 'MRP01' 
				    THEN ISNULL(P$TaxSplitHeader.Superficie,0)
				END AS BuildingsArea
			,CONVERT(FLOAT, 0) AS BldAverageCalculation
			,CONVERT(FLOAT, 0) AS BldAreaCalc
			,CASE 
				WHEN (LEFT(P$POLICY.ProductCode, 5) = 'FRABC')
					OR (LEFT(P$POLICY.ProductCode, 3) = 'CCC')
					THEN ISNULL(P$COVRBD.AnnContPremium, 0)
				WHEN P$POLICY.ProductCode = 'FBX-01'
					THEN ISNULL(P$covfbx.AnnContPremium, 0)
				WHEN LEFT(P$POLICY.ProductCode, 3) = 'OBH'
					THEN ISNULL(P$covobh.AnnContPremium, 0)
				WHEN LEFT(P$POLICY.ProductCode, 3) = 'CPX'
					THEN ISNULL(P$COVCPX.AnnContPremium, 0)
				WHEN P$POLICY.ProductCode = 'CCS01'
					THEN ISNULL(P$COVCCS.AnnContPremium, 0)
				WHEN P$POLICY.ProductCode = 'FSC01'
					THEN ISNULL(P$COVFSC.AnnContPremium, 0)
				WHEN P$POLICY.ProductCode = 'MRP01'
					THEN ISNULL(P$TaxSplitHeader.AnnContPremium, 0)	 
				ELSE 0
				END AS ContentsPremium
			,CASE 
				WHEN (LEFT(P$POLICY.ProductCode, 5) = 'FRABC')
					OR (LEFT(P$POLICY.ProductCode, 3) = 'CCC')
					THEN (ISNULL(P$COVRBD.ContContentsSI, 0) + ISNULL(P$COVRBD.ContOutsideSI, 0) + ISNULL(P$COVRBD.ContFreezerSI, 0)) + (
							SELECT TOP 1 ISNULL(sum(ItemSI), 0)
							FROM dbo.p$item i WITH (NOLOCK)
							WHERE i.ItemType IN ('CONT')
								AND i.policynumber = P$COVRBD.policynumber
								AND i.version = P$COVRBD.version
								AND addressnumber = P$COVRBD.addressnumber
							)
				WHEN P$POLICY.ProductCode = 'FBX-01'
					THEN ISNULL(P$covfbx.ContSI, 0)
				WHEN LEFT(P$POLICY.ProductCode, 3) = 'OBH'
					THEN ISNULL(P$covobh.TotalContSI, 0)
				WHEN LEFT(P$POLICY.ProductCode, 3) = 'CPX'
					THEN ISNULL(P$COVCPX.ContentsSITotal, 0)
				WHEN P$POLICY.ProductCode = 'CCS01'
					THEN ISNULL(P$COVCCS.ContSI, 0)
				WHEN P$POLICY.ProductCode = 'FSC01'
					THEN ISNULL(P$COVFSC.ContentsSITotal, 0)
                WHEN P$POLICY.ProductCode = 'MRP01' 
					THEN ISNULL(P$TaxSplitHeader.Contenu, 0)
				ELSE 0
				END AS ContentsSumInsured
			,CONVERT(FLOAT, 0) AS ContAverageCalc
			,CASE 
				WHEN (LEFT(P$POLICY.ProductCode, 5) = 'FRABC')
					OR (LEFT(P$POLICY.ProductCode, 3) = 'CCC')
					THEN ISNULL(P$COVRBD.AnnArtPremium, 0)
				WHEN P$POLICY.ProductCode = 'FBX-01'
					THEN ISNULL(P$covfbx.AnnArtPremium, 0)
				WHEN LEFT(P$POLICY.ProductCode, 3) = 'CPX'
					THEN ISNULL(P$COVCPX.AnnArtPremium, 0)
				ELSE 0
				END AS ArtPremium
			,CASE 
				WHEN (LEFT(P$POLICY.ProductCode, 5) = 'FRABC')
					OR (LEFT(P$POLICY.ProductCode, 3) = 'CCC')
					THEN ISNULL(P$COVRBD.ArtFragileSI, 0) + ISNULL(P$COVRBD.ArtNonFragileSI, 0) + ISNULL(P$COVRBD.ArtPreciousSI, 0) + (
							SELECT TOP 1 ISNULL(SUM(ItemSI), 0)
							FROM dbo.p$item i WITH (NOLOCK)
							WHERE i.ItemType IN ('ARTS')
								AND i.policynumber = P$COVRBD.policynumber
								AND i.version = P$COVRBD.version
								AND addressnumber = P$COVRBD.addressnumber
							)
				WHEN P$POLICY.ProductCode = 'FBX-01'
					THEN ISNULL(P$covfbx.ArtSI, 0)
				WHEN LEFT(P$POLICY.ProductCode, 3) = 'CPX'
					THEN ISNULL(P$COVCPX.ArtSITotal, 0)
				ELSE 0
				END AS ArtSI
			,CONVERT(FLOAT, 0) AS ArtAverage
			,CASE 
				WHEN (LEFT(P$POLICY.ProductCode, 5) = 'FRABC')
					OR (LEFT(P$POLICY.ProductCode, 3) = 'CCC')
					THEN ISNULL(P$COVRBD.AnnPPSPremium, 0)
				WHEN P$POLICY.ProductCode = 'FBX-01'
					THEN ISNULL(P$covfbx.AnnItemsPremium, 0)
				WHEN LEFT(P$POLICY.ProductCode, 3) = 'CPX'
					THEN ISNULL(P$COVCPX.AnnPPSPremium, 0)
				ELSE 0
				END AS PPSPremium
			,CASE 
				WHEN (LEFT(P$POLICY.ProductCode, 5) = 'FRABC')
					OR (LEFT(P$POLICY.ProductCode, 3) = 'CCC')
					THEN ISNULL(P$COVRBD.ItemsJewelleryHomeSI, 0) + ISNULL(P$COVRBD.ItemsJewelleryFranceSI, 0) + ISNULL(P$COVRBD.ItemsJewelleryWorldSI, 0) + ISNULL(P$COVRBD.ItemsJewelleryBankSI, 0) + ISNULL(P$COVRBD.ItemsPersonalFranceSI, 0) + ISNULL(P$COVRBD.ItemsPersonalWorldSI, 0) + ISNULL(P$COVRBD.ItemsFursHomeSI, 0) + ISNULL(P$COVRBD.ItemsFursFranceSI, 0) + ISNULL(P$COVRBD.ItemsFursWorldSI, 0)
				WHEN P$POLICY.ProductCode = 'FBX-01'
					THEN ISNULL(P$covfbx.JewellerySI, 0) + ISNULL(P$covfbx.PPSI, 0)
				WHEN LEFT(P$POLICY.ProductCode, 3) = 'CPX'
					THEN ISNULL(P$COVCPX.PPSSITotal, 0)
				ELSE 0
				END AS PPSSI
			,CONVERT(FLOAT, 0) AS AveragePPSCalc
			,CASE 
				WHEN LEFT(P$POLICY.ProductCode, 5) = 'FRABC'
					THEN CASE 
							WHEN (
									P$COVRBD.LiabOwnerRenter = 'Y'
									AND P$COVRBD.Residency <> 'L'
									)
								THEN CASE 
										WHEN (P$COVRBD.LiabOwnerRenterModif <> 0)
											THEN P$COVRBD.LiabOwnerRenterModif
										ELSE P$COVRBD.LiabOwnerRate
										END
							END
				WHEN LEFT(P$POLICY.ProductCode, 3) = 'CCC'
					THEN CASE 
							WHEN (
									P$COVRBD.LiabOwnerRenter = 'Y'
									AND P$COVRBD.CCCResidency <> 'L'
									)
								THEN CASE 
										WHEN (P$COVRBD.LiabOwnerRenterModif <> 0)
											THEN P$COVRBD.LiabOwnerRenterModif
										ELSE P$COVRBD.LiabOwnerRate
										END
							END
				WHEN LEFT(P$POLICY.ProductCode, 3) = 'OBH'
					THEN CASE 
							WHEN (P$COVOBH.LiabOwner = 'Y')
								AND (P$COVOBH.Residency <> 'L')
								AND (P$COVOBH.Residency <> 'A')
								THEN CASE 
										WHEN P$COVOBH.LiabOwnerModif <> 0
											THEN P$COVOBH.LiabOwnerModif
										ELSE P$COVOBH.LiabOwnerRate
										END
							END
				WHEN P$POLICY.ProductCode = 'FBX-01'
					THEN P$covfbx.AnnLiabPremium
						/*      WHEN P$POLICY.ProductCode = 'TRM01' THEN P$covtrm.AnnLiabPremium*/
				WHEN LEFT(P$POLICY.ProductCode, 3) = 'CPX'
					THEN CASE 
							WHEN (
									P$COVCPX.LiabOwnerRenter = 'Y'
									AND P$COVCPX.OwnerType <> 'L'
									)
								THEN CASE 
										WHEN (P$COVCPX.LiabOwnerRenterModif <> 0)
											THEN P$COVCPX.LiabOwnerRenterModif
										ELSE P$COVCPX.LiabOwnerRate
										END
							END
				WHEN LEFT(P$POLICY.ProductCode, 3) = 'FSC'
					THEN CASE 
							WHEN (P$COVFSC.LiabOwner = 'Y')
								AND (P$COVFSC.Residency <> 'L')
								AND (P$COVFSC.Residency <> 'A')
								THEN CASE 
										WHEN P$COVFSC.LiabOwnerModif <> 0
											THEN P$COVFSC.LiabOwnerModif
										ELSE P$COVFSC.LiabOwnerRate
										END
							END
				WHEN P$POLICY.ProductCode = 'MRP01' 
					THEN CASE 
							WHEN (P$COVMRP.LiabOwner = 'Y')
								THEN CASE 
										WHEN ISNULL(P$COVMRP.LiabOwnerModif,0) <> 0
											THEN P$COVMRP.LiabOwnerModif
										ELSE P$taxsplitheader.RCOPCoeff
							END
							END
				ELSE 0
				END AS LiabilitySIPremium
			,CASE 
				WHEN LEFT(P$POLICY.ProductCode, 5) = 'FRABC'
					THEN CASE 
							WHEN P$COVRBD.LiabOwnerRenter = 'Y'
								AND P$COVRBD.Residency <> 'L'
								THEN CASE 
										WHEN (P$COVRBD.LiabOwnerRenterModif <> 0)
											THEN P$COVRBD.LiabOwnerRenterModif
										ELSE P$COVRBD.LiabOwnerRate
										END
							END
				WHEN LEFT(P$POLICY.ProductCode, 3) = 'CCC'
					THEN CASE 
							WHEN P$COVRBD.LiabOwnerRenter = 'Y'
								AND P$COVRBD.CCCResidency <> 'L'
								THEN CASE 
										WHEN (P$COVRBD.LiabOwnerRenterModif <> 0)
											THEN P$COVRBD.LiabOwnerRenterModif
										ELSE P$COVRBD.LiabOwnerRate
										END
							END
				WHEN LEFT(P$POLICY.ProductCode, 3) = 'OBH'
					THEN CASE 
							WHEN (P$COVOBH.Residency <> 'L')
								AND (P$COVOBH.Residency <> 'A')
								THEN CASE 
										WHEN P$COVOBH.LiabOwnerModif <> 0
											THEN P$COVOBH.LiabOwnerModif
										ELSE P$COVOBH.LiabOwnerRate
										END
							END
				WHEN LEFT(P$POLICY.ProductCode, 3) = 'CPX'
					THEN CASE 
							WHEN P$COVCPX.LiabOwnerRenter = 'Y'
								AND P$COVCPX.OwnerType <> 'L'
								THEN CASE 
										WHEN (P$COVCPX.LiabOwnerRenterModif <> 0)
											THEN P$COVCPX.LiabOwnerRenterModif
										ELSE P$COVCPX.LiabOwnerRate
										END
							END
				WHEN P$POLICY.ProductCode = 'MRP01'
				    THEN CASE
					     WHEN (P$COVMRP.LiabOwner = 'Y')
								THEN CASE 
										WHEN P$COVMRP.LiabOwnerModif <> 0
											THEN P$COVMRP.LiabOwnerModif
										ELSE P$TaxSplitHeader.AnnRCOPPremium 
									END
							END
				ELSE 0
				END AS LiabilityAreaPremium
			,CONVERT(FLOAT, 0) AS LiabilityAverageSIRate
			,CONVERT(FLOAT, 0) AS LiabilityAverageAreaRate
			,CASE 
				WHEN LEFT(P$POLICY.ProductCode, 5) = 'FRABC'
					THEN CASE 
							WHEN P$COVRBD.LiabOwnerRenter = 'Y'
								AND P$COVRBD.Residency = 'L'
								THEN CASE 
										WHEN P$COVRBD.LiabOwnerRenterModif <> 0
											THEN P$COVRBD.LiabOwnerRenterModif
										ELSE P$COVRBD.LiabRenterRate
										END
							END
				WHEN LEFT(P$POLICY.ProductCode, 3) = 'OBH'
					THEN CASE 
							WHEN (P$COVOBH.LiabRenter = 'Y')
								AND (
									(P$COVOBH.Residency = 'L')
									OR (P$COVOBH.Residency = 'A')
									)
								THEN CASE 
										WHEN P$COVOBH.LiabRenterModif <> 0
											THEN P$COVOBH.LiabRenterModif
										ELSE P$COVOBH.LiabRenterRate
										END
							END
				WHEN LEFT(P$POLICY.ProductCode, 3) = 'CPX'
					THEN CASE 
							WHEN P$COVCPX.LiabOwnerRenter = 'Y'
								AND P$COVCPX.OwnerType = 'L'
								THEN CASE 
										WHEN P$COVCPX.LiabOwnerRenterModif <> 0
											THEN P$COVCPX.LiabOwnerRenterModif
										ELSE P$COVCPX.LiabRenterRate
										END
							END
				WHEN LEFT(P$POLICY.ProductCode, 3) = 'FSC'
					THEN CASE 
							WHEN (P$COVFSC.LiabRenter = 'Y')
								AND (
									(P$COVFSC.Residency = 'L')
									OR (P$COVFSC.Residency = 'A')
									)
								THEN CASE 
										WHEN P$COVFSC.LiabRenterModif <> 0
											THEN P$COVOBH.LiabRenterModif
										ELSE P$COVFSC.LiabRenterRate
										END
							END
				WHEN P$POLICY.ProductCode = 'MRP01'
					THEN CASE 
							WHEN (P$COVMRP.LiabRenter = 'Y')
								THEN CASE 
										WHEN P$COVMRp.LiabRenterModif <> 0
											THEN P$COVMRP.LiabRenterModif
										ELSE P$TaxSplitHeader.AnnRCOLPremium
										END
							END
				ELSE 0
				END AS LiabilityRenterPremium
			,CASE 
				WHEN LEFT(P$POLICY.ProductCode, 5) = 'FRABC'
					THEN CASE 
							WHEN P$COVRBD.Residency = 'L'
								THEN P$COVRBD.BldCoOwnerPropertyArea
							END
				WHEN LEFT(P$POLICY.ProductCode, 3) = 'CCC'
					THEN CASE 
							WHEN P$COVRBD.CCCResidency = 'L'
								THEN P$COVRBD.BldCoOwnerPropertyArea
							END
				WHEN LEFT(P$POLICY.ProductCode, 3) = 'OBH'
					THEN CASE 
							WHEN (P$COVOBH.Residency = 'L')
								OR (P$COVOBH.Residency = 'A')
								THEN P$COVOBH.BldOwnerMainArea
							END
				WHEN LEFT(P$POLICY.ProductCode, 3) = 'CPX'
					THEN CASE 
							WHEN P$COVCPX.OwnerType = 'L'
								THEN P$COVCPX.BldCoOwnerPropertyArea
							END
				ELSE 0
				END AS LiabilityRenterArea              

			,CONVERT(FLOAT, 0) AS LiabilityRenterAverageRate
			,CASE 
				WHEN (LEFT(P$POLICY.ProductCode, 5) = 'FRABC')
					OR (LEFT(P$POLICY.ProductCode, 3) = 'CCC')
					THEN CASE 
							WHEN P$COVRBD.LiabPrivateLife = 'Y'
								THEN CASE 
										WHEN P$COVRBD.LiabPrivateLifeModif <> 0
											THEN P$COVRBD.LiabPrivateLifeModif
										ELSE P$COVRBD.LiabPrivateLifeRate
										END
							ELSE 0
							END
				WHEN LEFT(P$POLICY.ProductCode, 3) = 'CPX'
					THEN CASE 
							WHEN P$COVCPX.LiabPrivateLife = 'Y'
								THEN CASE 
										WHEN P$COVCPX.LiabPrivateLifeModif <> 0
											THEN P$COVCPX.LiabPrivateLifeModif
										ELSE P$COVCPX.LiabPrivateLifeRate
										END
							ELSE 0
							END
				ELSE 0
				END AS LiabilityPriveePremium
			,CASE 
				WHEN (LEFT(P$POLICY.ProductCode, 5) = 'FRABC')
					OR (LEFT(P$POLICY.ProductCode, 3) = 'CCC')
					THEN P$COVRBD.LiabReduction
				ELSE 0
				END AS LiabReduction
			,CASE 
				WHEN (LEFT(P$POLICY.ProductCode, 5) = 'FRABC')
					OR (LEFT(P$POLICY.ProductCode, 3) = 'CCC')
					THEN P$COVRBD.AnnJudlPremium
				WHEN P$POLICY.ProductCode = 'FBX-01'
					THEN P$covfbx.AnnJudlPremium
				WHEN LEFT(P$POLICY.ProductCode, 3) = 'OBH'
					THEN p$covobh.AnnJudlPremium
				WHEN LEFT(P$POLICY.ProductCode, 3) = 'CPX'
					THEN P$COVCPX.AnnJudlPremium
				WHEN P$POLICY.ProductCode = 'CCS01'
					THEN P$COVCCS.AnnJudlPremium
				WHEN P$POLICY.ProductCode = 'FSC01'
					THEN P$COVFSC.AnnJudlPremium
				WHEN P$POLICY.ProductCode = 'MRP01'  
				    THEN P$COVMRP.AnnJudlPremium
				ELSE 0
				END AS LegalAidPremium
			,CASE 
				WHEN P$POLICY.ProductCode = 'FBX-01'
					THEN P$covfbx.AnnADPremium
				WHEN P$POLICY.ProductCode = 'EVE-01'
					THEN P$coveve.AnnIAPremium
				WHEN P$POLICY.ProductCode = 'CCS01'
					THEN P$covccs.AnnADPremium
				ELSE 0
				END AS PAPremium
			,CASE 
				WHEN P$POLICY.ProductCode = 'FBX-01'
					THEN P$covfbx.ADSI
				WHEN P$POLICY.ProductCode = 'EVE-01'
					THEN P$coveve.IASI
				WHEN P$POLICY.ProductCode = 'CCS01'
					THEN P$covccs.ADSI
				ELSE 0
				END AS PASI
			,CASE 
				WHEN P$POLICY.ProductCode = 'FBX-01'
					THEN CASE 
							WHEN (P$covfbx.AnnADPremium <> 0)
								AND (P$covfbx.ADSI <> 0)
								THEN P$covfbx.AnnADPremium / (P$covfbx.ADSI / 100)
							ELSE 0
							END
				WHEN P$POLICY.ProductCode = 'EVE-01'
					THEN CASE 
							WHEN (P$coveve.AnnIAPremium <> 0)
								AND (P$coveve.IASI <> 0)
								THEN P$coveve.AnnIAPremium / (P$coveve.IASI / 100)
							ELSE 0
							END
				WHEN P$POLICY.ProductCode = 'CCS01'
					THEN CASE 
							WHEN (P$covccs.AnnADPremium <> 0)
								AND (P$covccs.ADSI <> 0)
								THEN P$covccs.AnnADPremium / (P$covccs.ADSI / 100)
							ELSE 0
							END
				ELSE 0
				END AS ADAverageRate
			,CASE 
				WHEN P$POLICY.ProductCode = 'FBX-01'
					THEN p$covfbx.AnnBusIntPremium
				WHEN LEFT(P$POLICY.ProductCode, 3) = 'OBH'
					THEN p$covobh.AnnFinPremium
				WHEN LEFT(P$POLICY.ProductCode, 3) = 'FSC'
					THEN p$COVFSC.AnnFinPremium
				ELSE 0
				END AS AnnBusIntPremium
			,CASE 
				WHEN P$POLICY.ProductCode = 'FBX-01'
					THEN p$covfbx.BusIntSI
				WHEN LEFT(P$POLICY.ProductCode, 3) = 'OBH'
					THEN p$covobh.FinExpensesSI
				WHEN LEFT(P$POLICY.ProductCode, 3) = 'FSC'
					THEN ISNULL(p$COVFSC.FinLossSI, 0) + ISNULL(p$COVFSC.FinRevLSI, 0) + ISNULL(p$COVFSC.FinDenASI, 0) + ISNULL(p$COVFSC.FinSupESI, 0) + ISNULL(p$COVFSC.FinFeeSupp1, 0) + ISNULL(p$COVFSC.FinFeeSupp2, 0) + ISNULL(p$COVFSC.FinFeeSupp3, 0)
				ELSE 0
				END AS BusIntSI
			,CASE 
				WHEN P$POLICY.ProductCode = 'FBX-01'
					THEN CASE 
							WHEN P$covfbx.AnnBusIntPremium <> 0
								AND P$covfbx.BusIntSI <> 0
								THEN P$covfbx.AnnBusIntPremium / (P$covfbx.BusIntSI / 100)
							ELSE 0
							END
				WHEN LEFT(P$POLICY.ProductCode, 3) = 'OBH'
					THEN CASE 
							WHEN p$covobh.AnnFinPremium <> 0
								AND p$covobh.FinExpensesSI <> 0
								THEN p$covobh.AnnFinPremium / (p$covobh.FinExpensesSI / 100)
							ELSE 0
							END
				WHEN LEFT(P$POLICY.ProductCode, 3) = 'FSC'
					THEN CASE 
							WHEN (p$covfsc.AnnFinPremium <> 0)
								AND ((ISNULL(p$COVFSC.FinLossSI, 0) + ISNULL(p$COVFSC.FinRevLSI, 0) + ISNULL(p$COVFSC.FinDenASI, 0) + ISNULL(p$COVFSC.FinSupESI, 0) + ISNULL(p$COVFSC.FinFeeSupp1, 0) + ISNULL(p$COVFSC.FinFeeSupp2, 0) + ISNULL(p$COVFSC.FinFeeSupp3, 0)) <> 0)
								THEN p$covfsc.AnnFinPremium / ((ISNULL(p$COVFSC.FinLossSI, 0) + ISNULL(p$COVFSC.FinRevLSI, 0) + ISNULL(p$COVFSC.FinDenASI, 0) + ISNULL(p$COVFSC.FinSupESI, 0) + ISNULL(p$COVFSC.FinFeeSupp1, 0) + ISNULL(p$COVFSC.FinFeeSupp2, 0) + ISNULL(p$COVFSC.FinFeeSupp3, 0)) / 100)
							ELSE 0
							END
				ELSE 0
				END AS BIAverageRate
			,CASE 
				WHEN P$POLICY.ProductCode = 'RSP01'
					THEN P$covrsp.AnnMaterialPremium
				WHEN P$POLICY.ProductCode = 'EVE-01'
					THEN P$coveve.AnnMaterialPremium
				WHEN P$POLICY.ProductCode = 'TRM01'
					THEN ISNULL(P$covtrm.AnnMaterialPremium, 0) - ISNULL(P$covtrm.AnnExhibitorPremium, 0)
				WHEN P$POLICY.ProductCode = 'FSC01'
					THEN ISNULL(P$covfsc.AnnMechPremium, 0)
				ELSE 0
				END AS RSPMaterialPremium
			,CASE 
				WHEN P$POLICY.ProductCode = 'RSP01'
					THEN
						/*ISNULL(P$covrsp.MaterialTransportSI,0) + */
						ISNULL(P$covrsp.MaterialSI1, 0) + ISNULL(P$covrsp.MaterialSI2, 0) + ISNULL(P$covrsp.MaterialSI3, 0) + ISNULL(P$covrsp.MaterialSI4, 0)
				WHEN P$POLICY.ProductCode = 'EVE-01'
					THEN ISNULL(P$coveve.MaterialStandardSI, 0) + ISNULL(P$coveve.MaterialSensitiveSI, 0) + ISNULL(P$coveve.MaterialTentsSI, 0) + ISNULL(P$coveve.MaterialValuablesSI, 0) + ISNULL(P$coveve.MaterialTransportSI, 0) + ISNULL(P$coveve.MaterialFree1SI, 0) + ISNULL(P$coveve.MaterialFree2SI, 0)
				WHEN P$POLICY.ProductCode = 'TRM01'
					THEN ISNULL(P$covtrm.MobileMaterialSITotal, 0) + ISNULL(P$Covtrm.StaticMaterialSITotalNoExh, 0)
				WHEN P$POLICY.ProductCode = 'FSC01'
					THEN ISNULL(P$COVFSC2.MechBrisMaterialSI, 0) + ISNULL(P$COVFSC2.MechAddresseSI, 0) + ISNULL(P$COVFSC2.MechPortableSI, 0) + ISNULL(P$COVFSC2.MechBrisSuiteSI, 0) + ISNULL(P$COVFSC2.MechFraisSubstSI, 0) + ISNULL(P$COVFSC2.MechFraisUrgnSI, 0)
				ELSE 0
				END AS RSPMaterialSI
			,CONVERT(FLOAT, 0) AS RSPMaterialAverageCalc
			,CASE 
				WHEN P$POLICY.ProductCode = 'RSP01'
					THEN ISNULL(P$covrsp.AnnMusicPremium, 0)
				ELSE 0
				END AS RSPMusicPremium
			,CASE 
				WHEN P$POLICY.ProductCode = 'RSP01'
					THEN
						/*ISNULL(P$covrsp.MusicTransportSI,0) + */
						ISNULL(P$covrsp.MusicSI1, 0) + ISNULL(P$covrsp.MusicSI2, 0) + ISNULL(P$covrsp.MusicSI3, 0) + ISNULL(P$covrsp.MusicSI4, 0)
				ELSE 0
				END AS RSPMusicSI
			,CONVERT(FLOAT, 0) AS RSPMusicAverageCalc
			,CASE 
				WHEN P$POLICY.ProductCode = 'RSP01'
					THEN ISNULL(P$covrsp.AnnExhibitPremium, 0)
				WHEN P$POLICY.ProductCode = 'TRM01'
					THEN ISNULL(p$covtrm.AnnExhibitorPremium, 0)
				ELSE 0
				END AS RSPExhibitPremium
			,CASE 
				WHEN P$POLICY.ProductCode = 'RSP01'
					THEN ISNULL(P$covrsp.ExhibitOrgSI, 0) + ISNULL(P$covrsp.ExhibitBreakSI, 0) + ISNULL(P$covrsp.ExhibitSpeciesSI, 0) + ISNULL(P$covrsp.ExhibitSI1, 0) + ISNULL(P$covrsp.ExhibitSI2, 0) + ISNULL(P$covrsp.ExhibitSI3, 0) + ISNULL(P$covrsp.ExhibitSI4, 0)
				WHEN P$POLICY.ProductCode = 'TRM01'
					THEN ISNULL(p$covtrm.ExhibitorSITotal, 0)
				ELSE 0
				END AS RSPExhibitSI
			,CONVERT(FLOAT, 0) AS RSPExhibitAverageCalc
			,CASE 
				WHEN P$POLICY.ProductCode = 'RSP01'
					OR P$POLICY.ProductCode = 'RSP02'
					THEN ISNULL(P$covrsp.AnnArtPremium, 0)
				ELSE 0
				END AS RSPArtPremium
			,CASE 
				WHEN P$POLICY.ProductCode = 'RSP01'
					THEN
						/*ISNULL(P$covrsp.ArtTransportSI,0) + */
						ISNULL(P$covrsp.ArtBreakSI, 0) + ISNULL(P$covrsp.ArtSI1, 0) + ISNULL(P$covrsp.ArtSI2, 0) + ISNULL(P$covrsp.ArtSI3, 0) + ISNULL(P$covrsp.ArtSI4, 0)
				WHEN P$POLICY.ProductCode = 'RSP02'
					THEN ISNULL(P$covrsp2.FFAArtNonFragileSI, 0) + ISNULL(P$covrsp2.FFAArtFragileSI, 0) + ISNULL(P$covrsp2.FFAArtValuableSI, 0) + ISNULL(P$covrsp2.FFAArtUnspecNonFragileSI, 0) + ISNULL(P$covrsp2.FFAArtUnspecFragileSI, 0) + ISNULL(P$covrsp2.FFAArtUnspecValuableSI, 0) + ISNULL(P$covrsp.ArtSI1, 0) + ISNULL(P$covrsp.ArtSI2, 0) + ISNULL(P$covrsp.ArtSI3, 0) + ISNULL(P$covrsp.ArtSI4, 0)
				ELSE 0
				END AS RSPArtSI
			,CONVERT(FLOAT, 0) AS RSPArtAverageCalc
			,CASE 
				WHEN P$POLICY.ProductCode = 'RSP01'
					THEN ISNULL(P$covrsp.AnnGroupPremium, 0)
				ELSE 0
				END AS RSPGroupPremium
			,CASE 
				WHEN P$POLICY.ProductCode = 'RSP01'
					THEN
						/*ISNULL(P$covrsp.GroupTransportSI,0) + */
						ISNULL(P$covrsp.GroupSI1, 0) + ISNULL(P$covrsp.GroupSI2, 0) + ISNULL(P$covrsp.GroupSI3, 0) + ISNULL(P$covrsp.GroupSI4, 0)
				ELSE 0
				END AS RSPGroupSI
			,CONVERT(FLOAT, 0) AS RSPGroupAverageCalc
			,CASE 
				WHEN P$POLICY.ProductCode = 'RDO-01'
					THEN ISNULL(P$COVRDO.RiskSI1, 0)
				WHEN P$POLICY.ProductCode = 'RCP-01'
					THEN ISNULL(P$COVRCP.HighestSI, 0)
				WHEN P$POLICY.ProductCode = 'CDH01'
					THEN ISNULL(P$COVCDH.RiskSI1, 0)
				WHEN P$POLICY.ProductCode = 'FSC01'
					THEN (ISNULL(P$COVFSC2.RiskSI1, 0) + ISNULL(P$COVFSC2.RiskSI2, 0) + ISNULL(P$COVFSC2.RiskSI3, 0) + ISNULL(P$COVFSC2.RiskSI4, 0) + ISNULL(P$COVFSC2.RiskSI5, 0) + ISNULL(P$COVFSC2.RiskSI6, 0) + ISNULL(P$COVFSC2.RiskSI7, 0) + ISNULL(P$COVFSC2.RiskSI8, 0) 
					+ ISNULL(P$COVFSC2.RiskSI9, 0) + ISNULL(P$COVFSC2.RiskSI10, 0) + ISNULL(P$COVFSC2.RiskSI11, 0) + ISNULL(P$COVFSC2.RiskSI2, 0))
				ELSE 0
				END AS PILimit
			,CASE 
				WHEN P$POLICY.ProductCode = 'RDO-01'
					THEN P$COVRDO.AnnCommercialPremium
				WHEN P$POLICY.ProductCode = 'RCP-01'
					THEN P$COVRCP.AnnCommercialPremium
				WHEN P$POLICY.ProductCode = 'CDH01'
					THEN P$COVCDH.AnnCommercialPremium
				WHEN P$POLICY.ProductCode = 'FSC01'
					THEN P$COVFSC.AnnCommercialPremium
				ELSE 0
				END AS PIPremium
			,CASE 
				WHEN P$POLICY.ProductCode = 'RCP-01'
					THEN P$COVRCP.TotalTurnover
				WHEN P$POLICY.ProductCode = 'CDH01'
					THEN P$COVCDH.TotalTurnover
				WHEN P$POLICY.ProductCode = 'RDO-01'
					THEN P$COVRDO.TotalTurnover
				WHEN P$POLICY.ProductCode = 'FSC01'
					THEN P$COVFSC.Turnover
				WHEN P$POLICY.ProductCode = 'MRP01' 
					THEN P$COVMRP.Turnover
				ELSE 0
				END AS PISI
			,CONVERT(FLOAT, 0) AS PIAverageCalc
			,CASE 
				WHEN LEFT(P$POLICY.ProductCode, 5) = 'FRABC'
					AND P$COVRBD.Residency <> 'L'
					AND P$COVRBD.HouseType <> 'A'
					THEN ISNULL(P$COVRBD.BuildingsSITotal, 0) + ISNULL(P$COVRBD.ContentsSITotal, 0) + ISNULL(P$COVRBD.ArtSITotal, 0) + ISNULL(P$COVRBD.PPSSITotal, 0)
				WHEN LEFT(P$POLICY.ProductCode, 5) = 'FRABC'
					AND (
						P$COVRBD.Residency = 'L'
						OR P$COVRBD.HouseType = 'A'
						)
					THEN ISNULL(P$COVRBD.ContentsSITotal, 0) + ISNULL(P$COVRBD.ArtSITotal, 0) + ISNULL(P$COVRBD.PPSSITotal, 0)
				WHEN P$POLICY.ProductCode = 'CCC01'
					AND P$COVRBD.CCCResidency <> 'L'
					AND P$COVRBD.CCCHouseType <> 'A'
					THEN CASE 
							WHEN P$COVRBD.BldOwnerMainSIorAREA = 'A'
								THEN (ISNULL(P$COVRBD.BldOwnerMainArea, 0) * ISNULL(P$COVRBD.BldOwnerMainRebuilding, 0)) + ISNULL(P$COVRBD.ContentsSITotal, 0) + ISNULL(P$COVRBD.ArtSITotal, 0) + ISNULL(P$COVRBD.PPSSITotal, 0)
							ELSE ISNULL(P$COVRBD.BuildingsSITotal, 0) + ISNULL(P$COVRBD.ContentsSITotal, 0) + ISNULL(P$COVRBD.ArtSITotal, 0) + ISNULL(P$COVRBD.PPSSITotal, 0)
							END
				WHEN P$POLICY.ProductCode = 'CCC01'
					AND (
						P$COVRBD.CCCResidency = 'L'
						OR P$COVRBD.CCCHouseType = 'A'
						)
					THEN ISNULL(P$COVRBD.ContentsSITotal, 0) + ISNULL(P$COVRBD.ArtSITotal, 0) + ISNULL(P$COVRBD.PPSSITotal, 0)
				WHEN LEFT(P$POLICY.ProductCode, 3) = 'OBH'
					AND P$COVOBH.Residency <> 'L'
					AND P$COVOBH.Residency <> 'A'
					THEN ISNULL(P$COVOBH.BuildingsSITotal, 0) + ISNULL(P$COVOBH.TotalContSI, 0) + ISNULL(P$COVOBH.TotalFinSI, 0) + ISNULL(P$COVOBH.RCESITotal, 0)
				WHEN LEFT(P$POLICY.ProductCode, 3) = 'OBH'
					AND (
						P$COVOBH.Residency = 'L'
						OR P$COVOBH.Residency = 'A'
						)
					THEN ISNULL(P$COVOBH.TotalContSI, 0) + ISNULL(P$COVOBH.TotalFinSI, 0) + ISNULL(P$COVOBH.RCESITotal, 0)
				WHEN LEFT(P$POLICY.ProductCode, 3) = 'CPX'
					THEN CASE 
							WHEN ISNULL(P$COVCPX.BldgCover, 'N') = 'Y'
								THEN CASE 
										WHEN ISNULL(P$COVCPX.BldgLCIPercent, 0) <> 100
											THEN ISNULL(P$COVCPX.BldgLCIAmount, 0)
										ELSE ISNULL(P$COVCPX.BuildingsSITotal, 0)
										END
							ELSE 0
							END + ISNULL(P$COVCPX.ContentsSITotal, 0) + ISNULL(P$COVCPX.ArtSITotal, 0) + ISNULL(P$COVCPX.PPSSITotal, 0)
				WHEN P$POLICY.ProductCode = 'CCS01'
					THEN ISNULL(P$COVCCS.BldSI, 0) + ISNULL(P$COVCCS.BuildSIOut, 0) + ISNULL(P$COVCCS.ContSI, 0) + ISNULL(P$COVCCS.ADSI, 0)
				WHEN P$POLICY.ProductCode = 'EVE-01'
					THEN ISNULL(P$COVEVE.CancelSITotal, 0) + ISNULL(P$COVEVE.MaterialSITotal, 0) + ISNULL(P$COVEVE.LiabSITotal, 0) + ISNULL(P$COVEVE.ExhibitorSITotal, 0) + ISNULL(P$COVEVE.ParticSITotal, 0)
				WHEN P$POLICY.ProductCode = 'RCP-01'
					THEN ISNULL(P$COVRCP.HighestSI, 0)
				WHEN P$POLICY.ProductCode = 'CDH01'
					THEN ISNULL(P$COVCDH.RiskSI1, 0)
				WHEN P$POLICY.ProductCode = 'RDO-01'
					THEN ISNULL(P$COVRDO.RiskSI1, 0)
				WHEN P$POLICY.ProductCode = 'FBX-01'
					THEN ISNULL(P$COVFBX.BldSI, 0) + ISNULL(P$COVFBX.ContSI, 0) + ISNULL(P$COVFBX.ArtSI, 0) + ISNULL(P$COVFBX.JewellerySI, 0) + ISNULL(P$COVFBX.PPSI, 0) + ISNULL(P$COVFBX.BusIntSI, 0) + ISNULL(P$COVFBX.ADSI, 0)
				WHEN P$POLICY.ProductCode = 'RSP01'
					THEN /*ISNULL(P$covrsp.MaterialTransportSI,0) + */
						ISNULL(P$covrsp.MaterialSITotal, 0) +
						/*ISNULL(P$covrsp.MusicTransportSI,0) + */
						ISNULL(P$covrsp.MusicSITotal, 0) + ISNULL(P$covrsp.ExhibitOrgSI, 0) + ISNULL(P$covrsp.ExhibitBreakSI, 0) + ISNULL(P$covrsp.ExhibitSITotal, 0) +
						/*ISNULL(P$covrsp.GroupTransportSI,0) +*/
						ISNULL(P$covrsp.GroupSITotal, 0) +
						/*ISNULL(P$covrsp.ArtTransportSI,0) +*/
						ISNULL(P$covrsp.ArtBreakSI, 0) + ISNULL(P$covrsp.ArtSITotal, 0)
				WHEN P$POLICY.ProductCode = 'RSP02'
					THEN ISNULL(P$covrsp.ArtSITotal, 0)
				WHEN P$POLICY.ProductCode = 'TRM01'
					THEN ISNULL(P$covtrm.MaterialSITotal, 0) + ISNULL(P$Covtrm.LiabOrganiserSI, 0) -- + ISNULL(P$covtrm.ExhibitorSITotal,0) --+ ISNULL(P$covtrm.LiabSITotal,0)
				WHEN LEFT(P$POLICY.ProductCode, 3) = 'FSC'
					THEN ISNULL(P$COVFSC.BuildingsSITotal, 0) + ISNULL(P$COVFSC.TotalContSI, 0) + ISNULL(P$COVFSC.TotalFinSI, 0)
			  	WHEN P$POLICY.ProductCode = 'MRP01'
				THEN CASE 
					WHEN (P$COVMRP.ResidenceFlag = 'Y')
						THEN ISNULL(P$COVMRP.BuildingSITotal, 0) + ISNULL(P$COVMRP.BureauContentsSI, 0) + ISNULL(P$COVMRP.PESI, 0)
							+ ISNULL(P$COVMRP.PerteFiSI, 0) 
						else
						 ISNULL(P$COVMRP.BureauContentsSI, 0) + ISNULL(P$COVMRP.PESI, 0)
						+ ISNULL(P$COVMRP.PerteFiSI, 0)
							
                END
				ELSE 0
				END AS TotalSI
			,' '
			,' '
			,--these are here to keep the columns in the same order after removing the (2) old survey fields
			CASE 
				WHEN LEFT(P$POLICY.ProductCode, 5) = 'FRABC'
					THEN P$COVRBD.BldReduction
				WHEN P$POLICY.ProductCode = 'CCC01'
					THEN P$COVRBD.BldReduction
				WHEN P$POLICY.ProductCode = 'CCS01'
					THEN P$COVCCS.BldFranchise
				WHEN LEFT(P$POLICY.ProductCode, 3) = 'OBH'
					THEN P$COVOBH.BldReduction
				WHEN LEFT(P$POLICY.ProductCode, 3) = 'CPX'
					THEN P$COVCPX.BldgExcess
				WHEN LEFT(P$POLICY.ProductCode, 3) = 'FSC'
					THEN P$COVFSC.BldReduction
				WHEN P$POLICY.ProductCode = 'MRP01'
					THEN P$COVMRP.BureauContentsReduction
				ELSE 0
				END AS BldReduction
			,CASE 
				WHEN LEFT(P$POLICY.ProductCode, 5) = 'FRABC'
					THEN P$COVRBD.ContReduction
				WHEN P$POLICY.ProductCode = 'CCC01'
					THEN P$COVRBD.ContReduction
				WHEN P$POLICY.ProductCode = 'CCS01'
					THEN P$COVCCS.ContFranchise
				WHEN LEFT(P$POLICY.ProductCode, 3) = 'OBH'
					THEN P$COVOBH.ContReduction
				WHEN LEFT(P$POLICY.ProductCode, 3) = 'CPX'
					THEN P$COVCPX.ContExcess
				WHEN LEFT(P$POLICY.ProductCode, 3) = 'FSC'
					THEN P$COVFSC.ContMobiReduction
				WHEN P$POLICY.ProductCode = 'MRP01'
					THEN P$COVMRP.BureauContentsReduction
				ELSE 0
				END AS ContReduction
			,CASE 
				WHEN (LEFT(P$POLICY.ProductCode, 5) = 'FRABC')
					OR (LEFT(P$POLICY.ProductCode, 3) = 'CCC')
					THEN P$COVRBD.ArtReduction
				WHEN LEFT(P$POLICY.ProductCode, 3) = 'CPX'
					THEN P$COVCPX.ArtExcess
				ELSE 0
				END AS ArtReduction
			,CASE 
				WHEN (LEFT(P$POLICY.ProductCode, 5) = 'FRABC')
					OR (LEFT(P$POLICY.ProductCode, 3) = 'CCC')
					THEN P$COVRBD.ItemsReduction
				WHEN LEFT(P$POLICY.ProductCode, 3) = 'CPX'
					THEN P$COVCPX.ValExcess
				ELSE 0
				END AS ItemsReduction
			,CASE 
				WHEN P$POLICY.ProductCode = 'RCP-01'
					THEN ISNULL(P$COVRCP.Excess, 0)
				WHEN P$POLICY.ProductCode = 'CDH01'
					THEN ISNULL(P$COVCDH.Excess, 0)
				WHEN P$POLICY.ProductCode = 'RDO-01'
					THEN ISNULL(P$COVRDO.Excess, 0)
				WHEN P$POLICY.ProductCode = 'FSC01'
					THEN ISNULL(P$COVFSC2.Excess, 0)
				ELSE 0
				END AS PIReduction
			,CASE 
				WHEN P$POLICY.ProductCode = 'RCP-01'
					THEN ISNULL(P$COVRCP.ExcessUSA, 0)
				WHEN P$POLICY.ProductCode = 'CDH01'
					THEN ISNULL(P$COVCDH.ExcessUSA, 0)
				WHEN P$POLICY.ProductCode = 'FSC01'
					THEN ISNULL(P$COVFSC2.ExcessUSA, 0)
				ELSE 0
				END AS USPIReduction
			,P$POLICY.Reconstruction 
			,CASE 
				WHEN P$POLICY.ProductCode = 'RCP-01'
					THEN ISNULL(P$COVRCP.CommercialAdj1, 0) + ISNULL(P$COVRCP.CommercialAdj2, 0) + ISNULL(P$COVRCP.CommercialAdj3, 0) + ISNULL(P$COVRCP.CommercialAdj4, 0) + ISNULL(P$COVRCP.CommercialAdj5, 0)
				WHEN P$POLICY.ProductCode = 'CDH01'
					THEN ISNULL(P$COVCDH.CommercialAdj1, 0) + ISNULL(P$COVCDH.CommercialAdj2, 0) + ISNULL(P$COVCDH.CommercialAdj3, 0) + ISNULL(P$COVCDH.CommercialAdj4, 0) + ISNULL(P$COVCDH.CommercialAdj5, 0)
				WHEN P$POLICY.ProductCode = 'CPX01'
					THEN ISNULL(P$COVCPX.GlobalDiscount, 0)
				ELSE 0
				END AS Discount
			,(
				SELECT SUM(ISNULL(AnnPremium, 0))
				FROM dbo.p$anal WITH (NOLOCK)
				WHERE PolicyNumber = P$COVER.Policynumber
					AND Version = P$COVER.Version
					AND AddressNumber = P$COVER.AddressNumber
				) AS AnnAddressPremium
			,(
				SELECT SUM(ISNULL(DuePremium, 0))
				FROM dbo.p$anal WITH (NOLOCK)
				WHERE PolicyNumber = P$COVER.Policynumber
					AND Version = P$COVER.Version
					AND AddressNumber = P$COVER.AddressNumber
				) AS DueAddressPremium
			,P$POLICY.UnderwriterIndicator
			,CASE 
				WHEN LEFT(P$POLICY.ProductCode, 5) = 'FRABC'
					THEN P$COVRBD.GeneralSectionNo
				WHEN P$POLICY.ProductCode = 'FBX-01'
					THEN P$covfbx.GeneralSectionNo
				WHEN P$POLICY.ProductCode = 'CCS01'
					THEN P$covccs.GeneralSectionNo
				WHEN P$POLICY.ProductCode = 'RCP-01'
					THEN P$COVRCP.GeneralSectionNo
				WHEN P$POLICY.ProductCode = 'CDH01'
					THEN P$COVCDH.GeneralSectionNo
				WHEN P$POLICY.ProductCode = 'RDO-01'
					THEN P$COVRDO.GeneralSectionNo
				WHEN LEFT(P$POLICY.ProductCode, 3) = 'OBH'
					THEN P$COVOBH.GeneralSectionNo
				WHEN LEFT(P$POLICY.ProductCode, 3) = 'CPX'
					THEN P$COVCPX.GeneralSectionNo
				WHEN LEFT(P$POLICY.ProductCode, 3) = 'MRP'
					THEN P$COVMRP.GeneralSectionNo
				ELSE 'N/A'
				END AS GenSectionNo
			,P$POLICY.AgentPolicyNumber
			,P$POLICY.AnnualPremium
			,P$POLICY.DuePremium
			,P$POLICY.AnnualPremium - P$POLICY.AnnualTax + ISNULL(P$POLICY.AnnCatNat, 0) - ISNULL(P$POLICY.AnnFees, 0) AS AnnualPremiumMinusTax
			,P$POLICY.DuePremium - P$POLICY.DueTax + ISNULL(P$POLICY.DueCatNat, 0) - ISNULL(P$POLICY.DueFees, 0) AS DuePremiumMinusTax
			,P$POLICY.AnnualCommission
			,P$POLICY.DueCommission
			,P$POLICY.AnnualTax - ISNULL(P$POLICY.AnnCatNat, 0) - ISNULL(P$POLICY.AnnCommercialPremium, 0)
			,P$POLICY.DueTax - ISNULL(P$POLICY.DueCatNat, 0) - ISNULL(P$POLICY.DueCommercialPremium, 0)
			,'' AS StopCodes
			,--we update this with a list of stopcodes later on in this script
			ISNULL((
					SELECT TOP 1 REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(TaskComment, '"', ''), ',', ''), CHAR(10), ''), CHAR(13), ' '), CHAR(9), '')
					FROM dbo.p$diarytaskversion dtv WITH (NOLOCK)
						,dbo.p$diarytask dt WITH (NOLOCK)
					WHERE dtv.policynumber = P$POLICY.PolicyNumber
						AND dtv.TaskStatus = 'O'
						AND dtv.TaskNumber = dt.TaskNumber
						AND dtv.Version = dt.LatestVersionNo
					ORDER BY dtv.ModifiedDate DESC
						,Version DESC
					), '') AS Diary
			,P$POLICY.PolicyYear
			,P$COVER.AddressNumber
			,CASE 
				WHEN P$POLICY.ProductCode = 'RDO-01'
					THEN 'D&O'
				WHEN P$POLICY.ProductCode = 'RCP-01'
					THEN P$COVRCP.DocumentDesc2
				WHEN P$POLICY.ProductCode = 'CDH01'
					THEN ''
				WHEN P$POLICY.ProductCode = 'RSP02'
					THEN 'FFA'
				WHEN P$POLICY.ProductCode = 'RSP01'
					THEN CASE 
							WHEN ISNULL(P$POLRSP.AnnMaterialPremium, 0) <> 0
								THEN 'MAT'
							ELSE ''
							END + CASE 
							WHEN ISNULL(P$POLRSP.AnnMusicPremium, 0) <> 0
								THEN ' INST'
							ELSE ''
							END + CASE 
							WHEN ISNULL(P$POLRSP.AnnArtPremium, 0) <> 0
								THEN ' ART'
							ELSE ''
							END + CASE 
							WHEN ISNULL(P$POLRSP.AnnExhibitPremium, 0) <> 0
								THEN ' EXP'
							ELSE ''
							END + CASE 
							WHEN (ISNULL(P$COVRSP.ExhibitType, '') = 'CU')
								AND (ISNULL(P$POLRSP.AnnExhibitPremium, 0) <> 0)
								THEN 'A'
							ELSE ''
							END + CASE 
							WHEN ISNULL(P$POLRSP.AnnGroupPremium, 0) <> 0
								THEN ' GPMT'
							ELSE ''
							END
				ELSE P$PROD.PolicyNumberPrefix
				END AS IndustryCode
			,ISNULL(CAST(P$POLICY.DDAuthorisedDate AS VARCHAR(12)), '') AS DDAuthorisedDate
			,ISNULL(P$POLICY.BrokerContact, '') AS BrokerContact
			,ISNULL(P$POLICY.ContributorCode, '') AS ContributorCode
			,(
				SELECT TOP 1 CASE 
						WHEN ISNULL(EndorseInProgress, '') = 'X'
							THEN 'Y'
						ELSE ''
						END
				FROM dbo.p$policy pol WITH (NOLOCK)
				WHERE pol.PolicyNumber = P$POLICY.PolicyNumber
					AND pol.VersionType = 'L'
				) AS EndorseInProgress
			,ISNULL(P$POLICY.AnnCommercialPremium, 0) AS AnnCommercialPremium
			,ISNULL(P$POLICY.DueCommercialPremium, 0) AS DueCommercialPremium
			,ISNULL(P$POLICY.AnnFees, 0) AS AnnFees
			,ISNULL(P$POLICY.DueFees, 0) AS DueFees
			,(
				SELECT TOP 1 d.Description AS SurveyType
				FROM dbo.p$survey s WITH (NOLOCK)
					,dbo.p$desc d WITH (NOLOCK)
				WHERE d.Code = s.SurveyType
					AND d.Tablename = 'SVYTYPE'
					AND s.ClientReference = P$POLICY.ClientReference
					AND s.AddressNumber = P$COVER.AddressNumber
				ORDER BY ISNULL(SurveyDate, 0) DESC
				) AS SurveyType
			,(
				SELECT TOP 1 ISNULL(s.SurveyComplete, 'N') AS SurveyComplete
				FROM dbo.p$survey s WITH (NOLOCK)
				WHERE s.ClientReference = P$POLICY.ClientReference
					AND s.AddressNumber = P$COVER.AddressNumber
				ORDER BY ISNULL(SurveyDate, 0) DESC
				) AS SurveyComplete
			,(
				SELECT TOP 1 ISNULL(CAST(SurveyDate AS VARCHAR(12)), '') AS SurveyDate
				FROM dbo.p$survey s WITH (NOLOCK)
				WHERE s.ClientReference = P$POLICY.ClientReference
					AND s.AddressNumber = P$COVER.AddressNumber
				ORDER BY ISNULL(SurveyDate, 0) DESC
				) AS SurveyDate
			,CASE 
				WHEN P$POLICY.ProductCode = 'RSP01'
					THEN CASE 
							WHEN p$covrsp2.MaterialExcessGroup = 'NXS'
								THEN '0'
							WHEN p$covrsp2.MaterialExcessGroup = 'FXS'
								THEN CAST(ISNULL(p$covrsp2.MaterialFixedExcessAmount, '0') AS VARCHAR(50))
							WHEN p$covrsp2.MaterialExcessGroup = 'XS1'
								THEN CAST(ISNULL(p$covrsp2.MaterialExcessPercentage2, 0) AS VARCHAR(10)) + '% ' + CAST(ISNULL(p$covrsp2.MaterialExcessMinAmount2, 0) AS VARCHAR(50))
							WHEN p$covrsp2.MaterialExcessGroup = 'XS2'
								THEN CAST(ISNULL(p$covrsp2.MaterialExcessPercentage, 0) AS VARCHAR(10)) + '% ' + CAST(ISNULL(p$covrsp2.MaterialExcessMinAmount, 0) AS VARCHAR(10)) + '/' + CAST(ISNULL(p$covrsp2.MaterialExcessMaxAmount, 0) AS VARCHAR(50))
							ELSE '0'
							END
				WHEN P$POLICY.ProductCode = 'EVE-01'
					THEN CASE 
							WHEN p$coveve.MaterialExcessType = 'NXS'
								THEN '0'
							WHEN p$coveve.MaterialExcessType = 'FXS'
								THEN CAST(ISNULL(p$coveve.MaterialFixedExcessAmount, '0') AS VARCHAR(50))
							WHEN p$coveve.MaterialExcessType = 'XS1'
								THEN CAST(ISNULL(p$coveve.MaterialExcessPercentage2, 0) AS VARCHAR(10)) + '% ' + CAST(ISNULL(p$coveve.MaterialExcessMinAmount2, 0) AS VARCHAR(50))
							WHEN p$coveve.MaterialExcessType = 'XS2'
								THEN CAST(ISNULL(p$coveve.MaterialExcessPercentage, 0) AS VARCHAR(10)) + '% ' + CAST(ISNULL(p$coveve.MaterialExcessMinAmount, 0) AS VARCHAR(10)) + '/' + CAST(ISNULL(p$coveve.MaterialExcessMaxAmount, 0) AS VARCHAR(50))
							ELSE '0'
							END
				WHEN P$POLICY.ProductCode = 'TRM01'
					THEN CASE 
							WHEN p$covtrm.MaterialExcessType = 'NXS'
								THEN '0'
							WHEN p$covtrm.MaterialExcessType = 'FXS'
								THEN CAST(ISNULL(p$covtrm.MaterialFixedExcessAmount, '0') AS VARCHAR(50))
							WHEN p$covtrm.MaterialExcessType = 'XS1'
								THEN CAST(ISNULL(p$covtrm.MaterialExcessPercentage2, 0) AS VARCHAR(10)) + '% ' + CAST(ISNULL(p$covtrm.MaterialExcessMinAmount2, 0) AS VARCHAR(50))
							WHEN p$covtrm.MaterialExcessType = 'XS2'
								THEN CAST(ISNULL(p$covtrm.MaterialExcessPercentage, 0) AS VARCHAR(10)) + '% ' + CAST(ISNULL(p$covtrm.MaterialExcessMinAmount, 0) AS VARCHAR(10)) + '/' + CAST(ISNULL(p$covtrm.MaterialExcessMaxAmount, 0) AS VARCHAR(50))
							ELSE '0'
							END
				ELSE '0'
				END AS MaterialExcess
			,CASE 
				WHEN P$POLICY.ProductCode = 'RSP01'
					THEN CASE 
							WHEN p$covrsp2.MusicExcessGroup = 'NXS'
								THEN '0'
							WHEN p$covrsp2.MusicExcessGroup = 'FXS'
								THEN CAST(ISNULL(p$covrsp2.MusicFixedExcessAmount, '0') AS VARCHAR(50))
							WHEN p$covrsp2.MusicExcessGroup = 'XS1'
								THEN CAST(ISNULL(p$covrsp2.MusicExcessPercentage2, 0) AS VARCHAR(10)) + '% ' + CAST(ISNULL(p$covrsp2.MusicExcessMinAmount2, 0) AS VARCHAR(50))
							WHEN p$covrsp2.MusicExcessGroup = 'XS2'
								THEN CAST(ISNULL(p$covrsp2.MusicExcessPercentage, 0) AS VARCHAR(10)) + '% ' + CAST(ISNULL(p$covrsp2.MusicExcessMinAmount, 0) AS VARCHAR(10)) + '/' + CAST(ISNULL(p$covrsp2.MusicExcessMaxAmount, 0) AS VARCHAR(50))
							ELSE '0'
							END
				ELSE '0'
				END AS RSPMusicExcess
			,CASE 
				WHEN P$POLICY.ProductCode = 'RSP01'
					THEN CASE 
							WHEN p$covrsp2.ExhibitExcessGroup = 'NXS'
								THEN '0'
							WHEN p$covrsp2.ExhibitExcessGroup = 'FXS'
								THEN CAST(ISNULL(p$covrsp2.ExhibitFixedExcessAmount, '0') AS VARCHAR(50))
							WHEN p$covrsp2.ExhibitExcessGroup = 'XS1'
								THEN CAST(ISNULL(p$covrsp2.ExhibitExcessPercentage2, 0) AS VARCHAR(10)) + '% ' + CAST(ISNULL(p$covrsp2.ExhibitExcessMinAmount2, 0) AS VARCHAR(50))
							WHEN p$covrsp2.ExhibitExcessGroup = 'XS2'
								THEN CAST(ISNULL(p$covrsp2.ExhibitExcessPercentage, 0) AS VARCHAR(10)) + '% ' + CAST(ISNULL(p$covrsp2.ExhibitExcessMinAmount, 0) AS VARCHAR(10)) + '/' + CAST(ISNULL(p$covrsp2.ExhibitExcessMaxAmount, 0) AS VARCHAR(50))
							ELSE '0'
							END
				WHEN P$POLICY.ProductCode = 'EVE-01'
					THEN CASE 
							WHEN p$coveve.ExhibitorExcessType = 'NXS'
								THEN '0'
							WHEN p$coveve.ExhibitorExcessType = 'FXS'
								THEN CAST(ISNULL(p$coveve.ExhibitorFixedExcessAmount, '0') AS VARCHAR(50))
							WHEN p$coveve.ExhibitorExcessType = 'XS1'
								THEN CAST(ISNULL(p$coveve.ExhibitorExcessPercentage2, 0) AS VARCHAR(10)) + '% ' + CAST(ISNULL(p$coveve.ExhibitorExcessMinAmount2, 0) AS VARCHAR(50))
							WHEN p$coveve.ExhibitorExcessType = 'XS2'
								THEN CAST(ISNULL(p$coveve.ExhibitorExcessPercentage, 0) AS VARCHAR(10)) + '% ' + CAST(ISNULL(p$coveve.ExhibitorExcessMinAmount, 0) AS VARCHAR(10)) + '/' + CAST(ISNULL(p$coveve.ExhibitorExcessMaxAmount, 0) AS VARCHAR(50))
							ELSE '0'
							END
				WHEN P$POLICY.ProductCode = 'TRM01'
					THEN CASE 
							WHEN p$covtrm.ExhibitorExcessType = 'NXS'
								THEN '0'
							WHEN p$covtrm.ExhibitorExcessType = 'FXS'
								THEN CAST(ISNULL(p$covtrm.ExhibitorFixedExcessAmount, '0') AS VARCHAR(50))
							WHEN p$covtrm.ExhibitorExcessType = 'XS1'
								THEN CAST(ISNULL(p$covtrm.ExhibitorExcessPercentage2, 0) AS VARCHAR(10)) + '% ' + CAST(ISNULL(p$covtrm.ExhibitorExcessMinAmount2, 0) AS VARCHAR(50))
							WHEN p$covtrm.ExhibitorExcessType = 'XS2'
								THEN CAST(ISNULL(p$covtrm.ExhibitorExcessPercentage, 0) AS VARCHAR(10)) + '% ' + CAST(ISNULL(p$covtrm.ExhibitorExcessMinAmount, 0) AS VARCHAR(10)) + '/' + CAST(ISNULL(p$covtrm.ExhibitorExcessMaxAmount, 0) AS VARCHAR(50))
							ELSE '0'
							END
				ELSE '0'
				END AS RSPExhibitExcess
			,CASE 
				WHEN P$POLICY.ProductCode = 'RSP01'
					THEN CASE 
							WHEN p$covrsp2.ArtExcessGroup = 'NXS'
								THEN '0'
							WHEN p$covrsp2.ArtExcessGroup = 'FXS'
								THEN CAST(ISNULL(p$covrsp2.ArtFixedExcessAmount, '0') AS VARCHAR(50))
							WHEN p$covrsp2.ArtExcessGroup = 'XS1'
								THEN CAST(ISNULL(p$covrsp2.ArtExcessPercentage2, 0) AS VARCHAR(10)) + '% ' + CAST(ISNULL(p$covrsp2.ArtExcessMinAmount2, 0) AS VARCHAR(50))
							WHEN p$covrsp2.ArtExcessGroup = 'XS2'
								THEN CAST(ISNULL(p$covrsp2.ArtExcessPercentage, 0) AS VARCHAR(10)) + '% ' + CAST(ISNULL(p$covrsp2.ArtExcessMinAmount, 0) AS VARCHAR(10)) + '/' + CAST(ISNULL(p$covrsp2.ArtExcessMaxAmount, 0) AS VARCHAR(50))
							ELSE '0'
							END
				ELSE '0'
				END AS RSPArtExcess
			,CASE 
				WHEN P$POLICY.ProductCode = 'RSP01'
					THEN CASE 
							WHEN p$covrsp2.GroupExcessGroup = 'NXS'
								THEN '0'
							WHEN p$covrsp2.GroupExcessGroup = 'FXS'
								THEN CAST(ISNULL(p$covrsp2.GroupFixedExcessAmount, '0') AS VARCHAR(50))
							WHEN p$covrsp2.GroupExcessGroup = 'XS1'
								THEN CAST(ISNULL(p$covrsp2.GroupExcessPercentage2, 0) AS VARCHAR(10)) + '% ' + CAST(ISNULL(p$covrsp2.GroupExcessMinAmount2, 0) AS VARCHAR(50))
							WHEN p$covrsp2.GroupExcessGroup = 'XS2'
								THEN CAST(ISNULL(p$covrsp2.GroupExcessPercentage, 0) AS VARCHAR(10)) + '% ' + CAST(ISNULL(p$covrsp2.GroupExcessMinAmount, 0) AS VARCHAR(10)) + '/' + CAST(ISNULL(p$covrsp2.GroupExcessMaxAmount, 0) AS VARCHAR(50))
							ELSE '0'
							END
				ELSE '0'
				END AS RSPGroupExcess
			,CASE 
				WHEN P$POLICY.ProductCode = 'TRM01'
					THEN CASE 
							WHEN ISNULL(P$covtrm.AnnLiabPremium, 0) <= 0
								THEN 0
							ELSE CASE 
									WHEN ISNULL(P$covtrm.LiabOrganiserAnnualOverride, 0) > 0
										THEN (
												(
													ISNULL(P$covtrm.LiabOrganiserAnnualOverride, 0) + --premium for RCO line
													((ISNULL(P$covtrm.LiabOrganiserAnnualOverride, 0) * ISNULL(P$covtrm.LiabExcessDiscount, 0)) / 100)
													) * --loading/discount for RCO
												(1 + (ISNULL(P$covtrm.LiabAdj1, 0) / 100))
												) * --percentage adjustment
											(ISNULL(P$covtrm.AnnLiabPremium, 0) / ISNULL(P$covtrm.LiabPremiumBeforeMin, 0)) --minimum premium factored
									ELSE (
											(
												ISNULL(P$covtrm.LiabOrganiserAnnual, 0) + --premium for RCO line
												((ISNULL(P$covtrm.LiabOrganiserAnnual, 0) * ISNULL(P$covtrm.LiabExcessDiscount, 0)) / 100)
												) * --loading/discount for RCO
											(1 + (ISNULL(P$covtrm.LiabAdj1, 0) / 100))
											) * --percentage adjustment
										(ISNULL(P$covtrm.AnnLiabPremium, 0) / ISNULL(P$covtrm.LiabPremiumBeforeMin, 0)) --minimum premium factored
									END
							END
				WHEN P$POLICY.ProductCode = 'EVE-01'
					THEN CASE 
							WHEN ISNULL(P$covtrm.LiabOrganiserAnnualOverride, 0) > 0
								THEN ROUND(ISNULL(P$covtrm.LiabOrganiserAnnualOverride, 0) * (1 + ((ISNULL(p$coveve.LiabAdj8, 0) / 100) + (ISNULL(p$coveve.LiabAdj7, 0) / 100) + (ISNULL(p$coveve.LiabAdj6, 0) / 100) + (ISNULL(p$coveve.LiabAdj5, 0) / 100) + (ISNULL(p$coveve.LiabAdj4, 0) / 100) + (ISNULL(p$coveve.LiabAdj3, 0) / 100) + (ISNULL(p$coveve.LiabAdj2, 0) / 100) + (ISNULL(p$coveve.LiabAdj1, 0) / 100))), 2)
							ELSE ROUND(((ISNULL(p$coveve.LiabBasePrem, 0) + ISNULL(p$coveve.NumPartic, 0) * ISNULL(p$coveve.LiabBasePremPerPerson, 0)) * ISNULL(p$coveve.LiabLOIFactor, 0) * ISNULL(p$coveve.LiabilitySiteFactor, 0)) * (1 + ((ISNULL(p$coveve.LiabAdj8, 0) / 100) +
                                                             (ISNULL(p$coveve.LiabAdj7, 0) / 100) + (ISNULL(p$coveve.LiabAdj6, 0)/ 100) + (ISNULL(p$coveve.LiabAdj5, 0) / 100) + (ISNULL(p$coveve.LiabAdj4, 0) / 100) + (ISNULL(p$coveve.LiabAdj3, 0) / 100) + (ISNULL(p$coveve.LiabAdj2, 0) / 100) + (ISNULL(p$coveve.LiabAdj1, 0) / 100))
										), 2)
							END
				ELSE 0
				END AS RCOPremium
			,CASE 
				WHEN P$POLICY.ProductCode = 'TRM01'
					THEN ISNULL(P$covtrm.LiabOrganiserSI, 0)
				WHEN P$POLICY.ProductCode = 'EVE-01'
					THEN ISNULL(p$coveve.LiabOrganiserSI, 0)
				ELSE 0
				END AS RCOSI
			,0 AS RCOAverage
			,CASE 
				WHEN P$POLICY.ProductCode = 'TRM01'
					THEN CASE 
							WHEN ISNULL(P$covtrm.LiabExhibitorsAnnual, 0) <= 0
								THEN 0
							ELSE CASE 
									WHEN ISNULL(P$covtrm.LiabExhibitorsAnnualOverride, 0) > 0
										THEN (
												(
													ISNULL(P$covtrm.LiabExhibitorsAnnualOverride, 0) + --premium for RCO line
													((ISNULL(P$covtrm.LiabExhibitorsAnnualOverride, 0) * ISNULL(P$covtrm.LiabExcessDiscount, 0)) / 100)
													) * --loading/discount for RCO
												(1 + (ISNULL(P$covtrm.LiabAdj1, 0) / 100))
												) * --percentage adjustment
											(ISNULL(P$covtrm.AnnLiabPremium, 0) / ISNULL(LiabPremiumBeforeMin, 0)) --minimum premium factored
									ELSE (
											(
												ISNULL(P$covtrm.LiabExhibitorsAnnual, 0) + --premium for RCO line
												((ISNULL(P$covtrm.LiabExhibitorsAnnual, 0) * ISNULL(P$covtrm.LiabExcessDiscount, 0)) / 100)
												) * --loading/discount for RCO
											(1 + (ISNULL(P$covtrm.LiabAdj1, 0) / 100))
											) * --percentage adjustment
										(ISNULL(P$covtrm.AnnLiabPremium, 0) / ISNULL(LiabPremiumBeforeMin, 0)) --minimum premium factored
									END
							END
				WHEN P$POLICY.ProductCode = 'EVE-01'
					THEN CASE 
							WHEN ISNULL(p$coveve.LiabExhibitorsAnnualOverride, 0) > 0
								THEN ISNULL(p$coveve.LiabExhibitorsAnnualOverride, 0) * (1 + ((ISNULL(p$coveve.LiabAdj8, 0) / 100) + (ISNULL(p$coveve.LiabAdj7, 0) / 100) + (ISNULL(p$coveve.LiabAdj6, 0) / 100) + (ISNULL(p$coveve.LiabAdj5, 0) / 100) + (ISNULL(p$coveve.LiabAdj4, 0) / 100) + (ISNULL(p$coveve.LiabAdj3, 0) / 100) + (ISNULL(p$coveve.LiabAdj2, 0) / 100) + (ISNULL(p$coveve.LiabAdj1, 0) / 100)))
							ELSE ISNULL(p$coveve.LiabExhibitorsAnnual, 0) * (1 + ((ISNULL(p$coveve.LiabAdj8, 0) / 100) + (ISNULL(p$coveve.LiabAdj7, 0) / 100) + (ISNULL(p$coveve.LiabAdj6, 0) / 100) + (ISNULL(p$coveve.LiabAdj5, 0) / 100) + (ISNULL(p$coveve.LiabAdj4, 0) / 100) +
                                                                   (ISNULL(p$coveve.LiabAdj3, 0) / 100) + (ISNULL(p$coveve.LiabAdj2, 0) / 100) + (ISNULL(p$coveve.LiabAdj1, 0) / 100)))
							END
				WHEN P$POLICY.ProductCode = 'FSC01'
					THEN ISNULL(P$COVFSC.AnnLiabPremium, 0)
				WHEN P$POLICY.ProductCode = 'MRP01'
					THEN ISNULL(P$TaxSplitHeader.AnnRCEPremium, 0)
				ELSE 0
				END AS RCEPremium
			,CASE 
				WHEN P$POLICY.ProductCode = 'TRM01'
					THEN ISNULL(P$covtrm.LiabExhibitorSI, 0)
				WHEN P$POLICY.ProductCode = 'EVE-01'
					THEN ISNULL(p$coveve.LiabExhibitorSI, 0)
				ELSE 0
				END AS RCESI
			,0 AS RCEAverage
			,ISNULL(P$POLICY.EndorsementFeesApply, 'N') AS AdminFeesApply
			,ISNULL(P$POLICY.AdminFeeExempt, 'N') AS AdminFeeExempt
			,ISNULL(P$POLICY.DueAdminFees, 0) AS DueAdminFees
			,ISNULL(P$POLICY.DueAdminFeesTax, 0) AS DueAdminFeesTax
			,ISNULL(P$COVRCP2.GarantieCyberEnabled,'N') AS CyberRisk 
			,P$AGENT.HASASReference
		FROM dbo.P$POLICY P$POLICY WITH (NOLOCK)
		INNER JOIN dbo.P$AGENT P$AGENT WITH (NOLOCK) ON P$POLICY.AgentReference = P$AGENT.AgentReference
		INNER JOIN dbo.P$CLIENT P$CLIENT WITH (NOLOCK) ON P$POLICY.ClientReference = P$CLIENT.ClientReference
		INNER JOIN dbo.P$COVER P$COVER WITH (NOLOCK) ON P$POLICY.PolicyNumber = P$COVER.PolicyNumber
			AND P$POLICY.Version = P$COVER.Version
		INNER JOIN dbo.P$ADDR P$ADDR WITH (NOLOCK) ON P$COVER.ClientReference = P$ADDR.ClientReference
			AND P$COVER.AddressNumber = P$ADDR.AddressNumber

       LEFT JOIN dbo.P$TaxSplitHeader P$TaxSplitHeader WITH (NOLOCK) ON P$POLICY.PolicyNumber = P$TaxSplitHeader.PolicyNumber 
	   AND P$POLICY.Version = P$TaxSplitHeader.Version and P$TaxSplitHeader.Deleted  = 0
	   LEFT JOIN dbo.P$covfbx P$covfbx WITH (NOLOCK) ON P$COVER.PolicyNumber = P$covfbx.PolicyNumber
			AND P$COVER.Version = P$covfbx.Version
			AND P$COVER.AddressNumber = P$covfbx.AddressNumber
			AND P$COVER.Deleted = P$covfbx.Deleted
		LEFT JOIN dbo.P$COVRBD P$COVRBD WITH (NOLOCK) ON P$COVER.PolicyNumber = P$COVRBD.PolicyNumber
			AND P$COVER.Version = P$COVRBD.Version
			AND P$COVER.AddressNumber = P$COVRBD.AddressNumber
			AND P$COVER.Deleted = P$COVRBD.Deleted
		LEFT JOIN dbo.P$COVRCP P$COVRCP WITH (NOLOCK) ON P$COVER.PolicyNumber = P$COVRCP.PolicyNumber
			AND P$COVER.Version = P$COVRCP.Version
			AND P$COVER.AddressNumber = P$COVRCP.AddressNumber
			AND P$COVER.Deleted = P$COVRCP.Deleted
		LEFT JOIN dbo.P$COVRCP2 P$COVRCP2 WITH (NOLOCK) ON P$COVER.PolicyNumber = P$COVRCP2.PolicyNumber
			AND P$COVER.Version = P$COVRCP2.Version
			AND P$COVER.AddressNumber = P$COVRCP2.AddressNumber
			AND P$COVER.Deleted = P$COVRCP2.Deleted
		LEFT JOIN dbo.P$COVCDH P$COVCDH WITH (NOLOCK) ON P$COVER.PolicyNumber = P$COVCDH.PolicyNumber
			AND P$COVER.Version = P$COVCDH.Version
			AND P$COVER.AddressNumber = P$COVCDH.AddressNumber
			AND P$COVER.Deleted = P$COVCDH.Deleted
		LEFT JOIN dbo.P$COVRDO P$COVRDO WITH (NOLOCK) ON P$COVER.PolicyNumber = P$COVRDO.PolicyNumber
			AND P$COVER.Version = P$COVRDO.Version
			AND P$COVER.AddressNumber = P$COVRDO.AddressNumber
			AND P$COVER.Deleted = P$COVRDO.Deleted
		LEFT JOIN dbo.P$covrsp P$covrsp WITH (NOLOCK) ON P$COVER.PolicyNumber = P$covrsp.PolicyNumber
			AND P$COVER.Version = P$covrsp.Version
			AND P$COVER.AddressNumber = P$covrsp.AddressNumber
			AND P$COVER.Deleted = P$covrsp.Deleted
		LEFT JOIN dbo.P$coveve P$coveve WITH (NOLOCK) ON P$COVER.PolicyNumber = P$coveve.PolicyNumber
			AND P$COVER.Version = P$coveve.Version
			AND P$COVER.AddressNumber = P$coveve.AddressNumber
			AND P$COVER.Deleted = P$coveve.Deleted
		LEFT JOIN dbo.P$covccs P$covccs WITH (NOLOCK) ON P$COVER.PolicyNumber = P$covccs.PolicyNumber
			AND P$COVER.Version = P$covccs.Version
			AND P$COVER.AddressNumber = P$covccs.AddressNumber
			AND P$COVER.Deleted = P$covccs.Deleted
		LEFT JOIN dbo.P$covrsp2 P$covrsp2 WITH (NOLOCK) ON P$COVER.PolicyNumber = P$covrsp2.PolicyNumber
			AND P$COVER.Version = P$covrsp2.Version
			AND P$COVER.AddressNumber = P$covrsp2.AddressNumber
			AND P$COVER.Deleted = P$covrsp2.Deleted
		INNER JOIN dbo.P$DESC P$DESC WITH (NOLOCK) ON Case when P$POLICY.ProductCode = 'MRP01' Then P$TaxSplitHeader.Pays else P$ADDR.CountryCode end = P$DESC.Code 
		LEFT JOIN dbo.P$covobh P$covobh WITH (NOLOCK) ON P$COVER.PolicyNumber = P$covobh.PolicyNumber
			AND P$COVER.Version = P$covobh.Version
			AND P$COVER.AddressNumber = P$covobh.AddressNumber
			AND P$COVER.Deleted = P$covobh.Deleted
		LEFT JOIN dbo.P$COVCPX P$COVCPX WITH (NOLOCK) ON P$COVER.PolicyNumber = P$COVCPX.PolicyNumber
			AND P$COVER.Version = P$COVCPX.Version
			AND P$COVER.AddressNumber = P$COVCPX.AddressNumber
			AND P$COVER.Deleted = P$COVCPX.Deleted
		LEFT JOIN dbo.P$POLRSP P$POLRSP WITH (NOLOCK) ON P$COVER.PolicyNumber = P$POLRSP.PolicyNumber
			AND P$COVER.Version = P$POLRSP.Version
		INNER JOIN dbo.p$prod P$PROD WITH (NOLOCK) ON P$POLICY.ProductCode = P$PROD.ProductCode
		LEFT JOIN dbo.P$covtrm P$covtrm WITH (NOLOCK) ON P$COVER.PolicyNumber = P$COVTRM.PolicyNumber
			AND P$COVER.Version = P$COVTRM.Version
			AND P$COVER.AddressNumber = P$COVTRM.AddressNumber
			AND P$COVER.Deleted = P$COVTRM.Deleted
		LEFT JOIN dbo.P$COVFSC P$COVFSC WITH (NOLOCK) ON P$COVER.PolicyNumber = P$COVFSC.PolicyNumber
			AND P$COVER.Version = P$COVFSC.Version
			AND P$COVER.AddressNumber = P$COVFSC.AddressNumber
			AND P$COVER.Deleted = P$COVFSC.Deleted
		LEFT JOIN dbo.P$COVFSC2 P$COVFSC2 WITH (NOLOCK) ON P$COVER.PolicyNumber = P$COVFSC2.PolicyNumber
			AND P$COVER.Version = P$COVFSC2.Version
			AND P$COVER.AddressNumber = P$COVFSC2.AddressNumber
			AND P$COVER.Deleted = P$COVFSC2.Deleted

		LEFT JOIN dbo.P$COVMRP P$COVMRP WITH (NOLOCK) ON P$COVER.PolicyNumber = P$COVMRP.PolicyNumber 
			AND P$COVER.Version = P$COVMRP.Version
			AND P$COVER.AddressNumber = P$COVMRP.AddressNumber
			AND P$COVER.Deleted = P$COVMRP.Deleted

		LEFT JOIN dbo.P$POLFSC P$POLFSC WITH (NOLOCK) ON P$COVER.PolicyNumber = P$POLFSC.PolicyNumber
			AND P$COVER.Version = P$POLFSC.Version
		WHERE P$POLICY.STATUS IN (
				'EC'
				,'RS'
				,'LP'
				)
			AND ISNULL(P$POLICY.CFI, 'N') <> 'Y'
			AND P$POLICY.EffectiveTo >= @TODATE
			AND P$POLICY.EffectiveFrom < @TODATE + 1
			AND P$DESC.Tablename = 'COUNTRY'
			AND (
				p$Policy.agentreference = @AgentReference
				OR @AgentReference = ''
				)
			AND ISNULL(P$COVER.Deleted, '') <> 'X'
			AND
			/*We are excluding the TRM policies with liability that have null in the LiabPremiumBeforeMin field 
				as they break the calculation, and they only exist in test (field was added during dev, 
				and some test policies do not have it)*/
			(
				P$POLICY.ProductCode <> 'TRM01'
				OR (
					P$POLICY.ProductCode = 'TRM01'
					AND (
						ISNULL(P$covtrm.LiabPremiumBeforeMin, 0) > 0
						AND ISNULL(P$covtrm.AnnLiabPremium, 0) > 0
						)
					)
				OR (
					P$POLICY.ProductCode = 'TRM01'
					AND ISNULL(P$covtrm.AnnLiabPremium, 0) <= 0
					)
				)
		ORDER BY P$AGENT.AgentReference
			,P$CLIENT.Title
			,P$CLIENT.Surname
			,P$CLIENT.Forename
			,P$CLIENT.PartnerTitle
			,P$CLIENT.PartnerSurname
			,P$CLIENT.PartnerForename
			,P$POLICY.PolicyNumber



		-- Now do the average rates!!!
		UPDATE #ARRWORK
		SET BldAverageCalculation = CASE 
				WHEN (
						(ISNULL(BuildingsPremium, 0) <> 0)
						AND (ISNULL(BuildingSI / 100, 0) <> 0)
						)
					THEN ISNULL(BuildingsPremium, 0) / (ISNULL(BuildingSI, 0) / 100)
				ELSE 0
				END
			,BldAreaCalc = CASE 
				WHEN (
						ISNULL(BuildingsPremium, 0) <> 0
						AND ISNULL(BuildingsArea, 0) <> 0
						)
					THEN ISNULL(BuildingsPremium, 0) / ISNULL(BuildingsArea, 0)
				ELSE 0
				END
			,ContAverageCalc = CASE 
				WHEN (
						ISNULL(ContentsPremium, 0) <> 0
						AND ISNULL(ContentsSumInsured / 100, 0) <> 0
						)
					THEN ISNULL(ContentsPremium, 0) / (ISNULL(ContentsSumInsured / 100, 0))
				ELSE 0
				END
			,ArtAverage = CASE 
				WHEN (
						ISNULL(ArtPremium, 0) <> 0
						AND ISNULL(ArtSI / 100, 0) <> 0
						)
					THEN ISNULL(ArtPremium, 0) / ISNULL(ArtSI / 100, 0)
				ELSE 0
				END
			,AveragePPSCalc = CASE 
				WHEN (
						ISNULL(PPSPremium, 0) <> 0
						AND ISNULL(PPSSI / 100, 0) <> 0
						)
					THEN ISNULL(PPSPremium, 0) / ISNULL(PPSSI / 100, 0)
				ELSE 0
				END
			,LiabilityAverageSIRate = CASE 
				WHEN (
						ISNULL(LiabilitySIPremium, 0) <> 0
						AND ISNULL(BuildingSI / 100, 0) <> 0
						)
					THEN ISNULL(LiabilitySIPremium, 0) / (ISNULL(BuildingSI / 100, 0))
				ELSE 0
				END
			,LiabilityAverageAreaRate = CASE 
				WHEN (
						ISNULL(LiabilityAreaPremium, 0) <> 0
						AND ISNULL(BuildingsArea, 0) <> 0
						)
					THEN ISNULL(LiabilityAreaPremium, 0) / ISNULL(BuildingsArea, 0)
				ELSE 0
				END
			,LiabilityRenterAverageRate = CASE 
				WHEN (
						ISNULL(LiabilityRenterPremium, 0) <> 0
						AND ISNULL(LiabilityRenterArea, 0) <> 0
						)
					THEN ISNULL(LiabilityRenterPremium, 0 / ISNULL(LiabilityRenterArea, 0))
				ELSE 0
				END
			,RSPMaterialAverageCalc = CASE 
				WHEN (
						ISNULL(RSPMaterialPremium, 0) <> 0
						AND ISNULL(RSPMaterialSI / 100, 0) <> 0
						)
					THEN ISNULL(RSPMaterialPremium, 0) / (ISNULL(RSPMaterialSI / 100, 0))
				ELSE 0
				END
			,RSPMusicAverageCalc = CASE 
				WHEN (
						ISNULL(RSPMusicPremium, 0) <> 0
						AND ISNULL(RSPMusicSI / 100, 0) <> 0
						)
					THEN ISNULL(RSPMusicPremium, 0) / (ISNULL(RSPMusicSI / 100, 0))
				ELSE 0
				END
			,RSPExhibitAverageCalc = CASE 
				WHEN (
						ISNULL(RSPExhibitPremium, 0) <> 0
						AND ISNULL(RSPExhibitSI / 100, 0) <> 0
						)
					THEN ISNULL(RSPExhibitPremium, 0) / (ISNULL(RSPExhibitSI / 100, 0))
				ELSE 0
				END
			,RSPArtAverageCalc = CASE 
				WHEN (
						ISNULL(RSPArtPremium, 0) <> 0
						AND ISNULL(RSPArtSI / 100, 0) <> 0
						)
					THEN ISNULL(RSPArtPremium, 0) / (ISNULL(RSPArtSI / 100, 0))
				ELSE 0
				END
			,RSPGroupAverageCalc = CASE 
				WHEN (
						ISNULL(RSPGroupPremium, 0) <> 0
						AND ISNULL(RSPGroupSI / 100, 0) <> 0
						)
					THEN ISNULL(RSPGroupPremium, 0) / (ISNULL(RSPGroupSI / 100, 0))
				ELSE 0
				END
			,PIAverageCalc = CASE 
				WHEN (
						ISNULL(PIPremium, 0) <> 0
						AND ISNULL(PISI / 100, 0) <> 0
						)
					THEN ISNULL(PIPremium, 0) / (ISNULL(PISI / 100, 0))
				ELSE 0
				END
			,RCOAverage = CASE 
				WHEN (
						ISNULL(RCOPremium, 0) <> 0
						AND ISNULL(RCOSI, 0) <> 0
						)
					THEN ISNULL(RCOPremium, 0) / (ISNULL(RCOSI / 100, 0))
				ELSE 0
				END
			,RCEAverage = CASE 
				WHEN (
						ISNULL(RCEPremium, 0) <> 0
						AND ISNULL(RCESI, 0) <> 0
						)
					THEN ISNULL(RCEPremium, 0) / (ISNULL(RCESI / 100, 0))
				ELSE 0
				END

		--Set the stopcodes
		DECLARE @POLNUM VARCHAR(15)
			,@POLYEAR INT
			,@ADNUM INT
			,@StopCodeList VARCHAR(255)

		DECLARE StopCodeGroupLevel CURSOR
		FOR
		SELECT PolicyNumber
			,PolicyYear
			,AddressNumber
		FROM #ARRWORK WITH (NOLOCK)

		OPEN StopCodeGroupLevel

		FETCH NEXT
		FROM StopCodeGroupLevel
		INTO @POLNUM
			,@POLYEAR
			,@ADNUM

		WHILE @@FETCH_STATUS = 0
		BEGIN
			SET @StopCodeList = (
					SELECT TOP 1 StopDescription + ', '
					FROM p$prenstop d WITH (NOLOCK)
					WHERE policynumber = @POLNUM
						AND policyyear = @POLYEAR
						AND ISNULL(IgnoreStop, 'N') <> 'Y'
					ORDER BY d.StopCodeRef
					FOR XML PATH('')
					)
			SET @StopCodeList = SUBSTRING(@StopCodeList, 1, LEN(@StopCodeList) - 1)
			SET @StopCodeList = REPLACE(REPLACE(REPLACE(@StopCodeList, '&lt;', '<'), '&gt;', '>'), '&amp;', '&')

			UPDATE #ARRWORK
			SET StopCodes = ISNULL(@StopCodeList, '')
			WHERE PolicyNumber = @POLNUM
				AND PolicyYear = @POLYEAR
				AND AddressNumber = @ADNUM

			FETCH NEXT
			FROM StopCodeGroupLevel
			INTO @POLNUM
				,@POLYEAR
				,@ADNUM
		END

		CLOSE StopCodeGroupLevel

		DEALLOCATE StopCodeGroupLevel

		--Create the result table 
		SELECT *
		INTO #RES
		FROM #ARRWORK
		WHERE agentreference = 'xxxx'


		--This has to be dynamic SQL because we need an identitykey to keep the order for the report and ALTER TABLES's just don't
		--Run inside STORED PROCS, that was a new one on me I can tell ya!
		DECLARE @v_stmt VARCHAR(max)

		SET @v_stmt = 'ALTER TABLE #RES add IdNum int IDENTITY (1, 1)'

		EXEC (@v_stmt)

		-- Now do group the detail line of the average rates!!!
		-- So for each agent in the report, select the record and and a total row.
		DECLARE @CURRENTAGENT VARCHAR(15)

		DECLARE AgentGroupLevel CURSOR LOCAL 
		FOR
		SELECT DISTINCT (AgentReference)
		FROM #ARRWORK WITH (NOLOCK)

		OPEN AgentGroupLevel

		FETCH NEXT
		FROM AgentGroupLevel
		INTO @CURRENTAGENT

		WHILE @@FETCH_STATUS = 0
		BEGIN
			INSERT INTO #RES
			SELECT *
			FROM #ARRWORK
			WHERE agentreference = @CURRENTAGENT

			INSERT INTO #RES
			SELECT NULL
				,NULL
				,NULL
				,NULL
				,NULL
				,NULL
				,NULL
				,NULL
				,NULL
				,NULL
				,NULL
				,NULL
				,NULL
				,NULL
				,NULL
				,NULL
				,NULL
				,NULL
				,NULL
				,NULL
				,NULL
				,NULL
				,NULL
				,NULL
				,NULL
				,NULL
				,NULL
				,NULL
				,NULL
				,SUM(ISNULL(BuildingsPremium, 0))
				,SUM(ISNULL(BuildingSI, 0))
				,SUM(ISNULL(BuildingsAreaPremium, 0))
				,SUM(ISNULL(BuildingsArea, 0))
				,CASE 
					WHEN (
							SUM(ISNULL(BuildingsPremium, 0)) <> 0
							AND SUM(ISNULL(BuildingSI, 0)) <> 0
							)
						THEN SUM(ISNULL(BuildingsPremium, 0)) / (SUM(ISNULL(BuildingSI, 0)) / 100)
					END
				,CASE 
					WHEN (
							SUM(ISNULL(BuildingsAreaPremium, 0)) <> 0
							AND SUM(ISNULL(BuildingsArea, 0)) <> 0
							)
						THEN SUM(ISNULL(BuildingsAreaPremium, 0)) / SUM(ISNULL(BuildingsArea, 0))
					END
				,SUM(ISNULL(ContentsPremium, 0))
				,SUM(ISNULL(ContentsSumInsured, 0))
				,CASE 
					WHEN (
							SUM(ISNULL(ContentsPremium, 0)) <> 0
							AND SUM(ISNULL(ContentsSumInsured, 0)) <> 0
							)
						THEN SUM(ISNULL(ContentsPremium, 0)) / (SUM(ISNULL(ContentsSumInsured, 0)) / 100)
					END
				,SUM(ISNULL(ArtPremium, 0))
				,SUM(ISNULL(ArtSI, 0))
				,CASE 
					WHEN (
							SUM(ISNULL(ArtPremium, 0)) <> 0
							AND SUM(ISNULL(ArtSI, 0)) <> 0
							)
						THEN SUM(ISNULL(ArtPremium, 0)) / (SUM(ISNULL(ArtSI, 0)) / 100)
					END
				,SUM(ISNULL(PPSPremium, 0))
				,SUM(ISNULL(PPSSI, 0))
				,CASE 
					WHEN (
							SUM(ISNULL(PPSPremium, 0)) <> 0
							AND SUM(ISNULL(PPSSI, 0)) <> 0
							)
						THEN SUM(ISNULL(PPSPremium, 0)) / (SUM(ISNULL(PPSSI, 0)) / 100)
					END
				,SUM(ISNULL(LiabilitySIPremium, 0))
				,SUM(ISNULL(LiabilityAreaPremium, 0))
				,CASE 
					WHEN (
							SUM(ISNULL(LiabilitySIPremium, 0)) <> 0
							AND SUM(ISNULL(BuildingSI, 0)) <> 0
							)
						THEN SUM(ISNULL(LiabilitySIPremium, 0)) / (SUM(ISNULL(BuildingSI, 0)) / 100)
					END
				,CASE 
					WHEN (
							SUM(ISNULL(LiabilityAreaPremium, 0)) <> 0
							AND SUM(ISNULL(BuildingsArea, 0)) <> 0
							)
						THEN SUM(ISNULL(LiabilityAreaPremium, 0)) / SUM(ISNULL(BuildingsArea, 0)) --/100)was another thing wrong in crystal?????
					END
				,SUM(ISNULL(LiabilityRenterPremium, 0))
				,SUM(ISNULL(LiabilityRenterArea, 0))
				,CASE 
					WHEN (
							SUM(ISNULL(LiabilityRenterPremium, 0)) <> 0
							AND SUM(ISNULL(LiabilityRenterArea, 0)) <> 0
							)
						THEN SUM(ISNULL(LiabilityRenterPremium, 0)) / SUM(ISNULL(LiabilityRenterArea, 0)) --/100)was another thing wrong in crystal?????
					END
				,SUM(ISNULL(LiabilityPriveePremium, 0))
				,SUM(ISNULL(LiabReduction, 0))
				,SUM(ISNULL(LegalAidPremium, 0))
				,SUM(ISNULL(PAPremium, 0))
				,SUM(ISNULL(PASI, 0))
				,CASE 
					WHEN (
							SUM(ISNULL(PAPremium, 0)) <> 0
							AND SUM(ISNULL(PASI, 0)) <> 0
							)
						THEN SUM(ISNULL(PAPremium, 0)) / (SUM(ISNULL(PASI, 0)) / 100)
					END
				,SUM(ISNULL(AnnBusIntPremium, 0))
				,SUM(ISNULL(BusIntSI, 0))
				,CASE 
					WHEN (
							SUM(ISNULL(AnnBusIntPremium, 0)) <> 0
							AND SUM(ISNULL(BusIntSI, 0)) <> 0
							)
						THEN SUM(ISNULL(AnnBusIntPremium, 0)) / (SUM(ISNULL(BusIntSI, 0)) / 100)
					END
				,SUM(ISNULL(RSPMaterialPremium, 0))
				,SUM(ISNULL(RSPMaterialSI, 0))
				,CASE 
					WHEN (
							SUM(ISNULL(RSPMaterialPremium, 0)) <> 0
							AND SUM(ISNULL(RSPMaterialSI, 0)) <> 0
							)
						THEN SUM(ISNULL(RSPMaterialPremium, 0)) / (SUM(ISNULL(RSPMaterialSI, 0)) / 100)
					END
				,SUM(ISNULL(RSPMusicPremium, 0))
				,SUM(ISNULL(RSPMusicSI, 0))
				,CASE 
					WHEN (
							SUM(ISNULL(RSPMusicPremium, 0)) <> 0
							AND SUM(ISNULL(RSPMusicSI, 0)) <> 0
							)
						THEN SUM(ISNULL(RSPMusicPremium, 0)) / (SUM(ISNULL(RSPMusicSI, 0)) / 100)
					END
				,SUM(ISNULL(RSPExhibitPremium, 0))
				,SUM(ISNULL(RSPExhibitSI, 0))
				,CASE 
					WHEN (
							SUM(ISNULL(RSPExhibitPremium, 0)) <> 0
							AND SUM(ISNULL(RSPExhibitSI, 0)) <> 0
							)
						THEN SUM(ISNULL(RSPExhibitPremium, 0)) / (SUM(ISNULL(RSPExhibitSI, 0)) / 100)
					END
				,SUM(ISNULL(RSPArtPremium, 0))
				,SUM(ISNULL(RSPArtSI, 0))
				,CASE 
					WHEN (
							SUM(ISNULL(RSPArtPremium, 0)) <> 0
							AND SUM(ISNULL(RSPArtSI, 0)) <> 0
							)
						THEN SUM(ISNULL(RSPArtPremium, 0)) / (SUM(ISNULL(RSPArtSI, 0)) / 100)
					END
				,SUM(ISNULL(RSPGroupPremium, 0))
				,SUM(ISNULL(RSPGroupSI, 0))
				,CASE 
					WHEN (
							SUM(ISNULL(RSPGroupPremium, 0)) <> 0
							AND SUM(ISNULL(RSPGroupSI, 0)) <> 0
							)
						THEN SUM(ISNULL(RSPGroupPremium, 0)) / (SUM(ISNULL(RSPGroupSI, 0)) / 100)
					END
				,SUM(ISNULL(PILimit, 0))
				,SUM(ISNULL(PIPremium, 0))
				,SUM(ISNULL(PISI, 0))
				,CASE 
					WHEN (
							SUM(ISNULL(PIPremium, 0)) <> 0
							AND SUM(ISNULL(PISI, 0)) <> 0
							)
						THEN SUM(ISNULL(PIPremium, 0)) / (SUM(ISNULL(PISI, 0)) / 100)
					END
				,NULL
				,NULL
				,NULL
				,NULL
				,NULL
				,NULL
				,NULL
				,NULL
				,NULL
				,NULL
				,NULL
				,NULL
				,NULL
				,NULL
				,NULL
				,NULL
				,NULL
				,NULL
				,NULL
				,NULL
				,NULL
				,NULL
				,NULL
				,NULL
				,NULL
				,NULL
				,NULL
				,NULL
				,NULL
				,NULL
				,NULL
				,NULL
				,NULL
				,NULL
				,NULL
				,NULL
				,NULL
				,NULL
				,NULL
				,NULL
				,NULL
				,NULL
				,NULL
				,NULL
				,NULL
				,SUM(ISNULL(RCOPremium, 0))
				,SUM(ISNULL(RCOSI, 0))
				,CASE 
					WHEN (
							SUM(ISNULL(RCOPremium, 0)) <> 0
							AND SUM(ISNULL(RCOSI, 0)) <> 0
							)
						THEN SUM(ISNULL(RCOPremium, 0)) / (SUM(ISNULL(RCOSI, 0)) / 100)
					END
				,SUM(ISNULL(RCEPremium, 0))
				,SUM(ISNULL(RCESI, 0))
				,CASE 
					WHEN (
							SUM(ISNULL(RCEPremium, 0)) <> 0
							AND SUM(ISNULL(RCESI, 0)) <> 0
							)
						THEN SUM(ISNULL(RCEPremium, 0)) / (SUM(ISNULL(RCESI, 0)) / 100)
					END
				,NULL
				,NULL
				,NULL
				,NULL
				,NULL
				,NULL
			FROM #ARRWORK
			WHERE agentreference = @CURRENTAGENT

			FETCH NEXT
			FROM AgentGroupLevel
			INTO @CURRENTAGENT
		END

		CLOSE AgentGroupLevel
		DEALLOCATE AgentGroupLevel


		--This has to be dynamic SQL because the above is dynamic'
		--SET @v_stmt = 'SELECT AgentReference AS ''Agent Ref'', RenewalDate AS ''Date de Renouvellement'' FROM #RES order by IdNum ASC'
		SET @v_stmt = 'SELECT ' + 'CASE WHEN UnderwriterIndicator = ''S'' AND  dbo.BrexitEnabled() = ''Y'' THEN HASASReference ELSE AgentReference END AS ''Agent Ref'',' + 
		'AgentName AS ''Agent Name'',' + 'PolicyNumber AS ''Policy Number'',' + 'ProductCode AS ''Product Code'',' + 'IndustryCode AS ''Industry Code'',' +'CyberRisk AS ''Cyber risk'','+ 
		'Underwriter AS ''Souscripteur'',' + 'BrokerContact AS ''Gestionnaires'',' + 'ClientReference AS ''Client Code'',' + 'ClientName AS ''Client Name'',' + 
		'DocumentDesc1 AS ''Description de lactivité'',' + 'CompanyRegistrationNumber AS ''Numéro de SIREN/SIRET'',' + 'CompanyAPECategory AS ''Numéro APE'',' + 
		'CGReference AS ''Conditions Générales No'',' + 'PreviousPolicyNo AS ''Ancien N° de contrat'',' + 'NoOfInstalments AS ''Instalments'',' + 
		'PaymentLoad AS ''Fees %'',' + 'Payment AS ''Mode de règlement'',' + 'DDAuthorisedDate AS ''Date de Mise en Place Du DD'',' + 
		'CommissionRate AS ''Commission Rate :'',' + 'FixedCommissionRate AS ''Fixed Commission'',' + 'InceptionDate AS ''Inception Date :'',' + 
		'RenewalDate AS ''Date de Renouvellement'',' + 'MethodOfPlacement AS ''MOP'',' + 'ContributorCode AS ''Code apporteur'',' + 
		'NumberOfRisks AS ''Number of Risks'',' + 'Address AS ''Address'',' + 'Postcode AS ''Postcode'',' + 'Description AS ''Country'',' + 
		'Residence AS ''Residence'',' + 'HouseType AS ''House Type'',' + 'Residency AS ''Residency'',' + 'UnderwriterIndicator AS ''Underwritten By'',' + 
		'GenSectionNo AS ''General Section Number'',' + 'AgentPolicyNumber AS ''Référence Courtier du contrat'',' + 'CurrencyType AS ''Currency'',' + 
		'BldOwnerReconstruction AS ''Reconstruction'',' + 'BuildingsPremium AS ''Buildings Premium'',' + 'BuildingSI AS ''Buildings SI'',' + 
		'BuildingsAreaPremium AS ''Buildings Area Premium'',' + 'BuildingsArea AS ''Buildings Area'',' + 'ROUND(BldAverageCalculation,2) AS ''Buildings Av. SI Rate'',' + 
		'ROUND(BldAreaCalc,2) AS ''Buildings Av. Area Rate'',' + 'ContentsPremium AS ''Contents Premium'',' + 'ContentsSumInsured AS ''Contents SI'',' + 
		'ROUND(ContAverageCalc,2) AS ''Contents Av. Rate'',' + 'ArtPremium AS ''Art Premium'',' + 'ArtSI AS ''Art SI'',' + 'ROUND(ArtAverage,2) AS ''Art Av. Rate'','+
		'PPSPremium AS ''Valuables Premium'',' + 'PPSSI AS ''Valuables SI'',' + 'ROUND(AveragePPSCalc,2) AS ''Valuables Av. Rate'',' + 
		'LiabilitySIPremium AS ''R.C. Propriétaire SI Premium'',' + 'LiabilityAreaPremium AS ''R.C. Propriétaire Area Premium'',' + 
		'ROUND(LiabilityAverageSIRate,2) AS ''R.C. Propriétaire Av. SI Rate'',' + 'ROUND(LiabilityAverageAreaRate,2) AS ''R.C. Propriétaire Av. Area Rate'',' + 
		'LiabilityRenterPremium AS ''R.C. Locataire Premium'',' + 'LiabilityRenterArea AS ''R.C. Locataire Area'',' + 'ROUND(LiabilityRenterAverageRate,2) AS ''R.C. Locataire Av. Area Rate'',' + 
		'LiabilityPriveePremium AS ''R.C. Privée Premium'',' + 'LiabReduction AS ''R.C. Discount'',' + 'LegalAidPremium AS ''Legal Aid Premium'',' + 
		'PAPremium AS ''Personal Accident Premium'',' + 'PASI AS ''Personal Accident SI'',' + 'ROUND(ADAverageRate,2) AS ''Personal Accident Av. Rate'',' + 
		'AnnBusIntPremium AS ''Bus. Int. Premium'',' + 'BusIntSI AS ''Bus. Int. SI'',' + 'ROUND(BIAverageRate,2) AS ''Bus. Int. Av. Rate'',' + 
		'RSPMaterialPremium AS ''Matériel Premium'',' + 'RSPMaterialSI AS ''Matériel SI'',' + 'ROUND(RSPMaterialAverageCalc,2) AS ''Matériel Av. Rate'',' + 
		'RSPMusicPremium AS ''Musique Premium'',' + 'RSPMusicSI AS ''Musique SI'',' + 'ROUND(RSPMusicAverageCalc,2) AS ''Musique Av. Rate'',' + 
		'RSPExhibitPremium AS ''Expositions Premium'',' + 'RSPExhibitSI AS ''Expositions SI'',' + 'ROUND(RSPExhibitAverageCalc,2) AS ''Expositions Av. Rate'',' + 
		'RSPArtPremium AS ''RSP/FFA Art Premium'',' + 'RSPArtSI AS ''RSP/FFAArt SI'',' + 'ROUND(RSPArtAverageCalc,2) AS ''RSP/FFA Art Av. Rate'',' + 
		'RSPGroupPremium AS ''Groupement Premium'',' + 'RSPGroupSI AS ''Groupements SI'',' + 'ROUND(RSPGroupAverageCalc,2) AS ''Groupements Av. Rate'',' + 
		'PILimit AS ''Limite PI'',' + 'PIPremium AS ''PI/D&O Premium'',' + 'PISI AS ''PI/D&O CA'',' + 'ROUND(PIAverageCalc,2) AS ''PI/D&O Av. Rate'',' + 
		'TotalSI AS ''Total SI'',' + 'BlankColumn1 AS '' '',' + 'BlankColumn2 AS '' '',' + 'BldReduction AS ''Buildings Deductible'',' + 
		'ContReduction AS ''Contents Deductible'',' + 'ArtReduction AS ''Fine Art Deductible'',' + 'ItemsReduction AS ''Valuables Deductible'',' + 
		'PIReduction AS ''PI Deductible'',' + 'USPIReduction AS ''US PI Deductible'',' + 'FFBIndex AS ''FFB Index'',' + 'Discount AS ''Mesure commercial'',' + 
		'AnnAddressPremium AS ''Prime TTC adresse de risque A'',' + 'DueAddressPremium AS ''Prime TTC adresse de risque P'',' + 
		'AnnualPremium AS ''Prime TTC par police A'',' + 'DuePremium AS ''Prime TTC par police P'',' + 'AnnualPremiumMinusTax AS ''Prime HT par police A'',' + 
		'DuePremiumMinusTax AS ''Prime HT par police P'',' + 'AnnCommercialPremium AS ''Gareat A'',' + 'DueCommercialPremium AS ''Gareat P'',' + 
		'AnnFees AS ''Fees A'',' + 'DueFees AS ''Fees P'',' + 'AnnualCommission AS ''Total commission value A'',' + 'DueCommission AS ''Total commission value P'',' + 
		'AnnualTax AS ''Taxes Totales A'',' + 'DueTax AS ''Taxes Totales P'',' + 'StopCodes AS ''Stop Code'',' + 'Diary AS ''Diary'',' + 
		'EndorseInProgress AS ''Avenanten suspens'',' + 'SurveyType AS ''Survey Type'',' + 'SurveyComplete AS ''Survey complete'',' + 
		'SurveyDate AS ''Survey Date'',' + 'MaterialExcess AS ''Franchise Matériel'',' + 'RSPMusicExcess AS ''Franchise Musique'',' + 
		'RSPExhibitExcess AS ''Franchise Expositions'','+ 'RSPArtExcess AS ''Franchise Art'',' + 'RSPGroupExcess AS ''Franchise Groupments'',' + 
		'RCOPremium AS ''RCO Premium'',' + 'RCOSI AS ''RCO SI'',' + 'RCOAverage AS ''RCO Av. Rate'',' + 'RCEPremium AS ''RCE Premium'',' + 'RCESI AS ''RCE SI'',' + 
		'RCEAverage AS ''RCE Av. Rate'',' + 'AdminFeesApply AS ''Admin fees apply'',' + 'AdminFeeExempt AS ''Admin fees never apply'',' + 
		'DueAdminFees AS ''Admin fees'',' + 'DueAdminFeesTax AS ''Admin fees tax''' + ' FROM #RES order by IdNum ASC'
		SET NOCOUNT OFF
		SET ANSI_WARNINGS ON

		EXEC (@v_stmt)
	END TRY

	BEGIN CATCH
			
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT @ErrorMessage = ERROR_MESSAGE()
			,@ErrorSeverity = ERROR_SEVERITY()
			,@ErrorState = ERROR_STATE();

		RAISERROR (
				@ErrorMessage
				,@ErrorSeverity
				,@ErrorState
				);

		PRINT 'Encountered errors!';
	END CATCH;
END;

GO


