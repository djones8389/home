USE [SQL_Inventory]
 
 
SELECT h.[hostname] as [connection]
	, s.hostID
	, s.serverID
	, s.instanceName
	,d.databaseName 
from Servers s 
INNER JOIN [dbo].[Hosts] h ON s.hostid = h.[hostID]
inner JOIN [dbo].[Databases] d ON s.serverID = d.serverID  
where not exists (
	select 1
	from Databases d
	where d.serverID = s.serverID
		and d.databaseName = 'cdr_local'
)