select name
from syscomments sc
inner join sysobjects so
on so.id = sc.id 
where sc.text like '%EgpiMovements%'
--	and  sc.text like '%--Indemnity:15, Fees:16%'

----


USE OBX_BUD_REPL

SELECT NEWID() [id]
	  ,@@ServerName [Instance]
	  ,DB_NAME(database_id) AS DBName
      ,OBJECT_SCHEMA_NAME(object_id,database_id) AS [SCHEMA_NAME] 
      ,OBJECT_NAME(object_id,database_id)AS [OBJECT_NAME]
      ,cached_time
      ,last_execution_time
      ,execution_count
      ,total_worker_time CPU
      ,total_elapsed_time ELAPSED
	  ,cast((total_elapsed_time/execution_count) as float)/1000000 [AVGELAPSED_Seconds]
      ,total_logical_reads LOGICAL_READS
      ,total_logical_writes LOGICAL_WRITES
      ,total_physical_reads PHYSICAL_READS
FROM sys.dm_exec_procedure_stats 
where database_id > 4
	and DB_NAME(database_id) = 'OBX_BUD_REPL'
	--and OBJECT_NAME(object_id,database_id) in ('sp_BI_ClaimMovement_All','sp_BI_BindersPremiumMovement_All')
	and database_id != 32767
	order by last_execution_time DESC;


--10th sept 09:49--
--sp_BUD_SynchroniseBUDSearch
--BUD_SynchronisePolicyDetails



/*
sp_BI_ClaimMovement_All
sp_BI_PolicySectionYoaPerf_All
sp_BI_BindersPremiumMovement_All
sp_BI_PolicySectionSignings_All
*/

dbcc sqlperf(logspace)

USE [OBX_BUD_REPL]
GO
CREATE NONCLUSTERED INDEX [IX_lPolicyFolderKey_lPolicyKey]
ON [dbo].[PolicySection] ([lPolicyFolderKey])
INCLUDE ([lPolicyKey])
GO

ALTER INDEX [IX_lPolicyFolderKey_lPolicyKey] on [dbo].[PolicySection] DISABLE
DROP INDEX [IX_lPolicyFolderKey_lPolicyKey] on [dbo].[PolicySection] 
