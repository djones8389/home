--USE CbtrGRPBDL_MainFC_view

--DECLARE  @CheckTable nvarchar(MAX) = '';

--select @CheckTable +=CHAR(13) 
--	+ N'
--	  DBCC CHECKTABLE('''+quotename(SCHEMA_NAME(schema_id)) + '.' + quotename(name)+''') WITH all_errormsgs, no_infomsgs
--'
--from sys.tables

--PRINT(@CheckTable);



DECLARE CheckDBCursor CURSOR FOR
SELECT QUOTENAME(SCHEMA_NAME(schema_id)) + '.' + QUOTENAME(NAME)
from sys.tables

DECLARE @Object NVARCHAR(MAX);

OPEN CheckDBCursor

FETCH NEXT FROM CheckDBCursor INTO @OBJECT

WHILE @@FETCH_STATUS = 0

BEGIN
	EXEC ('DBCC CHECKTABLE('''+@Object+''') WITH all_errormsgs, no_infomsgs')
FETCH NEXT FROM CheckDBCursor INTO @OBJECT
END
CLOSE CheckDBCursor
DEALLOCATE  CheckDBCursor
