if OBJECT_ID('tempdb..#test') is not null drop table #test;

create table #test ( 
	ServerName nvarchar(max)
	, SQLInstance nvarchar(max)
	,ProductLevel	 nvarchar(max)
	,ProductVersion nvarchar(max)
	,Edition nvarchar(max)
)

bulk insert #test
from 'C:\SQL Versions.csv'
with (fieldterminator = ',' , rowterminator = '\n', firstrow=2)


select b.*
from (
	SELECT a.ServerName
		, a.SQLVersion
		, SUBSTRING([SQL SP CU],1,charindex(')',[SQL SP CU])) [SQL SP CU]
		, case
			when SQLVersion = 'Microsoft SQL Server 2012 ' then '(SP4-GDR)' 
			when SQLVersion = 'Microsoft SQL Server 2014 ' then '(SP3-CU4)'
			when SQLVersion = 'Microsoft SQL Server 2016 ' then '(SP2-CU11)'
			when SQLVersion = 'Microsoft SQL Server 2017 ' then '(RTM-CU17)'
			 end as 'LatestVersion'
	from (
		select ServerName 
		, substring(SQLInstance,0,charindex('(',SQLInstance)) SQLVersion
		, substring(SQLInstance,charindex('(',SQLInstance),charindex(')',SQLInstance)) [SQL SP CU]
		from #test
	) a 
) b
where LatestVersion = [SQL SP CU]
	order by 2,3