
SELECT *
from sys.dm_os_performance_counters
where (
		object_name like '%Buffer%'
		and 
		counter_name in ('Page life expectancy','Database pages','Buffer cache hit ratio','Buffer cache hit ratio base')
	 )
	 OR
	 (
	 object_name like '%Memory%'
	 and
	 counter_name in ('Total Server Memory (KB)','Target Server Memory (KB)','SQL Cache Memory (KB)','Optimizer Memory (KB)','Memory Grants Outstanding','Memory Grants Pending')
	 )

