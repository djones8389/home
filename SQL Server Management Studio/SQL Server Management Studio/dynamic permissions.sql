use master;

exec sp_MSforeachdb '

use [?];

if(''?'' like ''MAG%'' and ''?'' not in (''magiclogging'',''magicperformance'',''magicaudit'', ''magconfig''))

BEGIN
	
	if not exists (select 1 from sys.database_principals where name = ''HISCOX\App_MagicNet_Staging'')
		CREATE USER [HISCOX\App_MagicNet_Staging] FOR LOGIN [HISCOX\App_MagicNet_Staging]
	if not exists (select 1 from sys.database_principals where name = ''HISCOX\MagicSVC_Staging'')
		CREATE USER [HISCOX\MagicSVC_Staging] FOR LOGIN [HISCOX\App_MagicNet_Staging]

	ALTER ROLE [MagicAppRole] ADD MEMBER [HISCOX\App_MagicNet_Staging];
	ALTER ROLE [MagicAppRole] ADD MEMBER [HISCOX\MagicSVC_Staging];
END
'