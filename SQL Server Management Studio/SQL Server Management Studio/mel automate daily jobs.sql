SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

USE IDIT_PROD_MI;

DECLARE @LatestUpdatedP_POLICYRecord datetime = (select max(update_date) as 'Latest Updated P_POLICY Record' from P_POLICY)
DECLARE @LatestUpdatedAC_TRANSACTIONRecord datetime = (select max(update_date) as 'Latest Updated AC_TRANSACTION Record' from AC_TRANSACTION)
 
declare @html nvarchar(max) = '

<report>
<body>
<table>
<tr>
<th bgcolor ="#b3b300">Latest Updated P_POLICY Record</th> 
<th bgcolor ="#b3b300">Latest Updated AC_TRANSACTION Record</th>
</tr>
<tr>
 <td>'+(Select CONVERT(varchar, @LatestUpdatedP_POLICYRecord, 121))+'</td>
 <td>'+(Select CONVERT(varchar, @LatestUpdatedAC_TRANSACTIONRecord,121))+'</td>
</tr>
</table>
</body>
</report>'

EXEC msdb.dbo.sp_send_dbmail  
	@profile_name = 'SMTP'
	,@recipients  = 'dave.jones@hiscox.com;melanie.sweeting@hiscox.com'      
	,@subject     = 'Mel daily checks - automated!'
	,@body = @html 
	,@body_format = 'HTML'
	 