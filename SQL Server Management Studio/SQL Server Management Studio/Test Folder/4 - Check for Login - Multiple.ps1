﻿Clear-Host;

#Set relative location 

    $Location = split-path -parent $MyInvocation.MyCommand.Definition;
    $storeErrors = ($Location + "\Errors");

    Set-Location $Location;

#Import my modules:  Connect-SQL, Check-DBALogin, Store-Output

    GCI "$Location\Library" | ?{$_.Extension -eq '.ps1'} | foreach {
        Import-Module $_.FullName
    };

#Connect to the source which contains the SQL instances that I want to query

    $CDR ="hxp20203\dba01";
    $Repo = (Connect-Sql -instance $CDR -windowsAuthentication 1);

#The login I am interested in

    $DBALogin = "HISCOX\zSQLAdmin"          #Is HISCOX\zSQLAdmin there?
    
#Drop Errors folder (if exists) and create one

    if(Get-ChildItem | ?{$_.Name -eq "Errors"}) {
        Remove-Item -Path ($Location + "\Errors") -Recurse;
        New-Item -Path $Location -Name "Errors" -ItemType "Directory" | Out-Null;
    } else {
        New-Item -Path $Location -Name "Errors" -ItemType "Directory" | Out-Null;
    }


#Get SQL instances to report on
    
    $Instances =@();

    $GetInstances =  $Repo.Databases['SQL_Inventory'].ExecuteWithResults("select top 10 SQLServerID, case when [SQL Instance] = 'MSSQLSERVER' THEN [SQL Cluster Name] else [SQL Cluster Name] + '\' + [SQL Instance] end as InstanceName, Version, Edition from vw_CDRPortal_InstanceList where Environment = 'Production' and commissionedState = 'Commissioned' and [SQL Cluster Name] not like '%.hsx.%' and [SQL Cluster Name] not like '%nonprod%' order by [SQL Cluster Name], [SQL Instance]").Tables;
      
    $GetInstances | foreach {
        $Instances += $_.Rows
    }

#Start Loop

    $i = 1;         #We're going to iterate this number per SQL instance
    $array=@();     #Empty array, which we're going to populate

    $Instances | ForEach-Object {        

                $connectionString = $_.InstanceName;

            #Try to connect

                $connect = (Connect-SQL -instance $connectionString -windowsAuthentication 1);                
                $ErrorActionPreference = 'SilentlyContinue';

            #If it's not possible to connect, write it to a file   

                if(!$connect.Status) {    
                    ($connectionString) | Out-File $storeErrors\CouldNotConnect.txt -Append        
                }                
                else {
                    
                    "Processed $($i) out of $($Instances.Count)";
                    
                    #Now that we are connected, lets try find that login on that SQL Instance
            
                    $array+=(Check-DBALogin -connect $connect -DBALogin $DBALogin);
                    
                    $i++;
                }
    }  

    $array #| Select-Object Server, Message

    