USE MASTER;

IF OBJECT_ID ('tempdb..#indexes') IS NOT NULL 
	DROP TABLE #indexes;

CREATE TABLE #indexes (
	DbName sysname
	, TableName nvarchar(100)
	, IndexName nvarchar(100)
	, RowsCount int
	, avg_fragmentation_in_percent decimal (10,2)
	, page_count int
	, statement nvarchar(500)
)

INSERT #indexes
EXEC sp_MSforeachdb '

use [?];

if(''?'' not in (''msdb'', ''tempdb'', ''master'',''model''))

SELECT
	 ''?''
	, B.name AS TableName
	, C.name AS IndexName
	, D.rows AS RowsCount
	, A.avg_fragmentation_in_percent
	, A.page_count
	,''use ''+ quotename(''?'') +''; ALTER INDEX '' + quotename(C.name) + '' ON '' + quotename(SCHEMA_NAME(SCHEMA_ID)) + ''.'' + quotename(B.name) + '' REORGANIZE;''
FROM sys.dm_db_index_physical_stats(DB_ID(),NULL,NULL,NULL,NULL) A
INNER JOIN sys.objects B
ON A.object_id = B.object_id
INNER JOIN sys.indexes C
ON B.object_id = C.object_id AND A.index_id = C.index_id
INNER JOIN sys.partitions D
ON B.object_id = D.object_id AND A.index_id = D.index_id
WHERE C.index_id > 0
	and A.avg_fragmentation_in_percent > 5
'

--1767 before reorganise indexes
--997 after reorganise indexes

SELECT *
FROM #indexes
WHERE avg_fragmentation_in_percent > 10
ORDER BY avg_fragmentation_in_percent DESC;