 SELECT CASE CAST(p.state AS VARCHAR(100)) WHEN 'D' THEN 'DENY' WHEN 'R' THEN 'REVOKE' WHEN 'G' THEN 'GRANT' WHEN 'W' THEN 'GRANT' END
 ,    CASE CAST(p.state AS VARCHAR(100)) WHEN 'W' THEN 'WITH GRANT OPTION' ELSE '' END
 , CAST(p.permission_name AS VARCHAR(100)), RTRIM(p.class_desc),
    (SELECT [name] FROM sys.server_principals WHERE principal_id = p.grantor_principal_id), CAST(l.name AS VARCHAR(100))
    FROM sys.server_permissions p JOIN sys.server_principals l 
    ON p.grantee_principal_id = l.principal_id
    AND l.is_disabled = 0
    AND l.type IN ('S', 'U', 'G', 'R')
	and  CAST(p.permission_name AS VARCHAR(100)) = 'VIEW SERVER STATE'