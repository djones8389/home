--new server

--BACKUP DATABASE [prodalpha-integration] 
--TO  DISK = N'E:\SQL_Backups\prodalpha-integration_2019-08-11.bak' 
--	WITH  COPY_ONLY, FORMAT,  NAME = N'prodalpha-integration-Full Database Backup', COMPRESSION,  STATS = 10;

--E:\SQL_Backups\bborange_prodalpha-integration_2019-08-11.bak

--ALTER AVAILABILITY GROUP availabilitygroup REMOVE DATABASE [prodalpha-integration];

--declare  @dynamic nvarchar(max) = '';

--select @dynamic += CHAR(13) +
--		'kill ' + cast(spid as char(10))
--       from sys.sysprocesses
--where db_name(dbid) = 'prodalpha-integration'

--exec(@dynamic);

--RESTORE DATABASE [prodalpha-integration] 
--FROM DISK = N'E:\SQL_Backups\bborange_prodalpha-integration_2019-08-11.bak' 
--WITH FILE = 1, NOUNLOAD, NOREWIND, REPLACE, STATS = 10
--, MOVE 'Integration' TO N'E:\SQL_Databases\prodalpha-integration.mdf'
--, MOVE 'Integration_log' TO N'T:\SQL_Logs\prodalpha-integration.ldf'; 






--original primary box

--BACKUP DATABASE [prodalpha-integration] TO  DISK = N'E:\SQL_Backups\prodalpha-integration_2019-08-11.bak' 
--	WITH  COPY_ONLY, FORMAT,  NAME = N'prodalpha-integration-Full Database Backup', COMPRESSION,  STATS = 10;

--ALTER AVAILABILITY GROUP availabilitygroup FORCE_FAILOVER_ALLOW_DATA_LOSS;  

