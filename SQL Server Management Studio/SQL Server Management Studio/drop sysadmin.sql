DECLARE @DropSysAdmin nvarchar(MAX)='';

SELECT @DropSysAdmin+=CHAR(13) 
	+  'ALTER SERVER ROLE [sysadmin] DROP MEMBER '+QUOTENAME(name)+';'
FROM     master.sys.server_principals 
WHERE    IS_SRVROLEMEMBER ('sysadmin',name) = 1
  and name not in ('HISCOX\SQL_DBA_Admin','HISCOX\zSQLAdmin','sa','dbs')
  and SUBSTRING(name, 0,11) != 'NT SERVICE'

EXEC(@DropSysAdmin)