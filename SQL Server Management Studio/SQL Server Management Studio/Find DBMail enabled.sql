/****** Script for SelectTopNRows command from SSMS  ******/
SELECT h.hostName
      , s.instanceName 
	  --, a.[ServerID]
      ,[Database_Mail_XPs_enabled]
  FROM [SQL_Inventory].[dbo].[Extras_Instance_OA_checks] A
  inner join Servers s  
  on s.serverID = a.ServerID
  inner join Hosts h
  on h.hostID = s.hostID
where [Database_Mail_XPs_enabled] = 1
order by 1;