
SELECT DP1.name AS DatabaseRoleName,   
   isnull (DP2.name, 'No members') AS DatabaseUserName  
   ,'if not exists (select 1 from  sys.database_principals where name  = '''+DP2.name+''')  
	'+case when DP2.name not like '%@%'  
		  then 'create user '+quotename(DP2.name)+' FROM EXTERNAL PROVIDER' +'  alter role ' + DP1.name + ' add member ' +  quotename(isnull (DP2.name, 'No members'))	
		  else 'create user '+quotename(DP2.name)+'' +'  alter role ' + DP1.name + ' add member ' +  quotename(isnull (DP2.name, 'No members'))	
		end as [Grant Access]
 FROM sys.database_role_members AS DRM  
 RIGHT OUTER JOIN sys.database_principals AS DP1  
   ON DRM.role_principal_id = DP1.principal_id  
 LEFT OUTER JOIN sys.database_principals AS DP2  
   ON DRM.member_principal_id = DP2.principal_id  
WHERE DP1.type = 'R'
	and isnull (DP2.name, 'No members')  != 'No members'
ORDER BY DP1.name;


drop user  [Nancy.Goyal@HISCOX.com]
drop user  [Suman.Ghosh@HISCOX.com]
drop user  [sailendra.suda@Hiscox.com]

--drop user [dev_xdw_db_owner]
--create user [dev_xdw_db_owner] from external provider
--alter role db_owner add member [dev_xdw_db_owner]

--alter role db_datareader drop member [Suman.Ghosh@HISCOX.com]
--alter role db_datareader drop member [Nancy.Goyal@HISCOX.com]
--alter role db_datareader drop member [sailendra.suda@Hiscox.com]
--alter role db_datareader drop member [Apoorva.Nigam@HISCOX.com]


--alter role db_datawriter drop member [Suman.Ghosh@HISCOX.com]
--alter role db_datawriter drop member [Nancy.Goyal@HISCOX.com]
--alter role db_datawriter drop member [sailendra.suda@Hiscox.com]
--alter role db_datawriter drop member [Apoorva.Nigam@HISCOX.com]


drop user dev_fcp_xdw_db_reader_mi
drop user dev_fcp_xdw_db_reader
drop user dev_fcp_xdw_db_writer_mi
drop user dev_fcp_xdw_db_writer
drop user dev_fcp_xdw_db_owner_mi

if not exists (select 1 from  sys.database_principals where name  = 'dev_xdw_db_reader_mi')     create user [dev_xdw_db_reader_mi] FROM EXTERNAL PROVIDER  alter role db_datareader add member [dev_xdw_db_reader_mi]
if not exists (select 1 from  sys.database_principals where name  = 'dev_xdw_db_reader')     create user [dev_xdw_db_reader] FROM EXTERNAL PROVIDER  alter role db_datareader add member [dev_xdw_db_reader]
if not exists (select 1 from  sys.database_principals where name  = 'dev_xdw_db_writer_mi')     create user [dev_xdw_db_writer_mi] FROM EXTERNAL PROVIDER  alter role db_datawriter add member [dev_xdw_db_writer_mi]
if not exists (select 1 from  sys.database_principals where name  = 'dev_xdw_db_writer')     create user [dev_xdw_db_writer] FROM EXTERNAL PROVIDER  alter role db_datawriter add member [dev_xdw_db_writer]
--if not exists (select 1 from  sys.database_principals where name  = 'dbo')     create user [dbo] FROM EXTERNAL PROVIDER  alter role db_owner add member [dbo]
--if not exists (select 1 from  sys.database_principals where name  = 'migration')     create user [migration] FROM EXTERNAL PROVIDER  alter role db_owner add member [migration]
--if not exists (select 1 from  sys.database_principals where name  = 'svc_grpdt_tformsql@hiscox.com')     create user [svc_grpdt_tformsql@hiscox.com]  alter role db_owner add member [svc_grpdt_tformsql@hiscox.com]
--if not exists (select 1 from  sys.database_principals where name  = 'dev_xdw_db_owner')     create user [dev_xdw_db_owner] FROM EXTERNAL PROVIDER  alter role db_owner add member [dev_xdw_db_owner]
if not exists (select 1 from  sys.database_principals where name  = 'dev_xdw_db_owner_mi')     create user [dev_xdw_db_owner_mi] FROM EXTERNAL PROVIDER  alter role db_owner add member [dev_xdw_db_owner_mi]