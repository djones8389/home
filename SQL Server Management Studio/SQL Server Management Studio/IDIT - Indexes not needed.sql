use IDIT

if OBJECT_ID('tempdb..#IndexMetrics')is not null drop table #IndexMetrics;

CREATE TABLE #IndexMetrics (
	[ID] [uniqueidentifier] NOT NULL,
	[Instance] [nvarchar](500) NULL,
	[database] [nvarchar](500) NULL,
	[table] [nvarchar](500) NULL,
	[index] [nvarchar](500) NULL,
	[user_updates] [int] NULL,
	[last_user_update] [datetime] NULL,
	[user_seeks] [int] NULL,
	[user_scans] [int] NULL,
	[user_lookups] [int] NULL,
	[last_user_seek] [datetime] NULL,
	[last_user_scan] [datetime] NULL,
	[last_user_lookup] [datetime] NULL
)

INSERT #IndexMetrics
exec sp_MSforeachdb '

use [?];

if(db_ID() > 4)


BEGIN

	SELECT	newID() as [ID],
			@@SERVERNAME as [Instance],
			 DB_NAME() AS [database],
			 OBJECT_NAME(s.OBJECT_ID) AS [table],
			 i.name AS [index],
			 s.user_updates,
			 [last_user_update] ,
			 s.user_seeks , 
			 s.user_scans, 
			 s.user_lookups
			 ,  [last_user_seek]
			 ,  [last_user_scan]
			 ,  [last_user_lookup]
	 FROM		sys.dm_db_index_usage_stats AS s WITH (NOLOCK)
	 INNER JOIN sys.indexes AS i WITH (NOLOCK)
			 ON s.[object_id] = i.[object_id]
			AND i.index_id = s.index_id
	WHERE	 OBJECTPROPERTY(s.[OBJECT_ID],''IsUserTable'') = 1
	 AND	 i.index_id > 1
 	 and     s.database_id = DB_ID()
	ORDER BY 1,2
	OPTION	 (RECOMPILE);

END

'

select [database]	
	, [table]
	, [index]
	, user_updates
	, last_user_update
	, user_seeks
	, user_scans
	, user_lookups
	, 'IF EXISTS (SELECT [object_id] FROM sys.indexes WHERE object_name(object_id) = '''+[table]+''' AND [name] = '''+[index]+''') 
		DROP INDEX '+quotename([index])+' ON dbo.'+quotename([table])+';' [Drop Statement]
from #IndexMetrics a 
where [database] = 'idit'
	 and [table] = 'AC_ACCOUNT_TRANSACTION'
order by (user_lookups + user_scans + user_seeks) asc, user_updates desc;



/*



;with indexescte as (
select top 90 [index]
from #IndexMetrics a 
where [database] = 'idit'
	--and [table] = 'ac_entry'
order by (user_lookups + user_scans + user_seeks) asc, user_updates desc
 
)
  
SELECT 
	 sum(cast(LTRIM (STR ((CASE WHEN usedpages > pages THEN (usedpages - pages) ELSE 0 END) * 8, 15, 0)) as int)) [index_size-kb]
	 ,sum(cast(LTRIM (STR ((CASE WHEN usedpages > pages THEN (usedpages - pages) ELSE 0 END) * 8, 15, 0)) as int))/1024 [index_size-mb]
	 ,sum(cast(LTRIM (STR ((CASE WHEN usedpages > pages THEN (usedpages - pages) ELSE 0 END) * 8, 15, 0)) as int))/1024/1024 [index_size-gb]
	 --,a.[name]
FROM (

	SELECT 
		OBJECT_NAME(s.object_id) [TableName]
		, (used_page_count) [usedpages]
		, (
			CASE
				WHEN (s.index_id < 2) THEN (in_row_data_page_count + lob_used_page_count + row_overflow_used_page_count)
				ELSE lob_used_page_count + row_overflow_used_page_count
			END
		) [pages]
		, i.name
	FROM sys.dm_db_partition_stats AS s
		INNER JOIN sys.indexes AS i
			ON s.[object_id] = i.[object_id]
			AND s.[index_id] = i.[index_id]
		INNER JOIN sys.tables t
		on t.object_id = s.object_id
		
	 where i.name IN (select [index] from indexescte)
) A

--group by a.[name]
order by [index_size-kb] DESC


*/