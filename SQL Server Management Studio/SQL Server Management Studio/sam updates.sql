USE IDIT;

/**Step 1**/

UPDATE T_ASSET_RISK_Tmp
SET UPDATE_DATE = getdate();

/**Step 2**/

TRUNCATE TABLE T_ASSET_RISK;

/**Step 3**/

INSERT T_ASSET_RISK
select *
from T_ASSET_RISK_Tmp


/*  done
update PostcodeFile1
set update_date = replace(update_date,'"','');

update PostcodeFile2
set update_date = replace(update_date,'"','');

update PostcodeFile3
set update_date = replace(update_date,'"','');

update PostcodeFile4
set update_date = replace(update_date,'"','');

INSERT T_ASSET_RISK_Tmp
select * from PostcodeFile1
union
select * from PostcodeFile2
union
select * from PostcodeFile3
union
select * from PostcodeFile4
*/


/*
use IDIT

select'drop table ' + quotename(name) + ';'
from sys.tables
where name like 'postcode%'

drop table [PostcodeFile1];
drop table [PostcodeFile2];
drop table [PostcodeFile3];
drop table [PostcodeFile4];

*/


 --drop table #test

--create table T_ASSET_RISK_Tmp ( 

--	[ID] [numeric](12, 0) NOT NULL,
--	[PRODUCT_ID] [numeric](12, 0) NOT NULL,
--	[RISK_TYPE_ID] [numeric](12, 0) NOT NULL,
--	[KEY_VALUE] [varchar](255) NOT NULL,
--	[RISK_VALUE] [numeric](20, 8) NOT NULL,
--	[UPDATE_USER] [numeric](12, 0) NOT NULL,
--	[UPDATE_DATE] nvarchar(max) NOT NULL,
--	[UPDATE_VERSION] [numeric](12, 0) NOT NULL,
--	[RESERVED_VALUE_FLAG] [numeric](12, 0) NOT NULL,
--	[IS_DEFAULT] [numeric](12, 0) NOT NULL,
--	[EXTERNAL_CODE] [varchar](20) NULL,
--	[DISCONTINUE_DATE] [datetime] NULL,
--	[DEVELOPER_DESC] [varchar](4000) NULL,
--	[SORT_ORDER] [numeric](12, 0) NULL
--)

--bulk insert #test
--from 'T:\SQL_Backups\PostcodeFile1.csv'
--with (fieldterminator = 'r' , FIRSTROW = 2, rowterminator = '\n')

--select * from #test
