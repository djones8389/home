use CDR_local

SELECT A.Table_name
      ,(b.[data_kb] - a.[data_kb])/1024 [DataDiff_MB]
      ,(b.[index_kb] - a.[index_kb])/1024 [IndexDiff_MB]
	  ,((b.[data_kb] - a.[data_kb])/1024) + ((b.[index_kb] - a.[index_kb])/1024) [Data & Index Combined]
FROM (
		SELECT [table_name]
		  ,[data_kb]
		  ,[index_kb]
		  ,[collectionDate]
	FROM [CDR_local].[dbo].[ClientTableMetrics]
	where cast([collectionDate] as date) = '2019-11-25'
	) A
INNER JOIN (
	SELECT [table_name]
		  ,[data_kb]
		  ,[index_kb]
		  ,[collectionDate]
	FROM [CDR_local].[dbo].[ClientTableMetrics]
	where cast([collectionDate] as date) = '2019-12-01'
) B
ON A.table_name = B.table_name
where ((b.[data_kb] - a.[data_kb])/1024) + ((b.[index_kb] - a.[index_kb])/1024)> 0
 order by 4 desc