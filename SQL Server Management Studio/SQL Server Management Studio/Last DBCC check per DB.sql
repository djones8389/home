use master;

if OBJECT_ID('tempdb..##table') is not null drop table ##table;

create table ##table (
	ParentObj nvarchar(max)
	,Obj nvarchar(max)
	,Field nvarchar(max)
	,[Value] nvarchar(max)
	, DBName sysname null
);

exec sp_MSforeachdb '

use [?];

begin 

	insert ##table(ParentObj,Obj,Field,[Value])
	exec(''DBCC DBINFO(''''?'''') WITH TABLERESULTS'')

	update ##table
	set dbName = ''?''
	where dbName is null

end

'

select DBName
	, Value
from ##table
where Field= 'dbi_dbccLastKnownGood'
order by 2 asc;

--dbcc checkdb([Grp_IT_Azure_Costs]) with estimateonly  218702692 = 208.57 GB