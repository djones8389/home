use master



select 'use '+quotename(name)+' if exists(select 1 from sys.database_principals where name = ''RServices_Staging'')
	DROP USER [RServices_Staging];'
+  'use '+quotename(name)+' if exists(select 1 from sys.database_principals where name = ''RServices_Prod'')
	DROP USER [RServices_Prod];' [DropUsers]
	, 'use '+quotename(name)+'	if not exists(select 1 from sys.database_principals where name = ''RServices_Staging'')
			CREATE USER [RServices_Staging] FOR LOGIN [RServices_Staging];
			ALTER ROLE [db_datareader] ADD MEMBER [RServices_Staging];'
		+ 'use '+quotename(name)+' if not exists(select 1 from sys.database_principals where name = ''RServices_Prod'')
			CREATE USER [RServices_Prod] FOR LOGIN [RServices_Prod];
			ALTER ROLE [db_datareader] ADD MEMBER [RServices_Prod];' [AddUsers]
from sys.databases
where database_id > 4
	and name not in ('HISSQLCMR','cdr_local')

--Drop users
/* 
exec sp_MSforeachdb '

use [?]

if (''?'' in (select name
from sys.databases
where database_id > 4
	and name not in (''HISSQLCMR'',''cdr_local'')))

	begin
		
		if exists(select 1 from sys.database_principals where name = ''RServices_Staging'')
			DROP USER [RServices_Staging]
			
		if exists(select 1 from sys.database_principals where name = ''RServices_Prod'')
			DROP USER [RServices_Prod]
'
 
 

--Add users

exec sp_MSforeachdb '

use [?]

if (''?'' in (select name
from sys.databases
where database_id > 4
	and name not in (''HISSQLCMR'',''cdr_local'')))

	begin
		
		if not exists(select 1 from sys.database_principals where name = ''RServices_Staging'')
			CREATE USER [RServices_Staging] FOR LOGIN [RServices_Staging];
			ALTER ROLE [db_datareader] ADD MEMBER [RServices_Staging];
		if not exists(select 1 from sys.database_principals where name = ''RServices_Prod'')
			CREATE USER [RServices_Prod] FOR LOGIN [RServices_Prod];
			ALTER ROLE [db_datareader] ADD MEMBER [RServices_Prod];
	end

' 

*/