--hxp33446

BACKUP DATABASE [EDWMart] TO DISK =
N'\\hiscox.com\backup\ReleaseDump\FTP\EDWMart.bak' WITH copy_only, noformat
, noinit, NAME = N'EDWMart-Full Database Backup', skip, norewind, nounload,
compression, stats = 1
GO

 BACKUP DATABASE [ManagementInterfaceFeeds] TO DISK =
N'\\hiscox.com\backup\ReleaseDump\FTP\ManagementInterfaceFeeds.bak' WITH copy_only, noformat
, noinit, NAME = N'ManagementInterfaceFeeds-Full Database Backup', skip, norewind, nounload,
compression, stats = 1
GO




--BACKUP DATABASE [CoreMart] TO DISK =
--N'\\hiscox.com\backup\ReleaseDump\FTP\CoreMart.bak' WITH copy_only, noformat
--, noinit, NAME = N'CoreMart-Full Database Backup', skip, norewind, nounload,
--compression, stats = 1
--GO

--BACKUP DATABASE [EDW] TO DISK =
--N'\\hiscox.com\backup\ReleaseDump\FTP\EDW.bak' WITH copy_only, noformat
--, noinit, NAME = N'EDW-Full Database Backup', skip, norewind, nounload,
--compression, stats = 1
--GO

--BACKUP DATABASE [Mirror] TO DISK =
--N'\\hiscox.com\backup\ReleaseDump\FTP\Mirror.bak' WITH copy_only, noformat
--, noinit, NAME = N'Mirror-Full Database Backup', skip, norewind, nounload,
--compression, stats = 1
--GO