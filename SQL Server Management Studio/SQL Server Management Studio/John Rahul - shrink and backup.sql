USE [IDIT_Obfuscated]; 
--started shrink at 12:20 on 12.12.2019
--Shrink

	DBCC SHRINKFILE ('HSCX_DEV',1571401);  /* Space we are creating:  1060786.00 MB (Shrinking from: 2632187.00 to: 1571401)*/

--Maintenance

	DECLARE @IDIT_Obfuscated nvarchar(MAX) = '';

	SELECT @IDIT_Obfuscated +=CHAR(13) +
			'ALTER INDEX ' + quotename(C.name) +  ' ON ' + quotename(SCHEMA_NAME(SCHEMA_ID)) + '.' + quotename(B.name) 
				+ CASE
					WHEN avg_fragmentation_in_percent BETWEEN 5 AND 30 THEN ' REORGANIZE'
					ELSE ' REBUILD'
				END + ';'
	FROM sys.dm_db_index_physical_stats(DB_ID(),NULL,NULL,NULL,NULL) A
	INNER JOIN sys.objects B
	ON A.object_id = B.object_id
	INNER JOIN sys.indexes C
	ON B.object_id = C.object_id AND A.index_id = C.index_id
	INNER JOIN sys.partitions D
	ON B.object_id = D.object_id AND A.index_id = D.index_id
	WHERE C.index_id > 0
		and A.avg_fragmentation_in_percent > 30

	EXEC(@IDIT_Obfuscated);

	EXEC sp_updatestats;
	 

--Backup & email

	if exists (
	SELECT 1
	FROM sys.databases db    
	LEFT OUTER JOIN sys.dm_database_encryption_keys dm        
	ON db.database_id = dm.database_id
	where name = 'IDIT_Obfuscated'
		and is_encrypted = 0
	)
	BEGIN

	 
		DECLARE @Body nvarchar(MAX)='';

		BACKUP DATABASE [IDIT_Obfuscated]
		TO URL = 'https://devtestdbbackups.blob.core.windows.net/test/IDIT_Obfuscated_22_11_2019.bak'
		WITH credential = 'devtestdbbackups', copy_only, init, compression

		SELECT @Body = 'Database is now available at:  https://devtestdbbackups.blob.core.windows.net/test/IDIT_Obfuscated_22_11_2019.bak'
	
		EXECUTE [msdb].[dbo].[sp_send_dbmail]
   			 @profile_name = 'SMTP'
			,@recipients  = 'JohnRahul.Prajeeth@HISCOX.com;dave.jones@hiscox.com;'
   			,@body        =  @Body
			,@subject     = 'IDIT_Live_Supp - Post-Deployment'	
	END
 