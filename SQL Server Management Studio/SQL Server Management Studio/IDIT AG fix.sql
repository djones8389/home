-- alter availability group availabilitygroup remove database idit
-- go

declare  @dynamic nvarchar(max) = '';

select @dynamic += CHAR(13) +
		'kill ' + cast(spid as char(10))
       from sys.sysprocesses
where db_name(dbid) = 'idit'

exec(@dynamic);
go

drop database idit;
 