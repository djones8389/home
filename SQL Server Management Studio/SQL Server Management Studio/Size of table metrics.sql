use master

IF OBJECT_ID('tempdb..##ClientTableMetrics') IS NOT NULL DROP TABLE ##ClientTableMetrics;

CREATE TABLE ##ClientTableMetrics (
	
	ID uniqueIdentifier
	,server_name nvarchar(100)
	, database_name nvarchar(100)
	, database_id int
	, table_name nvarchar(100)
	, rows int
	, reserved_kb nvarchar(20)
	, data_kb nvarchar(20)
	, index_size_kb nvarchar(20)
	, unused_kb nvarchar(20)
);

exec sp_MSforeachdb '

USE [?];

if (''?'' in (select name from sys.databases where name = ''ManagementInterfaceFeeds''))

BEGIN

	declare @dynamic nvarchar(MAX) = '''';

	select @dynamic +=CHAR(13) +
		''exec sp_spaceused @objname = ''''''+s.name+''.''+t.name + '''''' ''
	from sys.tables T
	inner join sys.schemas S
	on s.schema_id = t.schema_id
	where type = ''u''
	order by t.name;

	INSERT ##ClientTableMetrics (table_name, [rows], reserved_kb, data_kb, index_size_kb, unused_kb)
	EXEC(@dynamic);

	UPDATE ##ClientTableMetrics
	SET database_id = db_id()
	where database_id IS NULL;

	UPDATE ##ClientTableMetrics
	SET server_name = @@SERVERNAME
		, database_name = DB_Name()
	where db_id() = database_id;
END
'

SELECT newID()
	,server_name
	, database_name
	, table_name
	, rows
	, cast(replace(data_kb,'kb','') as int)/1024  data_mb
	, cast(replace(index_size_kb ,'kb','') as int)/1024  index_size_mb
	, 'select * into [' + table_name + '_tmp] from '  + QUOTENAME(table_name)
FROM ##ClientTableMetrics
where database_name = 'ManagementInterfaceFeeds'
	and table_name in ('InterfaceFeedCurrent.LM_Quota_Share_Cession_Statement','InterfaceFeedCurrent.LM_Policy_Level_Accounts_Feed'
				,'InterfaceFeedHistory.LM_Quota_Share_Cession_Statement_Monthly','InterfaceFeedHistory.LM_Policy_Level_Accounts_Feed_Monthly')
				 
--USE ManagementInterfaceFeeds			 

--select * 
--into [copydata].[InterfaceFeedCurrent].[LM_Policy_Level_Accounts_Feed_tmp] 
--from [InterfaceFeedCurrent].[LM_Policy_Level_Accounts_Feed]

--select * 
--into [copydata].[InterfaceFeedHistory].[LM_Policy_Level_Accounts_Feed_Monthly_tmp] 
--from [InterfaceFeedHistory].[LM_Policy_Level_Accounts_Feed_Monthly]

--select * 
--into [copydata].[InterfaceFeedCurrent].[LM_Quota_Share_Cession_Statement_tmp] 
--from [InterfaceFeedCurrent].[LM_Quota_Share_Cession_Statement]

--select * 
--into [copydata].[InterfaceFeedHistory].[LM_Quota_Share_Cession_Statement_Monthly_tmp] 
--from [InterfaceFeedHistory].[LM_Quota_Share_Cession_Statement_Monthly]
 