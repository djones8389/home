USE [msdb]
GO

/****** Object:  Job [IDIT - IDIT_Obfuscated Restore]    Script Date: 22/08/2019 13:33:33 ******/
BEGIN TRANSACTION
DECLARE @ReturnCode INT
SELECT @ReturnCode = 0
/****** Object:  JobCategory [Database Maintenance]    Script Date: 22/08/2019 13:33:34 ******/
IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'Database Maintenance' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'Database Maintenance'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=N'IDIT - IDIT_Obfuscated Restore', 
		@enabled=1, 
		@notify_level_eventlog=0, 
		@notify_level_email=2, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description=N'Restore a native backup from production-idit to IDIT_Obfuscated', 
		@category_name=N'Database Maintenance', 
		@owner_login_name=N'dbs', 
		@notify_email_operator_name=N'PitStopDBA', @job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Set offline]    Script Date: 22/08/2019 13:33:34 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Set offline', 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=3, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'alter database [IDIT_Obfuscated] set offline with rollback immediate;', 
		@database_name=N'master', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Restore IDIT_Obfuscated]    Script Date: 22/08/2019 13:33:34 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Restore IDIT_Obfuscated', 
		@step_id=2, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=1, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'RESTORE DATABASE IDIT_Obfuscated FROM DISK = ''\\pr03039lueoh-00\LiveSupp\IDIT.bak''
WITH MOVE ''HSCX_DEV'' TO ''E:\SQL_Databases\IDIT_Obfuscated.mdf'',    
MOVE ''HSCX_DEV_log'' TO ''E:\SQL_Logs\IDIT_Obfuscated_log.ldf'', 
REPLACE, RECOVERY


/*
RESTORE DATABASE IDIT_Obfuscated FROM DISK = ''T:\SQL_Backups\LS\IDIT.bak''
WITH MOVE ''HSCX_DEV'' TO ''E:\SQL_Databases\IDIT_Obfuscated.mdf'',    
MOVE ''HSCX_DEV_log'' TO ''E:\SQL_Logs\IDIT_Obfuscated_log.ldf'', 
REPLACE, RECOVERY, stats = 1;
*/
', 
		@database_name=N'master', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Disable CDC components]    Script Date: 22/08/2019 13:33:34 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Disable CDC components', 
		@step_id=3, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=3, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'exec sys.sp_cdc_disable_db;', 
		@database_name=N'IDIT_Obfuscated', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Initiate decryption]    Script Date: 22/08/2019 13:33:34 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Initiate decryption', 
		@step_id=4, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'ALTER DATABASE IDIT_Obfuscated SET ENCRYPTION OFF;', 
		@database_name=N'IDIT_Obfuscated', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Set to simple recovery]    Script Date: 22/08/2019 13:33:34 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Set to simple recovery', 
		@step_id=5, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'ALTER DATABASE IDIT_Obfuscated SET RECOVERY SIMPLE; ', 
		@database_name=N'master', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Truncate tables]    Script Date: 22/08/2019 13:33:34 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Truncate tables', 
		@step_id=6, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'declare @truncate nvarchar(max)='''';

select @truncate+=CHAR(13) +
	''if exists (select 1 from sys.tables where name = ''''''+name+'''''') truncate table ''+QUOTENAME(name)+'';''
from sys.tables
where name in (''ST_GENERAL_LOG'',''P_COVER_CALC_STEP_BACKUP'',''ST_USER_MNG_LOG'',''ST_SERVICE_AUDIT_BLOB'',''ST_SERVICE_AUDIT'',''ST_TASK_LOG'')

exec(@truncate);', 
		@database_name=N'IDIT_Obfuscated', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Obfuscate IDIT_Obfuscated]    Script Date: 22/08/2019 13:33:34 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Obfuscate IDIT_Obfuscated', 
		@step_id=7, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'exec usp_ObfuscateIditData_new', 
		@database_name=N'ObfuscationControl', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [9 hr delay]    Script Date: 22/08/2019 13:33:34 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'9 hr delay', 
		@step_id=8, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'--Failed at 5 and 7 hrs. Changing to 9

WAITFOR DELAY ''09:00''', 
		@database_name=N'master', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Drop encryption key]    Script Date: 22/08/2019 13:33:34 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Drop encryption key', 
		@step_id=9, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'DROP DATABASE ENCRYPTION KEY ', 
		@database_name=N'IDIT_Obfuscated', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Set to full recovery]    Script Date: 22/08/2019 13:33:34 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Set to full recovery', 
		@step_id=10, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=3, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'ALTER DATABASE [IDIT_Obfuscated] SET RECOVERY FULL WITH NO_WAIT;', 
		@database_name=N'master', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Backup - 1 file - for Hiscox]    Script Date: 22/08/2019 13:33:34 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Backup - 1 file - for Hiscox', 
		@step_id=11, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=3, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'DECLARE @Body nvarchar(MAX)='''';

if exists (
SELECT 1
FROM sys.databases db    
LEFT OUTER JOIN sys.dm_database_encryption_keys dm        
ON db.database_id = dm.database_id
where name = ''IDIT_Obfuscated''
	and is_encrypted = 0
)
BEGIN

	SELECT @Body = ''Database is both obfuscated & decrypted, backing up to https://devtestdbbackups.blob.core.windows.net/test now...''

	EXECUTE [msdb].[dbo].[sp_send_dbmail]
   		 @profile_name = ''SMTP''
		,@recipients  = ''!Pitstop_Devops@HISCOX.com;dave.jones@hiscox.com; Victoria.Sims@HISCOX.com; david.livingstone@hiscox.com''
   		,@body        =  @Body
		,@subject     = ''Post-Deployment - Hiscox - Single file backup''
	
	declare @Single nvarchar(max)='''';

	select @Single+=CHAR(13) +  ''
	DECLARE @Body nvarchar(MAX)='''''''';

	BACKUP DATABASE [IDIT_Obfuscated]
	TO URL = ''''https://devtestdbbackups.blob.core.windows.net/test/IDIT_Obfuscated_''+(select cast((select cast(getDATE() as date)) as nvarchar(100)))+''.bak''''
	WITH credential = ''''devtestdbbackups'''', copy_only, init, compression

	SELECT @Body = ''''Database is now available at:  https://devtestdbbackups.blob.core.windows.net/test/IDIT_Obfuscated_''+(select cast((select cast(getDATE() as date)) as nvarchar(100)))+''.bak''''
	
	EXECUTE [msdb].[dbo].[sp_send_dbmail]
   		 @profile_name = ''''SMTP''''
		,@recipients  = ''''!Pitstop_Devops@HISCOX.com;dave.jones@hiscox.com; Victoria.Sims@HISCOX.com; david.livingstone@hiscox.com''''
   		,@body        =  @Body
		,@subject     = ''''IDIT_Live_Supp - Post-Deployment''''	
	''
	exec(@Single);
END

ELSE 
BEGIN

	SELECT @Body = ''Database is obfuscated, but not decrypted,  check back again later''

	EXECUTE [msdb].[dbo].[sp_send_dbmail]
   		@profile_name = ''SMTP''
    	,@recipients  = ''!Pitstop_Devops@HISCOX.com;dave.jones@hiscox.com; Victoria.Sims@HISCOX.com; david.livingstone@hiscox.com''
    	,@body        =  @Body
		,@subject     = ''Post-Deployment - Hiscox - Single file backup''

END', 
		@database_name=N'master', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Backup - 10 files - for Sapiens]    Script Date: 22/08/2019 13:33:34 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Backup - 10 files - for Sapiens', 
		@step_id=12, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'/*30/05/2019 - takes 3 hrs 8 mins to run*/

DECLARE @Body nvarchar(MAX)='''';

if exists (
SELECT 1
FROM sys.databases db    
LEFT OUTER JOIN sys.dm_database_encryption_keys dm        
ON db.database_id = dm.database_id
where name = ''IDIT_Obfuscated''
	and is_encrypted = 0
)
BEGIN

	SELECT @Body = ''Database is both obfuscated & decrypted, backing up to T:\SQL_Backups\LS now...''
	
	EXECUTE [msdb].[dbo].[sp_send_dbmail]
   		 @profile_name = ''SMTP''
    		,@recipients  = ''!Pitstop_Devops@HISCOX.com;dave.jones@hiscox.com; Victoria.Sims@HISCOX.com; david.livingstone@hiscox.com''
   		,@body        =  @Body
		,@subject     = ''Post-Deployment - Sapiens - Multi file backup''
	
	declare @Multi nvarchar(max)='''';

	select @Multi+=CHAR(13) + 	''

	DECLARE @Body nvarchar(max) = '''''''';

	BACKUP DATABASE [IDIT_Obfuscated] 
	TO DISK = ''''T:\SQL_Backups\LS\IDIT_Obfuscated_''+(select cast((select cast(getDATE() as date)) as nvarchar(100)))+''_1.bak''''
		, DISK = ''''T:\SQL_Backups\LS\IDIT_Obfuscated_''+(select cast((select cast(getDATE() as date)) as nvarchar(100)))+''_2.bak''''
		, DISK = ''''T:\SQL_Backups\LS\IDIT_Obfuscated_''+(select cast((select cast(getDATE() as date)) as nvarchar(100)))+''_3.bak''''
		, DISK = ''''T:\SQL_Backups\LS\IDIT_Obfuscated_''+(select cast((select cast(getDATE() as date)) as nvarchar(100)))+''_4.bak''''
		, DISK = ''''T:\SQL_Backups\LS\IDIT_Obfuscated_''+(select cast((select cast(getDATE() as date)) as nvarchar(100)))+''_5.bak''''
		, DISK = ''''T:\SQL_Backups\LS\IDIT_Obfuscated_''+(select cast((select cast(getDATE() as date)) as nvarchar(100)))+''_6.bak''''
		, DISK = ''''T:\SQL_Backups\LS\IDIT_Obfuscated_''+(select cast((select cast(getDATE() as date)) as nvarchar(100)))+''_7.bak''''
		, DISK = ''''T:\SQL_Backups\LS\IDIT_Obfuscated_''+(select cast((select cast(getDATE() as date)) as nvarchar(100)))+''_8.bak''''
		, DISK = ''''T:\SQL_Backups\LS\IDIT_Obfuscated_''+(select cast((select cast(getDATE() as date)) as nvarchar(100)))+''_9.bak''''
		, DISK = ''''T:\SQL_Backups\LS\IDIT_Obfuscated_''+(select cast((select cast(getDATE() as date)) as nvarchar(100)))+''_10.bak''''
	WITH copy_only, init, compression, MAXTRANSFERSIZE = 4194304;

	SELECT @Body = ''''Database has been backed up.  The powershell to send these files to Azure needs to run on IDIT_Live_Supp''''
	
	EXECUTE [msdb].[dbo].[sp_send_dbmail]
   		 @profile_name = ''''SMTP''''
    	,@recipients  = ''''!Pitstop_Devops@HISCOX.com;dave.jones@hiscox.com; Victoria.Sims@HISCOX.com; david.livingstone@hiscox.com''''
   		,@body        =  @Body
		,@subject     = ''''Post-Deployment - Sapiens - Multi file backup''''

	''
	exec(@Multi);


END

ELSE 
BEGIN

	SELECT @Body = ''Database is obfuscated, but not decrypted,  check back again later''

	EXECUTE [msdb].[dbo].[sp_send_dbmail]
   		@profile_name = ''SMTP''
    		,@recipients  = ''!Pitstop_Devops@HISCOX.com;dave.jones@hiscox.com; Victoria.Sims@HISCOX.com; david.livingstone@hiscox.com''
    		,@body        =  @Body
		,@subject     = ''Post-Deployment - Sapiens - Multi file backup''

END', 
		@database_name=N'master', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [NIU - Run local backup if decryption is complete]    Script Date: 22/08/2019 13:33:34 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'NIU - Run local backup if decryption is complete', 
		@step_id=13, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'DECLARE @Body nvarchar(MAX)='''';

if exists (
SELECT 1
FROM sys.databases db    
LEFT OUTER JOIN sys.dm_database_encryption_keys dm        
ON db.database_id = dm.database_id
where name = ''IDIT_Obfuscated''
	and is_encrypted = 0
)
BEGIN

	SELECT @Body = ''Database is both obfuscated & decrypted, backing up to T:\SQL_Backups\IDIT_Obfuscated.bak now...''
	
	EXECUTE [msdb].[dbo].[sp_send_dbmail]
   		 @profile_name = ''SMTP''
    		,@recipients  = ''!Pitstop_Devops@HISCOX.com;dave.jones@hiscox.com; Victoria.Sims@HISCOX.com; david.livingstone@hiscox.com''
   		,@body        =  @Body
		,@subject     = ''IDIT_Live_Supp - Post-Deployment''
	
	BACKUP DATABASE [IDIT_Obfuscated] 
	to DISK = ''T:\SQL_Backups\IDIT_Obfuscated.bak''
	WITH copy_only, init, compression, MAXTRANSFERSIZE = 4194304;
END

ELSE 
BEGIN

	SELECT @Body = ''Database is obfuscated, but not decrypted,  check back again later''

	EXECUTE [msdb].[dbo].[sp_send_dbmail]
   		@profile_name = ''SMTP''
    		,@recipients  = ''!Pitstop_Devops@HISCOX.com;dave.jones@hiscox.com; Victoria.Sims@HISCOX.com; david.livingstone@hiscox.com''
    		,@body        =  @Body
		,@subject     = ''IDIT_Live_Supp - Post-Deployment''

END', 
		@database_name=N'master', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [NIU - Run backup if decryption is also complete]    Script Date: 22/08/2019 13:33:34 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'NIU - Run backup if decryption is also complete', 
		@step_id=14, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'DECLARE @Body nvarchar(MAX)='''';

if exists (
SELECT 1
FROM sys.databases db    
LEFT OUTER JOIN sys.dm_database_encryption_keys dm        
ON db.database_id = dm.database_id
where name = ''IDIT_Obfuscated''
	and is_encrypted = 0
)
BEGIN

	SELECT @Body = ''Database is both obfuscated & decrypted, backing up to E:\SQL_Backups\''+(select cast((select cast(getDATE() as date)) as nvarchar(100)))+''.bak now...''
	
	EXECUTE [msdb].[dbo].[sp_send_dbmail]
    @profile_name = ''SMTP''
    ,@recipients  = ''!Pitstop_Devops@HISCOX.com;dave.jones@hiscox.com; Victoria.Sims@HISCOX.com; david.livingstone@hiscox.com''
    ,@body        =  @Body
	,@subject     = ''IDIT_Live_Supp - Post-Deployment''
	
	declare @backup nvarchar(max)='''';

	select @backup+=CHAR(13) + 
	''
	BACKUP DATABASE [IDIT_Obfuscated] 
	to DISK = ''''T:\SQL_Backups\IDIT_Obfuscated_''+(select cast((select cast(getDATE() as date)) as nvarchar(100)))+''.bak''''
	WITH copy_only, init, compression, MAXTRANSFERSIZE = 4194304;

	''
	exec(@backup);
END

ELSE 
BEGIN

	SELECT @Body = ''Database is obfuscated, but not decrypted,  check back again later''

	EXECUTE [msdb].[dbo].[sp_send_dbmail]
    @profile_name = ''SMTP''
    ,@recipients  = ''!Pitstop_Devops@HISCOX.com;dave.jones@hiscox.com; Victoria.Sims@HISCOX.com; david.livingstone@hiscox.com''
    ,@body        =  @Body
	,@subject     = ''IDIT_Live_Supp - Post-Deployment''

END', 
		@database_name=N'master', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [NIU - Restore IDIT_PROD_MI Restore]    Script Date: 22/08/2019 13:33:34 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'NIU - Restore IDIT_PROD_MI Restore', 
		@step_id=15, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'exec sp_start_job @job_name = ''IDIT - IDIT_PROD_MI Restore'';', 
		@database_name=N'msdb', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 9
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'Refresh IDIT_Obfuscated', 
		@enabled=0, 
		@freq_type=1, 
		@freq_interval=0, 
		@freq_subday_type=0, 
		@freq_subday_interval=0, 
		@freq_relative_interval=0, 
		@freq_recurrence_factor=0, 
		@active_start_date=20190803, 
		@active_end_date=99991231, 
		@active_start_time=150000, 
		@active_end_time=235959, 
		@schedule_uid=N'83d6dc41-9153-41cb-89ee-54f78e5fa4c4'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:
GO


