 
--RESTORE DATABASE [Sandeep_LMfeeds] FROM DISK = N'E:\SQL_SEQ_01\SQL_Backups\Sandeep_LMfeeds_2020-01-03.bak' 
--	WITH FILE = 1, NOUNLOAD, NOREWIND, REPLACE, STATS = 1
--	, MOVE 'Sandeep_LMfeeds' TO N'E:\SQL_RND_03\SQL_Databases\Sandeep_LMfeeds.mdf'
--	, MOVE 'Sandeep_LMfeeds_log' TO N'E:\SQL_SEQ_01\SQL_Logs\Sandeep_LMfeeds.ldf'; 



insert [ManagementInterfaceFeeds_221119_13].[bpu].[LM_Policy_Level_Accounts_Feed_Monthly_2]
select *
from [Sandeep_LMfeeds].[InterfaceFeedHistory].[LM_Policy_Level_Accounts_Feed_Monthly]

insert [ManagementInterfaceFeeds_221119_13].[bpu].[LM_Quota_Share_Cession_Statement_Monthly]
select *
from [Sandeep_LMfeeds].[InterfaceFeedHistory].[LM_Quota_Share_Cession_Statement_Monthly]

insert [ManagementInterfaceFeeds_221119_13].[bpu].Europe_Accounts_All_Retail_Premiums_Claims
select *
from [Sandeep_LMfeeds].InterfaceFeedCurrent.Europe_Accounts_All_Retail_Premiums_Claims

insert [ManagementInterfaceFeeds_221119_13].[bpu].UK_Accounts_All_Retail_Premiums_Claims
select *
from [Sandeep_LMfeeds].InterfaceFeedCurrent.UK_Accounts_All_Retail_Premiums_Claims

insert [ManagementInterfaceFeeds_221119_13].[bpu].US_Accounts_Feeds_HICI_Interface 
select *
from [Sandeep_LMfeeds].InterfaceFeedCurrent.US_Accounts_Feeds_HICI_Interface 
