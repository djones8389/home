CREATE TABLE IDIT.T_CYBER_DATA_PRIMARY_OPERATING_SYSTEM_HSCX (
 [ID] numeric(12, 0) NULL,
[DESCRIPTION] varchar(255) NULL,
[DSC_ID] numeric(12, 0) NULL,
[UPDATE_USER] numeric(12, 0) NULL,
[UPDATE_DATE] datetime NULL,
[UPDATE_VERSION] numeric(12, 0) NULL,
[RESERVED_VALUE_FLAG] numeric(12, 0) NULL,
[IS_DEFAULT] numeric(12, 0) NULL,
[EXTERNAL_CODE] varchar(20) NULL,
[DISCONTINUE_DATE] datetime NULL,
[DEVELOPER_DESC] varchar(4000) NULL,
[SORT_ORDER] numeric(12, 0) NULL,
[RowID] [bigint] IDENTITY(1,1) ,
[CDCOperation] [char](1) NULL,
[CDCDateTime] [datetime2](4) NULL,
[ArchiveDateTime] [datetime2](4) NULL,
[SourceSystemCode] [varchar](50) NULL
);  

CREATE TABLE IDIT.T_CYBER_DATA_ONLINE_SALES_REVENUE_HSCX (
  [ID] numeric(12, 0) NULL,
[DESCRIPTION] varchar(255) NULL,
[DSC_ID] numeric(12, 0) NULL,
[UPDATE_USER] numeric(12, 0) NULL,
[UPDATE_DATE] datetime NULL,
[UPDATE_VERSION] numeric(12, 0) NULL,
[RESERVED_VALUE_FLAG] numeric(12, 0) NULL,
[IS_DEFAULT] numeric(12, 0) NULL,
[EXTERNAL_CODE] varchar(20) NULL,
[DISCONTINUE_DATE] datetime NULL,
[DEVELOPER_DESC] varchar(4000) NULL,
[SORT_ORDER] numeric(12, 0) NULL,
[RowID] [bigint] IDENTITY(1,1) ,
[CDCOperation] [char](1) NULL,
[CDCDateTime] [datetime2](4) NULL,
[ArchiveDateTime] [datetime2](4) NULL,
[SourceSystemCode] [varchar](50) NULL
);

CREATE TABLE IDIT.T_BI_DEPENDENT_KEY_PROVIDERS_HSCX (
 [ID] numeric(12, 0) NULL,
[DESCRIPTION] varchar(255) NULL,
[DSC_ID] numeric(12, 0) NULL,
[UPDATE_USER] numeric(12, 0) NULL,
[UPDATE_DATE] datetime NULL,
[UPDATE_VERSION] numeric(12, 0) NULL,
[RESERVED_VALUE_FLAG] numeric(12, 0) NULL,
[IS_DEFAULT] numeric(12, 0) NULL,
[EXTERNAL_CODE] varchar(20) NULL,
[DISCONTINUE_DATE] datetime NULL,
[DEVELOPER_DESC] varchar(4000) NULL,
[SORT_ORDER] numeric(12, 0) NULL,
[RowID] [bigint] IDENTITY(1,1) ,
[CDCOperation] [char](1) NULL,
[CDCDateTime] [datetime2](4) NULL,
[ArchiveDateTime] [datetime2](4) NULL,
[SourceSystemCode] [varchar](50) NULL
);

CREATE TABLE IDIT.AS_DEPENDENT_BI_KEY_PROVIDERS_HSCX (
[ID] numeric(12, 0) NULL,
[INSURED_COMPANY_ID] numeric(12,0) NULL,
[DEPENDENT_BI_KEY_PROVIDER_NAME] numeric(12,0) NULL,
[DEPENDENT_BI_KEY_PROVIDER_NAME_DETAILS] varchar(255) NULL,
[UPDATE_USER] numeric(12, 0) NULL,
[UPDATE_DATE] datetime NULL,
[UPDATE_VERSION] numeric(12, 0) NULL,
[RowID] [bigint] IDENTITY(1,1) ,
[CDCOperation] [char](1) NULL,
[CDCDateTime] [datetime2](4) NULL,
[ArchiveDateTime] [datetime2](4) NULL,
[SourceSystemCode] [varchar](50) NULL
);



alter table IDIT.AC_INSTALLMENT_HSCX add IS_PAYMENT_RELEASED numeric(1,0);

ALTER TABLE IDIT.T_BUSINESS_EXCEPTION ADD HELP_URL VARCHAR(255);

alter table IDIT.AS_COMPANY_CYBER_DATA_HSCX add CYBER_PRIMARY_OS numeric (12,0);

alter table IDIT.AS_COMPANY_CYBER_DATA_HSCX add CYBER_PRIMARY_OS_DETAIL varchar (255);

alter table IDIT.AS_COMPANY_CYBER_DATA_HSCX add CYBER_ONLINE_SALES_REVENUE numeric (12,0);
 
alter table IDIT.AS_COMPANY_CYBER_DATA_HSCX add DEPENDENT_BI_KEY_PROVIDERS_CONTRACT numeric (12,0);

alter table IDIT.AS_COMPANY_CYBER_DATA_HSCX add WEBSITE varchar(255);

alter table IDIT.AS_COMPANY_CYBER_DATA_HSCX add CONTACT_ID numeric(12,0);