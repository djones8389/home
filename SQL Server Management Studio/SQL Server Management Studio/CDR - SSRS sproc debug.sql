USE [SQL_Inventory]
GO
--/****** Object:  StoredProcedure [dbo].[uspGetFailedAgentJobs_SSRS]    Script Date: 21/01/2019 06:25:52 ******/
--SET ANSI_NULLS ON
--GO
--SET QUOTED_IDENTIFIER ON
--GO

---- =============================================
---- Author: Dave Jones
---- Create date: 14/01/2019
---- Description:	Returns failed Agent Jobs in the SSRS report
------ =============================================
--ALTER PROCEDURE [dbo].[uspGetFailedAgentJobs_SSRS]

--AS
DECLARE @ERRORCODE [int]
	, @ERRMSG varchar(128)
	, @dt AS DATETIME
	, @ScriptName VARCHAR(128) = 'UpsertFailedAgentJobs.ps1'
	, @HideReplication bit
BEGIN
		IF DATENAME(dw,GETDATE()) = 'Monday'
			SELECT @dt = CONVERT(DATETIME,DATEADD(hh,-72,GETDATE()));
		ELSE
			SELECT @dt = CONVERT(DATETIME,DATEADD(hh,-24,GETDATE()));

		--select @dt;

		WITH tb(serverID,instanceName,hostName,Environment, LastUpdated, status,[Sorting])
		AS
		(
		SELECT 
			s.serverID,s.instanceName,h.hostname, e.Environment, sh.LastUpdated, status,
			case e.ShortCode 
				when 'P' then 1
				when 'E' then 2
				when 'F' then 3
				else '5'
			end as [Sorting]
		FROM 
			dbo.Servers s 
			INNER JOIN dbo.Hosts h ON s.hostid = h.hostid
			INNER JOIN [Environments] e on e.ShortCode = s.status
			LEFT JOIN dbo.udf_ScriptHistory(@ScriptName) sh on sh.hostID = h.hostID
		WHERE
			serverState = 'Commissioned'
	
		UNION
		SELECT 
			s.serverID,s.instanceName,c.sqlclustername, e.Environment, sh.LastUpdated, status,
			case e.ShortCode 
				when 'P' then 1
				when 'E' then 2
				when 'F' then 3
				else '5'
			end as [Sorting]
		FROM 
			dbo.Servers s  
			INNER JOIN dbo.Clusters c ON c.clusterID = s.clusterID
			LEFT JOIN dbo.udf_ScriptHistory(@ScriptName) sh on c.clusterID = sh.ClusterID
			INNER JOIN [Environments] e on e.ShortCode = s.status
		WHERE
			serverState = 'Commissioned'
		), LastFailedComments as (
			select a.hostName
				, a.instanceName
				, a.job
				, '(' + convert(nvarchar(20), recorded_date, 113) + '):  ' + owner + ' - ' + comment [LastFailedComment]
			from (
			select hostName
				, instanceName
				, job
				, recorded_date
				, owner
				, comment
				, ROW_NUMBER() over( partition by 
						 hostName
						, instanceName
						, job
						, recorded_date
				 order by recorded_date desc) r
			from Failed_jobs_additional_info 
			) a
			where r = 1
		)


		SELECT faj.lastRunDate,
			tb.instanceName AS InstanceName, 
			tb.hostname AS HostName, 
			tb.Environment AS [Environment], 
			tb.lastUpdated,
			saj.jobName AS [Job], 
			faj.errorMessage AS [Error],
			CONVERT(DATETIME,faj.lastRunDate,120) AS [LastErrorDate],
			faj.eServiceReference AS [eServiceRef],
			Alerted,
			lfc.[LastFailedComment],
			[Sorting]
		FROM 
			tb 
			INNER JOIN dbo.FailedAgentJobs faj ON faj.serverID = tb.serverid
			INNER JOIN dbo.ServerAgentJobs saj ON faj.jobID = saj.jobID
			LEFT JOIN LastFailedComments lfc 
				on lfc.hostName = tb.hostName
					and lfc.instanceName = tb.instanceName
					and lfc.job = saj.jobName
		WHERE 
			faj.lastRunDate > @dt
			--AND faj.lastRunDate = (SELECT MAX(lastRunDate)FROM dbo.FailedAgentJobs where jobID = faj.jobID) 
			AND faj.lastRunDate = (SELECT MAX(lastRunDate)FROM [SQL_Inventory].[dbo].vw_FailedAgentJobs where jobID = faj.jobID)      
			AND tb.status IN (N'A',N'B',N'C',N'D',N'E',N'F',N'G',N'P',N'R')
			order by Sorting,HostName asc;
END
