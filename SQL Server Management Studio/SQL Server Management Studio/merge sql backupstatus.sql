USE [SQL_Inventory]
GO
/****** Object:  StoredProcedure [dbo].[uspGetBackupStatus]    Script Date: 19/02/2019 14:42:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


--ALTER PROCEDURE [dbo].[uspGetBackupStatus]
--AS
BEGIN

	;WITH i
		 AS (SELECT CASE
					  WHEN Isnull(s.hostID, '') <> '' THEN
						CASE
						  WHEN s.instanceName = 'MSSQLSERVER' THEN Ltrim(
						  Rtrim(LEFT(h.hostName,
								Charindex('.',
								h.hostName) - 1)))
						  ELSE Ltrim(Rtrim(LEFT(h.hostName, Charindex('.',
							   h.hostName)
							   - 1))
							   )
							   + '\' + s.instanceName
						END
					  WHEN Isnull(s.clusterID, '') <> '' THEN
						CASE
						  WHEN s.instanceName = 'MSSQLSERVER' THEN Ltrim(
						  Rtrim(LEFT(c.SQLClusterName,
								Charindex('.',
								h.hostName) - 1)))
						  ELSE Ltrim(Rtrim(LEFT(c.SQLClusterName, Charindex('.',
							   c.SQLClusterName)
							   - 1))
							   )
							   + '\' + s.instanceName
						END
					END AS InstanceName
			 FROM   [SQL_Inventory].[dbo].[Servers] s
					LEFT JOIN [SQL_Inventory].[dbo].[Hosts] h
						   ON s.hostID = h.hostID
					LEFT JOIN [SQL_Inventory].[dbo].[Clusters] c
						   ON s.clusterID = c.clusterID
					JOIN [SQL_Inventory].[dbo].[Environments] e
					  ON s.status = e.ShortCode
			 WHERE  s.serverState <> 'Decommissioned'
					AND e.Environment = 'Production')
	SELECT i.[InstanceName],
		   b.[Database],
		   b.[LastCommvaultFull],
		   DATEDIFF("hh",b.[LastCommvaultFull],GETDATE()) AS HoursSinceFull,
		   b.[LastTran],
		   DATEDIFF("hh",b.[LastTran],GETDATE()) AS HoursSinceLog,
		   b.[LastDiff],
		   DATEDIFF("hh",b.[LastDiff],GETDATE()) AS HoursSinceDiff,
		   b.[PopulatedDate],
		   b.[RecoveryMode],
		   b.[DBCreationTime],
		   b.[DBStatus],
		   b.agrole
		   ,b.agpref
	FROM   [SQL_Inventory].[dbo].[SQLBackupStatus1] b
		   RIGHT JOIN i
				   ON i.InstanceName = b.ServerInstance
	WHERE i.InstanceName NOT LIKE 'mod%'
	AND i.InstanceName NOT IN (SELECT ServerInstance FROM dbo.SQLBackupExceptions WHERE IgnoreEntireInstance = 1)
	AND b.[Database] NOT IN (SELECT [Database] FROM dbo.SQLBackupExceptions WHERE ServerInstance = i.InstanceName)
	AND (b.[AGRole] IS NULL OR b.[AGRole] = b.[AGPref])
	and [Is_Read_Only] = 0
	and i.InstanceName in ('hxp20276\BUDA','hxp20278\BUDC','hxp20276\CRMA','hxp20278\CRMC')
	ORDER  BY InstanceName ASC 
  
END

--insert SQLBackupExceptions
--values('hxp20276\BUDA'),('hxp20278\BUDC'),('hxp20276\CRMA'),('hxp20278\CRMC'),(),()

select * from [SQLBackupStatus] where ServerInstance in ('hxp20276\BUDA','hxp20278\BUDC','hxp20276\CRMA','hxp20278\CRMC') --and [AGRole] is not null
select * from [SQLBackupStatus1] where ServerInstance in ('hxp20276\BUDA','hxp20278\BUDC','hxp20276\CRMA','hxp20278\CRMC')-- and [AGRole] is not null

select count(1) from [SQLBackupStatus1] where AGRole is not null and AGPref is not null
select count(1) from [SQLBackupStatus] where AGRole is not null and AGPref is not null

begin tran

merge into [SQLBackupStatus1] TGT
using (select * from [SQLBackupStatus] where agrole is not null) as SRC
on tgt.serverinstance = src.serverinstance
	and tgt.[database] = src.[database]
	--and tgt.populateddate = src.populateddate
when matched then
update
set agrole = src.agrole
	, agpref = src.agpref;

rollback tran
