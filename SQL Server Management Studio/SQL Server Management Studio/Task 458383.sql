SELECT Start_time AS Time, Group_name AS Batch_Run, Status AS Failed

FROM  [dbo].[SH_BATCH_LOG] (READUNCOMMITTED)

where GROUP_NAME like 'Acquire all batch%'

	and update_DATE >= dateadd(hour, -1, getdate())

	and status = '6'

order by CREATE_DATE desc

