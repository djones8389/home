backup database djtest
to url = 'https://devtestdbbackups.blob.core.windows.net/test/djtest123.bak'
with credential ='devtestdbbackups'
 

--singleton

	declare @Single nvarchar(max)='';

	select @Single+=CHAR(13) + 
	'
	BACKUP DATABASE [IDIT_Obfuscated]
	TO URL = ''https://devtestdbbackups.blob.core.windows.net/test/IDIT_Obfuscated_'+(select cast((select cast(getDATE() as date)) as nvarchar(100)))+'.bak''
	WITH credential = ''devtestdbbackups'', copy_only, init, compression

	'
	exec(@Single);
	 


--multi

	declare @Multi nvarchar(max)='';

	select @Multi+=CHAR(13) + 
	'
	BACKUP DATABASE [IDIT_Obfuscated] 
	TO URL = ''https://devtestdbbackups.blob.core.windows.net/test/djtest_1_'+(select cast((select cast(getDATE() as date)) as nvarchar(100)))+'.bak''
		, URL = ''https://devtestdbbackups.blob.core.windows.net/test/djtest_2_'+(select cast((select cast(getDATE() as date)) as nvarchar(100)))+'.bak''
		, URL = ''https://devtestdbbackups.blob.core.windows.net/test/djtest_3_'+(select cast((select cast(getDATE() as date)) as nvarchar(100)))+'.bak''
		, URL = ''https://devtestdbbackups.blob.core.windows.net/test/djtest_4_'+(select cast((select cast(getDATE() as date)) as nvarchar(100)))+'.bak''
		, URL = ''https://devtestdbbackups.blob.core.windows.net/test/djtest_5_'+(select cast((select cast(getDATE() as date)) as nvarchar(100)))+'.bak''
		, URL = ''https://devtestdbbackups.blob.core.windows.net/test/djtest_6_'+(select cast((select cast(getDATE() as date)) as nvarchar(100)))+'.bak''
		, URL = ''https://devtestdbbackups.blob.core.windows.net/test/djtest_7_'+(select cast((select cast(getDATE() as date)) as nvarchar(100)))+'.bak''
		, URL = ''https://devtestdbbackups.blob.core.windows.net/test/djtest_8_'+(select cast((select cast(getDATE() as date)) as nvarchar(100)))+'.bak''
		, URL = ''https://devtestdbbackups.blob.core.windows.net/test/djtest_9_'+(select cast((select cast(getDATE() as date)) as nvarchar(100)))+'.bak''
		, URL = ''https://devtestdbbackups.blob.core.windows.net/test/djtest_10_'+(select cast((select cast(getDATE() as date)) as nvarchar(100)))+'.bak''
	WITH credential = ''devtestdbbackups'', MAXTRANSFERSIZE = 4194304, BLOCKSIZE = 65536;

	'
	exec(@Multi);


	 