if OBJECT_ID('tempdb..#test') drop table #test;

create table #test ( 
	ServerName nvarchar(max)
	, SQLInstance nvarchar(max)
	,ProductLevel	 nvarchar(max)
	,ProductVersion nvarchar(max)
	,Edition nvarchar(max)
)

bulk insert #test
from 'C:\SQL Versions.csv'
with (fieldterminator = ',' , rowterminator = '\n', firstrow=2)

SELECT a.ServerName
	, a.SQLVersion
	, SUBSTRING([SQL SP CU],1,charindex(')',[SQL SP CU])) [SQL SP CU]
	, case
		when SQLVersion = 'Microsoft SQL Server 2012 ' then '(SP4)' 
		when SQLVersion = 'Microsoft SQL Server 2014 ' then '(SP3-CU4)'
		
		 end as 'LatestVersion'
from (
select ServerName 
, substring(SQLInstance,0,charindex('(',SQLInstance)) SQLVersion
, substring(SQLInstance,charindex('(',SQLInstance),charindex(')',SQLInstance)) [SQL SP CU]
from #test
) a
--where  [SQL SP CU] ='(SP4-GDR)'
--where (SQLVersion = 'Microsoft SQL Server 2012' and [SQL SP CU] ='(SP4-GDR)')
order by 2,3