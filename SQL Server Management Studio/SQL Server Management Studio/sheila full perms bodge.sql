use master

--if not exists (select 1 from sys.sql_logins where name = 'HISCOX\SQL_App_IMPACT_SQL_User_Prod')
--	CREATE LOGIN [HISCOX\SQL_App_IMPACT_SQL_User_Prod] FROM WINDOWS WITH DEFAULT_DATABASE=[master]
--if not exists (select 1 from sys.sql_logins where name = 'HISCOX\SQL_App_IMPACT_SQL_User_Prod')
--	CREATE LOGIN [HISCOX\SQL_App_IMPACT_SQL_User_Prod] FROM WINDOWS WITH DEFAULT_DATABASE=[master]
	  
exec sp_MSforeachdb '

use [?];

if (db_id() in (select database_id
from sys.databases
where database_id > 4
	and is_read_only = 0
	and name <> ''CDR_Local''
	and user_access_desc = ''MULTI_USER''))

begin
	
	/*Read Only*/
	

	IF NOT EXISTS(SELECT 1 FROM sys.database_principals WHERE name = ''HISCOX\SQL_Sequel_Impact_RW'')
	begin
		CREATE USER [HISCOX\SQL_Sequel_Impact_RW] FOR LOGIN [HISCOX\SQL_Sequel_Impact_RW]
	end
		exec sp_addrolemember ''db_datareader'', [HISCOX\SQL_Sequel_Impact_RW];	



	/*Read Write*/


	IF NOT EXISTS(SELECT 1 FROM sys.database_principals WHERE name = ''HISCOX\SQL_Sequel_Impact_RW'')
	begin
		CREATE USER [HISCOX\SQL_Sequel_Impact_RW] FOR LOGIN [HISCOX\SQL_Sequel_Impact_RW]
	end
		exec sp_addrolemember ''db_datawriter'', [HISCOX\SQL_Sequel_Impact_RW];
		exec sp_addrolemember ''db_datareader'', [HISCOX\SQL_Sequel_Impact_RW];
	
	declare @sp nvarchar(max)='''';

	select @sp+=char(13) +
		''GRANT EXECUTE ON OBJECT::''+quotename(SCHEMA_NAME(schema_id))+''.''+quotename(name)+'' TO [HISCOX\SQL_Sequel_Impact_RW];''
		+ '' GRANT VIEW DEFINITION ON OBJECT::''+quotename(SCHEMA_NAME(schema_id))+''.''+quotename(name)+'' TO [HISCOX\SQL_Sequel_Impact_RW];''
		+ '' GRANT CREATE VIEW TO [HISCOX\SQL_Sequel_Impact_RW];''
		+ '' GRANT CREATE PROCEDURE TO [HISCOX\SQL_Sequel_Impact_RW];''
	from sys.procedures
	where type = ''P''

	exec(@sp);
	

	declare @schema nvarchar(max)='''';

	select @schema +=CHAR(13)+ ''GRANT ALTER ON SCHEMA::''+quotename(name)+'' TO [HISCOX\SQL_Sequel_Impact_RW];'' 	 
	from sys.schemas
	where schema_id between 5 and 16380

	exec(@schema);

end
'


/*

	declare @sp nvarchar(max)=''
	
	select @sp+=char(13) +
		'GRANT EXECUTE ON OBJECT::'+quotename(SCHEMA_NAME(schema_id))+'.'+quotename(name)+' TO [HISCOX\SQL_Sequel_Impact_RW];'
		+ ' GRANT VIEW DEFINITION ON OBJECT::'+quotename(SCHEMA_NAME(schema_id))+'.'+quotename(name)+' TO [HISCOX\SQL_Sequel_Impact_RW];'
		+ ' GRANT CREATE VIEW TO [HISCOX\SQL_Sequel_Impact_RW];'
		+ ' GRANT CREATE PROCEDURE TO [HISCOX\SQL_Sequel_Impact_RW];'
		+ ' GRANT ALTER ON SCHEMA::[dbo] TO [HISCOX\SQL_Sequel_Impact_RW];'
	from sys.procedures
	where type = 'P'

	print(@sp);

	declare @schema nvarchar(max)='';
	select @schema +=CHAR(13)+ 'GRANT ALTER ON SCHEMA::'+quotename(name)+' TO [HISCOX\SQL_Sequel_Impact_RW];' 	 
	from sys.schemas
	where schema_id between 5 and 16380
	exec(@schema);
	
	*/