select 
	'ALTER DATABASE tempdb MODIFY FILE (name = '+name+', size = 6144MB, MAXSIZE = 6144MB);'
	, 'DBCC SHRINKFILE ('''+name+''',6144)'
	, size
	, a.*
from sys.master_files  a
where database_id = 2
 

--ALTER DATABASE tempdb MODIFY FILE (name = tempdev, size = 6144MB, MAXSIZE = 6144MB)
--ALTER DATABASE tempdb MODIFY FILE (name = temp2, size = 6144MB, MAXSIZE = 6144MB)
--ALTER DATABASE tempdb MODIFY FILE (name = temp3, size = 6144MB, MAXSIZE = 6144MB)
--ALTER DATABASE tempdb MODIFY FILE (name = temp4, size = 6144MB, MAXSIZE = 6144MB)
--ALTER DATABASE tempdb MODIFY FILE (name = temp5, size = 6144MB, MAXSIZE = 6144MB)
--ALTER DATABASE tempdb MODIFY FILE (name = temp6, size = 6144MB, MAXSIZE = 6144MB)
--ALTER DATABASE tempdb MODIFY FILE (name = temp7, size = 6144MB, MAXSIZE = 6144MB)
--ALTER DATABASE tempdb MODIFY FILE (name = temp8, size = 6144MB, MAXSIZE = 6144MB)


--ALTER DATABASE tempdb MODIFY FILE (name = templog, MAXSIZE = 20240MB);

--use tempdb
--DBCC SHRINKFILE ('temp2',6144)
--DBCC SHRINKFILE ('temp3',6144)
--DBCC SHRINKFILE ('temp4',6144)
--DBCC SHRINKFILE ('temp5',6144)
--DBCC SHRINKFILE ('temp6',6144)
--DBCC SHRINKFILE ('temp7',6144)
--DBCC SHRINKFILE ('temp8',6144)
--DBCC SHRINKFILE ('tempdev',6144)