declare @setsimple nvarchar(MAX)='';

select @setsimple +=char(13)
	+ 'alter database '+quotename(name)+' set recovery simple with no_wait;'
from sys.databases
where recovery_model_desc = 'full'
	and database_id > 4

exec(@setsimple);