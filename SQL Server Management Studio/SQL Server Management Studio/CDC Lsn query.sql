use idit;


SELECT top 100 b.tran_end_time as CDCDateTime, getdate() as ArchiveDateTime, 
case a.__$operation
	when 1 then 'D'
	when 2 then 'I'
	when 4 then  'U'
end as CDCOperation, 
(select DB_NAME()) as SoureceSytemCode
  FROM [cdc].[P_cover_CT] a
  inner join [cdc].[lsn_time_mapping] b
	on a.__$start_lsn = b.start_lsn
	where a.__$operation in (1,2,4)
	order by b.tran_end_time asc