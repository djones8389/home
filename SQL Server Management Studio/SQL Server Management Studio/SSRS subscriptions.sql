SELECT jobs.NAME AS JobName
	,C.path AS ReportPath
	,C.NAME AS ReportName
	,u.username AS SubscriptionOwner
	--,s.SubscriptionID
	--,s.OwnerID
	,s.LastStatus
	,s.LastRunTime
	,s.ModifiedDate
FROM dbo.ReportSchedule RS
INNER JOIN msdb.dbo.sysjobs jobs ON CONVERT(VARCHAR(36), RS.ScheduleID) = jobs.NAME
INNER JOIN dbo.Subscriptions S ON RS.SubscriptionID = S.SubscriptionID
INNER JOIN dbo.CATALOG C ON s.report_oid = C.itemid
INNER JOIN dbo.users u ON s.ownerid = u.userid
order by s.LastRunTime desc;