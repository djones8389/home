--select db_name(database_id)
--	, size/128 [mb]
--from sys.master_files
--where type_desc = 'rows'

/***	read the current SQL Server default backup location	***/  
DECLARE @BackupDirectory NVARCHAR(100)   
EXEC master..xp_instance_regread @rootkey = 'HKEY_LOCAL_MACHINE',  
    @key = 'Software\Microsoft\MSSQLServer\MSSQLServer',  
    @value_name = 'BackupDirectory', @BackupDirectory = @BackupDirectory OUTPUT ;  
 
--SELECT @BackupDirectory AS [SQL Server default backup Value]
SELECT @BackupDirectory = '\\hiscox.com\backup\releasedump'

SELECT 'BACKUP DATABASE [' + name + '] TO  DISK = N'''+@BackupDirectory+'\' + name + '_'+cast((SELECT cast(GETDATE() as date)) as nvarchar(30))+'.bak'' WITH  COPY_ONLY, FORMAT,  NAME = N''' + name + '-Full Database Backup'', COMPRESSION,  STATS = 1;'
FROM sys.databases
WHERE database_id > 4
order by 1; 

--BACKUP DATABASE [Group_Actuarial_Tools_Dev_2] TO  DISK = N'\\hiscox.com\backup\releasedump\Group_Actuarial_Tools_Dev_2_2020-01-14.bak' 
--	WITH  COPY_ONLY, FORMAT,  NAME = N'Group_Actuarial_Tools_Dev_2-Full Database Backup', COMPRESSION,  STATS = 1;