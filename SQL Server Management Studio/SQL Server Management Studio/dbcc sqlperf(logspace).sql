declare @log table (
	DatabaseName sysname
	, [Log Size (MB)] decimal (8,2)
	, [Log Space Used (%)] decimal (8,2)
	, [Status] bit
)

insert @log
exec ('dbcc sqlperf(logspace)')

select *
from @log
where DatabaseName not in ('model','tempdb','master','msdb','cdr_local')
order by 3 desc; 
 


 --xp_readerrorlog