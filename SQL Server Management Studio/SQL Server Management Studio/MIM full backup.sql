DECLARE @BackupDirectory NVARCHAR(100)   
EXEC master..xp_instance_regread @rootkey = 'HKEY_LOCAL_MACHINE',  
    @key = 'Software\Microsoft\MSSQLServer\MSSQLServer',  
    @value_name = 'BackupDirectory', @BackupDirectory = @BackupDirectory OUTPUT ;  
 
SELECT @BackupDirectory = @BackupDirectory +'\' 

DECLARE @Dynamic NVARCHAR(MAX)='';

SELECT @Dynamic +=CHAR(13) + 'BACKUP DATABASE [' + name + '] TO  DISK = N'''+@BackupDirectory+ name +'.bak'' WITH  FORMAT,  NAME = N''' + name + '-Full Database Backup'', COMPRESSION,  STATS = 10;'
FROM sys.databases
WHERE name <> 'tempdb'

exec(@Dynamic);