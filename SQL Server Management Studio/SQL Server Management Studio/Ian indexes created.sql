SELECT indexes.name AS Index_Name, 
    STATS_DATE(stats.object_id, stats.stats_id) AS Stats_Last_Update 
FROM sys.stats
JOIN sys.indexes
    ON stats.object_id = indexes.object_id
    AND stats.name = indexes.name
where STATS_DATE(stats.object_id, stats.stats_id) > '2019-02-12 16:30:00'
	order by 2 desc;