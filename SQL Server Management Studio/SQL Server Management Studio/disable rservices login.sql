declare @disable nvarchar(max)='';

select @disable+=CHAR(13) 
	+ 'ALTER LOGIN ' + QUOTENAME(name) + ' DISABLE;'
from sys.syslogins 
where name like '%Rserv%'

--exec(@disable);