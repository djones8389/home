SELECT 
	 sum(cast(LTRIM (STR ((CASE WHEN usedpages > pages THEN (usedpages - pages) ELSE 0 END) * 8, 15, 0)) as int)) [index_size-kb]
	 ,sum(cast(LTRIM (STR ((CASE WHEN usedpages > pages THEN (usedpages - pages) ELSE 0 END) * 8, 15, 0)) as int))/1024 [index_size-mb]
	 ,sum(cast(LTRIM (STR ((CASE WHEN usedpages > pages THEN (usedpages - pages) ELSE 0 END) * 8, 15, 0)) as int))/1024/1024 [index_size-gb]
	 
FROM (

	SELECT 
		OBJECT_NAME(s.object_id) [TableName]
		, (used_page_count) [usedpages]
		, (
			CASE
				WHEN (s.index_id < 2) THEN (in_row_data_page_count + lob_used_page_count + row_overflow_used_page_count)
				ELSE lob_used_page_count + row_overflow_used_page_count
			END
		) [pages]
		, i.name
	FROM sys.dm_db_partition_stats AS s
		INNER JOIN sys.indexes AS i
			ON s.[object_id] = i.[object_id]
			AND s.[index_id] = i.[index_id]
		INNER JOIN sys.tables t
		on t.object_id = s.object_id
		
	 where OBJECT_NAME(s.object_id) IN ('AC_ENTRY')
) A
	--order by [index_size-kb] DESC


