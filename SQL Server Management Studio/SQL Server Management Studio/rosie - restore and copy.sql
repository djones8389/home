use master;


RESTORE DATABASE [Rosie] 
FROM DISK = N'E:\SQL_SEQ_01\SQL_Backups\Rosie_2019-08-19.bak' 
	WITH FILE = 1, NOUNLOAD, NOREWIND, REPLACE, STATS = 1
	, MOVE 'Rosie' TO N'E:\SQL_RND_01\SQL_Databases\Rosie.mdf'
	, MOVE 'Rosie_log' TO N'E:\SQL_SEQ_01\SQL_Logs\Rosie.ldf'; 


--INSERT [Group_Actuarial_Tools_UAT].[bpu].[LM_Actuarial_ResQ_Data_Feed]
--SELECT *
--FROM [Rosie].[bpu].[LM_Actuarial_ResQ_Data_Feed];

--INSERT [Group_Actuarial_Tools_UAT].[bpu].[LM_Actuarial_ResQ_Event_Feed]
--SELECT *
--FROM [Rosie].[bpu].[LM_Actuarial_ResQ_Event_Feed];

--INSERT [Group_Actuarial_Tools_UAT].[bpu].[IBNR_Actuarial_Data_Feed]
--SELECT *
--FROM [Rosie].[bpu].[IBNR_Actuarial_Data_Feed];



















--USE Group_Actuarial_Tools_UAT;

--CREATE TABLE [bpu].[LM_Actuarial_ResQ_Data_Feed](
--	[RunDate] [date] NOT NULL,
--	[RowID] [bigint] NOT NULL,
--	[YearOfAccount] [int] NOT NULL,
--	[RiskCarrierName] [varchar](100) NOT NULL,
--	[ReportingViewName] [varchar](100) NOT NULL,
--	[SourceSystemName] [varchar](100) NOT NULL,
--	[SettlementCurrencyCode] [varchar](3) NOT NULL,
--	[ADQM3] [int] NOT NULL,
--	[ADQM2] [int] NOT NULL,
--	[ClaimEVentAttLargeCatName] [varchar](20) NOT NULL,
--	[DataSetType] [varchar](300) NOT NULL,
--	[ValueTriangle] [decimal](19, 4) NULL,
--	[ReservingClassGroupLevel3Name] [varchar](100) NOT NULL,
--	[ReservingClassName] [varchar](100) NOT NULL,
--	[ReservingClassCode] [varchar](20) NOT NULL,
--	[ReservingRiskClassGroupLevel1Name] [varchar](100) NOT NULL,
--	[DevelopmentMonthNum] [int] NOT NULL,
--	[ClaimEventAttLargeCatLevel1Name] [varchar](50) NOT NULL,
--	[FutureAccountingPeriodFlag] [bit] NOT NULL,
--	[BPUBusinessClassCode] [varchar](10) NOT NULL,
--	[BPUBusinessUnitCode] [varchar](10) NOT NULL,
--	[BPUDistributionChannelCode] [varchar](10) NOT NULL,
--	[IndustryClassCode] [varchar](10) NOT NULL
--) ON [PRIMARY]
--GO

--ALTER TABLE [bpu].[LM_Actuarial_ResQ_Data_Feed] ADD  CONSTRAINT [DF_LM_Actuarial_ResQ_Data_Feed_ValueTriangle]  DEFAULT ((0.00)) FOR [ValueTriangle]
--GO

--ALTER TABLE [bpu].[LM_Actuarial_ResQ_Data_Feed] ADD  CONSTRAINT [DF_LM_Actuarial_ResQ_Data_Feed_RCGL3]  DEFAULT ('') FOR [ReservingClassGroupLevel3Name]
--GO

--ALTER TABLE [bpu].[LM_Actuarial_ResQ_Data_Feed] ADD  CONSTRAINT [DF_LM_Actuarial_ResQ_Data_Feed_RCName]  DEFAULT ('') FOR [ReservingClassName]
--GO

--ALTER TABLE [bpu].[LM_Actuarial_ResQ_Data_Feed] ADD  CONSTRAINT [DF_LM_Actuarial_ResQ_Data_Feed_RCCode]  DEFAULT ('') FOR [ReservingClassCode]
--GO

--ALTER TABLE [bpu].[LM_Actuarial_ResQ_Data_Feed] ADD  CONSTRAINT [DF_LM_Actuarial_ResQ_Data_Feed_RRCGL1]  DEFAULT ('') FOR [ReservingRiskClassGroupLevel1Name]
--GO

--ALTER TABLE [bpu].[LM_Actuarial_ResQ_Data_Feed] ADD  CONSTRAINT [DF_LM_Actuarial_ResQ_Data_Feed_BPUBCC]  DEFAULT ('') FOR [BPUBusinessClassCode]
--GO

--ALTER TABLE [bpu].[LM_Actuarial_ResQ_Data_Feed] ADD  CONSTRAINT [DF_LM_Actuarial_ResQ_Data_Feed_BPUBUC]  DEFAULT ('') FOR [BPUBusinessUnitCode]
--GO

--ALTER TABLE [bpu].[LM_Actuarial_ResQ_Data_Feed] ADD  CONSTRAINT [DF_LM_Actuarial_ResQ_Data_Feed_BPUDCC]  DEFAULT ('') FOR [BPUDistributionChannelCode]
--GO

--ALTER TABLE [bpu].[LM_Actuarial_ResQ_Data_Feed] ADD  CONSTRAINT [DF_LM_Actuarial_ResQ_Data_Feed_ICC]  DEFAULT ('') FOR [IndustryClassCode]
--GO



--CREATE TABLE [bpu].[LM_Actuarial_ResQ_Event_Feed] (
--	[RunDate] [date] NOT NULL,
--	[RowID] [bigint] NOT NULL,
--	[YearOfAccount] [int] NOT NULL,
--	[RiskCarrierName] [varchar](100) NOT NULL,
--	[ReportingViewName] [varchar](100) NOT NULL,
--	[SourceSystemName] [varchar](100) NOT NULL,
--	[SettlementCurrencyCode] [varchar](3) NOT NULL,
--	[ADQM3] [int] NOT NULL,
--	[ADQM2] [int] NOT NULL,
--	[ClaimEVentAttLargeCatName] [varchar](20) NOT NULL,
--	[DataSetType] [varchar](300) NOT NULL,
--	[ValueTriangle] [decimal](19, 4) NULL,
--	[ReservingClassGroupLevel3Name] [varchar](100) NOT NULL,
--	[ReservingClassName] [varchar](100) NOT NULL,
--	[ReservingClassCode] [varchar](20) NOT NULL,
--	[ReservingRiskClassGroupLevel1Name] [varchar](100) NOT NULL,
--	[DevelopmentMonthNum] [int] NOT NULL,
--	[ClaimEventAttLargeCatLevel1Name] [varchar](50) NOT NULL,
--	[ClaimEventCode] [varchar](20) NOT NULL,
--	[ClaimEventName] [varchar](100) NOT NULL
--) ON [PRIMARY]
--GO

--ALTER TABLE [bpu].[LM_Actuarial_ResQ_Event_Feed] ADD  CONSTRAINT [DF_LM_Actuarial_ResQ_Event_Feed_ValueTriangle]  DEFAULT ((0.00)) FOR [ValueTriangle]
--GO

--ALTER TABLE [bpu].[LM_Actuarial_ResQ_Event_Feed] ADD  CONSTRAINT [DF_LM_Actuarial_ResQ_Event_Feed_RCGL3]  DEFAULT ('') FOR [ReservingClassGroupLevel3Name]
--GO

--ALTER TABLE [bpu].[LM_Actuarial_ResQ_Event_Feed] ADD  CONSTRAINT [DF_LM_Actuarial_ResQ_Event_Feed_RCName]  DEFAULT ('') FOR [ReservingClassName]
--GO

--ALTER TABLE [bpu].[LM_Actuarial_ResQ_Event_Feed] ADD  CONSTRAINT [DF_LM_Actuarial_ResQ_Event_Feed_RCCode]  DEFAULT ('') FOR [ReservingClassCode]
--GO

--ALTER TABLE [bpu].[LM_Actuarial_ResQ_Event_Feed] ADD  CONSTRAINT [DF_LM_Actuarial_ResQ_Event_Feed_RRCGL1]  DEFAULT ('') FOR [ReservingRiskClassGroupLevel1Name]
--GO

--CREATE TABLE [bpu].[IBNR_Actuarial_Data_Feed](
--	[RunDate] [datetime] NULL,
--	[UWYear] [int] NULL,
--	[BusinessUnit] [varchar](100) NULL,
--	[GlobalBusinessUnit] [varchar](50) NULL,
--	[Region] [varchar](100) NULL,
--	[SalesOffice] [varchar](100) NULL,
--	[Division] [varchar](100) NULL,
--	[LineOfBusiness] [varchar](100) NULL,
--	[BusinessType] [varchar](100) NULL,
--	[BusinessMember] [varchar](100) NULL,
--	[CoverageGroupName] [varchar](100) NULL,
--	[GMFlag] [varchar](2) NULL,
--	[HEDFlag] [varchar](6) NULL,
--	[MotorFlag] [int] NULL,
--	[QuotaShareTypeFlag] [int] NULL,
--	[DevelopmentMonthNum] [smallint] NULL,
--	[DevelopmentQuarterNumberM2] [smallint] NULL,
--	[DevelopmentQuarterNumberM3] [smallint] NULL,
--	[OrigCurrencyCode] [varchar](3) NULL,
--	[RepCurrencyCode] [varchar](4) NOT NULL,
--	[InceptedFlag] [char](1) NULL,
--	[SourceSystem] [varchar](20) NULL,
--	[RiskCountry] [varchar](100) NOT NULL,
--	[SIILineOfBusinessCode] [varchar](30) NOT NULL,
--	[SIILineOfBusinessName] [varchar](100) NOT NULL,
--	[ClaimEventAttLargeCatName] [varchar](20) NOT NULL,
--	[ClaimEventAttLargeCatLevel1Name] [varchar](50) NOT NULL,
--	[ReservingClassName] [varchar](100) NOT NULL,
--	[ReservingRiskClassGroupLevel1Name] [varchar](100) NOT NULL,
--	[ReservingClassGroupLevel3Name] [varchar](100) NOT NULL,
--	[ConversionCurrency] [varchar](9) NOT NULL,
--	[DataSetType] [nvarchar](128) NULL,
--	[ValueTriangle] [decimal](19, 4) NULL,
--	[RiskCarrierCode] [varchar](10) NULL,
--	[RisKCarrierArrangementCode] [varchar](50) NULL,
--	[BPUBusinessClassCode] [varchar](10) NOT NULL,
--	[BPUBusinessUnitCode] [varchar](10) NOT NULL,
--	[BPUDistributionChannelCode] [varchar](10) NOT NULL,
--	[IndustryClassCode] [varchar](10) NOT NULL
--) ON [PRIMARY]
--GO

--ALTER TABLE [bpu].[IBNR_Actuarial_Data_Feed] ADD  CONSTRAINT [DF_IBNR_Actuarial_Data_Feed_BPUBusinessClassCode]  DEFAULT ('') FOR [BPUBusinessClassCode]
--GO

--ALTER TABLE [bpu].[IBNR_Actuarial_Data_Feed] ADD  CONSTRAINT [DF_IBNR_Actuarial_Data_Feed_BPUBusinessUnitCode]  DEFAULT ('') FOR [BPUBusinessUnitCode]
--GO

--ALTER TABLE [bpu].[IBNR_Actuarial_Data_Feed] ADD  CONSTRAINT [DF_IBNR_Actuarial_Data_Feed_BPUDistributionChannelCode]  DEFAULT ('') FOR [BPUDistributionChannelCode]
--GO

--ALTER TABLE [bpu].[IBNR_Actuarial_Data_Feed] ADD  CONSTRAINT [DF_IBNR_Actuarial_Data_Feed_IndustryCode]  DEFAULT ('') FOR [IndustryClassCode]
--GO
