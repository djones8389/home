declare @table table (
	tabname sysname
	, schemaname sysname
)

insert @table
values ('DIM_AnnualStatementLine','STAGING'),('DIM_BusinessClass','STAGING'),('DIM_BusinessUnit','STAGING'),('DIM_DistributionChannel','STAGING'),('DIM_EconomicBalanceSheetLOB','STAGING'),('DIM_LedgerTransactionReversalType','STAGING'),('DIM_LegalEntity','STAGING'),('DIM_MDMKeys','STAGING'),('DIM_NominalAccount','STAGING'),('DIM_ReinsuranceProgramType','STAGING'),('DIM_SolvencyIILOB','STAGING'),('DIM_System','STAGING'),('DIM_TransactionType','STAGING'),('DIM_UNIndustrySector','STAGING'),('DIM_YearOfAccount','STAGING'),('EXTFDW_DIM_BusinessUnit','STAGING'),('EXTFDW_DIM_LegalEntity','STAGING'),('EXTFDW_Dim_System','STAGING'),('EXTFDW_DIM_TransactionType','STAGING'),('EXTFDW_MAP_AccountingCriteria','STAGING'),('EXTFDW_MAP_InceptionAccounting','STAGING'),('EXTFDW_MAP_ReinsuredParty','STAGING'),('EXTFDW_MAP_TransactionType','STAGING'),('MAP_CoverholderLocation','STAGING'),('MAP_InceptionAccounting','STAGING'),('MAP_InsuranceContractType','STAGING'),('MAP_Legalentity','STAGING'),('MAP_Policytype','STAGING'),('MAP_ReinsuredParty','STAGING'),('MAP_RiskLocation','STAGING'),('MAP_TransactionType','STAGING'),('MAP_UnderwritingLocation','STAGING'),('MAP_AccountingCriteria','Landing'),('MAP_CoverholderLocation','Landing'),('MAP_FinanceCloseTimetable','Landing'),('MAP_InceptionAccounting','Landing'),('MAP_InsuranceContractType','Landing'),('MAP_LegalEntity','Landing'),('MAP_PolicyType','Landing'),('MAP_ReinsuredParty','Landing'),('MAP_RiskLocation','Landing'),('MAP_SolvencyIILocation','Landing'),('MAP_TransactionType','Landing'),('MAP_UnderwritingLocation','Landing'),('DIM_AccountingCriteria','Landing'),('DIM_AnnualStatementLine','Landing'),('DIM_BusinessClass','Landing'),('DIM_BusinessUnit','Landing'),('DIM_ClaimBasis','Landing'),('DIM_ClaimStatus','Landing'),('DIM_CoverageClass','Landing'),('DIM_Currency','Landing'),('DIM_DeclineReason','Landing'),('DIM_DistributionChannel','Landing'),('DIM_EarningsPatternType','Landing'),('DIM_EconomicBalanceSheetLineOfBusiness','Landing'),('DIM_ExchangeRateType','Landing'),('DIM_FinanceCloseTimetable','Landing'),('DIM_ForeignInsuranceLegislationCode','Landing'),('DIM_Gender','Landing'),('DIM_IndustryClass','Landing'),('DIM_InsuranceContractType','Landing'),('DIM_InsuredAgeBand','Landing'),('DIM_InsuredParty','Landing'),('DIM_LegalEntity','Landing'),('DIM_LloydsCatastropheCode','Landing'),('DIM_LloydsRiskCode','Landing'),('DIM_Location','Landing'),('DIM_LossType','Landing'),('DIM_NominalAccount','Landing'),('DIM_PolicyEventType','Landing'),('DIM_PolicyStatus','Landing'),('DIM_PolicyType','Landing'),('DIM_ReinsuranceProgramType','Landing'),('DIM_ReinsuranceType','Landing'),('DIM_ReinsuredParty','Landing'),('DIM_SolvencyIILineOfBusiness','Landing'),('DIM_SolvencyIILOB','Landing'),('DIM_System','Landing'),('DIM_TransactionType','Landing'),('DIM_TrustFund','Landing'),('DIM_UNIndustrySector','Landing'),('DIM_YearOfAccount','Landing')
select *
from @table

select 'drop table ' + quotename(schema_name(schema_id)) + '.' + quotename(name) + ';'
from sys.tables  a
inner join @table b
on schema_name(a.schema_id) = b.schemaname
	and a.name = b.tabname
order by 1;

drop table [LANDING].[DIM_AccountingCriteria];
drop table [LANDING].[DIM_AnnualStatementLine];
drop table [LANDING].[DIM_BusinessClass];
drop table [LANDING].[DIM_BusinessUnit];
drop table [LANDING].[DIM_ClaimBasis];
drop table [LANDING].[DIM_ClaimStatus];
drop table [LANDING].[DIM_CoverageClass];
drop table [LANDING].[DIM_Currency];
drop table [LANDING].[DIM_DeclineReason];
drop table [LANDING].[DIM_DistributionChannel];
drop table [LANDING].[DIM_EarningsPatternType];
drop table [LANDING].[DIM_EconomicBalanceSheetLineOfBusiness];
drop table [LANDING].[DIM_ExchangeRateType];
drop table [LANDING].[DIM_FinanceCloseTimetable];
drop table [LANDING].[DIM_ForeignInsuranceLegislationCode];
drop table [LANDING].[DIM_Gender];
drop table [LANDING].[DIM_IndustryClass];
drop table [LANDING].[DIM_InsuranceContractType];
drop table [LANDING].[DIM_InsuredAgeBand];
drop table [LANDING].[DIM_InsuredParty];
drop table [LANDING].[DIM_LegalEntity];
drop table [LANDING].[DIM_LloydsCatastropheCode];
drop table [LANDING].[DIM_LloydsRiskCode];
drop table [LANDING].[DIM_Location];
drop table [LANDING].[DIM_LossType];
drop table [LANDING].[DIM_NominalAccount];
drop table [LANDING].[DIM_PolicyEventType];
drop table [LANDING].[DIM_PolicyStatus];
drop table [LANDING].[DIM_PolicyType];
drop table [LANDING].[DIM_ReinsuranceProgramType];
drop table [LANDING].[DIM_ReinsuranceType];
drop table [LANDING].[DIM_ReinsuredParty];
drop table [LANDING].[DIM_SolvencyIILineOfBusiness];
drop table [LANDING].[DIM_SolvencyIILOB];
drop table [LANDING].[DIM_System];
drop table [LANDING].[DIM_TransactionType];
drop table [LANDING].[DIM_TrustFund];
drop table [LANDING].[DIM_UNIndustrySector];
drop table [LANDING].[DIM_YearOfAccount];
drop table [LANDING].[MAP_AccountingCriteria];
drop table [LANDING].[MAP_CoverholderLocation];
drop table [LANDING].[MAP_FinanceCloseTimetable];
drop table [LANDING].[MAP_InceptionAccounting];
drop table [LANDING].[MAP_InsuranceContractType];
drop table [LANDING].[MAP_LegalEntity];
drop table [LANDING].[MAP_PolicyType];
drop table [LANDING].[MAP_ReinsuredParty];
drop table [LANDING].[MAP_RiskLocation];
drop table [LANDING].[MAP_SolvencyIILocation];
drop table [LANDING].[MAP_TransactionType];
drop table [LANDING].[MAP_UnderwritingLocation];
drop table [STAGING].[DIM_AnnualStatementLine];
drop table [STAGING].[DIM_BusinessClass];
drop table [STAGING].[DIM_BusinessUnit];
drop table [STAGING].[DIM_DistributionChannel];
drop table [STAGING].[DIM_EconomicBalanceSheetLOB];
drop table [STAGING].[DIM_LedgerTransactionReversalType];
drop table [STAGING].[DIM_LegalEntity];
drop table [STAGING].[DIM_MDMKeys];
drop table [STAGING].[DIM_NominalAccount];
drop table [STAGING].[DIM_ReinsuranceProgramType];
drop table [STAGING].[DIM_SolvencyIILOB];
drop table [STAGING].[DIM_System];
drop table [STAGING].[DIM_TransactionType];
drop table [STAGING].[DIM_UNIndustrySector];
drop table [STAGING].[DIM_YearOfAccount];
drop table [STAGING].[EXTFDW_DIM_BusinessUnit];
drop table [STAGING].[EXTFDW_DIM_LegalEntity];
drop table [STAGING].[EXTFDW_Dim_System];
drop table [STAGING].[EXTFDW_DIM_TransactionType];
drop table [STAGING].[EXTFDW_MAP_AccountingCriteria];
drop table [STAGING].[EXTFDW_MAP_InceptionAccounting];
drop table [STAGING].[EXTFDW_MAP_ReinsuredParty];
drop table [STAGING].[EXTFDW_MAP_TransactionType];
drop table [STAGING].[MAP_CoverholderLocation];
drop table [STAGING].[MAP_InceptionAccounting];
drop table [STAGING].[MAP_InsuranceContractType];
drop table [STAGING].[map_legalentity];
drop table [STAGING].[Map_Policytype];
drop table [STAGING].[MAP_ReinsuredParty];
drop table [STAGING].[Map_riskLocation];
drop table [STAGING].[MAP_TransactionType];
drop table [STAGING].[MAP_UnderwritingLocation];