USE [master]
GO

/****** Object:  LinkedServer [DBA_CDR]    Script Date: 03/12/2019 15:09:53 ******/
EXEC master.dbo.sp_addlinkedserver @server = N'DBA_CDR', @srvproduct=N'sql_server', @provider=N'SQLNCLI', @datasrc=N'HXS20203\DBA01'
 /* For security reasons the linked server remote logins password is changed with ######## */
EXEC master.dbo.sp_addlinkedsrvlogin @rmtsrvname=N'DBA_CDR',@useself=N'False',@locallogin=NULL,@rmtuser=N'cdr',@rmtpassword='########'
GO

EXEC master.dbo.sp_serveroption @server=N'DBA_CDR', @optname=N'collation compatible', @optvalue=N'false'
GO

EXEC master.dbo.sp_serveroption @server=N'DBA_CDR', @optname=N'data access', @optvalue=N'true'
GO

EXEC master.dbo.sp_serveroption @server=N'DBA_CDR', @optname=N'dist', @optvalue=N'false'
GO

EXEC master.dbo.sp_serveroption @server=N'DBA_CDR', @optname=N'pub', @optvalue=N'false'
GO

EXEC master.dbo.sp_serveroption @server=N'DBA_CDR', @optname=N'rpc', @optvalue=N'true'
GO

EXEC master.dbo.sp_serveroption @server=N'DBA_CDR', @optname=N'rpc out', @optvalue=N'true'
GO

EXEC master.dbo.sp_serveroption @server=N'DBA_CDR', @optname=N'sub', @optvalue=N'false'
GO

EXEC master.dbo.sp_serveroption @server=N'DBA_CDR', @optname=N'connect timeout', @optvalue=N'0'
GO

EXEC master.dbo.sp_serveroption @server=N'DBA_CDR', @optname=N'collation name', @optvalue=null
GO

EXEC master.dbo.sp_serveroption @server=N'DBA_CDR', @optname=N'lazy schema validation', @optvalue=N'false'
GO

EXEC master.dbo.sp_serveroption @server=N'DBA_CDR', @optname=N'query timeout', @optvalue=N'0'
GO

EXEC master.dbo.sp_serveroption @server=N'DBA_CDR', @optname=N'use remote collation', @optvalue=N'true'
GO

EXEC master.dbo.sp_serveroption @server=N'DBA_CDR', @optname=N'remote proc transaction promotion', @optvalue=N'true'
GO


