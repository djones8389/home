use MagicLogging

select count(1)
from [Log] (READUNCOMMITTED)
where [date] < DATEADD(MONTH, -18, GETDATE())

select count(1)
from [Log] (READUNCOMMITTED)
where [date] > DATEADD(MONTH, -18, GETDATE())