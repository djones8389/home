 alter availability group [availabilitygroup] remove database idit;

--exec sp_renamedb 'IDIT','IDIT_FailedDeploy';

--exec sp_renamedb 'IDIT_1','IDIT';

CREATE DATABASE IDIT_snapshot 
ON (
	Name ='HSCX_DEV', 
	FileName='T:\SQL_Backup\IDIT_snapshot.ss1'
) 
	AS SNAPSHOT OF IDIT; 
GO


