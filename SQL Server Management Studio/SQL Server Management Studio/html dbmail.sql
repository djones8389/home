USE PhoenixFrance;
GO

--every 5 minutes (take only last 10 minutes data)  

DECLARE @RowCnt INT;
DECLARE @tableHTML  NVARCHAR(MAX) ;  
DECLARE @Results TABLE
(
       ProductCode         VARCHAR(8)
       ,WebPolicy                 VARCHAR(15)
       ,WebClient                 VARCHAR(15)
       ,InceptionDate             DATE
       ,ExpiryDate                DATE
       ,EffectiveFrom             DATE
       ,AgentReference            VARCHAR(8)
       ,WebReference        VARCHAR(15)
       ,ProcessedStatus     VARCHAR(1)
       ,RequestedDate             DATETIME
       ,WebUserName         VARCHAR(40)
);


--Once a day for all rows for past 24 hrs
INSERT INTO @Results
(
       ProductCode         
       ,WebPolicy                 
       ,WebClient
       ,InceptionDate             
       ,ExpiryDate                
       ,EffectiveFrom             
       ,AgentReference            
       ,WebReference        
       ,ProcessedStatus     
       ,RequestedDate             
       ,WebUserName         
)
SELECT 
  e.ProductCode
,pol.WebPolicy
,pol.WebClient
,pol.InceptionDate
,pol.ExpiryDate
,pol.EffectiveFrom
,pol.AgentReference
,e.WebReference
,e.ProcessedStatus
,e.RequestedDate
,e.WebUserName
FROM dbo.W$POLICY pol WITH (NOLOCK)
INNER JOIN dbo.W$CONTROL e WITH (NOLOCK) ON e.WebPolicy = pol.WebPolicy
WHERE pol.SndRejEmail = 'Y'
AND e.RequestedDate between CONVERT(char(8), getdate()-1, 112) and CONVERT(char(8), getdate(), 112)

SET @RowCnt = @@ROWCOUNT;
select @RowCnt
IF @RowCnt > 0
BEGIN  
       SET @tableHTML =  N'MyHiscox Quote Rejections - AMBER <br><br>
<head>
<style>
/* Style Definitions */
table.MsoNormalTable
       {mso-style-name:"Table Normal";
       mso-tstyle-rowband-size:0;
       mso-tstyle-colband-size:0;
       mso-style-noshow:yes;
       mso-style-priority:99;
       mso-style-parent:"";
       mso-padding-alt:0cm 5.4pt 0cm 5.4pt;
       mso-para-margin:0cm;
       mso-para-margin-bottom:.0001pt;
       mso-pagination:widow-orphan;
       font-size:10.0pt;
       font-family:"Times New Roman","serif";}
</style>
</head>
<div class=WordSection1>
' +
N'<table class=MsoNormalTable border="1">' +  
N'<tr>'+
N'<th bgcolor = "#b3b300">ProductCode</th>' +
N'<th bgcolor = "#b3b300">WebPolicy</th>' +
N'<th bgcolor = "#b3b300">WebClient</th>' +  
N'<th bgcolor = "#b3b300">InceptionDate</th>' + 
N'<th bgcolor = "#b3b300">ExpiryDate</th>' +  
N'<th bgcolor = "#b3b300">EffectiveFrom</th>' +  
N'<th bgcolor = "#b3b300">AgentReference</th>' +
N'<th bgcolor = "#b3b300">WebReference</th>' +
N'<th bgcolor = "#b3b300">ProcessedStatus</th>' +
N'<th bgcolor = "#b3b300">RequestedDate</th>' +
N'<th bgcolor = "#b3b300">WebUserName</th>' +
N'</tr>' +  
              CAST ( ( SELECT td = ISNULL(ProductCode,'')                                                     , '',    
                                         td = WebPolicy                                                                           , '',  
                                         td = WebClient                                                                           , '',  
                                         td = CONVERT(VARCHAR(25),InceptionDate,121)                          , '',
                                         td = ISNULL(CONVERT(VARCHAR(25),ExpiryDate,121),'')           , '',
                                         td = ISNULL(CONVERT(VARCHAR(25),EffectiveFrom,121),'') , '',
                                         td = ISNULL(AgentReference, '')                                             , '',
                                         td = ISNULL(WebReference, '')                                               , '',
                                         td = ISNULL(ProcessedStatus,'')                                             , '',
                                         td = ISNULL(CONVERT(VARCHAR(25),RequestedDate,121),'') , '',
                                         td = ISNULL(WebUserName,'')
                             FROM @Results 
                             FOR XML PATH('tr'), TYPE   
              ) AS NVARCHAR(MAX) ) +  
              N'</table></div>' ;  
        
       SELECT @tableHTML = replace(@tableHTML,'<td>','<td bgcolor = "#ffffe6">');
       DELETE @Results;
       SET @RowCnt = 0;
END

--SELECT @tableHTML 
IF @tableHTML IS NOT NULL
       EXEC msdb.dbo.sp_send_dbmail  
        @profile_name = 'DefaultMailAccount'
       ,@recipients  = '!francechangeteam@hiscox.com'
       ,@copy_recipients = '!PhoenixSupport@hiscox.com; dave.jones@hiscox.com; suresh.paul@hiscox.com'
       ,@subject     = 'Monitoring - AMBER - MyHiscox Quote Rejections'
       ,@body = @tableHTML 
       ,@body_format = 'HTML' 
/*
else 
       EXEC msdb.dbo.sp_send_dbmail  
        @profile_name = 'DefaultMailAccount'
       ,@recipients  = ' dave.jones@hiscox.com; suresh.paul@hiscox.com'       
       ,@subject     = 'Monitoring - AMBER - MyHiscox Quote Rejections - nothing to report'
       ,@body = @tableHTML 
       ,@body_format = 'HTML'
*/