--1 hr 24 with archive remmed out

--prod-archive-db

BACKUP DATABASE [AcquiredData] TO DISK =
N'\\hiscox.com\backup\ReleaseDump\FTP\AcquiredData.bak' WITH copy_only, noformat
, noinit, NAME = N'AcquiredData-Full Database Backup', skip, norewind, nounload,
compression, stats = 1
GO
 
 --3 hr 54 for this one

--BACKUP DATABASE [Archive] TO DISK =
--N'\\hiscox.com\backup\ReleaseDump\FTP\Archive.bak' WITH copy_only, noformat
--, noinit, NAME = N'Archive-Full Database Backup', skip, norewind, nounload,
--compression, stats = 1
--GO

BACKUP DATABASE [ArchiveControlP46] TO DISK =
N'\\hiscox.com\backup\ReleaseDump\FTP\ArchiveControlP46.bak' WITH copy_only, noformat
, noinit, NAME = N'ArchiveControlP46-Full Database Backup', skip, norewind, nounload,
compression, stats = 1
GO

 BACKUP DATABASE [ArchiveCurrentView] TO DISK =
N'\\hiscox.com\backup\ReleaseDump\FTP\ArchiveCurrentView.bak' WITH copy_only, noformat
, noinit, NAME = N'ArchiveCurrentView-Full Database Backup', skip, norewind, nounload,
compression, stats = 1
GO

BACKUP DATABASE [MIControllerP46] TO DISK =
N'\\hiscox.com\backup\ReleaseDump\FTP\MIControllerP46.bak' WITH copy_only, noformat
, noinit, NAME = N'MIControllerP46-Full Database Backup', skip, norewind, nounload,
compression, stats = 1
GO
