ALTER AVAILABILITY GROUP [availabilitygroup] REMOVE DATABASE IDIT

declare  @dynamic nvarchar(max) = '';

select @dynamic += CHAR(13) +
        'kill ' + cast(spid as char(10))
       from sys.sysprocesses
where db_name(dbid) = 'IDIT'

exec(@dynamic);


RESTORE DATABASE IDIT FROM DISK = '\\pr03039lueoh-00\LiveSupp\IDIT.bak'
WITH MOVE 'HSCX_DEV' TO 'E:\SQL_Databases\IDIT.mdf',    
MOVE 'HSCX_DEV_log' TO 'T:\SQL_Logs\IDIT.ldf', 
REPLACE, RECOVERY, stats = 1

--ALTER AVAILABILITY GROUP [availabilitygroup] REMOVE DATABASE;