----pr0610-27001-10.hiscox.com\magic

----Sessions

--select db_name(dbid)
--	, count(1)
--from sys.sysprocesses
--where db_name(dbid)='magiclogging'
--group by db_name(dbid)

USE [MagicLogging]; 

DBCC SHRINKFILE ('MAGFILES_Data',443536);  /* Space we are creating:  105216.00 MB (Shrinking from: 469136.00 to: 363920)*/

--Free up 25GB

DECLARE @MagicLogging nvarchar(MAX) = '';

SELECT @MagicLogging +=CHAR(13) +
		'ALTER INDEX ' + quotename(C.name) +  ' ON ' + quotename(SCHEMA_NAME(SCHEMA_ID)) + '.' + quotename(B.name) 
			+ CASE
				WHEN avg_fragmentation_in_percent BETWEEN 5 AND 30 THEN ' REORGANIZE'
				ELSE ' REBUILD'
			END + ';'
FROM sys.dm_db_index_physical_stats(DB_ID(),NULL,NULL,NULL,NULL) A
INNER JOIN sys.objects B
ON A.object_id = B.object_id
INNER JOIN sys.indexes C
ON B.object_id = C.object_id AND A.index_id = C.index_id
INNER JOIN sys.partitions D
ON B.object_id = D.object_id AND A.index_id = D.index_id
WHERE C.index_id > 0
	and A.avg_fragmentation_in_percent > 30

EXEC(@MagicLogging);

EXEC sp_updatestats;