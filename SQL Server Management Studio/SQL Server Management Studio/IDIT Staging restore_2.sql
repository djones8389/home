
--DECLARE @DBName sysname = 'IDIT';
--DECLARE @DataRestoreLocation nvarchar(MAX)= (select physical_name from sys.master_files where db_name(database_id) = @DBName and type_desc = 'ROWS')
--DECLARE @DataLogicalName nvarchar(MAX)= (select name from sys.master_files where db_name(database_id) = @DBName and type_desc = 'ROWS')
--DECLARE @LogRestoreLocation nvarchar(MAX)= (select physical_name from sys.master_files where db_name(database_id) = @DBName and type_desc = 'LOG');
--DECLARE @LogLogicalName nvarchar(MAX)= (select name from sys.master_files where db_name(database_id) = @DBName and type_desc = 'LOG');
--DECLARE @AG nvarchar(50) = (select name from sys.availability_groups);

--restore database IDIT
--from disk = 'T:\SQL_Backups\IDIT.bak'
--WITH FILE = 1, MOVE 'HSCX_DEV' TO 'E:\SQL_Databases\IDIT.mdf',
--                     MOVE 'HSCX_DEV_Log' TO 'E:\SQL_Logs\IDIT.mdf'
--, RECOVERY, NOUNLOAD, REPLACE, NOREWIND, STATS = 1
 