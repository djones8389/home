DECLARE @Users table (
	userName varchar(100)
)

INSERT @Users
values ('MOLEFEH'),('MEYERHOZ')

DECLARE @UserRights table (
	Permission varchar(100)
)

INSERT @UserRights
VALUES('USR_MAINT'),('VIEW_BROKER_STATE_PRINT_ONLY'),('STREAM_SUPER'),('MAINTAIN')
	

MERGE INTO usrrght as SRC
USING (
	select username, permission
	from @Users
	CROSS APPLY @UserRights
)	as TGT
on SRC.UserID_GrpID = TGT.username	
	and SRC.System_Right = TGT.permission
WHEN NOT MATCHED THEN
INSERT
VALUES (TGT.username, TGT.permission);


UPDATE usrs	
SET developer ='T'
from usrs a
inner join @Users b
on a.Usr = b.userName
WHERE usr in ('MOLEFEH','MEYERHOZ');
 