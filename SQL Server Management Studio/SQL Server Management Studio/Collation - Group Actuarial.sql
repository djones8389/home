USE [SANDBOX_Group_Actuarial_Tools_Dev_2];

SELECT TOP (1000) [Id]
      ,[Class]
      ,[TestCase]
      ,[Name]
      ,[TranName]
      ,[Result]
      ,[Msg]
      ,[TestStartTime]
      ,[TestEndTime]
INTO [tSQLt].[TestResult_1]
FROM [tSQLt].[TestResult]

DROP TABLE [tSQLt].[TestResult];

use master

declare  @dynamic nvarchar(max) = '';

select @dynamic += CHAR(13) +
	'kill ' + cast(spid as char(10))
    from sys.sysprocesses
where db_name(dbid) = 'SANDBOX_Group_Actuarial_Tools_Dev_2'

exec(@dynamic);

ALTER DATABASE [SANDBOX_Group_Actuarial_Tools_Dev_2]  COLLATE Latin1_General_CI_AS;

USE [SANDBOX_Group_Actuarial_Tools_Dev_2];

CREATE TABLE [tSQLt].[TestResult](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Class] [nvarchar](max) NOT NULL,
	[TestCase] [nvarchar](max) NOT NULL,
	[Name]  AS ((quotename([Class])+'.')+quotename([TestCase])),
	[TranName] [nvarchar](max) NOT NULL,
	[Result] [nvarchar](max) NULL,
	[Msg] [nvarchar](max) NULL,
	[TestStartTime] [datetime] NOT NULL,
	[TestEndTime] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [tSQLt].[TestResult] ADD  CONSTRAINT [DF:TestResult(TestStartTime)]  DEFAULT (getdate()) FOR [TestStartTime]
GO

SET IDENTITY_INSERT [tSQLt].[TestResult] ON;

INSERT [tSQLt].[TestResult](ID, Class, TestCase, TranName, Result,Msg,TestStartTime,TestEndTime)
SELECT ID, Class, TestCase, TranName, Result,Msg,TestStartTime,TestEndTime
FROM [SANDBOX_Group_Actuarial_Tools_Dev_2].[tSQLt].[TestResult_1]

SET IDENTITY_INSERT [tSQLt].[TestResult] OFF;