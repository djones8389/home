use master

IF OBJECT_ID('tempdb..##ClientTableMetrics') IS NOT NULL DROP TABLE ##ClientTableMetrics;

CREATE TABLE ##ClientTableMetrics (
	
	 server_name nvarchar(100)
	, database_name nvarchar(100)
	, database_id int
	, table_name nvarchar(100)
	, rows int
	, reserved_kb nvarchar(20)
	, data_kb nvarchar(20)
	, index_size nvarchar(20)
	, unused_kb nvarchar(20)
);

exec sp_MSforeachdb '

USE [?];

if (''?'' in (select name from sys.databases where state_desc = ''ONLINE'' and database_id > 4))

BEGIN

	declare @dynamic nvarchar(MAX) = '''';

	select @dynamic +=CHAR(13) +
		''exec sp_spaceused @objname = ''''''+s.name+''.''+t.name + '''''' ''
	from sys.tables T
	inner join sys.schemas S
	on s.schema_id = t.schema_id
	where type = ''u''
	order by t.name;

	INSERT ##ClientTableMetrics (table_name, [rows], reserved_kb, data_kb, index_size, unused_kb)
	EXEC(@dynamic);

	UPDATE ##ClientTableMetrics
	SET database_id = db_id()
	where database_id IS NULL;

	UPDATE ##ClientTableMetrics
	SET server_name = @@SERVERNAME
		, database_name = DB_Name()
	where db_id() = database_id;
END
'

INSERT CDR_local.dbo.ClientTableMetrics([server_name], [database_name], [database_id], [table_name], [rows], [reserved_kb], [data_kb], [index_kb], [unused_kb])
SELECT  server_name
	, database_name
	, database_id int
	, table_name
	, rows
	, cast(replace(reserved_kb,'kb','') as int) reserved_kb
	, cast(replace(data_kb,'kb','') as int)  data_kb
	, cast(replace(index_size ,'kb','') as int) index_kb
	, cast(replace(unused_kb ,'kb','') as int)  unused_kb
FROM ##ClientTableMetrics
where database_name = 'IDIT';