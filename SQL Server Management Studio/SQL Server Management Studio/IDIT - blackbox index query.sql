USE IDIT;

SELECT distinct A.[TableName]
      ,A.[IndexName]
      --,[UsedYear]
      --,[UsedMonth_1]
      --,[UsedMonth_2]
      --,[UsedMonth_3]
      --,[UsedMonth_4]
      --,[UsedMonth_5]
      --,[UsedMonth_6]
      --,[UsedMonth_7]
      --,[UsedMonth_8]
      --,[UsedMonth_9]
      --,[UsedMonth_10]
      --,[UsedMonth_11]
      --,[UsedMonth_12]
	  ,([UsedMonth_1]+[UsedMonth_2]+[UsedMonth_3]+[UsedMonth_4]+[UsedMonth_5]+[UsedMonth_6]+[UsedMonth_7]+[UsedMonth_8]+[UsedMonth_9]+[UsedMonth_10]+[UsedMonth_11]+[UsedMonth_12]) [Total Use_2019/2020]
      ,B.[user_updates]  
	  ,B.[last_user_update]  
	  ,B.[user_seeks]  
	  ,B.[user_scans]  
	  ,B.[user_lookups] 
	  ,IndexSize.TotMBs 
--	  ,'drop index ' + QUOTENAME(a.IndexName) + ' on ' + quotename(a.TableName) + ';' [Drop Index]
	  ,'ALTER INDEX ' + QUOTENAME(a.IndexName) + ' ON ' + quotename(a.TableName) + ' DISABLE;' [Disable Index]
  FROM [BlackBox].[dbo].[tbl_IndexUsage_Aggregate_Yearly] A
  INNER JOIN ##IndexMetrics B
  ON A.DatabaseName = B.[database]
     and A.TableName = B.[table]
     and a.IndexName = b.[index]
  INNER JOIN (
	  SELECT			OBJECT_NAME(i.object_id) AS TableName
					, ISNULL(i.name, 'HEAP') AS IndexName
					, ISNULL(STUFF((SELECT ', ' + QUOTENAME(c2.name) + 
							CASE ic2.is_descending_key
							WHEN 0 THEN ' ASC'
							ELSE ' DESC'
							END						
							FROM sys.indexes i2 INNER JOIN sys.index_columns ic2
							ON i2.object_id = ic2.object_id AND i2.index_id = ic2.index_id
							INNER JOIN sys.columns c2 ON ic2.object_id = c2.object_id AND ic2.column_id = c2.column_id						
							WHERE ic2.object_id = i.object_id AND ic2.index_id = i.index_id
							ORDER BY ic2.object_id	
							FOR XML PATH(N''), TYPE).value(N'.[1]', N'NVARCHAR(MAX)'), 1, 1, N''), 'HEAP') AS IndexColumnOrder
					, i.index_id AS IndexID			
					, i.fill_factor				
					, p.rows AS NumRows			
					, au.total_pages AS NumPages
					, au.total_pages / 128 AS TotMBs
					, au.used_pages / 128 AS UsedMBs
					, au.data_pages / 128 AS DataMBs
	FROM sys.indexes i INNER JOIN sys.partitions p 
	ON i.object_id = p.object_id AND i.index_id = p.index_id
	INNER JOIN sys.allocation_units au ON
	CASE 
	WHEN au.type IN (1,3) THEN p.hobt_id
	WHEN au.type = 2 THEN p.partition_id
	END = au.container_id
	INNER JOIN sys.objects o ON i.object_id = o.object_id
	WHERE o.is_ms_shipped <> 1 AND au.type_desc = 'IN_ROW_DATA'
		AND p.partition_number = 1			
	) IndexSize
	ON IndexSize.TableName = A.TableName
		and IndexSize.IndexName = a.IndexName
   
  where a.TableName !='qrtz_triggers'
  order by ([UsedMonth_1]+[UsedMonth_2]+[UsedMonth_3]+[UsedMonth_4]+[UsedMonth_5]+[UsedMonth_6]+[UsedMonth_7]+[UsedMonth_8]+[UsedMonth_9]+[UsedMonth_10]+[UsedMonth_11]+[UsedMonth_12]) ASC
	, user_updates desc
--  where TableName in ('AC_ENTRY','P_COVER_CALC_STEP')
	--and UsedYear = 2019