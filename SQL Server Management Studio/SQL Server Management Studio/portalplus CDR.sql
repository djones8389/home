DECLARE
    @Application        nvarchar(128) = N'Mojo and PortalPlus',
    @BusinessOwner      nvarchar(128) = N'Esther Rutter',
    @TechnicalOwner     nvarchar(128) = N'Jon Pennington',
    @MaintenanceWindow  nvarchar(128) = N'Not defined',
    @TechnicalExpert    nvarchar(128) = N'Colin Ajayi-Obe',
    @VSR                nvarchar(128) = N'3194',
    @Notes              nvarchar(4000) = N'N/A'
                        
IF EXISTS (SELECT 1 FROM sys.extended_properties WHERE name = 'Application')
    EXEC sys.sp_updateextendedproperty @name = N'Application', @value = @Application;
ELSE
    EXEC sys.sp_addextendedproperty @name = N'Application', @value = @Application;

IF EXISTS (SELECT 1 FROM sys.extended_properties where name = 'BusinessOwner')
    EXEC sys.sp_updateextendedproperty @name = N'BusinessOwner', @value = @BusinessOwner;
ELSE
    EXEC sys.sp_addextendedproperty @name = N'BusinessOwner', @value = @BusinessOwner; 

IF EXISTS (SELECT 1 FROM sys.extended_properties WHERE name = 'TechnicalOwner')
    EXEC sys.sp_updateextendedproperty @name = N'TechnicalOwner', @value = @TechnicalOwner;
ELSE
    EXEC sys.sp_addextendedproperty @name = N'TechnicalOwner', @value = @TechnicalOwner;

IF EXISTS (SELECT 1 FROM sys.extended_properties WHERE name = 'MaintenanceWindow')
    EXEC sys.sp_updateextendedproperty @name = N'MaintenanceWindow', @value = @MaintenanceWindow;
ELSE
    EXEC sys.sp_addextendedproperty @name = N'MaintenanceWindow', @value = @MaintenanceWindow;

IF EXISTS (SELECT 1 FROM sys.extended_properties WHERE name = 'TechnicalExpert')
    EXEC sys.sp_updateextendedproperty @name = N'TechnicalExpert', @value = @TechnicalExpert;
ELSE
    EXEC sys.sp_addextendedproperty @name = N'TechnicalExpert', @value = @TechnicalExpert;

IF EXISTS (SELECT 1 FROM sys.extended_properties WHERE name = 'Notes')
    EXEC sys.sp_updateextendedproperty @name = N'Notes', @value = @Notes;
ELSE
    EXEC sys.sp_addextendedproperty @name = N'Notes', @value = @Notes;

IF EXISTS (SELECT 1 FROM sys.extended_properties WHERE name = 'VSR')
    EXEC sys.sp_updateextendedproperty @name = N'VSR', @value = @VSR;
ELSE
    EXEC sys.sp_addextendedproperty @name = N'VSR', @value = @VSR;