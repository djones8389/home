use master

if not exists (select 1 from sys.syslogins where name = 'NONPROD\SQL_Magic_Deploy')

	CREATE LOGIN [NONPROD\SQL_Magic_Deploy] FROM WINDOWS WITH DEFAULT_DATABASE=[master]
	GO
ALTER SERVER ROLE [processadmin] ADD MEMBER [NONPROD\SQL_Magic_Deploy]
GO
ALTER SERVER ROLE [securityadmin] ADD MEMBER [NONPROD\SQL_Magic_Deploy]
GO
ALTER SERVER ROLE [sysadmin] ADD MEMBER [NONPROD\SQL_Magic_Deploy]
GO
GRANT VIEW ANY DEFINITION TO [NONPROD\SQL_Magic_Deploy]
GO
use [master]
GO
GRANT VIEW SERVER STATE TO [NONPROD\SQL_Magic_Deploy]
GO





exec sp_MSforeachdb '

use [?];

if (db_id() in (select database_id
from sys.databases
where database_id > 4
	and is_read_only = 0
	and user_access_desc = ''MULTI_USER''
	and name <> ''CDR_Local''))

begin
	
	IF NOT EXISTS(SELECT 1 FROM sys.database_principals WHERE name = ''NONPROD\SQL_Magic_Deploy'')
	begin
		CREATE USER [NONPROD\SQL_Magic_Deploy] FOR LOGIN [NONPROD\SQL_Magic_Deploy]
	end
		exec sp_addrolemember ''db_datareader'', [NONPROD\SQL_Magic_Deploy];
		exec sp_addrolemember ''db_datawriter'', [NONPROD\SQL_Magic_Deploy];
		exec sp_addrolemember ''db_ddladmin'', [NONPROD\SQL_Magic_Deploy];

end
'