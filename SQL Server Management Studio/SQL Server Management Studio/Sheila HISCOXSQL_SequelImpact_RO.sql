use master

if not exists (select 1 from sys.syslogins where name = 'HISCOX\SQL_Sequel_Impact_Users_UAT')

	CREATE LOGIN [HISCOX\SQL_Sequel_Impact_Users_UAT] FROM WINDOWS WITH DEFAULT_DATABASE=[master]
	GO


exec sp_MSforeachdb '

use [?];

if (db_id() in (select database_id
from sys.databases
where database_id > 4
	and is_read_only = 0
	and name <> ''CDR_Local''
	and user_access_desc = ''MULTI_USER''))

begin
	
	IF NOT EXISTS(SELECT 1 FROM sys.database_principals WHERE name = ''HISCOX\SQL_Sequel_Impact_Users_UAT'')
	begin
		CREATE USER [HISCOX\SQL_Sequel_Impact_Users_UAT] FOR LOGIN [HISCOX\SQL_Sequel_Impact_Users_UAT]
	end
		exec sp_addrolemember ''db_datareader'', [HISCOX\SQL_Sequel_Impact_Users_UAT];

end
'

