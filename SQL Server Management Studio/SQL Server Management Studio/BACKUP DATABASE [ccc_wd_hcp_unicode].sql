BACKUP DATABASE [ccc_wd_hcp_unicode] TO  DISK = N'E:\SQL_SEQ_01\SQL_Backups\ccc_wd_hcp_unicode_2019-09-12.bak' 
	WITH  COPY_ONLY, FORMAT,  NAME = N'ccc_wd_hcp_unicode-Full Database Backup', COMPRESSION,  STATS = 1;

 
RESTORE DATABASE [P2_ccc_wd_hcp_unicode_DT] FROM DISK = N'E:\SQL_SEQ_01\SQL_Backups\ccc_wd_hcp_unicode_2019-09-12.bak' 
	WITH FILE = 1, NOUNLOAD, NOREWIND,  STATS = 1
	, MOVE 'ccc_wd_hcp' TO N'E:\SQL_RND_01\SQL_Databases\P2_ccc_wd_hcp_unicode.mdf'
	, MOVE 'ccc_wd_hcp_unicode_log' TO N'E:\SQL_SEQ_01\SQL_Logs\P2_ccc_wd_hcp_unicode.ldf'; 
