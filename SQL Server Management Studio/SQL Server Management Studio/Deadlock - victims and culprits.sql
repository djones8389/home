SELECT [Outer].*
FROM (
	SELECT Victim.[Database]
		, Execution_Time
		, Victim.[Query] Victim
		, Culprit.[Query]   Culprit
		, Victim.Sub VictimXML
		, Culprit.Sub CulpritXML
		, ROW_NUMBER() OVER(
			Partition by Victim.[Database]
						, Execution_Time
						, Victim.[Query] 
						, Culprit.[Query] order by Execution_Time) R
	FROM (		
		SELECT Victim.*
		FROM (
			SELECT 
				R
			   , c.d.query('.') [sub]
			   , Execution_Time
			   , DB_NAME(c.d.query('.').value('data(/process/@currentdb)[1]','nvarchar(max)')) [Database]
			   , c.d.value('data(/process/@id)[1]','nvarchar(100)') VictimProcessID
			   , c.d.query('.').value('data(/process/@id)[1]','nvarchar(max)') ProcessID
			   , c.d.query('.').value('data(/process/inputbuf)[1]','nvarchar(max)') [Query]
			   , CASE
					WHEN (c.d.value('data(/process/@id)[1]','nvarchar(100)') = c.d.query('.').value('data(/process/@id)[1]','nvarchar(max)'))
					THEN '1'
					ELSE '0'
					END AS [isVictim?]
			FROM (
			SELECT ROW_NUMBER() OVER (ORDER BY (SELECT 1)) R
				, CONVERT(xml, event_data).value('(event[@name="xml_deadlock_report"]/@timestamp)[1]','datetime') as Execution_Time 
				, CONVERT(xml, event_data).query('/event/data/value/deadlock/process-list/process').query('.') myXML
			FROM sys.fn_xe_file_target_read_file('D:\SQL_RND_01\SQL_Root\MSSQL11.OPENBOX\MSSQL\Log\system_health_*.xel', null, null, null)
			WHERE object_name like 'xml_deadlock_report'
			) b
			cross apply myXML.nodes('process') c(d)
		) Victim
		where [isVictim?] = 1

	) Victim

	INNER JOIN (
		SELECT Culprit.*
		FROM (
			SELECT Culprit.*
			FROM (
				SELECT 
					R
					, c.d.query('.') [sub]
					, DB_NAME(c.d.query('.').value('data(/process/@currentdb)[1]','nvarchar(max)')) [Database]
					, c.d.value('data(/process/@id)[1]','nvarchar(100)') VictimProcessID
					, c.d.query('.').value('data(/process/@id)[1]','nvarchar(max)') ProcessID
					, c.d.query('.').value('data(/process/inputbuf)[1]','nvarchar(max)') [Query]
					, CASE
						WHEN (c.d.value('data(/process/@id)[1]','nvarchar(100)') = c.d.query('.').value('data(/process/@id)[1]','nvarchar(max)'))
						THEN '1'
						ELSE '0'
						END AS [isVictim?]
				FROM (
				SELECT ROW_NUMBER() OVER (ORDER BY (SELECT 1)) R
					, CONVERT(xml, event_data).query('/event/data/value/deadlock/process-list/process').query('.') myXML
				FROM sys.fn_xe_file_target_read_file('D:\SQL_RND_01\SQL_Root\MSSQL11.OPENBOX\MSSQL\Log\system_health_*.xel', null, null, null)
				WHERE object_name like 'xml_deadlock_report'
				) b
				cross apply myXML.nodes('process') c(d)
			) Culprit
			where [isVictim?] = 0
		) Culprit
	) Culprit
	on Victim.R = Culprit.R
) [Outer]
where R = 1;