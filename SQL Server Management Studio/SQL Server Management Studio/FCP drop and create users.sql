
SELECT distinct 'drop user ' + quotename(isnull (DP2.name, 'No members'))
 FROM sys.database_role_members AS DRM  
 RIGHT OUTER JOIN sys.database_principals AS DP1  
   ON DRM.role_principal_id = DP1.principal_id  
 LEFT OUTER JOIN sys.database_principals AS DP2  
   ON DRM.member_principal_id = DP2.principal_id  
WHERE DP1.type = 'R'
 
 drop user [uat_fcp_fdw_db_owner]
drop user [uat_fcp_fdw_db_owner_mi]
drop user [uat_fcp_fdw_db_reader]
drop user [uat_fcp_fdw_db_reader_mi]
drop user [uat_fcp_fdw_db_writer]
drop user [uat_fcp_fdw_db_writer_mi]

IF NOT EXISTS(SELECT name FROM sys.sysusers WHERE name = 'uat_fcp_fdw_db_owner') BEGIN; CREATE USER [uat_fcp_fdw_db_owner] FROM EXTERNAL PROVIDER; END;
IF NOT EXISTS(SELECT name FROM sys.sysusers WHERE name = 'uat_fcp_fdw_db_reader') BEGIN; CREATE USER [uat_fcp_fdw_db_reader] FROM EXTERNAL PROVIDER; END;
IF NOT EXISTS(SELECT name FROM sys.sysusers WHERE name = 'uat_fcp_fdw_db_writer') BEGIN; CREATE USER [uat_fcp_fdw_db_writer] FROM EXTERNAL PROVIDER; END;
IF NOT EXISTS(SELECT name FROM sys.sysusers WHERE name = 'uat_fcp_fdw_db_owner_mi') BEGIN; CREATE USER [uat_fcp_fdw_db_owner_mi] FROM EXTERNAL PROVIDER; END;
IF NOT EXISTS(SELECT name FROM sys.sysusers WHERE name = 'uat_fcp_fdw_db_reader_mi') BEGIN; CREATE USER [uat_fcp_fdw_db_reader_mi] FROM EXTERNAL PROVIDER; END;
IF NOT EXISTS(SELECT name FROM sys.sysusers WHERE name = 'uat_fcp_fdw_db_writer_mi') BEGIN; CREATE USER [uat_fcp_fdw_db_writer_mi] FROM EXTERNAL PROVIDER; END;
ALTER ROLE [db_owner] ADD MEMBER [uat_fcp_fdw_db_owner];
ALTER ROLE [db_datareader] ADD MEMBER [uat_fcp_fdw_db_reader];
ALTER ROLE [db_datawriter] ADD MEMBER [uat_fcp_fdw_db_writer];

ALTER ROLE [db_owner] ADD MEMBER [uat_fcp_fdw_db_owner_mi];
ALTER ROLE [db_datareader] ADD MEMBER [uat_fcp_fdw_db_reader_mi];
ALTER ROLE [db_datawriter] ADD MEMBER [uat_fcp_fdw_db_writer_mi];
