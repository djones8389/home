/****** Script for SelectTopNRows command from SSMS  ******/
SELECT [ID]

      ,[ASSET_DESCRIPTION]
	  ,case when [ASSET_DESCRIPTION] like '%United Kingdom%' then 'Test House Test Street Testtown TE1 1ST'
	  else 'Test company'
	  end as 'TestingForSam'
  FROM [IDIT_Obfuscated].[dbo].[AS_ASSET]
 
  where [ASSET_DESCRIPTION] IS NOT NULL


begin tran 

	UPDATE [IDIT_Obfuscated].[dbo].[AS_ASSET]
	SET [ASSET_DESCRIPTION] = B.ASSET_DESCRIPTION
	From [AS_ASSET] A
	Inner join (
	select id
		, case when [ASSET_DESCRIPTION] like '%United Kingdom%' then 'Test House Test Street Testtown TE1 1ST'
			 else 'Test company'
			 end [ASSET_DESCRIPTION]
	from   [AS_ASSET]
	where ASSET_DESCRIPTION IS NOT NULL
	) B
	on A.ID = b.ID;

rollback tran

--where [ASSET_DESCRIPTION] IS NOT NULL