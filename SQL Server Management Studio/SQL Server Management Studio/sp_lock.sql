if object_id ('tempdb..#lock')is not null drop table #lock

create table #lock (
	[spid] smallint
	, [dbid] smallint
	, [objID] int
	, [indid] tinyint
	, [type] varchar(100)
	, [Resource] varchar(200)
	, [Mode] varchar(10)
	, [Status] varchar(100)
	
)
INSERT #Lock
EXEC ('sp_lock')

SELECT *
FROM #Lock
where [Status] <> 'GRANT'
