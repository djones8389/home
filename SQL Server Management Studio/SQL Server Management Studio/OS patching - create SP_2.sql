--ALTER PROC [usp_OSPatching]
declare
	@ServerList nvarchar(MAX) = 'hxp10340.hiscox.com';
--AS 

DECLARE @Dynamic nvarchar(MAX)='';

SELECT @Dynamic +=CHAR(13) + '

SELECT COMBINED.*
FROM (
SELECT
			h.[hostname],
			s.[serverID],
			s.[instanceName],
			d.[Application],
			extras.DNSAlias
		FROM 
			[dbo].[Servers] s
			INNER JOIN [dbo].[Hosts] h ON s.hostid = h.[hostID]
			LEFT JOIN [dbo].[Extras_Instance] extras ON s.serverID = extras.ServerID
			LEFT JOIN [dbo].[Databases] d ON s.serverID = d.serverID AND d.databaseName = ''model''
	
		UNION ALL 
	
		SELECT 
			c.[sqlclustername] as [hostname], 
			s.[serverID],
			s.[instanceName],
			d.[Application],		
			extras.DNSAlias
		FROM 
			[dbo].[Servers] s 
			INNER JOIN [dbo].[Clusters] c ON s.clusterid = c.clusterid
			LEFT JOIN [dbo].[Extras_Instance] extras ON s.serverID = extras.ServerID
			LEFT JOIN [dbo].[Databases] d ON s.serverID = d.serverID AND d.databaseName = ''model''
) COMBINED
	WHERE
		Combined.[hostname] in ('''+@ServerList+''')
	ORDER BY
		hostname;
'

exec(@Dynamic);