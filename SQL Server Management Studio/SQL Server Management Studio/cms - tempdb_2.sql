ALTER DATABASE tempdb MODIFY FILE (name = tempdev, size = 7168MB);
ALTER DATABASE tempdb MODIFY FILE (name = temp2, size = 7168MB);
ALTER DATABASE tempdb MODIFY FILE (name = temp3, size = 7168MB);
ALTER DATABASE tempdb MODIFY FILE (name = temp4, size = 7168MB);
ALTER DATABASE tempdb MODIFY FILE (name = temp5, size = 7168MB);
ALTER DATABASE tempdb MODIFY FILE (name = temp6, size = 7168MB);
ALTER DATABASE tempdb MODIFY FILE (name = temp7, size = 7168MB);
ALTER DATABASE tempdb MODIFY FILE (name = temp8, size = 7168MB);

ALTER DATABASE tempdb MODIFY FILE (name = templog, size = 16384MB);