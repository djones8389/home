--DECLARE @User nvarchar(100) = '33eUser';

SELECT 'IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = '''+ name + ''') ' + char(13) 
 + 'CREATE USER ' + quotename(cast(name as nvarchar(100))) + ' FOR LOGIN ' +quotename(cast(name as nvarchar(100))) + ' WITH DEFAULT_SCHEMA = dbo'
FROM sys.database_principals 
--WHERE  name like 'NONPROD%'
--type IN ('S', 'U') 
	where principal_id > 4
	--and name = @User

SELECT 'EXEC sys.sp_addrolemember @rolename = ' + CAST(b.name as nvarchar(100)) + ' , @membername = ''' + cast(c.name as nvarchar(100)) + ''''
from sys.database_role_members a 
INNER JOIN sys.database_principals b on a.role_principal_id = b.principal_id                                 
INNER JOIN sys.database_principals c on a.member_principal_id = c.principal_id
WHERE --c.name like 'NONPROD%'
	  c.name != 'dbo'
	--and c.name = @User
ORDER by b.name, c.name

select object_name(major_id) 
	 , 'GRANT '+permission_name+' ON OBJECT::'+quotename(object_name(major_id))+' TO '+quotename(b.name) collate Latin1_General_CI_AS+ ''
	 ,b.name
from sys.database_permissions a
inner join sys.database_principals b
on a.grantee_principal_id = b.principal_id
where b.name != 'public'
	and object_name(major_id)  is not null
--where name =  @User

