SELECT 
--	  ,@@ServerName [Instance]
--	  ,DB_NAME(database_id) AS DBName
--      ,OBJECT_SCHEMA_NAME(object_id,database_id) AS [SCHEMA_NAME] 
      OBJECT_NAME(object_id,database_id)AS [OBJECT_NAME]
   --   ,cached_time
      ,last_execution_time
	  --,total_elapsed_time
      ,execution_count
      ,cast((total_worker_time/execution_count) as float)/1000000 [CPU_AVG (seconds)]
      ,cast((total_elapsed_time/execution_count) as float)/1000000 [ELAPSED_AVG (seconds)]
      --,total_logical_reads LOGICAL_READS
      --,total_logical_writes LOGICAL_WRITES
      --,total_physical_reads PHYSICAL_READS
FROM sys.dm_exec_procedure_stats 
where database_id > 4
	and DB_NAME(database_id) = 'OBX_BUD_REPL'
	and database_id != 32767
	and OBJECT_NAME(object_id,database_id)= 'QuickSearch'
	--order by 5;