DECLARE 
	@HideReplication bit = 1
	, @Environment nvarchar(100) =N'A,B,C,D,E,F,G,P,R'

DECLARE @Report NVARCHAR(MAX)='';
DECLARE @WhereClausePrefix NVARCHAR(MAX)  = 'WHERE faj.lastRunDate > @dt'

if(@HideReplication=1)
	SELECT @WhereClausePrefix = @WhereClausePrefix + ' and errorMessage not like ''%replication agent%''';
	 
SELECT @Environment = REPLACE(REPLACE(@Environment,',',','''),',',''',');


SELECT @Report = '

	DECLARE  @dt AS DATETIME
		, @ScriptName VARCHAR(128) = ''UpsertFailedAgentJobs.ps1''
		, @HideReplication bit

	IF DATENAME(dw,GETDATE()) = ''Monday''
		SELECT @dt = CONVERT(DATETIME,DATEADD(hh,-72,GETDATE()));
	ELSE
		SELECT @dt = CONVERT(DATETIME,DATEADD(hh,-24,GETDATE()));
		
		select [Outer].lastRunDate
		, [Outer].InstanceName
		, [Outer].HostName
		, [Outer].Environment
		, [Outer].LastUpdated
		, [Outer].Job
		, [Outer].Error
		, case 
			when len([Outer].Alerted) > 1
				and DBMailTestResult = 1
				and len(LastFailedComment) < 1
				then ''Stakeholders informed via DBMail, no DBA actioned required''
				else LastFailedComment
					end as ''LastFailedComment''
		, [Outer].LastErrorDate
		, [Outer].Alerted
		, [Outer].Sorting

		from (
			SELECT faj.lastRunDate
				,tb.instanceName AS InstanceName
				,tb.hostname AS HostName
				,tb.Environment AS [Environment]
				,tb.lastUpdated
				,saj.jobName AS [Job]
				,faj.errorMessage AS [Error]
				,lfc.[LastFailedComment]
				,CONVERT(DATETIME,faj.lastRunDate,120) AS [LastErrorDate]
				,faj.Alerted
				,tb.[Sorting]
				,DBMail.DBMailTestResult

			FROM [vw_FailedAgentJobs_SSRSReport_GetCommissionedInstances] tb

			INNER JOIN dbo.FailedAgentJobs faj ON faj.serverID = tb.serverid
			INNER JOIN dbo.ServerAgentJobs saj ON faj.jobID = saj.jobID
			LEFT JOIN [vw_FailedAgentJobs_SSRSReport_GetDBAComments] lfc 
			on lfc.hostName = tb.hostName
				and lfc.instanceName = tb.instanceName
				and lfc.job = saj.jobName
			LEFT JOIN (
				select i.HostName
					, i.InstanceName
					, d.DBMailTestResult
				from BAU..DBMail d
				inner join BAU..Instances i
				on d.ServerID = i.ServerID
			) DBMail
			on DBMail.hostname = tb.hostname
				and DBMail.instanceName = tb.InstanceName
			'+@WhereClausePrefix+'
				AND faj.lastRunDate > @dt and errorMessage not like ''%replication agent%''
				AND faj.lastRunDate = (SELECT MAX(lastRunDate)FROM [SQL_Inventory].[dbo].vw_FailedAgentJobs where jobID = faj.jobID)   
				AND tb.status IN ('''+@Environment+''')
		) [Outer]
				order by Sorting,HostName asc;
'


EXEC(@Report);
