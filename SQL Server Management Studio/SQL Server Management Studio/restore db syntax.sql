USE [master]
RESTORE DATABASE [UAT_StuRestore_TestPackageManager] 
FROM DISK = N'I:\Backup\UAT_BC2_TestPackageManager.bak' 
WITH FILE = 1, MOVE N'Data' TO N'H:\Databases\UAT_StuRestore_TestPackageManager.mdf', 
			MOVE N'Log' TO N'H:\Databases\UAT_StuRestore_TestPackageManager.ldf'
, RECOVERY, NOUNLOAD, NOREWIND, STATS = 5

GO
