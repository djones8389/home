USE AdventureWorks2008R2;

BEGIN TRANSACTION;

BEGIN TRY

--UPDATE DatabaseLog
--set PostTime = NULL
--where DatabaseLogID = 1


END TRY

BEGIN CATCH

    SELECT 
        ERROR_NUMBER() AS ErrorNumber
		,ERROR_SEVERITY() AS ErrorSeverity
		,ERROR_MESSAGE() AS ErrorMessage;

    IF @@TRANCOUNT > 0
        ROLLBACK TRANSACTION;

END CATCH;

IF @@TRANCOUNT > 0
    COMMIT TRANSACTION;
GO
