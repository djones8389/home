/*1. create a new SQL login to be used to execute the sql agent job*/
	
	CREATE LOGIN PP_Deploy WITH PASSWORD = 'dhrmp64mFgSZ7Z0Pzfxb0LrXZnCXdn';
	GO

/*2. map the new login to a user in MSDB this will be the user which starts the SQL Agent Job*/

	USE MSDB;
	GO
	CREATE USER PP_Deploy FOR LOGIN PP_Deploy;
	GO

/*3. Map the inteneded business user who will be kicking off the job access to MSDB this user will only have access to run the objects specified */

	USE [msdb]
	GO
	CREATE USER [GPPUser] FOR LOGIN [GPPUser]
	GO

/*4. create a schema to put user object in*/

    USE [msdb]
	GO
	CREATE SCHEMA StartJob
	GO

/*5. create a role to add this user to*/

	USE [msdb]
	GO
	CREATE ROLE [StartJobRole]
	GO

/*6. grant execute to this schema to the the new role created */

	USE [msdb]
	GO
	GRANT 
	EXECUTE ON SCHEMA::[StartJob] TO [StartJobRole]
	GO

--GRANT EXECUTE ON StartJob TO [StartJobRole] 
--GO-� if you want to give permissions to procedure only

/*7. Add user to new role */

	USE [msdb]
	GO
	EXEC sp_addrolemember N'StartJobRole', N'PP_Deploy'
	GO

 /*8. Add new SQL Login user to SQLAgent roles to allow execution of sp_start_job*/

	USE [msdb]
	GO
	EXEC sp_addrolemember N'SQLAgentReaderRole', N'PP_Deploy'
	GO
	USE [msdb]
	GO
	EXEC sp_addrolemember N'SQLAgentOperatorRole', N'PP_Deploy'
	GO
	USE [msdb]
	GO
	EXEC sp_addrolemember N'SQLAgentUserRole', N'PP_Deploy'
	GO

/*9. create a table to log when the proc has been called and by who for auditing*/

    USE [MSDB]
	GO
	CREATE TABLE [StartJob].[ExecutionHistory](
		[Id] [int] IDENTITY(1,1) NOT NULL,
		[UserRequested] [varchar](50) NULL,
		[ProcRun] [varchar](100) NULL,
		[JobRun] [varchar](100) NULL,
		[dateRun] [datetime] NULL
	) ON [PRIMARY]
	GO

/*10. create a procedure in MSDB which will run the required SQL Agent job 
This will execute as the SQL User created above to run sp_start_job preventing the calling user from being given direct permissions to run the job */
    
	USE msdb;
	GO
	CREATE PROC StartJob.[Restore_Obfuscate]
	   WITH 
	EXECUTE AS 'PP_Deploy'
	   AS
	BEGIN

	EXECUTE AS CALLER /*used to allow logging of execution in user db table created above */
	INSERT INTO MSDB.StartJob.ExecutionHistory
	  (UserRequested,ProcRun,jobRun,DateRun)
	  SELECT SUSER_NAME(),
	  'StartJob.Restore_Obfuscate',
	  'PortalPlus - Restore & Obfuscate',
		GETDATE()
		REVERT
	EXEC msdb.dbo.sp_start_job @job_name = 'PortalPlus - Restore & Obfuscate'
	IF @@error=0 /*Return message to user */
	  BEGIN
		 PRINT 'The process is currently running, check destination for output'
	  END
	END
	GO

--/*11 create schema in database the user will be executing the stored procedure in*/

--   USE Db01
--	GO
--	CREATE SCHEMA ManualJob

--/*12 Create a procedure in the user db to kick off the proc created in MSDB*/

--   USE db01
--	GO
--	CREATE PROC ManualJob.StartSQLAgentJobX
--	   WITH ENCRYPTION /*optional*/
--	   AS
--	BEGIN
--	EXEC msdb.StartJob.StartJobX
--	END
--GO

/*13 test execution of proc / agent job */

	--USE msdb
	--EXECUTE AS LOGIN = 'GPPUser';
	--SELECT SUSER_NAME(),USER, SUSER_SNAME(), SYSTEM_USER,ORIGINAL_LOGIN()
	--EXEC StartJob.[Restore_Obfuscate]
	----   REVERT

use master

CREATE USER [PP_Deploy] FOR LOGIN [PP_Deploy]

Grant Execute On master.dbo.xp_sqlagent_is_starting to [PP_Deploy]

Grant Execute On master.dbo.xp_sqlagent_notify to [PP_Deploy]

 