--BEFORE the restore, grab the LastNumberAllocated

	SELECT [LastNumberAllocated]
	FROM [MagED].[dbo].[Counters]
	WHERE CounterCode = 'BATCH'

--Restore DB via CommVault

--Set LastNumberAllocated to be the original value, plus 1

	UPDATE [MagED].[dbo].[Counters]
	--SET LastNumberAllocated = LastNumberAllocated + 1
	SET LastNumberAllocated = 909189 + 1
	WHERE CounterCode = 'BATCH'
 
 --Grab this value

	SELECT [CURRISN] 
	FROM [Hiscox_Shared].[dbo].[Counters] 
	WHERE [CTINDESCR] = 'PI-EDI Batch Number';

--Create a table based on it

	USE [Hiscox_Shared]
	GO 
	/****** Object:  Table [dbo].[Counter_EDI_BATCH_NO]    Script Date: 17/09/2015 12:48:37 ******/
	SET ANSI_NULLS ON
	GO 
	SET QUOTED_IDENTIFIER ON
	GO 
	CREATE TABLE [dbo].[Counter_EDI_BATCH_NO](
		   [Counter] [int] IDENTITY(486342,1) NOT NULL
	) ON [PRIMARY] 
	GO
