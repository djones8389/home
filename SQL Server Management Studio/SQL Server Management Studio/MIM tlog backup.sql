DECLARE @BackupDirectory NVARCHAR(100)   
EXEC master..xp_instance_regread @rootkey = 'HKEY_LOCAL_MACHINE',  
    @key = 'Software\Microsoft\MSSQLServer\MSSQLServer',  
    @value_name = 'BackupDirectory', @BackupDirectory = @BackupDirectory OUTPUT ;  
 
SELECT @BackupDirectory = @BackupDirectory +'\' 
 

DECLARE @Dynamic nvarchar(max) = '';

select @Dynamic+=CHAR(13) + '
BACKUP LOG '+quotename(name)+'
TO  DISK = N'''+@BackupDirectory+'\Logs\'+name+ '_' +(SELECT FORMAT(GETDATE(),'dd.MM.yyyy hh.mm.ss'))+'.trn''
WITH  FORMAT,  NAME = N'''+name+' - Log Backup'', COMPRESSION,  STATS = 10;
'
from sys.databases
where database_id > 4
 
exec(@Dynamic); 
