exec sp_spaceused 'ST_GENERAL_LOG';
exec sp_spaceused 'P_COVER_CALC_STEP_BACKUP';
exec sp_spaceused 'ST_USER_MNG_LOG';
--exec sp_spaceused 'ST_SERVICE_AUDIT_BLOB';
exec sp_spaceused 'ST_SERVICE_AUDIT';
exec sp_spaceused 'ST_TASK_LOG';


use idit;

declare @truncate nvarchar(max)='';

select @truncate+=CHAR(13) +
	'if exists (select 1 from sys.tables where name = '''+name+''') truncate table '+QUOTENAME(name)+';'
from sys.tables
where name in ('ST_GENERAL_LOG','P_COVER_CALC_STEP_BACKUP','ST_USER_MNG_LOG','ST_SERVICE_AUDIT_BLOB','ST_SERVICE_AUDIT','ST_TASK_LOG')

--print(@truncate);
