ALTER TRIGGER [dbo].[trEntityInstanceStates] 
ON [dbo].[EntityInstanceStates] 
AFTER UPDATE
AS
 BEGIN    
       
       IF EXISTS (SELECT 1 FROM inserted where lEntityKey = 419) 
              BEGIN

				  DECLARE @AuditEntityKey   INT
				  DECLARE @AuditObjectKey   INT     
			    
				  DECLARE @ObjectInstanceKeys TABLE (
					ObjectInstanceKey INT,
					ChangeType INT
				  )       


                 SELECT @AuditObjectKey = 5002
						, @AuditEntityKey = inserted.lInstanceKey 
				FROM inserted 
					--where inserted.lEntityKey = 419                 
              END                  

              BEGIN TRY 
			 --    INSERT INTO @ObjectInstanceKeys (ObjectInstanceKey, ChangeType) 
			 --    VALUES (@AuditEntityKey, @AuditObjectKey)   
						                            
				INSERT INTO BUD.AuditChangeTracker (ObjectInstanceKey, ChangeType, DateCreated, Processed)
				SELECT @AuditEntityKey, @AuditObjectKey, GETDATE(), 0 
						   --FROM @ObjectInstanceKeys        
              END TRY  
              BEGIN CATCH  
                     DECLARE @ErrorMessage NVARCHAR(4000);
                     DECLARE @ErrorSeverity INT;
                     DECLARE @ErrorState INT;

                     SELECT @ErrorMessage = 'An error occurred while executing trEntityInstanceStates trigger. ' + ERROR_MESSAGE(),
                              @ErrorSeverity = ERROR_SEVERITY(),
                              @ErrorState = ERROR_STATE();
              END CATCH;    
 END
