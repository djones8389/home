use CDR_Local

;with currentCTE as (
SELECT [table_name]
      ,[data_kb]
      ,[index_kb]
      ---,[unused_kb]
      ,[collectionDate]
FROM [CDR_local].[dbo].[ClientTableMetrics]
where cast([collectionDate] as date) = (select cast(getdate() as date))
  
),  olderCTE as (
SELECT [table_name]
      ,[data_kb]
      ,[index_kb]
      ---,[unused_kb]
      ,[collectionDate]
FROM [CDR_local].[dbo].[ClientTableMetrics]
where cast([collectionDate] as date) = '2019-08-30'
	--oldest one we have recorded
  
)

--14 weeks

--select a.table_name
--	, a.data_kb
--	, b.data_kb
--	, (a.data_kb - b.data_kb) [Diff_kb]
--	, (a.data_kb - b.data_kb)/1024 [Diff_mb]
select SUM((a.data_kb - b.data_kb)/1024) + SUM((a.index_kb - b.index_kb)/1024) [Diff_mb]
from currentCTE a
inner join olderCTE b
on a.table_name = b.table_name
	--order by (a.data_kb - b.data_kb) desc

--34.7216796875 per week


select * 
	, data_kb/1024/1024 data_gb
	, index_kb/1024/1024 index_gb
FROM [CDR_local].[dbo].[ClientTableMetrics]
where cast([collectionDate] as date) = '2020-01-09'
	and rows > 0
	and left(table_name, 3) = 'cdc'
  order by 1 


select * 
	, data_kb/1024/1024 data_gb
	, index_kb/1024/1024 index_gb
FROM [CDR_local].[dbo].[ClientTableMetrics]
where cast([collectionDate] as date) = (select cast(getdate() as date))
	and rows > 0
	and left(table_name, 3) = 'cdc'
  order by 1  
   

   --cdc.P_COVER_CALC_STEP_CT	339450236 109
   --cdc.P_COVER_CALC_STEP_CT	16436855  5
/*
   
	 delete from [CDR_local].[dbo].[ClientTableMetrics]
	 where [collectionDate] between '2020-01-10 12:44:41.403' and '2020-01-10 12:48:41.403'

*/