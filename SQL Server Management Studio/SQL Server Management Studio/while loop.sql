--IF (SELECT hars.role_desc
--    FROM sys.DATABASES d
--    INNER JOIN sys.dm_hadr_availability_replica_states hars ON d.replica_id = hars.replica_id
--    WHERE database_id = DB_ID('idit')) = 'PRIMARY'
 
use idit;


if not exists (select 1 from sys.tables where name = 'timerecorder')
	create table [timerecorder] (
		[timerecorder] datetime
	)
DECLARE @i int = 0;
DECLARE @increment tinyint = 1;
DECLARE @Max int = 10;  --number of seconds in 10 hours

WHILE ((@i < @Max) and ((SELECT hars.role_desc
    FROM sys.DATABASES d
    INNER JOIN sys.dm_hadr_availability_replica_states hars ON d.replica_id = hars.replica_id
    WHERE database_id = DB_ID('idit')) = 'PRIMARY'))
BEGIN

	INSERT [timerecorder]
	values((select getdate()))
	
	SELECT @i = @i + @increment
	WAITFOR DELAY '00:00:01'

END


use idit;

select * from [timerecorder]