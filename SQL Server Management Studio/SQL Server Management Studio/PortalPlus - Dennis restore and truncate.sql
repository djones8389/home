 
--RESTORE DATABASE [PortalPlusDK] FROM DISK = N'\\HXF20240\SQL_Backup\PortalPlus.bak' WITH FILE = 1
--	, NOUNLOAD, NOREWIND, REPLACE, STATS = 1, MOVE 'PortalPlus' TO N'F:\SQL_Databases_01\PortalPlusDK.mdf', MOVE 'PortalPlus_log' TO N'M:\SQL_Logs_01\PortalPlusDK.ldf'; 


--USE [PortalPlusDK];

--truncate table [Attachment];
--truncate table [Document];
--truncate table [EmailAttachment];
--truncate table [ResponseHistory];
----truncate table [TeamAttachment];

--dbcc shrinkfile('PortalPlus',2560)

--exec sp_msforeachdb '

--use [?];

--if (''?'' = ''PortalPlusDK'')

--begin

--	DECLARE IndexMaintenance CURSOR FOR
--		SELECT	''ALTER INDEX ['' + ix.name + ''] ON ['' + DB_NAME(ix_phy.database_id) + ''].[''+ OBJECT_SCHEMA_NAME(ix.object_id, ix_phy.database_id) +''].'' + QUOTENAME(OBJECT_NAME(ix.object_id)) + '' '' +
--				CASE
--					WHEN avg_fragmentation_in_percent BETWEEN 5 AND 30 THEN ''REORGANIZE''
--					ELSE ''REBUILD''
--				END + '';''
--			FROM	sys.dm_db_index_physical_stats(DB_ID(''?''), NULL, NULL, NULL, NULL) ix_phy
--			INNER JOIN sys.indexes ix
--					ON ix_phy.object_id = ix.object_id
--				AND ix_phy.index_id = ix.index_id
--		WHERE	avg_fragmentation_in_percent > 5
--		AND	ix.index_id <> 0
--		ORDER BY database_id;
--	DECLARE @SQL NVARCHAR(MAX);	
--	OPEN IndexMaintenance;
--	FETCH NEXT FROM IndexMaintenance INTO @SQL;
--	WHILE @@FETCH_STATUS = 0
--	BEGIN
--		EXEC(@SQL);
--		FETCH NEXT FROM IndexMaintenance INTO @SQL;
--	END
--	CLOSE IndexMaintenance;
--	DEALLOCATE IndexMaintenance;
--end

--'

--BACKUP DATABASE [PortalPlusDK] TO  DISK = N'\\hiscox.com\backup\ReleaseDump\PortalPlusDK_2019-08-01.bak'
-- WITH  COPY_ONLY, FORMAT,  NAME = N'PortalPlusDK-Full Database Backup', COMPRESSION,  STATS = 10;