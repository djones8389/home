/****** Object:  Table [DW].[LloydsSBFClassOfBusiness]    Script Date: 24/12/2019 11:46:14 ******/
DROP TABLE [DW].[LloydsSBFClassOfBusiness]
GO

/****** Object:  Table [DW].[LloydsSBFClassOfBusiness]    Script Date: 24/12/2019 11:46:14 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [DW].[LloydsSBFClassOfBusiness](
	[LloydsSBFClassOfBusinessID] [int] NOT NULL,
	[LloydsSBFClassOfBusinessCode] [varchar](10) NOT NULL,
	[LloydsSBFClassOfBusinessName] [varchar](100) NOT NULL,
	[LloydsSBFClassOfBusinessDescr] [varchar](255) NOT NULL,
	[LloydsSBFClassOfBusinessETLMergeKey] [varchar](50) NOT NULL,
	[SourceSystemID] [smallint] NOT NULL,
	[SourceValidFromDatetime] [smalldatetime] NOT NULL,
	[DWValidFromDatetime] [smalldatetime] NOT NULL,
	[LastModifiedWorkHistoryID] [int] NOT NULL,
	[DWDeletedFlag] [tinyint] NOT NULL
) ON [PRIMARY]
GO


