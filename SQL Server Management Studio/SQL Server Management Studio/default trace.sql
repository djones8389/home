DECLARE @current_tracefilename VARCHAR(500);
DECLARE @0_tracefilename VARCHAR(500);
DECLARE @indx INT;
SELECT @current_tracefilename = path
FROM sys.traces
WHERE is_default = 1;
SET @current_tracefilename = REVERSE(@current_tracefilename);
SELECT @indx = PATINDEX('%\%', @current_tracefilename);
SET @current_tracefilename = REVERSE(@current_tracefilename);
SET @0_tracefilename = LEFT(@current_tracefilename, LEN(@current_tracefilename) - @indx) + '\log.trc';
SELECT 
    TextData,
    HostName,
    ApplicationName,
   LoginName, 
    StartTime  
FROM ::fn_trace_gettable(@0_tracefilename, DEFAULT) t
     WHERE TextData LIKE '%SHRINKFILE%'
ORDER BY t.StartTime desc;
