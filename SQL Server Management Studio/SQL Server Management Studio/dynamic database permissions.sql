use master

declare @login sysname = 'NONPROD\bachum'
declare @dynamic nvarchar(max)='';
select @dynamic +=CHAR(13) +  '

use ['+name+']; 

if not exists (select 1 from sys.syslogins where name = '''+@login+''')
	CREATE LOGIN '+quotename(@login)+' FROM WINDOWS WITH DEFAULT_DATABASE=[master]
	
	IF NOT EXISTS(SELECT 1 FROM sys.database_principals WHERE name = '''+@login+''')
	begin
		CREATE USER '+quotename(@login)+' FOR LOGIN '+quotename(@login)+'
	end
		exec sp_addrolemember ''db_datareader'', '+quotename(@login)+';
	 
'
from sys.databases
where database_id > 4
	and is_read_only = 0
	--and name like 'Phoenix[B-Z]%'
	and user_access_desc = 'MULTI_USER'
	
exec(@dynamic);
