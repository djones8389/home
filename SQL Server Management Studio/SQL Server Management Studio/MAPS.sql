--IF OBJECT_ID('tempdb..#MyTable') IS NOT NULL DROP TABLE #MyTable

--create table #MyTable

--(
--	[Computer Name] [nvarchar](150) NULL,
--	[SQL Server Instance Name] [nvarchar](100) NULL,
--	[SQL Server Product Name] [nvarchar](255) NULL,
--	[SQL Server Version] [nvarchar](255) NULL,
--	[SQL Server Service Pack] [nvarchar](255) NULL,
--	[SQL Server Edition] [nvarchar](255) NULL,
--	[Clustered] [nvarchar](255) NULL,
--	[SQL Server Cluster Network Name] [nvarchar](255) NULL,
--	[SQL Service State] [nvarchar](255) NULL,
--	[SQL Service Start Mode] [nvarchar](255) NULL,
--	[Language] [nvarchar](255) NULL,
--	[SQL Server Sub-Directory] [nvarchar](255) NULL,
--	[Current Operating System] [nvarchar](255) NULL,
--	[Operating System Service Pack Level] [nvarchar](255) NULL,
--	[Operating System Architecture Type] [nvarchar](255) NULL,
--	[Number of Processors] [nvarchar](255) NULL,
--	[Number of Total Cores] [nvarchar](255) NULL,
--	[Number of Logical Processors] [nvarchar](255) NULL,
--	[CPU] [nvarchar](255) NULL,
--	[System Memory (MB)] [nvarchar](255) NULL,
--	[Logical Disk Drive Name] [nvarchar](255) NULL,
--	[Logical Disk Size (GB)] [nvarchar](255) NULL,
--	[Logical Disk Free Space (GB)] [nvarchar](255) NULL,
--	[Machine Type] [nvarchar](255) NULL,
--	[Number of Host Processors] [nvarchar](255) NULL,
--	[WMI Status] [nvarchar](255) NULL
--);

--bulk insert #MyTable
--from 'C:\Users\jonesdx\Desktop\VicServers.csv'
--with (fieldterminator = ',', rowterminator = '\n')
--go

--INSERT [MAPS].dbo.[MAP_Servers]
--SELECT distinct *
--FROM #MyTable


USE MAPS

select a.*
	--, serverState
from (

SELECT DISTINCT [Computer Name]
	,[SQL Server Instance Name]
	,[SQL Server Product Name]
	,[SQL Server Version]
	,isnull([SQL Server Service Pack],'') [SQL Server Service Pack]
	,[SQL Server Edition]
FROM [dbo].[MAP_Servers_2017]
WHERE [Computer Name] NOT LIKE '%nonprod%' --and [Computer Name] = 'hxp20276.hiscox.com'

 EXCEPT

SELECT DISTINCT [Computer Name]
	,[SQL Server Instance Name]
	,[SQL Server Product Name]
	,[SQL Server Version]
	,isnull([SQL Server Service Pack],'') [SQL Server Service Pack]
	,[SQL Server Edition]
FROM [dbo].[MAP_Servers]
WHERE [Computer Name] NOT LIKE '%nonprod%'-- and [Computer Name] = 'hxp20276.hiscox.com'
) a
inner join SQL_Inventory.dbo.hosts h
on h.hostname = a.[Computer Name]
inner join SQL_Inventory.dbo.servers s
on s.hostID = h.hostID

WHERE serverState = 'Commissioned'




SELECT case
when [SQL Server Cluster Network Name] is not null
then [SQL Server Cluster Network Name] + '.' + substring([computer name],charindex('.',[computer name],1) +1 , len([computer name]) - charindex('.',[computer name],1)) + '\' + substring([SQL Server Instance Name],charindex('$',[SQL Server Instance Name],1) +1 , len([SQL Server Instance Name]) - charindex('$',[SQL Server Instance Name],1))
when [SQL Server Instance Name] = 'MSSQLSERVER' then [Computer Name]
else [Computer Name] + '\' + substring([SQL Server Instance Name],charindex('$',[SQL Server Instance Name],1) +1 , len([SQL Server Instance Name]) - charindex('$',[SQL Server Instance Name],1))
end as SQL_Instance
FROM [MAPS].[dbo].[MAP_Servers]
