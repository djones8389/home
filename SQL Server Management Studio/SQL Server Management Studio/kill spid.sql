 declare  @dynamic nvarchar(max) = '';

select @dynamic += CHAR(13) +
		'kill ' + cast(spid as char(10))
       from sys.sysprocesses
where db_name(dbid) = 'EDW_230919_6'

exec(@dynamic);
 
drop database EDW_230919_6

--select *       from sys.sysprocesses
--where db_name(dbid) = 'ManagementInterfaceFeeds_270919_8'

--select * from sys.master_files
 ALTER DATABASE IDIT_Obfuscated SET  SINGLE_USER WITH NO_WAIT
ALTER DATABASE IDIT_Obfuscated SET EMERGENCY;