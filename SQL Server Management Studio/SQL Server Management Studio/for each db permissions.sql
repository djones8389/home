use master

declare @dynamic nvarchar(max)='';

select @dynamic +=CHAR(13) +  '

use ['+name+']; 

if not exists (select 1 from sys.syslogins where name = ''NONPROD\App_Phoenix'')
	CREATE LOGIN [NONPROD\App_Phoenix] FROM WINDOWS WITH DEFAULT_DATABASE=[master]
if not exists (select 1 from sys.syslogins where name = ''NONPROD\SQL_Phoenix_Developers'')
	CREATE LOGIN [NONPROD\SQL_Phoenix_Developers] FROM WINDOWS WITH DEFAULT_DATABASE=[master]

begin
	
	IF NOT EXISTS(SELECT 1 FROM sys.database_principals WHERE name = ''NONPROD\App_Phoenix'')
	begin
		CREATE USER [NONPROD\App_Phoenix] FOR LOGIN [NONPROD\App_Phoenix]
	end
		exec sp_addrolemember ''db_datawriter'', [NONPROD\App_Phoenix];
		exec sp_addrolemember ''db_datareader'', [NONPROD\App_Phoenix];
	
	IF NOT EXISTS(SELECT 1 FROM sys.database_principals WHERE name = ''NONPROD\SQL_Phoenix_Developers'')
	begin
		CREATE USER [NONPROD\SQL_Phoenix_Developers] FOR LOGIN [NONPROD\SQL_Phoenix_Developers]
	end
		exec sp_addrolemember ''db_ddladmin'', [NONPROD\SQL_Phoenix_Developers]; 
	

	declare @'+name+'_sp nvarchar(max)='''';

	select @'+name+'_sp+=char(13) +
		''GRANT EXECUTE ON OBJECT::''+quotename(SCHEMA_NAME(schema_id))+''.''+quotename(name)+'' TO [NONPROD\App_Phoenix];''
	from sys.procedures
	where type = ''P''

	exec(@'+name+'_sp); 	

	
	declare @'+name+'_fn nvarchar(max)='''';

	select @'+name+'_fn+=char(13) +
	case  
	 when type = ''tf'' then ''GRANT SELECT ON OBJECT::''+quotename(SCHEMA_NAME(schema_id))+''.''+quotename(name)+'' TO [NONPROD\App_Phoenix];''
	 when type = ''fn'' then ''GRANT EXECUTE ON OBJECT::''+quotename(SCHEMA_NAME(schema_id))+''.''+quotename(name)+'' TO [NONPROD\App_Phoenix];''
	 end
	from sys.objects
	where type in (''tf'',''fn'')

	exec(@'+name+'_fn);

end
'
from sys.databases
where database_id > 4
	and is_read_only = 0
	and name like 'Phoenix[B-Z]%'
	and user_access_desc = 'MULTI_USER'
	
EXEC(@dynamic);
 