use MAGup

SELECT a.name AS Stats
       , STATS_DATE(a.object_id, stats_id) AS LastStatsUpdate
FROM sys.stats a
inner join sys.indexes b
on a.name = b.name
WHERE a.object_id = OBJECT_ID('dbo.RiskDet')
       and type_desc = 'Clustered'
       and left(a.name,4)!='_WA_';


	   ---mageu no clustered index on riskdet?