USE [OBX_BUD_REPL]
GO
/****** Object:  StoredProcedure [BUD].[QuickSearch_DJ]    Script Date: 12/11/2018 10:14:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:              Ashik Wani
-- Create date: 24/01/2017
-- Description: Searches for the policies based on the given search criteria
-- History: 09/01/2017 MJJ Add Snapshot Isolation on to minimise blocking
-- =============================================
ALTER procedure [BUD].[QuickSearch_DJ]
	  @searchText nvarchar(1000)
	 ,@baKeys udt_BUD_NumericIdentifier readonly
	 ,@searchColumns varchar(1000) = null
	 ,@combinedstatus varchar(200) = null
as
begin

IF (EXISTS (select * from sys.databases where name = db_name() and snapshot_isolation_state = 1))        
BEGIN        
	SET TRANSACTION ISOLATION LEVEL SNAPSHOT        
END 

declare @PolicyDescriptionBaseRanking int;
declare @ReinsuredBaseRanking int;
declare @AdditionalInsuredsBaseRanking int;
declare @Contingency_EventNamesBaseRanking int;
declare @Space_SatelliteNamesBaseRanking int;
declare @VesselNamesBaseRanking int;
declare @PA_AdditionalInsuredsBaseRanking int;
declare @searchTerm nvarchar(1000);

set @AdditionalInsuredsBaseRanking = 1000;
set @Contingency_EventNamesBaseRanking= 2000;
set @Space_SatelliteNamesBaseRanking = 3000;
set @VesselNamesBaseRanking = 4000;
set @PolicyDescriptionBaseRanking = 5000;
set @ReinsuredBaseRanking = 6000;
set @PA_AdditionalInsuredsBaseRanking = 7000;

	
set @searchText = RTrim(Ltrim(@searchText))
set @searchTerm = '"' + @searchText + '*"'

select top 101
	 CASE WHEN CHARINDEX(@searchText , FolderName)=0 THEN
			 CASE WHEN AdditionalInsureds IS NULL OR CHARINDEX(@searchText,AdditionalInsureds)=0 THEN
			  CASE WHEN PA_AdditionalInsureds IS NULL OR CHARINDEX(@searchText,PA_AdditionalInsureds)=0 THEN
			   CASE WHEN Contingency_EventNames IS NULL OR CHARINDEX(@searchText,Contingency_EventNames)=0 THEN
					CASE WHEN Space_SatelliteNames IS NULL OR CHARINDEX(@searchText,Space_SatelliteNames)=0 THEN
					 CASE WHEN VesselNames IS NULL OR CHARINDEX(@searchText,VesselNames)=0 THEN
					  CASE WHEN PolicyDescription IS NULL OR CHARINDEX(@searchText,PolicyDescription)=0 THEN 
					   CASE WHEN Reinsured IS NULL OR CHARINDEX(@searchText,Reinsured)=0 THEN 10000000
					   ELSE CHARINDEX(@searchText,Reinsured) + @PolicyDescriptionBaseRanking + @VesselNamesBaseRanking + @ReinsuredBaseRanking
					   END
					  ELSE CHARINDEX(@searchText,PolicyDescription) + @PolicyDescriptionBaseRanking + @VesselNamesBaseRanking
					  END
					  WHEN CHARINDEX(@searchText,VesselNames) > 0 THEN @VesselNamesBaseRanking
					  END
					 WHEN CHARINDEX(@searchText,Space_SatelliteNames) > 0 THEN @Space_SatelliteNamesBaseRanking
					 END
				WHEN CHARINDEX(@searchText,Contingency_EventNames) > 0 THEN @Contingency_EventNamesBaseRanking
				END
			   WHEN CHARINDEX(@searchText,PA_AdditionalInsureds) > 0 THEN @PA_AdditionalInsuredsBaseRanking
			   END
			 WHEN CHARINDEX(@searchText,AdditionalInsureds) > 0 THEN @AdditionalInsuredsBaseRanking
			 END
			 WHEN CHARINDEX(@searchText,FolderName) = 1 THEN 1
			 WHEN CHARINDEX(@searchText,FolderName) > 1 THEN CHARINDEX(@searchText, FolderName)
			 WHEN CHARINDEX(@searchText,FolderName) = 1 THEN 2
			ELSE CHARINDEX(@searchText,FolderName) 
			END as folderRank
			,l.Id
			,BusinessAreaCode
			,FolderName
			,PolicyReference   
			,SectionReference as UWRef
			,UMR
			,YearOfAccount
			,CombinedStatus
			,Inception
			,Expiry       
			,BrokerReference     
			,PolicyDescription
			,PolicyKey
			,SectionKey as PolicySectionKey
			,lTypeofAsheetKey as BusinessAreaId
			,PolicyFolderKey
			,UWName
	from 
		BUD.BUDQuickSearch  as l  --WITH (FORCESEEK, INDEX (pkBUDQuickSearch#Id))
	inner join
	(
		select id
		from
		(
			SELECT Id from BUD.BUDQuickSearch 
			WHERE CHARINDEX('FolderName', @searchColumns) > 0 AND CONTAINS(FolderName, @searchTerm)  
			UNION
			SELECT Id from BUD.BUDQuickSearch 
			WHERE  CHARINDEX( 'AdditionalInsureds', @searchColumns) > 0 AND CONTAINS(AdditionalInsureds, @searchTerm) 
			UNION
			SELECT Id from BUD.BUDQuickSearch 
			WHERE CHARINDEX( 'Contingency_EventNames', @searchColumns) > 0 AND CONTAINS(Contingency_EventNames, @searchTerm)  
			UNION
			SELECT Id from BUD.BUDQuickSearch 
			WHERE CHARINDEX( 'Space_SatelliteNames', @searchColumns) > 0 AND CONTAINS(Space_SatelliteNames, @searchTerm)
			UNION
			SELECT Id from BUD.BUDQuickSearch 
			WHERE CHARINDEX( 'VesselNames', @searchColumns) > 0 AND CONTAINS(VesselNames, @searchTerm)
			UNION
			SELECT Id from BUD.BUDQuickSearch 
			WHERE CHARINDEX( 'PolicyDescription', @searchColumns) > 0 AND CONTAINS(PolicyDescription, @searchTerm)
			UNION
			SELECT Id from BUD.BUDQuickSearch 
			WHERE CHARINDEX( 'Reinsured', @searchColumns) > 0 AND  CONTAINS(Reinsured, @searchTerm)
			UNION
			SELECT Id from BUD.BUDQuickSearch 
			WHERE CHARINDEX( 'PA_AdditionalInsureds', @searchColumns) > 0 AND CONTAINS(PA_AdditionalInsureds, @searchTerm)
			UNION
			SELECT Id from BUD.BUDQuickSearch 
			WHERE CHARINDEX( 'InsuredName', @searchColumns) > 0 AND CONTAINS(InsuredName, @searchTerm)
			UNION
			SELECT Id from BUD.BUDQuickSearch 
			WHERE CHARINDEX( 'SectionReference', @searchColumns) > 0 AND CONTAINS(SectionReference, @searchTerm)
			UNION
			SELECT Id from BUD.BUDQuickSearch 
			WHERE CHARINDEX( 'PolicyReference', @searchColumns) > 0 AND CONTAINS(PolicyReference, @searchTerm)
			UNION
			SELECT Id from BUD.BUDQuickSearch 
			WHERE CHARINDEX( 'UMR', @searchColumns) > 0 AND CONTAINS(UMR, @searchTerm)
			UNION
			SELECT Id from BUD.BUDQuickSearch 
			WHERE CHARINDEX( 'BrokerReference', @searchColumns) > 0 AND CONTAINS(BrokerReference, @searchTerm)
		) as x
		group by Id) as f
	on l.Id = f.Id inner join @baKeys lst on lst.Id = l.lTypeOfAsheetKey
	where  
		(@combinedStatus Is Null Or l.CombinedStatus != @combinedStatus)
	order by 
		YearOfAccount desc, folderRank,  FolderName,Inception desc, SectionReference
	
	OPTION(RECOMPILE)
end
