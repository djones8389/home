BACKUP DATABASE [CDR_local] TO  DISK = N'E:\SQL_Backups\CDR_local_2019-09-02.bak' 
WITH FORMAT,  NAME = N'CDR_local-Full Database Backup', COMPRESSION,  STATS = 1;

BACKUP LOG [CDR_local] 
TO  DISK = N'E:\SQL_Backups\CDR_local_2019-09-02_1.trn'
 WITH COMPRESSION,  STATS = 1;

BACKUP LOG [CDR_local] 
TO  DISK = N'E:\SQL_Backups\CDR_local_2019-09-02_2.trn'
 WITH COMPRESSION,  STATS = 1;

 
BACKUP LOG [CDR_local] 
TO  DISK = N'E:\SQL_Backups\CDR_local_2019-09-02_3.trn'
 WITH COMPRESSION,  STATS = 1;

--BACKUP LOG [CDR_local] TO  DISK = N'E:\SQL_Backups\CDR_local_2019-09-02_3.trn' WITH COMPRESSION,  STATS = 1;

--\\st0303mwtei2-00\SQL_Backups


--RESTORE DATABASE [CDR_local] FROM  DISK = N'\\st0303mwtei2-00\SQL_Backups\CDR_local_2019-09-02.bak' 
--	WITH NORECOVERY
--RESTORE LOG [CDR_local] FROM  DISK = N'\\st0303mwtei2-00\SQL_Backups\CDR_local_2019-09-02_1.trn'
--	 WITH NORECOVERY
--RESTORE LOG [CDR_local] FROM  DISK = N'\\st0303mwtei2-00\SQL_Backups\CDR_local_2019-09-02_2.trn' 
--	WITH RECOVERY

