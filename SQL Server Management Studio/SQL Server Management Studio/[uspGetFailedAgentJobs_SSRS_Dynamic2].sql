USE [SQL_Inventory]
GO
/****** Object:  StoredProcedure [dbo].[uspGetFailedAgentJobs_SSRS_Dynamic1]    Script Date: 29/11/2019 14:43:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
--ALTER PROCEDURE [dbo].[uspGetFailedAgentJobs_SSRS_Dynamic1]
--(
DECLARE
	@HideReplication bit = 1
	, @Environment nvarchar(100)  =N'A,B,C,D,E,F,G,P,R'
--)
--AS

DECLARE @Report NVARCHAR(MAX)='';
DECLARE @WhereClausePrefix NVARCHAR(MAX)  = 'WHERE faj.lastRunDate > @dt'

if(@HideReplication=1)
	SELECT @WhereClausePrefix = @WhereClausePrefix + ' and errorMessage not like ''%replication agent%''';
	 
SELECT @Environment = REPLACE(REPLACE(@Environment,',',','''),',',''',');

SELECT @Report = '

	DECLARE  @dt AS DATETIME
		, @ScriptName VARCHAR(128) = ''UpsertFailedAgentJobs.ps1''
		, @HideReplication bit

	IF DATENAME(dw,GETDATE()) = ''Monday''
		SELECT @dt = CONVERT(DATETIME,DATEADD(hh,-72,GETDATE()));
	ELSE
		SELECT @dt = CONVERT(DATETIME,DATEADD(hh,-24,GETDATE()));
		
		SELECT faj.lastRunDate
			,tb.instanceName AS InstanceName
			,tb.hostname AS HostName
			,tb.Environment AS [Environment]
			,tb.lastUpdated
			,saj.jobName AS [Job]
			,faj.errorMessage AS [Error]
			,CONVERT(DATETIME,faj.lastRunDate,120) AS [LastErrorDate]
			,faj.Alerted
			,lfc.[LastFailedComment]
			,tb.[Sorting]
			,DBMail.DBMailTestResult
		FROM [vw_FailedAgentJobs_SSRSReport_GetCommissionedInstances] tb

		INNER JOIN dbo.FailedAgentJobs faj ON faj.serverID = tb.serverid
		INNER JOIN dbo.ServerAgentJobs saj ON faj.jobID = saj.jobID
		LEFT JOIN [vw_FailedAgentJobs_SSRSReport_GetDBAComments] lfc 
		on lfc.hostName = tb.hostName
			and lfc.instanceName = tb.instanceName
			and lfc.job = saj.jobName
		LEFT JOIN (
			select i.HostName
				, i.InstanceName
				, d.DBMailTestResult
			from BAU..DBMail d
			inner join BAU..Instances i
			on d.ServerID = i.ServerID
		) DBMail
		on DBMail.hostname = tb.hostname
			and DBMail.instanceName = tb.InstanceName
		'+@WhereClausePrefix+'
			AND faj.lastRunDate = (SELECT MAX(lastRunDate)FROM [SQL_Inventory].[dbo].vw_FailedAgentJobs where jobID = faj.jobID)   
			AND tb.status IN ('''+@Environment+''')
			order by Sorting,HostName asc;
'

EXEC(@Report);

--exec uspGetFailedAgentJobs_SSRS_Dynamic1 @HideReplication=1,@environment=N'A,B,C,D,E,F,G,P,R'

--AND faj.lastRunDate = (SELECT MAX(lastRunDate)FROM [SQL_Inventory].[dbo].vw_FailedAgentJobs where jobID = faj.jobID)  

--select i.HostName
--	, i.InstanceName
--	, d.DBMailTestResult
--from BAU..DBMail d
--inner join BAU..Instances i
--on d.ServerID = i.ServerID
 