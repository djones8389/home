update Databases	
set technicalExpert = 'Tony Li, Chris Wolland, Alex Paschall'
where databaseID in (

	select  d.databaseID
	from [SQL_Inventory].[dbo].[Servers] s --ON s.[serverID] = faj.[serverID]
	LEFT OUTER JOIN [SQL_Inventory].[dbo].[Clusters] c ON c.[clusterID] = s.[clusterID]
	LEFT OUTER JOIN [SQL_Inventory].[dbo].[ClusterNodes] cn ON cn.[clusterID] = s.[clusterID]
	LEFT OUTER JOIN [SQL_Inventory].[dbo].[Hosts] ch ON ch.[hostID] = cn.[nodeID]
	LEFT OUTER JOIN [SQL_Inventory].[dbo].[Hosts] h ON h.[hostID] = s.[hostID]
	inner join Databases D on D.serverID = s.serverID
	where SQLClusterName = 'aphxe96s-usmi02.hiscox.com'
		and technicalExpert = 'Tony Li'
)