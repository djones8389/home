--use bau

--/****** Script for SelectTopNRows command from SSMS  ******/
SELECT TOP (1000) a.[ServerID]
      ,[DBMailEnabled]
      ,[DBMailTested]
      ,[DBMailTestResult]
	  , i.ConnectionString
  FROM [BAU].[dbo].[DBMail] a 
  inner join [BAU].[dbo].Instances i 
  on i.ServerID = a.ServerID
  where DBMailEnabled = 0;

exec sp_configure 'show advanced options',1
reconfigure

exec sp_configure 'Database Mail XPs', 1
reconfigure with override;

--Check if mail account and profile have been setup by querying:
	
	--select * from msdb.[dbo]‌‌.[sysmail_account]
	--select * from msdb.[dbo]‌‌.[sysmail_profile]

--If mail is not setup then follow these steps:
--Add Database Mail Account

declare @hostvar varchar(50)
declare @hostmailvar varchar(110)
select @hostvar = convert(varchar(50), SERVERPROPERTY('MachineName'))
select @hostmailvar = @hostvar + '@hiscox.com'

EXECUTE msdb.dbo.sysmail_add_account_sp
	@account_name = 'DefaultMailAccount',
	@description = 'The default Hiscox mail account',
	@email_address = @hostmailvar,
	@replyto_address = '',
	@display_name = @hostvar,
	@mailserver_name = 'relay.hiscox.com',
	@port = 25;
-- Create a Database Mail Profile and add the account to the profile
DECLARE @profile_id INT, @profile_description sysname;

SELECT @profile_id = COALESCE(MAX(profile_id),1) FROM msdb.dbo.sysmail_profile
SELECT @profile_description = 'Database Mail Profile for ' + @@servername

EXECUTE msdb.dbo.sysmail_add_profile_sp 
	@profile_name = 'DefaultMailProfile'
	, @description = @profile_description;

EXECUTE msdb.dbo.sysmail_add_profileaccount_sp
	 @profile_name = 'DefaultMailProfile'
	 , @account_name = 'DefaultMailAccount'
	 , @sequence_number = @profile_id;

-- Grant access to the profile to the DBMailUsers role
EXECUTE msdb.dbo.sysmail_add_principalprofile_sp
	@profile_name = 'DefaultMailProfile',
	@principal_id = 0,
	@is_default = 1 ;
-- Enable Database Mail

--EXEC master.dbo.xp_instance_regwrite N'HKEY_LOCAL_MACHINE', N'SOFTWARE\Microsoft\MSSQLServer\SQLServerAgent', N'DatabaseMailProfile', N'REG_SZ', N''
--EXEC master.dbo.xp_instance_regwrite N'HKEY_LOCAL_MACHINE', N'SOFTWARE\Microsoft\MSSQLServer\SQLServerAgent', N'UseDatabaseMail', N'REG_DWORD', 1--GO

EXEC msdb.dbo.sp_set_sqlagent_properties @email_save_in_sent_folder=1

declare @testmail nvarchar(max)='';

select @testmail +=char(13) + '
EXECUTE [msdb].[dbo].[sp_send_dbmail]
    @profile_name = ''DefaultMailProfile''
    ,@recipients  = ''dave.jones@hiscox.com''
    ,@body        = ''This is a test e-mail sent from Database Mail on '+(select @@SERVERNAME)+'.''
    ,@subject     = '''+(select @@SERVERNAME)+'''
'

exec(@testmail);
   