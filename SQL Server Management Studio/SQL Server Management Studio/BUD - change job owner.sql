SELECT [name]  
	, isnull(SUSER_NAME(owner_sid),SUSER_SNAME(owner_sid)) [owner]
	, 'exec msdb..sp_update_job
        @job_name = '''+[name]+''',
        @owner_login_name = ''dbs''
	' [Update]
FROM msdb..sysjobs
where name like 'Job_Sync_%'
	--and isnull(SUSER_NAME(owner_sid),SUSER_SNAME(owner_sid)) <> 'HISCOX\SVC_BUD_DB_Deploy'




--Pre Deployment:  Set it to HISCOX\SVC_BUD_DB_Deploy so that the deployment will succeed 
	
	ALTER LOGIN [HISCOX\SVC_BUD_DB_Deploy] ENABLE;

	exec msdb..sp_update_job @job_name = 'Job_Sync_BUDVesselSearch', @owner_login_name = 'HISCOX\SVC_BUD_DB_Deploy'   
	exec msdb..sp_update_job @job_name = 'Job_Sync_BUDSearch Failing Rows Warning', @owner_login_name = 'HISCOX\SVC_BUD_DB_Deploy'   
	exec msdb..sp_update_job @job_name = 'Job_Sync_BUDSearch', @owner_login_name = 'HISCOX\SVC_BUD_DB_Deploy'   
	exec msdb..sp_update_job @job_name = 'Job_Sync_BUDSearch - Check Synchronisation', @owner_login_name = 'HISCOX\SVC_BUD_DB_Deploy'   
	exec msdb..sp_update_job @job_name = 'Job_Sync_BUDPolicyDetails', @owner_login_name = 'HISCOX\SVC_BUD_DB_Deploy'   
	exec msdb..sp_update_job @job_name = 'Job_Sync_BUDSearch - Long Run Checker', @owner_login_name = 'HISCOX\SVC_BUD_DB_Deploy'   
	exec msdb..sp_update_job @job_name = 'Job_Sync_BUDPolicySection', @owner_login_name = 'HISCOX\SVC_BUD_DB_Deploy'   

--Post Deployment:  Set back to dbs as is the DBA standard for SQL jobs.  This ensures it can't get locked out by AD

	exec msdb..sp_update_job @job_name = 'Job_Sync_BUDVesselSearch', @owner_login_name = 'dbs'   
	exec msdb..sp_update_job @job_name = 'Job_Sync_BUDSearch Failing Rows Warning', @owner_login_name = 'dbs'   
	exec msdb..sp_update_job @job_name = 'Job_Sync_BUDSearch', @owner_login_name = 'dbs'   
	exec msdb..sp_update_job @job_name = 'Job_Sync_BUDSearch - Check Synchronisation', @owner_login_name = 'dbs'   
	exec msdb..sp_update_job @job_name = 'Job_Sync_BUDPolicyDetails', @owner_login_name = 'dbs'   
	exec msdb..sp_update_job @job_name = 'Job_Sync_BUDSearch - Long Run Checker', @owner_login_name = 'dbs'   
	exec msdb..sp_update_job @job_name = 'Job_Sync_BUDPolicySection', @owner_login_name = 'dbs'   

	ALTER LOGIN [HISCOX\SVC_BUD_DB_Deploy] DISABLE;