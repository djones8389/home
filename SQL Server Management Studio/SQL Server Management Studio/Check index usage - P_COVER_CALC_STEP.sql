USE IDIT_PROD_MI;

	SELECT	 
			 OBJECT_NAME(s.OBJECT_ID) AS [table],
			 i.name AS [index],
			 s.user_updates,
			 [last_user_update] ,
			 s.user_seeks , 
			 s.user_scans, 
			 s.user_lookups
			 ,  [last_user_seek]
			 ,  [last_user_scan]
			 ,  [last_user_lookup]
	 FROM		sys.dm_db_index_usage_stats AS s WITH (NOLOCK)
	 INNER JOIN sys.indexes AS i WITH (NOLOCK)
			 ON s.[object_id] = i.[object_id]
			AND i.index_id = s.index_id
	WHERE	 OBJECTPROPERTY(s.[OBJECT_ID],'IsUserTable') = 1
	 AND	 i.index_id > 1
 	 and     s.database_id = DB_ID()
	 and	OBJECT_NAME(s.OBJECT_ID)= 'AC_ENTRY'
	ORDER BY 1,2
	OPTION	 (RECOMPILE)

 