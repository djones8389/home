USE [SQL_Inventory]
GO
 
--DROP PROC [dbo].[vw_OSPatching]

ALTER PROC [dbo].[sp_OSPatching]
AS

SELECT A.* 
INTO #OSPatching
FROM (

SELECT
			h.[hostname] as [connection],
			s.[serverID],
			s.[instanceName],
			d.[Application],
			extras.DNSAlias as [DNS / ClusterName]
		FROM  [dbo].[Servers] s
			INNER JOIN [dbo].[Hosts] h ON s.hostid = h.[hostID]
			LEFT JOIN [dbo].[Extras_Instance] extras ON s.serverID = extras.ServerID
			LEFT JOIN [dbo].[Databases] d ON s.serverID = d.serverID AND d.databaseName = 'model'
		WHERE     s.serverState <> 'Decommissioned'
UNION ALL 
	
		SELECT 
			c.[sqlclustername] as [connection], 
			s.[serverID],
			s.[instanceName],
			d.[Application],		
			extras.DNSAlias as [DNS / ClusterName]
		FROM [dbo].[Servers] s 
			INNER JOIN [dbo].[Clusters] c ON s.clusterid = c.clusterid
			LEFT JOIN [dbo].[Extras_Instance] extras ON s.serverID = extras.ServerID
			LEFT JOIN [dbo].[Databases] d ON s.serverID = d.serverID AND d.databaseName = 'model'
		WHERE     s.serverState <> 'Decommissioned'
) A 
 

INSERT #OSPatching
SELECT hostName
	, a.serverid
	, b.instanceName
	, b.instanceName1
	, [SQL Access Point]
FROM #OSPatching  a
inner join (

	SELECT hostName
		, '' [serverid]
		, instanceName as instanceName
		, instanceName as instanceName1
		, [SQL Access Point]
	FROM [SQL_Inventory].[dbo].[vw_CDRPortal_ClusterList] a
	inner join [SQL_Inventory].[dbo].[vw_HostClusters] b
	on a.[SQL Access Point] = b.SQLClusterName
	where [Commissioned?] = 'Commissioned' 
) b
on a.connection = b.[SQL Access Point]

select *
from #OSPatching




--order by 1

--LEFT JOIN (
--	SELECT b.hostName
--		, '' f
--		, instanceName
--	  FROM [SQL_Inventory].[dbo].[vw_CDRPortal_ClusterList] a
--	  inner join [SQL_Inventory].[dbo].[vw_HostClusters] b
--	  on a.[SQL Access Point] = b.SQLClusterName
--	  where [Commissioned?] = 'Commissioned' 
--  ) B
--ON A.connection = B.hostName

--UNION ALL
	
--	SELECT [hostName]
--		  ,''
--		  ,[SQLClusterName]
--		  ,[instanceName]
--		  ,[Windows Cluster]
--	  FROM [SQL_Inventory].[dbo].[vw_HostClusters]
	order by 1

GO

 	SELECT [hostName]
		  ,''
		  ,[SQLClusterName]
		  ,[instanceName]
		  ,[Windows Cluster]
	  FROM [SQL_Inventory].[dbo].[vw_HostClusters]