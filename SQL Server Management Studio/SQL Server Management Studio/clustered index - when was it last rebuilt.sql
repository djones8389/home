SELECT a.name AS Stats
	, STATS_DATE(a.object_id, stats_id) AS LastStatsUpdate
FROM sys.stats a
inner join sys.indexes b
on a.name = b.name
WHERE a.object_id = OBJECT_ID('dbo.CommandLog')
	and type_desc = 'Clustered'
	and left(a.name,4)!='_WA_'
order by 2 desc;






use master;

declare @table table (
	db sysname
	, tablename sysname
	, [name] varchar(100)
	, laststatsupdate datetime

)

insert @table
exec sp_MSforeachdb '

use [?];

SELECT top 5''?'' [DB]
	, object_name(b.object_id) [TableName]
	, a.name
	, STATS_DATE(a.object_id, stats_id) AS LastStatsUpdate
FROM sys.stats a
inner join sys.indexes b
on a.name = b.name
WHERE a.object_id in (OBJECT_ID(''dbo.BRANCHS''),OBJECT_ID(''dbo.PolHist''),OBJECT_ID(''dbo.Names''))
	and type_desc = ''Clustered''
	and left(a.name,4)!=''_WA_''
	order by STATS_DATE(a.object_id, stats_id) desc;
'

select *
from @table
order by 1