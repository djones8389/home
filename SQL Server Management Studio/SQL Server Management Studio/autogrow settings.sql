DECLARE @AutoGrow TABLE
(
	Name nvarChar(100)
	, fileID tinyint
	, filename nvarchar(1000)
	, filegroup nvarchar(100)
	, size nvarchar(100)
	, maxSize nvarchar(100)
	, growth nvarchar(20)
	, usage nvarchar(30)

)
INSERT @AutoGrow
exec sp_MSforeachdb 'use [?];

IF (
''?'' not in (''msdb'',''model'',''master'',''tempdb'')
)
EXEC sp_helpfile '

SELECT * 
FROM @AutoGrow 
where fileid = 1
ORDER BY 1
