use [NMC] if not exists(select 1 from sys.database_principals where name = 'RServices_Staging')    
	 CREATE USER [RServices_Staging] FOR LOGIN [RServices_Staging];     
		ALTER ROLE [db_datareader] ADD MEMBER [RServices_Staging];
		ALTER ROLE [db_datawriter] ADD MEMBER [RServices_Staging];
		
use [NMC] if not exists(select 1 from sys.database_principals where name = 'RServices_Prod')    
	CREATE USER [RServices_Prod] FOR LOGIN [RServices_Prod];    
		ALTER ROLE [db_datareader] ADD MEMBER [RServices_Prod];
		ALTER ROLE [db_datawriter] ADD MEMBER [RServices_Prod];

use [NMC_dev] if not exists(select 1 from sys.database_principals where name = 'RServices_Staging')    
	 CREATE USER [RServices_Staging] FOR LOGIN [RServices_Staging];     
		ALTER ROLE [db_datareader] ADD MEMBER [RServices_Staging];
		ALTER ROLE [db_datawriter] ADD MEMBER [RServices_Staging];

use [NMC_dev] if not exists(select 1 from sys.database_principals where name = 'RServices_Prod')     
	CREATE USER [RServices_Prod] FOR LOGIN [RServices_Prod];     
		ALTER ROLE [db_datareader] ADD MEMBER [RServices_Prod];
		ALTER ROLE [db_datawriter] ADD MEMBER [RServices_Prod];


use [NMC]

GRANT EXECUTE ON SCHEMA::[audit] TO [RServices_Prod]

GRANT EXECUTE ON SCHEMA::[input] TO [RServices_Prod]

GRANT EXECUTE ON SCHEMA::[nmc] TO [RServices_Prod]


GRANT EXECUTE ON SCHEMA::[audit] TO [RServices_Staging]

GRANT EXECUTE ON SCHEMA::[input] TO [RServices_Staging]

GRANT EXECUTE ON SCHEMA::[nmc] TO [RServices_Staging]


use [NMC_Dev]

GRANT EXECUTE ON SCHEMA::[audit] TO [RServices_Prod]

GRANT EXECUTE ON SCHEMA::[cds] TO [RServices_Prod]

GRANT EXECUTE ON SCHEMA::[cleanup] TO [RServices_Prod]

GRANT EXECUTE ON SCHEMA::[elt] TO [RServices_Prod]

GRANT EXECUTE ON SCHEMA::[events] TO [RServices_Prod]

GRANT EXECUTE ON SCHEMA::[input] TO [RServices_Prod]

GRANT EXECUTE ON SCHEMA::[staging] TO [RServices_Prod]


GRANT EXECUTE ON SCHEMA::[audit] TO [RServices_Staging]

GRANT EXECUTE ON SCHEMA::[cds] TO [RServices_Staging]

GRANT EXECUTE ON SCHEMA::[cleanup] TO [RServices_Staging]

GRANT EXECUTE ON SCHEMA::[elt] TO [RServices_Staging]

GRANT EXECUTE ON SCHEMA::[events] TO [RServices_Staging]

GRANT EXECUTE ON SCHEMA::[input] TO [RServices_Staging]

GRANT EXECUTE ON SCHEMA::[staging] TO [RServices_Staging]


ALTER LOGIN [RServices_Staging] ENABLE;
ALTER LOGIN [RServices_Prod] ENABLE;