use Archive;

--These need adding into Archive

declare @tab table (myVal nvarchar(max))
insert @tab
values('AS_ASSET_ADDITIONAL_DATA'),('AS_ASSET_REFFERAL'),('AS_BENEFICIARY'),('AS_CARAVAN'),('AS_CARGO'),('AS_CARGO_TRANSHIPMENT'),('AS_COMPANY_CI_BUYER'),('AS_COMPANY_FIRE_INSURANCE'),('AS_COMPANY_PERSONNEL'),('AS_ENGINEERING'),('AS_EVENT'),('AS_EVENT_PARTICIPANTS'),('AS_ITEM'),('AS_LIVESTOCK'),('AS_MARINE'),('AS_MEDICAL'),('AS_MORTGAGE'),('AS_OFFICE_COMPUTER'),('AS_PA_BESPOKE_COVERAGE_HSCX'),('AS_PERSON'),('AS_PERSON_DISEASE_CODES'),('AS_PERSON_DOMESTIC'),('AS_PERSON_ICD_CODES'),('AS_PERSON_PROFESSIONS'),('AS_PERSON_SPORT_ACTIVITIES'),('AS_PI_TECHNOLOGY_HOSTING_PROVIDERS_HSCX'),('AS_PROPERTY_OFFICE_DETAILS'),('AS_PROPERTY_ROOMS'),('AS_PROPERTY_THEFT'),('AS_PROPERTY_THEFT_ITEM'),('AS_SUBSIDIARY_TURNOVER_BREAKDOWN_HSCX'),('AS_TRAVEL_FAMILY_MEMBERS'),('AS_TRAVEL_VEHICLE'),('AS_VEHICLE_ANTITHEFTS'),('AS_VEHICLE_USAGE'),('C_DRIVER'),('C_MOTOR_CLAIM_MATCH'),('C_MOTOR_CLAIM_REQUEST'),('T_AGE_BAND_HSCX'),('T_ASSET_ITEM_WORN_BY_HSCX'),('T_CYBER_CRIME_COVER_HSCX'),('T_CYBER_DATA_CATEGORY_HSCX'),('T_CYBER_DATA_CYBER_ESSENTIALS_LIST_OF_VALUES_HSCX'),('T_CYBER_DATA_DEPENDENT_BI_COVER_HSCX'),('T_CYBER_DATA_DISASTER_RECOVERY_HSCX'),('T_CYBER_DATA_DUE_DILIGENCE_HSCX'),('T_CYBER_DATA_INFORMATION_SECURITY_HSCX'),('T_CYBER_DATA_RECORDS_HSCX'),('T_CYBER_DATA_RISK_TYPE_HSCX'),('T_CYBER_DATA_SENSITIVE_INFORMATION_RECORDS_HSCX'),('T_CYBER_DATA_SYSTEM_FAILURE_BI_COVER_HSCX'),('T_CYBER_DATA_TEST_SYSTEMS_HSCX'),('T_CYBER_DATA_TIME_EXCESS_HSCX'),('T_CYBER_DATA_TRAINING_PROGRAMME_HSCX'),('T_CYBER_PROPERTY_SI_TYPE_HSCX'),('T_DRIVER_EXPERIENCE'),('T_DRIVER_TYPES'),('T_DRIVING_LICENSE_TYPE'),('T_DRIVING_PERMISSION'),('T_FAMILY_STATUS'),('T_HOSTING_PROVIDERS_HSCX'),('T_ITEM_GROUP'),('T_MAIN_MODEL'),('T_MAIN_VEHICLE'),('T_MAIN_VEHICLE_TYPES'),('T_MODEL'),('T_MQ_DESTINATION'),('T_PRODUCT_DRIVER_TYPES'),('T_PRODUCT_VEHICLE_USAGE'),('T_REFERENCE_CLASS '),('T_VEHICLE_BODY_STYLE'),('T_VEHICLE_GEAR'),('T_VEHICLE_MORTGAGE_TYPES'),('T_VEHICLE_ORIGIN'),('T_VEHICLE_POWER_TYPES'),('T_VEHICLE_PRIOR_DAMAGES'),('T_VEHICLE_PRIOR_USE'),('T_VEHICLE_TYPE_BY_MAIN_TYPE'),('T_VEHICLE_TYPES'),('T_VEHICLE_USAGE')

select myVal
from @tab
except
select name
from sys.tables a
order by 1

/*

1) enable tables for cdc on IDIT

2) add tables to archive (ensure every column is NULL and no defaults) 

3) check rowcounts aren't mega 

4) add into cdc tables, e.g.
 
	INSERT [CdcManagement].[dbo].[cdc_states] VALUES ('IDIT.T_PA_ILLNESS_COVERED_HSCX','')	

	INSERT [CdcManagement].[Control].[CdcExecutionControl] VALUES ('IDIT','T_PA_ILLNESS_COVERED_HSCX','1')

5) use VS to generate the 49 new ones  

6) backup the master package > store it in SAFE folder

7) update ssis master package 

8) copy & paste over the top in E:\SSIS Packages


*/

	INSERT [CdcManagement].[dbo].[cdc_states] VALUES ('IDIT.T_CYBER_DATA_PRIMARY_OPERATING_SYSTEM_HSCX','')	

	INSERT [CdcManagement].[Control].[CdcExecutionControl] VALUES ('IDIT','T_CYBER_DATA_PRIMARY_OPERATING_SYSTEM_HSCX','1')

	INSERT [CdcManagement].[dbo].[cdc_states] VALUES ('IDIT.T_CYBER_DATA_ONLINE_SALES_REVENUE_HSCX','')	

	INSERT [CdcManagement].[Control].[CdcExecutionControl] VALUES ('IDIT','T_CYBER_DATA_ONLINE_SALES_REVENUE_HSCX','1')

	INSERT [CdcManagement].[dbo].[cdc_states] VALUES ('IDIT.T_BI_DEPENDENT_KEY_PROVIDERS_HSCX','')	

	INSERT [CdcManagement].[Control].[CdcExecutionControl] VALUES ('IDIT','T_BI_DEPENDENT_KEY_PROVIDERS_HSCX','1')


	INSERT [CdcManagement].[dbo].[cdc_states] VALUES ('IDIT.AS_DEPENDENT_BI_KEY_PROVIDERS_HSCX','')	

	INSERT [CdcManagement].[Control].[CdcExecutionControl] VALUES ('IDIT','AS_DEPENDENT_BI_KEY_PROVIDERS_HSCX','1')

	INSERT [CdcManagement].[dbo].[cdc_states] VALUES ('IDIT.T_EVENT_TYPE_HSCX','')	

	INSERT [CdcManagement].[Control].[CdcExecutionControl] VALUES ('IDIT','T_EVENT_TYPE_HSCX','1')

	INSERT [CdcManagement].[dbo].[cdc_states] VALUES ('IDIT.SH_BATCH_LOG','')	

	INSERT [CdcManagement].[Control].[CdcExecutionControl] VALUES ('IDIT','SH_BATCH_LOG','1')

	INSERT [CdcManagement].[dbo].[cdc_states] VALUES ('IDIT.ST_TASK_LOG','')	

	INSERT [CdcManagement].[Control].[CdcExecutionControl] VALUES ('IDIT','ST_TASK_LOG','1')

	INSERT [CdcManagement].[dbo].[cdc_states] VALUES ('IDIT.T_TOT_VERSION_LINES','')	

	INSERT [CdcManagement].[Control].[CdcExecutionControl] VALUES ('IDIT','T_TOT_VERSION_LINES','1')

	EXEC sys.sp_cdc_enable_table @source_schema = 'dbo', @source_name   = 'T_CYBER_DATA_PRIMARY_OPERATING_SYSTEM_HSCX', @capture_instance = 'T_CYBER_DATA_PRIMARY_OPERATING_SYSTEM_HSCX', @role_name = null
EXEC sys.sp_cdc_enable_table @source_schema = 'dbo', @source_name   = 'T_CYBER_DATA_ONLINE_SALES_REVENUE_HSCX', @capture_instance = 'T_CYBER_DATA_ONLINE_SALES_REVENUE_HSCX', @role_name = null
EXEC sys.sp_cdc_enable_table @source_schema = 'dbo', @source_name   = 'T_BI_DEPENDENT_KEY_PROVIDERS_HSCX', @capture_instance = 'T_BI_DEPENDENT_KEY_PROVIDERS_HSCX', @role_name = null
EXEC sys.sp_cdc_enable_table @source_schema = 'dbo', @source_name   = 'AS_DEPENDENT_BI_KEY_PROVIDERS_HSCX', @capture_instance = 'AS_DEPENDENT_BI_KEY_PROVIDERS_HSCX', @role_name = null
EXEC sys.sp_cdc_enable_table @source_schema = 'dbo', @source_name   = 'T_EVENT_TYPE_HSCX', @capture_instance = 'T_EVENT_TYPE_HSCX', @role_name = null
EXEC sys.sp_cdc_enable_table @source_schema = 'dbo', @source_name   = 'SH_BATCH_LOG', @capture_instance = 'SH_BATCH_LOG', @role_name = null
EXEC sys.sp_cdc_enable_table @source_schema = 'dbo', @source_name   = 'ST_TASK_LOG', @capture_instance = 'ST_TASK_LOG', @role_name = null
EXEC sys.sp_cdc_enable_table @source_schema = 'dbo', @source_name   = 'T_TOT_VERSION_LINES', @capture_instance = 'T_TOT_VERSION_LINES', @role_name = null
