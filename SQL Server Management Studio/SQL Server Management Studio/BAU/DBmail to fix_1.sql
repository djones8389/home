/****** Script for SelectTopNRows command from SSMS  ******/
SELECT TOP (1000) a.[ServerID]
      ,[DBMailEnabled]
      ,[NoOfMailProfiles]
      ,[DBMailTested]
      ,[DBMailTestResult]
	  , b.*
  FROM [BAU].[dbo].[DBMail] a
  inner join Instances b
  on a.ServerID =b.ServerID
  where a.DBMailTestResult = 0
	  
	--pe02036d26fd-00.azure.hiscox.com fine

	SELECT j.name, c.name, j.enabled
	FROM msdb.dbo.sysjobs j
	INNER JOIN msdb.dbo.syscategories c on j.category_id = c.category_id
	WHERE c.name in ('AG_Enabled', 'AG_Disabled', 'DATABASE MAINTENANCE', 'FIM')
