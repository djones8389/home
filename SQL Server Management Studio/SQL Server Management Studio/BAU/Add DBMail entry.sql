use BAU

select i.HostName
	, i.InstanceName
	, i.ConnectionString
	, 'insert DBMail ([ServerID], [DBMailEnabled], [NoOfMailProfiles], [DBMailTested], [DBMailTestResult]) values ('+cast(i.ServerID as varchar(5))+',1,1,1,1)' [Insert DBMail]
from Instances i
left join DBMail d
on d.ServerID = i.ServerID
where d.DBMailEnabled is null
	and hostname = 'pr0203-03001-02.hiscox.com'
order by 2


