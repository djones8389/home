/****** Script for SelectTopNRows command from SSMS  ******/
SELECT distinct [OperatorName]
      ,[EmailAddress]
      --,[IsEnabled]
  FROM [BAU].[dbo].[Operators]
where OperatorName in ('SQL_DBAs','DBA','DBAs')
	or  emailaddress in ('!ITDBATeam@hiscox.com','!SQLAlerts@hiscox.com','!SQLAlerts@hiscox.nonprod','dbateam@hiscox.nonprod','dbateam@hiscox.com')
	order by 1;

 /****** Script for SelectTopNRows command from SSMS  ******/
SELECT *
  FROM [BAU].[dbo].[Operators] o
  inner join Instances i 
  on i.ServerID = o.ServerID
  --inner join Jobs j 
  --on j.ServerID = o.ServerID
	 --and o.OperatorName = j.OperatorID
where (
    OperatorName in ('SQL_DBAs','DBA','DBAs','DBA Team')
	or  
	emailaddress in ('!ITDBATeam@hiscox.com','!SQLAlerts@hiscox.com','!SQLAlerts@hiscox.nonprod','dbateam@hiscox.nonprod','dbateam@hiscox.com')
	)
	and HostName like 'hxe%' --or HostName like 'aphxe%'
	
	order by 1;

