USE [BAU]
GO

/****** Object:  StoredProcedure [dbo].[Get-ADLogins]    Script Date: 08/02/2019 12:42:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[Get-ADLogins]
AS

select * 
from [SQL AD Logins] 
UNION
select * 
from [CDR_NONPROD].[BAU].[dbo].[SQL AD Logins]
GO


