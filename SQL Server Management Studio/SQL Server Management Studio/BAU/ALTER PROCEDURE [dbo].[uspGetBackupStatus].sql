USE [SANDBOX_SQL_Inventory]
GO
ALTER PROCEDURE [dbo].[uspGetBackupStatus]
AS
BEGIN

	WITH i
		 AS (SELECT CASE
					  WHEN Isnull(s.hostID, '') <> '' THEN
						CASE
						  WHEN s.instanceName = 'MSSQLSERVER' THEN Ltrim(
						  Rtrim(LEFT(h.hostName,
								Charindex('.',
								h.hostName) - 1)))
						  ELSE Ltrim(Rtrim(LEFT(h.hostName, Charindex('.',
							   h.hostName)
							   - 1))
							   )
							   + '\' + s.instanceName
						END
					  WHEN Isnull(s.clusterID, '') <> '' THEN
						CASE
						  WHEN s.instanceName = 'MSSQLSERVER' THEN Ltrim(
						  Rtrim(LEFT(c.SQLClusterName,
								Charindex('.',
								h.hostName) - 1)))
						  ELSE Ltrim(Rtrim(LEFT(c.SQLClusterName, Charindex('.',
							   c.SQLClusterName)
							   - 1))
							   )
							   + '\' + s.instanceName
						END
					END AS InstanceName
			FROM   [SQL_Inventory].[dbo].[Servers] s
			LEFT JOIN [SQL_Inventory].[dbo].[Hosts] h
			ON s.hostID = h.hostID
			LEFT JOIN [SQL_Inventory].[dbo].[Clusters] c
			ON s.clusterID = c.clusterID
			INNER JOIN [SQL_Inventory].[dbo].[Environments] e
			ON s.status = e.ShortCode
			 WHERE  s.serverState <> 'Decommissioned'
			 		AND e.Environment = 'Production')
	SELECT distinct b.ServerInstance [InstanceName],
		   b.[Database],
		   b.[LastCommvaultFull],
		   DATEDIFF("hh",b.[LastCommvaultFull],GETDATE()) AS HoursSinceFull,
		   b.[LastTran],
		   DATEDIFF("hh",b.[LastTran],GETDATE()) AS HoursSinceLog,
		   b.[LastDiff],
		   DATEDIFF("hh",b.[LastDiff],GETDATE()) AS HoursSinceDiff,
		   b.[PopulatedDate],
		   b.[RecoveryMode],
		   b.[DBCreationTime],
		   b.[DBStatus],
		   b.[AGRole]
	FROM   [SQL_Inventory].[dbo].[SQLBackupStatus] b
	LEFT JOIN SQLBackupExceptions X
	ON x.ServerInstance = b.ServerInstance
		and x.[Database] = b.[Database]
	WHERE b.ServerInstance NOT LIKE 'mod%'
		and x.ServerInstance is null
		AND (b.[AGRole] IS NULL OR b.[AGRole] = 'PRIMARY')
		and [Is_Read_Only] = 0
		--and ServerInstance like 'hxp33663%' -- or ServerInstance like 'hxp20241%'
	ORDER  BY InstanceName ASC 
  
END