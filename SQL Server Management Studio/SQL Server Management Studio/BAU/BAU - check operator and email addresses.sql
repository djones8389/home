USE [BAU]
SELECT  [OperatorName]
      ,[EmailAddress]
      , I.ConnectionString
FROM [BAU].[dbo].[Operators] o
inner join Instances I
on I.ServerID = o.ServerID
where OperatorName not in ('DBA Team')
	and EmailAddress = 'Dave.Jones@Hiscox.com'

	 