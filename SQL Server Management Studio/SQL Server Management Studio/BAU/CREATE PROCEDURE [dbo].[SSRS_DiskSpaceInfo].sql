USE [SQL_Inventory]
GO

/****** Object:  StoredProcedure [dbo].[SSRS_FailedJobInfo]    Script Date: 29/03/2019 10:41:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SSRS_DiskSpaceInfo] 

	@Servername varchar(100)
	, @VolumeName varchar(100)
	, @Comment varchar(200)

AS
 
BEGIN TRY
    BEGIN TRANSACTION;

		DECLARE @Source TABLE (
			Servername varchar(100)
			, VolumeName varchar(100)
			, comment varchar(200)
		)

		INSERT @Source(Servername, VolumeName, Comment)
		SELECT @Servername, @VolumeName, @Comment

		MERGE INTO Drive_space_additional_info AS TGT
		USING (
			SELECT Servername,VolumeName,[comment]
			FROM @Source
		) as SRC
		on TGT.hostname = SRC.Servername
			and TGT.VolumeName = SRC.VolumeName
			
		WHEN NOT MATCHED THEN
		INSERT([hostname], VolumeName, [recorded_date], [owner], [comment])
		VALUES(SRC.Servername, SRC.VolumeName, getdate(), SUSER_SNAME(SUSER_SID()), SRC.[comment])
		WHEN MATCHED THEN
		UPDATE
		SET [recorded_date] = GETDATE()
			, [owner] = SUSER_SNAME(SUSER_SID())
			, [comment] = SRC.[comment];

    COMMIT TRANSACTION;
    
END TRY
BEGIN CATCH

	SELECT ERROR_NUMBER() AS ErrorNumber
		, ERROR_SEVERITY() AS ErrorSeverity
		, ERROR_STATE() AS ErrorState
		, ERROR_PROCEDURE() AS ErrorProcedure
		, ERROR_LINE() AS ErrorLine
		, ERROR_MESSAGE() AS ErrorMessage

END CATCH;

GO


