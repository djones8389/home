--Run from HXP20203\DBA01

EXEC [SQL_Inventory].[dbo].[SSRS_DiskSpaceInfo]
		@Servername = N'hxf20240.hiscox.com',
		@VolumeName = N'F:\',
		@Comment = N'Daily backup from Prod is causing this.  Isn''t likely to spike beyond this anytime soon'

EXEC [SQL_Inventory].[dbo].[SSRS_FailedJobInfo]
		@hostname = N'pr03039lueoh-00.azure.hiscox.com',
		@instanceName = N'MSSQLSERVER',
		@jobName = N'CDR: Index Optimise - All DBs (Daily)',
		@Comment = N'Someone rebooted this server at 06:34.  Will run fine tomorrow.'
