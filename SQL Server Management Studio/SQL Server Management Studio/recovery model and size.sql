select db_name(a.database_id) DatabaseName
	, physical_name
	, size/128 [Size MB]
	, recovery_model_desc
from sys.master_files a
inner join sys.databases b
on b.name = db_name(a.database_id)
where type_desc = 'ROWS'
	and a.database_id > 4
	--and physical_name like 'D:\%'
	 order by 3 desc

--dbcc sqlperf(logspace)

--select SUM(size/128) [Size MB]	 
--	, SUM(size/128)/1024 [Size GB]	 
--from sys.master_files a
--inner join sys.databases b
--on b.name = db_name(a.database_id)
--where type_desc = 'ROWS'
--	and a.database_id > 4
 

