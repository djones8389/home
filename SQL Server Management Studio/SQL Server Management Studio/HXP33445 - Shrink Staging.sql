USE [Staging]; 

DBCC SHRINKFILE ('BI_Staging',479241);  /* Space we are creating:  237559.00 MB (Shrinking from: 716800.00 to: 479241)*/

DECLARE @Staging nvarchar(MAX) = '';

SELECT @Staging +=CHAR(13) +
		'ALTER INDEX ' + quotename(C.name) +  ' ON ' + quotename(SCHEMA_NAME(SCHEMA_ID)) + '.' + quotename(B.name) 
			+ CASE
				WHEN avg_fragmentation_in_percent BETWEEN 5 AND 30 THEN ' REORGANIZE'
				ELSE ' REBUILD'
			END + ';'
FROM sys.dm_db_index_physical_stats(DB_ID(),NULL,NULL,NULL,NULL) A
INNER JOIN sys.objects B
ON A.object_id = B.object_id
INNER JOIN sys.indexes C
ON B.object_id = C.object_id AND A.index_id = C.index_id
INNER JOIN sys.partitions D
ON B.object_id = D.object_id AND A.index_id = D.index_id
WHERE C.index_id > 0
	and A.avg_fragmentation_in_percent > 5

EXEC(@Staging);

EXEC sp_updatestats;