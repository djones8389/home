USE [master];

EXEC sp_MSforeachdb '

use [?];

if(DB_ID()>4)

BEGIN

	 
	SELECT ''?'' [DatabaseName]
		,t.name [TableName]
	FROM sys.tables t
	INNER join sys.indexes i
	ON i.object_id = t.object_id
	WHERE index_id = 1				/*Clustered*/
		AND t.modify_date > DATEADD(HOUR,-24,GETDATE())			/* Where the object has been recently modified */
			OR t.create_date > DATEADD(HOUR,-24,GETDATE())      /* ...or created (maybe using DROP and CREATE */
END
';