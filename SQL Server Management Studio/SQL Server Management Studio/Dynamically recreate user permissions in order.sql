DECLARE @Statement table (
	[Order] tinyint
	,[Statement] nvarchar(MAX)
)
INSERT @Statement
SELECT A.*
FROM (

	SELECT 1 [Order], 'IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = '''+ name + ''') ' + char(13) 
	 + 'CREATE USER ' + quotename(cast(name as nvarchar(100))) + ' FOR LOGIN ' +quotename(cast(name as nvarchar(100))) + ' WITH DEFAULT_SCHEMA = dbo' [1-Create user]
	FROM sys.database_principals 
	--WHERE  name like 'NONPROD%'
	--type IN ('S', 'U') 
		where principal_id > 4
	
	UNION

	SELECT 2 [Order], 'EXEC sys.sp_addrolemember @rolename = ' + CAST(b.name as nvarchar(100)) + ' , @membername = ''' + cast(c.name as nvarchar(100)) + '''' [2-sp_addrolemember]
	from sys.database_role_members a 
	INNER JOIN sys.database_principals b on a.role_principal_id = b.principal_id                                 
	INNER JOIN sys.database_principals c on a.member_principal_id = c.principal_id
	WHERE   c.name != 'dbo'

	UNION
	
	SELECT
		  3 [Order], 'GRANT '+permission_name+' ON OBJECT::'+object_name(major_id)+' TO '+quotename(b.name) collate Latin1_General_CI_AS+ '' [3-GRANT]
		 --,b.name
	from sys.database_permissions a
	inner join sys.database_principals b
	on a.grantee_principal_id = b.principal_id
	where b.name != 'public'
		and object_name(major_id)  is not null
) A

DECLARE @Dynamic nvarchar(MAX) = '';
SELECT @Dynamic +=CHAR(13) + 
	[Statement] 
FROM @Statement 
order by [Order];

print(@Dynamic);

