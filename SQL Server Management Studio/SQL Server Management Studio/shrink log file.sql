set nocount on;

declare @log table (
	DatabaseName sysname
	, [Log Size (MB)] decimal (8,2)
	, [Log Space Used (%)] decimal (8,2)
	, [Status] bit
)

insert @log
exec ('dbcc sqlperf(logspace) WITH NO_INFOMSGS')

declare @execute nvarchar(MAX)='';

select @execute+=char(13) 
	+ 'use ' + quotename(a.databasename) + ' DBCC SHRINKFILE ('''+ b.name + ''', 1024)' 
from @log a
inner join sys.master_files b
on a.DatabaseName = db_name(b.database_id)
where DatabaseName not in ('model','tempdb','master','msdb','cdr_local')
	and b.type_desc = 'log'

print(@execute)
