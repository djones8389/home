USE [ODS_RAQ]
GO

/****** Object:  StoredProcedure [dbo].[DeDupe_tblUsers]    Script Date: 13/12/2018 17:14:12 ******/
DROP PROCEDURE [dbo].[DeDupe_tblUsers]
GO

/****** Object:  StoredProcedure [dbo].[DeDupe_tblUsers]    Script Date: 13/12/2018 17:14:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


	
		create procedure [dbo].[DeDupe_tblUsers] (
			@Date date,
			@DuplicateCount int output
		) AS
		
		delete 
		from [ODS_RAQ].[dbo].[tblUsers]
		where ods_DateFrom = ods_DateTo
			and ods_DateTo <> '31 Dec 2999'

		;with duplicates as (
			select count(*) ods_RecordCount, min(ods_DateFrom) min_ods_DateFrom, max(ods_DateFrom) max_ods_DateFrom, max(ods_DateTo) max_ods_DateTo
				,[UserId],[UserName],[EmailAddress],[Rowguid],[AddNewPolicies],[ViewExistingPolicies],[ViewStatistics],[AgentID],[ViewManagementInfo],[FullName],[DepartmentID],[JobTitle],[PhoneNumber],[Blocked],[UserGroup],[PasswordLastChanged],[LanguageID],[AuthorityLevel],[DocFolder],[DocFolderLocation]
			from [ODS_RAQ].[dbo].[tblUsers]
			group by [UserId],[UserName],[EmailAddress],[Rowguid],[AddNewPolicies],[ViewExistingPolicies],[ViewStatistics],[AgentID],[ViewManagementInfo],[FullName],[DepartmentID],[JobTitle],[PhoneNumber],[Blocked],[UserGroup],[PasswordLastChanged],[LanguageID],[AuthorityLevel],[DocFolder],[DocFolderLocation]
			having COUNT(*) > 1
		)
		select A.[min_ods_DateFrom], A.[max_ods_DateFrom], A.[max_ods_DateTo], A.[ods_RecordCount]
			,A.[UserId],A.[UserName],A.[EmailAddress],A.[Rowguid],A.[AddNewPolicies],A.[ViewExistingPolicies],A.[ViewStatistics],A.[AgentID],A.[ViewManagementInfo],A.[FullName],A.[DepartmentID],A.[JobTitle],A.[PhoneNumber],A.[Blocked],A.[UserGroup],A.[PasswordLastChanged],A.[LanguageID],A.[AuthorityLevel],A.[DocFolder],A.[DocFolderLocation]
		into #temp
		from duplicates A
			join [ODS_RAQ].[dbo].[tblUsers] B on A.[UserName] = B.[UserName]
				and A.[min_ods_DateFrom] <= B.[ods_DateFrom]
				and A.[max_ods_DateFrom] >= B.[ods_DateFrom]
		group by A.[min_ods_DateFrom], A.[max_ods_DateFrom], A.[max_ods_DateTo], A.[ods_RecordCount]
			,A.[UserId],A.[UserName],A.[EmailAddress],A.[Rowguid],A.[AddNewPolicies],A.[ViewExistingPolicies],A.[ViewStatistics],A.[AgentID],A.[ViewManagementInfo],A.[FullName],A.[DepartmentID],A.[JobTitle],A.[PhoneNumber],A.[Blocked],A.[UserGroup],A.[PasswordLastChanged],A.[LanguageID],A.[AuthorityLevel],A.[DocFolder],A.[DocFolderLocation]
		having Count(*) = A.[ods_RecordCount]
		
		if @@ROWCOUNT <> 0 begin
			delete A
			from [ODS_RAQ].[dbo].[tblUsers] A
			join #temp B on A.[UserName] = B.[UserName]
				and (A.ods_DateFrom >= B.min_ods_DateFrom and A.ods_DateFrom <= B.max_ods_DateFrom)

			insert into [ODS_RAQ].[dbo].[tblUsers] (
				ods_DateFrom, ods_DateTo,[UserId],[UserName],[EmailAddress],[Rowguid],[AddNewPolicies],[ViewExistingPolicies],[ViewStatistics],[AgentID],[ViewManagementInfo],[FullName],[DepartmentID],[JobTitle],[PhoneNumber],[Blocked],[UserGroup],[PasswordLastChanged],[LanguageID],[AuthorityLevel],[DocFolder],[DocFolderLocation]
			)
			select min_ods_DateFrom, max_ods_DateTo,[UserId],[UserName],[EmailAddress],[Rowguid],[AddNewPolicies],[ViewExistingPolicies],[ViewStatistics],[AgentID],[ViewManagementInfo],[FullName],[DepartmentID],[JobTitle],[PhoneNumber],[Blocked],[UserGroup],[PasswordLastChanged],[LanguageID],[AuthorityLevel],[DocFolder],[DocFolderLocation]
			from #temp 

			set @DuplicateCount = @@ROWCOUNT
		end else begin
			set @DuplicateCount = 0
		end


	
GO


