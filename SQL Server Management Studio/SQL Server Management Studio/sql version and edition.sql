SELECT  @@VERSION as SQLInstance
	, SERVERPROPERTY('Edition') AS Edition
	, SERVERPROPERTY('ProductLevel') AS ProductLevel
	, SERVERPROPERTY('ProductVersion') AS ProductVersion
    , SERVERPROPERTY('PatchLevel') as PatchLevel