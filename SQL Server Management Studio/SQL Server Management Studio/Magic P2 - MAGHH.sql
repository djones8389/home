/****** Script for SelectTopNRows command from SSMS  ******/
SELECT [POLISN]
      ,[POLNUM]
      ,[MODULE]
      ,[CLTNUM]
      ,[BRKNUM]
      ,[ORGINC]
      ,[PERMTH]
      ,[LSTEVT]
      ,[BRKREF]
      ,[PRVCLM]
      ,[OPNDAT]
      ,[OPNUSR]
      ,[CTYTAX]
      ,[POLSTS]
      ,[STSDAT]
      ,[STSTIM]
      ,[STSUSR]
      ,[ENDDAT]
      ,[SUBBRF]
      ,[INTBRK]
      ,[NOTSTS]
      ,[FACREF]
      ,[POLDSC]
      ,[PRDGRP]
      ,[REVRQD]
      ,[LOGDLT]
      ,[SUBBRK]
      ,[INDIDF]
      ,[LVLTYP]
      ,[BUSIDC]
      ,[POLSTE]
      ,[BNDATH]
      ,[REVTXT]
      ,[RUNOFF]
      ,[MOPCOD]
      ,[BRKRF2]
      ,[BNDINF]
      ,[Department]
      ,[Revenue]
      ,[Reinsurer_Number]
      ,[International]
      ,[Project_Num]
      ,[Reinsured_Ref]
      ,[InsuranceType]
      ,[UWCountry]
      ,[WWLoB]
      ,[RowID]
      ,[CDCOperation]
      ,[CDCDateTime]
      ,[ArchiveDateTime]
      ,[SourceSystemCode]
      ,[Auditable]
      ,[DocSigned]
      ,[RenContactNo]
      ,[USBrokerStarRating]
      ,[PaymentCardRecords]
      ,[PersonalRecords]
  FROM [Archive].[Magic].[POLICIES] NOLOCK
  where [SourceSystemCode] = 'MHH'
	and CDCDateTime > '2019-06-21 10:15:00'
	and STSUSR = 'MALLETTC'
   