SELECT b.name, a.retention, b.enabled, b.date_created, b.date_modified, a.*
FROM msdb.dbo.cdc_jobs a
       INNER JOIN msdb..sysjobs b ON b.job_id = a.job_id

--7200
use idit
--exec sp_cdc_change_job @job_type='cleanup', @retention=7200
--10800 = 7 days