/***	read the current SQL Server default backup location	***/  
DECLARE @BackupDirectory NVARCHAR(100)   
EXEC master..xp_instance_regread @rootkey = 'HKEY_LOCAL_MACHINE',  
    @key = 'Software\Microsoft\MSSQLServer\MSSQLServer',  
    @value_name = 'BackupDirectory', @BackupDirectory = @BackupDirectory OUTPUT ;  
 
SELECT @BackupDirectory AS [SQL Server default backup Value]
SELECT @BackupDirectory = 'T:\SQL_Backup'


SELECT 'BACKUP DATABASE [' + name + '] TO  DISK = N'''+@BackupDirectory+'\BBDatabases.bak'' WITH  COPY_ONLY, NOINIT,   NAME = N''' + name + '-Full Database Backup'', COMPRESSION,  STATS = 10;'
FROM sys.databases
WHERE database_id > 4
	and name <> 'CDR_Local'
order by 1;

--BACKUP DATABASE [cloudci-contentservices] TO  DISK = N'T:\SQL_Backup\BBDatabases.bak' WITH  COPY_ONLY, NOINIT,   NAME = N'cloudci-contentservices-Full Database Backup', COMPRESSION,  STATS = 10;
--BACKUP DATABASE [cloudci-integration] TO  DISK = N'T:\SQL_Backup\BBDatabases.bak' WITH  COPY_ONLY, NOINIT,   NAME = N'cloudci-integration-Full Database Backup', COMPRESSION,  STATS = 10;
--BACKUP DATABASE [cloudci-orchestrator] TO  DISK = N'T:\SQL_Backup\BBDatabases.bak' WITH  COPY_ONLY, NOINIT,   NAME = N'cloudci-orchestrator-Full Database Backup', COMPRESSION,  STATS = 10;
--BACKUP DATABASE [cloudci-portal] TO  DISK = N'T:\SQL_Backup\BBDatabases.bak' WITH  COPY_ONLY, NOINIT,   NAME = N'cloudci-portal-Full Database Backup', COMPRESSION,  STATS = 10;
--BACKUP DATABASE [production-contentservices] TO  DISK = N'T:\SQL_Backup\BBDatabases.bak' WITH  COPY_ONLY, NOINIT,   NAME = N'production-contentservices-Full Database Backup', COMPRESSION,  STATS = 10;
--BACKUP DATABASE [production-integration] TO  DISK = N'T:\SQL_Backup\BBDatabases.bak' WITH  COPY_ONLY, NOINIT,   NAME = N'production-integration-Full Database Backup', COMPRESSION,  STATS = 10;
--BACKUP DATABASE [production-orchestrator] TO  DISK = N'T:\SQL_Backup\BBDatabases.bak' WITH  COPY_ONLY, NOINIT,   NAME = N'production-orchestrator-Full Database Backup', COMPRESSION,  STATS = 10;
--BACKUP DATABASE [production-portal] TO  DISK = N'T:\SQL_Backup\BBDatabases.bak' WITH  COPY_ONLY, NOINIT,   NAME = N'production-portal-Full Database Backup', COMPRESSION,  STATS = 10;
--BACKUP DATABASE [staging-contentservices] TO  DISK = N'T:\SQL_Backup\BBDatabases.bak' WITH  COPY_ONLY, NOINIT,   NAME = N'staging-contentservices-Full Database Backup', COMPRESSION,  STATS = 10;
--BACKUP DATABASE [staging-integration] TO  DISK = N'T:\SQL_Backup\BBDatabases.bak' WITH  COPY_ONLY, NOINIT,   NAME = N'staging-integration-Full Database Backup', COMPRESSION,  STATS = 10;
--BACKUP DATABASE [staging-orchestrator] TO  DISK = N'T:\SQL_Backup\BBDatabases.bak' WITH  COPY_ONLY, NOINIT,   NAME = N'staging-orchestrator-Full Database Backup', COMPRESSION,  STATS = 10;
--BACKUP DATABASE [staging-portal] TO  DISK = N'T:\SQL_Backup\BBDatabases.bak' WITH  COPY_ONLY, NOINIT,   NAME = N'staging-portal-Full Database Backup', COMPRESSION,  STATS = 10;