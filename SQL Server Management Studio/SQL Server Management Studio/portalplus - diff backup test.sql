--commvault  @ 21:00

	backup database CDR_Local to disk = '\\hiscox.com\backup\ReleaseDump\PortalPlus\CDR_Local_full.bak'

--native every 15 mins

	backup log CDR_Local to disk = '\\hiscox.com\backup\ReleaseDump\PortalPlus\CDR_Local_log1.trn'
	
	use [CDR_Local]; create table [mytable] (id int);

	backup log CDR_Local to disk = '\\hiscox.com\backup\ReleaseDump\PortalPlus\CDR_Local_log2.trn'

	backup log CDR_Local to disk = '\\hiscox.com\backup\ReleaseDump\PortalPlus\CDR_Local_log3.trn'

--Glynn

	backup database CDR_Local 
		to disk = '\\hiscox.com\backup\ReleaseDump\PortalPlus\CDR_Local_diff.bak' with differential

--Native every 15 mins - disable?

	backup log CDR_Local to disk = '\\hiscox.com\backup\ReleaseDump\PortalPlus\CDR_Local_log4.trn'

	use master
	restore database CDR_Local 
	from disk = '\\hiscox.com\backup\ReleaseDump\PortalPlus\CDR_Local_full.bak'
	with norecovery, replace

	restore database CDR_Local 
	from disk = '\\hiscox.com\backup\ReleaseDump\PortalPlus\CDR_Local_diff.bak'
	with recovery


--Glynn @08:00
--Stop the sites - no new data written to the databases
--Disable the T-log backups
--Runs a Diff backup > email confirmation of this
--Run a deployment
	--On success > enable t-log backups > disable login
	--On fail > rollback to the diff (manual dba task)

--exec msdb.dbo.sp_update_job @job_name = 'CDR: Backup Log - All DBs', @enabled = 0;