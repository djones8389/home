--prodalpha-idit - Disable/re-enable CDC on affected tables

--Refresh tables

EXEC sys.sp_cdc_disable_table @source_schema = 'dbo', @source_name   = 'AC_INSTALLMENT_HSCX', @capture_instance = 'AC_INSTALLMENT_HSCX'
EXEC sys.sp_cdc_enable_table @source_schema = 'dbo', @source_name   = 'AC_INSTALLMENT_HSCX', @capture_instance = 'AC_INSTALLMENT_HSCX', @role_name = null

EXEC sys.sp_cdc_disable_table @source_schema = 'dbo', @source_name   = 'AS_COMPANY_CYBER_DATA_HSCX', @capture_instance = 'AS_COMPANY_CYBER_DATA_HSCX'
EXEC sys.sp_cdc_enable_table @source_schema = 'dbo', @source_name   = 'AS_COMPANY_CYBER_DATA_HSCX', @capture_instance = 'AS_COMPANY_CYBER_DATA_HSCX', @role_name = null

EXEC sys.sp_cdc_disable_table @source_schema = 'dbo', @source_name   = 'T_BUSINESS_EXCEPTION', @capture_instance = 'T_BUSINESS_EXCEPTION'
EXEC sys.sp_cdc_enable_table @source_schema = 'dbo', @source_name   = 'T_BUSINESS_EXCEPTION', @capture_instance = 'T_BUSINESS_EXCEPTION', @role_name = null

EXEC sys.sp_cdc_disable_table @source_schema = 'dbo', @source_name   = 'P_POLICY_LOB_ASSET_HSCX', @capture_instance = 'P_POLICY_LOB_ASSET_HSCX'
EXEC sys.sp_cdc_enable_table @source_schema = 'dbo', @source_name   = 'P_POLICY_LOB_ASSET_HSCX', @capture_instance = 'P_POLICY_LOB_ASSET_HSCX', @role_name = null

EXEC sys.sp_cdc_disable_table @source_schema = 'dbo', @source_name   = 'P_POLICY_HSCX', @capture_instance = 'P_POLICY_HSCX'
EXEC sys.sp_cdc_enable_table @source_schema = 'dbo', @source_name   = 'P_POLICY_HSCX', @capture_instance = 'P_POLICY_HSCX', @role_name = null

EXEC sys.sp_cdc_disable_table @source_schema = 'dbo', @source_name   = 'P_POL_HEADER_HSCX', @capture_instance = 'P_POL_HEADER_HSCX'
EXEC sys.sp_cdc_enable_table @source_schema = 'dbo', @source_name   = 'P_POL_HEADER_HSCX', @capture_instance = 'P_POL_HEADER_HSCX', @role_name = null



--New tables

EXEC sys.sp_cdc_enable_table @source_schema = 'dbo', @source_name   = 'T_CYBER_DATA_PRIMARY_OPERATING_SYSTEM_HSCX', @capture_instance = 'T_CYBER_DATA_PRIMARY_OPERATING_SYSTEM_HSCX', @role_name = null
EXEC sys.sp_cdc_enable_table @source_schema = 'dbo', @source_name   = 'T_CYBER_DATA_ONLINE_SALES_REVENUE_HSCX', @capture_instance = 'T_CYBER_DATA_ONLINE_SALES_REVENUE_HSCX', @role_name = null
EXEC sys.sp_cdc_enable_table @source_schema = 'dbo', @source_name   = 'T_BI_DEPENDENT_KEY_PROVIDERS_HSCX', @capture_instance = 'T_BI_DEPENDENT_KEY_PROVIDERS_HSCX', @role_name = null
EXEC sys.sp_cdc_enable_table @source_schema = 'dbo', @source_name   = 'AS_DEPENDENT_BI_KEY_PROVIDERS_HSCX', @capture_instance = 'AS_DEPENDENT_BI_KEY_PROVIDERS_HSCX', @role_name = null
EXEC sys.sp_cdc_enable_table @source_schema = 'dbo', @source_name   = 'T_EVENT_TYPE_HSCX', @capture_instance = 'T_EVENT_TYPE_HSCX', @role_name = null
EXEC sys.sp_cdc_enable_table @source_schema = 'dbo', @source_name   = 'SH_BATCH_LOG', @capture_instance = 'SH_BATCH_LOG', @role_name = null
EXEC sys.sp_cdc_enable_table @source_schema = 'dbo', @source_name   = 'ST_TASK_LOG', @capture_instance = 'ST_TASK_LOG', @role_name = null
EXEC sys.sp_cdc_enable_table @source_schema = 'dbo', @source_name   = 'T_TOT_VERSION_LINES', @capture_instance = 'T_TOT_VERSION_LINES', @role_name = null
