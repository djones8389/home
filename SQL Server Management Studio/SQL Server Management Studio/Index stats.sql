if OBJECT_ID('tempdb..#IndexMetrics')is not null drop table #IndexMetrics;

CREATE TABLE #IndexMetrics (
	[ID] [uniqueidentifier] NOT NULL,
	[Instance] [nvarchar](500) NULL,
	[database] [nvarchar](500) NULL,
	[table] [nvarchar](500) NULL,
	[index] [nvarchar](500) NULL,
	[user_updates] [int] NULL,
	[last_user_update] [datetime] NULL,
	[user_seeks] [int] NULL,
	[user_scans] [int] NULL,
	[user_lookups] [int] NULL,
	[last_user_seek] [datetime] NULL,
	[last_user_scan] [datetime] NULL,
	[last_user_lookup] [datetime] NULL
)

INSERT #IndexMetrics
exec sp_MSforeachdb '

use [?];

if(db_ID() > 4)


BEGIN

	SELECT	newID() as [ID],
			@@SERVERNAME as [Instance],
			 DB_NAME() AS [database],
			 OBJECT_NAME(s.OBJECT_ID) AS [table],
			 i.name AS [index],
			 s.user_updates,
			 [last_user_update] ,
			 s.user_seeks , 
			 s.user_scans, 
			 s.user_lookups
			 ,  [last_user_seek]
			 ,  [last_user_scan]
			 ,  [last_user_lookup]
	 FROM		sys.dm_db_index_usage_stats AS s WITH (NOLOCK)
	 INNER JOIN sys.indexes AS i WITH (NOLOCK)
			 ON s.[object_id] = i.[object_id]
			AND i.index_id = s.index_id
	WHERE	 OBJECTPROPERTY(s.[OBJECT_ID],''IsUserTable'') = 1
	 AND	 i.index_id > 1
 	 and     s.database_id = DB_ID()
	ORDER BY 1,2
	OPTION	 (RECOMPILE);

END

'

select *
from #IndexMetrics
where [table] = 'ac_entry'
order by [index]


select count([index]) [Count]
	 , [table]
from #IndexMetrics
group by [table] 
order by 1 desc