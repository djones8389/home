use msdb
       DECLARE
              @Application nvarchar(128) = N'FTP',
              @BusinessOwner nvarchar(128) = N'Ian Brock',
              @TechnicalOwner nvarchar(128) = N'Ian Brock',
              @TechnicalExpert nvarchar(128) = N'Dave Jones',	
@Notes nvarchar(4000) = N'This is the prod equivalent of dv0603-21006-00\dev_oriallo'

 
 
       IF EXISTS (SELECT 1 FROM sys.extended_properties WHERE name = 'Application')
       EXEC sys.sp_updateextendedproperty @name = N'Application', @value = @Application;
       ELSE
       EXEC sys.sp_addextendedproperty @name = N'Application', @value = @Application;
 
       IF EXISTS (SELECT 1 FROM sys.extended_properties where name = 'BusinessOwner')
       EXEC sys.sp_updateextendedproperty @name = N'BusinessOwner', @value = @BusinessOwner;
       ELSE
       EXEC sys.sp_addextendedproperty @name = N'BusinessOwner', @value = @BusinessOwner;
 
       IF EXISTS (SELECT 1 FROM sys.extended_properties WHERE name = 'TechnicalOwner')
       EXEC sys.sp_updateextendedproperty @name = N'TechnicalOwner', @value = @TechnicalOwner;
       ELSE
       EXEC sys.sp_addextendedproperty @name = N'TechnicalOwner', @value = @TechnicalOwner;
 
       IF EXISTS (SELECT 1 FROM sys.extended_properties WHERE name = 'TechnicalExpert')
       EXEC sys.sp_updateextendedproperty @name = N'TechnicalExpert', @value = @TechnicalExpert;
       ELSE
       EXEC sys.sp_addextendedproperty @name = N'TechnicalExpert', @value = @TechnicalExpert;


 IF EXISTS (SELECT 1 FROM sys.extended_properties WHERE name = 'Notes')
EXEC sys.sp_updateextendedproperty @name = N'Notes', @value = @Notes;
ELSE
EXEC sys.sp_addextendedproperty @name = N'Notes', @value = @Notes;