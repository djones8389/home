if exists(select 1 from sys.databases where name = 'CDR_Local')
	BEGIN
		 declare  @kill nvarchar(max) = '';

		select @kill += CHAR(13) +
				'kill ' + cast(spid as char(10))
				from sys.sysprocesses
		where db_name(dbid) = 'CDR_Local'

		exec(@kill);
 
		DROP DATABASE CDR_Local
	END

declare @drop nvarchar(MAX)='';
select @drop +=CHAR(13)+'exec sp_delete_job @job_name='''+name+''''
from sysjobs
exec(@drop);

if exists (select 1 from sys.servers where name = 'DBA_CDR')
	begin	
		EXEC master.dbo.sp_dropserver @server=N'DBA_CDR', @droplogins='droplogins'
	end