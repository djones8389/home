CREATE DATABASE fxrates AS COPY OF [nft-fxrates-sql-northeurope].fxrates;
drop  USER ut_fxr_db_writer
CREATE USER ut_fxr_db_writer WITH PASSWORD = '8q2RQKSt7VvbuyXDYqsU';
GO

CREATE ROLE [db_executor] AUTHORIZATION [dbo]
GO

CREATE USER uat_fdw_db_reader for external provider

--Grant permissions to role

GRANT EXECUTE TO [db_executor]
GO

--Add user to role
use fxrates
ALTER ROLE db_datareader ADD MEMBER uat_fdw_db_reader
ALTER ROLE db_datawriter ADD MEMBER st_fxr_db_writer;
ALTER ROLE db_executor ADD MEMBER st_fxr_db_writer

 

SELECT DP1.name AS DatabaseRoleName,   
   isnull (DP2.name, 'No members') AS DatabaseUserName   
   
 FROM sys.database_role_members AS DRM  
 RIGHT OUTER JOIN sys.database_principals AS DP1  
   ON DRM.role_principal_id = DP1.principal_id  
 LEFT OUTER JOIN sys.database_principals AS DP2  
   ON DRM.member_principal_id = DP2.principal_id  
WHERE DP1.type = 'R'
	--and  isnull (DP2.name, 'No members')  = 'st_fxr_db_writer'
ORDER BY DP1.name;

drop user  nft_fxr_db_writer
drop user  [Suman.Ghosh@HISCOX.com]
drop user  [sailendra.suda@Hiscox.com]
   alter role db_datareader add member ut_fxr_db_writer alter role db_datawriter add member ut_fxr_db_writer   alter role db_executor add member ut_fxr_db_writer
