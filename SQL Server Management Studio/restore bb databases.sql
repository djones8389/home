 --alter availability group [availabilitygroup] remove database [production-contentservices]
 --alter availability group [availabilitygroup] remove database [production-integration]
 --alter availability group [availabilitygroup] remove database [production-orchestrator]
 --alter availability group [availabilitygroup] remove database [production-portal]

 --alter availability group [availabilitygroup] add database [prodalpha-contentservices]
 --alter availability group [availabilitygroup] add database [prodalpha-integration]
 --alter availability group [availabilitygroup] add database [prodalpha-orchestrator]
 --alter availability group [availabilitygroup] add database [prodalpha-portal]
   

declare  @dynamic nvarchar(max) = '';

select @dynamic += CHAR(13) +
        'kill ' + cast(spid as char(10))
       from sys.sysprocesses
where db_name(dbid) = 'prodalpha-contentservices'

exec(@dynamic);
go


RESTORE DATABASE [prodalpha-contentservices] FROM DISK = N'T:\SQL_Backup\BBDatabases.bak' WITH FILE = 5
,NOUNLOAD
,NOREWIND
,REPLACE
,STATS = 10
,MOVE 'Contentservices' TO N'E:\SQL_Databases\prodalpha-contentservices.mdf'
,MOVE 'Contentservices_log' TO N'E:\SQL_Logs\prodalpha-contentservicess.ldf';

declare  @dynamic nvarchar(max) = '';

select @dynamic += CHAR(13) +
        'kill ' + cast(spid as char(10))
       from sys.sysprocesses
where db_name(dbid) = 'prodalpha-integration'

exec(@dynamic);
go


RESTORE DATABASE [prodalpha-integration] FROM DISK = N'T:\SQL_Backup\BBDatabases.bak' WITH FILE = 6
,NOUNLOAD
,NOREWIND
,REPLACE
,STATS = 10
,MOVE 'Integration' TO N'E:\SQL_Databases\prodalpha-integration.mdf'
,MOVE 'Integration_log' TO N'E:\SQL_Logs\prodalpha-integration.ldf';

declare  @dynamic nvarchar(max) = '';

select @dynamic += CHAR(13) +
        'kill ' + cast(spid as char(10))
       from sys.sysprocesses
where db_name(dbid) = 'prodalpha-orchestrator'

exec(@dynamic);

go
RESTORE DATABASE [prodalpha-orchestrator] FROM DISK = N'T:\SQL_Backup\BBDatabases.bak' WITH FILE = 7
,NOUNLOAD
,NOREWIND
,REPLACE
,STATS = 10
,MOVE 'Orchestrator' TO N'E:\SQL_Databases\prodalpha-orchestrator.mdf'
,MOVE 'Orchestrator_log' TO N'E:\SQL_Logs\prodalpha-orchestrator.ldf';


declare  @dynamic nvarchar(max) = '';

select @dynamic += CHAR(13) +
        'kill ' + cast(spid as char(10))
       from sys.sysprocesses
where db_name(dbid) = 'prodalpha-portal'

exec(@dynamic);

go

RESTORE DATABASE [prodalpha-portal] FROM DISK = N'T:\SQL_Backup\BBDatabases.bak' WITH FILE = 8
,NOUNLOAD
,NOREWIND
,REPLACE
,STATS = 10
,MOVE 'Portal' TO N'E:\SQL_Databases\prodalpha-portal.mdf'
,MOVE 'Portal_log' TO N'E:\SQL_Logs\prodalpha-portal.ldf';