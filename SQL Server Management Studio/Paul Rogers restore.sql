use master;

RESTORE DATABASE [IDIT_PROD_MI] FROM DISK = 'T:\SQL_TDB\IDIT.bak'
WITH MOVE 'HSCX_DEV' TO 'E:\SQL_Databases\IDIT_PROD_MI.mdf',    
MOVE 'HSCX_DEV_log' TO 'E:\SQL_Logs\IDIT_PROD_MI_log.ldf', 
REPLACE, RECOVERY
GO

use [IDIT_PROD_MI]

exec sp_grantdbaccess 'HISCOX\idit_live_supp_IDIT_db_datareader' 
exec sp_addrolemember 'db_datareader', 'HISCOX\idit_live_supp_IDIT_db_datareader'
exec sp_grantdbaccess 'HISCOX\idit_live_supp_db_LiveSupport' 
exec sp_addrolemember 'db_datareader', 'HISCOX\idit_live_supp_db_LiveSupport'
GO

use [CDR_Local];

EXECUTE dbo.IndexOptimize @Databases = 'IDIT_PROD_MI', @FragmentationLow = NULL, @FragmentationMedium = 'INDEX_REORGANIZE,INDEX_REBUILD_ONLINE,INDEX_REBUILD_OFFLINE', @FragmentationHigh = 'INDEX_REBUILD_ONLINE,INDEX_REBUILD_OFFLINE', @FragmentationLevel1 = 10, @FragmentationLevel2 = 30, @UpdateStatistics = 'ALL', @FillFactor = 85, @PageCountLevel      = 100, @LogToTable = 'Y', @SortInTempdb = 'Y', @TimeLimit = 10800 --seconds
 

