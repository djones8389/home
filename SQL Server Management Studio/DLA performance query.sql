SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

use DLACore

;with cte as (
select
    tl.SchemaName,
    tl.TableName,
    tl.starttime,
    tl.RunId,
    --tl.Outcome,
    --max(ttl.ErrorMesage) as ErrorMessage,
    max(case when ttl.task = 'GetMetaData' then Duration else NULL end) as GetMetaData_Duration,
    max(case when ttl.task = 'LookupNewWaterMarkActivity' then Duration else NULL end) as LookupNewWaterMarkActivity_Duration,
    max(case when ttl.task = 'IncrementalCopyActivity' then Duration else NULL end)  as IncrementalCopyActivity_Duration,
    max(case when ttl.task = 'StoredProcedureToUpsert' then Duration else NULL end)  as StoredProcedureToUpsert_Duration,
    --max(case when ttl.task = 'GetMetaData' then Status else NULL end) as GetMetaData_Status,
    --max(case when ttl.task = 'LookupNewWaterMarkActivity' then Status else NULL end) as LookupNewWaterMarkActivity_Status,
    --max(case when ttl.task = 'IncrementalCopyActivity' then Status else NULL end)  as IncrementalCopyActivity_Status,
    --max(case when ttl.task = 'StoredProcedureToUpsert' then Status else NULL end)  as StoredProcedureToUpsert_Status,
    --max(case when ttl.task = 'GetMetaData'              then JSON_VALUE(ttl.log, '$.Output.firstRow.WatermarkValue') else NULL end)   as Watermarkvalue,
    --max(case when ttl.task = 'LookupNewWaterMarkActivity' then JSON_VALUE(ttl.log, '$.Output.firstRow.NewWatermarkvalue') else NULL end)  as NewWatermarkvalue,
    max(case when ttl.task = 'IncrementalCopyActivity' then  JSON_VALUE(ttl.log,'$.Output.dataRead') else NULL end)  as dataRead,
    max(case when ttl.task = 'IncrementalCopyActivity' then  JSON_VALUE(ttl.log,'$.Output.dataWritten') else NULL end)  as dataWritten,
    max(case when ttl.task = 'IncrementalCopyActivity' then  JSON_VALUE(ttl.log,'$.Output.rowsRead') else NULL end)  as rowsRead,
    max(case when ttl.task = 'IncrementalCopyActivity' then  JSON_VALUE(ttl.log,'$.Output.rowsCopied') else NULL end)  as rowsCopied
from control.TableTaskLog ttl
join control.TableLog tl on ttl.RunId = tl.RunId
where tl.StartTime > '2019-05-29 07:00:00' and tl.StartTime < '2019-05-29 10:00:00'
 group by
    tl.SchemaName,
    tl.TableName,
    tl.starttime,
    tl.RunId,
    tl.Outcome
)

select * from cte
where schemaname = 'magic' --and  TableName ='polhist'
order by 3 asc
