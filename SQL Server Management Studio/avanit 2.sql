select *
from SoftwareTitle st
inner join [dbo].[DetectedSoftwareAsset] DSA
on dsa.softwareTitleId = st.id

inner join [ScanMachines] sm on sm.[smachID] = dsa.scanMachineId
inner JOIN [dbo].[Scans] AS scan ON scan.[ScanID] = sm.smachScanID
inner join [ManagedMachines] machine on machine.lastPatchScanId = scan.scanid
where st.name like '%sql%'

select * 
from [Scans] AS scan  
inner join [ManagedMachines] machine 
on machine.lastPatchScanId = scan.scanid
where cast(ScanDate as date) = '2019-01-31'

use Protect
SELECT
	machine.[mmKey] AS Id,
	machine.[dnsName] AS DnsName,
	machine.[domain] AS Domain,
	machine.[language] AS [Language],
	machine.[name] AS Name,
	machine.[mmOSID] AS ProductId,
	[Reporting].[IPAddressToString](machine.[lastKnownIPAddress]) AS LastKnownIP,
	sm.[smachScanDate] AS LastPatchAssessedOn,
	sm.[smachID] AS LastAssessedMachineStateId,
	sm.[groupName] AS LastPatchMachineGroupName
FROM [dbo].[ManagedMachines] AS machine
LEFT JOIN [dbo].[Scans] AS scan 
ON scan.[ScanID] = machine.[lastPatchScanId]
LEFT  JOIN [dbo].[ScanMachines] AS sm 
	ON sm.[smachmmKey] = machine.[mmKey] 
	AND sm.smachScanID = scan.[ScanID]
LEFT JOIN LinkSPProduct LSP
on LSP.spplID = smachOSID
LEFT JOIN Products P
on P.prodID = LSP.spplprodID

LEFT JOIN DBA_CDR.sql_inventory.dbo.Hosts h
on h.hostname = machine.[name]
where h.hostname is not null;

--where p.prodName like '%SQL%'