use primarydb

create table tab1 (
	myint int PRIMARY KEY
	, mychar Char(2)
)

create table tab2 (
	mybigint bigint PRIMARY KEY
	, myvarchar varchar(200)
)
