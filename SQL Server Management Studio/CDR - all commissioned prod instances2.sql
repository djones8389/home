SELECT  case when [instanceName] = 'MSSQLSERVER' 
	THEN [hostname] 
	else [hostname] + '\' + [instanceName] 
	end as InstanceName
FROM [SQL_Inventory].[dbo].[vw_CommissionedSQLInstances]
	



select case when [instanceName] = 'MSSQLSERVER'
 THEN [host] else [host] + '\' + [instanceName] end as InstanceName
from (
	SELECT 
		CASE WHEN hostname = '' then sqlclustername else hostname end as host
		, instanceName
	FROM (
		SELECT     s.instanceName, h.hostname, '' sqlclustername, s.status[environment]
		FROM         dbo.Servers s INNER JOIN
							  dbo.Hosts h ON s.hostid = h.hostid
		WHERE     s.serverState <> 'Decommissioned' 	and s.status ='p'
		UNION
		SELECT     s.instanceName, '', c.sqlclustername sqlclustername, s.status[environment]
		FROM         dbo.Servers s JOIN
							  dbo.Clusters c ON c.clusterID = s.clusterID
		WHERE     s.serverState <> 'Decommissioned'	and s.status ='p'
	) a
) b
order by 1;