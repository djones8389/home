USE ODS_RAQ;
GO

/****** Object:  Table [dbo].[tblNLAlloc]    Script Date: 27/11/2018 16:27:27 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[temp_tblNLAlloc](
	[ods_DateFrom] [date] NOT NULL,
	[ods_DateTo] [date] NULL,
	[AccountID] [varchar](10) NOT NULL,
	[ParentRef] [varchar](20) NOT NULL,
	[OtherRef] [varchar](20) NOT NULL,
	[Amount] [numeric](15, 2) NULL,
	[PeriodDate] [datetime] NULL,
	[Posted] [smallint] NULL,
	[Rowguid] [uniqueidentifier] NULL,
	[InstalID] [int] NOT NULL
 CONSTRAINT [PK_dbo_temp_tblNLAlloc] PRIMARY KEY CLUSTERED 
(
	[ods_DateFrom] ASC,
	[AccountID] ASC,
	[ParentRef] ASC,
	[OtherRef] ASC,
	[InstalID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [TABLES]
) ON [TABLES]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[temp_tblNLAlloc] ADD  DEFAULT ((0)) FOR [InstalID]
GO

INSERT [temp_tblNLAlloc] (ods_DateFrom, ods_DateTo, AccountID, ParentRef, OtherRef, Amount, PeriodDate, Posted, Rowguid, InstalID)
SELECT ods_DateFrom, ods_DateTo, AccountID, ParentRef, OtherRef, Amount, PeriodDate, Posted, Rowguid, 0
FROM [tblNLAlloc]


DROP FUNCTION [dbo].[tblNLAlloc_AsAt]
GO 

DROP VIEW [dbo].[tblNLAlloc_Latest]
GO


DROP TABLE [tblNLAlloc];

exec sp_rename 'temp_tblNLAlloc','tblNLAlloc';

/****** Object:  UserDefinedFunction [dbo].[tblNLAlloc_AsAt]    Script Date: 27/11/2018 16:33:54 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


	
		CREATE FUNCTION [dbo].[tblNLAlloc_AsAt] (@Date date) 

		returns table WITH SCHEMABINDING as return (

			select ods_DateFrom, ods_DateTo, [AccountID],[ParentRef],[OtherRef],[Amount],[PeriodDate],[Posted],[Rowguid]
			from [dbo].[tblNLAlloc]
			where ods_DateFrom <= isnull(@Date, sysdatetime())
				and ods_DateTo > isnull(@Date, sysdatetime())

		)
	
GO
 
	
		CREATE VIEW [dbo].[tblNLAlloc_Latest] WITH SCHEMABINDING AS

		select [AccountID],[ParentRef],[OtherRef],[Amount],[PeriodDate],[Posted],[Rowguid]
		from [dbo].[tblNLAlloc]
		where ods_DateTo = '31 dec 2999'

	
GO


exec sp_rename 'PK_dbo_temp_tblNLAlloc','PK_dbo_tblNLAlloc';


CREATE NONCLUSTERED INDEX [IX_dbo_tblNLAlloc_ods_DateFrom] ON [dbo].[tblNLAlloc]
(
	[ods_DateFrom] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [TABLES]
CREATE NONCLUSTERED INDEX [IX_dbo_tblNLAlloc_ods_DateTo] ON [dbo].[tblNLAlloc]
(
	[ods_DateTo] ASC,
	[ods_DateFrom] ASC,
	[AccountID] ASC,
	[ParentRef] ASC,
	[OtherRef] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [TABLES]
CREATE NONCLUSTERED INDEX [IX_dbo_tblNLAlloc_ods_PK] ON [dbo].[tblNLAlloc]
(
	[AccountID] ASC,
	[ParentRef] ASC,
	[OtherRef] ASC,
	[ods_DateTo] ASC,
	[ods_DateFrom] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [TABLES]

exec sp_rename 'DF__temp_tblN__Insta__17A5B89A','DF__tblN__Insta__17A5B89A';