--pr0103-20100-00\SOLARWINDS

use SolarWindsOrion;

SELECT A.*
	, D.LastFailedComment
FROM (
SELECT DISTINCT
       LOWER(nod.Caption) AS Caption,
       CASE CHARINDEX('Label',vol.Caption)
              WHEN 0 THEN vol.Caption ELSE RTRIM(LEFT(vol.Caption,CHARINDEX('Label',vol.Caption)-1)) END AS [VolumeName],
       nod.[Environment],
		   ISNULL(vol.[ALERT_VOL],90) AS [% Alert Threshold],
       vol.VolumePercentUsed AS [% Used],
       CONVERT(NUMERIC(10,2),vol.VolumeSize/1024/1024/1024) AS [Capacity Gb],
       CONVERT(NUMERIC(10,2),vol.VolumeSpaceUsed/1024/1024/1024) AS [Used Gb],
       CONVERT(NUMERIC(10,2),vol.VolumeSpaceAvailable/1024/1024/1024) AS [Available Gb],
       vol.VolumeResponding AS [LUN Online?],
       cap.WarningThreshold as [Custom Threshold]
FROM [SolarWindsOrion].[dbo].[Nodes] nod
INNER JOIN  [SolarWindsOrion].[dbo].[Volumes] vol
LEFT JOIN [SolarWindsOrion].[dbo].VolumesForecastCapacity cap
on vol.VolumeID = cap.InstanceId
	ON nod.nodeid = vol.nodeid
 
WHERE  SubRole = 'DB'
       --AND Environment IN ('Production', 'Infrastructure')
       AND VolumeTypeID IN (4,100)
       AND Environment in ('prod','production')
        AND vol.VolumePercentUsed > ISNULL([ALERT_VOL],90)
              AND vol.VolumePercentUsed > ISNULL([WarningThreshold],90)
        AND CONVERT(NUMERIC(10,2),vol.VolumeSpaceAvailable/1024/1024/1024) < 80
) A
LEFT JOIN [DBA_CDR].SQL_Inventory.[dbo].[vw_DiskSpaceAlerts_SSRSReport_GetDBAComments] D
on D.[hostName] = A.Caption
	and D.[VolumeName] = A.[VolumeName]

ORDER BY [% Used] DESC

/*
SELECT DISTINCT
       LOWER(nod.Caption) AS Caption,
       CASE CHARINDEX('Label',vol.Caption)
              WHEN 0 THEN vol.Caption ELSE RTRIM(LEFT(vol.Caption,CHARINDEX('Label',vol.Caption)-1)) END AS [VolumeName],
       nod.[Environment],
		   ISNULL(vol.[ALERT_VOL],90) AS [% Alert Threshold],
       vol.VolumePercentUsed AS [% Used],
       CONVERT(NUMERIC(10,2),vol.VolumeSize/1024/1024/1024) AS [Capacity Gb],
       CONVERT(NUMERIC(10,2),vol.VolumeSpaceUsed/1024/1024/1024) AS [Used Gb],
       CONVERT(NUMERIC(10,2),vol.VolumeSpaceAvailable/1024/1024/1024) AS [Available Gb],
       vol.VolumeResponding AS [LUN Online?],
       cap.WarningThreshold as [Custom Threshold]
FROM [SolarWindsOrion].[dbo].[Nodes] nod
       INNER JOIN  [SolarWindsOrion].[dbo].[Volumes] vol
             left join [SolarWindsOrion].[dbo].VolumesForecastCapacity cap
             on vol.VolumeID = cap.InstanceId
                ON nod.nodeid = vol.nodeid
WHERE 
       SubRole = 'DB'
       --AND Environment IN ('Production', 'Infrastructure')
       AND VolumeTypeID IN (4,100)
       AND Environment in (@Disk_Environment)
        AND vol.VolumePercentUsed > ISNULL([ALERT_VOL],90)
              AND vol.VolumePercentUsed > ISNULL([WarningThreshold],90)
        AND CONVERT(NUMERIC(10,2),vol.VolumeSpaceAvailable/1024/1024/1024) < 80
ORDER BY
       vol.VolumePercentUsed DESC
*/