use SQL_Inventory_DJDev

select *  
from SQL_Inventory_DJDev..Failed_jobs_additional_info
--order by 1,2,3
where hostName = 'hxe70201.hiscoxbm.local'
GO

exec [SSRS_FailedJobInfo] 
	@hostname = 'hxe70201.hiscoxbm.local'
	, @instanceName = 'MSSQLSERVER'
	, @jobName = 'Refresh Environment after Prod Deployment'
	, @Comment = 'Add your comment in here'

		 
CREATE PROCEDURE [SSRS_FailedJobInfo] 

	@hostname sysname
	, @instanceName varchar(100)
	, @jobName varchar(200)
	, @Comment varchar(200)

AS
 
BEGIN TRY
    BEGIN TRANSACTION;

		DECLARE @Source TABLE (
			hostName sysname
			, instanceName varchar(100)
			, job varchar(200)
			, comment varchar(200)
		)

		INSERT @Source(hostname, instanceName, job, Comment)
		SELECT @hostname, @instanceName, @jobName,@Comment

		MERGE INTO Failed_jobs_additional_info AS TGT
		USING (
			SELECT [hostname], [instanceName], job, getdate() [recorded_date], SUSER_SNAME(SUSER_SID()) [owner], [comment]
			FROM @Source
		) as SRC
		on TGT.hostName = SRC.hostName
			and TGT.instanceName = SRC.instanceName
			and TGT.job = SRC.job
		WHEN NOT MATCHED THEN
		INSERT([hostname], [instanceName], [job], [recorded_date], [owner], [comment])
		VALUES(SRC.hostname, SRC.instanceName, SRC.job, SRC.[recorded_date], SRC.[owner], SRC.[comment])
		WHEN MATCHED THEN
		UPDATE
		SET [recorded_date] = GETDATE()
			, [owner] = SRC.[owner]
			, [comment] = SRC.[comment];

    COMMIT TRANSACTION;
    
END TRY
BEGIN CATCH

	SELECT ERROR_NUMBER() AS ErrorNumber
		, ERROR_SEVERITY() AS ErrorSeverity
		, ERROR_STATE() AS ErrorState
		, ERROR_PROCEDURE() AS ErrorProcedure
		, ERROR_LINE() AS ErrorLine
		, ERROR_MESSAGE() AS ErrorMessage

END CATCH;

GO



/*
insert Failed_jobs_additional_info 
values('hxe70201.hiscoxbm.local'
	,'MSSQLSERVER'
	,'Refresh Environment after Prod Deployment'
	, getdate()
	, 'DaveJ'
	, 'Nick Webber working on this'
	)

update Failed_jobs_additional_info
set comment = 'Working now, after restarting SQL Agent service on server'
	, recorded_date = getdate()
where job = 'Server Maintanence: Recycle Error Logs'
	and hostName = 'hxd70203.hiscoxbm.local'
	
update Failed_jobs_additional_info
set comment =  'Emailed:  Alastair Gilhooley, Ben Walker and Andy Isgate'
where hostName = 'aphxp06s-uk01.hiscox.com'
*/