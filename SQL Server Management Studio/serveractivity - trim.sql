use SQLdmRepository

select   

--CHECKSUM(cast(SystemProcesses as varbinary(MAX)))
--	, CHECKSUM(cast(LockStatistics as varbinary(MAX)))
--	, CHECKSUM(cast(LockList as varbinary(MAX)))
	top 10 
	 UTCCollectionDateTime
	 ,cast(UTCCollectionDateTime as date)
	, DATEPART(hour,UTCCollectionDateTime)
	--min(UTCCollectionDateTime), max(UTCCollectionDateTime)
from ServerActivity NOLOCK
where  SQLServerID = 6
	and SystemProcesses is not null
	order by UTCCollectionDateTime

	--and UTCCollectionDateTime > '2018-05-20 02:00:46.907'
	--and UTCCollectionDateTime <= '2018-05-20 07:07:46.907'
	