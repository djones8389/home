IF EXISTS (select 1 from sys.tables where name = 'HNYUSMGAManagementAccounts_temp')
	BEGIN
		DROP TABLE HNYUSMGAManagementAccounts_temp;
	END
GO	

SELECT *
INTO RAQ_NonSystemData.dbo.HNYUSMGAManagementAccounts_temp
FROM RAQ_NonSystemData.dbo.HNYUSMGAManagementAccounts;

IF(@@ROWCOUNT <= 0) 
	PRINT 'Table has not been backed up - abort!';

ELSE
DECLARE 
	@DateFrom  Date	= '2018-07-01'
   , @DateTo Date  = '2018-07-31'

	INSERT INTO RAQ_NonSystemData.dbo.HNYUSMGAManagementAccounts
	select @DateTo as RunDate, p.PolicyID, datepart(yyyy,th.TransDate) as AccountYear,
	RIGHT(CONVERT(varchar,th.TransDate,101),4) + LEFT(CONVERT(varchar,th.TransDate,101),2) as AccountingPeriodID,
	RIGHT(CONVERT(varchar,th.TransDate,101),4) + LEFT(CONVERT(varchar,th.TransDate,101),2) as AccountingPeriod, 
	'' as I90BatchPeriodID, '' as I90BatchPeriod,
	datepart(yyyy,p.CoverFrom) as UWYear, datepart(mm,p.CoverFrom) as UWMonth, 'Current Month' as CurrentMonth,
	'S33' as RiskCarrierCode, 'Syndicate 33' as RiskCarrierName, 'PRP' as LineOfBusinessGroupLevel2Code, 
	'Property' as LineOfBusinessGroupLevel2Name, 'PRP' as LineOfBusinessGroupLevel1Code,
	'Property' as LineOfBusinessGroupLevel1Name, 'PRP01' as LineOfBusinessCode, 'Property' as LineOfBusinessName,
	substring(tr.PremiumID,3,2) as LloydsSectionCode, '???' as ProcessingCentreCode, p.CurrencyID as CurrencyCode,
	substring(isnull(ei.ExtraInfo2,''),1,2) as InsuredAddressStateCode, 'PRP' as LineOfBusinessDivisionCode, 'Property' as LineOfBusinessDivisionName, 
	SalesUnitCode = 
	case when isnull(acb.Region,'') = 'Midwest' then 'CHI01'
		 when isnull(acb.Region,'') = 'Northeast' then 'MNH01' 
		 when isnull(acb.Region,'') = 'Northwest'  then 'SAN01' 
		 when isnull(acb.Region,'') = 'Southeast' then 'ATL01' 
		 when isnull(acb.Region,'') = 'Southwest' then 'LAN01'  else '' end,
	SalesUnitName = 
	case when isnull(acb.Region,'') = 'Midwest' then 'Chicago'
		 when isnull(acb.Region,'') = 'Northeast' then 'Manhattan' 
		 when isnull(acb.Region,'') = 'Northwest'  then 'San Francisco' 
		 when isnull(acb.Region,'') = 'Southeast' then 'Atlanta' 
		 when isnull(acb.Region,'') = 'Southwest' then 'Los Angeles' else ''  end,
	SalesUnitGroupLevel1Code = 
	case when isnull(acb.Region,'') = 'Midwest' then 'CHI'
		 when isnull(acb.Region,'') = 'Northeast' then 'MNH' 
		 when isnull(acb.Region,'') = 'Northwest'  then 'SAN' 
		 when isnull(acb.Region,'') = 'Southeast' then 'ATL' 
		 when isnull(acb.Region,'') = 'Southwest' then 'LAN' else '' end,
	SalesUnitGroupLevel1Name = 
	case when isnull(acb.Region,'') = 'Midwest' then 'Chicago'
		 when isnull(acb.Region,'') = 'Northeast' then 'Manhattan' 
		 when isnull(acb.Region,'') = 'Northwest'  then 'San Francisco' 
		 when isnull(acb.Region,'') = 'Southeast' then 'Atlanta' 
		 when isnull(acb.Region,'') = 'Southwest' then 'Los Angeles' else '' end,
	'URG' as SalesUnitGroupLevel2Code, 'US Regions' as SalesUnitGroupLevel2Name, 'USA' as SalesUnitBusinessUnitCode, 
	'United States' as SalesUnitBusinessUnitName, 
	NewRenewalBlockBookedCode = case When p.RecordType = 'R' Then 'RN' else 'NB' end,
	NewRenewalBlockBookedName = case When p.RecordType = 'R' Then 'Renewal' else 'New Business' end,
	'???' as CoverageClassCode, '???' as CoverageClassName, '***' as ClaimPaymentTypeCode,
	'Not Applicable' as ClaimPaymentTypeName, tr.premium as WrittenPremium, 
	tr.premium * (isnull(tpm.Amount,0) / 100) as WrittenCommission,
	tr.premium * (isnull(tpa.Amount,0) / 100) as WrittenExternalCommission,
	tr.premium *  (isnull(tpp.Amount,0) / 100) as WrittenPBFEECommission,
	tr.premium * ((isnull(tpm.Amount,0) - isnull(tpa.Amount,0) - isnull(tpp.Amount,0)) / 100)  as WrittenInternalCommission,  EarnedPremium = 
	case when tr.EffectiveDate > @DateTo then 0
	when tr.CoverTo <= @DateTo then tr.Premium 
	else (tr.Premium * cast (datediff(dd,tr.EffectiveDate,@DateTo) + 1 as integer)) / cast (datediff(dd,tr.EffectiveDate, tr.CoverTo) + 1 as integer) end,
	EarnedCommission = 
	case when tr.EffectiveDate > @DateTo then 0
	when tr.CoverTo <= @DateTo then tr.premium * (isnull(tpm.Amount,0) / 100)
	else ((tr.premium * (isnull(tpm.Amount,0) / 100)) * cast (datediff(dd,tr.EffectiveDate,@DateTo) + 1 as integer)) / cast (datediff(dd,tr.EffectiveDate, tr.CoverTo) + 1 as integer) end,
	EarnedExternalCommission = 
	case when tr.EffectiveDate > @DateTo then 0
	when tr.CoverTo <= @DateTo then tr.premium * (isnull(tpa.Amount,0) / 100)
	else ((tr.premium * (isnull(tpa.Amount,0) / 100)) * cast (datediff(dd,tr.EffectiveDate,@DateTo) + 1 as integer)) / cast (datediff(dd,tr.EffectiveDate, tr.CoverTo) + 1 as integer) end,
	EarnedPBFEECommission = 
	case when tr.EffectiveDate > @DateTo then 0
	when tr.CoverTo <= @DateTo then tr.premium * (isnull(tpp.Amount,0) / 100)
	else ((tr.premium *  (isnull(tpp.Amount,0) / 100)) * cast (datediff(dd,tr.EffectiveDate,@DateTo) + 1 as integer)) / cast (datediff(dd,tr.EffectiveDate, tr.CoverTo) + 1 as integer) end,
	EarnedInternalCommission = 
	case when tr.EffectiveDate > @DateTo then 0
	when tr.CoverTo <= @DateTo then tr.premium * ((isnull(tpm.Amount,0) - isnull(tpa.Amount,0) - isnull(tpp.Amount,0)) / 100)
	else ((tr.premium * ((isnull(tpm.Amount,0) - isnull(tpa.Amount,0) - isnull(tpp.Amount,0)) / 100)) * cast (datediff(dd,tr.EffectiveDate,@DateTo) + 1 as integer)) / cast (datediff(dd,tr.EffectiveDate, tr.CoverTo) + 1 as integer) end,
	0 as PremiumTax, 
	NewBusinessWrittenPremium = case When p.RecordType = 'R' then 0 else tr.Premium end,
	RenewalWrittenPremium = case When p.RecordType = 'R' then tr.Premium else 0 end,
	0 as BlockBookedPremium, 0 as ClaimPaidIndemnity, 0 as ClaimPaidFee, 0 as ClaimOutstandingIndemnity,
	0 as ClaimOutstandingFee, 0 as OutstandingIndemnityRecovery, 0 as ClaimPaidTotal, 0 as ClaimOutstandingTotal,
	0 as ClaimIncurred, 0 as PolicyFees, 1902 as ClaimLossYear, 1902 as ClaimReportedYear, '' as RiskAddressStateCode,
	'???' as GLLineOfBusinessCode, '???' as NAICAnnualStatementLineCode, '???' as ProductGroupLevel2Code,
	0 as PolicySurcharges, 'Broker' as DistributionChannelName, 'BRK' as DistributionChannelCode
	from RAQ.dbo.tblTransHeader th
	inner join RAQ.dbo.tblPolicy p on th.PolicyID = p.PolicyID 
	inner join RAQ.dbo.tblTrans  tr on tr.TransID = th.TransID
	left join RAQ.dbo.tblAccount acc on p.ClientID = acc.AccountID
	left join RAQ.dbo.tblAccount acb on p.AgentID = acb.AccountID
	left join RAQ.dbo.tblExtraInfo ei on p.PolicyID = ei.RecordID and TabName = 'LYDRPT'
	left join RAQ.dbo.tblPolicySplit tpm on p.PolicyID = tpm.PolicyID and tpm.SplitID = '1' and tpm.IntermediaryID = 'MGACOM'  
	left join RAQ.dbo.tblPolicySplit tpa on p.PolicyID = tpa.PolicyID and tpa.SplitID = '1' and tpa.IntermediaryID = 'AGENT'
	left join RAQ.dbo.tblPolicySplit tpp on p.PolicyID = tpp.PolicyID and tpp.SplitID = '1' and tpp.IntermediaryID = 'PBFEE'

	where p.SchemeID = 1239
	and p.RecordType != 'T'
	and p.Status IN ('OnRisk','Renewed','Cancelled','Lapsed')
	and ((th.TransDate >= @DateFrom and th.TransDate <= @DateTo and p.CoverFrom <= @DateTo)
	or (th.TransDate < @DateFrom and p.CoverFrom >= @DateFrom and p.CoverFrom <= @DateTo))

	order by 2

