DECLARE @Body nvarchar(MAX)='';

if exists (
SELECT 1
FROM sys.databases db    
LEFT OUTER JOIN sys.dm_database_encryption_keys dm        
ON db.database_id = dm.database_id
where name = 'IDIT_Obfuscated'
	and is_encrypted = 0
)
BEGIN

	SELECT @Body = 'Database is both obfuscated & decrypted, backing up to E:\SQL_Backups\'+(select cast((select cast(getDATE() as date)) as nvarchar(100)))+'.bak now...'
	
	EXECUTE [msdb].[dbo].[sp_send_dbmail]
    @profile_name = 'SMTP'
    ,@recipients  = 'dave.jones@hiscox.com;'
    ,@body        =  @Body
	,@subject     = 'IDIT_Live_Supp - Post-Deployment'
	
	declare @backup nvarchar(max)='';

	select @backup+=CHAR(13) + 
	'
	BACKUP DATABASE [IDIT_Obfuscated] 
	to DISK = ''E:\SQL_Backups\IDIT_Obfuscated_'+(select cast((select cast(getDATE() as date)) as nvarchar(100)))+'.bak''
	WITH copy_only, init, compression, MAXTRANSFERSIZE = 4194304, STATS = 1;

	'
	exec(@backup);
END

ELSE 
BEGIN

	SELECT @Body = 'Database is obfuscated, but not decrypted,  check back again later'

	EXECUTE [msdb].[dbo].[sp_send_dbmail]
    @profile_name = 'SMTP'
    ,@recipients  = 'dave.jones@hiscox.com;'
    ,@body        =  @Body
	,@subject     = 'IDIT_Live_Supp - Post-Deployment'

END





