USE [msdb]
GO

/****** Object:  Job [Job_Sync_BUDPolicyDetails]    Script Date: 05/11/2018 14:25:21 ******/
EXEC msdb.dbo.sp_delete_job @job_id=N'791406f6-1b16-4c2d-b4d6-aabc24407201', @delete_unused_schedule=1
GO

/****** Object:  Job [Job_Sync_BUDPolicyDetails]    Script Date: 05/11/2018 14:25:21 ******/
BEGIN TRANSACTION
DECLARE @ReturnCode INT
SELECT @ReturnCode = 0
/****** Object:  JobCategory [BUD]    Script Date: 05/11/2018 14:25:21 ******/
IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'BUD' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'BUD'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=N'Job_Sync_BUDPolicyDetails', 
		@enabled=1, 
		@notify_level_eventlog=0, 
		@notify_level_email=0, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description=N'Syncs BUDPolicyDetails Table', 
		@category_name=N'BUD', 
		@owner_login_name=N'dbs', @job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Step_Sync_BUDPolicyDetails_Table]    Script Date: 05/11/2018 14:25:22 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Step_Sync_BUDPolicyDetails_Table', 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'IF (SELECT hars.role_desc
    FROM sys.DATABASES d
    INNER JOIN sys.dm_hadr_availability_replica_states hars ON d.replica_id = hars.replica_id
    WHERE database_id = DB_ID(''OBX_BUD_REPL'')) = ''PRIMARY''
BEGIN
    EXEC [OBX_BUD_REPL].[dbo].[BUD_SynchronisePolicyDetails]
END
    ELSE
BEGIN
    PRINT ''Skipping step as running on secondary''
END
', 
		@database_name=N'master', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'Every 20 Seconds', 
		@enabled=1, 
		@freq_type=4, 
		@freq_interval=1, 
		@freq_subday_type=2, 
		@freq_subday_interval=20, 
		@freq_relative_interval=0, 
		@freq_recurrence_factor=0, 
		@active_start_date=20170602, 
		@active_end_date=99991231, 
		@active_start_time=0, 
		@active_end_time=235959, 
		@schedule_uid=N'8c03034e-2197-481c-9f7a-7bd37adc7a13'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:
GO


