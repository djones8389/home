USE master;  
GO 
CREATE MASTER KEY ENCRYPTION BY PASSWORD = 'YYYthdh758934759875kfljfsdj';  
go

CREATE CERTIFICATE IDIT_Prod_Cert   
FROM FILE = 'T:\keys\IDIT_Prod_Cert'  
WITH PRIVATE KEY   
(  
    FILE = 'T:\keys\IDIT_PrivateKeyFile',  
    DECRYPTION BY PASSWORD = 'J(*%kkjhdsfjk65768763434'  
);  
GO 

USE [master]

 
RESTORE DATABASE IDIT
FROM DISK = '\\HXP205-00047-00\Backups\sqlbackups\IDIT.BAK'
      
WITH FILE = 1, MOVE 'HSCX_DEV' TO 'E:\SQL_Databases\IDIT.mdf',
                     MOVE 'HSCX_DEV_log' TO 'E:\SQL_Logs\IDIT.ldf'
, RECOVERY, NOUNLOAD, REPLACE, NOREWIND, STATS = 1

--restore filelistonly FROM DISK = '\\HXP205-00047-00\Backups\sqlbackups\IDIT.BAK'
 