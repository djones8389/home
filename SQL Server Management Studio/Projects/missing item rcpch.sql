SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

use rcpch_secureassess

select west.id 
	, keycode
	, itemid
	, wesirt.itemresponsedata.value('data(p/@ua)[1]','bit') [Attempted]
	, wesirt.itemresponsedata.query('data(//@typ)') [type] --,'nvarchar(10)'
	,itemresponsedata
	, resultdata
from warehouse_examsessiontable west
left join warehouse_examsessionitemresponsetable wesirt
on wesirt.warehouseexamsessionid = west.id
where itemid in ('5008P14101') -- ('5008P14099','5008P14101','5008P14103','5008P14105','5008P24262','5008P24263','5008P24268','5008P24271')
	 and keycode in ('C8TRM37V')
	 --and keycode = '6WJDQJ7V'

	 
