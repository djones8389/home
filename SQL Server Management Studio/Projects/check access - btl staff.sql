/****** Script for SelectTopNRows command from SSMS  ******/
SELECT b.ClientName as 'Instance'
	, [server_name]
      ,[database_Name]
      ,[user_forename]
      ,[user_surname]
      ,[user_username]
      ,[user_disabled]
      ,[user_email]
  FROM [PSCollector].[dbo].[AllSurpassUsers] a
  inner join InstanceLookUpTable b
  on a.server_name = b.InstanceName
  where [user_disabled] = 0
	--and ([user_surname] = 'Kimberley' and [user_forename] = 'Claire')
	--and ([user_surname] = 'May' and [user_forename] = 'Emily')
	--and ([user_surname] = 'Stevens' and [user_forename] = 'Kirk')
	and [user_email] like '%btl%'
	and user_email <> 'theunknownuser@btl.com'
	and user_username not in ('superuser','IntegationUser','statsExporter','salocal_user','BTLLoginCheck','OpenassessAdmin','CP3T01B3Imp0rt5erv1ce5','AspireImport','AspireAdmin','statsManager','SecureMarker','IntegrationUser','SecureAssess','importer','logincheck','UserManager','stateManager','systemadmin','btlmonitor')
	and b.ClientName  <> 'PreProdSQL'

	order by 1,2

