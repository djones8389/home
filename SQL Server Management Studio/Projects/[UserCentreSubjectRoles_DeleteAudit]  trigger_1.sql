use Ocr_SurpassManagement

select *
from Ocr_SurpassManagement..[UserCentreSubjectRoles_DeleteAudit]
go


INSERT [UserCentreSubjectRoles_DeleteAudit]
VALUES(1,2,3,4,5,6,7,(SELECT getDATE()))


ALTER TRIGGER [fireEmail] on [UserCentreSubjectRoles_DeleteAudit] 
	AFTER INSERT
AS
 BEGIN

	DECLARE @query nvarchar(MAX);
	DECLARE @subject nvarchar(200) = 'OCR Deleted exams:  ' + (Select CONVERT(VARCHAR,GETDATE(), 21))

		BEGIN

		--SELECT Id, UserId, CentreId, SubjectId, SubjectLinkType, Assignable, RoleId, Timestamp
		--from inserted

			EXECUTE msdb.dbo.sp_send_dbmail
					@profile_name = 'SMTP Virtual Server',
					@recipients = N'support@btl.com; dave.jones@btl.com',
					@subject = @subject,
					@body = N'Some entries have been deleted,  DaveJ will look into this & re-add them'
					
		END
	END
GO

