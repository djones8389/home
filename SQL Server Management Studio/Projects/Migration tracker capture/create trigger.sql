CREATE TRIGGER [dbo].[MigrationTracker_Trigger]
   ON  [dbo].[_MigrationScripts]
   AFTER INSERT
AS 
BEGIN

	DECLARE @id tinyint;

	select  
		@id = [ID]
	from inserted

	INSERT [dbo].[_MigrationDatabaseMetrics] ([MigrationID], [DatabaseSizeOnDisk MB], [SpaceUsedInDB MB], [LogFileSize MB])
	select @id
		,  
			 (
					SELECT CONVERT(DECIMAL(18, 2), SUM(CAST(df.size AS FLOAT)) * 8 / 1024.0)
					FROM sys.database_files AS df
					WHERE df.type IN (0,2,4)
					) AS [DbSize]
				, CONVERT(DECIMAL(18, 2), SUM(a.total_pages) * 8 / 1024.0)  AS [SpaceUsed]
				,(
					SELECT CONVERT(DECIMAL(18, 2), SUM(CAST(df.size AS FLOAT)) * 8 / 1024.0)
					FROM sys.database_files AS df
					WHERE df.type IN (1,3)
					) AS [LogSize]
	FROM sys.partitions p
	INNER JOIN sys.allocation_units a ON p.partition_id = a.container_id;

	declare @dynamic nvarchar(MAX) = '';

	select @dynamic +=CHAR(13) +
		'exec sp_spaceused @objname = '''+s.name+'.'+t.name + ''' '
	from sys.tables T
	inner join sys.schemas S
	on s.schema_id = t.schema_id
	where type = 'u'
		and t.name not in ('_MigrationDatabaseMetrics','_MigrationScripts','_MigrationTableMetrics' )
	order by t.name;


	INSERT [dbo].[_MigrationTableMetrics] (table_name, [rows], reserved_kb, data_kb, index_size, unused_kb)
	EXEC(@dynamic);

	UPDATE [_MigrationTableMetrics]
	SET fileID = @id
	where fileID is null

END