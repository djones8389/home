SET NOCOUNT ON;

IF OBJECT_ID('tempdb..#ExamXML') IS NOT NULL DROP TABLE #ExamXML
IF OBJECT_ID('tempdb..#Item') IS NOT NULL DROP TABLE #Item
IF OBJECT_ID('tempdb..#WAREHOUSE_ExamSessionItemResponseTable') IS NOT NULL DROP TABLE #WAREHOUSE_ExamSessionItemResponseTable

CREATE TABLE #ExamXML (	
	ID int
	,[StructureXML] XML
	,ResultData XML
	,[ResultDataFull] XML
) 

CREATE TABLE #WAREHOUSE_ExamSessionItemResponseTable (
	[ID] [int],
	[WAREHOUSEExamSessionID] [int],
	[ItemID] [nvarchar](15),
	[ItemVersion] [int] ,
	[ItemResponseData] [xml] ,
	[MarkerResponseData] [xml]
)

BULK INSERT #WAREHOUSE_ExamSessionItemResponseTable
from 'C:\Users\977496-davej\Desktop\ExamSessionItemResponseTable - Copy.csv'
with (fieldterminator = ',', rowterminator = '\n')
go


INSERT #ExamXML(id,  StructureXML, ResultData, ResultDataFull) 
VALUES (
763
,'<assessmentDetails>
  <assessmentID>8</assessmentID>
  <qualificationID>103</qualificationID>
  <qualificationName>Construction Specifications Institute</qualificationName>
  <qualificationReference>CSI_</qualificationReference>
  <assessmentGroupName>Construction Documents Technologist</assessmentGroupName>
  <assessmentGroupID>1</assessmentGroupID>
  <assessmentGroupReference>CDT</assessmentGroupReference>
  <assessmentName>Domestic Form 1</assessmentName>
  <validFromDate>08 Feb 2016</validFromDate>
  <expiryDate>08 Feb 2026</expiryDate>
  <startTime>00:00:00</startTime>
  <endTime>23:59:59</endTime>
  <duration>135</duration>
  <defaultDuration>135</defaultDuration>
  <scheduledDuration>
    <value>135</value>
    <reason></reason>
  </scheduledDuration>
  <sRBonus>180</sRBonus>
  <sRBonusMaximum>240</sRBonusMaximum>
  <externalReference>6038039021</externalReference>
  <passLevelValue>75</passLevelValue>
  <passLevelType>1</passLevelType>
  <status>2</status>
  <testFeedbackType>
    <passFail>0</passFail>
    <percentageMark>0</percentageMark>
    <allowSummaryFeedback>0</allowSummaryFeedback>
    <summaryFeedbackType>1</summaryFeedbackType>
    <itemSummary>0</itemSummary>
    <itemReview>0</itemReview>
    <itemReviewCorrectAnswer>0</itemReviewCorrectAnswer>
    <itemFeedback>0</itemFeedback>
    <printableSummary>0</printableSummary>
    <candidateDetails>0</candidateDetails>
    <feedbackByReference>0</feedbackByReference>
    <allowFeedbackDuringExam>0</allowFeedbackDuringExam>
    <allowFeedbackDuringSummary>0</allowFeedbackDuringSummary>
  </testFeedbackType>
  <itemFeedback>0</itemFeedback>
  <allowCalculator>0</allowCalculator>
  <lastInUseDate>29 Feb 2016 09:40:57</lastInUseDate>
  <completedScriptReview>0</completedScriptReview>
  <assessment currentSection="0" totalTime="0" currentTime="0">
    <intro id="0" name="Introduction" currentItem="">
      <item id="5001P3414" name="CDT Introduction Page" totalMark="0" version="11" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="4" LO="" unit="" quT="0" />
      <item id="5001P4848" name="Tutorial - Intro" totalMark="0" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="4" LO="" unit="" quT="0" />
      <item id="5001P4850" name="Tutorial - Mouse" totalMark="0" version="2" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="4" LO="" unit="" quT="0" />
      <item id="5001P4853" name="Tutorial - Navigation" totalMark="0" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="4" LO="" unit="" quT="0" />
      <item id="5001P4855" name="Tutorial - Time" totalMark="0" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="4" LO="" unit="" quT="0" />
      <item id="5001P4857" name="Tutorial - Scroll" totalMark="0" version="2" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="4" LO="" unit="" quT="0" />
      <item id="5001P4859" name="Tutorial - Multiple Choice" totalMark="0" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="4" LO="" unit="" quT="0" />
      <item id="5001P4862" name="Tutorial - Flags" totalMark="0" version="2" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="4" LO="" unit="" quT="0" />
      <item id="5001P4864" name="Tutorial - Highlight" totalMark="0" version="2" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="4" LO="" unit="" quT="0" />
      <item id="5001P4866" name="Tutorial - Strikeout" totalMark="0" version="7" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="4" LO="" unit="" quT="0" />
      <item id="5001P4868" name="Tutorial - Ending Exam" totalMark="0" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="4" LO="" unit="" quT="0" />
      <item id="5001P4871" name="Tutorial - Conclusion" totalMark="0" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="4" LO="" unit="" quT="0" />
    </intro>
    <outro id="0" name="Finish" currentItem="">
      <item id="5001P3402" name="Finish Page" totalMark="0" version="14" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="2" LO="" unit="" quT="0" />
    </outro>
    <tools id="0" name="Tools" currentItem="" />
    <section id="1" name="1" passLevelValue="0" passLevelType="1" itemsToMark="0" currentItem="" fixed="1">
      <item id="5001P2781" name="IT002379B" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="" unit="" quT="10" nonScored="1" />
      <item id="5001P2903" name="IT130504A" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="" unit="" quT="10" nonScored="1" />
      <item id="5001P2661" name="CSI00027A" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Fundamentals" unit="" quT="10" />
      <item id="5001P2887" name="IT130479" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction Documents" unit="" quT="10" />
      <item id="5001P2703" name="CSI00171" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="" unit="" quT="10" nonScored="1" />
      <item id="5001P2856" name="IT130421" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction Documents" unit="" quT="10" />
      <item id="5001P2875" name="IT130457" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction." unit="" quT="10" />
      <item id="5001P2959" name="IT215041" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Lifecycle Activities" unit="" quT="10" />
      <item id="5001P2707" name="CSI00182" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Design Process" unit="" quT="10" />
      <item id="5001P2712" name="CSI00210A" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction." unit="" quT="10" />
      <item id="5001P2762" name="IT002278" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="" unit="" quT="10" nonScored="1" />
      <item id="5001P2914" name="IT131044" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Procurement" unit="" quT="10" />
      <item id="5001P2908" name="IT130510" totalMark="1" version="6" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction." unit="" quT="10" />
      <item id="5001P2757" name="IT002210A" totalMark="1" version="7" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="" unit="" quT="10" nonScored="1" />
      <item id="5001P2786" name="IT002396" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction." unit="" quT="10" />
      <item id="5001P2844" name="IT003413" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="" unit="" quT="10" nonScored="1" />
      <item id="5001P2666" name="CSI00033" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction Documents" unit="" quT="10" />
      <item id="5001P2926" name="IT131089" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction Documents" unit="" quT="10" />
      <item id="5001P2783" name="IT002387A" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Lifecycle Activities" unit="" quT="10" />
      <item id="5001P2688" name="CSI00093" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Design Process" unit="" quT="10" />
      <item id="5001P2941" name="IT138769" totalMark="1" version="6" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Project Concept and Delivery" unit="" quT="10" />
      <item id="5001P2713" name="CSI00216" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction Documents" unit="" quT="10" />
      <item id="5001P2904" name="IT130505" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction Documents" unit="" quT="10" />
      <item id="5001P2723" name="CSI00311" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Procurement" unit="" quT="10" />
      <item id="5001P2933" name="IT131114" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction Documents" unit="" quT="10" />
      <item id="5001P2951" name="IT214566" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction Documents" unit="" quT="10" />
      <item id="5001P2843" name="IT003412" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="" unit="" quT="10" nonScored="1" />
      <item id="5001P2909" name="IT130511" totalMark="1" version="6" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction." unit="" quT="10" />
      <item id="5001P2697" name="CSI00111B" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction Documents" unit="" quT="10" />
      <item id="5001P2720" name="CSI00230B" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction Documents" unit="" quT="10" />
      <item id="5001P2867" name="IT130439A" totalMark="1" version="6" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Project Concept and Delivery" unit="" quT="10" />
      <item id="5001P2860" name="IT130426" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Preconstruction" unit="" quT="10" />
      <item id="5001P2853" name="IT130418A" totalMark="1" version="6" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Project Concept and Delivery" unit="" quT="10" />
      <item id="5001P2972" name="IT215098A" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="" unit="" quT="10" nonScored="1" />
      <item id="5001P2870" name="IT130449" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction Documents" unit="" quT="10" />
      <item id="5001P2955" name="IT214577" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction Documents" unit="" quT="10" />
      <item id="5001P2977" name="IT215540B" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Lifecycle Activities" unit="" quT="10" />
      <item id="5001P2949" name="IT214552" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction." unit="" quT="10" />
      <item id="5001P2850" name="IT130258A" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction Documents" unit="" quT="10" />
      <item id="5001P2806" name="IT002718" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Fundamentals" unit="" quT="10" />
      <item id="5001P2714" name="CSI00223" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction Documents" unit="" quT="10" />
      <item id="5001P2689" name="CSI00094" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Fundamentals" unit="" quT="10" />
      <item id="5001P2779" name="IT002378" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Lifecycle Activities" unit="" quT="10" />
      <item id="5001P2737" name="CSI00611" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction Documents" unit="" quT="10" />
      <item id="5001P2660" name="CSI00026" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Preconstruction" unit="" quT="10" />
      <item id="5001P2922" name="IT131081" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction." unit="" quT="10" />
      <item id="5001P2975" name="IT215128A" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="" unit="" quT="10" nonScored="1" />
      <item id="5001P2889" name="IT130485" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction Documents" unit="" quT="10" />
      <item id="5001P2930" name="IT131095" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction Documents" unit="" quT="10" />
      <item id="5001P2924" name="IT131087A" totalMark="1" version="6" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Project Concept and Delivery" unit="" quT="10" />
      <item id="5001P2715" name="CSI00225" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Fundamentals" unit="" quT="10" />
      <item id="5001P2751" name="IT002197A" totalMark="1" version="7" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="" unit="" quT="10" nonScored="1" />
      <item id="5001P2655" name="CSI00023B" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction Documents" unit="" quT="10" />
      <item id="5001P2910" name="IT130512" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction." unit="" quT="10" />
      <item id="5001P2902" name="IT130503" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction Documents" unit="" quT="10" />
      <item id="5001P2920" name="IT131069" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Design Process" unit="" quT="10" />
      <item id="5001P2863" name="IT130432A" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction Documents" unit="" quT="10" />
      <item id="5001P2708" name="CSI00190A" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Procurement" unit="" quT="10" />
      <item id="5001P2857" name="IT130422B" totalMark="1" version="6" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Project Concept and Delivery" unit="" quT="10" />
      <item id="5001P2805" name="IT002717" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Fundamentals" unit="" quT="10" />
      <item id="5001P2854" name="IT130419A" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Fundamentals" unit="" quT="10" />
      <item id="5001P2728" name="CSI00467A" totalMark="1" version="6" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Project Concept and Delivery" unit="" quT="10" />
      <item id="5001P2826" name="IT003386" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Design Process" unit="" quT="10" />
      <item id="5001P2896" name="IT130493A" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Procurement" unit="" quT="10" />
      <item id="5001P2874" name="IT130456" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction." unit="" quT="10" />
      <item id="5001P2906" name="IT130508" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Fundamentals" unit="" quT="10" />
      <item id="5001P2809" name="IT002921A" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Design Process" unit="" quT="10" />
      <item id="5001P2654" name="CSI00022A" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="" unit="" quT="10" nonScored="1" />
      <item id="5001P2684" name="CSI00079" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Lifecycle Activities" unit="" quT="10" />
      <item id="5001P2748" name="IT002183A" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="" unit="" quT="10" nonScored="1" />
      <item id="5001P2923" name="IT131086" totalMark="1" version="6" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Project Concept and Delivery" unit="" quT="10" />
      <item id="5001P2956" name="IT214580" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Preconstruction" unit="" quT="10" />
      <item id="5001P2892" name="IT130489" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction Documents" unit="" quT="10" />
      <item id="5001P2877" name="IT130462" totalMark="1" version="6" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction." unit="" quT="10" />
      <item id="5001P2971" name="IT215097A" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Procurement" unit="" quT="10" />
      <item id="5001P2746" name="IT000152" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Design Process" unit="" quT="10" />
      <item id="5001P2749" name="IT002189A" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="" unit="" quT="10" nonScored="1" />
      <item id="5001P2893" name="IT130490A" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction Documents" unit="" quT="10" />
      <item id="5001P2960" name="IT215044A" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Fundamentals" unit="" quT="10" />
      <item id="5001P2678" name="CSI00067A" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Procurement" unit="" quT="10" />
      <item id="5001P2829" name="IT003389" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Design Process" unit="" quT="10" />
      <item id="5001P2760" name="IT002276A" totalMark="1" version="6" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="" unit="" quT="10" nonScored="1" />
      <item id="5001P2664" name="CSI00028A" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="" unit="" quT="10" nonScored="1" />
      <item id="5001P2859" name="IT130425" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction Documents" unit="" quT="10" />
      <item id="5001P2830" name="IT003390" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Design Process" unit="" quT="10" />
      <item id="5001P2929" name="IT131093" totalMark="1" version="6" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Project Concept and Delivery" unit="" quT="10" />
      <item id="5001P2945" name="IT214538" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Preconstruction" unit="" quT="10" />
      <item id="5001P2928" name="IT131091" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Preconstruction" unit="" quT="10" />
      <item id="5001P2732" name="CSI00501" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Fundamentals" unit="" quT="10" />
      <item id="5001P2741" name="IT000084" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Fundamentals" unit="" quT="10" />
      <item id="5001P2882" name="IT130473" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction Documents" unit="" quT="10" />
      <item id="5001P2789" name="IT002401A" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Preconstruction" unit="" quT="10" />
      <item id="5001P2973" name="IT215099A" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Procurement" unit="" quT="10" />
      <item id="5001P2820" name="IT003380" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Design Process" unit="" quT="10" />
      <item id="5001P2690" name="CSI00098A" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction Documents" unit="" quT="10" />
      <item id="5001P2888" name="IT130484" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction Documents" unit="" quT="10" />
      <item id="5001P2744" name="IT000139A" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction." unit="" quT="10" />
      <item id="5001P2740" name="IT000044" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Fundamentals" unit="" quT="10" />
      <item id="5001P2872" name="IT130453" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction." unit="" quT="10" />
      <item id="5001P2885" name="IT130476" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="" unit="" quT="10" nonScored="1" />
      <item id="5001P2838" name="IT003404" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="" unit="" quT="10" nonScored="1" />
      <item id="5001P2944" name="IT214533" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Fundamentals" unit="" quT="10" />
      <item id="5001P2785" name="IT002390" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="" unit="" quT="10" nonScored="1" />
      <item id="5001P2782" name="IT002380" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Lifecycle Activities" unit="" quT="10" />
      <item id="5001P2962" name="IT215050" totalMark="1" version="6" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Project Concept and Delivery" unit="" quT="10" />
      <item id="5001P2818" name="IT003378" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Design Process" unit="" quT="10" />
      <item id="5001P2766" name="IT002317A" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Design Process" unit="" quT="10" />
      <item id="5001P2790" name="IT002409A" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction." unit="" quT="10" />
      <item id="5001P2822" name="IT003382" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Design Process" unit="" quT="10" />
      <item id="5001P2702" name="CSI00170" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction." unit="" quT="10" />
      <item id="5001P2698" name="CSI00112" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Procurement" unit="" quT="10" />
      <item id="5001P2694" name="CSI00104B" totalMark="1" version="6" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Project Concept and Delivery" unit="" quT="10" />
      <item id="5001P2925" name="IT131088A" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="" unit="" quT="10" nonScored="1" />
      <item id="5001P2801" name="IT002499" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="" unit="" quT="10" nonScored="1" />
      <item id="5001P2800" name="IT002486" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction." unit="" quT="10" />
      <item id="5001P2915" name="IT131045A" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction." unit="" quT="10" />
      <item id="5001P2758" name="IT002275A" totalMark="1" version="6" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Procurement" unit="" quT="10" />
      <item id="5001P2868" name="IT130441" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Procurement" unit="" quT="10" />
      <item id="5001P2851" name="IT130415" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction Documents" unit="" quT="10" />
      <item id="5001P2652" name="CSI00021" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction Documents" unit="" quT="10" />
    </section>
    <section id="2" name="S" passLevelValue="0" passLevelType="1" itemsToMark="0" currentItem="" fixed="1">
      <item id="5001P4221" name="Survey Intro" totalMark="0" version="2" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="3" LO="" unit="" quT="0" />
      <item id="5001P4189" name="Survey 01" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="" unit="" quT="10" nonScored="1" />
      <item id="5001P4192" name="Survey 02" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="" unit="" quT="10" nonScored="1" />
      <item id="5001P4196" name="Survey 03" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="" unit="" quT="10" nonScored="1" />
      <item id="5001P4201" name="Survey 04" totalMark="1" version="2" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="" unit="" quT="10" nonScored="1" />
      <item id="5001P4203" name="Survey 05" totalMark="1" version="2" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="" unit="" quT="10" nonScored="1" />
      <item id="5001P4205" name="Survey 06" totalMark="1" version="2" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="" unit="" quT="10" nonScored="1" />
      <item id="5001P4207" name="Survey 07" totalMark="1" version="2" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="" unit="" quT="10" nonScored="1" />
      <item id="5001P4209" name="Survey 08" totalMark="1" version="2" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="" unit="" quT="10" nonScored="1" />
      <item id="5001P4211" name="Survey 09" totalMark="1" version="2" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="" unit="" quT="10" nonScored="1" />
      <item id="5001P4213" name="Survey 10" totalMark="1" version="2" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="" unit="" quT="10" nonScored="1" />
      <item id="5001P4215" name="Survey 11" totalMark="1" version="2" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="" unit="" quT="10" nonScored="1" />
      <item id="5001P6156" name="Survey 12" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="" unit="" quT="11" nonScored="1" />
    </section>
  </assessment>
  <isValid>1</isValid>
  <isPrintable>0</isPrintable>
  <qualityReview>0</qualityReview>
  <numberOfGenerations>1</numberOfGenerations>
  <requiresInvigilation>0</requiresInvigilation>
  <advanceContentDownloadTimespan>120</advanceContentDownloadTimespan>
  <automaticVerification>1</automaticVerification>
  <offlineMode>0</offlineMode>
  <requiresValidation>0</requiresValidation>
  <requiresSecureClient>1</requiresSecureClient>
  <examType>0</examType>
  <advanceDownload>0</advanceDownload>
  <gradeBoundaryData>
    <gradeBoundaries passLevelType="1" adjustedGradeBoundaries="0">
      <grade modifier="lt" value="75" description="Fail" />
      <grade modifier="gt" value="75" description="Pass" />
    </gradeBoundaries>
  </gradeBoundaryData>
  <autoViewExam>0</autoViewExam>
  <autoProgressItems>0</autoProgressItems>
  <confirmationText>
    <confirmationText>This examination is confidential and proprietary. It is made available to you, the examinee, solely for the purpose of assessing your proficiency. You are expressly prohibited from disclosing, discussing, publishing, reproducing, or transmitting this exam, or its content, in whole or in part, in any form or by any means, verbal or written, electronic or mechanical, for any purpose.

To proceed, you must accept the terms of this agreement.</confirmationText>
  </confirmationText>
  <requiresConfirmationCheck>1</requiresConfirmationCheck>
  <allowCloseWithoutSubmit>0</allowCloseWithoutSubmit>
  <useSecureMarker>0</useSecureMarker>
  <annotationVersion>0</annotationVersion>
  <allowPackagingDelivery>1</allowPackagingDelivery>
  <scoreBoundaryData>
    <scoreBoundaries showScoreBoundaryColumn="0">
      <score modifier="lt" value="40" description="Not met" higherBoundarySet="1" />
      <score modifier="lt" value="40" description="Not met" higherBoundarySet="0" />
      <score modifier="gt" value="40" description="Close to met" higherBoundarySet="1" />
      <score modifier="gt" value="40" description="Met" higherBoundarySet="0" />
      <score modifier="gt" value="45" description="Met" higherBoundarySet="1" />
      <score modifier="gt" value="80" description="Exceeded" higherBoundarySet="1" />
    </scoreBoundaries>
  </scoreBoundaryData>
  <showCandidateReportResultColumn>0</showCandidateReportResultColumn>
  <showCandidateReportPercentageColumn>1</showCandidateReportPercentageColumn>
  <certifiedAccessible>0</certifiedAccessible>
  <markingProgressVisible>1</markingProgressVisible>
  <markingProgressMode>0</markingProgressMode>
  <secureClientOperationMode>1</secureClientOperationMode>
  <examDeliverySystemStyleType>delivery_flat</examDeliverySystemStyleType>
  <markingAutoVoidPeriod>999</markingAutoVoidPeriod>
  <showPageRequiresScrollingAlert>1</showPageRequiresScrollingAlert>
  <project ID="5001" />
</assessmentDetails>'
,'<exam passMark="75" passType="1" originalPassMark="75" originalPassType="1" totalMark="100" userMark="0" userPercentage="0" passValue="0" originalPassValue="0" totalTimeSpent="0" closeBoundaryType="0" closeBoundaryValue="0" grade="Fail" originalGrade="Fail">
	<gradeBoundaryData>
		<gradeBoundaries passLevelType="1" adjustedGradeBoundaries="0">
			<grade modifier="lt" value="75" description="Fail" />
			<grade modifier="gt" value="75" description="Pass" />
		</gradeBoundaries>
	</gradeBoundaryData>
	<section id="1" passMark="0" passType="1" name="1" totalMark="100" userMark="0" userPercentage="0" passValue="1" totalTimeSpent="0" itemsToMark="0">
		<item id="5001P2781" name="IT002379B" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="5" type="1" nonScored="1" />
		<item id="5001P2903" name="IT130504A" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="5" type="1" nonScored="1" />
		<item id="5001P2661" name="CSI00027A" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="5" type="1" nonScored="0" />
		<item id="5001P2887" name="IT130479" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="4" type="1" nonScored="0" />
		<item id="5001P2703" name="CSI00171" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="5" type="1" nonScored="1" />
		<item id="5001P2856" name="IT130421" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="4" type="1" nonScored="0" />
		<item id="5001P2875" name="IT130457" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="5" type="1" nonScored="0" />
		<item id="5001P2959" name="IT215041" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="4" type="1" nonScored="0" />
		<item id="5001P2707" name="CSI00182" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="4" type="1" nonScored="0" />
		<item id="5001P2712" name="CSI00210A" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="5" type="1" nonScored="0" />
		<item id="5001P2762" name="IT002278" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="5" type="1" nonScored="1" />
		<item id="5001P2914" name="IT131044" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="4" type="1" nonScored="0" />
		<item id="5001P2908" name="IT130510" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="6" type="1" nonScored="0" />
		<item id="5001P2757" name="IT002210A" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="7" type="1" nonScored="1" />
		<item id="5001P2786" name="IT002396" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="5" type="1" nonScored="0" />
		<item id="5001P2844" name="IT003413" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="5" type="1" nonScored="1" />
		<item id="5001P2666" name="CSI00033" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="4" type="1" nonScored="0" />
		<item id="5001P2926" name="IT131089" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="4" type="1" nonScored="0" />
		<item id="5001P2783" name="IT002387A" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="4" type="1" nonScored="0" />
		<item id="5001P2688" name="CSI00093" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="4" type="1" nonScored="0" />
		<item id="5001P2941" name="IT138769" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="6" type="1" nonScored="0" />
		<item id="5001P2713" name="CSI00216" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="5" type="1" nonScored="0" />
		<item id="5001P2904" name="IT130505" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="4" type="1" nonScored="0" />
		<item id="5001P2723" name="CSI00311" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="4" type="1" nonScored="0" />
		<item id="5001P2933" name="IT131114" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="4" type="1" nonScored="0" />
		<item id="5001P2951" name="IT214566" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="4" type="1" nonScored="0" />
		<item id="5001P2843" name="IT003412" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="5" type="1" nonScored="1" />
		<item id="5001P2909" name="IT130511" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="6" type="1" nonScored="0" />
		<item id="5001P2697" name="CSI00111B" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="5" type="1" nonScored="0" />
		<item id="5001P2720" name="CSI00230B" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="4" type="1" nonScored="0" />
		<item id="5001P2867" name="IT130439A" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="6" type="1" nonScored="0" />
		<item id="5001P2860" name="IT130426" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="5" type="1" nonScored="0" />
		<item id="5001P2853" name="IT130418A" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="6" type="1" nonScored="0" />
		<item id="5001P2972" name="IT215098A" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="5" type="1" nonScored="1" />
		<item id="5001P2870" name="IT130449" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="5" type="1" nonScored="0" />
		<item id="5001P2955" name="IT214577" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="4" type="1" nonScored="0" />
		<item id="5001P2977" name="IT215540B" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="5" type="1" nonScored="0" />
		<item id="5001P2949" name="IT214552" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="5" type="1" nonScored="0" />
		<item id="5001P2850" name="IT130258A" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="4" type="1" nonScored="0" />
		<item id="5001P2806" name="IT002718" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="4" type="1" nonScored="0" />
		<item id="5001P2714" name="CSI00223" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="4" type="1" nonScored="0" />
		<item id="5001P2689" name="CSI00094" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="4" type="1" nonScored="0" />
		<item id="5001P2779" name="IT002378" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="5" type="1" nonScored="0" />
		<item id="5001P2737" name="CSI00611" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="4" type="1" nonScored="0" />
		<item id="5001P2660" name="CSI00026" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="4" type="1" nonScored="0" />
		<item id="5001P2922" name="IT131081" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="5" type="1" nonScored="0" />
		<item id="5001P2975" name="IT215128A" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="5" type="1" nonScored="1" />
		<item id="5001P2889" name="IT130485" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="4" type="1" nonScored="0" />
		<item id="5001P2930" name="IT131095" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="4" type="1" nonScored="0" />
		<item id="5001P2924" name="IT131087A" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="6" type="1" nonScored="0" />
		<item id="5001P2715" name="CSI00225" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="4" type="1" nonScored="0" />
		<item id="5001P2751" name="IT002197A" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="7" type="1" nonScored="1" />
		<item id="5001P2655" name="CSI00023B" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="4" type="1" nonScored="0" />
		<item id="5001P2910" name="IT130512" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="5" type="1" nonScored="0" />
		<item id="5001P2902" name="IT130503" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="4" type="1" nonScored="0" />
		<item id="5001P2920" name="IT131069" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="5" type="1" nonScored="0" />
		<item id="5001P2863" name="IT130432A" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="5" type="1" nonScored="0" />
		<item id="5001P2708" name="CSI00190A" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="4" type="1" nonScored="0" />
		<item id="5001P2857" name="IT130422B" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="6" type="1" nonScored="0" />
		<item id="5001P2805" name="IT002717" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="4" type="1" nonScored="0" />
		<item id="5001P2854" name="IT130419A" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="4" type="1" nonScored="0" />
		<item id="5001P2728" name="CSI00467A" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="6" type="1" nonScored="0" />
		<item id="5001P2826" name="IT003386" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="4" type="1" nonScored="0" />
		<item id="5001P2896" name="IT130493A" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="4" type="1" nonScored="0" />
		<item id="5001P2874" name="IT130456" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="5" type="1" nonScored="0" />
		<item id="5001P2906" name="IT130508" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="4" type="1" nonScored="0" />
		<item id="5001P2809" name="IT002921A" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="4" type="1" nonScored="0" />
		<item id="5001P2654" name="CSI00022A" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="5" type="1" nonScored="1" />
		<item id="5001P2684" name="CSI00079" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="4" type="1" nonScored="0" />
		<item id="5001P2748" name="IT002183A" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="5" type="1" nonScored="1" />
		<item id="5001P2923" name="IT131086" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="6" type="1" nonScored="0" />
		<item id="5001P2956" name="IT214580" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="4" type="1" nonScored="0" />
		<item id="5001P2892" name="IT130489" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="4" type="1" nonScored="0" />
		<item id="5001P2877" name="IT130462" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="6" type="1" nonScored="0" />
		<item id="5001P2971" name="IT215097A" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="5" type="1" nonScored="0" />
		<item id="5001P2746" name="IT000152" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="4" type="1" nonScored="0" />
		<item id="5001P2749" name="IT002189A" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="5" type="1" nonScored="1" />
		<item id="5001P2893" name="IT130490A" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="4" type="1" nonScored="0" />
		<item id="5001P2960" name="IT215044A" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="4" type="1" nonScored="0" />
		<item id="5001P2678" name="CSI00067A" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="4" type="1" nonScored="0" />
		<item id="5001P2829" name="IT003389" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="4" type="1" nonScored="0" />
		<item id="5001P2760" name="IT002276A" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="6" type="1" nonScored="1" />
		<item id="5001P2664" name="CSI00028A" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="5" type="1" nonScored="1" />
		<item id="5001P2859" name="IT130425" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="5" type="1" nonScored="0" />
		<item id="5001P2830" name="IT003390" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="4" type="1" nonScored="0" />
		<item id="5001P2929" name="IT131093" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="6" type="1" nonScored="0" />
		<item id="5001P2945" name="IT214538" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="4" type="1" nonScored="0" />
		<item id="5001P2928" name="IT131091" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="4" type="1" nonScored="0" />
		<item id="5001P2732" name="CSI00501" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="4" type="1" nonScored="0" />
		<item id="5001P2741" name="IT000084" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="4" type="1" nonScored="0" />
		<item id="5001P2882" name="IT130473" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="4" type="1" nonScored="0" />
		<item id="5001P2789" name="IT002401A" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="5" type="1" nonScored="0" />
		<item id="5001P2973" name="IT215099A" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="4" type="1" nonScored="0" />
		<item id="5001P2820" name="IT003380" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="4" type="1" nonScored="0" />
		<item id="5001P2690" name="CSI00098A" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="4" type="1" nonScored="0" />
		<item id="5001P2888" name="IT130484" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="4" type="1" nonScored="0" />
		<item id="5001P2744" name="IT000139A" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="5" type="1" nonScored="0" />
		<item id="5001P2740" name="IT000044" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="4" type="1" nonScored="0" />
		<item id="5001P2872" name="IT130453" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="5" type="1" nonScored="0" />
		<item id="5001P2885" name="IT130476" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="5" type="1" nonScored="1" />
		<item id="5001P2838" name="IT003404" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="5" type="1" nonScored="1" />
		<item id="5001P2944" name="IT214533" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="4" type="1" nonScored="0" />
		<item id="5001P2785" name="IT002390" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="5" type="1" nonScored="1" />
		<item id="5001P2782" name="IT002380" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="4" type="1" nonScored="0" />
		<item id="5001P2962" name="IT215050" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="6" type="1" nonScored="0" />
		<item id="5001P2818" name="IT003378" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="4" type="1" nonScored="0" />
		<item id="5001P2766" name="IT002317A" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="4" type="1" nonScored="0" />
		<item id="5001P2790" name="IT002409A" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="5" type="1" nonScored="0" />
		<item id="5001P2822" name="IT003382" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="4" type="1" nonScored="0" />
		<item id="5001P2702" name="CSI00170" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="5" type="1" nonScored="0" />
		<item id="5001P2698" name="CSI00112" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="4" type="1" nonScored="0" />
		<item id="5001P2694" name="CSI00104B" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="6" type="1" nonScored="0" />
		<item id="5001P2925" name="IT131088A" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="5" type="1" nonScored="1" />
		<item id="5001P2801" name="IT002499" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="5" type="1" nonScored="1" />
		<item id="5001P2800" name="IT002486" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="5" type="1" nonScored="0" />
		<item id="5001P2915" name="IT131045A" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="5" type="1" nonScored="0" />
		<item id="5001P2758" name="IT002275A" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="6" type="1" nonScored="0" />
		<item id="5001P2868" name="IT130441" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="5" type="1" nonScored="0" />
		<item id="5001P2851" name="IT130415" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="4" type="1" nonScored="0" />
		<item id="5001P2652" name="CSI00021" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="4" type="1" nonScored="0" />
	</section>
	<section id="2" passMark="0" passType="1" name="S" totalMark="0" userMark="0" userPercentage="0" passValue="1" totalTimeSpent="0" itemsToMark="0">
		<item id="5001P4221" name="Survey Intro" totalMark="0" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="2" type="3" nonScored="0" />
		<item id="5001P4189" name="Survey 01" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="5" type="1" nonScored="1" />
		<item id="5001P4192" name="Survey 02" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="4" type="1" nonScored="1" />
		<item id="5001P4196" name="Survey 03" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="3" type="1" nonScored="1" />
		<item id="5001P4201" name="Survey 04" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="2" type="1" nonScored="1" />
		<item id="5001P4203" name="Survey 05" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="2" type="1" nonScored="1" />
		<item id="5001P4205" name="Survey 06" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="2" type="1" nonScored="1" />
		<item id="5001P4207" name="Survey 07" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="2" type="1" nonScored="1" />
		<item id="5001P4209" name="Survey 08" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="2" type="1" nonScored="1" />
		<item id="5001P4211" name="Survey 09" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="2" type="1" nonScored="1" />
		<item id="5001P4213" name="Survey 10" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="2" type="1" nonScored="1" />
		<item id="5001P4215" name="Survey 11" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="2" type="1" nonScored="1" />
		<item id="5001P6156" name="Survey 12" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="3" type="1" nonScored="1" />
	</section>
</exam>
'
,'<assessmentDetails>
	<assessmentID>8</assessmentID>
	<qualificationID>103</qualificationID>
	<qualificationName>Construction Specifications Institute</qualificationName>
	<qualificationReference>CSI_</qualificationReference>
	<assessmentGroupName>Construction Documents Technologist</assessmentGroupName>
	<assessmentGroupID>1</assessmentGroupID>
	<assessmentGroupReference>CDT</assessmentGroupReference>
	<assessmentName>Domestic Form 1</assessmentName>
	<validFromDate>08 Feb 2016</validFromDate>
	<expiryDate>08 Feb 2026</expiryDate>
	<startTime>00:00:00</startTime>
	<endTime>23:59:59</endTime>
	<duration>135</duration>
	<defaultDuration>135</defaultDuration>
	<scheduledDuration>
		<value>135</value>
		<reason />
	</scheduledDuration>
	<sRBonus>180</sRBonus>
	<sRBonusMaximum>240</sRBonusMaximum>
	<externalReference>6038039021</externalReference>
	<passLevelValue>75</passLevelValue>
	<passLevelType>1</passLevelType>
	<status>2</status>
	<testFeedbackType>
		<passFail>0</passFail>
		<percentageMark>0</percentageMark>
		<allowSummaryFeedback>0</allowSummaryFeedback>
		<summaryFeedbackType>1</summaryFeedbackType>
		<itemSummary>0</itemSummary>
		<itemReview>0</itemReview>
		<itemReviewCorrectAnswer>0</itemReviewCorrectAnswer>
		<itemFeedback>0</itemFeedback>
		<printableSummary>0</printableSummary>
		<candidateDetails>0</candidateDetails>
		<feedbackByReference>0</feedbackByReference>
		<allowFeedbackDuringExam>0</allowFeedbackDuringExam>
		<allowFeedbackDuringSummary>0</allowFeedbackDuringSummary>
	</testFeedbackType>
	<itemFeedback>0</itemFeedback>
	<allowCalculator>0</allowCalculator>
	<lastInUseDate>29 Feb 2016 09:40:57</lastInUseDate>
	<completedScriptReview>0</completedScriptReview>
	<assessment currentSection="0" totalTime="0" currentTime="0" originalPassMark="75" originalPassType="1" passMark="75" passType="1" totalMark="100" userMark="0" userPercentage="0" passValue="0" originalGrade="Fail">
		<intro id="0" name="Introduction" currentItem="">
			<item id="5001P3414" name="CDT Introduction Page" totalMark="0" version="11" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="4" LO="" unit="" quT="0" />
			<item id="5001P4848" name="Tutorial - Intro" totalMark="0" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="4" LO="" unit="" quT="0" />
			<item id="5001P4850" name="Tutorial - Mouse" totalMark="0" version="2" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="4" LO="" unit="" quT="0" />
			<item id="5001P4853" name="Tutorial - Navigation" totalMark="0" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="4" LO="" unit="" quT="0" />
			<item id="5001P4855" name="Tutorial - Time" totalMark="0" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="4" LO="" unit="" quT="0" />
			<item id="5001P4857" name="Tutorial - Scroll" totalMark="0" version="2" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="4" LO="" unit="" quT="0" />
			<item id="5001P4859" name="Tutorial - Multiple Choice" totalMark="0" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="4" LO="" unit="" quT="0" />
			<item id="5001P4862" name="Tutorial - Flags" totalMark="0" version="2" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="4" LO="" unit="" quT="0" />
			<item id="5001P4864" name="Tutorial - Highlight" totalMark="0" version="2" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="4" LO="" unit="" quT="0" />
			<item id="5001P4866" name="Tutorial - Strikeout" totalMark="0" version="7" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="4" LO="" unit="" quT="0" />
			<item id="5001P4868" name="Tutorial - Ending Exam" totalMark="0" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="4" LO="" unit="" quT="0" />
			<item id="5001P4871" name="Tutorial - Conclusion" totalMark="0" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="4" LO="" unit="" quT="0" />
		</intro>
		<outro id="0" name="Finish" currentItem="">
			<item id="5001P3402" name="Finish Page" totalMark="0" version="14" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="2" LO="" unit="" quT="0" />
		</outro>
		<tools id="0" name="Tools" currentItem="" />
		<section id="1" name="1" passLevelValue="0" passLevelType="1" itemsToMark="0" currentItem="" fixed="1" totalMark="100" userMark="0" userPercentage="0" passValue="1">
			<item id="5001P2781" name="IT002379B" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="" unit="" quT="10" nonScored="1">
				<p cs="1" ua="1" id="5001P2781" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="1" sl="1" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2903" name="IT130504A" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="" unit="" quT="10" nonScored="1">
				<p cs="1" ua="1" id="5001P2903" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="1" sl="1" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2661" name="CSI00027A" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Fundamentals" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2661" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="1" sl="1" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2887" name="IT130479" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction Documents" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2887" um="0">
					<s id="1" ua="1" um="0">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="0">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="1" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="1" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2703" name="CSI00171" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="" unit="" quT="10" nonScored="1">
				<p cs="1" ua="1" id="5001P2703" um="0">
					<s id="1" ua="1" um="0">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="0">
							<i id="1" ca="1" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="1" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2856" name="IT130421" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction Documents" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2856" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="1" sl="1" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2875" name="IT130457" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction." unit="" quT="10">
				<p cs="1" ua="1" id="5001P2875" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="1" sl="1" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2959" name="IT215041" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Lifecycle Activities" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2959" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="1" sl="1" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2707" name="CSI00182" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Design Process" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2707" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="1" sl="1" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2712" name="CSI00210A" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction." unit="" quT="10">
				<p cs="1" ua="1" id="5001P2712" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="1" sl="1" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2762" name="IT002278" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="" unit="" quT="10" nonScored="1">
				<p cs="1" ua="1" id="5001P2762" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="1" sl="1" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2914" name="IT131044" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Procurement" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2914" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="1" sl="1" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2908" name="IT130510" totalMark="1" version="6" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction." unit="" quT="10">
				<p cs="1" ua="1" id="5001P2908" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="1" sl="1" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2757" name="IT002210A" totalMark="1" version="7" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="" unit="" quT="10" nonScored="1">
				<p cs="1" ua="1" id="5001P2757" um="0">
					<s id="1" ua="1" um="0">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="0">
							<i id="1" ca="1" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="1" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2786" name="IT002396" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction." unit="" quT="10">
				<p cs="1" ua="1" id="5001P2786" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="1" sl="1" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2844" name="IT003413" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="" unit="" quT="10" nonScored="1">
				<p cs="1" ua="1" id="5001P2844" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="1" sl="1" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2666" name="CSI00033" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction Documents" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2666" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="1" sl="1" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2926" name="IT131089" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction Documents" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2926" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="1" sl="1" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2783" name="IT002387A" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Lifecycle Activities" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2783" um="0">
					<s id="1" ua="1" um="0">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="0">
							<i id="1" ca="1" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="1" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2688" name="CSI00093" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Design Process" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2688" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="1" sl="1" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2941" name="IT138769" totalMark="1" version="6" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Project Concept and Delivery" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2941" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="1" sl="1" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2713" name="CSI00216" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction Documents" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2713" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="1" sl="1" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2904" name="IT130505" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction Documents" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2904" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="1" sl="1" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2723" name="CSI00311" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Procurement" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2723" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="1" sl="1" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2933" name="IT131114" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction Documents" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2933" um="0">
					<s id="1" ua="1" um="0">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="0">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="1" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="1" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2951" name="IT214566" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction Documents" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2951" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="1" sl="1" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2843" name="IT003412" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="" unit="" quT="10" nonScored="1">
				<p cs="1" ua="1" id="5001P2843" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="1" sl="1" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2909" name="IT130511" totalMark="1" version="6" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction." unit="" quT="10">
				<p cs="1" ua="1" id="5001P2909" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="1" sl="1" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2697" name="CSI00111B" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction Documents" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2697" um="0">
					<s id="1" ua="1" um="0">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="0">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="1" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="1" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2720" name="CSI00230B" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction Documents" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2720" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="1" sl="1" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2867" name="IT130439A" totalMark="1" version="6" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Project Concept and Delivery" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2867" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="1" sl="1" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2860" name="IT130426" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Preconstruction" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2860" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="1" sl="1" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2853" name="IT130418A" totalMark="1" version="6" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Project Concept and Delivery" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2853" um="0">
					<s id="1" ua="1" um="0">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="0">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="1" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="1" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2972" name="IT215098A" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="" unit="" quT="10" nonScored="1">
				<p cs="1" ua="1" id="5001P2972" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="1" sl="1" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2870" name="IT130449" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction Documents" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2870" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="1" sl="1" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2955" name="IT214577" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction Documents" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2955" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="1" sl="1" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2977" name="IT215540B" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Lifecycle Activities" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2977" um="0">
					<s id="1" ua="1" um="0">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="0">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="1" ac="C" struckOut="0" />
							<i id="4" ca="1" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2949" name="IT214552" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction." unit="" quT="10">
				<p cs="1" ua="1" id="5001P2949" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="1" sl="1" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2850" name="IT130258A" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction Documents" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2850" um="0">
					<s id="1" ua="1" um="0">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="0">
							<i id="1" ca="1" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="1" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2806" name="IT002718" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Fundamentals" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2806" um="0">
					<s id="1" ua="1" um="0">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="0">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="1" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="1" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2714" name="CSI00223" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction Documents" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2714" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="1" sl="1" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2689" name="CSI00094" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Fundamentals" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2689" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="1" sl="1" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2779" name="IT002378" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Lifecycle Activities" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2779" um="0">
					<s id="1" ua="1" um="0">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="0">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="1" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="1" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2737" name="CSI00611" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction Documents" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2737" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="1" sl="1" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2660" name="CSI00026" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Preconstruction" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2660" um="0">
					<s id="1" ua="1" um="0">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="0">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="1" ac="B" struckOut="0" />
							<i id="3" ca="1" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2922" name="IT131081" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction." unit="" quT="10">
				<p cs="1" ua="1" id="5001P2922" um="0">
					<s id="1" ua="1" um="0">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="0">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="1" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="1" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2975" name="IT215128A" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="" unit="" quT="10" nonScored="1">
				<p cs="1" ua="1" id="5001P2975" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="1" sl="1" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2889" name="IT130485" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction Documents" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2889" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="1" sl="1" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2930" name="IT131095" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction Documents" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2930" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="1" sl="1" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2924" name="IT131087A" totalMark="1" version="6" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Project Concept and Delivery" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2924" um="0">
					<s id="1" ua="1" um="0">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="0">
							<i id="1" ca="0" sl="1" ac="A" struckOut="0" />
							<i id="2" ca="1" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2715" name="CSI00225" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Fundamentals" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2715" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="1" sl="1" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2751" name="IT002197A" totalMark="1" version="7" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="" unit="" quT="10" nonScored="1">
				<p cs="1" ua="1" id="5001P2751" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="1" sl="1" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2655" name="CSI00023B" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction Documents" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2655" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="1" sl="1" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2910" name="IT130512" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction." unit="" quT="10">
				<p cs="1" ua="1" id="5001P2910" um="0">
					<s id="1" ua="1" um="0">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="0">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="1" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="1" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2902" name="IT130503" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction Documents" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2902" um="0">
					<s id="1" ua="1" um="0">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="0">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="1" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="1" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2920" name="IT131069" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Design Process" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2920" um="0">
					<s id="1" ua="1" um="0">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="0">
							<i id="1" ca="1" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="1" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2863" name="IT130432A" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction Documents" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2863" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="1" sl="1" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2708" name="CSI00190A" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Procurement" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2708" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="1" sl="1" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2857" name="IT130422B" totalMark="1" version="6" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Project Concept and Delivery" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2857" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="1" sl="1" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2805" name="IT002717" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Fundamentals" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2805" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="1" sl="1" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2854" name="IT130419A" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Fundamentals" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2854" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="1" sl="1" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2728" name="CSI00467A" totalMark="1" version="6" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Project Concept and Delivery" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2728" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="1" sl="1" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2826" name="IT003386" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Design Process" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2826" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="1" sl="1" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2896" name="IT130493A" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Procurement" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2896" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="1" sl="1" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2874" name="IT130456" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction." unit="" quT="10">
				<p cs="1" ua="1" id="5001P2874" um="0">
					<s id="1" ua="1" um="0">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="0">
							<i id="1" ca="1" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="1" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2906" name="IT130508" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Fundamentals" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2906" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="1" sl="1" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2809" name="IT002921A" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Design Process" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2809" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="1" sl="1" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2654" name="CSI00022A" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="" unit="" quT="10" nonScored="1">
				<p cs="1" ua="1" id="5001P2654" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="1" sl="1" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2684" name="CSI00079" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Lifecycle Activities" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2684" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="1" sl="1" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2748" name="IT002183A" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="" unit="" quT="10" nonScored="1">
				<p cs="1" ua="1" id="5001P2748" um="0">
					<s id="1" ua="1" um="0">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="0">
							<i id="1" ca="0" sl="1" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="1" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2923" name="IT131086" totalMark="1" version="6" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Project Concept and Delivery" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2923" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="1" sl="1" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2956" name="IT214580" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Preconstruction" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2956" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="1" sl="1" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2892" name="IT130489" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction Documents" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2892" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="1" sl="1" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2877" name="IT130462" totalMark="1" version="6" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction." unit="" quT="10">
				<p cs="1" ua="1" id="5001P2877" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="1" sl="1" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2971" name="IT215097A" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Procurement" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2971" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="1" sl="1" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2746" name="IT000152" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Design Process" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2746" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="1" sl="1" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2749" name="IT002189A" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="" unit="" quT="10" nonScored="1">
				<p cs="1" ua="1" id="5001P2749" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="1" sl="1" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2893" name="IT130490A" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction Documents" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2893" um="0">
					<s id="1" ua="1" um="0">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="0">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="1" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="1" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2960" name="IT215044A" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Fundamentals" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2960" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="1" sl="1" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2678" name="CSI00067A" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Procurement" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2678" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="1" sl="1" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2829" name="IT003389" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Design Process" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2829" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="1" sl="1" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2760" name="IT002276A" totalMark="1" version="6" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="" unit="" quT="10" nonScored="1">
				<p cs="1" ua="1" id="5001P2760" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="1" sl="1" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2664" name="CSI00028A" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="" unit="" quT="10" nonScored="1">
				<p cs="1" ua="1" id="5001P2664" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="1" sl="1" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2859" name="IT130425" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction Documents" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2859" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="1" sl="1" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2830" name="IT003390" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Design Process" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2830" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="1" sl="1" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2929" name="IT131093" totalMark="1" version="6" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Project Concept and Delivery" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2929" um="0">
					<s id="1" ua="1" um="0">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="0">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="1" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="1" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2945" name="IT214538" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Preconstruction" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2945" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="1" sl="1" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2928" name="IT131091" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Preconstruction" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2928" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="1" sl="1" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2732" name="CSI00501" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Fundamentals" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2732" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="1" sl="1" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2741" name="IT000084" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Fundamentals" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2741" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="1" sl="1" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2882" name="IT130473" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction Documents" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2882" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="1" sl="1" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2789" name="IT002401A" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Preconstruction" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2789" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="1" sl="1" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2973" name="IT215099A" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Procurement" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2973" um="0">
					<s id="1" ua="1" um="0">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="0">
							<i id="1" ca="1" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="1" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2820" name="IT003380" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Design Process" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2820" um="0">
					<s id="1" ua="1" um="0">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="0">
							<i id="1" ca="0" sl="1" ac="A" struckOut="0" />
							<i id="2" ca="1" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2690" name="CSI00098A" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction Documents" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2690" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="1" sl="1" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2888" name="IT130484" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction Documents" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2888" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="1" sl="1" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2744" name="IT000139A" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction." unit="" quT="10">
				<p cs="1" ua="1" id="5001P2744" um="0">
					<s id="1" ua="1" um="0">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="0">
							<i id="1" ca="1" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="1" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2740" name="IT000044" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Fundamentals" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2740" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="1" sl="1" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2872" name="IT130453" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction." unit="" quT="10">
				<p cs="1" ua="1" id="5001P2872" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="1" sl="1" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2885" name="IT130476" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="" unit="" quT="10" nonScored="1">
				<p cs="1" ua="1" id="5001P2885" um="0">
					<s id="1" ua="1" um="0">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="0">
							<i id="1" ca="1" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="1" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2838" name="IT003404" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="" unit="" quT="10" nonScored="1">
				<p cs="1" ua="1" id="5001P2838" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="1" sl="1" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2944" name="IT214533" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Fundamentals" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2944" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="1" sl="1" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2785" name="IT002390" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="" unit="" quT="10" nonScored="1">
				<p cs="1" ua="1" id="5001P2785" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="1" sl="1" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2782" name="IT002380" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Lifecycle Activities" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2782" um="0">
					<s id="1" ua="1" um="0">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="0">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="1" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="1" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2962" name="IT215050" totalMark="1" version="6" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Project Concept and Delivery" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2962" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="1" sl="1" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2818" name="IT003378" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Design Process" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2818" um="0">
					<s id="1" ua="1" um="0">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="0">
							<i id="1" ca="0" sl="1" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="1" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2766" name="IT002317A" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Design Process" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2766" um="0">
					<s id="1" ua="1" um="0">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="0">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="1" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="1" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2790" name="IT002409A" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction." unit="" quT="10">
				<p cs="1" ua="1" id="5001P2790" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="1" sl="1" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2822" name="IT003382" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Design Process" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2822" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="1" sl="1" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2702" name="CSI00170" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction." unit="" quT="10">
				<p cs="1" ua="1" id="5001P2702" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="1" sl="1" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2698" name="CSI00112" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Procurement" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2698" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="1" sl="1" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2694" name="CSI00104B" totalMark="1" version="6" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Project Concept and Delivery" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2694" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="1" sl="1" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2925" name="IT131088A" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="" unit="" quT="10" nonScored="1">
				<p cs="1" ua="1" id="5001P2925" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="1" sl="1" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2801" name="IT002499" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="" unit="" quT="10" nonScored="1">
				<p cs="1" ua="1" id="5001P2801" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="1" sl="1" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2800" name="IT002486" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction." unit="" quT="10">
				<p cs="1" ua="1" id="5001P2800" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="1" sl="1" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2915" name="IT131045A" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction." unit="" quT="10">
				<p cs="1" ua="1" id="5001P2915" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="1" sl="1" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2758" name="IT002275A" totalMark="1" version="6" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Procurement" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2758" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="1" sl="1" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2868" name="IT130441" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Procurement" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2868" um="0">
					<s id="1" ua="1" um="0">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="0">
							<i id="1" ca="1" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="1" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2851" name="IT130415" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction Documents" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2851" um="0">
					<s id="1" ua="1" um="0">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="0">
							<i id="1" ca="0" sl="1" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="1" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2652" name="CSI00021" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction Documents" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2652" um="0">
					<s id="1" ua="1" um="0">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="0">
							<i id="1" ca="1" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="1" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
		</section>
		<section id="2" name="S" passLevelValue="0" passLevelType="1" itemsToMark="0" currentItem="" fixed="1" totalMark="0" userMark="0" userPercentage="0" passValue="1">
			<item id="5001P4221" name="Survey Intro" totalMark="0" version="2" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="3" LO="" unit="" quT="0">
				<p cs="1" ua="0" id="5001P4221" um="0">
					<s id="1" ua="0" um="0">
						<c id="2" typ="1">
							<i id="1" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P4189" name="Survey 01" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="" unit="" quT="10" nonScored="1">
				<p cs="1" ua="1" id="5001P4189" um="0">
					<s id="1" ua="1" um="0">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="0">
							<i id="1" ca="1" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="1" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
							<i id="5" ca="0" sl="0" ac="E" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P4192" name="Survey 02" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="" unit="" quT="10" nonScored="1">
				<p cs="1" ua="1" id="5001P4192" um="0">
					<s id="1" ua="1" um="0">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="0">
							<i id="1" ca="1" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
							<i id="5" ca="0" sl="1" ac="E" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P4196" name="Survey 03" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="" unit="" quT="10" nonScored="1">
				<p cs="1" ua="1" id="5001P4196" um="0">
					<s id="1" ua="1" um="0">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="0">
							<i id="1" ca="1" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="1" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
							<i id="5" ca="0" sl="0" ac="E" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P4201" name="Survey 04" totalMark="1" version="2" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="" unit="" quT="10" nonScored="1">
				<p cs="1" ua="1" id="5001P4201" um="0">
					<s id="1" ua="1" um="0">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="0">
							<i id="1" ca="1" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="1" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
							<i id="5" ca="0" sl="0" ac="E" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P4203" name="Survey 05" totalMark="1" version="2" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="" unit="" quT="10" nonScored="1">
				<p cs="1" ua="1" id="5001P4203" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="1" sl="1" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
							<i id="5" ca="0" sl="0" ac="E" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P4205" name="Survey 06" totalMark="1" version="2" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="" unit="" quT="10" nonScored="1">
				<p cs="1" ua="1" id="5001P4205" um="0">
					<s id="1" ua="1" um="0">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="0">
							<i id="1" ca="1" sl="0" ac="A" struckOut="0" x="145" y="317" wid="795" hei="60" labelId="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" x="145" y="242" wid="795" hei="60" labelId="0" />
							<i id="3" ca="0" sl="1" ac="C" struckOut="0" x="145" y="392" wid="795" hei="60" labelId="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" x="145" y="167" wid="795" hei="60" labelId="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P4207" name="Survey 07" totalMark="1" version="2" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="" unit="" quT="10" nonScored="1">
				<p cs="1" ua="1" id="5001P4207" um="0">
					<s id="1" ua="1" um="0">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="0">
							<i id="1" ca="1" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="1" ac="D" struckOut="0" />
							<i id="5" ca="0" sl="0" ac="E" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P4209" name="Survey 08" totalMark="1" version="2" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="" unit="" quT="10" nonScored="1">
				<p cs="1" ua="1" id="5001P4209" um="0">
					<s id="1" ua="1" um="0">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="0">
							<i id="1" ca="1" sl="1" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="1" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
							<i id="5" ca="0" sl="0" ac="E" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P4211" name="Survey 09" totalMark="1" version="2" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="" unit="" quT="10" nonScored="1">
				<p cs="1" ua="1" id="5001P4211" um="0">
					<s id="1" ua="1" um="0">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="0">
							<i id="1" ca="1" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="1" ac="D" struckOut="0" />
							<i id="5" ca="0" sl="0" ac="E" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P4213" name="Survey 10" totalMark="1" version="2" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="" unit="" quT="10" nonScored="1">
				<p cs="1" ua="1" id="5001P4213" um="0">
					<s id="1" ua="1" um="0">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="0">
							<i id="1" ca="1" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
							<i id="5" ca="0" sl="1" ac="E" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P4215" name="Survey 11" totalMark="1" version="2" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="" unit="" quT="10" nonScored="1">
				<p cs="1" ua="1" id="5001P4215" um="0">
					<s id="1" ua="1" um="0">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="0">
							<i id="1" ca="1" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="1" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
							<i id="5" ca="0" sl="0" ac="E" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P6156" name="Survey 12" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="" unit="" quT="11" nonScored="1">
				<p cs="1" ua="1" id="5001P6156" um="0">
					<s id="1" ua="1" um="0">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="11" um="0">
							<i id="1" ca="This is the correct answer.">none</i>
						</c>
					</s>
				</p>
			</item>
		</section>
	</assessment>
	<isValid>1</isValid>
	<isPrintable>0</isPrintable>
	<qualityReview>0</qualityReview>
	<numberOfGenerations>1</numberOfGenerations>
	<requiresInvigilation>0</requiresInvigilation>
	<advanceContentDownloadTimespan>120</advanceContentDownloadTimespan>
	<automaticVerification>1</automaticVerification>
	<offlineMode>0</offlineMode>
	<requiresValidation>0</requiresValidation>
	<requiresSecureClient>1</requiresSecureClient>
	<examType>0</examType>
	<advanceDownload>0</advanceDownload>
	<gradeBoundaryData>
		<gradeBoundaries passLevelType="1" adjustedGradeBoundaries="0">
			<grade modifier="lt" value="75" description="Fail" />
			<grade modifier="gt" value="75" description="Pass" />
		</gradeBoundaries>
	</gradeBoundaryData>
	<autoViewExam>0</autoViewExam>
	<autoProgressItems>0</autoProgressItems>
	<confirmationText>
		<confirmationText>This examination is confidential and proprietary. It is made available to you, the examinee, solely for the purpose of assessing your proficiency. You are expressly prohibited from disclosing, discussing, publishing, reproducing, or transmitting this exam, or its content, in whole or in part, in any form or by any means, verbal or written, electronic or mechanical, for any purpose. To proceed, you must accept the terms of this agreement.</confirmationText>
	</confirmationText>
	<requiresConfirmationCheck>1</requiresConfirmationCheck>
	<allowCloseWithoutSubmit>0</allowCloseWithoutSubmit>
	<useSecureMarker>0</useSecureMarker>
	<annotationVersion>0</annotationVersion>
	<allowPackagingDelivery>1</allowPackagingDelivery>
	<scoreBoundaryData>
		<scoreBoundaries showScoreBoundaryColumn="0">
			<score modifier="lt" value="40" description="Not met" higherBoundarySet="1" />
			<score modifier="lt" value="40" description="Not met" higherBoundarySet="0" />
			<score modifier="gt" value="40" description="Close to met" higherBoundarySet="1" />
			<score modifier="gt" value="40" description="Met" higherBoundarySet="0" />
			<score modifier="gt" value="45" description="Met" higherBoundarySet="1" />
			<score modifier="gt" value="80" description="Exceeded" higherBoundarySet="1" />
		</scoreBoundaries>
	</scoreBoundaryData>
	<showCandidateReportResultColumn>0</showCandidateReportResultColumn>
	<showCandidateReportPercentageColumn>1</showCandidateReportPercentageColumn>
	<certifiedAccessible>0</certifiedAccessible>
	<markingProgressVisible>1</markingProgressVisible>
	<markingProgressMode>0</markingProgressMode>
	<secureClientOperationMode>1</secureClientOperationMode>
	<examDeliverySystemStyleType>delivery_flat</examDeliverySystemStyleType>
	<markingAutoVoidPeriod>999</markingAutoVoidPeriod>
	<showPageRequiresScrollingAlert>1</showPageRequiresScrollingAlert>
	<project ID="5001" />
</assessmentDetails>'
);

SELECT	 E.ID
		,E.ResultData.value('(/exam/@passMark)[1]', 'int') AS [ExamPassMark]
		,E.ResultData.value('(/exam/@passType)[1]', 'tinyint') AS [ExamPassType]
		,E.ResultData.value('(/exam/@totalMark)[1]', 'int') AS [ExamTotalMark]
		,E.ResultData.value('(/exam/@userMark)[1]', 'decimal(6,3)') AS [ExamUserMark]
		,E.ResultData.value('(/exam/@userPercentage)[1]', 'decimal(6,3)') AS [ExamUserPercentage]
		,E.ResultData.value('(/exam/@passValue)[1]', 'tinyint') AS [ExamPassValue]
		,E.ResultData.value('(/exam/@originalPassValue)[1]', 'tinyint') AS [ExamOriginalPassValue]
		,E.ResultData.value('(/exam/@grade)[1]', 'nvarchar(100)') AS [ExamGrade]
		,E.ResultData.value('(/exam/@originalGrade)[1]', 'nvarchar(100)') AS [ExamOriginalGrade]
		--,Exam.Section.query('.')
		,Exam.Section.value('@id', 'int') AS [SectionID]
		,Exam.Section.value('@passMark', 'int') AS [SectionPassMark]
		,Exam.Section.value('@passType', 'tinyint') AS [SectionPassType]
		,Exam.Section.value('@totalMark', 'int') AS [SectionTotalMark]
		,Exam.Section.value('@userMark', 'decimal(6,3)') AS [SectionUserMark]
		,Exam.Section.value('@userPercentage', 'decimal(6,3)') AS [SectionUserPercentage]
		,Exam.Section.value('@passValue', 'tinyint') AS [SectionPassValue]
		--,Section.Item.query('.')
		,Section.Item.value('@id', 'nvarchar(50)') AS [ItemID]
		,Section.Item.value('@totalMark', 'int') AS [ItemTotalMark]
		,Section.Item.value('@userMark', 'decimal(6,3)') AS [ItemUserMark]
		,Section.Item.value('@actualUserMark', 'decimal(6,3)') AS [ItemActualUserMark]
		,Section.Item.value('@userAttempted', 'tinyint') AS [ItemUserAttempted]
INTO	 #Item
FROM	 #ExamXML E
CROSS APPLY E.ResultData.nodes('/exam/section') Exam(Section)
CROSS APPLY Exam.Section.nodes('item') Section(Item)
where ISNULL(Section.Item.value('@nonScored', 'bit'), 0) = '0';

SELECT * FROM #ExamXML;
SELECT * FROM #Item;


/* Update item user attempted and item user mark */
UPDATE #Item
SET ItemUserAttempted = #WAREHOUSE_ExamSessionItemResponseTable.ItemResponseData.value('(/p/@ua)[1]', 'tinyint')
   ,ItemUserMark = (#WAREHOUSE_ExamSessionItemResponseTable.ItemResponseData.value('(/p/@um)[1]', 'decimal(6,3)') * ItemTotalMark)
FROM #Item
INNER JOIN #WAREHOUSE_ExamSessionItemResponseTable
ON #Item.ID = #WAREHOUSE_ExamSessionItemResponseTable.WAREHOUSEExamSessionID
AND #Item.ItemID = #WAREHOUSE_ExamSessionItemResponseTable.ItemID

/* Update item user mark based on section marking percentage
   Correct the ones over 100% */
UPDATE #Item
SET ItemUserMark = CASE
				       WHEN SectionPassType = 1 AND ItemTotalMark > 1
							THEN CAST((ItemUserMark / ItemTotalMark) AS DECIMAL(6,3))
					   ELSE ItemUserMark
				   END;
/* Update item actual user mark */
UPDATE #Item
SET ItemActualUserMark = ROUND(CASE
						     WHEN SectionPassType = 1 THEN ItemTotalMark * ItemUserMark
							 ELSE ItemActualUserMark
						 END, 0);

/* Update section mark based on item mark */

UPDATE #Item
SET SectionUserMark = SectionMarks.SectionUserMark
   ,SectionTotalMark = SectionMarks.SectionTotalMark
   ,SectionUserPercentage = 
	  CASE WHEN SectionMarks.SectionTotalMark > 0
		THEN  CAST((SectionMarks.SectionUserMark / SectionMarks.SectionTotalMark) AS DECIMAL(6,3)) * 100
		ELSE SectionUserPercentage
		END	
FROM	#Item Item
INNER JOIN (
	SELECT	 ID
			,SectionID
			,SUM(CASE
					WHEN SectionPassType = 1 THEN ItemActualUserMark
					WHEN SectionPassType = 0 THEN ItemUserMark
				END) AS [SectionUserMark]
			,SUM(ItemTotalMark) AS [SectionTotalMark]
	FROM	 #Item
	GROUP BY ID
			,SectionPassType
			,SectionID
		) SectionMarks
ON Item.ID = SectionMarks.ID
AND Item.SectionID = SectionMarks.SectionID;


/* Update section pass value */
UPDATE #Item
SET SectionPassValue = CASE
						   WHEN CASE SectionPassType
								    WHEN 1 THEN 
									    SectionUserPercentage
								    WHEN 0 THEN
									    SectionUserMark
							    END >= SectionPassMark THEN 1
						   ELSE 0
					   END;

/* Update exam metadata */
UPDATE	 #Item
SET ExamTotalMark = ExamMarks.ExamTotalMark
   ExamUserMark = ExamMarks.ExamUserMark
FROM #Item AS [Item]
INNER JOIN (
	SELECT	 ID
			,SUM(SectionTotalMark) AS [ExamTotalMark]
			,SUM(SectionUserMark) AS [ExamUserMark]
	FROM	(
		SELECT	 ID
				,SectionTotalMark
				,SectionUserMark
		FROM #Item
		GROUP BY ID
				,SectionID
				,SectionTotalMark
				,SectionUserMark
			) Section
	GROUP BY ID
	) ExamMarks
ON Item.ID = ExamMarks.ID;

/* Update exam user percentage */
UPDATE #Item
SET ExamUserPercentage = (ExamUserMark / ExamTotalMark) * 100;

/* Update exam pass value */
UPDATE #Item
SET ExamPassValue = CASE
						WHEN CASE ExamPassType
								WHEN 1 THEN 
									ExamUserPercentage
								WHEN 0 THEN
									ExamUserMark
							END >= ExamPassMark THEN 1
						ELSE 0
					END;

UPDATE #Item
SET ExamOriginalPassValue = ExamPassValue;

UPDATE #Item
SET ExamGrade = CASE ExamPassValue
					WHEN 1 THEN N'Pass'
					WHEN 0 THEN N'Fail'
				END;

UPDATE #Item
SET ExamOriginalGrade = ExamGrade;

DECLARE Exams CURSOR FOR
	SELECT DISTINCT ID, ExamTotalMark, ExamUserMark, ExamUserPercentage, ExamPassValue, ExamOriginalPassValue, ExamGrade, ExamOriginalGrade FROM #Item;
OPEN Exams;
DECLARE @ID INT, @ExamTotalMark INT, @ExamUserMark DECIMAL(6,3), @ExamUserPercentage DECIMAL(6,3), @ExamPassValue TINYINT, @ExamOriginalPassValue TINYINT, @ExamGrade NVARCHAR(100), @ExamOriginalGrade NVARCHAR(100);
FETCH NEXT FROM Exams INTO @ID, @ExamTotalMark, @ExamUserMark, @ExamUserPercentage, @ExamPassValue, @ExamOriginalPassValue, @ExamGrade, @ExamOriginalGrade;

WHILE @@FETCH_STATUS = 0
BEGIN
	/* Update exam */
	/* Update exam total mark */
	UPDATE #ExamXML
	SET StructureXML.modify('replace value of (/assessmentDetails/assessment/@totalMark)[1] with sql:variable("@ExamTotalMark")')
	   ,ResultData.modify('replace value of (/exam/@totalMark)[1] with sql:variable("@ExamTotalMark")')
	   ,ResultDataFull.modify('replace value of (/assessmentDetails/assessment/@totalMark)[1] with sql:variable("@ExamTotalMark")')
	WHERE ID = @ID;

	/* Update exam user mark */
	UPDATE #ExamXML
	SET StructureXML.modify('replace value of (/assessmentDetails/assessment/@userMark)[1] with sql:variable("@ExamUserMark")')
	   ,ResultData.modify('replace value of (/exam/@userMark)[1] with sql:variable("@ExamUserMark")')
	   ,ResultDataFull.modify('replace value of (/assessmentDetails/assessment/@userMark)[1] with sql:variable("@ExamUserMark")')
	WHERE ID = @ID;

	/* Update exam user percentage */
	UPDATE #ExamXML
	SET StructureXML.modify('replace value of (/assessmentDetails/assessment/@userPercentage)[1] with sql:variable("@ExamUserPercentage")')
	   ,ResultData.modify('replace value of (/exam/@userPercentage)[1] with sql:variable("@ExamUserPercentage")')
	   ,ResultDataFull.modify('replace value of (/assessmentDetails/assessment/@userPercentage)[1] with sql:variable("@ExamUserPercentage")')
	WHERE ID = @ID;

	/* Update exam pass value */
	UPDATE #ExamXML
	SET StructureXML.modify('replace value of (/assessmentDetails/assessment/@passValue)[1] with sql:variable("@ExamPassValue")')
	   ,ResultData.modify('replace value of (/exam/@passValue)[1] with sql:variable("@ExamPassValue")')
	   ,ResultDataFull.modify('replace value of (/assessmentDetails/assessment/@passValue)[1] with sql:variable("@ExamPassValue")')
	WHERE ID = @ID;

	/* Update exam original pass value */
	UPDATE #ExamXML
	SET ResultData.modify('replace value of (/exam/@originalPassValue)[1] with sql:variable("@ExamOriginalPassValue")')
	WHERE ID = @ID;

	/* Update exam grade */
	UPDATE #ExamXML
	SET ResultData.modify('replace value of (/exam/@grade)[1] with sql:variable("@ExamGrade")')
	WHERE ID = @ID;

	/* Update exam original grade */
	UPDATE #ExamXML
	SET StructureXML.modify('replace value of (/assessmentDetails/assessment/@originalGrade)[1] with sql:variable("@ExamOriginalGrade")')
	   ,ResultData.modify('replace value of (/exam/@originalGrade)[1] with sql:variable("@ExamOriginalGrade")')
	   ,ResultDataFull.modify('replace value of (/assessmentDetails/assessment/@originalGrade)[1] with sql:variable("@ExamOriginalGrade")')
	WHERE ID = @ID;

	DECLARE Sections CURSOR FOR
		SELECT DISTINCT SectionID, SectionTotalMark, SectionUserMark, SectionUserPercentage, SectionPassValue FROM #Item WHERE ID = @ID;
	OPEN Sections;
	DECLARE @SectionID TINYINT, @SectionTotalMark INT, @SectionUserMark DECIMAL(6,3), @SectionUserPercentage DECIMAL(6,3), @SectionPassValue TINYINT;
	FETCH NEXT FROM Sections INTO @SectionID, @SectionTotalMark, @SectionUserMark, @SectionUserPercentage, @SectionPassValue;
	
	WHILE @@FETCH_STATUS = 0
	BEGIN
		/* Update exam section */
		/* Update exam section total mark */
		UPDATE #ExamXML
		SET StructureXML.modify('replace value of (/assessmentDetails/assessment/section[@id = sql:variable("@SectionID")]/@totalMark)[1] with sql:variable("@SectionTotalMark")')
		   ,ResultData.modify('replace value of (/exam/section[@id = sql:variable("@SectionID")]/@totalMark)[1] with sql:variable("@SectionTotalMark")')
		   ,ResultDataFull.modify('replace value of (/assessmentDetails/assessment/section[@id = sql:variable("@SectionID")]/@totalMark)[1] with sql:variable("@SectionTotalMark")')
		WHERE ID = @ID;

		/* Update exam section user mark */
		UPDATE #ExamXML
		SET StructureXML.modify('replace value of (/assessmentDetails/assessment/section[@id = sql:variable("@SectionID")]/@userMark)[1] with sql:variable("@SectionUserMark")')
		   ,ResultData.modify('replace value of (/exam/section[@id = sql:variable("@SectionID")]/@userMark)[1] with sql:variable("@SectionUserMark")')
		   ,ResultDataFull.modify('replace value of (/assessmentDetails/assessment/section[@id = sql:variable("@SectionID")]/@userMark)[1] with sql:variable("@SectionUserMark")')
		WHERE ID = @ID;

		/* Update exam section user percentage */
		UPDATE #ExamXML
		SET StructureXML.modify('replace value of (/assessmentDetails/assessment/section[@id = sql:variable("@SectionID")]/@userPercentage)[1] with sql:variable("@SectionUserPercentage")')
		   ,ResultData.modify('replace value of (/exam/section[@id = sql:variable("@SectionID")]/@userPercentage)[1] with sql:variable("@SectionUserPercentage")')
		   ,ResultDataFull.modify('replace value of (/assessmentDetails/assessment/section[@id = sql:variable("@SectionID")]/@userPercentage)[1] with sql:variable("@SectionUserPercentage")')
		WHERE ID = @ID;

		/* Update exam section pass value */
		UPDATE #ExamXML
		SET StructureXML.modify('replace value of (/assessmentDetails/assessment/section[@id = sql:variable("@SectionID")]/@passValue)[1] with sql:variable("@SectionPassValue")')
		   ,ResultData.modify('replace value of (/exam/section[@id = sql:variable("@SectionID")]/@passValue)[1] with sql:variable("@SectionPassValue")')
		   ,ResultDataFull.modify('replace value of (/assessmentDetails/assessment/section[@id = sql:variable("@SectionID")]/@passValue)[1] with sql:variable("@SectionPassValue")')
		WHERE ID = @ID;

		DECLARE Items CURSOR FOR
			SELECT DISTINCT ItemID, ItemUserMark, ItemActualUserMark, ItemUserAttempted FROM #Item WHERE ID = @ID AND SectionID = @SectionID;
		OPEN Items;
		DECLARE @ItemID NVARCHAR(100), @ItemUserMark DECIMAL(6,3), @ItemActualUserMark DECIMAL(6,3), @ItemUserAttempted TINYINT;
		FETCH NEXT FROM Items INTO @ItemID, @ItemUserMark, @ItemActualUserMark, @ItemUserAttempted;

		WHILE @@FETCH_STATUS = 0
		BEGIN
			/* Update exam section item */
			/* Update exam section item user mark */
			UPDATE #ExamXML
			SET StructureXML.modify('replace value of (/assessmentDetails/assessment/section[@id = sql:variable("@SectionID")]/item[@id = sql:variable("@ItemID")]/@userMark)[1] with sql:variable("@ItemUserMark")')
			   ,ResultData.modify('replace value of (/exam/section[@id = sql:variable("@SectionID")]/item[@id = sql:variable("@ItemID")]/@userMark)[1] with sql:variable("@ItemUserMark")')
			   ,ResultDataFull.modify('replace value of (/assessmentDetails/assessment/section[@id = sql:variable("@SectionID")]/item[@id = sql:variable("@ItemID")]/@userMark)[1] with sql:variable("@ItemUserMark")')
			WHERE ID = @ID;

			/* Update exam section item actual user mark */
			UPDATE #ExamXML
			SET ResultData.modify('replace value of (/exam/section[@id = sql:variable("@SectionID")]/item[@id = sql:variable("@ItemID")]/@actualUserMark)[1] with sql:variable("@ItemActualUserMark")')
			WHERE ID = @ID;

			/* Directly update the marker response */
			UPDATE #WAREHOUSE_ExamSessionItemResponseTable
			SET MarkerResponseData.modify('replace value of (/entries/entry[userId = -1]/assignedMark/text())[1] with sql:variable("@ItemActualUserMark")')
			WHERE WAREHOUSEExamSessionID = @ID
				AND ItemID = @ItemID;

			/* Update exam section item user attempted */
			UPDATE #ExamXML
			SET StructureXML.modify('replace value of (/assessmentDetails/assessment/section[@id = sql:variable("@SectionID")]/item[@id = sql:variable("@ItemID")]/@userAttempted)[1] with sql:variable("@ItemUserAttempted")')
			   ,ResultData.modify('replace value of (/exam/section[@id = sql:variable("@SectionID")]/item[@id = sql:variable("@ItemID")]/@userAttempted)[1] with sql:variable("@ItemUserAttempted")')
			   ,ResultDataFull.modify('replace value of (/assessmentDetails/assessment/section[@id = sql:variable("@SectionID")]/item[@id = sql:variable("@ItemID")]/@userAttempted)[1] with sql:variable("@ItemUserAttempted")')
			WHERE ID = @ID;

			FETCH NEXT FROM Items INTO @ItemID, @ItemUserMark, @ItemActualUserMark, @ItemUserAttempted;
		END

		CLOSE Items;
		DEALLOCATE Items;

		FETCH NEXT FROM Sections INTO @SectionID, @SectionTotalMark, @SectionUserMark, @SectionUserPercentage, @SectionPassValue;
	END

	CLOSE Sections;
	DEALLOCATE Sections;

	FETCH NEXT FROM Exams INTO @ID, @ExamTotalMark, @ExamUserMark, @ExamUserPercentage, @ExamPassValue, @ExamOriginalPassValue, @ExamGrade, @ExamOriginalGrade;
END

CLOSE Exams;
DEALLOCATE Exams;
			
SELECT * FROM #ExamXML;
SELECT * FROM #Item;

/*

/* Update database tables */
UPDATE WAREHOUSE_ExamSessionTable
SET StructureXML = #ExamXML.StructureXML, ResultData = #ExamXML.ResultData, ResultDataFull = #ExamXML.ResultDataFull
FROM WAREHOUSE_ExamSessionTable
INNER JOIN #ExamXML ON WAREHOUSE_ExamSessionTable.ID = #ExamXML.ID;

UPDATE WAREHOUSE_ExamSessionTable_Shreded
SET ResultData = #ExamXML.ResultData
FROM WAREHOUSE_ExamSessionTable_Shreded
INNER JOIN #ExamXML ON WAREHOUSE_ExamSessionTable_Shreded.ExamSessionID = #ExamXML.ID;

/* Delete shredded & shredded items */
DELETE FROM dbo.WAREHOUSE_ExamSessionTable_ShrededItems WHERE ExamSessionID IN (SELECT ID FROM #ExamXML);
DELETE FROM dbo.WAREHOUSE_ExamSessionTable_Shreded WHERE ExamSessionID IN (SELECT ID FROM #ExamXML);

/* Re-Shred */
INSERT INTO WAREHOUSE_ExamSessionTable_Shreded (
	examSessionId
	,structureXml
	,examVersionName
	,examVersionRef
	,examVersionId
	,examName
	,examRef
	,qualificationid
	,qualificationName
	,qualificationRef
	,resultData
	,submittedDate
	,originatorId
	,centreName
	,centreCode
	,foreName
	,dateOfBirth
	,gender
	,candidateRef
	,surName
	,scheduledDurationValue
	,previousExamState
	,examStateInformation
	,examResult
	,passValue
	,closeValue
	,externalReference
	,examType
	,warehouseTime
	,CQN
	,actualDuration
	,appeal
	,reMarkStatus
	,qualificationLevel
	,centreId
	,uln
	,AllowPackageDelivery
	,ExportToSecureMarker
	,ContainsBTLOffice
	,ExportedToIntegration
	,WarehouseExamState
	,[language]
	,KeyCode
	,TargetedForVoid
	,EnableOverrideMarking
	,AddressLine1
	,AddressLine2
	,Town
	,County
	,Country
	,Postcode
	,ScoreBoundaryData
	,itemMarksUploaded
	,passMark
	,totalMark
	,passType
	,voidJustificationLookupTableId
	,automaticVerification
	,resultSampled
	,[started]
	,submitted
	,validFromDate
	,expiryDate
	,totalTimeSpent
	,defaultDuration
	,scheduledDurationReason
	,WAREHOUSEScheduledExamID
	,WAREHOUSEUserID
	,itemDataFull
	,examId
	,userId
	,TakenThroughLocalScan
	,LocalScanDownloadDate
	,LocalScanUploadDate
	,LocalScanNumPages
	)
SELECT examSessionId
	,structureXml
	,examVersionName
	,examVersionRef
	,examVersionId
	,examName
	,examRef
	,qualificationid
	,qualificationName
	,qualificationRef
	,resultData
	,submittedDate
	,originatorId
	,centreName
	,centreCode
	,foreName
	,dateOfBirth
	,gender
	,candidateRef
	,surName
	,scheduledDurationValue
	,previousExamState
	,examStateInformation
	,examResult
	,passValue
	,closeValue
	,externalReference
	,examType
	,warehouseTime
	,CQN
	,actualDuration
	,appeal
	,reMarkStatus
	,qualificationLevel
	,centreId
	,uln
	,AllowPackageDelivery
	,ExportToSecureMarker
	,ContainsBTLOffice
	,ExportedToIntegration
	,WarehouseExamState
	,[language]
	,KeyCode
	,TargetedForVoid
	,EnableOverrideMarking
	,AddressLine1
	,AddressLine2
	,Town
	,County
	,Country
	,Postcode
	,ScoreBoundaryData
	,itemMarksUploaded
	,passMark
	,totalMark
	,passType
	,voidJustificationLookupTableId
	,automaticVerification
	,resultSampled
	,[started]
	,submitted
	,validFromDate
	,expiryDate
	,totalTimeSpent
	,defaultDuration
	,scheduledDurationReason
	,WAREHOUSEScheduledExamID
	,WAREHOUSEUserID
	,itemDataFull
	,examId
	,userId
	,TakenThroughLocalScan
	,LocalScanDownloadDate
	,LocalScanUploadDate
	,LocalScanNumPages
FROM sa_CandidateExamAudit_View
WHERE examSessionId IN (
		SELECT ID
		FROM #ExamXML
		);

/* Re-Shred items */
INSERT INTO WAREHOUSE_ExamSessionTable_ShrededItems (
	ExamSessionID
	,ItemRef
	,ItemName
	,UserMark
	,MarkerUserMark
	,UserAttempted
	,ItemVersion
	,MarkingIgnored
	,MarkedMetadataCount
	,ExamPercentage
	,TotalMark
	,ResponseXML
	,OptionsChosen
	,SelectedCount
	,CorrectAnswerCount
	,itemType
	,FriendItems
	)
SELECT WAREHOUSE_ExamSessionTable.ID AS [examSessionId]
	,Result.Item.value('@id', 'nvarchar(15)') AS [itemRef]
	,Result.Item.value('@name', 'nvarchar(200)') AS [itemName]
	,Result.Item.value('@userMark', 'decimal(6, 3)') AS [userMark]
	,Result.Item.value('@markerUserMark', 'nvarchar(max)') AS [markerUserMark]
	,Result.Item.value('@userAttempted', 'bit') AS [userAttempted]
	,Result.Item.value('@version', 'int') AS [itemVersion]
	,Result.Item.value('@markingIgnored', 'tinyint') AS [markingIgnored]
	,Result.Item.value('count(mark)', 'int') AS [markedMetadataCount]
	,WAREHOUSE_ExamSessionTable.ResultData.value('(exam/@userPercentage)[1]', 'decimal(6, 3)') AS [examPercentage]
	,Result.Item.value('@totalMark', 'decimal(6, 3)') AS [totalMark]
	,#WAREHOUSE_ExamSessionItemResponseTable.ItemResponseData AS [responseXml]
	,CAST(ISNULL(#WAREHOUSE_ExamSessionItemResponseTable.ItemResponseData.query('data(p/s/c[@typ = "10"]/i[@sl = "1"]/@ac)'), ' UA ') AS NVARCHAR(200)) AS [optionsChosen]
	,#WAREHOUSE_ExamSessionItemResponseTable.ItemResponseData.value('count(p/s/c/i[@sl = "1"])', 'int') AS [selectedCount]
	,#WAREHOUSE_ExamSessionItemResponseTable.ItemResponseData.value('count(p/s/c/i[@ca = "1"])', 'int') AS [correctAnswerCount]
	,Result.Item.value('@type', 'int') AS ItemType
	,Result.Item.value('@SurpassFriendItems', 'NVARCHAR(MAX)') AS FriendItems
FROM WAREHOUSE_ExamSessionTable
CROSS APPLY WAREHOUSE_ExamSessionTable.ResultData.nodes('exam/section/item') Result(Item)
LEFT JOIN #WAREHOUSE_ExamSessionItemResponseTable ON #WAREHOUSE_ExamSessionItemResponseTable.WAREHOUSEExamSessionID = WAREHOUSE_ExamSessionTable.ID
	AND #WAREHOUSE_ExamSessionItemResponseTable.ItemID = Result.Item.value('@id', 'nvarchar(15)')
WHERE WAREHOUSE_ExamSessionTable.ID IN (
		SELECT ID
		FROM #ExamXML
		);
*/
/* Cleanup */
DROP TABLE #Item;
DROP TABLE #ExamXML;












