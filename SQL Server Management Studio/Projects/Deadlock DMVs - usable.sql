use master;


exec sp_MSforeachdb '

Declare @@DBswithwaits table (
	DBID tinyint
)

INSERT @@DBswithwaits
select database_id
from sys.dm_exec_requests 
where status not in (''background'',''sleeping'',''running'')

use [?];

if (db_ID() in (select DBID from @@DBswithwaits))
BEGIN

	SELECT A.[Database]
		--, t.name [Table Name]
		, session_id
		, text [Query]
		, Status	
		, command
		, wait_type
		, last_wait_type
		, wait_time
		, total_elapsed_time
		, reads
		, writes
		,wait_resource
	FROM (
		select DB_NAME(database_id) [Database] 
		, session_id
		, status
		, command
		, wait_type
		, last_wait_type
		, wait_time
		, total_elapsed_time
		, reads
		, writes
		, text
		,wait_resource
		--, SUBSTRING(wait_resource, LEN(wait_resource)- CHARINDEX('':'',REVERSE(wait_resource))+2, LEN(wait_resource)- CHARINDEX(''('',wait_resource)+LEN(wait_resource)- CHARINDEX('':'',REVERSE(wait_resource))-3) [WaitResource]
		from sys.dm_exec_requests 
		CROSS APPLY sys.dm_exec_sql_text(sql_handle) st
		where status not in (''background'',''sleeping'',''running'')
	) A
	--INNER JOIN sys.partitions P
	--on P.partition_id = A.[WaitResource]
	--INNER JOIN sys.tables T
	--on T.object_id = p.object_id;

END
'