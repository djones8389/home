SET NOCOUNT ON

/*
ALTER PROCEDURE [dbo].[sa_SECUREASSESSINTEGRATION_AddOrUpdateCentreWithQualifications_sp]
	@centreRef			nvarchar(50),
	@centreName			nvarchar(100),
	@addressLine1		nvarchar(100),
	@addressLine2		nvarchar(100),
	@town               nvarchar(100),
	@postCode           nvarchar(8),
	@countyID			int,
	@countryID			int,
	@primaryContactID	int,
	@technicalContactID	int,
	@offlineEnabled		bit,
	@cohortDescription	nvarchar(max),
	@qualList			nvarchar(max)    
*/

USE STG_WJEC_SecureAssess

IF OBJECT_ID('tempdb..#centres') IS NOT NULL DROP TABLE #centres;

create table #centres
(
	centreName			nvarchar(100),
	centreRef			nvarchar(50),
	Added char(8)
)

bulk insert #centres
from 'C:\WJEC Centres_Need adding to SA.csv'
with (fieldterminator = ',', rowterminator = '\n')

update #centres
set centreName = REPLACE(Centrename, '*',',');


declare @total smallint = 100; --(select count(*) from #centres where Added = 0);
declare @increment int = 10
declare @counter int = 0;
declare @dynamic nvarchar(max)='';


--while (@counter < @total)

--BEGIN

DECLARE Centres Cursor FOR
select TOP (@increment)
	centreName
	, centreRef
	, Added
from #centres
where Added = 0

DECLARE @CentreName nvarchar(100), @centreRef nvarchar(50),  @Added char(8)

OPEN Centres

FETCH NEXT FROM Centres INTO @CentreName, @centreRef, @Added

WHILE @@FETCH_STATUS = 0

BEGIN

--run sp
--set flag
SELECT @dynamic += CHAR(13) +
'exec [dbo].[sa_SECUREASSESSINTEGRATION_AddOrUpdateCentreWithQualifications_sp] @centreRef = ' + '''' + convert(nvarchar(10),@centreRef) +'''' + ', @centreName =' + '''' 
	+ REPLACE(LTRIM(RTRIM(@centreName)), '''','''''')+''''+ ',  @addressLine1 = ''Default Address'' , @addressLine2 = NULL, @town = ''Cardiff'', @postCode = NULL, @countyID = ''91'', @countryID = ''3''
	, @primaryContactID = ''1'', @technicalContactID = ''1'', @offlineEnabled = ''0'', @cohortDescription = NULL, @qualList = ''64''
	 '
UPDATE #centres 
set Added = 1 
where centreRef = @centreRef

PRINT(@dynamic)

--select @counter += @increment

FETCH NEXT FROM Centres INTO @CentreName, @centreRef, @Added

END
CLOSE Centres
DEALLOCATE Centres


--END









/*
SELECT top (@increment)
'exec [dbo].[sa_SECUREASSESSINTEGRATION_AddOrUpdateCentreWithQualifications_sp] @centreRef = ' + '''' + convert(nvarchar(10),centreRef) +'''' + ', @centreName =' + '''' 
	+ REPLACE(LTRIM(RTRIM(#centres.centreName)), '''','''''')+''''+ ',  @addressLine1 = ''Default Address'' , @addressLine2 = NULL, @town = ''Cardiff'', @postCode = NULL, @countyID = ''91'', @countryID = ''3''
	, @primaryContactID = ''1'', @technicalContactID = ''1'', @offlineEnabled = ''0'', @cohortDescription = NULL, @qualList = ''64''
	 '
FROM #centres 
LEFT JOIN CentreTable 
on #centres.centreName = CentreTable.CentreName	
	or #centres.centreRef = CentreTable.CentreCode
where CentreTable.id IS NULL
	and Added = 0;

UPDATE top (@increment) #centres 
set Added = 1
where Added = 0;

	print @counter

	select @counter += @increment   
*/




















--DECLARE @ChangeRecoveryModel NVARCHAR(MAX) = ''

--select @ChangeRecoveryModel += CHAR(13) + 'ALTER DATABASE ' + QUOTENAME (Name) + ' SET RECOVERY SIMPLE;'
--from sys.databases
--where recovery_model_desc = 'FULL'

--EXEC(@ChangeRecoveryModel);




--select
--#centres.centreName
--, centreRef
----,'exec [dbo].[sa_SECUREASSESSINTEGRATION_AddOrUpdateCentreWithQualifications_sp] @centreRef = ' + '''' + convert(nvarchar(10),centreRef) +'''' + ', @centreName =' + '''' 
----	+ REPLACE(LTRIM(RTRIM(#centres.centreName)), '''','''''')+''''+ ',  @addressLine1 = ''Default Address'' , @addressLine2 = NULL, @town = ''Cardiff'', @postCode = NULL, @countyID = ''91'', @countryID = ''3''
----	, @primaryContactID = ''1'', @technicalContactID = ''1'', @offlineEnabled = ''0'', @cohortDescription = NULL, @qualList = ''64''
----	 '
--FROM #centres 
--LEFT JOIN CentreTable 
--on #centres.centreName = CentreTable.CentreName	
--	or #centres.centreRef = CentreTable.CentreCode
--where CentreTable.id IS NULL
--	and Added = 0;

--select top 100 * from #centres