use master

if OBJECT_ID('tempdb..##ReportParams') IS NOT NULL DROP TABLE ##ReportParams;

DECLARE @Licence tinyint= 2
	,@StartDate DATE = '01 Sep 2016'
	, @EndDate DATE = '03 Nov 2016';

CREATE TABLE ##ReportParams (
	[EditionID] tinyint
	,[startDate] date
	,[endDate] date
);
INSERT ##ReportParams([EditionID],[startDate],[endDate])
SELECT @Licence,@StartDate, @EndDate;

DECLARE @Results TABLE (
	ClientName sysname
	, ExamCount int
);

DECLARE myLoop CURSOR FOR
SELECT 
'USE '+A.Name+'  
	BEGIN
		SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
			
		Declare @StartDate DATE = (select [startDate] from ##ReportParams);
		Declare @EndDate DATE = (select [endDate] from ##ReportParams);
		Declare @Licence tinyint = (select [EditionID] from ##ReportParams);
		
		if OBJECT_ID (''tempdb..#Live'') IS NOT NULL DROP TABLE #Live;
		if OBJECT_ID (''tempdb..#Warehouse'') IS NOT NULL DROP TABLE #Warehouse;

		CREATE TABLE #Live (ExamSessionID int, userID int);
			INSERT #Live (ExamSessionID, UserID)
			SELECT ExamSessionTable.ID ExamSessionID
				, ExamSessionTable.UserID
			FROM  (
			select ExamSessionID
			from ExamStateChangeAuditTable
			where ExamStateChangeAuditTable.NewState = 9
				and StateChangeDate between @StartDate and @EndDate 
				group by ExamSessionID
			) State9
			INNER JOIN ExamSessionTable
			on ExamSessionTable.ID = State9.ExamSessionID
			where exists (	
			select 1
			from ExamStateChangeAuditTable
			where ExamStateChangeAuditTable.ExamSessionID = State9.ExamSessionID
			and ExamStateChangeAuditTable.NewState = 6
			);

		CREATE TABLE #Warehouse  (ExamSessionID int, userID int);
			INSERT #Warehouse (ExamSessionID, UserID)
			SELECT WAREHOUSE_ExamSessionTable.ExamSessionID
				, WAREHOUSE_ExamSessionTable_Shreded.userId
			from WAREHOUSE_ExamSessionTable_Shreded
			inner join WAREHOUSE_ExamSessionTable
			on WAREHOUSE_ExamSessionTable.ID = WAREHOUSE_ExamSessionTable_Shreded.examSessionId
			where [started] > @StartDate	
				and submittedDate < @EndDate;

		
		DECLARE @SQL1 NVARCHAR(MAX) = ''''
		SELECT @SQL1 = ''
		;WITH Academic AS (
			select userId from #Live
			UNION
			select userId from #Warehouse
		)
			select db_name() [Client]
				, count(userId) [ExamCount]
			from Academic
		''
		DECLARE @SQL2 NVARCHAR(MAX) = ''''
		SELECT @SQL2 = ''
			;With Professional as (
			select ExamSessionID from #Live
			UNION
			select ExamSessionID from #Warehouse
			)

			select db_name() [Client]
				, count(ExamSessionID) [ExamCount]
			from Professional
		''
		if(@Licence=1)
			exec(@SQL1)
		if(@Licence=2)
			exec(@SQL2)		
		
		DROP TABLE #Live; DROP TABLE #Warehouse;	
	END
'
FROM CMS..[Clients] 
INNER JOIN CMS..[Editions]
ON [Clients].EditionID = [Editions].ID
INNER JOIN sys.databases A
on a.name = [Clients].Name + '_SecureAssess' 
where [Editions].ID =  (SELECT [EditionID] FROM ##ReportParams)

DECLARE @SQL NVARCHAR(MAX);

OPEN myLoop

FETCH NEXT FROM myLoop INTO @SQL

WHILE @@FETCH_STATUS = 0

BEGIN

	INSERT @Results
	EXEC(@SQL)

FETCH NEXT FROM myLoop INTO @SQL

END
CLOSE myLoop
DEALLOCATE myLoop



SELECT ClientName
	, ExamCount
FROM @Results
order by ClientName;