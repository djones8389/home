DROP TABLE IF EXISTS #PageTable;
DROP TABLE IF EXISTS #MCQ;
DROP TABLE IF EXISTS #Custom;

CREATE TABLE #PageTable (
	id NVARCHAR(10)
);
INSERT #PageTable 
VALUES('123p456'),('456p123'),('509p509');

CREATE TABLE #MCQ (
	id NVARCHAR(10)
	, [MCQ_No] INT
);
INSERT #MCQ
VALUES('123p456',3),('456p123',10),('509p509',34);

CREATE TABLE #Custom (
	id NVARCHAR(10)
	, [Custom_No] int
);
INSERT #Custom
VALUES('123p456',3),('456p123',10),('509p509',34);




SELECT * 
FROM 
(
	SELECT id	
		, MCQ_No
		, 1 Num
	FROM #MCQ
) AS S
PIVOT (
	
	SUM(Num)
	FOR id IN ([1], [123p456])

) AS P




SELECT * 
FROM #PageTable
SELECT * FROM #MCQ
SELECT * FROM #Custom


INSERT #Custom
VALUES('681p342',17),('456p123',10),('509p509',34);


SELECT p.id
	, a.MCQ_No
	, b.Custom_No

FROM #PageTable p
LEFT JOIN #MCQ a
on a.id = p.id
LEFT JOIN #Custom b
ON b.id = p.id
