/****** Script for SelectTopNRows command from SSMS  ******/
SELECT distinct
      [computerName]
      ,[identification].value('data(identification/macAddress)[1]','nvarchar(MAX)') macAddress
	  ,[identification].value('data(identification/ipAddress)[1]','nvarchar(MAX)') ipAddress      
FROM [Bpec_SurpassDataWarehouse].[dbo].[FactClientInformation]
/*
<identification>
  <computerName>S81303009</computerName>
  <macAddress>38:60:77:B5:2B:59</macAddress>
  <ipAddress>172.25.2.169</ipAddress>
  <ipAddress>fe80::c829:a76b:c1a3:dfd3</ipAddress>
</identification>

*/