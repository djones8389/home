select id, resultDataFull, examstatechangeauditxml, warehouseTime, WarehouseExamState
from warehouse_ExamSessionTable
INNER JOIN (
	SELECT A.*
	FROM (
	select WarehouseExamSessionID
			,	 min(DATE) [Date]
	from WAREHOUSE_ExamStateAuditTable
	WHERE ExamState = 1
		--and WarehouseExamSessionID = 2698791
	group by WarehouseExamSessionID
	) A
	where [Date] > DATEADD(DAY,-1,GETDATE())
) Auditing
on Auditing.WarehouseExamSessionID = warehouse_ExamSessionTable.ID
where keycode = '64JB99A6'



/*



select ExamSessionID, warehouseTime, est.ID [est], ExamStateChangeAuditXml
from WAREHOUSE_ExamSessionTable
left join ExamSessionTable est on est.id = ExamSessionID
where DATEADD(HOUR, -25, getdate()) < warehouseTime


order by warehouseTime desc

select @@ServerName [Instance]
	  ,DB_NAME(database_id) AS DBName
      ,OBJECT_SCHEMA_NAME(object_id,database_id) AS [SCHEMA_NAME] 
      ,OBJECT_NAME(object_id,database_id)AS [OBJECT_NAME]
      ,cached_time
      ,last_execution_time
	  , execution_count
from sys.dm_exec_procedure_stats 
where OBJECT_NAME(object_id,database_id) = 'sa_HOUSEKEEPING_RemoveWarehousedExamsFromLiveTables_sp'

select id
from ExamSessionTable NOLOCK
WHERE ID IN (2752581,2752740,2749979,2751582,2733504,2745913,2751497,2737483,2733496,2747440,2747439,2752584,2750072,2744737,2748447,2749699,2750959,2752601,2752428,2736263,2752567,2750380,2750499,2752767,2752036,2743248,2747153,2752000,2752158,2751702,2750495,2745638,2743771,2735731,2746061,2747020,2749793,2750289,2749859,2732000,2745567,2750559,2752652,2751681,2734149,2750867,2749460,2750247,2736726,2752008,2751575,2750250,2748797,2736977,2742557,2742568,2741008,2741019,2750414,2749597,2748847,2749328,2748669,2752557,2737971,2733984,2747130,2751954,2736598,2750889,2750209,2751945,2740894,2748648,2749666,2736753,2737551,2752566,2751949,2736246,2747144,2574702,2752755)

--2017-09-26 10:13:58.513
--2017-09-26 11:14:00.037


*/


 SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	select 
		weisrt.ID
		, weisrt.ItemID
		, weisrt.ItemVersion
		, KeyCode
		, ItemResponseData
	from WAREHOUSE_ExamSessionItemResponseTable weisrt
	inner join (
			select cp.ItemID, cp.ItemVersion, replace(replace(convert(nvarchar(max), cp.ItemXML), CHAR(10), ''), CHAR(13), '') [ItemXML]
			from (	select ItemID, ItemVersion
					from CPAuditTable
					where ItemXML.exist('P/S/C/I/extension/t/r/c[@dt]') = 1
			) i
			inner join CPAuditTable cp
			on cp.ItemID = i.itemId 
				and cp.ItemVersion = i.itemVersion
	) CPAUdit
	on CPAUdit.ItemID = weisrt.ItemID
		and CPAUdit.ItemVersion = weisrt.ItemVersion
	INNER JOIN WAREHOUSE_ExamSessionTable west
	on weisrt.WAREHOUSEExamSessionID = west.ID

	INNER JOIN (
		SELECT A.*
		FROM (
		select WarehouseExamSessionID
				,	 min(DATE) [Date]
		from WAREHOUSE_ExamStateAuditTable
		WHERE ExamState = 1
		group by WarehouseExamSessionID
		) A
		where [Date] > DATEADD(DAY,-1,GETDATE())
	) Auditing
	on Auditing.WarehouseExamSessionID = west.ID


--	where warehousetime > DATEADD(DAY,-1,GETDATE());




