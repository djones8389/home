USE [Demo_SDWH_Perf2]
GO

/****** Object:  Index [09_idx_fqr_cpid]    Script Date: 02/19/2016 08:49:42 ******/
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[FactQuestionResponses]') AND name = N'09_idx_fqr_cpid')
DROP INDEX [09_idx_fqr_cpid] ON [dbo].[FactQuestionResponses] WITH ( ONLINE = OFF )
GO

USE [Demo_SDWH_Perf2]
GO

/****** Object:  Index [09_idx_fqr_cpid]    Script Date: 02/19/2016 08:49:42 ******/
CREATE NONCLUSTERED INDEX [09_idx_fqr_cpid] ON [dbo].[FactQuestionResponses] 
(
	[CPID] ASC
)
INCLUDE ( [ExamSessionKey]) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO


