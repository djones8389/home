DBCC FREEPROCCACHE;
DBCC DROPCLEANBUFFERS;

DECLARE @centres VARCHAR(MAX) = '30,32,33,34,36,43,45,48,49,56,57,58,59,60,61,62,63,65,68,72,73,77,78,80,82,83,84,86,87,88,93,94,95,96,97,99,101,103,105,107,108,111,112,114,117,120,121,126,129,130,131,134,135,137,141,143,144,146,148,151,153,154,156,158,160,161,163,166,167,170,173,174,175,176,177,180,181,183,187,188,189,194,195,198,199,201,203,205,208,209,212,213,215,216,219,222,223,224,226,227,230,231,235,237,238,239,241,242,244,246,250,251,254,255,259,264,267,272,274,275,277,279,281,283,286,288,289,290,292,293,294,296,297,299,301,302,303,305,309,310,311,312,317,318,322,323,324,328,329,330,331,332,334,337,339,343,345,348,349,352,353,358,359,361,363,364,367,368,370,374,375,376,377,378,379,380,382,383,386,387,389,392,393,394,396,398,399,400,401,402,403,405,407,409,410,412,413,415,417,419,421'
--DECLARE @centres VARCHAR(MAX) = '30,32,33,34,36'
DECLARE @subjects VARCHAR(MAX) = '103,104,110,112,116,121,129,130,133,135,136,137,140,142,144,145,146,148,150,156,161,162,168,173,187,191,195,198,204,205,209,210,214,220,223'
DECLARE @startdate DATE = '2000-01-01';
DECLARE @enddate DATE = '2016-12-31';
DECLARE @showretiredsubjects INT = 1;
DECLARE @onlyshowsubjectgroups INT = 0;
DECLARE @subjecttext VARCHAR(50) = '';

DECLARE @StartDateKey int = (SELECT TimeKey FROM DimTime WHERE CAST(FullDateAlternateKey AS DATE)=CAST(@startdate AS DATE))
DECLARE @EndDateKey int =	(SELECT TimeKey FROM DimTime WHERE CAST(FullDateAlternateKey AS DATE)=CAST(@enddate AS DATE))
IF @StartDateKey IS NULL BEGIN
	IF @startdate<=(SELECT MIN(FullDateAlternateKey) FROM DimTime) SELECT @StartDateKey = MIN(TimeKey) FROM DimTime
	ELSE SELECT @StartDateKey = MAX(TimeKey)+1 FROM DimTime
END
IF @EndDateKey IS NULL BEGIN
	IF @enddate<=(SELECT MIN(FullDateAlternateKey) FROM DimTime) SELECT @EndDateKey = MIN(TimeKey)-1 FROM DimTime
	ELSE SELECT @EndDateKey = MAX(TimeKey) FROM DimTime
END

	
DECLARE @Q table (QualificationKey int Primary key clustered)
INSERT INTO @Q
SELECT Value FROM dbo.fn_ParamsToList(@subjects,0)
OPTION(MAXRECURSION 0)

IF @showretiredsubjects = 0
	DELETE FROM Q
	FROM DimQualifications DQ
		JOIN @Q Q ON DQ.QualificationKey = Q.QualificationKey
	WHERE dq.Status<>0

IF @onlyshowsubjectgroups = 1 BEGIN
	;WITH SubjectGroups AS(
		SELECT 
			DISTINCT QualificationKey 
		FROM DimProjectGroups DPG
			JOIN DimProjects DP ON DPG.ParentProjectKey = DP.ProjectKey
	)
	DELETE FROM Q
	FROM @Q Q 
		LEFT JOIN SubjectGroups SG ON Q.QualificationKey = SG.QualificationKey
	WHERE SG.QualificationKey IS NULL
END

DECLARE @C table (CentreKey int Primary key clustered)
INSERT INTO @C
SELECT Value FROM dbo.fn_ParamsToList(@centres,0)
OPTION(MAXRECURSION 0)

IF OBJECT_ID('tempdb..#SubjectsReport_ES') IS NOT NULL
DROP TABLE #SubjectsReport_ES

CREATE TABLE #SubjectsReport_ES(
	ExamSessionKey int PRIMARY KEY CLUSTERED
	,QualificationKey int
	,ExamKey int
	,ExamVersionKey int
	,CompletionDateTime datetime
	)
INSERT INTO #SubjectsReport_ES
SELECT 
	ExamSessionKey
	,FES.QualificationKey
	,ExamKey
	,ExamVersionKey 
	,CAST(39080+CompletionDateKey AS datetime)
FROM 
	FactExamSessions FES
	JOIN @C C ON C.CentreKey = FES.CentreKey
	JOIN @Q Q ON Q.QualificationKey = FES.QualificationKey
WHERE 
	FES.CompletionDateKey BETWEEN @StartDateKey AND @EndDateKey
	AND FES.FinalExamState<>10
OPTION(MAXRECURSION 0)

-- Test counts --
;WITH TestCounts AS
(
	SELECT  
		QualificationKey, 
		COUNT(DISTINCT ExamKey) AS Tests
	FROM #SubjectsReport_ES
	GROUP BY QualificationKey
),
-- Test from counts --
TestFormCounts AS
(
	SELECT  
		QualificationKey, 
		COUNT(DISTINCT ExamVersionKey) AS TestForms
	FROM #SubjectsReport_ES
	GROUP BY QualificationKey
),
--Tests Taken --
TestsTaken AS
(
	SELECT 
		QualificationKey, 
		COUNT(ExamSessionKey) TestsTaken, 
		MAX(CompletionDateTime) LastSat
	FROM #SubjectsReport_ES fes
	GROUP BY fes.QualificationKey
),
---ItemsDelivered--
ItemsDelivered AS
(
	SELECT 
		FES.QualificationKey, 
		COUNT(DISTINCT FQR.CPID) AS ItemsDelivered 
	FROM #SubjectsReport_ES FES
	JOIN FactQuestionResponses fqr ON FES.ExamSessionKey = FQR.ExamSessionKey
	GROUP BY FES.QualificationKey
),

ItemBank AS
(
	SELECT  
		t.QualificationKey, 
		SUM(t.ItemBank) as ItemBank
	FROM    ( 
		SELECT 
			dp.QualificationKey, 
			COUNT(DISTINCT dq.CPID) as ItemBank
		FROM 
			DimQuestions dq
			INNER JOIN 
				DimProjects dp ON dq.ProjectKey = dp.ProjectKey
		WHERE
			Deleted =0
			AND ISNULL(CAQuestionTypeKey,0)<>1
		GROUP BY dp.QualificationKey
		
		UNION ALL
		
		SELECT 
			dp.QualificationKey, 
			COUNT(DISTINCT dq.CPID) as ItemBank
		FROM DimProjects dp
			INNER JOIN
				DimProjectGroups dpg ON dp.ProjectKey = dpg.ParentProjectKey
			INNER JOIN
				DimQuestions dq ON dq.ProjectKey = dpg.ProjectKey
		WHERE
			Deleted =0
			AND ISNULL(CAQuestionTypeKey,0)<>1
		GROUP BY dp.QualificationKey
	) t
	GROUP BY t.QualificationKey
)

--Main--
SELECT (
	SELECT 
		ISNULL (DQ.ExternalID,'') AS '@ID',
		ISNULL (DQ.QualificationName,'') AS '@SubjectName',
		ISNULL (tc.Tests,'') AS '@Tests', 
		ISNULL (tt.TestsTaken,'') AS '@TestsTaken', 
		ISNULL (tt.LastSat,'') AS '@LastSat', 
		ISNULL (id.ItemsDelivered,'') AS '@ItemDelivered', 
		ISNULL (tfc.TestForms,'') AS '@TestForms', 
		ISNULL (ib.ItemBank,'') AS '@ItemBank',
		DQ.[Status] AS '@status'
	FROM 
		@Q q
		JOIN DimQualifications DQ ON DQ.QualificationKey = Q.QualificationKey
		JOIN TestCounts tc ON tc.QualificationKey = q.QualificationKey
		JOIN TestFormCounts tfc ON tfc.QualificationKey = q.QualificationKey
		JOIN TestsTaken tt ON tt.QualificationKey = q.QualificationKey
		JOIN ItemsDelivered id ON id.QualificationKey = q.QualificationKey
		JOIN ItemBank ib ON ib.QualificationKey = q.QualificationKey
	FOR XML PATH('subject')
	,ROOT('subjects')
	,TYPE
)

FOR XML PATH('report'), TYPE

--IF OBJECT_ID('tempdb..#SubjectsReport_ES') IS NOT NULL DROP TABLE #SubjectsReport_ES

