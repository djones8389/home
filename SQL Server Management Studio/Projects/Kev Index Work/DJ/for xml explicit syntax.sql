declare @T table
(
  centrekey varchar(20),
  centrename varchar(20),
  qualkey varchar(10),
  qualname varchar(100),
  esid int,
  Completed datetime
)

insert into @T (centrekey, centrename, qualkey, qualname,esid,Completed)
values('30', 'TestCentre', '103', 'Test Subject',21102,'2015-10-31T02:45:00')
	,('30', 'TestCentre', '103', 'Test Subject',21103,'2015-10-31T02:45:00')
	,('30', 'TestCentre', '103', 'Test Subject',21104,'2015-10-31T02:45:00')
	,('30', 'TestCentre', '103', 'Test Subject',21105,'2015-10-31T02:45:00')
	,('32', '123hsjcisdhi', '110', '6uvTIJIq1xYAbtL02i0F', 36024, '2015-11-01T14:32:00')
	,('34', 'yzs3IVMK', '123', '456',15005,'2015-10-30T18:51:00')

SELECT 
	centrekey AS "@Key"
	,centrename AS "@Name"
	,(
		SELECT qualkey AS "@Key"
			,qualname AS "@Name"
		FOR XML path('Qualification')
			,type
		)
	,(
		SELECT esid AS "@Key"
			,Completed AS "@Completed"
		FOR XML path('Exam')
			,type
		)
FROM @T
--group by centrekey, centrename, qualkey, qualname
FOR XML path('Centre'), Root ('Report')