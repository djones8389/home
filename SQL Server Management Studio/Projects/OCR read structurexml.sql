DECLARE @XML XML = '<result errorCode="0">
  <return>
    <examScript>
      <examInstanceId>215688</examInstanceId>
      <examId>66</examId>
      <examName>Level 2 Functional Skills English - Reading Task</examName>
      <candidateId>59764</candidateId>
      <pinNumber>6V7G3V</pinNumber>
      <assessmentDetails>
        <assessmentID>2199</assessmentID>
        <qualificationID>132</qualificationID>
        <qualificationName>OCR Functional Skills in English Reading Level 2</qualificationName>
        <qualificationReference>FSEnglishReadingL2</qualificationReference>
        <assessmentGroupName>Level 2 Functional Skills English - Reading Task</assessmentGroupName>
        <assessmentGroupID>66</assessmentGroupID>
        <assessmentGroupReference>09499/4</assessmentGroupReference>
        <assessmentName>Level 2 Functional Skills English Reading - ONDEMAND - BR27</assessmentName>
        <validFromDate>18 Apr 2012</validFromDate>
        <expiryDate>14 Aug 2022</expiryDate>
        <startTime>00:00:00</startTime>
        <endTime>23:59:59</endTime>
        <duration>55</duration>
        <defaultDuration>55</defaultDuration>
        <scheduledDuration>
          <value>55</value>
          <reason />
        </scheduledDuration>
        <sRBonus>0</sRBonus>
        <sRBonusMaximum>14</sRBonusMaximum>
        <externalReference>FSUM_ONDEMAND_0000_m_09499_4_BR27</externalReference>
        <passLevelValue>12</passLevelValue>
        <passLevelType>0</passLevelType>
        <status>2</status>
        <testFeedbackType>
          <passFail>0</passFail>
          <percentageMark>0</percentageMark>
          <allowSummaryFeedback>0</allowSummaryFeedback>
          <summaryFeedbackType>1</summaryFeedbackType>
          <itemSummary>0</itemSummary>
          <itemReview>0</itemReview>
          <itemReviewCorrectAnswer>0</itemReviewCorrectAnswer>
          <itemFeedback>0</itemFeedback>
          <printableSummary>0</printableSummary>
          <candidateDetails>0</candidateDetails>
          <feedbackByReference>0</feedbackByReference>
          <allowFeedbackDuringExam>0</allowFeedbackDuringExam>
          <allowFeedbackDuringSummary>0</allowFeedbackDuringSummary>
        </testFeedbackType>
        <itemFeedback>0</itemFeedback>
        <allowCalculator>0</allowCalculator>
        <lastInUseDate>21 Feb 2014 09:24:48</lastInUseDate>
        <completedScriptReview>0</completedScriptReview>
        <assessment currentSection="1" totalTime="0" currentTime="0">
          <intro id="0" name="Introduction" currentItem="">
            <item id="6002P6863" name="BR27_FS" totalMark="1" version="50" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
          </intro>
          <outro id="0" name="Finish" currentItem="" />
          <tools id="0" name="Tools" currentItem="">
            <item id="6002P6872" name="BR27_Doc1" toolName="BR27_Doc1" totalMark="1" version="10" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
            <item id="6002P6873" name="BR27_Doc2" toolName="BR27_Doc2" totalMark="1" version="10" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
            <item id="6002P6874" name="BR27_Doc3" toolName="BR27_Doc3" totalMark="1" version="6" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
          </tools>
          <section id="1" name="Section 1" passLevelValue="0" passLevelType="1" itemsToMark="0" currentItem="6050P6783" fixed="1">
            <item id="6050P6776" name="BR27_Info1" totalMark="0" version="70" markingType="0" markingState="0" userMark="-1" markerUserMark="" userAttempted="0" viewingTime="28281" flagged="0" LO="Information page only (no marks)" unit="2" dirty="2">
              <userData>
                <p um="-1" cs="1" ua="0" id="6050P6776">
                  <s ua="0" um="-1" id="1">
                    <c typ="4" id="43">
                      <i cc="0" id="1" />
                    </c>
                    <c typ="4" id="44">
                      <i cc="0" id="1" />
                    </c>
                    <c typ="4" id="45">
                      <i cc="0" id="1" />
                    </c>
                  </s>
                </p>
              </userData>
            </item>
            <item id="6050P6777" name="BR27_01" totalMark="3" version="71" markingType="1" markingState="0" userMark="0" markerUserMark="" userAttempted="1" viewingTime="690892" flagged="0" unit="2" quT="11" MarkedMetadata_4="3" dirty="24">
              <userData>
                <p um="0" cs="1" ua="1" id="6050P6777">
                  <s ua="1" um="0" id="1">
                    <c typ="4" id="28">
                      <i cc="3" id="1" />
                    </c>
                    <c typ="4" id="29">
                      <i cc="4" id="1" />
                    </c>
                    <c wei="3" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="30">
                      <i ca="" id="1">Document 1- This document is informing the reader about the fesitval, for example how much the tickets are, transport, accomodation etc.#!##!#Document 2- This is a formal text which the writer has a biased opinion which is advising the reader of the negatives of going to festivals.#!##!#Document 3- This is an informal text with different opinions regarding different peoples opinions of going to a festival.</i>
                    </c>
                    <c typ="4" id="33">
                      <i cc="2" id="1" />
                    </c>
                  </s>
                </p>
              </userData>
            </item>
            <item id="6050P6778" name="BR27_02" totalMark="3" version="74" markingType="1" markingState="0" userMark="0" markerUserMark="" userAttempted="1" viewingTime="543158" flagged="0" unit="2" quT="11" MarkedMetadata_4="3" dirty="21">
              <userData>
                <p um="0" cs="1" ua="1" id="6050P6778">
                  <s ua="1" um="0" id="1">
                    <c typ="4" id="28">
                      <i cc="5" id="1" />
                    </c>
                    <c typ="4" id="29">
                      <i cc="2" id="1" />
                    </c>
                    <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="30">
                      <i ca="" id="1">Palaver means something is a lot of hard work.</i>
                    </c>
                    <c typ="4" id="33">
                      <i cc="5" id="1" />
                    </c>
                    <c wei="2" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="35">
                      <i ca="" id="1">Document 3- "The ticket system is so complicated"#!#"Its unbeliveable that people don''t catch all sorts of nasty diseases." - Document 2.</i>
                    </c>
                  </s>
                </p>
              </userData>
            </item>
            <item id="6050P6779" name="BR27_03" totalMark="3" version="74" markingType="1" markingState="0" userMark="0" markerUserMark="" userAttempted="1" viewingTime="211297" flagged="0" unit="2" quT="11" MarkedMetadata_5="3" dirty="8">
              <userData>
                <p um="0" cs="1" ua="1" id="6050P6779">
                  <s ua="1" um="0" id="1">
                    <c typ="4" id="28">
                      <i cc="0" id="1" />
                    </c>
                    <c typ="4" id="29">
                      <i cc="4" id="1" />
                    </c>
                    <c wei="3" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="30">
                      <i ca="" id="1">Toilet and shower facilities #!#Free access to the solar showers#!#If you are coming from 20 miles they will pick up your luggage</i>
                    </c>
                    <c typ="4" id="33">
                      <i cc="0" id="1" />
                    </c>
                  </s>
                </p>
              </userData>
            </item>
            <item id="6050P6780" name="BR27_04" totalMark="4" version="72" markingType="1" markingState="0" userMark="0" markerUserMark="" userAttempted="1" viewingTime="540189" flagged="0" unit="2" quT="11" MarkedMetadata_6="4" dirty="19">
              <userData>
                <p um="0" cs="1" ua="1" id="6050P6780">
                  <s ua="1" um="0" id="1">
                    <c typ="4" id="28">
                      <i cc="3" id="1" />
                    </c>
                    <c typ="4" id="29">
                      <i cc="2" id="1" />
                    </c>
                    <c wei="2" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="30">
                      <i ca="" id="1">To get their point to the reader the writer has sectioned the text into paragraphs to help you understand the information.#!#The writer has used bold fonts to help the reader understand key information.#!#</i>
                    </c>
                    <c typ="4" id="33">
                      <i cc="0" id="1" />
                    </c>
                    <c wei="2" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="35">
                      <i ca="" id="1">The writer is using narrative language to get his point accross, by telling stories about his experiences.#!##!#The writer uses a lot of questioning techniques to get the reader to think about what he is saying and influence them into his way of thinking.#!#</i>
                    </c>
                  </s>
                </p>
              </userData>
            </item>
            <item id="6050P6781" name="BR27_05" totalMark="4" version="71" markingType="1" markingState="0" userMark="0" markerUserMark="" userAttempted="1" viewingTime="513126" flagged="0" unit="2" quT="11" MarkedMetadata_5="4" dirty="19">
              <userData>
                <p um="0" cs="1" ua="1" id="6050P6781">
                  <s ua="1" um="0" id="1">
                    <c typ="4" id="28">
                      <i cc="3" id="1" />
                    </c>
                    <c typ="4" id="29">
                      <i cc="0" id="1" />
                    </c>
                    <c wei="4" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="30">
                      <i ca="" id="1">Jays is against the festival as he is saying what a rip off the ticket is whereas Mark is saying how it is a great value for money. Mark has said that nearly 80,000 people have bought into it so it can''t be that bad. Jay is saying how this would not have very good facities whereas Mark has said how it has ''state-of-the-arts toilets and solar showers''.</i>
                    </c>
                    <c typ="4" id="33">
                      <i cc="7" id="1" />
                    </c>
                  </s>
                </p>
              </userData>
            </item>
            <item id="6050P6782" name="BR27_06" totalMark="4" version="73" markingType="1" markingState="0" userMark="0" markerUserMark="" userAttempted="1" viewingTime="415251" flagged="0" unit="2" quT="11" MarkedMetadata_6="4" dirty="15">
              <userData>
                <p um="0" cs="1" ua="1" id="6050P6782">
                  <s ua="1" um="0" id="1">
                    <c typ="4" id="28">
                      <i cc="9" id="1" />
                    </c>
                    <c typ="4" id="29">
                      <i cc="1" id="1" />
                    </c>
                    <c wei="3" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="30">
                      <i ca="" id="1">I think document 2 is the most biased because it is one persons opinion. This is seen when the writer speaks about "why on earth would anyone want to pay such rip off prices to go to a festival?". This is the writers opinion, not everyone would think it is a rip off to go to a festival. Another way this is shown in the text is "As we live in England, the weather is invariably damp, dull and miserable." The weather is not always damp, dull and miserable again this is just his view.</i>
                    </c>
                    <c typ="4" id="33">
                      <i cc="0" id="1" />
                    </c>
                    <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="35">
                      <i ca="" id="1">You need to know what bias means otherwise when you read this you would be persuaded not to go to the festival because you would be listening to someones opinion not facts.</i>
                    </c>
                  </s>
                </p>
              </userData>
            </item>
            <item id="6050P6783" name="BR27_07" totalMark="4" version="73" markingType="1" markingState="0" userMark="0" markerUserMark="" userAttempted="1" viewingTime="358171" flagged="0" unit="2" quT="11" MarkedMetadata_6="4" dirty="15">
              <userData>
                <p um="0" cs="1" ua="1" id="6050P6783">
                  <s ua="1" um="0" id="1">
                    <c typ="4" id="28">
                      <i cc="1" id="1" />
                    </c>
                    <c typ="4" id="29">
                      <i cc="7" id="1" />
                    </c>
                    <c wei="4" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="30">
                      <i ca="" id="1">I would reccomend this to a friend as the tickets are quite cheap, and you don''t have to pay this upfront you just have to pay a deposit. The festival has top of the range facilities now such as Solar showers, the festival also has a range of different music and lots to do. There will also be top line acts from UK and America and there will be atleast 80,000 people there so it will be good and it will also be held in June which most of the time in England the weather is normally nice. If you bike there from 20 miles someone will carry your luggage for you which is good.</i>
                    </c>
                    <c typ="4" id="33">
                      <i cc="1" id="1" />
                    </c>
                  </s>
                </p>
              </userData>
            </item>
          </section>
        </assessment>
        <isValid>1</isValid>
        <isPrintable>0</isPrintable>
        <qualityReview>0</qualityReview>
        <numberOfGenerations>1</numberOfGenerations>
        <requiresInvigilation>0</requiresInvigilation>
        <advanceContentDownloadTimespan>24</advanceContentDownloadTimespan>
        <automaticVerification>0</automaticVerification>
        <offlineMode>2</offlineMode>
        <requiresValidation>0</requiresValidation>
        <requiresSecureClient>1</requiresSecureClient>
        <examType>0</examType>
        <advanceDownload>0</advanceDownload>
        <gradeBoundaryData>
          <gradeBoundaries passLevelType="0" adjustedGradeBoundaries="0">
            <grade modifier="lt" value="12" description="Fail" />
            <grade modifier="gt" value="12" description="Fail n" />
            <grade modifier="gt" value="14" description="Pass" userCreated="1" />
          </gradeBoundaries>
        </gradeBoundaryData>
        <autoViewExam>0</autoViewExam>
        <autoProgressItems>0</autoProgressItems>
        <confirmationText>
          <confirmationText>By ticking this box you confirm that the above details are correct and you will adhere to OCR''s code of conduct.</confirmationText>
        </confirmationText>
        <requiresConfirmationCheck>1</requiresConfirmationCheck>
        <allowCloseWithoutSubmit>0</allowCloseWithoutSubmit>
        <useSecureMarker>0</useSecureMarker>
        <annotationVersion>2</annotationVersion>
        <allowPackagingDelivery>1</allowPackagingDelivery>
        <scoreBoundaryData>
          <scoreBoundaries showScoreBoundaryColumn="0">
            <score modifier="lt" value="40" description="Not met" higherBoundarySet="1" />
            <score modifier="lt" value="40" description="Not met" higherBoundarySet="0" />
            <score modifier="gt" value="40" description="Close to met" higherBoundarySet="1" />
            <score modifier="gt" value="40" description="Met" higherBoundarySet="0" />
            <score modifier="gt" value="45" description="Met" higherBoundarySet="1" />
            <score modifier="gt" value="80" description="Exceeded" higherBoundarySet="1" />
          </scoreBoundaries>
        </scoreBoundaryData>
        <showCandidateReportResultColumn>0</showCandidateReportResultColumn>
        <showCandidateReportPercentageColumn>1</showCandidateReportPercentageColumn>
        <certifiedAccessible>0</certifiedAccessible>
        <markingProgressVisible>1</markingProgressVisible>
        <markingProgressMode>0</markingProgressMode>
        <secureClientOperationMode>1</secureClientOperationMode>
        <examDeliverySystemStyleType>delivery</examDeliverySystemStyleType>
        <markingAutoVoidPeriod>365</markingAutoVoidPeriod>
        <showPageRequiresScrollingAlert>1</showPageRequiresScrollingAlert>
        <project ID="4209" />
        <project ID="6002" />
        <project ID="6050">
          <value display="1R1 Candidate has identified the main points, ideas and how they are presented in a variety of texts">0</value>
          <value display="1R2 Candidate has read and understood texts in detail">1</value>
          <value display="1R3 Candidate has utilised information contained in texts">2</value>
          <value display="1R4 Candidate has identified suitable responses to texts">3</value>
          <value display="2R1 Candidate has obtained and utilised relevant information and identified purposes of texts">4</value>
          <value display="2R2 Candidate has read and summarised info, analysed texts in rel to needs and considered suit resp">5</value>
          <value display="2R3 Candidate has commented on how meaning is conveyed, detected views, implic. meaning and/or bias ">6</value>
        </project>
      </assessmentDetails>
      <requiresHumanMarking>1</requiresHumanMarking>
      <language>en        </language>
      <scheduledForInvigilate>0</scheduledForInvigilate>
      <autoSubmitDate>2017-07-05T23:59:00</autoSubmitDate>
      <serverTime>2017-06-20T08:40:32.900</serverTime>
      <isProjectBased>0</isProjectBased>
      <startDate>2017-06-21T00:00:00</startDate>
      <endDate>2017-07-05T00:00:00</endDate>
      <centreId>38</centreId>
      <canExtendTime>0</canExtendTime>
    </examScript>
    <documentScript examSessionId="215688" />
    <localData>QWQG13fl0P+RTtXh0DDDt6CB2iUMWOuBiGcgPKlWbWS4/uHlLcU391GC4VXdQJFRj9NshUh+el+sQlR4xVj0isPdRVhuR40976Su9oGmreEgY0v5u/Ual9S2eSxlTzeaanLSRJ6WtL43GjzE8LLxZtg5DqKFKmQDravkgr5hBDBX5lw2N6/zWUBGqbvoaCijl+wEITvfwEmE+p9xrlzJm9Ix8/FMLmNKto8bZybnexZA9CILnuui+nZhcZd3Le+8mfyM5es6vZp+29FphPVxOFIrMre7Ui3RebWYlb84mZSnACINSC6pwy0oRisNAeL8DqCAc7sJGC5+1bO2yQcrotXKRdVfKJgADH7JW6gc2Dbrbi6O7sTEZVUG3CF3gBnjTBFX6i621i5WiKsrODQ1JL/9o9SCmwzILAlsesTJyMWHQggX/gVaJFf6+jxIU3ajmQOZ9gkFAnNkRMop4NLiQfOTciRp9xWvYQLVDdWwRlLWFEYLd89AX1s60DdlYJdD4cfofJkEInGUr176jXkBsD+HrK8KNjdFB4h5M7D0NpITUpH/IYaEWwz8SmxwRTDD8veN0dcrSPp0BRNsHB641P+HVGNiQZy79Vzc4y8VcnkbcLj/dHs9pfFQaF2FBZG90/SSDtyQg4bi6qRawa1dBP3CngwsoEkwxfRMV+KL7+E6W/vqXhEanpgQwkxIVbZZm35zuAV9jjTngLI/eGttpgG2z2wpfxHdGwCxE6BvXqTtN85mmysK2QEltN9f+L6p+tndi3a+98IZeOKxbeuFgfu8nTzWZKpFaPv+ISxFn6Y=</localData>
  </return>
</result>'

SELECT c.d.value('@id','nvarchar(100)') [ItemID]
	,  c.d.value('@name','nvarchar(100)') [ItemName]  --totalMark
	,  c.d.value('@totalMark','nvarchar(100)') [totalMark]
	,  c.d.value('@userAttempted','bit') [userAttempted]	
	,  REPLACE(REPLACE(CAST(e.f.query('.') as nvarchar(MAX)), '<i ca="" id="1">',''), '</i>','')
	--,  e.f.query('.')
FROM (VALUES (@XML)) a(b)
CROSS APPLY a.b.nodes('/result/return/examScript/assessmentDetails/assessment/section/item') c(d)
CROSS APPLY c.d.nodes('userData/p/s/c/i') e(f)
where LEN(CAST(e.f.query('.') as nvarchar(MAX))) > 18


--usp_SendWarehouseTrackingRequest