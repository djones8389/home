use Training_SecureAssess

SELECT EST.ID [ESID]
	, SCET.ID
	, EST.KeyCode
	, EST.examState
	, UT.Forename
	, UT.Surname
	, CT.CentreName
	, IB.QualificationName
	, SCET.examName
	, scet.scheduledstartdatetime
	, scet.scheduledenddatetime
	, structurexml
	, esirt.*
	, esdt.*
  FROM ExamSessionTable as EST (NOLOCK)

  Inner Join ScheduledExamsTable as SCET (NOLOCK)  on SCET.ID = EST.ScheduledExamID   
  Inner Join UserTable as UT (NOLOCK)  on UT.ID = EST.UserID
  Inner Join CentreTable as CT (NOLOCK)  on CT.ID = SCET.CentreID
  Inner Join IB3QualificationLookup as IB (NOLOCK) on IB.ID = scet.qualificationId
  left join ExamSessionItemResponseTable esirt (NOLOCK) on esirt.ExamSessionID = EST.id
  left join ExamSessionDocumentTable esdt	(NOLOCK) on esdt.ExamSessionID = EST.id

where  examState = 99


SELECT *
FROM EXamStateChangeAuditTable
where examSessionid in (7285,8556)



SELECT *
FROM StateManagementException
where examSessionid in (193,24187,27702,27703,41818,41819,41826,41828,41833,41834,41835,41856,41858,41866,41867,41868,41869,41870,41871,41872,41876,41877,41886,41887,41962,41985,41986,41987,41988,41989,41990,41991,41995,41996,41997,41998,41999,42004,42006,42023,42024,42025,42042,42043,42045,42046,42145,42146,42147,42148,42149,42150,42151,42170,42171)
	order by datestamp desc

select examSessionid, resultData
from Warehouse_ExamSessionTable
where examSessionid in (193,24187,27702,27703,41818,41819,41826,41828,41833,41834,41835,41856,41858,41866,41867,41868,41869,41870,41871,41872,41876,41877,41886,41887,41962,41985,41986,41987,41988,41989,41990,41991,41995,41996,41997,41998,41999,42004,42006,42023,42024,42025,42042,42043,42045,42046,42145,42146,42147,42148,42149,42150,42151,42170,42171)

--UPDATE ExamSessionTable
--set AwaitingUploadGracePeriodInDays = 1, AwaitingUploadStateChangingTime = '2015-11-29 09:59:57.247', examState = 18, attemptAutoSubmitForAwaitingUpload = 1
--where id in (120232,166135)


select id,scheduledexamid, examState
from examsessiontable
where scheduledexamid in (5222,5223,5224,5225,5235,5237,5238,5239,5240,5251,5252,5253,5254,5255,5256,5257,5258,5259,5260,5261,5262,5263,5264,5265,5266,5267)

--delete
--from ScheduledExamsTable
--where id in (5222,5223,5224,5225,5235,5237,5238,5239,5240,5251,5252,5253,5254,5255,5256,5257,5258,5259,5260,5261,5262,5263,5264,5265,5266,5267)

--delete 
--from ExamSessionTable
--where id = 1868;

--UPDATE ExamSessionTable
--set AwaitingUploadGracePeriodInDays = 1, AwaitingUploadStateChangingTime = '2015-11-29 09:59:57.247', examState = 18, attemptAutoSubmitForAwaitingUpload = 1
--where ID in (1683,7063,7493,8825,8925,8948,8949,8950,8951,8952,8953,8954,9545,9546,9947,9948)



--IF OBJECT_ID('tempdb..#ClearDownAuditing') IS NOT NULL DROP TABLE #ClearDownAuditing;

--select
--	ROW_NUMBER() OVER(PARTITION BY ExamSessionID, NewState ORDER BY ExamSessionID, NewState) AS [N]
--	, id
--	, ExamSessionID
--	, NewState
--	, StateChangeDate
--	, StateInformation
--into #ClearDownAuditing	
--from ExamStateChangeAuditTable
--where ExamSessionID IN (
--	select ID
--	from ExamSessionTable
--	where examState = 99
--	);

----237922
--DELETE A   
----select distinct a.id
--FROM ExamStateChangeAuditTable A

--INNER JOIN	 (

--SELECT MAX(N) as N, ID, ExamSessionID, NewState, StateChangeDate
--FROM #ClearDownAuditing
--GROUP BY ID, ExamSessionID, NewState, StateChangeDate
--) B

--ON A.ExamSessionID = B.ExamSessionID
--	AND A.NewState = B.NewState
--	AND A.StateChangeDate = B.StateChangeDate
--Where B.N != 1
--	--and A.NewState = 99;

--DROP TABLE #ClearDownAuditing;
