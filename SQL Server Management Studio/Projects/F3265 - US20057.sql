use master

IF OBJECT_ID('tempdb..##DATALENGTH') IS NOT NULL DROP TABLE ##DATALENGTH;

CREATE TABLE ##DATALENGTH (
	client nvarchar(1000)
	, CentreID int
	, SubjectID int
	, Users int
);

DECLARE @dynamic nvarchar(MAX)='';

select @dynamic +=CHAR(13)+ '
use '+QUOTENAME([NAME])+'

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

INSERT ##DATALENGTH
SELECT db_name()
	, CentreId
	, SubjectId
	, COUNT(1) AS Users
FROM dbo.UserCentreSubjectRoles
WHERE CentreId IS NOT NULL AND SubjectId IS NOT NULL
GROUP BY CentreId, SubjectId
ORDER BY Users DESC;
'
from sys.databases
where state_desc = 'ONLINE'
	and name like '%[_]SurpassManagement';

exec(@dynamic);



select substring(client, 0, charindex('_',client)) client
, CentreID
,SubjectID
, Users
from ##DATALENGTH
order by 1
