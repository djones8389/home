IF OBJECT_ID('tempdb..##ExamsSat') IS NOT NULL DROP TABLE ##ExamsSat;

CREATE TABLE ##ExamsSat (
	client nvarchar(1000)
	, examsSat int
);

DECLARE @dynamic nvarchar(MAX)='';

select @dynamic +=CHAR(13)+ '
use '+QUOTENAME([NAME])+'

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

INSERT ##ExamsSat
SELECT db_name()
	, COUNT(FES.[ExamSessionKey])
FROM '+QUOTENAME(NAME)+'..FactExamSessions FES
INNER JOIN '+QUOTENAME(NAME)+'..FactExamSessionsStateAudit FEST
ON FEST.[ExamSessionKey] = FES.ExamSessionKey
where FEST.ExamStateId = 6
'
from sys.databases
where state_desc = 'ONLINE'
	and name like '%[_]SurpassDataWarehouse';

exec(@dynamic);


select *
from ##ExamsSat
order by 1