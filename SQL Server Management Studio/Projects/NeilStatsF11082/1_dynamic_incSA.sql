IF OBJECT_ID('tempdb..##ExamsSat') IS NOT NULL DROP TABLE ##ExamsSat;

CREATE TABLE ##ExamsSat (
	client nvarchar(1000)
	, examsSat int
);


DECLARE @dynamic nvarchar(MAX)='';

select @dynamic += CHAR(13) + '

;with '+name+' as (
SELECT ExamSessionID
FROM '+name+'_SecureAssess.dbo.ExamStateChangeAuditTable
where newstate = 6 

UNION

SELECT west.ExamSessionID
FROM '+name+'_SecureAssess.dbo.WAREHOUSE_ExamSessionTable west
inner join '+name+'_SecureAssess.dbo.WAREHOUSE_ExamSessionTable_Shreded wests 
on west.id = wests.examSessionId
where started != ''1900-01-01 00:00:00.000''
)

INSERT ##ExamsSat
SELECT DISTINCT db_name()
	, FES.[ExamSessionKey]
FROM '+name+'_SurpassDataWarehouse.dbo.FactExamSessions FES
INNER JOIN '+name+'_SurpassDataWarehouse.dbo.FactExamSessionsStateAudit FEST
ON FEST.[ExamSessionKey] = FES.ExamSessionKey
where FEST.ExamStateId = 6

UNION

SELECT DISTINCT db_name()
	, WEST.ID
FROM '+name+'_SecureAssess.dbo.WAREHOUSE_ExamSessionTable WEST
INNER JOIN '+name+' SA
on SA.ExamSessionID = WEST.ExamSessionID
LEFT JOIN '+name+'_SurpassDataWarehouse.dbo.FactExamSessions FES
on FES.ExamSessionKey = WEST.ID
where FES.ExamSessionKey IS NULL
'
from (
	select distinct B.name
	FROM (
		select SUBSTRING(name, 0, charindex('_',name)) [Name]
			, ROW_NUMBER() OVER(PARTITION BY SUBSTRING(name, 0, charindex('_',name)) ORDER BY SUBSTRING(name, 0, charindex('_',name)) ) R
		from sys.databases
		where state_desc = 'ONLINE'
			and [Name] like '%/_%' ESCAPE '/'
			and name not like 'test%'
		) B
	where R > 3
) a

exec(@dynamic)


select *
from ##ExamsSat

--F11082_1_ExamCount.sql