IF OBJECT_ID('tempdb..##Countries') IS NOT NULL DROP TABLE ##Countries;

CREATE TABLE ##Countries (
	client nvarchar(1000)
	, countries nvarchar(200)
);

DECLARE @dynamic nvarchar(MAX)='';

select @dynamic +=CHAR(13)+ '
use '+QUOTENAME([NAME])+'

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

INSERT ##Countries
SELECT distinct db_name()
	, dc.Country
FROM '+QUOTENAME(NAME)+'.dbo.FactExamSessions FES
INNER JOIN '+QUOTENAME(NAME)+'.dbo.FactExamSessionsStateAudit FEST
ON FEST.[ExamSessionKey] = FES.ExamSessionKey
INNER JOIN '+QUOTENAME(NAME)+'.dbo.DimCentres DCE 
ON DCE.CentreKey = FES.CentreKey
INNER JOIN '+QUOTENAME(NAME)+'.dbo.DimCountry DC
on DC.Country = DCE.Country
where FEST.ExamStateId = 6
	
'
from sys.databases
where state_desc = 'ONLINE'
	and name like '%[_]SurpassDataWarehouse';

exec(@dynamic);


select distinct countries
from ##Countries
order by 1