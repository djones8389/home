IF OBJECT_ID('tempdb..##CompMarkedItems') IS NOT NULL DROP TABLE ##CompMarkedItems;

CREATE TABLE ##CompMarkedItems (
	client nvarchar(1000)
	, CompMarkedItems int
);

DECLARE @dynamic nvarchar(MAX)='';

select @dynamic +=CHAR(13)+ '
use '+QUOTENAME([NAME])+'

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

INSERT ##CompMarkedItems
SELECT db_name()
	, count(FCR.CPID)
FROM '+QUOTENAME(NAME)+'.dbo.FactComponentResponses FCR
INNER JOIN '+QUOTENAME(NAME)+'.dbo.[DimQuestions] DQ
on DQ.CPID = FCR.CPID
	and DQ.CPVersion = FCR.CPVersion
where DQ.MarkingTypeKey = 0 
'
from sys.databases
where state_desc = 'ONLINE'
	and name like '%[_]SurpassDataWarehouse';

exec(@dynamic);


select *
from ##CompMarkedItems
order by 1
