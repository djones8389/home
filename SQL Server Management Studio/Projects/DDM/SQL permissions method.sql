CREATE TABLE [UserTable] (	
	ID INT
	, forename NVARCHAR(100)
	, surname NVARCHAR(100)
	, email NVARCHAR(100)
	, addressline1 NVARCHAR(100)
	, DOB DATETIME 
)

INSERT [UserTable]
VALUES('1','Dave','Jones','dave.jones@btl.com','DaveHouse','1900-01-01')
,('2','Peter','Griffin','Peter.jones@btl.com','PeterHouse','1988-03-06')
,('3','William','Wallace','William.Wallace@scotland.com','ScotlandSomewhere','1972-05-19')


CREATE USER Developer --WITHOUT LOGIN;
GRANT SELECT ON UserTable TO Developer;  

EXECUTE AS USER = 'Developer';  
SELECT [ID]
	, [forename]
	, [surname]
	, [email]
	, [addressline1]
	, [DOB]
FROM [dbo].[UserTable]
REVERT;  

DENY SELECT ON dbo.UserTable ([forename],[forename],[email]) TO Developer;
