SELECT '
	SELECT A.PageID
		, Count(A.Component) ComponentCount_'+name+'
	FROM (
	SELECT SUBSTRING(ID, 0, CHARINDEX(''S'',ID)) PageID
		, SUBSTRING(ID, CHARINDEX(''C'', ID) + 1, CHARINDEX(''I'',ID) - CHARINDEX(''C'', ID) - 2 + Len(''I'')) ''Component''
	From ' + 'dbo.'+ QUOTENAME(name) + ' (READUNCOMMITTED)
	) A
	group by PageID
'	
FROM (
	SELECT name
	FROM sys.tables
	WHERE name LIKE 'Item%'
		AND name NOT IN ('ItemSpecificationLookup','ItemType')

) a

/*
    SELECT A.PageID   , Count(A.Component) ComponentCount_ItemVideoTable  FROM (  SELECT SUBSTRING(ID, 0, CHARINDEX('S',ID)) PageID   , SUBSTRING(ID, CHARINDEX('C', ID) + 1, CHARINDEX('I',ID) - CHARINDEX('C', ID) - 2 + Len('I')) 'Component'  From dbo.[ItemVideoTable] (READUNCOMMITTED)  ) A  group by PageID    
    SELECT A.PageID   , Count(A.Component) ComponentCount_ItemTextSelectorTable  FROM (  SELECT SUBSTRING(ID, 0, CHARINDEX('S',ID)) PageID   , SUBSTRING(ID, CHARINDEX('C', ID) + 1, CHARINDEX('I',ID) - CHARINDEX('C', ID) - 2 + Len('I')) 'Component'  From dbo.[ItemTextSelectorTable] (READUNCOMMITTED)  ) A  group by PageID    
    SELECT A.PageID   , Count(A.Component) ComponentCount_ItemTextBoxTable  FROM (  SELECT SUBSTRING(ID, 0, CHARINDEX('S',ID)) PageID   , SUBSTRING(ID, CHARINDEX('C', ID) + 1, CHARINDEX('I',ID) - CHARINDEX('C', ID) - 2 + Len('I')) 'Component'  From dbo.[ItemTextBoxTable] (READUNCOMMITTED)  ) A  group by PageID    
    SELECT A.PageID   , Count(A.Component) ComponentCount_ItemSceneConditionTable  FROM (  SELECT SUBSTRING(ID, 0, CHARINDEX('S',ID)) PageID   , SUBSTRING(ID, CHARINDEX('C', ID) + 1, CHARINDEX('I',ID) - CHARINDEX('C', ID) - 2 + Len('I')) 'Component'  From dbo.[ItemSceneConditionTable] (READUNCOMMITTED)  ) A  group by PageID    
    SELECT A.PageID   , Count(A.Component) ComponentCount_ItemReorderingTable  FROM (  SELECT SUBSTRING(ID, 0, CHARINDEX('S',ID)) PageID   , SUBSTRING(ID, CHARINDEX('C', ID) + 1, CHARINDEX('I',ID) - CHARINDEX('C', ID) - 2 + Len('I')) 'Component'  From dbo.[ItemReorderingTable] (READUNCOMMITTED)  ) A  group by PageID    
    SELECT A.PageID   , Count(A.Component) ComponentCount_ItemPicklistTable  FROM (  SELECT SUBSTRING(ID, 0, CHARINDEX('S',ID)) PageID   , SUBSTRING(ID, CHARINDEX('C', ID) + 1, CHARINDEX('I',ID) - CHARINDEX('C', ID) - 2 + Len('I')) 'Component'  From dbo.[ItemPicklistTable] (READUNCOMMITTED)  ) A  group by PageID    
    SELECT A.PageID   , Count(A.Component) ComponentCount_ItemMultipleChoiceTable  FROM (  SELECT SUBSTRING(ID, 0, CHARINDEX('S',ID)) PageID   , SUBSTRING(ID, CHARINDEX('C', ID) + 1, CHARINDEX('I',ID) - CHARINDEX('C', ID) - 2 + Len('I')) 'Component'  From dbo.[ItemMultipleChoiceTable] (READUNCOMMITTED)  ) A  group by PageID    
    SELECT A.PageID   , Count(A.Component) ComponentCount_ItemImageMapTable  FROM (  SELECT SUBSTRING(ID, 0, CHARINDEX('S',ID)) PageID   , SUBSTRING(ID, CHARINDEX('C', ID) + 1, CHARINDEX('I',ID) - CHARINDEX('C', ID) - 2 + Len('I')) 'Component'  From dbo.[ItemImageMapTable] (READUNCOMMITTED)  ) A  group by PageID    
    SELECT A.PageID   , Count(A.Component) ComponentCount_ItemHotSpotTable  FROM (  SELECT SUBSTRING(ID, 0, CHARINDEX('S',ID)) PageID   , SUBSTRING(ID, CHARINDEX('C', ID) + 1, CHARINDEX('I',ID) - CHARINDEX('C', ID) - 2 + Len('I')) 'Component'  From dbo.[ItemHotSpotTable] (READUNCOMMITTED)  ) A  group by PageID    
    SELECT A.PageID   , Count(A.Component) ComponentCount_ItemGraphicTable  FROM (  SELECT SUBSTRING(ID, 0, CHARINDEX('S',ID)) PageID   , SUBSTRING(ID, CHARINDEX('C', ID) + 1, CHARINDEX('I',ID) - CHARINDEX('C', ID) - 2 + Len('I')) 'Component'  From dbo.[ItemGraphicTable] (READUNCOMMITTED)  ) A  group by PageID    
    SELECT A.PageID   , Count(A.Component) ComponentCount_ItemGapfillTable  FROM (  SELECT SUBSTRING(ID, 0, CHARINDEX('S',ID)) PageID   , SUBSTRING(ID, CHARINDEX('C', ID) + 1, CHARINDEX('I',ID) - CHARINDEX('C', ID) - 2 + Len('I')) 'Component'  From dbo.[ItemGapfillTable] (READUNCOMMITTED)  ) A  group by PageID    
    SELECT A.PageID   , Count(A.Component) ComponentCount_ItemDragAndDropTable  FROM (  SELECT SUBSTRING(ID, 0, CHARINDEX('S',ID)) PageID   , SUBSTRING(ID, CHARINDEX('C', ID) + 1, CHARINDEX('I',ID) - CHARINDEX('C', ID) - 2 + Len('I')) 'Component'  From dbo.[ItemDragAndDropTable] (READUNCOMMITTED)  ) A  group by PageID    
    SELECT A.PageID   , Count(A.Component) ComponentCount_ItemDocumentTable  FROM (  SELECT SUBSTRING(ID, 0, CHARINDEX('S',ID)) PageID   , SUBSTRING(ID, CHARINDEX('C', ID) + 1, CHARINDEX('I',ID) - CHARINDEX('C', ID) - 2 + Len('I')) 'Component'  From dbo.[ItemDocumentTable] (READUNCOMMITTED)  ) A  group by PageID    
    SELECT A.PageID   , Count(A.Component) ComponentCount_ItemCustomQuestionTable  FROM (  SELECT SUBSTRING(ID, 0, CHARINDEX('S',ID)) PageID   , SUBSTRING(ID, CHARINDEX('C', ID) + 1, CHARINDEX('I',ID) - CHARINDEX('C', ID) - 2 + Len('I')) 'Component'  From dbo.[ItemCustomQuestionTable] (READUNCOMMITTED)  ) A  group by PageID    
    SELECT A.PageID   , Count(A.Component) ComponentCount_ItemAutoShapeTable  FROM (  SELECT SUBSTRING(ID, 0, CHARINDEX('S',ID)) PageID   , SUBSTRING(ID, CHARINDEX('C', ID) + 1, CHARINDEX('I',ID) - CHARINDEX('C', ID) - 2 + Len('I')) 'Component'  From dbo.[ItemAutoShapeTable] (READUNCOMMITTED)  ) A  group by PageID    
    SELECT A.PageID   , Count(A.Component) ComponentCount_ItemAutoAudio  FROM (  SELECT SUBSTRING(ID, 0, CHARINDEX('S',ID)) PageID   , SUBSTRING(ID, CHARINDEX('C', ID) + 1, CHARINDEX('I',ID) - CHARINDEX('C', ID) - 2 + Len('I')) 'Component'  From dbo.[ItemAutoAudio] (READUNCOMMITTED)  ) A  group by PageID    
    SELECT A.PageID   , Count(A.Component) ComponentCount_ItemAudioRecorderTable  FROM (  SELECT SUBSTRING(ID, 0, CHARINDEX('S',ID)) PageID   , SUBSTRING(ID, CHARINDEX('C', ID) + 1, CHARINDEX('I',ID) - CHARINDEX('C', ID) - 2 + Len('I')) 'Component'  From dbo.[ItemAudioRecorderTable] (READUNCOMMITTED)  ) A  group by PageID    

*/