declare @Keycode nvarchar(10) = 'GNKQ4G89'


select * 
FROM STG_BC2_TestPackageManager.dbo.CommonSettings;

select top 100 examsessionid, warehouseTime, usermark, userpercentage, warehouseexamstate, keycode, previousExamState, centreName, foreName, surName, resultData,itemDataFull
from STG_BC2_SecureAssess.dbo.WAREHOUSE_ExamSessionTable_Shreded  SA
INNER JOIN STG_BC2_TestPackageManager.dbo.ScheduledPackageCandidateExams TPM
on SA.KeyCode = TPM.ScheduledExamRef COLLATE SQL_Latin1_General_CP1_CI_AS 
where previousExamState !=10
	and userMark > 0
	and KeyCode = @Keycode
order by warehouseTime desc;

select IsVoided, IsCompleted, DateCompleted, ExamCompletionDate, ScheduledExamRef, [Score]
from STG_BC2_TestPackageManager.dbo.[ScheduledPackageCandidateExams]
where ScheduledExamRef = @Keycode;


select Score, PackageScore
from STG_BC2_TestPackageManager.dbo.ScheduledPackageCandidates SPC
INNER JOIN STG_BC2_TestPackageManager.dbo.ScheduledPackageCandidateExams SPCE
on SPCE.Candidate_ScheduledPackageCandidateId = SPC.ScheduledPackageCandidateId
where ScheduledExamRef = @Keycode;


UPDATE STG_BC2_TestPackageManager.dbo.ScheduledPackageCandidateExams
set IsCompleted = 0, IsVoided = 0, Score = '0.00'
where ScheduledExamRef = @Keycode;

UPDATE STG_BC2_TestPackageManager.dbo.ScheduledPackageCandidates
set IsCompleted = 0, IsVoided = 0--, PackageScore = NULL
FROM STG_BC2_TestPackageManager.dbo.ScheduledPackageCandidates SPC
INNER JOIN STG_BC2_TestPackageManager.dbo.ScheduledPackageCandidateExams SPCe
on SPCE.Candidate_ScheduledPackageCandidateId = SPC.ScheduledPackageCandidateId
where ScheduledExamRef = @Keycode;

UPDATE STG_BC2_SecureAssess.dbo.WAREHOUSE_ExamSessionTable
set warehouseTime = '2016-11-03 12:45:00.001', resultData = '<exam passMark="0" passType="1" originalPassMark="0" originalPassType="1" totalMark="25" userMark="2.000" userPercentage="8.000" passValue="1" originalPassValue="1" totalTimeSpent="12322" closeBoundaryType="0" closeBoundaryValue="0" grade="Pass" originalGrade="Pass">
  <gradeBoundaryData>
    <gradeBoundaries passLevelType="1" adjustedGradeBoundaries="0">
      <grade modifier="lt" value="0" description="Fail" />
      <grade modifier="gt" value="0" description="Pass" />
    </gradeBoundaries>
  </gradeBoundaryData>
  <section id="1" passMark="0" passType="1" name="Section 1" totalMark="25" userMark="2.000" userPercentage="8.000" passValue="1" totalTimeSpent="12322" itemsToMark="0">
    <item id="3521P3670" name="Template_A1_dropdown sentence" totalMark="5" userMark="1" actualUserMark="5.00" markingType="0" markerUserMark="" viewingTime="5110" userAttempted="1" version="30" />
    <item id="3521P3673" name="Copy of Template_A2_reorder text" totalMark="6" userMark="1" actualUserMark="6.00" markingType="0" markerUserMark="" viewingTime="7212" userAttempted="1" version="32" />
    <item id="3521P3668" name="Template_B1_drag and drop" totalMark="7" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="31" />
    <item id="3521P3671" name="Copy of Template_B2_long text" totalMark="7" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="37" />
    <item id="3521P3581" name="Reading_Submit_Finish" totalMark="0" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="15" />
  </section>
</exam>'
where Keycode = @Keycode;

UPDATE STG_BC2_SecureAssess.dbo.WAREHOUSE_ExamSessionTable_Shreded
set warehouseTime = '2016-11-03 12:45:00.001', resultData = '<exam passMark="0" passType="1" originalPassMark="0" originalPassType="1" totalMark="25" userMark="2.000" userPercentage="8.000" passValue="1" originalPassValue="1" totalTimeSpent="12322" closeBoundaryType="0" closeBoundaryValue="0" grade="Pass" originalGrade="Pass">
  <gradeBoundaryData>
    <gradeBoundaries passLevelType="1" adjustedGradeBoundaries="0">
      <grade modifier="lt" value="0" description="Fail" />
      <grade modifier="gt" value="0" description="Pass" />
    </gradeBoundaries>
  </gradeBoundaryData>
  <section id="1" passMark="0" passType="1" name="Section 1" totalMark="25" userMark="2.000" userPercentage="8.000" passValue="1" totalTimeSpent="12322" itemsToMark="0">
    <item id="3521P3670" name="Template_A1_dropdown sentence" totalMark="5" userMark="1" actualUserMark="5.00" markingType="0" markerUserMark="" viewingTime="5110" userAttempted="1" version="30" />
    <item id="3521P3673" name="Copy of Template_A2_reorder text" totalMark="6" userMark="1" actualUserMark="6.00" markingType="0" markerUserMark="" viewingTime="7212" userAttempted="1" version="32" />
    <item id="3521P3668" name="Template_B1_drag and drop" totalMark="7" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="31" />
    <item id="3521P3671" name="Copy of Template_B2_long text" totalMark="7" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="37" />
    <item id="3521P3581" name="Reading_Submit_Finish" totalMark="0" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="15" />
  </section>
</exam>'
where Keycode = @Keycode;



--SELECT SUM(a.b.value('@userMark','float') * a.b.value('@totalMark','float'))
--from STG_BC2_SecureAssess.dbo.WAREHOUSE_ExamSessionTable_Shreded
--CROSS APPLY itemDataFull.nodes('itemData/item')a(b)
--where keycode = @Keycode



--select examsessionid, warehouseTime, usermark, userpercentage, warehouseexamstate, keycode, previousExamState, centreName, foreName, surName, resultData,itemDataFull
--from STG_BC2_SecureAssess.dbo.WAREHOUSE_ExamSessionTable_Shreded
--where keycode = @Keycode
--order by warehouseTime desc;

--UPDATE STG_BC2_SecureAssess.dbo.WAREHOUSE_ExamSessionTable_Shreded
--set ItemDataFUll = '<itemData>'
--where keycode = @Keycode


--select IsVoided, IsCompleted, DateCompleted, ExamCompletionDate, ScheduledExamRef, [Score]
--from STG_BC2_TestPackageManager.dbo.[ScheduledPackageCandidateExams]
--where ScheduledExamRef = @Keycode


--SELECT *
--from STG_BC2_TestPackageManager.dbo.CommonSettings


--UPDATE STG_BC2_TestPackageManager.dbo.[ScheduledPackageCandidateExams]
--set IsVoided = 0, IsCompleted = 0, Score = 0.00
--where ScheduledExamRef IN ('XAN43M01');

--update STG_BC2_TestPackageManager.dbo.ScheduledPackageCandidates
--set IsCompleted = 0, IsVoided = 0, PackageScore = NULL
--where ScheduledPackageCandidateId = 1;

--UPDATE STG_BC2_SecureAssess.dbo.WAREHOUSE_ExamSessionTable
--set warehouseTime = '21 Feb 2016 12:53:22.390'
--where Keycode IN ('XAN43M01');

--UPDATE STG_BC2_SecureAssess.dbo.WAREHOUSE_ExamSessionTable_Shreded
--set warehouseTime = '21 Feb 2016 12:53:22.390'
--where Keycode IN ('XAN43M01');



--Key	Value
--02/21/2016 12:44:15


--SELECT ItemResponseData.value('(/p/@um)[1]','float')
--	--,ItemResponseData
--from STG_BC2_SecureAssess.dbo.WAREHOUSE_ExamSessionItemResponseTable
--where WAREHOUSE_ExamSessionItemResponseTable.WAREHOUSEExamSessionID = 1097067
--	--group by WAREHOUSEExamSessionID






/*



UPDATE STG_BC2_TestPackageManager.dbo.[ScheduledPackageCandidateExams]
set IsVoided = 0, IsCompleted = 0, Score = 0.00
where ScheduledExamRef IN ('XAN43M01');

update STG_BC2_TestPackageManager.dbo.ScheduledPackageCandidates
set IsCompleted = 0, IsVoided = 0, PackageScore = NULL
where ScheduledPackageCandidateId = 282250;

UPDATE STG_BC2_SecureAssess.dbo.WAREHOUSE_ExamSessionTable_Shreded
set warehouseTime = '2016-02-11 17:01:22.390', resultData = '<exam passMark="0" passType="1" originalPassMark="0" originalPassType="1" totalMark="71" userMark="35.000" userPercentage="49.296" passValue="1" originalPassValue="1" totalTimeSpent="864561" closeBoundaryType="0" closeBoundaryValue="0" grade="Pass" originalGrade="Pass">
  <gradeBoundaryData>
    <gradeBoundaries passLevelType="1" adjustedGradeBoundaries="0">
      <grade modifier="lt" value="0" description="Fail" />
      <grade modifier="gt" value="0" description="Pass" />
    </gradeBoundaries>
  </gradeBoundaryData>
  <section id="1" passMark="0" passType="1" name="Grammar and Vocabulary" totalMark="21" userMark="14.333" userPercentage="68.254" passValue="1" totalTimeSpent="267886" itemsToMark="0">
    <item id="3520P3592" name="General Intro_Famtest" totalMark="0" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="25040" userAttempted="0" version="20" />
    <item id="3520P3587" name="Grammar_Famtest_intro_01" totalMark="0" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="25786" userAttempted="0" version="15" />
    <item id="3520P3598" name="Copy of Example_MCQ_Grammar_Famtest_02" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="13509" userAttempted="1" version="30" />
    <item id="3520P3588" name="Vocabulary_Famtest_intro_coll_03" totalMark="0" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="15413" userAttempted="0" version="15" />
    <item id="3520P3593" name="Example_Voc_Coll_Famtest_04" totalMark="5" userMark="0.33333333" actualUserMark="1.66666665" markingType="0" markerUserMark="" viewingTime="24680" userAttempted="1" version="28" />
    <item id="3520P3589" name="Vocabulary_Famtest_intro_syn_05" totalMark="0" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="15507" userAttempted="0" version="14" />
    <item id="3520P3594" name="Example_Voc_Syn_Famtest_06" totalMark="5" userMark="1" actualUserMark="5" markingType="0" markerUserMark="" viewingTime="30997" userAttempted="1" version="30" />
    <item id="3520P3590" name="Vocabulary_Famtest_intro_def_07" totalMark="0" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="6162" userAttempted="0" version="15" />
    <item id="3520P3595" name="Example_Voc_Def_Famtest_08" totalMark="5" userMark="0.33333333" actualUserMark="1.66666665" markingType="0" markerUserMark="" viewingTime="40389" userAttempted="1" version="30" />
    <item id="3520P3565" name="Vocabulary_Famtest_intro_use_09" totalMark="0" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="13510" userAttempted="0" version="15" />
    <item id="3520P3596" name="Example_Voc_Use_Famtest_10" totalMark="5" userMark="1" actualUserMark="5" markingType="0" markerUserMark="" viewingTime="56893" userAttempted="1" version="26" />
  </section>
  <section id="2" passMark="0" passType="1" name="Listening" totalMark="1" userMark="1" userPercentage="100" passValue="1" totalTimeSpent="66756" itemsToMark="0">
    <item id="3522P3662" name="Listening_General_Info_Famtest_01" totalMark="0" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="14508" userAttempted="0" version="17" />
    <item id="3522P3658" name="Listening_Famtest_Intro_02" totalMark="0" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="16599" userAttempted="0" version="15" />
    <item id="3522P3594" name="Demo_Listen_Q1" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="35649" userAttempted="1" version="33" />
  </section>
  <section id="3" passMark="0" passType="1" name="Reading" totalMark="25" userMark="6.500" userPercentage="26.000" passValue="1" totalTimeSpent="364406" itemsToMark="0">
    <item id="3521P3570" name=" Reading_Famtest_General Intro" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="14342" userAttempted="0" version="15" />
    <item id="3521P3574" name="Reading_Famtest_intro_task_1" totalMark="0" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="1014" userAttempted="0" version="13" />
    <item id="3521P3565" name="Copy of Demo_Reading_Task1" totalMark="5" userMark="0.5" actualUserMark="2.5" markingType="0" markerUserMark="" viewingTime="39093" userAttempted="1" version="28" />
    <item id="3521P3573" name=" Reading_Famtest_intro_task_2" totalMark="0" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="15491" userAttempted="0" version="14" />
    <item id="3521P3566" name="Copy of Demo_Reading_Task_2" totalMark="6" userMark="0.5" actualUserMark="3.0" markingType="0" markerUserMark="" viewingTime="96003" userAttempted="1" version="27" />
    <item id="3521P3572" name=" Reading_Famtest_intro_task_3" totalMark="0" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="16629" userAttempted="0" version="14" />
    <item id="3521P3567" name="Copy of Demo_Reading_Task_3" totalMark="7" userMark="0.14285714" actualUserMark="0.99999998" markingType="0" markerUserMark="" viewingTime="138419" userAttempted="1" version="28" />
    <item id="3521P3571" name="Reading_Famtest_intro_task_4" totalMark="0" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="8284" userAttempted="0" version="14" />
    <item id="3521P3568" name="Copy of Demo_Reading_Task_4" totalMark="6" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="35131" userAttempted="0" version="34" />
  </section>
  <section id="5" passMark="0" passType="1" name="Writing" totalMark="20" userMark="0" userPercentage="0" passValue="1" totalTimeSpent="69405" itemsToMark="0">
    <item id="3523P3574" name="Writing Fam Test General Intro" totalMark="0" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="1029" userAttempted="0" version="20" />
    <item id="3523P3583" name="Writing Famtest_intro_task_1 " totalMark="0" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="9344" userAttempted="0" version="17" />
    <item id="3523P3575" name="Writing Famtest_Task_1" totalMark="5" userMark="0" actualUserMark="0" markingType="1" markerUserMark="" viewingTime="5164" userAttempted="0" version="40" />
    <item id="3523P3591" name="Writing Famtest_intro_task_2" totalMark="0" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="7176" userAttempted="0" version="18" />
    <item id="3523P3777" name="Writing Famtest_Task_2" totalMark="5" userMark="0" actualUserMark="0" markingType="1" markerUserMark="" viewingTime="5195" userAttempted="0" version="33" />
    <item id="3523P3592" name="Writing Famtest_intro_task_3" totalMark="0" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="22870" userAttempted="0" version="18" />
    <item id="3523P3579" name="Writing Famtest_Task_3" totalMark="5" userMark="0" actualUserMark="0" markingType="1" markerUserMark="" viewingTime="3105" userAttempted="0" version="42" />
    <item id="3523P3593" name="Writing Famtest_intro_task_4" totalMark="0" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="2074" userAttempted="0" version="16" />
    <item id="3523P3577" name="Writing Famtest_Task_4" totalMark="5" userMark="0" actualUserMark="0" markingType="1" markerUserMark="" viewingTime="13448" userAttempted="0" version="32" />
  </section>
  <section id="6" passMark="0" passType="1" name="Speaking" totalMark="4" userMark="0" userPercentage="0" passValue="1" totalTimeSpent="96108" itemsToMark="0">
    <item id="3524P3619" name="Speaking_Fam_intro_task_1" totalMark="0" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="2043" userAttempted="0" version="15" />
    <item id="3524P3600" name="Speaking_Demo_Task_1" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="16072" userAttempted="0" version="49" />
    <item id="3524P3588" name="Speaking_Fam_intro_task_2" totalMark="0" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="5090" userAttempted="0" version="12" />
    <item id="3524P3683" name="Speaking_Demo_Task_2" totalMark="1" userMark="0" actualUserMark="0" markingType="1" markerUserMark="" viewingTime="1040" userAttempted="0" version="48" />
    <item id="3524P3620" name="Speaking_Fam_intro_task_3" totalMark="0" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="11088" userAttempted="0" version="11" />
    <item id="3524P3685" name="Speaking_Demo_Task_3" totalMark="1" userMark="0" actualUserMark="0" markingType="1" markerUserMark="" viewingTime="17060" userAttempted="0" version="37" />
    <item id="3524P3621" name="Speaking_Fam_intro_task_4" totalMark="0" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="9030" userAttempted="0" version="11" />
    <item id="3524P3687" name="Speaking_Demo_Task_4" totalMark="1" userMark="0" actualUserMark="0" markingType="1" markerUserMark="" viewingTime="12150" userAttempted="0" version="28" />
    <item id="3524P4062" name="Finish Page " totalMark="0" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="22535" userAttempted="0" version="19" />
  </section>
</exam>'
where keycode in ('XAN43M01');

UPDATE STG_BC2_SecureAssess.dbo.WAREHOUSE_ExamSessionTable
set warehouseTime = '2016-02-11 17:01:22.390', resultData = '<exam passMark="0" passType="1" originalPassMark="0" originalPassType="1" totalMark="71" userMark="35.000" userPercentage="49.296" passValue="1" originalPassValue="1" totalTimeSpent="864561" closeBoundaryType="0" closeBoundaryValue="0" grade="Pass" originalGrade="Pass">
  <gradeBoundaryData>
    <gradeBoundaries passLevelType="1" adjustedGradeBoundaries="0">
      <grade modifier="lt" value="0" description="Fail" />
      <grade modifier="gt" value="0" description="Pass" />
    </gradeBoundaries>
  </gradeBoundaryData>
  <section id="1" passMark="0" passType="1" name="Grammar and Vocabulary" totalMark="21" userMark="14.333" userPercentage="68.254" passValue="1" totalTimeSpent="267886" itemsToMark="0">
    <item id="3520P3592" name="General Intro_Famtest" totalMark="0" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="25040" userAttempted="0" version="20" />
    <item id="3520P3587" name="Grammar_Famtest_intro_01" totalMark="0" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="25786" userAttempted="0" version="15" />
    <item id="3520P3598" name="Copy of Example_MCQ_Grammar_Famtest_02" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="13509" userAttempted="1" version="30" />
    <item id="3520P3588" name="Vocabulary_Famtest_intro_coll_03" totalMark="0" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="15413" userAttempted="0" version="15" />
    <item id="3520P3593" name="Example_Voc_Coll_Famtest_04" totalMark="5" userMark="0.33333333" actualUserMark="1.66666665" markingType="0" markerUserMark="" viewingTime="24680" userAttempted="1" version="28" />
    <item id="3520P3589" name="Vocabulary_Famtest_intro_syn_05" totalMark="0" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="15507" userAttempted="0" version="14" />
    <item id="3520P3594" name="Example_Voc_Syn_Famtest_06" totalMark="5" userMark="1" actualUserMark="5" markingType="0" markerUserMark="" viewingTime="30997" userAttempted="1" version="30" />
    <item id="3520P3590" name="Vocabulary_Famtest_intro_def_07" totalMark="0" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="6162" userAttempted="0" version="15" />
    <item id="3520P3595" name="Example_Voc_Def_Famtest_08" totalMark="5" userMark="0.33333333" actualUserMark="1.66666665" markingType="0" markerUserMark="" viewingTime="40389" userAttempted="1" version="30" />
    <item id="3520P3565" name="Vocabulary_Famtest_intro_use_09" totalMark="0" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="13510" userAttempted="0" version="15" />
    <item id="3520P3596" name="Example_Voc_Use_Famtest_10" totalMark="5" userMark="1" actualUserMark="5" markingType="0" markerUserMark="" viewingTime="56893" userAttempted="1" version="26" />
  </section>
  <section id="2" passMark="0" passType="1" name="Listening" totalMark="1" userMark="1" userPercentage="100" passValue="1" totalTimeSpent="66756" itemsToMark="0">
    <item id="3522P3662" name="Listening_General_Info_Famtest_01" totalMark="0" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="14508" userAttempted="0" version="17" />
    <item id="3522P3658" name="Listening_Famtest_Intro_02" totalMark="0" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="16599" userAttempted="0" version="15" />
    <item id="3522P3594" name="Demo_Listen_Q1" totalMark="1" userMark="1" actualUserMark="1" markingType="0" markerUserMark="" viewingTime="35649" userAttempted="1" version="33" />
  </section>
  <section id="3" passMark="0" passType="1" name="Reading" totalMark="25" userMark="6.500" userPercentage="26.000" passValue="1" totalTimeSpent="364406" itemsToMark="0">
    <item id="3521P3570" name=" Reading_Famtest_General Intro" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="14342" userAttempted="0" version="15" />
    <item id="3521P3574" name="Reading_Famtest_intro_task_1" totalMark="0" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="1014" userAttempted="0" version="13" />
    <item id="3521P3565" name="Copy of Demo_Reading_Task1" totalMark="5" userMark="0.5" actualUserMark="2.5" markingType="0" markerUserMark="" viewingTime="39093" userAttempted="1" version="28" />
    <item id="3521P3573" name=" Reading_Famtest_intro_task_2" totalMark="0" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="15491" userAttempted="0" version="14" />
    <item id="3521P3566" name="Copy of Demo_Reading_Task_2" totalMark="6" userMark="0.5" actualUserMark="3.0" markingType="0" markerUserMark="" viewingTime="96003" userAttempted="1" version="27" />
    <item id="3521P3572" name=" Reading_Famtest_intro_task_3" totalMark="0" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="16629" userAttempted="0" version="14" />
    <item id="3521P3567" name="Copy of Demo_Reading_Task_3" totalMark="7" userMark="0.14285714" actualUserMark="0.99999998" markingType="0" markerUserMark="" viewingTime="138419" userAttempted="1" version="28" />
    <item id="3521P3571" name="Reading_Famtest_intro_task_4" totalMark="0" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="8284" userAttempted="0" version="14" />
    <item id="3521P3568" name="Copy of Demo_Reading_Task_4" totalMark="6" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="35131" userAttempted="0" version="34" />
  </section>
  <section id="5" passMark="0" passType="1" name="Writing" totalMark="20" userMark="0" userPercentage="0" passValue="1" totalTimeSpent="69405" itemsToMark="0">
    <item id="3523P3574" name="Writing Fam Test General Intro" totalMark="0" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="1029" userAttempted="0" version="20" />
    <item id="3523P3583" name="Writing Famtest_intro_task_1 " totalMark="0" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="9344" userAttempted="0" version="17" />
    <item id="3523P3575" name="Writing Famtest_Task_1" totalMark="5" userMark="0" actualUserMark="0" markingType="1" markerUserMark="" viewingTime="5164" userAttempted="0" version="40" />
    <item id="3523P3591" name="Writing Famtest_intro_task_2" totalMark="0" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="7176" userAttempted="0" version="18" />
    <item id="3523P3777" name="Writing Famtest_Task_2" totalMark="5" userMark="0" actualUserMark="0" markingType="1" markerUserMark="" viewingTime="5195" userAttempted="0" version="33" />
    <item id="3523P3592" name="Writing Famtest_intro_task_3" totalMark="0" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="22870" userAttempted="0" version="18" />
    <item id="3523P3579" name="Writing Famtest_Task_3" totalMark="5" userMark="0" actualUserMark="0" markingType="1" markerUserMark="" viewingTime="3105" userAttempted="0" version="42" />
    <item id="3523P3593" name="Writing Famtest_intro_task_4" totalMark="0" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="2074" userAttempted="0" version="16" />
    <item id="3523P3577" name="Writing Famtest_Task_4" totalMark="5" userMark="0" actualUserMark="0" markingType="1" markerUserMark="" viewingTime="13448" userAttempted="0" version="32" />
  </section>
  <section id="6" passMark="0" passType="1" name="Speaking" totalMark="4" userMark="0" userPercentage="0" passValue="1" totalTimeSpent="96108" itemsToMark="0">
    <item id="3524P3619" name="Speaking_Fam_intro_task_1" totalMark="0" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="2043" userAttempted="0" version="15" />
    <item id="3524P3600" name="Speaking_Demo_Task_1" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="16072" userAttempted="0" version="49" />
    <item id="3524P3588" name="Speaking_Fam_intro_task_2" totalMark="0" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="5090" userAttempted="0" version="12" />
    <item id="3524P3683" name="Speaking_Demo_Task_2" totalMark="1" userMark="0" actualUserMark="0" markingType="1" markerUserMark="" viewingTime="1040" userAttempted="0" version="48" />
    <item id="3524P3620" name="Speaking_Fam_intro_task_3" totalMark="0" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="11088" userAttempted="0" version="11" />
    <item id="3524P3685" name="Speaking_Demo_Task_3" totalMark="1" userMark="0" actualUserMark="0" markingType="1" markerUserMark="" viewingTime="17060" userAttempted="0" version="37" />
    <item id="3524P3621" name="Speaking_Fam_intro_task_4" totalMark="0" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="9030" userAttempted="0" version="11" />
    <item id="3524P3687" name="Speaking_Demo_Task_4" totalMark="1" userMark="0" actualUserMark="0" markingType="1" markerUserMark="" viewingTime="12150" userAttempted="0" version="28" />
    <item id="3524P4062" name="Finish Page " totalMark="0" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="22535" userAttempted="0" version="19" />
  </section>
</exam>'
where keycode in ('XAN43M01');

*/


/*
select SPC.IsCompleted, SPCE.IsCompleted, SPC.ScheduledPackageCandidateId, Score
	, PackageScore, SPCE.ScheduledPackageCandidateExamId--, SPCEI.ExternalItemId
	 from STG_BC2_TestPackageManager.dbo.ScheduledPackageCandidateExams as SPCE

	inner join STG_BC2_TestPackageManager.dbo.ScheduledPackageCandidates as SPC
	on SPC.ScheduledPackageCandidateId =  SPCE.Candidate_ScheduledPackageCandidateId

--left join ScheduledPackageCandidateExamItems SPCEI
--on SPCEI.CandidateExam_ScheduledPackageCandidateExamId = SPCE.ScheduledPackageCandidateExamId

where ScheduledExamRef in ('XAN43M01')

*/