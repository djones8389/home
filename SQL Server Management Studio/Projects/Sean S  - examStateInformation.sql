DECLARE @ESID INT = 23766

UPDATE ExamSessionTable
SET previousExamState = ExamState
	, examState = 10
	, examstateInformation = StateChanges.StateInformation
FROM ExamSessionTable
INNER JOIN ( 
	select ExamSessionID
		, StateInformation
	FROM (

	SELECT row_number() OVER(Partition by  newState order by ID desc) R
		, ExamSessionID
		, StateInformation
	FROM [ExamStateChangeAuditTable]
	where examsessionid =  @ESID
		and newState = 10
	) A
	where R = 1
 ) StateChanges
 on StateChanges.ExamSessionID = ExamSessionTable.ID
 where ExamSessionTable.id = @ESID


	


--BEGIN TRAN

--select examstateInformation, previousExamState, examState
--from examsessiontable
--where id = 23766

--UPDATE ExamSessionTable
--SET previousExamState = ExamState
--    , examState = 10
--	, examstateInformation = '<stateChangeInformation>
--							  <type>void</type>
--							  <reason>5</reason>
--							  <description />
--							</stateChangeInformation>'
	
--FROM ExamSessionTable
--where id = 23766


--select examstateInformation, previousExamState, examState
--from examsessiontable
--where id = 23766

--ROLLBACK


--select *
--from [ExamStateChangeAuditTable]
--where examsessionid = 23766