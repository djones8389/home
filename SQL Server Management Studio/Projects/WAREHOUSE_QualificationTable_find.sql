use master

IF OBJECT_ID('tempdb..##DATALENGTH') IS NOT NULL DROP TABLE ##DATALENGTH;

CREATE TABLE ##DATALENGTH (
	client nvarchar(1000)
	, 	[ID] [int]
	,	[QualificationID] [int]
	,	[QualificationName] [nvarchar](100)
	,	[QualificationLevel] [int]
);

DECLARE @dynamic nvarchar(MAX)='';

select @dynamic +=CHAR(13)+ '
use '+QUOTENAME([NAME])+'

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
INSERT ##DATALENGTH
SELECT db_name()
	, *
FROM [dbo].[WAREHOUSE_QualificationTable] WITH (NOLOCK)
where QualificationID <> QualificationLevel

'
from sys.databases
where state_desc = 'ONLINE'
	and name like '%[_]SecureAssess';

exec(@dynamic);



select *
from ##DATALENGTH
order by 1






