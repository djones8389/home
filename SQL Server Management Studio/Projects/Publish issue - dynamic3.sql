use master

if OBJECT_ID('tempdb..#results') is not null drop table #results;

create table #results (

	client nvarchar(100)
	, itemid nvarchar(100)
	, IMediaItemId nvarchar(100)
	, IAssistiveMediaAudioId nvarchar(1000)
	, SCMediaItemId nvarchar(1000)
	, AOMediaItemId nvarchar(1000)
	, AOAdditionalMediaItemId nvarchar(1000)
	, AOAssistiveMediaId nvarchar(1000)
	, SMMediaItemId nvarchar(1000)
	, HSHotSpotMediaItemId nvarchar(1000)
	, CFMediaItemId nvarchar(1000)
)  

declare @dynamic nvarchar(MAX) = ''
select 
 i.id, i.MediaItemId, i.AssistiveMediaAudioId, sc.MediaItemId, ao.MediaItemId, ao.AdditionalMediaItemId, ao.AssistiveMediaAudioId, sm.MediaItemId, hs.HotSpotMediaItemId, cf.MediaItemId

select @dynamic +=CHAR(13) + 
	'
INSERT #results

select '''+name+''',i.id, i.MediaItemId, i.AssistiveMediaAudioId, sc.MediaItemId, ao.MediaItemId, ao.AdditionalMediaItemId, ao.AssistiveMediaAudioId, sm.MediaItemId, hs.HotSpotMediaItemId, cf.MediaItemId
from ['+name+'_ContentAuthor].[dbo].[Items] i
left join ['+name+'_ContentAuthor].[dbo].StemComponents sc
on sc.ItemId = i.id
left join ['+name+'_ContentAuthor].[dbo].AnswerOptions ao
on ao.ItemId = i.id
left join ['+name+'_ContentAuthor].[dbo].SourceMaterials sm
on sm.ItemId = i.id
left join ['+name+'_ContentAuthor].[dbo].HotSpotItems hs
on hs.Id = i.Id
left join ['+name+'_ContentAuthor].[dbo].CandidateFeedbacks cf
on cf.ItemId = i.id


where i.id in (


	SELECT  i.id [ItemID]      
	FROM ['+name+'_ContentAuthor].[dbo].[Subjects] S
	INNER JOIN ['+name+'_ContentAuthor].[dbo].[Items]  I
	ON I.SubjectId = S.Id
	INNER JOIN (
		   SELECT ProjectID
				  , ItemRef
				  , MAX(Version) [Version]
		   FROM '+name+'_ItemBank.[dbo].[ItemTable] 
		   group by ProjectID
				  , ItemRef
		   ) IT
	on IT.ProjectID = S.ProjectId
		   and IT.ItemRef = I.id
	where I.[Status] = 3               /*  Live Items  */                       
		  and I.Version > IT.Version
)
	  '
from (
	select distinct B.name
	FROM (
		select SUBSTRING(name, 0, charindex('_',name)) [Name]
			, ROW_NUMBER() OVER(PARTITION BY SUBSTRING(name, 0, charindex('_',name)) ORDER BY SUBSTRING(name, 0, charindex('_',name)) ) R
		from sys.databases
		where state_desc = 'ONLINE'
			and [Name] like '%/_%' ESCAPE '/'
			and name not like 'nbme%'
		) B
	where R > 3
) a


exec(@dynamic) ;


select *
from #results
order by 1;