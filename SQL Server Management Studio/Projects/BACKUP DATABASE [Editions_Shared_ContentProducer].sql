select 'kill ' + convert(varchar(5),spid)
from sys.sysprocesses s
where db_name(dbid) = 'Editions_Shared_ContentProducer'



BACKUP DATABASE [Editions_Shared_ContentProducer] TO DISK = N'T:\Backup\Editions_Shared_ContentProducer.2017.08.02.bak' 
	WITH NAME = N'Editions_Shared_ContentProducer- Database Backup', COPY_ONLY, NOFORMAT, NOINIT, SKIP, COMPRESSION, NOREWIND, NOUNLOAD, STATS = 10;

--RESTORE DATABASE [SHA_Editions_Shared_ContentProducer]
--FROM DISK = N'T:\Backup\Editions_Shared_ContentProducer.2017.08.02.bak' 
--WITH FILE = 1
--	, MOVE 'Data' TO 'D:\SQL\Data\SHA_Editions_Shared_ContentProducer.mdf'
--	, MOVE 'Log' TO 'D:\SQL\Log\SHA_Editions_Shared_ContentProducer.ldf'
--	, RECOVERY, NOUNLOAD, NOREWIND,  STATS = 5