use master;

IF OBJECT_ID('tempdb..##Target') IS NOT NULL DROP TABLE ##Target;

SELECT  
	ROW_NUMBER() OVER(PARTITION BY SUBSTRING(Name, 0, CHARINDEX('_', Name)) ORDER BY SUBSTRING(Name, 0, CHARINDEX('_', Name)), create_date) R
	, Name as DBName
INTO ##Target
FROM SYS.Databases
where name like '%PRV_NCFE_SecureAssess%'
	and state_desc = 'ONLINE';


exec sp_msforeachdb '

use [?];
		
if (''?'' in (SELECT DBName 
			FROM ##Target 
			WHERE R = 1
			)
   )

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

SET QUOTED_IDENTIFIER ON;

SELECT COUNT(ID)
FROM WAREHOUSE_UserTable
LEFT JOIN (

	SELECT WAREHOUSEUserID FROM WAREHOUSE_ExamSessionTable
	UNION
	SELECT AssignedMarkerUserID FROM WAREHOUSE_ExamSessionTable
	UNION
	SELECT AssignedModeratorUserID FROM WAREHOUSE_ExamSessionTable
	UNION
	SELECT WAREHOUSECreatedBy FROM WAREHOUSE_ScheduledExamsTable
	UNION
	SELECT Warehoused_UploadingUserID FROM WAREHOUSE_ExamSessionDocumentInfoTable
	UNION
	SELECT Warehoused_UserID FROM WAREHOUSE_ExamSessionDurationAuditTable
	UNION
	SELECT 
	  y.value(''userId[1]'', ''int'') AS UserId
	FROM WAREHOUSE_ExamSessionItemResponseTable
	CROSS APPLY MarkerResponseData.nodes(''entries/entry[not(userId/text()=-1)]'') x(y)

) A
 on A.WAREHOUSEUserID = WAREHOUSE_UserTable.ID

WHERE A.WAREHOUSEUserID IS NULL;
'