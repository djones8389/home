select TextData
	, StartTime
	, EndTime
	, Duration
	, CPU
	, Reads
	, Writes
	, ObjectName
	, RowCounts
	,case when ISNUMERIC(substring(cast(textdata as nvarchar(max)), charindex('@examInstanceList=', cast(textdata as nvarchar(max))) + 19, 5)) = 1
	then
	substring(cast(textdata as nvarchar(max)), charindex('@examInstanceList=', cast(textdata as nvarchar(max))) + 19, 5)
	else '' end
	, EventClass
from ::fn_trace_gettable('E:\Users\DaveJ\Downloads\DB_Trace9.trc',1) a
where StartTime >= '2017-10-17 13:24:28.307' and StartTime <= '2017-10-17 13:25:26.600'
	 and EventClass not in (42,45)
order by a.StartTime