USE PRV_OCR_ContentProducer

DECLARE @INSERT nvarchar(MAX) = 'markingSchemeFor="-1" isWordTemplate="F" isSourceItem="F"';

SELECT ID [ProjectID]
	, Name
	, a.b.value('@ID','smallint') [PageID]
	, a.b.value('@markingSchemeFor','smallint') [markingSchemeFor]
	, a.b.value('@isWordTemplate','nvarchar(10)') [isWordTemplate]
	, a.b.value('@isSourceItem','nvarchar(10)') [PageID]
	, cast(a.b.query('.') as nvarchar(MAX))
	, a.b.query('.')
	, REPLACE(cast(a.b.query('.') as nvarchar(MAX)), 'Nam',(@INSERT+'Nam'))
	--, ProjectStructureXml
FROM ProjectListTable
cross apply ProjectStructureXml.nodes ('Pro//Pag') a(b)
where a.b.value('@markingSchemeFor','smallint') is null
	and id = '6029'


<Pag ID="8950" markingSchemeFor="-1" isWordTemplate="F" isSourceItem="F"Nam="00975 F7.1" sta="5" oID="975" quT="10" TemplateType="MCQ_NO_GRAPHIC" alF="0" tes="0" unit="P" LO="F7. Be familiar with the rules for introducing road passenger transport services and the drawing up of transport plans." Assessment_Objective="F7" Assessment_Criteria="F7.1" Question_Type="2" lMU="118" lMD="15/01/2015 14:34:10" TotalMark="1" MarkingType="0" pMU="111" pMD="14/06/2013 10:49:15" pEv="1" orP="7821"/>

SELECT ID
	, ProjectStructureXml
into #ProjectStructureXml
FROM ProjectListTable
WHERE id = '6029'





DECLARE @INSERT nvarchar(MAX) = 'markingSchemeFor="-1" isWordTemplate="F" isSourceItem="F"';
DECLARE @PAGEID int = '7350';

UPDATE #ProjectStructureXml
--set ProjectStructureXml.modify('insert attribute sql:variable("@INSERT") into (/Pro//Pag[@ID=sql:variable("@PAGEID")])[1]')

set ProjectStructureXml.modify('
	insert (              
		   attribute Nam{"01466 H2.1"},
           attribute markingSchemeFor{"-1"},
		   attribute isWordTemplate{"F"}, 
		   attribute isSourceItem{"F"}    
        )             
into (/Pro//Pag[@ID=sql:variable("@PAGEID")])[1]')

SELECT TOP 1 * FROM #ProjectStructureXml




/*
UPDATE #ProjectStructureXml
--set ProjectStructureXml = ''
*/