use master

GRANT VIEW SERVER STATE TO [LON\3168686-Admins]

GRANT VIEW ANY DEFINITION TO [LON\3168686-Admins]

USE [PRV_BritishCouncil_SecureAssess]
CREATE USER [ImranP] FOR LOGIN [ImranP]
exec sp_addrolemember 'db_owner','ImranP'
ALTER USER  [ImranP] WITH DEFAULT_SCHEMA  = dbo;

USE [PRV_BritishCouncil_ItemBank]
CREATE USER [ImranP] FOR LOGIN [ImranP]
exec sp_addrolemember 'db_owner','ImranP'
ALTER USER  [ImranP] WITH DEFAULT_SCHEMA  = dbo;

USE [PRV_BritishCouncil_CPProjectAdmin]
CREATE USER [ImranP] FOR LOGIN [ImranP]
exec sp_addrolemember 'db_owner','ImranP'
ALTER USER  [ImranP] WITH DEFAULT_SCHEMA  = dbo;

--USE [SANDBOX_CandG_SecureAssess]
--ALTER USER  [ImranP] WITH DEFAULT_SCHEMA  = dbo;


USE [SANDBOX_CandG_ContentProducer_001]
CREATE USER [ImranP] FOR LOGIN [ImranP]
exec sp_addrolemember 'db_owner','ImranP'
ALTER USER  [ImranP] WITH DEFAULT_SCHEMA  = dbo;

USE [SANDBOX_CandG_ItemBank_001]
CREATE USER [ImranP] FOR LOGIN [ImranP]
exec sp_addrolemember 'db_owner','ImranP'
ALTER USER  [ImranP] WITH DEFAULT_SCHEMA  = dbo;

--REVOKE VIEW DEFINITION TO [LON\3168686-Admins]
/*

USE master
kill 157
DROP DATABASE [PRV_AAT_ContentProducer]
DROP DATABASE [PRV_AAT_ItemBank]
DROP DATABASE [PRV_AAT_SecureAssess]

exec sp_renamedb 'PPD_AAT_ContentProducer','PRV_AAT_ContentProducer'
exec sp_renamedb 'PPD_AAT_ItemBank','PRV_AAT_ItemBank'
exec sp_renamedb 'PPD_AAT_SecureAssess','PRV_AAT_SecureAssess'


select 'kill ' + convert(nvarchar(10), spid)
from sys.sysprocesses where loginame like '%imran%'

kill 62
kill 65
kill 67
kill 72
kill 75
kill 88
kill 95
kill 96
kill 101
kill 113
kill 114
kill 115
kill 122
kill 125
kill 129
kill 132
kill 136
kill 139
kill 140
kill 142
kill 143
kill 144
kill 146
kill 152

*/