SELECT Name
	, PageID
	, [ComponentCount]
from Projectlisttable PLT
INNER JOIN (
		SELECT A.PageID	
			, COUNT(cast(C as int)) [ComponentCount]
		FROM (
		SELECT [ID]
			, SUBSTRING([ID],0,CHARINDEX('S',[ID])) [PageID]
			, SUBSTRING(ID, CHARINDEX('S', ID) + 1, CHARINDEX('C',ID) - CHARINDEX('S', ID) - 2 + Len('C')) [S]
			, SUBSTRING(ID, CHARINDEX('C', ID) + 1, CHARINDEX('I',ID) - CHARINDEX('C', ID) - 2 + Len('I')) [C]
		FROM [PRV_DEMO_CPProjectAdmin].[dbo].[ItemMultipleChoiceTable] MCQ
		INNER JOIN (
			select 
				cast(ID as nvarchar(10)) + 'P' + cast(project.page.value('@ID','nvarchar(12)') as nvarchar(10)) PageID
			from Projectlisttable
			cross apply ProjectStructureXml.nodes('Pro//Pag') project(page)
			where project.page.value('@sta','int') Between PublishSettingsXML.value('(/PublishConfig/StatusLevelFrom)[1]','int') and PublishSettingsXML.value('(/PublishConfig/StatusLevelTo)[1]','int')

			EXCEPT

			select 
				cast(ID as nvarchar(10)) + 'P' + cast(project.page.value('@ID','nvarchar(12)') as nvarchar(10)) PageID
			from Projectlisttable
			cross apply ProjectStructureXml.nodes('Pro/Rec//Pag') project(page)
			where project.page.value('@sta','int') Between PublishSettingsXML.value('(/PublishConfig/StatusLevelFrom)[1]','int') and PublishSettingsXML.value('(/PublishConfig/StatusLevelTo)[1]','int')
		) P
		   on P.PageID = SUBSTRING([ID],0,CHARINDEX('S',[ID]))
	) A
	GROUP BY PAGEID
) MCQ
on PLT.ID = SUBSTRING(MCQ.PageID, 0, CHARINDEX('P',MCQ.PageID))
order by 1

