--GENERATE DROP SCRIPTS FIRST

--Online

SELECT 'ALTER DATABASE ' + QUOTENAME(Name) + ' SET ONLINE;'
from sys.databases
where database_id > 4
	and state_desc = 'Offline';


--Backup


DECLARE @DefaultLocation TABLE (
     Value NVARCHAR(100)  
     , Data NVARCHAR(100)
) 

INSERT @DefaultLocation
EXEC  master.dbo.xp_instance_regread 
 N'HKEY_LOCAL_MACHINE', N'Software\Microsoft\MSSQLServer\MSSQLServer',N'BackupDirectory'

--DECLARE @Location nvarchar(100) = 'T:\Backup'
DECLARE @Location nvarchar(100) = (SELECT Data from @DefaultLocation)

SELECT N'BACKUP DATABASE ['+name+N'] TO DISK = N''' +  @Location + '\' + 'Utwente' + '.' + CONVERT(VARCHAR(10), GETDATE(), 102) + '.bak'' WITH NAME = N'''+name+N'- Database Backup'', COPY_ONLY, NOFORMAT, NOINIT, SKIP, COMPRESSION, NOREWIND, NOUNLOAD, STATS = 10;'  
FROM sys.databases 
where database_id > 4
ORDER BY name;


--Drop

SELECT 'DROP DATABASE ' + QUOTENAME(Name) + ';'
from sys.databases
where database_id > 4
	and state_desc = 'Offline';