

select *
from sys.dm_db_index_operational_stats(DB_ID(), NULL, NULL, NULL)
where object_id = 1387868011

select [is].user_seeks
	, [is].user_scans
	, [is].user_lookups
	, [is].last_user_seek
	, [is].last_user_scan
	, [is].last_user_update
	, i.name
	
from sys.dm_db_index_usage_stats [is]
inner join sys.indexes i
on i.index_id = [is].index_id
	and i.object_id = [is].object_id
where [is].object_id = 1387868011
	and database_id =  db_ID()

SELECT	distinct
		 DB_NAME() AS [database],
		 OBJECT_NAME(s.OBJECT_ID) AS [table],
		 i.name AS [index],
		 i.index_id,
         s.user_updates,
         s.user_seeks + s.user_scans + s.user_lookups AS [user_selects],
         [Difference] = user_updates - (user_seeks + user_scans + user_lookups)
 FROM		sys.dm_db_index_usage_stats AS s WITH (NOLOCK)
 INNER JOIN sys.indexes AS i WITH (NOLOCK)
		 ON s.[object_id] = i.[object_id]
		AND i.index_id = s.index_id
WHERE	 OBJECTPROPERTY(s.[OBJECT_ID],'IsUserTable') = 1
 AND	 i.index_id > 1
 and OBJECT_NAME(s.OBJECT_ID) = 'ExamSessionTable'
ORDER BY [DIFFERENCE] DESC
OPTION	 (RECOMPILE);



SELECT  deqs.query_hash ,
        deqs.query_plan_hash ,
        cast(deqp.query_plan as xml),
        dest.text
		,deqp.objectid
FROM    sys.dm_exec_query_stats AS deqs
        CROSS APPLY sys.dm_exec_query_plan(deqs.plan_handle) AS deqp
        CROSS APPLY sys.dm_exec_sql_text(deqs.sql_handle) AS dest
WHERE   deqs.query_hash = 0x4C6AB4E2CEA6C834;


select sql_handle
	, plan_handle
	, query_hash
	, query_plan_hash
from sys.dm_exec_query_stats
order by (total_worker_time / execution_count) desc

select 
	 query_plan.value()
from sys.dm_exec_query_plan(0x06009500B1621A1540E1C4B8080000000000000000000000)
--cross apply query_plan.nodes('ShowPlanXML/BatchSequence/Batch/Statements/StmtSimple') a(b)

/*
select *
from sys.dm_exec_query_stats
order by (total_worker_time / execution_count) desc



select *
from sys.dm_exec_sql_text(0x06009500CD969F0E40A131A7070000000000000000000000)

select a.*
	, d.name
from sys.dm_exec_sql_text(0x06009500B1621A1540E1C4B8080000000000000000000000) a
inner join sys.databases d
on d.database_id = a.dbid


--0x02000000B1621A1593B980D66015F9890BF381F09921C49F	0x06009500B1621A1540E1C4B8080000000000000000000000


SELECT TOP 5 query_stats.query_hash AS "Query Hash", 
    SUM(query_stats.total_worker_time) / SUM(query_stats.execution_count) AS "Avg CPU Time",
    MIN(query_stats.statement_text) AS "Statement Text"
 , CASE dbid WHEN 32767 THEN 'Resource' ELSE DB_NAME(dbid) END AS DBName
 , OBJECT_NAME(objectid, dbid)
FROM 
    (SELECT QS.*, st.dbid, st.objectid,
    SUBSTRING(ST.text, (QS.statement_start_offset/2) + 1,
    ((CASE statement_end_offset 
        WHEN -1 THEN DATALENGTH(ST.text)
        ELSE QS.statement_end_offset END 
            - QS.statement_start_offset)/2) + 1) AS statement_text
     FROM sys.dm_exec_query_stats AS QS
     CROSS APPLY sys.dm_exec_sql_text(QS.sql_handle) as ST) as query_stats
GROUP BY query_stats.query_hash, dbid, objectid
ORDER BY 2 DESC;
*/