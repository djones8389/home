--ALTER PROCEDURE [dbo].[sa_CANDIDATEEXAMSTATEMANAGEMENTSERVICE_SetStructureXmlForCandidate_sp]
DECLARE
	-- Add the parameters for the stored procedure here    
	@examInstance INT = 0
	,@examInstanceList NVARCHAR(max) ='1408546'
	,@newExamState INT =2
	,@isInvigilated BIT =1
	,@isHumanMarked BIT =0
	,@isAutoVerified BIT = 1
	,@advanceContentDownloadTimespanInHours INT = 60
	,@structureXml XML = convert(xml,N'<assessmentDetails><assessmentID>1773</assessmentID><qualificationID>84</qualificationID><qualificationName>Costs and revenues (AQ2013)</qualificationName><qualificationReference>AATQCF</qualificationReference><assessmentGroupName>CSTR (AQ2013)</assessmentGroupName><assessmentGroupID>1670</assessmentGroupID><assessmentGroupReference>CACSTR</assessmentGroupReference><assessmentName>Cost and revenues (AQ2013) - Services</assessmentName><validFromDate>01 Oct 2013</validFromDate><expiryDate>31 Dec 2017</expiryDate><startTime>00:00:00</startTime><endTime>23:59:59</endTime><duration>150</duration><defaultDuration>150</defaultDuration><scheduledDuration><value>150</value><reason/></scheduledDuration><sRBonus>0</sRBonus><sRBonusMaximum>50</sRBonusMaximum><externalReference>CBTAAT</externalReference><passLevelValue>70</passLevelValue><passLevelType>1</passLevelType><status>2</status><testFeedbackType><passFail>0</passFail><percentageMark>0</percentageMark><allowSummaryFeedback>0</allowSummaryFeedback><summaryFeedbackType>1</summaryFeedbackType><itemSummary>0</itemSummary><itemReview>0</itemReview><itemReviewCorrectAnswer>0</itemReviewCorrectAnswer><itemFeedback>0</itemFeedback><printableSummary>0</printableSummary><candidateDetails>0</candidateDetails><feedbackByReference>0</feedbackByReference><allowFeedbackDuringExam>0</allowFeedbackDuringExam><allowFeedbackDuringSummary>0</allowFeedbackDuringSummary></testFeedbackType><itemFeedback>0</itemFeedback><allowCalculator>0</allowCalculator><lastInUseDate>17 Sep 2013 11:29:03</lastInUseDate><completedScriptReview>0</completedScriptReview><assessment currentSection="0" totalTime="0" currentTime="0"><intro id="0" name="Introduction" currentItem=""><item id="910P1340" name="Live introduction" totalMark="1" version="8" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" LO="Introduction" unit="Costs and Revenues"/></intro><outro id="0" name="Finish" currentItem=""/><tools id="0" name="Tools" currentItem=""/><section id="1" name="1" passLevelValue="70" passLevelType="1" itemsToMark="0" currentItem="" fixed="0"><item id="910P936" name="01 CSTR LN002 Serv" totalMark="16" version="25" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" LO="01. Inventory control" unit="Costs and Revenues" quT="11,20"/><item id="910P948" name="02 CSTR LN004 Serv" totalMark="16" version="26" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" LO="02. Cost accounting journal entries" unit="Costs and Revenues" quT="11"/><item id="910P959" name="03 CSTR LN010 Serv" totalMark="12" version="22" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" LO="03. Calculation of direct labour costs" unit="Costs and Revenues" quT="11"/><item id="910P968" name="04 CSTR LN004 Serv" totalMark="18" version="45" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" LO="04. Overhead absorption/Choice of costing principles" unit="Costs and Revenues" quT="20"/><item id="910P971" name="05 CSTR LN002 Serv" totalMark="15" version="26" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" LO="05. Overhead absorption/Choice of costing principles" unit="Costs and Revenues" quT="10,11,12"/><item id="910P985" name="06 CSTR LN004 Serv" totalMark="20" version="30" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" LO="06. Activity effects/segmental reporting" unit="Costs and Revenues" quT="20"/><item id="910P996" name="07 CSTR LN010 Serv" totalMark="16" version="29" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" LO="07. Break-even (C-V-P) analysis" unit="Costs and Revenues" quT="11"/><item id="910P1006" name="08 CSTR LN010 Serv" totalMark="16" version="23" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" LO="08. Limiting factor decision making/Types of costing systems" unit="Costs and Revenues" quT="20"/><item id="910P1015" name="09 CSTR LN004 Serv" totalMark="16" version="15" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" LO="09. Variance analysis" unit="Costs and Revenues" quT="11"/><item id="910P1025" name="10 CSTR LN005 Serv" totalMark="20" version="38" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" LO="10. Capital investment appraisal" unit="Costs and Revenues" quT="20"/></section></assessment><isValid>1</isValid><isPrintable>0</isPrintable><numberOfGenerations>1</numberOfGenerations><requiresInvigilation>1</requiresInvigilation><advanceContentDownloadTimespan>60</advanceContentDownloadTimespan><automaticVerification>1</automaticVerification><offlineMode>0</offlineMode><requiresValidation>0</requiresValidation><requiresSecureClient>1</requiresSecureClient><examType>0</examType><advanceDownload>0</advanceDownload><gradeBoundaryData><gradeBoundaries passLevelType="1" adjustedGradeBoundaries="0"><grade modifier="lt" value="70" description="Fail"/><grade modifier="gt" value="70" description="Pass"/></gradeBoundaries></gradeBoundaryData><autoViewExam>0</autoViewExam><autoProgressItems>0</autoProgressItems><confirmationText><confirmationText>If you accept these terms, you are confirming that the CBA submission is your own unaided work and you will not distribute, reproduce or circulate AAT assessment material. Once you have accepted, you will be able to start your assessment.</confirmationText></confirmationText><requiresConfirmationCheck>1</requiresConfirmationCheck><allowCloseWithoutSubmit>0</allowCloseWithoutSubmit><useSecureMarker>0</useSecureMarker><annotationVersion>0</annotationVersion><allowPackagingDelivery>1</allowPackagingDelivery><scoreBoundaryData><scoreBoundaries showScoreBoundaryColumn="1"><score modifier="lt" value="36" description="Significantly below" higherBoundarySet="1"/><score modifier="lt" value="36" description="Significantly below" higherBoundarySet="0"/><score modifier="gt" value="36" description="Below requirement" higherBoundarySet="1"/><score modifier="gt" value="60" description="Borderline" higherBoundarySet="1"/><score modifier="gt" value="70" description="Met" higherBoundarySet="0"/><score modifier="gt" value="70" description="Met" higherBoundarySet="1"/><score modifier="gt" value="36" description="Below requirement" higherBoundarySet="0" userCreated="1"/><score modifier="gt" value="60" description="Borderline" higherBoundarySet="0" userCreated="1"/><score modifier="gt" value="85" description="Exceeded" higherBoundarySet="1" userCreated="1"/></scoreBoundaries></scoreBoundaryData><showCandidateReportResultColumn>0</showCandidateReportResultColumn><showCandidateReportPercentageColumn>1</showCandidateReportPercentageColumn><certifiedAccessible>0</certifiedAccessible><markingProgressVisible>1</markingProgressVisible><markingProgressMode>0</markingProgressMode><secureClientOperationMode>1</secureClientOperationMode><examDeliverySystemStyleType>delivery</examDeliverySystemStyleType><markingAutoVoidPeriod>365</markingAutoVoidPeriod><showPageRequiresScrollingAlert>0</showPageRequiresScrollingAlert><requiresVersionAvailability>0</requiresVersionAvailability><versionValidFrom>01 Oct 2013</versionValidFrom><versionExpiryDate>31 Dec 2017</versionExpiryDate><nonStructuredData><preCaching>0</preCaching><isProjectBasedTest>0</isProjectBasedTest><canExtendTime>0</canExtendTime><attemptAutoSubmit>1</attemptAutoSubmit><awaitingUploadPeriod>31</awaitingUploadPeriod><automaticCreatePin>0</automaticCreatePin><certifiedForTabletDelivery>0</certifiedForTabletDelivery><strictControlOnDDA>0</strictControlOnDDA><noOfResitsAllowed/><itembankstyleprofileVersion>1</itembankstyleprofileVersion><candidateDetailsTime>0</candidateDetailsTime><ndaTime>0</ndaTime><showCandidateDetailsPage>1</showCandidateDetailsPage><showScoreReport>0</showScoreReport><showPrintScoreReportButton>0</showPrintScoreReportButton><enableLogging>0</enableLogging><suppressResultsScreenMark>0</suppressResultsScreenMark><suppressResultsScreenPercent>0</suppressResultsScreenPercent><suppressResultsScreenResult>0</suppressResultsScreenResult><disableReportingCandidateReport>0</disableReportingCandidateReport><disableReportingSummaryReport>0</disableReportingSummaryReport><disableReportingCandidateBreakdownReport>0</disableReportingCandidateBreakdownReport><disableReportingExamBreakdownReport>0</disableReportingExamBreakdownReport><disableReportingResultSlip>0</disableReportingResultSlip></nonStructuredData><project ID="910"/></assessmentDetails>')
	
BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from    
	-- interfering with SELECT statements.    
	SET NOCOUNT ON;

	DECLARE @errorReturnString NVARCHAR(max)
	DECLARE @errorNum NVARCHAR(100)
	DECLARE @errorMess NVARCHAR(max)
	DECLARE @changedDuration INT
	DECLARE @changedDurationReason NVARCHAR(max)
	DECLARE @changedDurationReasonID INT
	DECLARE @expiryDate DATETIME
	DECLARE @myExamName NVARCHAR(200)
	DECLARE @myExamRef NVARCHAR(100)
	DECLARE @myExamVersionName NVARCHAR(100)
	DECLARE @myExamVersionRef NVARCHAR(100)
	DECLARE @myExamSessionID INT
	DECLARE @StructureXMLWithQR AS XML
	DECLARE @StructureXMLWithOutQR AS XML
	DECLARE @nonStructuredData XML
	DECLARE @CanExtendTime BIT
	DECLARE @StrictControlOnDDA BIT
	DECLARE @PreCachingDefault BIT
	DECLARE @MarkingAutoVoidPeriod INT
	DECLARE @NoOfResitsAllowed INT
	DECLARE @CandidateDetailsTime INT
	DECLARE @NdaTime INT
	DECLARE @ShowCandidateDetailsPage BIT
	DECLARE @ItemBankStyleProfileID INT
	DECLARE @ItemBankStyleProfileVersion INT
	DECLARE @ShowScoreReport BIT
	DECLARE @ShowPrintScoreReportButton BIT
	DECLARE @SecureAssessStyleProfileID INT
	DECLARE @isPrintable VARCHAR(1);
	DECLARE @ScheduledDuration INT;
	DECLARE @srDefault INT;
	DECLARE @srMaximum INT;
	DECLARE @defaultDuration INT;
	DECLARE @advanceDownload BIT;
	DECLARE @allowPackageDelivery BIT;
	DECLARE @ExportToSecureMarker BIT;
	DECLARE @ContainsBTLOffice INT;
	DECLARE @IsProjectBased BIT;
	DECLARE @AttemptAutoSubmitForAWaitingUpload BIT;
	DECLARE @AwaitingUploadGracePeriodInDays INT;
	DECLARE @automaticCreatePin BIT;
	DECLARE @CertifiedForTablet INT;
	DECLARE @EnableLogging INT;
	DECLARE @SuppressResultsScreenMark BIT;
	DECLARE @SuppressResultsScreenPercent BIT;
	DECLARE @SuppressResultsScreenResult BIT;
	DECLARE @DisableReportingCandidateReport BIT;
	DECLARE @DisableReportingSummaryReport BIT;
	DECLARE @DisableReportingCandidateBreakdownReport BIT;
	DECLARE @DisableReportingExamBreakdownReport BIT;
	DECLARE @DisableReportingResultSlip BIT;

	BEGIN TRY
		IF @examInstance <> 0
		BEGIN
			SET @examInstanceList = @examInstance
		END
	
		-- Note: This Sproc will not only set the structure Xml for the specified exam instance.    
		-- If the exam instance is part of a shared group then we need to set the structure for    
		-- all candidates as all their structures will be identical. I.e. We're setting the structure    
		-- for all exam instances with a common scheduled exam parent to the specifed instance.
		
		--Extract all the structureXml items together.
		select
			@expiryDate =
				CASE @structureXml.value('/assessmentDetails[1]/requiresVersionAvailability[1]/text()[1]', 'bit')
					WHEN 0
					THEN CONVERT(DATETIME, @structureXml.value('/assessmentDetails[1]/expiryDate[1]/text()[1]', 'varchar(15)'))
					ELSE CONVERT(DATETIME, @structureXml.value('/assessmentDetails[1]/versionExpiryDate[1]/text()[1]', 'varchar(15)'))
				END,
			@myExamName = @structureXml.value('/assessmentDetails[1]/assessmentGroupName[1]/text()[1]','Nvarchar(max)'),
			@myExamRef = @structureXml.value( '/assessmentDetails[1]/assessmentGroupReference[1]/text()[1]','Nvarchar(max)'),
			@myExamVersionName = @structureXml.value( '/assessmentDetails[1]/assessmentName[1]/text()[1]','Nvarchar(max)'),
			@myExamVersionRef = @structureXml.value( '/assessmentDetails[1]/externalReference[1]/text()[1]','Nvarchar(max)'),
			@MarkingAutoVoidPeriod = @structureXml.value('/assessmentDetails[1]/markingAutoVoidPeriod[1]/text()[1]', 'int'),
			@isPrintable = @structureXml.value('/assessmentDetails[1]/isPrintable[1]/text()[1]', 'varchar(1)'),
			@ScheduledDuration = @structureXml.value('/assessmentDetails[1]/scheduledDuration[1]/value[1]/text()[1]', 'int'),
			@srDefault = @structureXml.value('/assessmentDetails[1]/sRBonus[1]/text()[1]', 'int'),
			@srMaximum = @structureXml.value('/assessmentDetails[1]/sRBonusMaximum[1]/text()[1]', 'int'),
			@defaultDuration = @structureXml.value('/assessmentDetails[1]/defaultDuration[1]/text()[1]', 'int'),
			@advanceDownload = @structureXml.value('/assessmentDetails[1]/advanceDownload[1]/text()[1]', 'bit'),
			@allowPackageDelivery = ISNULL(@structureXml.value('/assessmentDetails[1]/allowPackagingDelivery[1]/text()[1]', 'bit'), 0),
			@ExportToSecureMarker = ISNULL(@structureXml.value('/assessmentDetails[1]/useSecureMarker[1]/text()[1]', 'bit'), 0),
			@ContainsBTLOffice = @structureXml.value('count(/assessmentDetails[1]/assessment[1]/section/item[@BTLOffice = 1])', 'int'),
			@nonStructuredData = @structureXml.query('/assessmentDetails[1]/nonStructuredData[1]')

		-- extra nodes from itembank are invalid for structureXML column
		SET @structureXml.modify('delete /assessmentDetails/requiresVersionAvailability')
		SET @structureXml.modify('delete /assessmentDetails/versionExpiryDate')
		SET @structureXml.modify('delete /assessmentDetails/versionValidFrom')

		--store nonStructure separately before deleting from structureXml			
		SET @structureXml.modify('delete /assessmentDetails/nonStructuredData')
		
		-- Create versions of the structure XML with and without quality review set to 1
		SELECT @StructureXMLWithQR = @structureXML
		SELECT @StructureXMLWithOutQR = @structureXML

		SET @StructureXMLWithQR.modify('insert element qualityReview {1} after (/assessmentDetails/isPrintable)[1]')
		SET @StructureXMLWithOutQR.modify('insert element qualityReview {0} after (/assessmentDetails/isPrintable)[1]')	
		
		--Extract all the nonStructuredData together.
		Select
			@IsProjectBased = @nonStructuredData.value('/nonStructuredData[1]/isProjectBasedTest[1]/text()[1]', 'bit'),
			@CanExtendTime = @nonStructuredData.value('/nonStructuredData[1]/canExtendTime[1]/text()[1]', 'bit'),
			@StrictControlOnDDA = @nonStructuredData.value('/nonStructuredData[1]/strictControlOnDDA[1]/text()[1]', 'bit'),
			@PreCachingDefault = @nonStructuredData.value('/nonStructuredData[1]/preCaching[1]/text()[1]', 'bit'),
			@NoOfResitsAllowed = @nonStructuredData.value('/nonStructuredData[1]/noOfResitsAllowed[1]/text()[1]', 'int'),
			@CandidateDetailsTime = @nonStructuredData.value('/nonStructuredData[1]/candidateDetailsTime[1]/text()[1]', 'int'),
			@NdaTime = @nonStructuredData.value('/nonStructuredData[1]/ndaTime[1]/text()[1]', 'int'),
			@ShowCandidateDetailsPage = @nonStructuredData.value('/nonStructuredData[1]/showCandidateDetailsPage[1]/text()[1]', 'bit'),
			@ItemBankStyleProfileID = @nonStructuredData.value('/nonStructuredData[1]/itembankstyleprofileID[1]/text()[1]', 'int'),
			@ItemBankStyleProfileVersion = @nonStructuredData.value('/nonStructuredData[1]/itembankstyleprofileVersion[1]/text()[1]', 'int'),
			@ShowScoreReport = @nonStructuredData.value('/nonStructuredData[1]/showScoreReport[1]/text()[1]', 'bit'),
			@ShowPrintScoreReportButton = @nonStructuredData.value('/nonStructuredData[1]/showPrintScoreReportButton[1]/text()[1]', 'bit'),
			@AttemptAutoSubmitForAWaitingUpload = @nonStructuredData.value('/nonStructuredData[1]/attemptAutoSubmit[1]/text()[1]', 'bit'),
			@AwaitingUploadGracePeriodInDays = @nonStructuredData.value('/nonStructuredData[1]/awaitingUploadPeriod[1]/text()[1]', 'int'),
			@automaticCreatePin =  @nonStructuredData.value('/nonStructuredData[1]/automaticCreatePin[1]/text()[1]', 'bit'),
			@CertifiedForTablet = @nonStructuredData.value('/nonStructuredData[1]/certifiedForTabletDelivery[1]/text()[1]', 'int'),
			@EnableLogging = @nonStructuredData.value('/nonStructuredData[1]/enableLogging[1]/text()[1]', 'int'),
			@SuppressResultsScreenMark = @nonStructuredData.value('(//nonStructuredData/suppressResultsScreenMark)[1]', 'bit'),
			@SuppressResultsScreenPercent = @nonStructuredData.value('(//nonStructuredData/suppressResultsScreenPercent)[1]', 'bit'),
			@SuppressResultsScreenResult = @nonStructuredData.value('(//nonStructuredData/suppressResultsScreenResult)[1]', 'bit'),
			@DisableReportingCandidateReport = @nonStructuredData.value('(//nonStructuredData/disableReportingCandidateReport)[1]', 'bit'),
			@DisableReportingSummaryReport = @nonStructuredData.value('(//nonStructuredData/disableReportingSummaryReport)[1]', 'bit'),
			@DisableReportingCandidateBreakdownReport = @nonStructuredData.value('(//nonStructuredData/disableReportingCandidateBreakdownReport)[1]', 'bit'),
			@DisableReportingExamBreakdownReport = @nonStructuredData.value('(//nonStructuredData/disableReportingExamBreakdownReport)[1]', 'bit'),
			@DisableReportingResultSlip = @nonStructuredData.value('(//nonStructuredData/disableReportingResultSlip)[1]', 'bit')
		
		SET @SecureAssessStyleProfileID = (SELECT ID FROM StyleProfileTable WHERE IBStyleProfileID = @ItemBankStyleProfileID AND VersionNumber = @ItemBankStyleProfileVersion)

		--Store ScheduledExamIds Once rather than making several calls
		DECLARE @ScheduledIds TABLE (
			[ScheduledExamID] INT NOT NULL,
			[ExamSessionId] INT,
			[UserID] int,
			examId int,
			ChangedDuration INT,
			ChangedDurationReason NVarchar(max),
			ChangedDurationReasonID int,
			CorrectXML XML,
			PreCachingAvailable BIT
		)
		
		INSERT INTO @ScheduledIds (
			ScheduledExamID,
			[ExamSessionId],
			UserID,
			examId,
			ChangedDuration,
			ChangedDurationReason,
			ChangedDurationReasonID,
			CorrectXML,
			PreCachingAvailable
		)
		SELECT
			est.[ScheduledExamID],
			est.[ID],
			est.UserID,
			examId,
			est.ScheduledDurationXml.value('./duration[1]/value[1]/text()[1]', 'int'),
			est.ScheduledDurationXml.value('./duration[1]/reason[1]/text()[1]', 'nvarchar(max)'),
			est.ScheduledDurationXml.value('./duration[1]/reason[1]/@ID', 'INT'),
			CASE sett.qualityReview
				WHEN 0
				THEN @StructureXMLWithOutQR
				ELSE 
					CASE [dbo].[fn_CheckUserHasPermissionAtCentre](est.UserID, 30, sett.CentreID)
						WHEN 1
						THEN @StructureXMLWithQR
						ELSE @StructureXMLWithoutQR
					END
			END as	CorrectXML,
			dbo.fn_CheckPreCachingIsEnabled(sett.scheduledForInvigilate, est.pinNumber, @PreCachingDefault) as PreCachingAvailable
		FROM [ExamSessionTable] as est
		INNER JOIN ScheduledExamsTable sett ON est.ScheduledExamID = sett.ID
		WHERE [ScheduledExamID] IN (
				SELECT [ScheduledExamID]
				FROM [ExamSessionTable]
				INNER JOIN ParmsToList(@examInstanceList) p ON p.value = ID
			)

		BEGIN TRAN

			UPDATE [dbo].[ExamSessionTable]
			SET [StructureXml] = s.CorrectXML
				,[examState] = @newExamState
				,[previousExamState] = [examState]
				,isPrintable = @isPrintable
				,ScheduledDuration = @ScheduledDuration
				,srDefault = @srDefault
				,srMaximum = @srMaximum
				,defaultDuration = @defaultDuration
				,advanceDownload = @advanceDownload
				,allowPackageDelivery = @allowPackageDelivery
				,ExportToSecureMarker = @ExportToSecureMarker
				,ContainsBTLOffice = @ContainsBTLOffice
				,ExpiryDate = @expiryDate
				,IsProjectBased = @IsProjectBased
				,AttemptAutoSubmitForAWaitingUpload = @AttemptAutoSubmitForAWaitingUpload
				,AwaitingUploadGracePeriodInDays = @AwaitingUploadGracePeriodInDays
				,automaticCreatePin = @automaticCreatePin
				,CertifiedForTablet = @CertifiedForTablet
				,PreCachingDefault = @PreCachingDefault
				,PreCachingAvailable = s.PreCachingAvailable
				,MarkingAutoVoidPeriod = @MarkingAutoVoidPeriod
				,AutoVoidDate = DATEADD(day, @MarkingAutoVoidPeriod, GETDATE())
				,ItemBankStyleProfileID = @ItemBankStyleProfileID
				,ItemBankStyleProfileVersion = @ItemBankStyleProfileVersion
				,SecureAssessStyleProfileID = @SecureAssessStyleProfileID
				,EnableLogging= @EnableLogging
			FROM ExamSessionTable est
			INNER join @ScheduledIds as S on S.ExamSessionId = est.ID;

			UPDATE [dbo].[ScheduledExamsTable]
			SET [invigilated] = @isInvigilated
				,[humanMarked] = @isHumanMarked
				,[AdvanceContentDownloadTimespanInHours] = @advanceContentDownloadTimespanInHours
				,[autoVerify] = @isAutoVerified
				,[groupState] = 2
				,[examName] = @myExamName
				,[examRef] = @myExamRef
				,[examVersionName] = @myExamVersionName
				,[examVersionRef] = @myExamVersionRef
				,CanExtendTime = @CanExtendTime
				,StrictControlOnDDA = @StrictControlOnDDA
				,CandidateDetailsTime = @CandidateDetailsTime
				,NdaTime = @NdaTime
				,ShowCandidateDetailsPage = @ShowCandidateDetailsPage
				,ShowScoreReport = @ShowScoreReport
				,ShowPrintScoreReportButton = @ShowPrintScoreReportButton
				,SuppressResultsScreenMark = @SuppressResultsScreenMark
				,SuppressResultsScreenPercent = @SuppressResultsScreenPercent
				,SuppressResultsScreenResult = @SuppressResultsScreenResult
				,DisableReportingCandidateReport = @DisableReportingCandidateReport
				,DisableReportingSummaryReport = @DisableReportingSummaryReport
				,DisableReportingCandidateBreakdownReport = @DisableReportingCandidateBreakdownReport
				,DisableReportingExamBreakdownReport = @DisableReportingExamBreakdownReport
				,DisableReportingResultSlip = @DisableReportingResultSlip
			FROM [ScheduledExamsTable]
			INNER join @ScheduledIds as S on S.[ScheduledExamID] = [ScheduledExamsTable].ID;


				DECLARE TempExamSessionCursor cursor fast_forward local for
					SELECT
						[ExamSessionId],
						ChangedDuration,
						ChangedDurationReason,
						ChangedDurationReasonID
					FROM @ScheduledIds;
	
				OPEN TempExamSessionCursor;

				FETCH NEXT FROM TempExamSessionCursor INTO @myExamSessionID, @changedDuration, @changedDurationReason, @changedDurationReasonID;
		
				WHILE @@FETCH_STATUS = 0
					
				BEGIN
					EXEC [sa_INVIGILATESERVICE_ModifyDurationForExamInstance_sp] @myExamSessionID
						,@changedDuration
						,@changedDurationReason
						,1
						,@changedDurationReasonID
						,- 1

				FETCH NEXT FROM TempExamSessionCursor INTO @myExamSessionID, @changedDuration, @changedDurationReason, @changedDurationReasonID;

				END;
	
				CLOSE TempExamSessionCursor;	
				DEALLOCATE TempExamSessionCursor;

	

				INSERT [UserExamHistoryResitTrackerTable] (userID, examID, startedCount, noOfResitsAllowed)
				SELECT
					userId,
					examId,
					0 as startedCount,
					@noOfResitsAllowed
				FROM @ScheduledIds A
				where not Exists (	select 1
									from [UserExamHistoryResitTrackerTable]
									where userID = A.UserID
									and examID = A.ExamID
									);
			COMMIT TRAN


		SET @errorReturnString = '<result errorCode="0"><return>true</return></result>'

		SELECT @errorReturnString

		RETURN
	END TRY

	BEGIN CATCH
		ROLLBACK TRANSACTION;

		SET @errorNum = (SELECT ERROR_NUMBER() AS ErrorNumber)
		SET @errorMess = (SELECT ERROR_MESSAGE() AS ErrorMessage)
		SET @errorReturnString = '<result errorCode="2"><return>SQL Error, Number: ' + @errorNum + ' MESSAGE: ' + @errorMess + '</return></result>'

		SELECT @errorReturnString

		RETURN
	END CATCH
END



rollback tran