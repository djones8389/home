USE [Develop-MI-IB]
GO

if object_id(N'dbo.GetAssessmentVersionsForSuppliedStatusList') is not null
	drop function dbo.GetAssessmentVersionsForSuppliedStatusList
GO

CREATE FUNCTION [dbo].[GetAssessmentVersionsForSuppliedStatusList](@groupID int, @statusList varchar(max))  
RETURNS xml  
WITH RETURNS NULL ON NULL INPUT   
  
BEGIN 

    DECLARE @isVersionAvailabilityRequired BIT
    DECLARE @VersionXML XML	
    SELECT  @isVersionAvailabilityRequired=RequiresVersionAvailability FROM dbo.AssessmentGroupTable WHERE ID=@groupID    
   
   IF @isVersionAvailabilityRequired = 1
   BEGIN
	   
	  SET  @VersionXML = 
		  (SELECT id            as "vId",   
		 AssessmentName           as "vName",   
		 ISNULL(CONVERT(varchar(20), LastInUseDate, 113), '') as "LastInUseDate",  
		 AssessmentStatus          AS "vStatus",   
		 ctdfa.TotalDuration AS "defaultDuration",
		 ctdfa.TotalDuration AS "scheduledDuration",
		 DurationMode					AS "durationMode",
		 SRBonus             AS "sRBonus",  
		 SRBonusMaximum           AS "sRBonusMaximum",  
		 AdvanceContentDownloadTimespan       AS "advanceCDT",  
		 RequiresInvigilation as "requiresInvigilation",
		 ISNULL(CONVERT(varchar(11), ValidFrom, 106),'')        AS "ValidFrom",
		 ISNULL(CONVERT(varchar(11),ExpiryDate,106),'')         AS "ValidTo",
		 ISNULL(ProjectDuration, 90)							AS "projectDuration",
		 ISNULL(ProjectSRBonus, 0)								AS "projectSRBonusDefault",
		 ISNULL(ProjectSRBonusMaximum, 0)						AS "projectSRBonusMaximum",
		 PreCaching												AS "preCaching",
		 ISNULL(EnableCandidateBreak, 0)                         AS "enableCandidateBreak",
		 ISNULL(CandidateBreakStyle, 0)                          AS "candidateBreakStyle",
		 ISNULL(ScheduledBreakType,0)                            AS "scheduledBreakType",
		 ISNULL(BreakPooledTime, 0)                              AS "breakPooledTime",
		 ISNULL(MaximumNumberOfBreaksPerSection, 0)              AS "maximumNumberOfBreaksPerSection",
		 ISNULL(TotalBreakTime, 0)                               AS "totalBreakTime",
		 ISNULL(IsUnlimitedBreaks, 0)                            AS "isUnlimitedBreaks",
		 CONVERT(nvarchar(100),Round(AssessmentRules.value('sum(/PaperRules/Section/@BreakDuration)', 'nvarchar(100)'),0)) AS "sectionTotalBreakTime" --rounding to get round of conversion to int		 			 
		 FROM AssessmentTable  
		 cross apply dbo.CalculateTotalDurationForAssessment( AssessmentTable.id) as ctdfa
		 WHERE AssessmentGroupID = @groupID   
		 AND [IsValid] = 1  
		 AND [AssessmentTable].[AssessmentStatus] IN (select cast(value as int) from dbo.ParmsToList(@statusList)) 
         --AND CONVERT(varchar(11),ExpiryDate,106) >=CONVERT(varchar(11),GetDate() ,106) 
         AND DateDiff ( d, GetDate(), ExpiryDate) >= 0
		 FOR XML PATH('version'))  
   END
   ELSE 
   BEGIN

	   SET  @VersionXML = 
		  (SELECT id            as "vId",   
		 AssessmentName           as "vName",   
		 ISNULL(CONVERT(varchar(20), LastInUseDate, 113), '') as "LastInUseDate",  
		 AssessmentStatus          AS "vStatus",   
		 ctdfa.TotalDuration AS "defaultDuration",
		 ctdfa.TotalDuration AS "scheduledDuration",
		 DurationMode					AS "durationMode",
		 SRBonus             AS "sRBonus",  
		 SRBonusMaximum           AS "sRBonusMaximum",  
		 AdvanceContentDownloadTimespan       AS "advanceCDT",  
		 RequiresInvigilation as "requiresInvigilation",
		 ISNULL(CONVERT(varchar(11), ValidFrom, 106),'')        AS "ValidFrom",
		 ISNULL(CONVERT(varchar(11),ExpiryDate,106),'')         AS "ValidTo",
		 ISNULL(ProjectDuration, 90)							AS "projectDuration",
		 ISNULL(ProjectSRBonus, 0)								AS "projectSRBonusDefault",
		 ISNULL(ProjectSRBonusMaximum, 0)						AS "projectSRBonusMaximum",
		 PreCaching												AS "preCaching",
		 ISNULL(EnableCandidateBreak, 0)                         AS "enableCandidateBreak",
		 ISNULL(CandidateBreakStyle, 0)                          AS "candidateBreakStyle",
		 ISNULL(ScheduledBreakType,0)                            AS "scheduledBreakType",
		 ISNULL(BreakPooledTime, 0)                              AS "breakPooledTime",
		 ISNULL(MaximumNumberOfBreaksPerSection, 0)              AS "maximumNumberOfBreaksPerSection",
		 ISNULL(TotalBreakTime, 0)                               AS "totalBreakTime",
		 ISNULL(IsUnlimitedBreaks, 0)                            AS "isUnlimitedBreaks",
		 CONVERT(nvarchar(100),Round(AssessmentRules.value('sum(/PaperRules/Section/@BreakDuration)', 'nvarchar(100)'),0)) AS "sectionTotalBreakTime" --rounding to get round of conversion to int		 			 
		 FROM AssessmentTable  
		 cross apply dbo.CalculateTotalDurationForAssessment( AssessmentTable.id) as ctdfa
		 WHERE AssessmentGroupID = @groupID   
		 AND [IsValid] = 1  
		 AND [AssessmentTable].[AssessmentStatus] IN (select cast(value as int) from dbo.ParmsToList(@statusList))  
		 FOR XML PATH('version'))  
    END
          
    RETURN @VersionXML
END


GO