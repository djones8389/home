USE [Develop-MI-IB]
GO

if object_id(N'dbo.GetAssessmentVersionsForSuppliedStatusList') is not null
	drop function dbo.GetAssessmentVersionsForSuppliedStatusList
GO

CREATE FUNCTION [dbo].[GetAssessmentVersionsForSuppliedStatusList](@groupID int, @statusList varchar(max))  
RETURNS xml  
WITH RETURNS NULL ON NULL INPUT   
  
BEGIN 

    DECLARE @isVersionAvailabilityRequired BIT
    DECLARE @VersionXML XML	
    SELECT  @isVersionAvailabilityRequired=RequiresVersionAvailability FROM dbo.AssessmentGroupTable WHERE ID=@groupID    
   
   IF @isVersionAvailabilityRequired = 1
   BEGIN
	   
	  SET  @VersionXML = 
		  (SELECT id            as "vId",   
		 AssessmentName           as "vName",   
		 ISNULL(CONVERT(varchar(20), LastInUseDate, 113), '') as "LastInUseDate",  
		 AssessmentStatus          AS "vStatus",   
		 AssessmentDuration          AS "defaultDuration",  
		 AssessmentDuration          AS "scheduledDuration",
		 DurationMode					AS "durationMode",
		 SRBonus             AS "sRBonus",  
		 SRBonusMaximum           AS "sRBonusMaximum",  
		 AdvanceContentDownloadTimespan       AS "advanceCDT",  
		 RequiresInvigilation as "requiresInvigilation",
		 ISNULL(CONVERT(varchar(11), ValidFrom, 106),'')        AS "ValidFrom",
		 ISNULL(CONVERT(varchar(11),ExpiryDate,106),'')         AS "ValidTo",
		 ISNULL(ProjectDuration, 90)							AS "projectDuration",
		 ISNULL(ProjectSRBonus, 0)								AS "projectSRBonusDefault",
		 ISNULL(ProjectSRBonusMaximum, 0)						AS "projectSRBonusMaximum",
		 PreCaching												AS "preCaching"
		 FROM AssessmentTable  
		 WHERE AssessmentGroupID = @groupID   
		 AND [IsValid] = 1  
		 AND [AssessmentTable].[AssessmentStatus] IN (select cast(value as int) from dbo.ParmsToList(@statusList)) 
         --AND CONVERT(varchar(11),ExpiryDate,106) >=CONVERT(varchar(11),GetDate() ,106) 
         AND DateDiff ( d, GetDate(), ExpiryDate) >= 0
		 FOR XML PATH('version'))  
   END
   ELSE 
   BEGIN

	   SET  @VersionXML = 
		  (SELECT id            as "vId",   
		 AssessmentName           as "vName",   
		 ISNULL(CONVERT(varchar(20), LastInUseDate, 113), '') as "LastInUseDate",  
		 AssessmentStatus          AS "vStatus",   
		 AssessmentDuration          AS "defaultDuration",  
		 AssessmentDuration          AS "scheduledDuration",  
		 DurationMode					AS "durationMode",
		 SRBonus             AS "sRBonus",  
		 SRBonusMaximum           AS "sRBonusMaximum",  
		 AdvanceContentDownloadTimespan       AS "advanceCDT",  
		 RequiresInvigilation as "requiresInvigilation",
		 ISNULL(CONVERT(varchar(11), ValidFrom, 106),'')        AS "ValidFrom",
		 ISNULL(CONVERT(varchar(11),ExpiryDate,106),'')         AS "ValidTo",
		 ISNULL(ProjectDuration, 90)							AS "projectDuration",
		 ISNULL(ProjectSRBonus, 0)								AS "projectSRBonusDefault",
		 ISNULL(ProjectSRBonusMaximum, 0)						AS "projectSRBonusMaximum",
		 PreCaching												AS "preCaching"
		 FROM AssessmentTable  
		 WHERE AssessmentGroupID = @groupID   
		 AND [IsValid] = 1  
		 AND [AssessmentTable].[AssessmentStatus] IN (select cast(value as int) from dbo.ParmsToList(@statusList))  
		 FOR XML PATH('version'))  
    END
          
    RETURN @VersionXML
END



GO