SELECT [deadlock_id]
      ,[deadlock_date]
      ,[server_name]
      ,[deadlock_xml]
	  --, a.b.value('.[1]','nvarchar(1000)') as victimSQL
	   , [deadlock_xml].value('(/TextData/deadlock-list/deadlock/@victim)[1]','nvarchar(1000)') as victimProcessID
	  , c.d.value('(.)[1]', 'nvarchar(MAX)') as victimSQL
  FROM [DBADeadlocks].[dbo].[deadlock_emails]

  CROSS APPLY deadlock_xml.nodes('TextData/deadlock-list') a(b)
  CROSS APPLY a.b.nodes('deadlock/process-list/process/executionStack') c(d)

  where CONVERT(nvarchar(MAX), [deadlock_xml])LIKE '%ib3_SetProjectStructure_SP%'