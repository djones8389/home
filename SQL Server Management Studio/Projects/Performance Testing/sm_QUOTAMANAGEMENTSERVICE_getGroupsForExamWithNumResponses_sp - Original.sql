--ALTER PROCEDURE [dbo].[sm_QUOTAMANAGEMENTSERVICE_getGroupsForExamWithNumResponses_sp]
SET STATISTICS IO ON

DECLARE	@examId INT = 8
--AS
BEGIN
    SET NOCOUNT ON;
    
    SELECT GD.ID          AS 'ID'
          ,GD.Name        AS 'NAME'
          ,(
               SELECT CAST(
                          CASE 
                               WHEN (
                                        (
                                            SELECT COUNT(*)
                                            FROM   dbo.Items I
                                                   INNER JOIN dbo.GroupDefinitionItems GDI ON  GDI.ItemID = I.ID
                                                   INNER JOIN dbo.GroupDefinitions GDF ON  GDF.ID = GDI.GroupID
                                            WHERE  I.MarkingType = 1
                                                   AND GDF.ID = GD.ID
                                        )>0
                                    ) THEN 1
                               ELSE 0
                          END AS BIT
                      )
           )              AS 'IsManualMarkingType'
          ,COUNT(UGR.ID)  AS 'NumResponses'
          ,SUM(CASE WHEN UGR.ConfirmedMark IS NULL THEN 0 ELSE 1 END)	 AS 'NumMarkedResponses'
          ,SUM(CASE WHEN UGR.CI=1 AND UGR.CI_Review=0 THEN 1 ELSE 0 END) AS 'NumAwaitingReview'
          ,SUM(
               CASE 
                    WHEN UGR.ConfirmedMark IS NOT NULL
               AND UGR.CI=0
               AND AGM.ID IS NULL THEN 1
                   ELSE 0
                   END
           )              AS 'NumHumanMarkedResponses'        
          ,SUM(CASE WHEN UGR.IsEscalated = 1 THEN 1 ELSE 0 END)             AS 'NumParkedResponses'
    FROM   dbo.GroupDefinitions GD
           LEFT JOIN dbo.UniqueGroupResponses UGR ON  UGR.GroupDefinitionID = GD.ID
                    --detect auto marked UGRs              
           LEFT JOIN dbo.AssignedGroupMarks AGM ON  AGM.UniqueGroupResponseId = UGR.ID
                    AND AGM.MarkingMethodId = 12
                    AND AGM.IsConfirmedMark = 1
                    AND AGM.GroupDefinitionID = GD.ID
    WHERE  GD.ExamID = @examId
           AND GD.ID IN (SELECT  GDSCR.GroupDefinitionID
                         FROM   dbo.GroupDefinitionStructureCrossRef GDSCR
                                INNER JOIN dbo.ExamVersionStructures EVS ON  EVS.ID = GDSCR.ExamVersionStructureID
                         WHERE  GDSCR.Status = 0 -- 0 = Active
                                AND EVS.StatusID = 0 -- 0 = Released
                        )
    GROUP BY
           GD.ID
          ,GD.Name
END
