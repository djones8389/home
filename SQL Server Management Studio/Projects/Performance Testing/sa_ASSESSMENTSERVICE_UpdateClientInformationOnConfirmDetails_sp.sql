--SELECT EST.ID, EST.KeyCode, EST.examState, UT.Forename, UT.Surname, CT.CentreName, SCET.examName, scet.ScheduledStartDateTime, scet.ScheduledEndDateTime, clientInformation
--  FROM ExamSessionTable as EST (NOLOCK)

--  Inner Join ScheduledExamsTable as SCET (NOLOCK) 
--  on SCET.ID = EST.ScheduledExamID
   
--  Inner Join UserTable as UT (NOLOCK)
--  on UT.ID = EST.UserID

--  Inner Join CentreTable as CT (NOLOCK)
--  on CT.ID = SCET.CentreID

--where examstate =4
SET STATISTICS IO ON
--ALTER PROCEDURE [dbo].[sa_ASSESSMENTSERVICE_UpdateClientInformationOnConfirmDetails_sp]  
DECLARE
	@examInstanceId			int = 2070020,  
	@clientInformation		nvarchar(max) =N'<systemConfiguration source="1" web="0"><dotNet><version>2.0.50727.5420 Service Pack 2</version><version>3.0.30729.5420 Service Pack 2</version><version>3.5.30729.5420 Service Pack 1</version></dotNet><flashPlayer><name>Adobe Flash Player 19</name><version>19</version></flashPlayer><flashVersion /><secureClient><CurrentVersion>11004114000</CurrentVersion><Application>not available</Application></secureClient><environment><operatingSystem><version>Microsoft Windows 7 Enterprise </version><servicePack>Service Pack 1.0</servicePack></operatingSystem><processor><name>Intel(R) Core(TM) i3 CPU       M 380  @ 2.53GHz</name><manufacturer>GenuineIntel</manufacturer><cores>2</cores></processor><memory><installedPhysicalMemory>4 GB</installedPhysicalMemory><totalVisibleMemorySize>3.8 GB</totalVisibleMemorySize><availablePhysicalMemory>2.45 GB</availablePhysicalMemory></memory><hardDisk><drives><drive><name>C:</name><totalSize>232.54 GB</totalSize><freeSpace>160.04 GB</freeSpace></drive></drives></hardDisk><video><deviceName>Intel(R) HD Graphics</deviceName><resolution>1366 x 768 x 4294967296 colors</resolution><memory>1721.34 MB</memory></video><soundCard><name>Realtek High Definition Audio</name></soundCard></environment><timeZone><timeZoneName>GMT Standard Time</timeZoneName><daylightSavingTime>1</daylightSavingTime></timeZone><identification><computerName>DC019748</computerName><macAddress>1C:65:9D:C7:3A:D8</macAddress><ipAddress>172.27.16.116</ipAddress><ipAddress>fe80::54ce:baa1:7772:c4f2</ipAddress></identification><windowsservices><running><Name>Adobe Acrobat Update Service</Name><Name>Application Experience</Name><Name>Application Information</Name><Name>Background Intelligent Transfer Service</Name><Name>Base Filtering Engine</Name><Name>Certificate Propagation</Name><Name>CNG Key Isolation</Name><Name>COM+ Event System</Name><Name>Computer Browser</Name><Name>Configuration Manager Remote Control</Name><Name>Cryptographic Services</Name><Name>DCOM Server Process Launcher</Name><Name>Desktop Window Manager Session Manager</Name><Name>DHCP Client</Name><Name>Diagnostic Policy Service</Name><Name>Diagnostic Service Host</Name><Name>Diagnostic System Host</Name><Name>Diagnostics Tracking Service</Name><Name>Distributed Link Tracking Client</Name><Name>DNS Client</Name><Name>Extensible Authentication Protocol</Name><Name>Function Discovery Resource Publication</Name><Name>Group Policy Client</Name><Name>IKE and AuthIP IPsec Keying Modules</Name><Name>IP Helper</Name><Name>IPsec Policy Agent</Name><Name>Microsoft Antimalware Service</Name><Name>Microsoft Network Inspection</Name><Name>Microsoft Policy Platform Local Authority</Name><Name>Multimedia Class Scheduler</Name><Name>Netlogon</Name><Name>Network Connections</Name><Name>Network List Service</Name><Name>Network Location Awareness</Name><Name>Network Store Interface Service</Name><Name>Offline Files</Name><Name>Plug and Play</Name><Name>Portable Device Enumerator Service</Name><Name>Power</Name><Name>Print Spooler</Name><Name>Program Compatibility Assistant Service</Name><Name>Remote Desktop Configuration</Name><Name>Remote Desktop Services</Name><Name>Remote Desktop Services UserMode Port Redirector</Name><Name>Remote Procedure Call (RPC)</Name><Name>RPC Endpoint Mapper</Name><Name>Security Accounts Manager</Name><Name>Security Center</Name><Name>Server</Name><Name>Shell Hardware Detection</Name><Name>SMS Agent Host</Name><Name>Software Protection</Name><Name>SSDP Discovery</Name><Name>Superfetch</Name><Name>System Event Notification Service</Name><Name>Task Scheduler</Name><Name>TCP/IP NetBIOS Helper</Name><Name>Themes</Name><Name>User Profile Service</Name><Name>WebClient</Name><Name>Windows Audio</Name><Name>Windows Audio Endpoint Builder</Name><Name>Windows Event Log</Name><Name>Windows Firewall</Name><Name>Windows Font Cache Service</Name><Name>Windows Installer</Name><Name>Windows Management Instrumentation</Name><Name>Windows Modules Installer</Name><Name>Windows Search</Name><Name>Windows Time</Name><Name>Windows Update</Name><Name>WinHTTP Web Proxy Auto-Discovery Service</Name><Name>WLAN AutoConfig</Name><Name>Workstation</Name></running><stopped><Name>ActiveX Installer (AxInstSV)</Name><Name>Adaptive Brightness</Name><Name>Adobe Flash Player Update Service</Name><Name>Application Identity</Name><Name>Application Layer Gateway Service</Name><Name>Application Management</Name><Name>ASP.NET State Service</Name><Name>BitLocker Drive Encryption Service</Name><Name>Block Level Backup Engine Service</Name><Name>Bluetooth Support Service</Name><Name>BranchCache</Name><Name>COM+ System Application</Name><Name>ConfigMgr Task Sequence Agent</Name><Name>Credential Manager</Name><Name>Disk Defragmenter</Name><Name>Distributed Transaction Coordinator</Name><Name>Encrypting File System (EFS)</Name><Name>Fax</Name><Name>Function Discovery Provider Host</Name><Name>Health Key and Certificate Management</Name><Name>HomeGroup Provider</Name><Name>Human Interface Device Access</Name><Name>Interactive Services Detection</Name><Name>Internet Connection Sharing (ICS)</Name><Name>Internet Explorer ETW Collector Service</Name><Name>KtmRm for Distributed Transaction Coordinator</Name><Name>Link-Layer Topology Discovery Mapper</Name><Name>Media Center Extender Service</Name><Name>Microsoft .NET Framework NGEN v2.0.50727_X64</Name><Name>Microsoft .NET Framework NGEN v2.0.50727_X86</Name><Name>Microsoft .NET Framework NGEN v4.0.30319_X64</Name><Name>Microsoft .NET Framework NGEN v4.0.30319_X86</Name><Name>Microsoft iSCSI Initiator Service</Name><Name>Microsoft Policy Platform Processor</Name><Name>Microsoft Software Shadow Copy Provider</Name><Name>Net.Msmq Listener Adapter</Name><Name>Net.Pipe Listener Adapter</Name><Name>Net.Tcp Listener Adapter</Name><Name>Net.Tcp Port Sharing Service</Name><Name>Network Access Protection Agent</Name><Name>Office  Source Engine</Name><Name>Office Software Protection Platform</Name><Name>Parental Controls</Name><Name>Peer Name Resolution Protocol</Name><Name>Peer Networking Grouping</Name><Name>Peer Networking Identity Manager</Name><Name>Performance Counter DLL Host</Name><Name>Performance Logs &amp; Alerts</Name><Name>PnP-X IP Bus Enumerator</Name><Name>PNRP Machine Name Publication Service</Name><Name>Problem Reports and Solutions Control Panel Support</Name><Name>Protected Storage</Name><Name>Quality Windows Audio Video Experience</Name><Name>Remote Access Auto Connection Manager</Name><Name>Remote Access Connection Manager</Name><Name>Remote Procedure Call (RPC) Locator</Name><Name>Remote Registry</Name><Name>Routing and Remote Access</Name><Name>Secondary Logon</Name><Name>Secure Socket Tunneling Protocol Service</Name><Name>Smart Card</Name><Name>Smart Card Removal Policy</Name><Name>SNMP Trap</Name><Name>SPP Notification Service</Name><Name>Storage Service</Name><Name>Tablet PC Input Service</Name><Name>Telephony</Name><Name>Thread Ordering Server</Name><Name>TPM Base Services</Name><Name>UPnP Device Host</Name><Name>Virtual Disk</Name><Name>Volume Shadow Copy</Name><Name>Windows Activation Technologies Service</Name><Name>Windows Backup</Name><Name>Windows Biometric Service</Name><Name>Windows CardSpace</Name><Name>Windows Color System</Name><Name>Windows Connect Now - Config Registrar</Name><Name>Windows Defender</Name><Name>Windows Driver Foundation - User-mode Driver Framework</Name><Name>Windows Error Reporting Service</Name><Name>Windows Event Collector</Name><Name>Windows Image Acquisition (WIA)</Name><Name>Windows Media Center Receiver Service</Name><Name>Windows Media Center Scheduler Service</Name><Name>Windows Media Player Network Sharing Service</Name><Name>Windows Presentation Foundation Font Cache 3.0.0.0</Name><Name>Windows Remote Management (WS-Management)</Name><Name>Wired AutoConfig</Name><Name>WMI Performance Adapter</Name><Name>WWAN AutoConfig</Name></stopped><other /></windowsservices></systemConfiguration>'

UPDATE ExamSessionTable
set clientInformation = NULL
WHERE ID = 2070020;
	
BEGIN  
	DECLARE @errorReturnString  nvarchar(max)  
	DECLARE @errorNum  nvarchar(100)  
	DECLARE @errorMess  nvarchar(max)  
    DECLARE @currentClientInformation nvarchar(max)  
    DECLARE @myExistingInfo nvarchar(max)  
    DECLARE @myUpdatedInfo xml  
    DECLARE @ExamState nvarchar(20)

	SET NOCOUNT ON;  

	BEGIN TRANSACTION
	
	BEGIN TRY  
		IF (EXISTS(SELECT id FROM [dbo].[ExamSessionTable] WHERE [ID] = @examInstanceId))  
			BEGIN    
				DECLARE @hdoc int  
				EXEC sp_xml_preparedocument @hdoc OUTPUT, @clientInformation  
		       
				DECLARE @myMacAddress nvarchar(max)  
				SET @myMacAddress = (SELECT MacAddress FROM OPENXML(@hdoc, '/', 2) WITH(MacAddress nvarchar(max) '//macAddress'))  
       
				EXEC sp_xml_removedocument @hdoc
		 
				--Update clientInformation here.  
				SELECT
					 @currentClientInformation= CONVERT(nvarchar(max),clientInformation)
						, @ExamState = ExamState
				FROM ExamSessionTable WHERE ID = @examInstanceId


			IF @currentClientInformation IS NULL  
				
				BEGIN  
					UPDATE [dbo].ExamSessionTable    
					SET clientInformation = '<clientInformation>' + @clientInformation +  '</clientInformation>'     
					WHERE ID= @examInstanceId
				END
			ELSE  
			BEGIN  
				DECLARE @myCurrentClientInformationXml XML

				SELECT 
					@myCurrentClientInformationXml = clientInformation 
				FROM ExamSessionTable WHERE ID = @examInstanceId
			
				IF 
					@myMacAddress IS NULL 
					OR 
					@myCurrentClientInformationXml.value('count(clientInformation/systemConfiguration)', 'int') IS NULL
					OR 
					@myCurrentClientInformationXml.exist('//macAddress[text()=sql:variable("@myMacAddress")]') = 0
					BEGIN
						--Removing root node to get actual xml data nodes       
						SET @myExistingInfo =   REPLACE(@currentClientInformation, '<clientInformation>' , '')   
						SET @myExistingInfo =   REPLACE(@myExistingInfo, '</clientInformation>', '')  
				  
						--Concatinating Exsiting & New data and adding Root node on above that.  
						SET @myUpdatedInfo=CONVERT(xml, '<clientInformation>' + @clientInformation + @myExistingInfo + '</clientInformation>')       
		       
						UPDATE [dbo].ExamSessionTable    
						SET clientInformation = @myUpdatedInfo          
						WHERE (ID= @examInstanceId)  
					END
				 
		
			END  -- End ELSE PART for = IF @currentClientInformation IS NULL         

			  

			IF(NOT(@myMacAddress IS NULL))  
		    BEGIN  
				   DECLARE @myCentreID int  
				   SET @myCentreID = (SELECT CentreID FROM ExamSessionTable INNER JOIN ScheduledExamsTable ON ScheduledExamsTable.ID = ExamSessionTable.ScheduledExamID WHERE ExamSessionTable.ID = @examInstanceId)  
  
  --DROP INDEX [IX_MacAddress_CentreID_central] ON [dbo].[MacAddressTrackingTable]
  CREATE NONCLUSTERED INDEX [IX_MacAddress_CentreID_central] ON [dbo].[MacAddressTrackingTable] (
	CentreID, central
  ) INCLUDE (
	 MACAddress
  )
				   DELETE FROM MacAddressTrackingTable  
				   WHERE MacAddress = @myMacAddress AND central = 1 AND CentreID = @myCentreID  
  
				   INSERT INTO [dbo].[MacAddressTrackingTable]  
					   ([MACAddress]  
					   ,[CentreID]  
					   ,[central]  
					   ,[LastUsedDate])  
				   VALUES  
					   (@myMacAddress  
					   ,@myCentreID  
					   ,1  
					   ,GetDate())  
			END 
       
			SET @errorReturnString = '<result errorCode="0"><return><state>' + @ExamState + '</state></return></result>'  
			SELECT @errorReturnString  
			
			
		END
		ELSE  
		BEGIN  
			SET @errorReturnString = '<result errorCode="1"><return>ExamSession ID does not exist</return></result>'  
			SELECT @errorReturnString  
		END   
	END TRY  
	BEGIN CATCH  
  
		SET @errorNum = (SELECT ERROR_NUMBER() AS ErrorNumber)  
		SET @errorMess = (SELECT ERROR_MESSAGE() AS ErrorMessage)  
		SET @errorReturnString = '<result errorCode="2"><return>SQL Error, Number: ' + @errorNum + ' MESSAGE: ' + @errorMess + '</return></result>'  
		SELECT @errorReturnString
	END CATCH  
	
	IF @@ERROR = 0 COMMIT TRANSACTION ELSE ROLLBACK TRANSACTION
	  
END  
  
  
  
  
  
  


