USE [PRV_AAT_SecureAssess]


set statistics io on

DECLARE
 @examState       int = 15,                  
 @filterXml      nvarchar(max) =N' <request><filter pageSize="50" pageIndex="0" filterType="AND">  <i name="qualifications">%</i><i name="centreName">%</i><i name="completionDateStartRange">16/03/2016 00:00:00</i>  <i name="completionDateEndRange">14/07/2016 23:59:59</i><i name="examId"></i><i name="examVersionRef">%</i>  <i name="centreCode">%</i><i name="sorting">completionDate ASC</i><i name="hideMarkedExams">0</i>  <i name="candidateForename">%</i>  <i name="candidateSurname">%</i>  <i name="totalMark"></i>  <i name="totalMarkStartRange" ></i>  <i name="totalMarkEndRange" ></i>  <i name="totalMarkAwarded"></i>  <i name="totalMarkAwardedStartRange" ></i>  <i name="totalMarkAwardedEndRange" ></i>  <i name="percentage"></i>  <i name="percentageStartRange" ></i>  <i name="percentageEndRange" ></i>  <i name="markingProgress"></i>  <i name="markingProgressStartRange" ></i>  <i name="markingProgressEndRange" ></i>  <i name="autoVoidDateStartRange"></i>  <i name="autoVoidDateEndRange"></i>  <i name="userAssociationMarker"></i> <i name="userAssociationModerator"></i> </filter></request> ',                  
 @qualificationIdList   nvarchar(max) =N'',                  
 @centreQualificationLevelsXml nvarchar(max) =N'<list><i level="1" centre="84" editable="1"/><i level="1" centre="83" editable="1"/><i level="1" centre="128" editable="1"/><i level="1" centre="247" editable="1"/><i level="1" centre="150" editable="1"/><i level="1" centre="73" editable="1"/><i level="1" centre="322" editable="1"/><i level="1" centre="222" editable="1"/><i level="1" centre="200" editable="1"/><i level="1" centre="125" editable="1"/><i level="1" centre="78" editable="1"/><i level="1" centre="519" editable="1"/><i level="1" centre="177" editable="1"/><i level="1" centre="120" editable="1"/><i level="1" centre="234" editable="1"/><i level="1" centre="167" editable="1"/><i level="1" centre="130" editable="1"/><i level="1" centre="85" editable="1"/><i level="1" centre="131" editable="1"/><i level="1" centre="86" editable="1"/><i level="1" centre="132" editable="1"/><i level="1" centre="183" editable="1"/><i level="1" centre="133" editable="1"/><i level="1" centre="92" editable="1"/></list>'                  ,
 @assignedQualIdList    nvarchar(max) =N'41,141,147,82,150,33,23,159,22,30,31,19,80,175,100,148,138,42,27,85,144,25,96,24,88,170,164,154,184,160,156,139,142,163,91,103,99,21,84,89,32,185,186,162,187,29,90,28,87,43,86,44,97,224,146,165,176,172,135,78,104,188,93,189,95,145,26,190,83,151,191,79,102,98,20,173,174,153,158,143,166,171,149,155,140,161,152,157,196,81,177,101,70,192'                    
--WITH_ENCRYPTION_REPLACE_ME_FOR_LOCAL--                  
--AS                  
BEGIN                  
 SET NOCOUNT ON;                  
                  
if object_ID('tempdb..#TEMP_CentreQualificationLevels_TABLE') IS NOT NULL DROP TABLE #TEMP_CentreQualificationLevels_TABLE    
if object_ID('tempdb..#TEMP_ExamDetailsSearch_TABLE') IS NOT NULL DROP TABLE #TEMP_ExamDetailsSearch_TABLE              

BEGIN TRY                  
                   
  set dateformat dmy;                  
                     
  DECLARE @spReturnString    nvarchar(max)                  
                    
  /*                  
  paging vars                  
  */                  
  declare @PageSize     int                  
  declare @PageNumber     int                  
  declare @TotalCount     int                   
  declare @TotalResultCount   int                   
  declare @RowStart     int                   
  declare @RowEnd      int                     
                    
  -- select vars                  
  declare @tempTableInsert varchar(max)                   
  declare @filterType     varchar(3)                  
  Declare @sorting     varchar(max)                  
  declare @selectDetails    varchar(max)                  
  declare @whereclause    varchar(max)                  
  declare @pagingwhereclause   varchar(max)                  
  declare @sqlPrePaging    nvarchar(max)                  
  declare @sqlPostPaging    nvarchar(max)                  
  declare @xmlUserSearch    varchar(max)                  
  declare @orderby     varchar(max)           
                    
  --create filter holding variables                  
  DECLARE @centreName     nvarchar(max)                  
  DECLARE @examId     nvarchar(max)                  
  DECLARE @completionDateStartRange nvarchar(max)                  
  DECLARE @completionDateEndRange  nvarchar(max)                  
  DECLARE @centreCode nvarchar(max)                  
  DECLARE @hideMarkedExams nvarchar(max)                  
  DECLARE @examVersionRef NVARCHAR(MAX)  
  
  DECLARE @candidateForename NVARCHAR(MAX)  
  DECLARE @candidateSurname NVARCHAR(MAX)  
  DECLARE @totalMark  NVARCHAR(MAX)  
  DECLARE @totalMarkStartRange NVARCHAR(MAX)  
  DECLARE @totalMarkEndRange NVARCHAR(MAX) 
  DECLARE @TotalMarksAvailableClause NVarchar(max) 
  
  DECLARE @totalMarkAwarded   NVarchar(max) 
  DECLARE @totalMarkAwardedStartRange  NVarchar(max) 
  DECLARE @totalMarkAwardedEndRange  NVarchar(max) 
  DECLARE @totalMarkAwardedClause  NVarchar(max) 
  
  DECLARE @percentage  NVarchar(max) 
  DECLARE @percentageStartRange   NVarchar(max) 
  DECLARE @percentageEndRange  NVarchar(max) 
  DECLARE @percentageClause  NVarchar(max)   
  
  DECLARE @markingProgress   NVarchar(max) 
  DECLARE @markingProgressStartRange   NVarchar(max) 
  DECLARE @markingProgressEndRange   NVarchar(max) 
  DECLARE @markingProgressClause   NVarchar(max)   
   
  DECLARE @autoVoidDateStartRange   NVarchar(max) 
  DECLARE @autoVoidDateEndRange   NVarchar(max) 
  DECLARE @autoVoidDateClause   NVarchar(max)   
  
  DECLARE @userAssociationMarker NVARCHAR(max)
  DECLARE @userAssociationModerator NVARCHAR(max)
   
  
  --Open the filter XML passed into the sp                  
  declare @hdoc  int                  
	                    
  exec sp_xml_preparedocument @hdoc OUTPUT, @filterXml                  
                   
                  
  -- initialise the filter values                  
  --SET filter vars
  SELECT  
  
   @PageNumber = pageIndex,                   
   @PageSize = pageSize,                   
   @filterType = filterType,                  
   @centreName = [centreName],                  
   @examId = [examId],                  
   @completionDateStartRange = [completionDateStartRange],                  
   @completionDateEndRange = [completionDateEndRange],                  
   @sorting = [sorting],                
   @centreCode = [centreCode],        
   @hideMarkedExams = [hideMarkedExams],  
   @examVersionRef = [examVersionRef],
   @candidateForename = [candidateForename],
   @candidateSurname =  [candidateSurname],
   @totalMark  = [totalMark],
   @totalMarkStartRange = [totalMarkStartRange] ,
	@totalMarkEndRange = [totalMarkEndRange],
   @totalMarkAwarded  = [totalMarkAwarded],
   @totalMarkAwardedStartRange = [totalMarkAwardedStartRange] ,
	@totalMarkAwardedEndRange = [totalMarkAwardedEndRange],
   @percentage = [percentage] ,
   @percentageStartRange = [percentageStartRange] ,
   @percentageEndRange = [percentageEndRange] ,   
   @markingProgress = [markingProgress] ,
   @markingProgressStartRange = [markingProgressStartRange]  ,
   @markingProgressEndRange = [markingProgressEndRange] ,    
   @autoVoidDateStartRange = [autoVoidDateStartRange] ,    
   @autoVoidDateEndRange = [autoVoidDateEndRange],
   @userAssociationMarker = [userAssociationMarker],
   @userAssociationModerator = [userAssociationModerator]
   
  FROM                  
  OPENXML(@hdoc, '//filter',1)                  
  WITH (                  
   pageSize int,                  
   pageIndex int,                  
   filterType nvarchar(3),                  
   [centreName] nvarchar(max)  './i[@name="centreName"]',                  
   [examId] nvarchar(max)  './i[@name="examId"]',                  
   [completionDateStartRange] nvarchar(max)  './i[@name="completionDateStartRange"]',                  
   [completionDateEndRange] nvarchar(max)  './i[@name="completionDateEndRange"]',                  
   [sorting] varchar(max)  './i[@name="sorting"]',                  
   [centreCode]  nvarchar(max)  './i[@name="centreCode"]',        
   [hideMarkedExams] nvarchar(max)  './i[@name="hideMarkedExams"]',  
   [examVersionRef] nvarchar(max)  './i[@name="examVersionRef"]',
   [candidateForename] nvarchar(max)  './i[@name="candidateForename"]',
   [candidateSurname] nvarchar(max)  './i[@name="candidateSurname"]',
   [totalMark] nvarchar(max)  './i[@name="totalMark"]',
   [totalMarkStartRange] nvarchar(max)  './i[@name="totalMarkStartRange"]' ,
   [totalMarkEndRange] nvarchar(max)  './i[@name="totalMarkEndRange"]',
   [totalMarkAwarded] nvarchar(max)  './i[@name="totalMarkAwarded"]',
   [totalMarkAwardedStartRange] nvarchar(max)  './i[@name="totalMarkAwardedStartRange"]' ,
   [totalMarkAwardedEndRange] nvarchar(max)  './i[@name="totalMarkAwardedEndRange"]',
   [percentage] nvarchar(max)  './i[@name="percentage"]',
   [percentageStartRange] nvarchar(max)  './i[@name="percentageStartRange"]' ,
   [percentageEndRange] nvarchar(max)  './i[@name="percentageEndRange"]',   
   [markingProgress] nvarchar(max)  './i[@name="markingProgress"]',
   [markingProgressStartRange] nvarchar(max)  './i[@name="markingProgressStartRange"]' ,
   [markingProgressEndRange] nvarchar(max)  './i[@name="markingProgressEndRange"]',    
   [autoVoidDateStartRange] nvarchar(max)  './i[@name="autoVoidDateStartRange"]',    
   [autoVoidDateEndRange] nvarchar(max)  './i[@name="autoVoidDateEndRange"]',
   [userAssociationMarker] nvarchar(max)  './i[@name="userAssociationMarker"]',
   [userAssociationModerator] nvarchar(max)  './i[@name="userAssociationModerator"]'
  )        
                    
                  
  --SET row request vars                  
  SET @RowStart = @PageSize * @PageNumber +1;                  
  SET @RowEnd = @RowStart + @PageSize -1;                  
                  
  exec sp_xml_preparedocument @hdoc OUTPUT, @centreQualificationLevelsXML                  
                    
  set dateformat dmy;                  
                    
  CREATE TABLE #TEMP_CentreQualificationLevels_TABLE                   
  (                  
   [qualLevel] int,                  
   [centreId] int                  
  )                  
         
          
  INSERT INTO #TEMP_CentreQualificationLevels_TABLE                   
  SELECT  *                  
  FROM                  
  OPENXML(@hdoc, '//list/i',1)                  
  WITH (                  
   [level]      int  '@level',                  
   [centre]     int  '@centre'                  
  )                  
                  
  DECLARE @myQualLevelWhereClause nvarchar(max)                  
  SET @myQualLevelWhereClause = ''                  
  IF((SELECT COUNT(DISTINCT qualLevel) FROM #TEMP_CentreQualificationLevels_TABLE WHERE centreId = 1) < (SELECT COUNT([level]) FROM QualificationLevelsTable))                  
   BEGIN                  
    SET @myQualLevelWhereClause = ' AND EXISTS(SELECT TOP (1) ID FROM #TEMP_CentreQualificationLevels_TABLE                  
           INNER JOIN IB3QualificationLookup                  
           ON IB3QualificationLookup.QualificationLevel = #TEMP_CentreQualificationLevels_TABLE.qualLevel                  
  WHERE ID=dbo.ScheduledExamsTable.qualificationId AND (centreId=1 OR centreId=dbo.CentreTable.ID))'                  
   END                  
                  
  DECLARE @myCheckQualifications bit                  
  SET @myCheckQualifications = 1                  
  IF(LEN(@qualificationIdList) = 0)                  
   BEGIN                  
    SET @myCheckQualifications = 0                  
   END                  
                  
  DECLARE @myCheckAssignedQualifications bit                  
  SET @myCheckAssignedQualifications = 1                  
  IF(LEN(@assignedQualIdList) = 0)                  
   BEGIN                  
    SET @myCheckAssignedQualifications = 0                  
   END                  
                  
  --clean the filter doc from memory                  
  exec sp_xml_removedocument @hdoc     
    
  IF @totalMark  <> '' 
  		SET @TotalMarksAvailableClause  = ' AND TotalMarksAvailable = '+ @totalMark + ''
  ELSE IF @totalMarkStartRange <> ''
  		SET @TotalMarksAvailableClause  = ' AND TotalMarksAvailable >= '+@totalMarkStartRange+ ' AND TotalMarksAvailable <= ' + @totalMarkEndRange + ''
  ELSE 		
		SET @TotalMarksAvailableClause  = ''

  IF @totalMarkAwarded <> ''
		SET @totalMarkAwardedClause  = ' AND CumulativeUserMarks = '+ @totalMarkAwarded + ''
  ELSE IF @totalMarkAwardedStartRange <> ''		
  		SET @TotalMarksAvailableClause  = ' AND CumulativeUserMarks >= '+ @totalMarkAwardedStartRange + ' AND CumulativeUserMarks <= ' + @totalMarkAwardedEndRange + ''
  ELSE		
		SET @totalMarkAwardedClause  = ''  
		

  DECLARE @DividebyZeroCheck NVarchar(MAX)
  SET @DividebyZeroCheck =  '
	   (CASE WHEN ISNULL(dbo.CumulativeMarkingtable.CumulativeUserMarks, 0) = 0 OR ISNULL(dbo.CumulativeMarkingtable.TotalMarksAvailable, 0) = 0 THEN 0
				ELSE dbo.CumulativeMarkingtable.CumulativeUserMarks * 100 / dbo.CumulativeMarkingtable.TotalMarksAvailable END )'
  IF @percentage <> ''
		SET @percentageClause  = ' AND ' + @DividebyZeroCheck + ' = ' + @percentage
  ELSE IF @percentageStartRange <> ''
		SET @percentageClause  = ' AND ' + @DividebyZeroCheck + ' >= ' + @percentageStartRange + ' AND ' + @DividebyZeroCheck + ' <= ' + @percentageEndRange
  ELSE
		SET @percentageClause  = ''

  IF @markingProgress <> ''
		SET @markingProgressClause = ' AND ISNULL(dbo.CumulativeMarkingtable.MarkingProgress,0) =  ' + @markingProgress + ''
  ELSE IF @markingProgressStartRange <> ''		
		SET @markingProgressClause = ' AND ISNULL(dbo.CumulativeMarkingtable.MarkingProgress,0) >=  ' + @markingProgressStartRange + ' AND ' + ' ISNULL(dbo.CumulativeMarkingtable.MarkingProgress,0) <=  ' + @markingProgressEndRange 
  ELSE			
		SET @markingProgressClause = ''
     

  IF @autoVoidDateStartRange <> '' AND @autoVoidDateEndRange <> ''
		SET @autoVoidDateClause   = ' AND AutoVoidDate >= ''' + @autoVoidDateStartRange + ''' AND AutoVoidDate <= ''' + @autoVoidDateEndRange + ''''
  ELSE
		SET @autoVoidDateClause   = ' '
	
		
	  
		                  
  -- Create a temporary table to keep selected test data for the group                  
  CREATE TABLE #TEMP_ExamDetailsSearch_TABLE            
  (
   [row]	int,
   [id]      int,                  
   [examState]     int,                  
   [examId]     int,                  
   [examName]     nvarchar(200),                  
   [centreId]     int,                  
   [centreName]    nvarchar(100),                  
   [startDate]     datetime,                  
   [endDate]     datetime,                  
   [startTime]     int,                  
   [endTime]     int,                  
   [candidateForename]   nvarchar(50),                  
   [candidateSurname]   nvarchar(50),                  
   [qualName]     nvarchar(100),                  
   [qualId]     int,                  
   [qualLevel] int,
   [completionDate]   nvarchar(20),                
   [centreCode]  nvarchar(100),              
   [examVersionRef] nvarchar(100)         
   ,[totalMark]   nvarchar(100)            
   ,[totalMarkAwarded] nvarchar(100)  
   ,[percentage] nvarchar(100)          
   ,[markingProgress] nvarchar(100)            
   ,[autoVoidDate] nvarchar(34)
   ,[EnableOverrideMarking] bit
   ,[keycode] varchar(12)
   ,[projectBased] bit
   ,[userAssociationMarker] nvarchar(200)
   ,[userAssociationModerator] nvarchar(200)
  )                  
                  
                   
  --REPLACE SORTING WITH DB COLS                  
  SET @sorting = REPLACE(@sorting, 'centreName', 'centreName')                  
  SET @sorting = REPLACE(@sorting, 'examId', 'examName')                  
  SET @sorting = REPLACE(@sorting, 'candidateSurname', 'candidateSurname')                  
  SET @sorting = REPLACE(@sorting, 'candidateForename', 'candidateForename')                  
  SET @sorting = REPLACE(@sorting, 'completedDate', 'completionDate')                  
  SET @sorting = REPLACE(@sorting, 'qualification', 'qualName')     

               --DROP TABLE #TEMP_CentreQualificationLevels_TABLE
               --    DROP TABLE  #TEMP_ExamDetailsSearch_TABLE
--DROP TABLE  #TEMP_CentreQualificationLevels_TABLE
  set @tempTableInsert = 'INSERT INTO #TEMP_ExamDetailsSearch_TABLE                   
      ([row],
      [id],                  
      [examState],                  
      [examId],                  
      [examName],                  
      [centreId],                  
      [centreName],                  
      [startDate],                  
      [endDate],                  
      [startTime],                  
      [endTime],                  
      [candidateForename],                  
      [candidateSurname],                  
      [qualName],                  
      [qualId],                  
      [qualLevel],
      [completionDate],                
      [centreCode],              
      [examVersionRef] ,            
      [totalMark],            
      [totalMarkAwarded],  
      [percentage],          
      [markingProgress],
      [autoVoidDate],
      [EnableOverrideMarking],
      [keycode],
      [projectBased],
      [userAssociationMarker],
      [userAssociationModerator]) '                   
                   
  -- SET the selection details. This should map to columns identified in the select xml above, it must include the full select. If all the data cannot be retreived at this level                  
  -- then the procedure must be re designed. the use of subqueries and joins is advised for complex queries.                  
  -- This select also include the sort order passed in                  
  set @selectDetails =                   
    'select RowNumber as row, examSessionId, examState, IB3ExamId, examName, centreId, centreName, startDate, endDate, startTime, endTime,                  
      candidateForename, candidateSurname, qualName, qualId, qualLevel, completionDate, CentreCode, examVersionRef, totalMark, totalMarkAwarded, percentage, markingProgress,Convert(varchar(34),autoVoidDate,113) AS autoVoidDate,EnableOverrideMarking,keycode,projectBased,userAssociationMarker,userAssociationModerator             
      from (                  
      SELECT  ROW_NUMBER() OVER (ORDER BY ' + @sorting + ') as RowNumber,                   
      examSessionId, examState, IB3ExamId, examName, centreId, centreName, startDate, endDate, startTime, endTime,                  
      candidateForename, candidateSurname, qualName, qualId, qualLevel, completionDate, CentreCode, examVersionRef, totalMark, totalMarkAwarded, percentage, markingProgress,autoVoidDate,EnableOverrideMarking,keycode,projectBased,userAssociationMarker,userAssociationModerator
      FROM                   
       (select                  
       dbo.ExamSessionTable.ID        AS examSessionId,                   
       dbo.ExamSessionTable.examState       AS ExamState,                   
       dbo.ScheduledExamsTable.examId       AS IB3ExamId,                  
       dbo.ScheduledExamsTable.examName     AS examName,                  
       dbo.ScheduledExamsTable.CentreId     AS CentreId,                  
       dbo.CentreTable.CentreName       AS CentreName,              
       dbo.ScheduledExamsTable.ScheduledStartDateTime  AS startDate,                  
       dbo.ScheduledExamsTable.ScheduledEndDateTime  AS endDate,                  
       dbo.ScheduledExamsTable.ActiveStartTime    AS startTime,                  
       dbo.ScheduledExamsTable.ActiveEndTime    AS endTime,                  
       dbo.UserTable.Forename        AS candidateForename,                  
       dbo.UserTable.Surname        AS candidateSurname,                  
       dbo.IB3QualificationLookup.QualificationName  AS qualName,                  
       dbo.IB3QualificationLookup.ID      AS qualId,                  
       dbo.IB3QualificationLookup.QualificationLevel      AS qualLevel,                  
       dbo.ExamStateChangeAuditTable.StateChangeDate  AS completionDate,              
	   dbo.CentreTable.CentreCode    AS CentreCode,              
	   dbo.ScheduledExamsTable.examVersionRef AS examVersionRef,          
	   dbo.CumulativeMarkingtable.TotalMarksAvailable AS totalMark,            
	   dbo.CumulativeMarkingtable.CumulativeUserMarks AS totalMarkAwarded, 
	   (CASE
		WHEN ISNULL(dbo.CumulativeMarkingtable.CumulativeUserMarks, 0) = 0 OR ISNULL(dbo.CumulativeMarkingtable.TotalMarksAvailable, 0) = 0
		THEN 0
		ELSE (dbo.CumulativeMarkingtable.CumulativeUserMarks * 100 / dbo.CumulativeMarkingtable.TotalMarksAvailable)
		END) AS percentage,
		dbo.CumulativeMarkingtable.MarkingProgress AS markingProgress,		       
	    [AutoVoidDate] AS autoVoidDate,
        dbo.ExamSessionTable.EnableOverrideMarking,
        dbo.ExamSessionTable.KeyCode AS keycode,
        dbo.ExamSessionTable.IsProjectBased AS projectBased,
		CASE USR.RelationTypeID 
				WHEN 1 then USR.Username 
			END AS ''userAssociationMarker'',
		CASE USR.RelationTypeID 
				WHEN 2 then USR.Username 
			END AS ''userAssociationModerator''
      from [dbo].[ExamSessionTable]
      INNER JOIN  dbo.ExamStateChangeAuditTable ON (dbo.ExamStateChangeAuditTable.ExamSessionID = dbo.ExamSessionTable.ID 
		AND dbo.ExamStateChangeAuditTable.NewState = 9)                  
      INNER JOIN  dbo.ScheduledExamsTable ON dbo.ScheduledExamsTable.ID = dbo.ExamSessionTable.ScheduledExamID                                     
      INNER JOIN  dbo.UserTable ON dbo.ExamSessionTable.UserID = dbo.UserTable.ID
      INNER JOIN  dbo.CentreTable ON dbo.ScheduledExamsTable.CentreID = dbo.CentreTable.ID                  
      INNER JOIN  dbo.IB3QualificationLookup ON dbo.IB3QualificationLookup.ID = dbo.ScheduledExamsTable.qualificationId                 
      LEFT OUTER JOIN dbo.CumulativeMarkingtable ON dbo.ExamSessionTable.ID = dbo.CumulativeMarkingtable.ExamSessionId  
	  LEFT JOIN (
			select examsessionid
				, username
				,RelationTypeID
			from UserSessionRelationTable
			inner join usertable 
			on UserSessionRelationTable.userid = usertable.id
	 ) USR 
	 ON USR.examsessionid = ExamSessionTable.ID
	  
       '                        
  -- handle filtertype operator scenarios of AND and OR                  
  IF @filterType = 'AND'                  
  BEGIN        
                     
    SET @whereclause =  ' WHERE                  
          ExamStateChangeAuditTable.ID = (SELECT MIN(ID) FROM ExamStateChangeAuditTable WHERE [NewState] IN (9) AND ExamSessionTable.ID = ExamStateChangeAuditTable.ExamSessionID)                  
          AND                  
          ExamSessionTable.examState = ' + CONVERT(varchar(max), @examState) + '                  
          AND                  
          (((centreName LIKE '''+ @centreName + ''')   
          AND (examVersionRef LIKE '''+ @examVersionRef +''')  
          ' + CASE When @examId <> '' THEN ' AND   examId IN ('+ @examId + ') ' ELSE '' END + ' ) '+   
			 ' AND CentreTable.CentreCode Like ''' + @centreCode + ''''                     
    IF(@myCheckQualifications = 1)                  
    BEGIN                  
		SET @whereclause = @whereclause + 'AND dbo.ScheduledExamsTable.qualificationId IN (' + @qualificationIdList + ')'                  
    END                  
    IF(@myCheckAssignedQualifications = 1)                  
    BEGIN                  
		SET @whereclause = @whereclause + ' AND dbo.ScheduledExamsTable.qualificationId IN (' + @assignedQualIdList + ')'                  
    END      
      
    IF(@hideMarkedExams = 1)        
	 BEGIN        
		SET @whereclause = @whereclause + ' AND ISNULL(dbo.CumulativeMarkingtable.MarkingProgress,0) < 100 '        
    END   

    IF @candidateForename <> '%'
      SET @whereclause = @whereclause + ' AND UserTable.Forename like ''' +  @candidateForename +''''

    IF @candidateSurname <> '%'
      SET @whereclause = @whereclause + ' AND UserTable.Surname like ''' +  @candidateSurname + ''''

    IF @TotalMarksAvailableClause <> ''
		SET @whereclause = @whereclause + @TotalMarksAvailableClause  
      
    IF @totalMarkAwardedClause <> ''   
      SET @whereclause = @whereclause + @totalMarkAwardedClause 
      
    IF @percentageClause <> ''   
      SET @whereclause = @whereclause + @percentageClause 
      
    IF @markingProgressClause <> ''   
      SET @whereclause = @whereclause + @markingProgressClause
      
    IF @autoVoidDateClause <> ''   
      SET @whereclause = @whereclause + @autoVoidDateClause
            
    IF @completionDateStartRange != ''
		BEGIN
			SET @whereclause = @whereclause + ' AND dbo.ExamStateChangeAuditTable.StateChangeDate >= Convert(smalldatetime, ''' + @completionDateStartRange +''')'
		END
	IF @completionDateEndRange != ''
		BEGIN
			SET @whereclause = @whereclause + ' AND dbo.ExamStateChangeAuditTable.StateChangeDate <= Convert(smalldatetime, ''' + @completionDateEndRange +''')'
		END
		
	
	IF @userAssociationMarker <> ''
	BEGIN
		SET @whereclause = @whereclause + ' AND (dbo.fn_GetUserRelationForExamSession(ExamSessionTable.ID, 1)) LIKE ''' +  @userAssociationMarker + ''''
	END
	
	IF @userAssociationModerator <> ''
	BEGIN
		SET @whereclause = @whereclause + ' AND (dbo.fn_GetUserRelationForExamSession(ExamSessionTable.ID, 2)) LIKE ''' +  @userAssociationModerator + ''''
	END
                 
    SET @whereclause = @whereclause + @myQualLevelWhereClause + ')) 
          
          as ExamDetails) as examSearch'                  
        
        
        
   END                  
 
              
                  
  -- build the pre and post paging strings                  
  set @pagingwhereclause = ' WHERE RowNumber BETWEEN @RowStart AND @RowEnd ORDER BY ' + @sorting                  
                                        
  set @sqlPrePaging = 'SELECT @TotalResultCount =  Count(*) FROM (' + @selectDetails + @whereclause + ') as TotalRowCount'                  
      
  set @sqlPostPaging =                   
   @tempTableInsert + @selectDetails + @whereclause + @pagingwhereclause                  
  
  /*print @selectDetails
  print @whereclause*/
                      
  -- ** we need to execute as an ansi string for the parameterised sorting to work **                  
  exec sp_executesql @sqlPrePaging, N'@TotalResultCount INT OUTPUT', @TotalResultCount OUTPUT                  
                    
  -- required variables set before paging applied                  
  set @TotalCount = CEILING((@TotalResultCount * 1.0) / @PageSize)  
  
  -- execute post paging, this will select the page of records we are interedted in.                  
  exec sp_executesql @sqlPostPaging, N'@RowStart INT, @RowEnd INT, @TotalResultCount INT, @TotalCount INT,                  
    @PageNumber INT, @PageSize INT',                   
   @RowStart, @RowEnd, @TotalResultCount, @TotalCount, @PageNumber, @PageSize  
   
-- WARNING: 
-- Any changes to this SP may affect the external marking popup! The UI model is built using the return of this.
-- Please see the 'SERVICE_GetExamByKeycode' version of this sp in this service! - JC                
       
  SELECT '0' as '@errorCode',
  (                  
   SELECT @PageNumber as '@pageIndex', @PageSize as '@pageSize', @TotalCount as '@totalCount', @TotalResultCount as '@totalRecords',                  
   (                  
    SELECT  [id]										AS 'examInstanceId',                  
    [examState]										AS 'examInstanceStatus',                  
    [examId]										AS 'examId',                   
    [examName]										AS 'examName',                  
    [centreId]										AS 'centreId',                  
    REPLACE([centreName],'&','&amp;')				AS 'centreName',                   
    [startDate]										AS 'startDate',                  
    [endDate]										AS 'endDate',                  
    [startTime]										AS 'startTime',                  
    [endTime]										AS 'endTime',                  
    REPLACE([candidateForename], '&apos;', '')		AS 'candidateForename',                  
    REPLACE([candidateSurname], '&apos;', '')		AS 'candidateSurname',                  
    REPLACE([qualName],'&','&amp;')					AS 'qualification',                  
    [qualId]										AS 'qualId',                  
    [qualLevel]										AS 'qualLevel',
    Convert(datetime,[completionDate],113)			AS 'completedDate',                  
	CentreCode										AS 'centreCode',              
	examVersionRef									AS 'examVersionRef',            
	isnull([totalMark],0)							AS 'totalMark',   
	[percentage]									AS [percentage],        
	isnull([totalMarkAwarded],0)					AS 'totalMarkAwarded',            
	case           
	when isnull([markingProgress],'')=''
	then ''    
	else [markingProgress] + '%'          
	end												AS 'markingProgress',
    [autoVoidDate]									AS 'autoVoidDate',
    [EnableOverrideMarking]							AS 'EnableOverrideMarking',
    [keycode]										AS 'keycode',
    [projectBased]									AS 'projectBased',
    ISNULL([userAssociationMarker],'')				AS 'userAssociationMarker',
    ISNULL([userAssociationModerator],'')			AS 'userAssociationModerator'
	,    dbo.GetExamSessionUserAssociations(#TEMP_ExamDetailsSearch_TABLE.id)
    FROM #TEMP_ExamDetailsSearch_TABLE ORDER BY row
    FOR XML PATH('exam') ,TYPE                       
   )                  
   FOR XML PATH('return') ,TYPE                   
  )                  
  FOR XML PATH('result')
                    
  --clean up the sp                  
  DROP TABLE #TEMP_ExamDetailsSearch_TABLE                  
  DROP TABLE #TEMP_CentreQualificationLevels_TABLE                  
END TRY                  
BEGIN CATCH                  
 EXEC sa_SHARED_GetErrorDetails_sp                  
END CATCH;                   
END 





