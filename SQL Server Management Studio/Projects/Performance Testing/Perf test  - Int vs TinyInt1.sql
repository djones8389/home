--DROP TABLE PerformanceTest_INT;
--DROP TABLE PerformanceTest_TinyINT;

--USE TempDBForTom

--TRUNCATE TABLE PerformanceTest_INT;
--TRUNCATE TABLE PerformanceTest_TinyINT;

--CREATE TABLE dbo.PerformanceTest_INT 
--(
--		Id INT NOT NULL, 
--		InsertDate DATETIME NOT NULL, 
--		ABunchOfLetters NVARCHAR(100)
--)

--CREATE TABLE PerformanceTest_TinyINT
--(
--		Id TINYINT NOT NULL, 
--		InsertDate DATETIME NOT NULL, 
--		ABunchOfLetters NVARCHAR(100)
--)

--GO
--SET NOCOUNT ON
--GO



DECLARE @GenerateDate nvarchar(MAX) =  	'
		DECLARE @i INT = 1
		WHILE @i <= 250
		BEGIN
			INSERT PerformanceTest_INT (ID, InsertDate, ABunchOfLetters)
			SELECT @i, GETDATE(), REPLICATE(N''ABCD'', 25)
			SET @i+=1
		END
		
	
		DECLARE @j INT = 1
		WHILE @j <= 250
		BEGIN
			INSERT PerformanceTest_TinyINT (ID, InsertDate, ABunchOfLetters)
			SELECT @j, GETDATE(), REPLICATE(N''ABCD'', 25)
			SET @j+=1
		END
		'

DECLARE @NumberOfIterations smallint = 1;
WHILE @NumberOfIterations <= 4000
BEGIN

EXEC(@GenerateDate)


SET @NumberOfIterations +=1
END
GO



SELECT DATEDIFF(MINUTE, MIN(InsertDate), MAX(InsertDate) FROM  PerformanceTest_INT; --where ID = 201
SELECT * FROM  PerformanceTest_TinyINT; --where ID = 201


select top 10 DATEDIFF(Minute, InsertDate, GETDATE()) FROM  PerformanceTest_INT;

select DATEDIFF(SECOND, min(InsertDate), max(InsertDate) ) FROM  PerformanceTest_INT;

select DATEDIFF(SECOND, min(InsertDate), max(InsertDate) ) FROM  PerformanceTest_TinyINT;



SET STATISTICS IO ON

select *
FROM  PerformanceTest_INT;

select *
FROM  PerformanceTest_TinyINT;