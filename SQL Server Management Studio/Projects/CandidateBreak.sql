IF OBJECT_ID('tempdb..##DATALENGTH') IS NOT NULL DROP TABLE ##DATALENGTH;

CREATE TABLE ##DATALENGTH (
	client nvarchar(1000)
	, [Count] smallint
);

DECLARE @dynamic nvarchar(MAX)='';

select @dynamic +=CHAR(13)+ '
use '+QUOTENAME([NAME])+'

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

INSERT ##DATALENGTH
SELECT db_name()
	, COUNT(ID) 
FROM AssessmentTable 
WHERE EnableCandidateBreak = 1 
	AND CandidateBreakStyle = 1 
'
from sys.databases
where state_desc = 'ONLINE'
	and name like '%[_]ItemBank';

exec(@dynamic);



select 
	substring(client,0,charindex('_',client)) [Client]
	, Client as [Database]
	, [Count]
from ##DATALENGTH
order by 1