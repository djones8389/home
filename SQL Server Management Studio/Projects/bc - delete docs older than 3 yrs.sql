SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

use BRITISHCOUNCIL_SecureAssess

SELECT *
INTO [DJTemp_ESID_DocName]
FROM [430326-BC-SQL2\SQL2].[BritishCouncil_SecureMarker].dbo.[ESID_DocumentNAME]

select 
	wesdt.ID
	--, wesdt.uploadDate
	--SUM(CAST(DATALENGTH(DOCUMENT) AS BIGINT))/1024/1024/1024 [GB]
INTO [WH_OldDocsToDelete]
from WAREHOUSE_ExamSessionDocumentTable wesdt	WITH (NOLOCK)
inner join WAREHOUSE_ExamSessionTable_Shreded wests WITH (NOLOCK)
on wests.examsessionid = wesdt.warehouseexamsessionid
inner join [DJTemp_ESID_DocName] A
on A.[ExternalSessionID] = wests.examSessionId	
	AND A.[name] = wesdt.itemId 
where wesdt.uploadDate < DATEADD(YEAR, -3, getdate())
	and qualificationName != 'International Comparisons Research'
order by 1;	

SELECT *
FROM [WH_OldDocsToDelete]

CREATE CLUSTERED INDEX [IX_ID] on [WH_OldDocsToDelete] (ID);

SELECT max(uploaddate)
FROM WAREHOUSE_ExamSessionDocumentTable A
INNER JOIN [WH_OldDocsToDelete] B
ON A.ID = B.ID

DROP TABLE [DJTemp_ESID_DocName]