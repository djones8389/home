--Mirror

ALTER LOGIN [Saxion_AnalyticsManagementUser] DISABLE ;
ALTER LOGIN [Saxion_ContentAuthorUser] DISABLE ;
ALTER LOGIN [Saxion_CPPreProd] DISABLE ;
ALTER LOGIN [Saxion_ETLUser] DISABLE ;
ALTER LOGIN [Saxion_ItemBankUser] DISABLE ;
ALTER LOGIN [Saxion_SecureAssessUser] DISABLE ;
ALTER LOGIN [Saxion_SecureMarkerUser] DISABLE ;
ALTER LOGIN [Saxion_SurpassManagementUser] DISABLE ;
ALTER LOGIN [SecureAssessUser] DISABLE ;
ALTER LOGIN [Editions_Mirror_ContentProducer_Login] DISABLE ;
ALTER LOGIN [IntegrationUser] DISABLE ;
DROP LOGIN [Ztest_ItemBankUser] --DISABLE ;
DROP LOGIN [Ztest_SecureAssessUser] --DISABLE ;
DROP LOGIN [Ztest_SurpassManagementUser] --DISABLE ;

--Live

ALTER LOGIN [Saxion_CPPreProd] DISABLE ;
ALTER LOGIN [ReportingUser_Saxion] DISABLE ;
ALTER LOGIN [PerformanceTest1_SecureAssessUser] DISABLE ;
ALTER LOGIN [Editions_Shared_ContentProducer_Login] DISABLE ;
ALTER LOGIN [cpuser_demo_editions] DISABLE ;
ALTER LOGIN [cpuser_demoEditions] DISABLE ;
ALTER LOGIN [cpuser_saxion] DISABLE ;
ALTER LOGIN [cpuser_saxion_editions] DISABLE ;
ALTER LOGIN [AQAEditions_CP] DISABLE ;


--PrpPrv

ALTER LOGIN [SANDBOX_SQA_CPProjectAdmin_Login] DISABLE ;
ALTER LOGIN [SHA_BIS_CPProjectAdmin_11.0_Login] DISABLE ;
ALTER LOGIN [SHA_BIS_ItemBank_11.0_Login] DISABLE ;
ALTER LOGIN [SHA_BIS_SecureAssess_11.0_Login] DISABLE ;
ALTER LOGIN [STG_SQA2_ContentProducer_Login] DISABLE ;
ALTER LOGIN [STG_SQA2_ItemBank_Login] DISABLE ;
ALTER LOGIN [STG_SQA2_OpenAssess_Login] DISABLE ;
ALTER LOGIN [STG_SQA2_SecureAssess_Login] DISABLE ;