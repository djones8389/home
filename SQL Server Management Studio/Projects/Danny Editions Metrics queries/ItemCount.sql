IF OBJECT_ID('tempdb..##ItemCount') IS NOT NULL DROP TABLE ##ItemCount;

CREATE TABLE ##ItemCount (
	client sysname
	, ItemCount int
);

DECLARE @dynamic nvarchar(MAX)='';

select @dynamic +=CHAR(13)+ '
use '+QUOTENAME([NAME])+'

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

INSERT ##ItemCount
select db_name() [client]
	, count(ID) [ItemCount]
from Items

'
from sys.databases
where state_desc = 'ONLINE'
	and name like '%[_]ContentAuthor';

exec(@dynamic);

select substring(client, 0, charindex('_',client)) [Client]
	, ItemCount
from ##ItemCount
order by 1 asc;






