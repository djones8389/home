select Keycode
	, cev.DateSubmitted [Exam Imported to SM]
	, ur.insertionDate [Item Imported to SM]
	, i.ExternalItemName
	, i.ExternalItemID
from CandidateExamVersions cev
inner join CandidateResponses cr
on cr.CandidateExamVersionID = cev.ID
inner join UniqueResponses ur
on ur.id = cr.UniqueResponseID
inner join Items i
on i.id = ur.itemId
where  DATEDIFF(DAY,ur.insertionDate, cev.DateSubmitted) > 1
	order by DateSubmitted desc;

	--ur.insertionDate < cev.DateSubmitted
	