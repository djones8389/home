use djtest123

SELECT B.ID	
	, B.D
	, MAX(R)
FROM (
	SELECT A.ID
		, b.c.value('.','nvarchar(MAX)')	d
		, ROW_NUMBER() OVER(partition by a.id order by (SELECT 1)) R
	FROM (
		SELECT id, cast(itemvalue as xml) itemValue
		FROM Pagetable
	) A
	cross apply itemValue.nodes('ItemValue/metaData/HISTORY/USER/checkedInCmt') b(c)

	where b.c.value('.','nvarchar(MAX)') <> ''
) B
group by B.ID	
	, B.D


	select itemValue
		, b.c.value('.','varchar(200)')
	FROM (
		SELECT id, cast(itemvalue as xml) itemValue
		FROM PAGETABLE
		where id = '680p706'
	) g
	cross apply itemValue.nodes('ItemValue/metaData/HISTORY/USER/checkedInCmt[text() = (Recursive Check-in)]') b(c)


	--//group/p1[string-length(text()) > 0] 


/*
<xml>
    <node>
       <node1/>
       <node2/>
       <node3>value</node3>
    </node>
</xml>
*/







/*
create table PageTable (
	ID nvarchar(20)
	, itemValue nvarchar(MAX)
)

insert PageTable(ID,itemValue)
values('680p706','<ItemValue>
  <metaData>
    <HISTORY>
      <USER>
        <userName>twebb</userName>
        <userId>726</userId>
        <clientDataTime>30/05/2013 09:00:04</clientDataTime>
        <serverDataTime>30/05/2013 09:00:04</serverDataTime>
        <checkedInStatus>Released</checkedInStatus>
        <checkedInCmt />
        <attributesSet />
      </USER>
      <USER>
        <userName>twebb</userName>
        <userId>726</userId>
        <clientDataTime>02/03/2015 09:46:48</clientDataTime>
        <serverDataTime>02/03/2015 09:46:47</serverDataTime>
        <checkedInStatus>Released</checkedInStatus>
        <checkedInCmt>Recursive Check-in</checkedInCmt>
        <attributesSet />
      </USER>
      <USER>
        <userName>twebb</userName>
        <userId>726</userId>
        <clientDataTime>02/03/2015 10:06:19</clientDataTime>
        <serverDataTime>02/03/2015 10:06:17</serverDataTime>
        <checkedInStatus>Released</checkedInStatus>
        <checkedInCmt>Recursive Check-in</checkedInCmt>
        <attributesSet />
      </USER>
      <USER>
        <userName>nicolaa</userName>
        <userId>865</userId>
        <clientDataTime>04/03/2015 14:27:48</clientDataTime>
        <serverDataTime>04/03/2015 14:27:48</serverDataTime>
        <checkedInStatus>Released</checkedInStatus>
        <checkedInCmt>Recursive Check-in</checkedInCmt>
        <attributesSet />
      </USER>
      <USER>
        <userName>nicolaa</userName>
        <userId>865</userId>
        <clientDataTime>04/03/2015 15:39:43</clientDataTime>
        <serverDataTime>04/03/2015 15:39:43</serverDataTime>
        <checkedInStatus>Released</checkedInStatus>
        <checkedInCmt>Recursive Check-in</checkedInCmt>
        <attributesSet />
      </USER>
      <USER>
        <userName>nicolaa</userName>
        <userId>865</userId>
        <clientDataTime>04/03/2015 16:34:06</clientDataTime>
        <serverDataTime>04/03/2015 16:34:06</serverDataTime>
        <checkedInStatus>Withdrawn</checkedInStatus>
        <checkedInCmt />
        <attributesSet />
      </USER>
    </HISTORY>
  </metaData>
</ItemValue>'),('680p707','<ItemValue>
  <metaData>
    <HISTORY>
      <USER>
        <userName>twebb</userName>
        <userId>726</userId>
        <clientDataTime>30/05/2013 09:00:26</clientDataTime>
        <serverDataTime>30/05/2013 09:00:26</serverDataTime>
        <checkedInStatus>Released</checkedInStatus>
        <checkedInCmt />
        <attributesSet />
      </USER>
      <USER>
        <userName>twebb</userName>
        <userId>726</userId>
        <clientDataTime>25/06/2013 09:09:10</clientDataTime>
        <serverDataTime>25/06/2013 09:09:10</serverDataTime>
        <checkedInStatus>Released</checkedInStatus>
        <checkedInCmt>6705.101A.04.04 Pictures in Question</checkedInCmt>
        <attributesSet />
      </USER>
      <USER>
        <userName>twebb</userName>
        <userId>726</userId>
        <clientDataTime>02/07/2013 10:10:08</clientDataTime>
        <serverDataTime>02/07/2013 10:10:08</serverDataTime>
        <checkedInStatus>Released</checkedInStatus>
        <checkedInCmt />
        <attributesSet />
      </USER>
      <USER>
        <userName>twebb</userName>
        <userId>726</userId>
        <clientDataTime>02/07/2013 10:22:30</clientDataTime>
        <serverDataTime>02/07/2013 10:22:29</serverDataTime>
        <checkedInStatus>Released</checkedInStatus>
        <checkedInCmt>6705.101A.04.04 Pictures in Question</checkedInCmt>
        <attributesSet />
      </USER>
      <USER>
        <userName>twebb</userName>
        <userId>726</userId>
        <clientDataTime>02/03/2015 09:46:52</clientDataTime>
        <serverDataTime>02/03/2015 09:46:51</serverDataTime>
        <checkedInStatus>Released</checkedInStatus>
        <checkedInCmt>Recursive Check-in</checkedInCmt>
        <attributesSet />
      </USER>
      <USER>
        <userName>twebb</userName>
        <userId>726</userId>
        <clientDataTime>02/03/2015 10:06:23</clientDataTime>
        <serverDataTime>02/03/2015 10:06:22</serverDataTime>
        <checkedInStatus>Released</checkedInStatus>
        <checkedInCmt>Recursive Check-in</checkedInCmt>
        <attributesSet />
      </USER>
      <USER>
        <userName>nicolaa</userName>
        <userId>865</userId>
        <clientDataTime>04/03/2015 14:27:51</clientDataTime>
        <serverDataTime>04/03/2015 14:27:51</serverDataTime>
        <checkedInStatus>Released</checkedInStatus>
        <checkedInCmt>Recursive Check-in</checkedInCmt>
        <attributesSet />
      </USER>
      <USER>
        <userName>nicolaa</userName>
        <userId>865</userId>
        <clientDataTime>04/03/2015 15:39:47</clientDataTime>
        <serverDataTime>04/03/2015 15:39:47</serverDataTime>
        <checkedInStatus>Released</checkedInStatus>
        <checkedInCmt>Recursive Check-in</checkedInCmt>
        <attributesSet />
      </USER>
    </HISTORY>
  </metaData>
</ItemValue>')
*/
