select west.examSessionId
	, wests.started [WHStarted]
	, west.completionDate [WHCompleted]
	, LiveStates.NewState
	, LiveStates.StateChangeDate
from WAREHOUSE_ExamSessionTable_Shreded wests
inner join WAREHOUSE_ExamSessionTable west
on west.id = wests.examSessionId
INNER JOIN 
 (
	select a.*
	FROM (
	select est.ID
		, escat.NewState
		, ROW_NUMBER() over (partition by est.ID
		, escat.NewState order by est.ID
		, escat.NewState) R
		, escat.StateChangeDate
	from ExamSessionTable EST
	inner join ExamStateChangeAuditTable escat
	on escat.ExamSessionID = est.ID
	where est.examState = 13		
		and NewState in (6,9)
	) A
		where R = 1
		
) LiveStates	
on LiveStates.ID = west.ExamSessionID
	where LiveStates.ID = 1377987


/*
WHStarted					 WHCompleted
2014-07-08 19:43:42.000	     2014-07-08 20:37:00.000

Live Started				 LiveCompleted
2014-07-08 19:43:42.007		 2014-07-08 20:37:04.063


*/