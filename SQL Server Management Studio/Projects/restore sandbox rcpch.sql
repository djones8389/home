RESTORE DATABASE [SANDBOX_Rcpch_SurpassDataWarehouse] FROM  DISK = N'T:\Backup\RCPCH.AMSDWSM.2017.06.23.bak' WITH  FILE = 1
	,  MOVE N'Data' TO N'S:\DATA\SANDBOX_Rcpch_SurpassDataWarehouse.mdf'
	,  MOVE N'Log' TO N'L:\LOGS\SANDBOX_Rcpch_SurpassDataWarehouse.ldf'
	,  NOUNLOAD,  REPLACE,  STATS = 10

RESTORE DATABASE [SANDBOX_Rcpch_AnalyticsManagement] FROM  DISK = N'T:\Backup\RCPCH.AMSDWSM.2017.06.23.bak' WITH  FILE = 1
	,  MOVE N'Data' TO N'S:\DATA\SANDBOX_Rcpch_AnalyticsManagement.mdf'
	,  MOVE N'Log' TO N'L:\LOGS\SANDBOX_Rcpch_AnalyticsManagement.ldf'
	,  NOUNLOAD,  REPLACE,  STATS = 10

RESTORE DATABASE [SANDBOX_Rcpch_SurpassDataWarehouse] FROM  DISK = N'T:\Backup\RCPCH.AMSDWSM.2017.06.23.bak' WITH  FILE = 1
	,  MOVE N'Data' TO N'S:\DATA\SANDBOX_Rcpch_SurpassDataWarehouse.mdf'
	,  MOVE N'Log' TO N'L:\LOGS\SANDBOX_Rcpch_SurpassDataWarehouse.ldf'
	,  NOUNLOAD,  REPLACE,  STATS = 10



