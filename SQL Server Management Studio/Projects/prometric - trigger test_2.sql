use Windesheim_SecureAssess

select id, examstate
from examsessiontable
order by 2

declare @ESID int = 77046

--select warehousetime, examsessionid,warehouseexamstate from WAREHOUSE_ExamSessionTable where examsessionid = @ESID

update WAREHOUSE_ExamSessionTable 
set WarehouseExamState = 2
where examsessionid = @ESID

exec sa_SHARED_warehouseExam_sp @examInstanceID=@ESID, @originatorID = 1,@returnString=NULL


create trigger [dbo].[whtrigger] 	
ON [dbo].[WAREHOUSE_ExamSessionTable]
AFTER UPDATE
AS	
		IF UPDATE(warehouseexamstate) AND ((SELECT WarehouseExamState from inserted)=1)

		BEGIN

			SELECT reMarkStatus FROM inserted      
			select warehouseexamstate FROM inserted        
			print 'updated'
				 
		END   
GO

/*

delete ws
from warehouse_examsectionstable ws  
inner join WAREHOUSE_ExamSessionTable we 
on ws.examsessionid = we.id
where we.examsessionid = @ESID

delete from WAREHOUSE_ExamSessionTable where examsessionid = @ESID

delete ws 
from WAREHOUSE_ScheduledExamsTable ws 
inner join WAREHOUSE_ExamSessionTable we 
on ws.id = we.warehousescheduledexamid 
where  examsessionid = @ESID





select warehousetime, examsessionid,warehouseexamstate from WAREHOUSE_ExamSessionTable where examsessionid = @ESID

update WAREHOUSE_ExamSessionTable
set warehouseexamstate = 1
where id = 2


insert WAREHOUSE_ExamSessionTable
values(3,3,3)
v