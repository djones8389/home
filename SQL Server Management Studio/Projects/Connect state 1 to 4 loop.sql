--select ID
--	, 0 Processed
--INTO [AlterStates]
--from examsessiontable NOLOCK
--where examState = 4
--	and cast(structurexml as xml).exist('assessmentDetails/assessment/section/item[@id="5050P2982" and @version != "10"]') = 1
--select * from [AlterStates]

use Connect_SecureAssess;

DECLARE @Min int = 1;
DECLARE @Max int = 2266;

 --(
	--select count(ID)
	--from examsessiontable NOLOCK
	--where examState = 4
	--	and cast(structurexml as xml).exist('assessmentDetails/assessment/section/item[@id="5050P2982" and @version != "10"]') = 1
	--);


DECLARE @Increment int = 100;

WHILE (@Min < @Max)

BEGIN	
	
	DECLARE @IDs table (ID INT)
	INSERT @IDs
	select top (@Increment) ID
	from examsessiontable NOLOCK
	where examState = 4
		and cast(structurexml as xml).exist('assessmentDetails/assessment/section/item[@id="5050P2982" and @version != "10"]') = 1
	
	UPDATE A
	SET previousExamState = examState
		, examState = 1
	FROM [ExamSessionTable] A
	INNER JOIN @IDs B
	on A.ID = B.ID
	
	
	SELECT @Min = @Min + @Increment

	DELETE @IDs

	WAITFOR DELAY '00:00:20'

END

select *
into #states
from ExamStateChangeAuditTable


select *
from [AlterStates]

UPDATE [ExamSessionTable]
SET previousExamState = examState
	, examState = 1
where ID IN (
	select top 200 id
	from examsessiontable NOLOCK
	where examState = 4
		and cast(structurexml as xml).exist('assessmentDetails/assessment/section/item[@id="5050P2982" and @version != "10"]') = 1
)

sp_whoisactive

select a.DBName
	, examState
	, count(ID) [Count]
FROM (
select db_name() DBName
	, ID
	, examState
from ExamSessionTable NOLOCK
) a
group by DBName,examState
order by 1,2



select count(ID)
	, [version]
from (
	select ID
		, a.b.value('@version','int') [version]
	from (
	select ID
		, cast(structurexml as xml) structurexml
	from examsessiontable NOLOCK
	where examState = 4
	) c
	CROSS APPLY structurexml.nodes('assessmentDetails/assessment/section/item') a(b)
	--where a.b.value('@id','nvarchar(20)') = '5050P2982' and a.b.value('@version','int') <> 10
	where structurexml.exist('assessmentDetails/assessment/section/item[@id="5050P2982" and @version != "10"]') = 1
 ) d
 group by [version]