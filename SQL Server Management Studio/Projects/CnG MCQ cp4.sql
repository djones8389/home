use PRV_Evolve_CPProjectAdmin

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
SET NOCOUNT ON;

IF OBJECT_ID('tempdb..#Questions') IS NOT NULL DROP TABLE #Questions;
IF OBJECT_ID('tempdb..#Answers') IS NOT NULL DROP TABLE #Answers;

CREATE TABLE #Questions (
	ItemID VARCHAR(20)
	, Question VARCHAR(max)
	, Answers VARCHAR(max)
);

INSERT #Questions(ItemID,Question)
select distinct 
	itt.itemID
	, itt.Question
FROM ItemMultipleChoiceTable MCQ
INNER JOIN  (
	select a.itemID
		, a.Question
	from (
		select distinct substring(ID, 0, charindex('S',ID)) [ItemID]	
			, cast(ITT.ItemValue as xml).value('data(ItemValue/TEXTFORMAT/P/FONT)[1]','nvarchar(1000)') Question
			, ROW_NUMBER() OVER(PARTITION BY substring(ITT.ID, 0, charindex('S',ITT.ID)) ORDER BY ITT.Y ASC) R
		from [ItemTextBoxTable] ITT
		where substring(ID, 0, charindex('S',ID)) like '344P1151%'
	) a 
	where r = 1
) ITT
on ITT.itemID = substring(MCQ.ID, 0, charindex('S',MCQ.ID));


SELECT 
	substring(ID, 0, charindex('S',ID)) [ItemID]
	,[asc]
	,cast(mcq.ItemValue as xml).value('data(ItemValue/TEXTFORMAT/P/FONT)[1]','nvarchar(1000)') Answers
INTO #Answers
FROM [dbo].[ItemMultipleChoiceTable] mcq
where substring(ID, 0, charindex('S',ID)) like '344P1151%'

CREATE CLUSTERED INDEX [IX_ItemID] on #Questions (itemid);
CREATE CLUSTERED INDEX [IX_ItemID] on #Answers (itemid);

update #questions
set answers = a.NameValues
from #questions q
inner join (
	 SELECT 
	  itemid,
		substring((
			SELECT ', ' + [answers]  
			FROM #answers 
			WHERE (itemid = Results.itemid) 
			FOR XML PATH('')
		), 2,1000) NameValues
	FROM #answers Results
	GROUP BY itemid
) a
on a.itemid = q.itemid


select *
FROM #questions