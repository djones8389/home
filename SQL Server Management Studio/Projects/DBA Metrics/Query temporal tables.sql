--/****** Script for SelectTopNRows command from SSMS  ******/
--SELECT
--      *
--  FROM [PSCollector].[dbo].DBGrowthMetrics (READUNCOMMITTED)
--  where StartDate > '2017-05-02'
--	and Instance = '524778-SQL'
	

select 'select Distinct [Instance] from '+quotename(Name) + ' where StartDate > ''2017-05-02'''
from sys.tables
where temporal_type = '2'

select Distinct [Instance] from [DiskSpace] where StartDate > '2017-05-02'
select Distinct [Instance] from [DBGrowthMetrics] where StartDate > '2017-05-02'
select Distinct [Instance] from [Backup Metrics] where StartDate > '2017-05-02'
select Distinct [Instance] from [MissingIndexes] where StartDate > '2017-05-02'
select Distinct [Instance] from [SQLErrorLog] where StartDate > '2017-05-02'
select Distinct [Instance] from [Sproc Metrics] where StartDate > '2017-05-02'
select Distinct [Instance] from [IndexMetrics] where StartDate > '2017-05-02'