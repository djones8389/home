SELECT 
	A.Instance
	, A.[database_name]
	, SUM(a.data_kb) data_kb
	, SUM(a.index_size) index_size
	, SUM(a.data_kb) + SUM(a.index_size) [total]
	,[StartDate]
FROM (

SELECT 
	[Instance]
      ,[database_name]
      --,[table_name]
      --,[rows]
      --,[reserved_kb]
      ,cast(replace([data_kb], 'KB','') as float) [data_kb]
      ,cast(replace([index_size], 'KB','') as float) [index_size]
      --,[unused_kb]
      ,cast([StartDate] as nvarchar(16)) [StartDate]
      --,[EndDate]
  FROM [PSCollector].[dbo].[DBGrowthMetrics]
 -- where instance = '403252-ACTVARIE'
	--and database_name = 'TAP_SecureAssess_9.0'
) A
--where database_name like 'aat_secureassess'
where Instance IN ('430069-AAT-SQL\SQL1','430327-AAT-SQL2\SQL2') -- '430088-BC-SQL\SQL1'--'553092-PRDSQL'
	--and StartDate in ('2017-04-11 07:54','2017-04-24 07:15')
group by 
	A.Instance
	, A.[database_name]
	, a.StartDate

order by StartDate desc;  --Instance asc,[database_name] asc,

