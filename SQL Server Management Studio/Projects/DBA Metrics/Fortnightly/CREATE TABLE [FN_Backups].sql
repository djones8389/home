USE [PSCollector]
GO

ALTER TABLE [dbo].[FN_Backups] SET (SYSTEM_VERSIONING = OFF)
GO
DROP TABLE [dbo].[FN_BackupsHistory]
GO
DROP TABLE [dbo].[FN_Backups]
GO

CREATE TABLE [FN_Backups] (
	[ID] uniqueidentifier PRIMARY KEY CLUSTERED,
	[Instance] [nvarchar](500) NULL,
	[DatabaseName] [nvarchar](500) NULL,
	type CHAR(1),
	[backup_start_date] [datetime] NULL,	
	[BackupName] nvarchar(500),
	[UserName] nvarchar(100),
	StartDate [datetime2](2) GENERATED ALWAYS AS ROW START NOT NULL,
	EndDate [datetime2](2) GENERATED ALWAYS AS ROW END NOT NULL,
	PERIOD FOR SYSTEM_TIME (StartDate,EndDate)  
)WITH (
	SYSTEM_VERSIONING = ON ( HISTORY_TABLE = [dbo].[FN_BackupsHistory] )
)
GO


