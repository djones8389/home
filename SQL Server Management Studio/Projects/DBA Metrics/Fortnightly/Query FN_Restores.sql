use [PSCollector]

if OBJECT_ID('tempdb..#restores') is not null drop table #restores

SELECT 
	COALESCE(b.ClientName,'GRAND TOTAL') 'ClientName'
	, COALESCE(a.[user_name],'TOTAL') 'UserName'
	, count([ID]) [Count]
into #restores
FROM [PSCollector].[dbo].[FN_Restores] A
inner join PSCollector.[dbo].[InstanceLookUpTable] B
on a.Instance = b.InstanceName
where cast(StartDate as date) = '2017-10-30'
	group by ClientName,[user_name] WITH ROLLUP


UPDATE #restores
set UserName = ''
	, [ClientName] = ''
where [Count] = (SELECT MAX([Count]) from #restores)

	
select [Count]
	, ClientName
	, UserName
from #restores
where UserName <> 'Total'
	order by [Count] desc