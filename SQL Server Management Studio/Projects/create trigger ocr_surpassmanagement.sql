USE [Ocr_SurpassManagement]
GO

CREATE TABLE [dbo].[UserCentreSubjectRoles_DeleteAudit] (
	[Id] [bigint] NULL,
	[UserId] [bigint] NULL,
	[CentreId] [bigint] NULL,
	[SubjectId] [bigint] NULL,
	[SubjectLinkType] [int] NOT NULL,
	[Assignable] [bit] NOT NULL,
	[RoleId] [int] NOT NULL,
	[Timestamp] datetime
) ON [PRIMARY]
GO


CREATE TRIGGER [dbo].[UserCentreSubjectRoles_Trigger] ON  [dbo].[UserCentreSubjectRoles] 
   AFTER DELETE
AS 
BEGIN
	  
	  INSERT [UserCentreSubjectRoles_DeleteAudit]
	  SELECT Id, UserId, CentreId, SubjectId, SubjectLinkType, Assignable, RoleId, (Select getDate())
	  FROM deleted

END
GO

