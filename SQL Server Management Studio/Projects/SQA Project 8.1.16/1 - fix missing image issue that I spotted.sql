SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

IF OBJECT_ID ('tempdb..##Pages') IS NOT NULL DROP TABLE ##Pages;

select 
	DISTINCT
	A.id			
	 , A.name
	 , A.Projectid
	 --, A.DL
	 , Items.ID as ItemID
	 , delCol
	 --, Items.ali
INTO ##Pages
from (
	select id
		, name
		, projectid
		, ISNULL(DATALENGTH(IMAGE), 0) as DL				
		, delCol
	from ProjectManifestTable as PMT with (NOLOCK)
	where location not like '%background%'
		--and delCol = 1
 ) A

LEFT JOIN								
(
      SELECT ID, ali
      FROM ItemCustomQuestionTable with (NOLOCK)
      UNION
      SELECT ID, ali
      FROM ItemGraphicTable with (NOLOCK)
      UNION
      SELECT ID, ali
      FROM ItemVideoTable with (NOLOCK)
      UNION
      SELECT ID, ali
      FROM ItemHotSpotTable with (NOLOCK)
)  as Items
ON 
      Items.ID LIKE CONVERT(NVARCHAR(10), A.ProjectId) + 'P%' 
      AND 
      (
            CONVERT(NVARCHAR(50), A.ID) = ali
            OR
            SUBSTRING(A.name, 0, LEN(A.name) - 3) = ali
      )
      
--where Items.ID IS NOT NULL;


CREATE INDEX [IX_Pages] on [##Pages] (
	ID
	, ProjectID
) INCLUDE (
	delCol
)

/*




SELECT * FROM ProjectManifestTable where ProjectId = 867
select * from ProjectListTable where id  = 867
*/






































/*
CREATE TABLE #NewItemXML (
	ProjectID INT
	,ItemXML XML
	);

INSERT INTO #NewItemXML
SELECT ProjectManifestTable.ProjectId
	,CAST((
			SELECT ProjectManifestTable.ID AS [@id]
				,ProjectManifestTable.NAME AS [@name]
				,CASE 
					WHEN ProjectManifestTable.[source] IS NULL
						THEN 0
					ELSE 1
					END AS [@visible]
				,SUBSTRING(ProjectManifestTable.NAME, (CHARINDEX(N'.', ProjectManifestTable.NAME) + 1), LEN(ProjectManifestTable.NAME)) AS [@type]
				,CASE 
					WHEN ProjectManifestTable.[source] IS NULL
						THEN N''
					ELSE ProjectManifestTable.[source]
					END AS [@source]
			FOR XML PATH('item')
			) AS XML) NewItemXML
FROM dbo.ProjectManifestTable
INNER JOIN ##Pages 
	ON ##Pages.ID = ProjectManifestTable.ID 
	AND ##Pages.ProjectID = ProjectManifestTable.ProjectID
LEFT JOIN (
	SELECT X.ProjectID
		,sl.i.value('@id', 'int') ItemID
	FROM (
		SELECT SharedLibraryTable.ProjectID
			,CAST(SharedLibraryTable.structureXML AS XML) AS [StructureXML]
		FROM dbo.SharedLibraryTable
		) X
	CROSS APPLY X.StructureXML.nodes('//item') sl(i)
	) X ON ProjectManifestTable.ProjectId = X.ProjectID
	AND ProjectManifestTable.ID = X.ItemID
INNER JOIN dbo.SharedLibraryTable ON ProjectManifestTable.ProjectID = SharedLibraryTable.ProjectID
WHERE ProjectManifestTable.sharedLib = 1
	AND X.ProjectID IS NULL;

CREATE TABLE #NewStructureXML (
	ProjectID INT
	,StructureXML XML
	);

INSERT INTO #NewStructureXML
SELECT SharedLibraryTable.ProjectID
	,CAST(SharedLibraryTable.structureXML AS XML)
FROM dbo.SharedLibraryTable
WHERE SharedLibraryTable.ProjectID IN (
		SELECT ProjectID
		FROM #NewItemXML
		);

DECLARE Items CURSOR
FOR
SELECT ProjectID
	,ItemXML
FROM #NewItemXML;

OPEN Items;

DECLARE @ProjectID INT
	,@ItemXML XML;

FETCH NEXT
FROM Items
INTO @ProjectID
	,@ItemXML;

WHILE @@FETCH_STATUS = 0
BEGIN
	DECLARE @StructureXML XML;

	UPDATE #NewStructureXML
	SET StructureXML.modify('insert sql:variable("@ItemXML") as first into ((/sharedLibrary)[1])')
	WHERE ProjectID = @ProjectID;

	FETCH NEXT
	FROM Items
	INTO @ProjectID
		,@ItemXML;
END

CLOSE Items;

DEALLOCATE Items;


SELECT * FROM ##Pages where itemID IS NOT NULL
SELECT * FROM #NewStructureXML where projectID = 872

SELECT distinct Username 
FROM UserTokenStore
INNER JOIN UserTable T
on T.UserID = UserTokenStore.userId


UPDATE SharedLibraryTable
SET StructureXML = CONVERT(NVARCHAR(MAX),N.StructureXML)
FROM SharedLibraryTable
INNER JOIN #NewStructureXML N
ON SharedLibraryTable.ProjectID = N.ProjectID;


UPDATE ProjectManifestTable
Set delCol = 0
FROM ProjectManifestTable A
INNER Join ##Pages B
on B.ID = A.ID
	And B.ProjectID = A.ProjectId;

*/
--SELECT *
--FROM #NewStructureXML order by 1;


