USE AAT_ItemBank
GO
CREATE VIEW [Adhoc_report_vw] 
AS(
	SELECT 
		qt.ID [qualificationID]
		, at.ID	[examID]
		, agt.MarkingAutoVoidPeriod
	FROM dbo.AssessmentGroupTable agt
	INNER JOIN dbo.QualificationTable qt
	ON qt.ID = agt.QualificationID
	inner JOIN dbo.AssessmentTable at
	ON at.AssessmentGroupID = agt.ID
	)
GO



SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

USE AAT_SecureAssess

SELECT State9.ExamSessionID
	,ib.QualificationName
	,scet.examName
	,est.KeyCode
	,ut.Forename
	,ut.Surname
	,ut.CandidateRef [Membership No.]
	,ct.CentreName
	,ct.CentreCode
	,State9.StateChangeDate [State9]
	,State10.StateChangeDate [State10]
	,est.MarkingAutoVoidPeriod
	,DATEDIFF(DAY,State9.StateChangeDate,State10.StateChangeDate) [DateDiff]
FROM 
	(
SELECT ExamSessionID
	, NewState
	, MIN(StateChangeDate) StateChangeDate
FROM dbo.ExamStateChangeAuditTable
WHERE NewState = 10
	AND StateInformation.exist('stateChangeInformation[reason=8]') = 1
GROUP BY ExamSessionID
	, NewState
) State10
INNER JOIN (
SELECT ExamSessionID
	, NewState
	, MIN(StateChangeDate) StateChangeDate
FROM dbo.ExamStateChangeAuditTable
WHERE  NewState = 9
GROUP BY ExamSessionID
	, NewState
) State9
	ON State9.ExamSessionID = State10.examsessionid
INNER JOIN dbo.ExamSessionTable est
ON est.id = State9.ExamSessionID
INNER JOIN dbo.ScheduledExamsTable scet
ON scet.id = est.ScheduledExamID
INNER JOIN dbo.IB3QualificationLookup ib
ON ib.id = scet.qualificationid
INNER JOIN dbo.UserTable ut
ON ut.ID = est.UserID
INNER JOIN dbo.CentreTable ct
ON ct.ID = scet.centreid
WHERE MarkingAutoVoidPeriod <> DATEDIFF(DAY,State9.StateChangeDate,State10.StateChangeDate)


UNION


SELECT A.[ExamSessionID]	
	, A.qualificationName
	, A.examName
	, A.KeyCode
	, A.foreName
	, A.surName
	, A.[Membership No.]
	, A.centreName
	, A.centreCode
	, MIN(a.State9) State9
	, MIN(a.State10) State10
	, MarkingAutoVoidPeriod
	, DATEDIFF(DAY,State9,State10)  [DateDiff]

FROM (
	SELECT ID [ExamSessionID]
		, wests.qualificationName
		, wests.examName
		, west.KeyCode
		, wests.foreName
		, wests.surName
		, wests.candidateRef [Membership No.]
		, wests.centreName
		, wests.centreCode
		, wests.qualificationid
		, wests.examId
		, MarkingAutoVoidPeriod
		, west.ExamStateChangeAuditXml.value('data(exam/stateChange[newStateID=9]/changeDate)[1]','datetime')  [State9]
		, west.ExamStateChangeAuditXml.value('data(exam/stateChange[newStateID=10]/changeDate)[1]','datetime')  [State10]
	FROM dbo.WAREHOUSE_ExamSessionTable west
	INNER JOIN dbo.WAREHOUSE_ExamSessionTable_Shreded wests
	ON west.id = wests.examSessionId
	INNER JOIN (
	
	SELECT *
	FROM AAT_ItemBank.dbo.[Adhoc_report_vw]

	) IB
	ON ib.[qualificationID] = wests.[qualificationid]
		AND ib.[examID] = wests.examID

	WHERE west.warehouseTime >  DATEADD(YEAR,-2,GETDATE())
		AND wests.examStateInformation.exist('stateChangeInformation[reason=8]') = 1
		AND ExamStateChangeAuditXml.exist('exam/stateChange[newStateID=(9,10)]') = 1
) A
WHERE MarkingAutoVoidPeriod <> DATEDIFF(DAY,State9,State10)

GROUP BY [ExamSessionID]	
	, A.qualificationName
	, A.examName
	, A.KeyCode
	, A.foreName
	, A.surName
	, A.[Membership No.]
	, A.centreName
	, A.centreCode
	,A.State9
	,A.State10
	,A.MarkingAutoVoidPeriod

USE [AAT_ItemBank]
GO
DROP VIEW [Adhoc_report_vw];