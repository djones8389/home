SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

select 
	SUBSTRING(ITT.ID, 0, CHARINDEX('P', ITT.ID)) [ProjectID]
	, PLT.[Name] [Project Name]
	, SUBSTRING(PT.ID, CHARINDEX('P', PT.ID)+1,LEN(PT.ID)) [PageID]
	, ITT.ID [FullID]
	, CONVERT(xml, ITT.ItemValue) html
from ItemTextBoxTable ITT
inner join ProjectListTable PLT
on PLT.id = SUBSTRING(ITT.ID, 0, CHARINDEX('P', ITT.ID))
inner join PageTable PT
on PT.ID = SUBSTRING(ITT.ID, 0, CHARINDEX('S', ITT.ID))
where CONVERT(xml, ITT.ItemValue).exist('ItemValue/P') = 1
	order by 2;
