-- =============================================
-- Author:		Umair Rafique
-- Create date: 08/02/2011
-- Description:	To updated WAREHOUSE_ExamStateAuditTable whenever WarehouseExamState column is changed
-- Modified by Awais: 02/06/2011 Updating [WAREHOUSE_ExamSessionTable_Shreded] whenever WarehouseExamState column is changed
-- =============================================
CREATE TRIGGER [dbo].[WarehouseExamState_Trigger]
   ON  [dbo].[WAREHOUSE_ExamSessionTable] 
   AFTER INSERT,UPDATE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from	
	SET NOCOUNT ON;
	
	Declare @WarehouseExamSessionID Int
	Declare @WarehouseExamState Int
	
	SELECT @WarehouseExamSessionID = ID,
		   @WarehouseExamState = WarehouseExamState
	FROM INSERTED
	
	
	--IF NOT EXISTS (SELECT * FROM [dbo].[WAREHOUSE_ExamStateAuditTable] 
	--				Where [WarehouseExamSessionID] = @WarehouseExamSessionID
	--					AND [WarehouseExamState] = @WarehouseExamState)		   
							
		INSERT INTO [dbo].[WAREHOUSE_ExamStateAuditTable]
			   ([WarehouseExamSessionID]
			   ,[ExamState]
			   ,[Date])     
			SELECT	[ID],
					[WarehouseExamState],
					GetDate()
			FROM INSERTED
    
		UPDATE [dbo].[WAREHOUSE_ExamSessionTable_Shreded]
		   SET [WarehouseExamState] = INSERTED.[WarehouseExamState]
        FROM INSERTED,[WAREHOUSE_ExamSessionTable_Shreded]
		WHERE [WAREHOUSE_ExamSessionTable_Shreded].[examSessionId]=INSERTED.[ID]    
		
		IF UPDATE([WarehouseExamState])
		--When the [WarehouseExamState] changes, we change the [warehouseTime] to now (so it can be picked up in getExamResults integration web method
		BEGIN
			UPDATE [dbo].[WAREHOUSE_ExamSessionTable]
			   SET [warehouseTime] = GetDate()
			   FROM INSERTED
			 WHERE [WAREHOUSE_ExamSessionTable].[ID]=INSERTED.[ID] 
		END
    --END
END
GO

/*Extended properties*/
EXECUTE sp_addextendedproperty @name = N'MS_Description', 
    @value = N'A boolean denoting if the exam session has been exported to an external system. The value can only be 1 if the value in live was 1 (set by integration) or it can be set to 1 (via integration) in the warehouse', 
    @level0type = N'SCHEMA', 
    @level0name = N'dbo', 
    @level1type = N'TABLE', 
    @level1name = N'WAREHOUSE_ExamSessionTable', 
    @level2type = N'COLUMN', 
    @level2name = N'submissionExported';

EXECUTE sp_addextendedproperty @name = N'MS_Description', 
    @value = N' ', 
    @level0type = N'SCHEMA', 
    @level0name = N'dbo', 
    @level1type = N'TABLE', 
    @level1name = N'WAREHOUSE_ExamSessionTable', 
    @level2type = N'COLUMN', 
    @level2name = N'TargetedForVoid';

EXECUTE sp_addextendedproperty @name = N'MS_Description', 
    @value = N' ', 
    @level0type = N'SCHEMA', 
    @level0name = N'dbo', 
    @level1type = N'TABLE', 
    @level1name = N'WAREHOUSE_ExamSessionTable_Shreded', 
    @level2type = N'COLUMN', 
    @level2name = N'examSessionId';

/*FK*/
ALTER TABLE [dbo].[WAREHOUSE_ExamSessionTable_ShreddedItems_Mark] WITH NOCHECK
    ADD CONSTRAINT [FK_WAREHOUSE_ExamSessionTable_ShreddedItems_Mark_WAREHOUSE_ExamSessionTable_ShrededItems]
    FOREIGN KEY ([ExamSessionID], [ItemID]) 
    REFERENCES [dbo].[WAREHOUSE_ExamSessionTable_ShrededItems] ([examSessionId], [ItemRef]);
