/*Notes: This script drops the original tables with 
    large data and renames the temporary tables.*/

/*TODO - We will need to deal with FK's and ordering when data is involved*/

/*Drop tables*/
DROP TABLE dbo.WAREHOUSE_ExamSessionTable_ShreddedItems_Mark;
DROP TABLE dbo.WAREHOUSE_ExamSessionTable_ShrededItems; --Has FK
DROP TABLE dbo.WAREHOUSE_ExamSessionTable_Shreded;
DROP TABLE dbo.WAREHOUSE_ExamSessionAvailableItemsTable;
DROP TABLE dbo.WAREHOUSE_ExamSessionDocumentTable;
DROP TABLE dbo.WAREHOUSE_ExamSessionItemResponseTable;
DROP TABLE dbo.WAREHOUSE_ExamSessionResultHistoryTable;
DROP TABLE dbo.WAREHOUSE_ExamStateAuditTable;
DROP TABLE dbo.WAREHOUSE_ExamSessionTable;--Has FK
DROP TABLE dbo.WAREHOUSE_ScheduledExamsTable;--Has FK

/*Rename tables*/
EXECUTE sp_rename @objname = N'TEMP_WAREHOUSE_ExamSessionTable_ShreddedItems_Mark', @newname = N'WAREHOUSE_ExamSessionTable_ShreddedItems_Mark';
EXECUTE sp_rename @objname = N'TEMP_WAREHOUSE_ExamSessionTable_ShrededItems', @newname = N'WAREHOUSE_ExamSessionTable_ShrededItems';
EXECUTE sp_rename @objname = N'TEMP_WAREHOUSE_ExamSessionTable_Shreded', @newname = N'WAREHOUSE_ExamSessionTable_Shreded';
EXECUTE sp_rename @objname = N'TEMP_WAREHOUSE_ExamSessionAvailableItemsTable', @newname = N'WAREHOUSE_ExamSessionAvailableItemsTable';
EXECUTE sp_rename @objname = N'TEMP_WAREHOUSE_ExamSessionDocumentTable', @newname = N'WAREHOUSE_ExamSessionDocumentTable';
EXECUTE sp_rename @objname = N'TEMP_WAREHOUSE_ExamSessionItemResponseTable', @newname = N'WAREHOUSE_ExamSessionItemResponseTable';
EXECUTE sp_rename @objname = N'TEMP_WAREHOUSE_ExamSessionResultHistoryTable', @newname = N'WAREHOUSE_ExamSessionResultHistoryTable';
EXECUTE sp_rename @objname = N'TEMP_WAREHOUSE_ExamStateAuditTable', @newname = N'WAREHOUSE_ExamStateAuditTable';
EXECUTE sp_rename @objname = N'TEMP_WAREHOUSE_ExamSessionTable', @newname = N'WAREHOUSE_ExamSessionTable';
EXECUTE sp_rename @objname = N'TEMP_WAREHOUSE_ScheduledExamsTable', @newname = N'WAREHOUSE_ScheduledExamsTable';

DROP TABLE dbo.TEMP_WAREHOUSEExamsToKeep;