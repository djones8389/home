USE [PPD_BCIntegration_SecureAssess]


ALTER TABLE [WAREHOUSE_ExamSessionItemResponseTable] DROP CONSTRAINT [DF_WAREHOUSE_ExamSessionItemResponseTable_MarkingIgnored];
ALTER TABLE [WAREHOUSE_ExamSessionTable] DROP CONSTRAINT [DF_WAREHOUSE_ExamSessionTable_IsProjectBased];
ALTER TABLE [WAREHOUSE_ExamSessionTable] DROP CONSTRAINT [DF_WAREHOUSE_ExamSessionTable_CertifiedForTablet];
ALTER TABLE [WAREHOUSE_ExamSessionTable] DROP CONSTRAINT [DF_WAREHOUSE_ExamSessionTable_AllowPackageDelivery];
ALTER TABLE [WAREHOUSE_ExamSessionTable] DROP CONSTRAINT [DF_WAREHOUSE_ExamSessionTable_appeal];
ALTER TABLE [WAREHOUSE_ExamSessionTable] DROP CONSTRAINT [DF_WAREHOUSE_ExamSessionTable_availableForCentreReview];
ALTER TABLE [WAREHOUSE_ExamSessionTable] DROP CONSTRAINT [DF_WAREHOUSE_ExamSessionTable_ContainsBTLOffice];
ALTER TABLE [WAREHOUSE_ExamSessionTable] DROP CONSTRAINT [DF_WAREHOUSE_ExamSessionTable_EnableOverrideMarking];
ALTER TABLE [WAREHOUSE_ExamSessionTable] DROP CONSTRAINT [DF_WAREHOUSE_ExamSessionTable_ExportedToIntegration];
ALTER TABLE [WAREHOUSE_ExamSessionTable] DROP CONSTRAINT [DF_WAREHOUSE_ExamSessionTable_ExportToSecureMarker];
ALTER TABLE [WAREHOUSE_ExamSessionTable] DROP CONSTRAINT [DF_WAREHOUSE_ExamSessionTable_remarkStatus];
ALTER TABLE [WAREHOUSE_ExamSessionTable] DROP CONSTRAINT [DF_WAREHOUSE_ExamSessionTable_submissionExported];
ALTER TABLE [WAREHOUSE_ExamSessionTable] DROP CONSTRAINT [DF_WAREHOUSE_ExamSessionTable_WarehouseExamState];
ALTER TABLE [WAREHOUSE_ExamSessionTable] DROP CONSTRAINT [DF_WAREHOUSE_ExamSessionTable_itemMarksUploaded];
ALTER TABLE [WAREHOUSE_ExamSessionTable] DROP CONSTRAINT [DF_WAREHOUSE_ExamSessionTable_TakenThroughLocalScan];
ALTER TABLE [WAREHOUSE_ExamSessionTable_Shreded] DROP CONSTRAINT [DF_WAREHOUSE_ExamSessionTable_Shreded_CertifiedForTablet];
ALTER TABLE [WAREHOUSE_ExamSessionTable_Shreded] DROP CONSTRAINT [DF_WAREHOUSE_ExamSessionTable_Shreded_IsProjectBased];
ALTER TABLE [WAREHOUSE_ExamSessionTable_Shreded] DROP CONSTRAINT [DF_WAREHOUSE_ExamSessionTable_Shreded_WarehouseExamState];
ALTER TABLE [WAREHOUSE_ExamSessionTable_Shreded] DROP CONSTRAINT [DF_WAREHOUSE_ExamSessionTable_Shreded_KeyCode];
ALTER TABLE [WAREHOUSE_ExamSessionTable_Shreded] DROP CONSTRAINT [DF_WAREHOUSE_ExamSessionTable_Shreded_EnableOverrideMarking];
ALTER TABLE [WAREHOUSE_ExamSessionTable_Shreded] DROP CONSTRAINT [DF_WAREHOUSE_ExamSessionTable_Shreded_IsExternal];
ALTER TABLE [WAREHOUSE_ExamSessionTable_Shreded] DROP CONSTRAINT [DF_WAREHOUSE_ExamSessionTable_Shreded_itemMarksUploaded];
ALTER TABLE [WAREHOUSE_ExamSessionTable_Shreded] DROP CONSTRAINT [DF_WAREHOUSE_ExamSessionTable_Shreded_TakenThroughLocalScan];
ALTER TABLE [WAREHOUSE_ExamSessionTable_Shreded] DROP CONSTRAINT [DF_WAREHOUSE_ExamSessionTable_Shreded_ExportToSecureMarker];
ALTER TABLE [WAREHOUSE_ExamSessionTable_Shreded] DROP CONSTRAINT [DF_WAREHOUSE_ExamSessionTable_Shreded_AllowPackageDelivery];
ALTER TABLE [WAREHOUSE_ExamSessionTable_Shreded] DROP CONSTRAINT [DF_WAREHOUSE_ExamSessionTable_Shreded_ExportedToIntegration];
ALTER TABLE [WAREHOUSE_ExamSessionTable_Shreded] DROP CONSTRAINT [DF_WAREHOUSE_ExamSessionTable_Shreded_ContainsBTLOffice];
ALTER TABLE [WAREHOUSE_ExamSessionTable_Shreded] DROP CONSTRAINT [DF_WAREHOUSE_ExamSessionTable_Shreded_qualificationLevel];
ALTER TABLE [WAREHOUSE_ExamSessionTable_ShrededItems] DROP CONSTRAINT [DF__WAREHOUSE__Total__28A2FA0E];
ALTER TABLE [WAREHOUSE_ScheduledExamsTable] DROP CONSTRAINT [DF_WAREHOUSE_ScheduledExamsTable_IsExternal];

/*select  
	t.name
  , dc.name [DCNAME]
  , 'ALTER TABLE ' + QUOTENAME(t.name) + ' DROP CONSTRAINT ' + quotename(dc.name) + ';'
--, kc.name [KCNAME]
from sys.tables t
inner join sys.objects o
on o.object_id = t.object_id
inner join sys.default_constraints dc
on dc.parent_object_id = o.object_id
--inner join sys.key_constraints kc
--on kc.parent_object_id = o.object_id
order by 1
*/

