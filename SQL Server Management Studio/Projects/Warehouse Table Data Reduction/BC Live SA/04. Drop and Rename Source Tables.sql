ALTER TABLE [WAREHOUSE_ScheduledExamsTable] DROP CONSTRAINT [FK_WAREHOUSE_ScheduledExamsTable_WAREHOUSE_UserTable];
ALTER TABLE [WAREHOUSE_ScheduledExamsTable] DROP CONSTRAINT [FK_WAREHOUSE_ScheduledExamsTable_WAREHOUSE_CentreTable];
ALTER TABLE [WAREHOUSE_ExamSessionTable] DROP CONSTRAINT [FK_WAREHOUSE_ExamSessionTable_WAREHOUSE_UserTable];
ALTER TABLE [WAREHOUSE_ExamSessionTable] DROP CONSTRAINT [PK_WAREHOUSE_ExamSessionTable_1];
ALTER TABLE [WAREHOUSE_ExamSessionTable] DROP CONSTRAINT [FK_WAREHOUSE_ExamSessionTable_WAREHOUSE_ScheduledExamsTable];
ALTER TABLE [WAREHOUSE_ExamSessionTable] DROP CONSTRAINT [FK_WAREHOUSE_ExamSessionTable_WAREHOUSE_ExamStateLookupTable];
ALTER TABLE [WAREHOUSE_ExamSessionTable_ShrededItems] DROP CONSTRAINT [FK_WAREHOUSE_ExamSessionTable_ShrededItems_WAREHOUSE_ExamSessionTable_Shreded];
ALTER TABLE [WAREHOUSE_ExamSessionTable_Shreded] DROP CONSTRAINT [FK_WAREHOUSE_ExamSessionTable_Shreded_WAREHOUSE_ExamSessionTable];
ALTER TABLE [WAREHOUSE_ExamSessionDocumentTable] DROP CONSTRAINT [FK_WAREHOUSE_ExamSessionDocumentTable_WAREHOUSE_ExamSessionTable];

ALTER TABLE [WAREHOUSE_ExamSessionTable_ShrededItems] DROP CONSTRAINT [PK_WAREHOUSE_ExamSessionTable_ShrededItems];
ALTER TABLE [WAREHOUSE_ExamSessionTable_Shreded] DROP CONSTRAINT [PK_WAREHOUSE_ExamSessionTable_Shreded];
ALTER TABLE [WAREHOUSE_ExamSessionTable] DROP CONSTRAINT [PK_WAREHOUSE_ExamSessionTable_1];
ALTER TABLE [WAREHOUSE_ExamSessionDocumentTable] DROP CONSTRAINT [PK_WAREHOUSE_ExamSessionDocumentTable];


--ALTER TABLE [dbo].[WAREHOUSE_ExamSessionDurationAuditTable] DROP CONSTRAINT [FK_WAREHOUSE_ExamSessionDurationAuditTable_WAREHOUSE_ExamSessionTable]
--GO

----ALTER TABLE [WAREHOUSE_ExamSessionTable] DROP CONSTRAINT [PK_WAREHOUSE_ExamSessionTable_1];

--DROP TABLE [WAREHOUSE_ExamSessionTable];

/*
*****Need to rebuild this*****

ALTER TABLE [dbo].[WAREHOUSE_ExamSessionDurationAuditTable]  WITH CHECK ADD  CONSTRAINT [FK_WAREHOUSE_ExamSessionDurationAuditTable_WAREHOUSE_ExamSessionTable] FOREIGN KEY([Warehoused_ExamSessionID])
REFERENCES [dbo].[WAREHOUSE_ExamSessionTable] ([ID])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[WAREHOUSE_ExamSessionDurationAuditTable] CHECK CONSTRAINT [FK_WAREHOUSE_ExamSessionDurationAuditTable_WAREHOUSE_ExamSessionTable]
GO

----ALTER TABLE [dbo].[WAREHOUSE_CandidateReviewTable] DROP CONSTRAINT [FK_WAREHOUSE_CandiateReviewTable_WAREHOUSE_ExamSessionTable_Shreded]
----GO

ALTER TABLE [dbo].[WAREHOUSE_CandidateReviewTable]  WITH CHECK ADD  CONSTRAINT [FK_WAREHOUSE_CandiateReviewTable_WAREHOUSE_ExamSessionTable_Shreded] FOREIGN KEY([ExamSessionID])
REFERENCES [dbo].[WAREHOUSE_ExamSessionTable_Shreded] ([examSessionId])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[WAREHOUSE_CandidateReviewTable] CHECK CONSTRAINT [FK_WAREHOUSE_CandiateReviewTable_WAREHOUSE_ExamSessionTable_Shreded]
GO



----ALTER TABLE [dbo].[WAREHOUSE_ExamSessionTable_ShreddedItems_Mark] DROP CONSTRAINT [FK_WAREHOUSE_ExamSessionTable_ShreddedItems_Mark_WAREHOUSE_ExamSessionTable_ShrededItems]
----GO

ALTER TABLE [dbo].[WAREHOUSE_ExamSessionTable_ShreddedItems_Mark]  WITH CHECK ADD  CONSTRAINT [FK_WAREHOUSE_ExamSessionTable_ShreddedItems_Mark_WAREHOUSE_ExamSessionTable_ShrededItems] FOREIGN KEY([ExamSessionID], [ItemID])
REFERENCES [dbo].[WAREHOUSE_ExamSessionTable_ShrededItems] ([examSessionId], [ItemRef])
GO

ALTER TABLE [dbo].[WAREHOUSE_ExamSessionTable_ShreddedItems_Mark] CHECK CONSTRAINT [FK_WAREHOUSE_ExamSessionTable_ShreddedItems_Mark_WAREHOUSE_ExamSessionTable_ShrededItems]
GO

----ALTER TABLE [dbo].[WAREHOUSE_ExamSessionDocumentInfoTable] DROP CONSTRAINT [FK_WAREHOUSE_ExamSessionDocumentInfoTable_WAREHOUSE_ExamSessionDocumentTable]
----GO

ALTER TABLE [dbo].[WAREHOUSE_ExamSessionDocumentInfoTable]  WITH CHECK ADD  CONSTRAINT [FK_WAREHOUSE_ExamSessionDocumentInfoTable_WAREHOUSE_ExamSessionDocumentTable] FOREIGN KEY([Warehoused_ExamSessionDocumentID])
REFERENCES [dbo].[WAREHOUSE_ExamSessionDocumentTable] ([ID])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[WAREHOUSE_ExamSessionDocumentInfoTable] CHECK CONSTRAINT [FK_WAREHOUSE_ExamSessionDocumentInfoTable_WAREHOUSE_ExamSessionDocumentTable]
GO



*/




--DROP TABLE [WAREHOUSE_ExamSessionAvailableItemsTable];
--DROP TABLE [WAREHOUSE_ExamSessionDocumentTable];
--DROP TABLE [WAREHOUSE_ExamSessionItemResponseTable];
--DROP TABLE [WAREHOUSE_ExamSessionResultHistoryTable];

--DROP TABLE [WAREHOUSE_ExamSessionTable_Shreded];
--DROP TABLE [WAREHOUSE_ExamSessionTable_ShrededItems];
--DROP TABLE [WAREHOUSE_ExamStateAuditTable];
--DROP TABLE [WAREHOUSE_ScheduledExamsTable];

/*

select 
	distinct
	o.name
	, fk.name
	, 'ALTER TABLE ' + QUOTENAME(o.name) + ' DROP CONSTRAINT ' + QUOTENAME(fk.name) + ';'
from sys.foreign_key_columns FKC
inner join sys.objects o
on fkc.parent_object_id = o.object_id
inner join sys.foreign_keys fk
on fk.parent_object_id = o.object_id
order by fk.name desc

*/


select  'ALTER TABLE ' + QUOTENAME(o.name) + ' DROP CONSTRAINT ' + QUOTENAME(i.name) + ';' 
from sys.indexes I
inner join sys.objects o
on i.object_id = o.object_id
where is_primary_key = 1
	order by o.name desc





exec sp_rename 'temp_WAREHOUSE_ExamSessionAvailableItemsTable','WAREHOUSE_ExamSessionAvailableItemsTable';
exec sp_rename 'temp_WAREHOUSE_ExamSessionItemResponseTable','WAREHOUSE_ExamSessionItemResponseTable';
exec sp_rename 'temp_WAREHOUSE_ExamSessionResultHistoryTable','WAREHOUSE_ExamSessionResultHistoryTable';
exec sp_rename 'temp_WAREHOUSE_ExamStateAuditTable','WAREHOUSE_ExamStateAuditTable';

exec sp_rename 'temp_WAREHOUSE_ExamSessionDocumentTable','WAREHOUSE_ExamSessionDocumentTable';
exec sp_rename 'temp_WAREHOUSE_ExamSessionTable','WAREHOUSE_ExamSessionTable';
exec sp_rename 'temp_WAREHOUSE_ExamSessionTable_Shreded','WAREHOUSE_ExamSessionTable_Shreded';
exec sp_rename 'temp_WAREHOUSE_ExamSessionTable_ShrededItems','WAREHOUSE_ExamSessionTable_ShrededItems';
exec sp_rename 'temp_WAREHOUSE_ScheduledExamsTable','WAREHOUSE_ScheduledExamsTable';

