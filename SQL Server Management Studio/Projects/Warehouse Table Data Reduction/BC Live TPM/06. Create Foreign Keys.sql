use  [PPD_BCIntegration_TestPackage];

ALTER TABLE [dbo].[ScheduledPackageCandidateExamItems]  WITH CHECK ADD  CONSTRAINT [FK_ScheduledPackageCandidateExamItems_ScheduledPackageCandidateExams_CandidateExam_ScheduledPackageCandidateExamId] FOREIGN KEY([CandidateExam_ScheduledPackageCandidateExamId])
REFERENCES [dbo].[ScheduledPackageCandidateExams] ([ScheduledPackageCandidateExamId])
GO
ALTER TABLE [dbo].[ScheduledPackageCandidateExamItems] CHECK CONSTRAINT [FK_ScheduledPackageCandidateExamItems_ScheduledPackageCandidateExams_CandidateExam_ScheduledPackageCandidateExamId]
GO
ALTER TABLE [dbo].[ScheduledPackageCandidateExams]  WITH CHECK ADD  CONSTRAINT [FK_ScheduledPackageCandidateExams_PackageExams_PackageExamId] FOREIGN KEY([PackageExamId])
REFERENCES [dbo].[PackageExams] ([PackageExamId])
GO
ALTER TABLE [dbo].[ScheduledPackageCandidateExams] CHECK CONSTRAINT [FK_ScheduledPackageCandidateExams_PackageExams_PackageExamId]
GO
ALTER TABLE [dbo].[ScheduledPackageCandidateExams]  WITH CHECK ADD  CONSTRAINT [FK_ScheduledPackageCandidateExams_ScheduledPackageCandidates_Candidate_ScheduledPackageCandidateId] FOREIGN KEY([Candidate_ScheduledPackageCandidateId])
REFERENCES [dbo].[ScheduledPackageCandidates] ([ScheduledPackageCandidateId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ScheduledPackageCandidateExams] CHECK CONSTRAINT [FK_ScheduledPackageCandidateExams_ScheduledPackageCandidates_Candidate_ScheduledPackageCandidateId]
GO
ALTER TABLE [dbo].[ScheduledPackageCandidates]  WITH CHECK ADD  CONSTRAINT [FK_ScheduledPackageCandidates_ScheduledPackages_ScheduledPackage_ScheduledPackageId] FOREIGN KEY([ScheduledPackage_ScheduledPackageId])
REFERENCES [dbo].[ScheduledPackages] ([ScheduledPackageId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ScheduledPackageCandidates] CHECK CONSTRAINT [FK_ScheduledPackageCandidates_ScheduledPackages_ScheduledPackage_ScheduledPackageId]
GO
ALTER TABLE [dbo].[ScheduledPackages]  WITH CHECK ADD  CONSTRAINT [FK_ScheduledPackages_Packages_Package_PackageId] FOREIGN KEY([PackageId])
REFERENCES [dbo].[Packages] ([PackageId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ScheduledPackages] CHECK CONSTRAINT [FK_ScheduledPackages_Packages_Package_PackageId]
GO
