/*Notes: This script drops the original tables with 
    large data and renames the temporary tables.*/

/*TODO - We will need to deal with FK's and ordering when data is involved*/

/*Drop tables*/

USE [PPD_BCIntegration_TestPackage]

DROP TABLE [dbo].[ScheduledPackageCandidateExamItems];
DROP TABLE [dbo].[ScheduledPackageCandidateExams];
DROP TABLE [dbo].[ScheduledPackageCandidates];
DROP TABLE [dbo].[ScheduledPackages];


/*Rename tables*/
EXECUTE sp_rename @objname = N'temp_ScheduledPackageCandidateExamItems', @newname = N'ScheduledPackageCandidateExamItems';
EXECUTE sp_rename @objname = N'temp_ScheduledPackageCandidateExams', @newname = N'ScheduledPackageCandidateExams';
EXECUTE sp_rename @objname = N'temp_ScheduledPackageCandidates', @newname = N'ScheduledPackageCandidates';
EXECUTE sp_rename @objname = N'temp_ScheduledPackages', @newname = N'ScheduledPackages';
