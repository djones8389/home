USE [PPD_BCIntegration_SecureMarker]



ALTER TABLE [AssignedGroupMarks] DROP CONSTRAINT [DF_AssignedGroupMarks_EscalatedWithoutMark];
ALTER TABLE [AssignedGroupMarks] DROP CONSTRAINT [DF__AssignedG__IsCon__1B7E091A];
ALTER TABLE [AssignedGroupMarks] DROP CONSTRAINT [DF__AssignedG__Disre__1C722D53];
ALTER TABLE [AssignedGroupMarks] DROP CONSTRAINT [DF__AssignedG__IsEsc__49954745];
ALTER TABLE [AssignedGroupMarks] DROP CONSTRAINT [DF_AssignedGroupMarks_IsReportableCI];
ALTER TABLE [CandidateExamVersions] DROP CONSTRAINT [DF_CandidateExamVersions_IsModerated];
ALTER TABLE [CandidateExamVersions] DROP CONSTRAINT [DF_CandidateExamVersions_IsFlagged];
ALTER TABLE [CandidateExamVersions] DROP CONSTRAINT [DF_CandidateExamVersions_ExamSessionState];
ALTER TABLE [CandidateExamVersions] DROP CONSTRAINT [DF_CandidateExamVersions_VIP];
ALTER TABLE [CandidateExamVersions] DROP CONSTRAINT [DF__Candidate__Scrip__5F492382];
ALTER TABLE [CandidateExamVersions] DROP CONSTRAINT [DF__CandidateE__Mark__41E93885];
ALTER TABLE [CandidateExamVersions] DROP CONSTRAINT [DF__Candidate__Total__42DD5CBE];
ALTER TABLE [Exams] DROP CONSTRAINT [DF_Exams_IsDecimalMarksEnabled];
ALTER TABLE [Exams] DROP CONSTRAINT [DF_Exams_DefaultMaximumCIsMarkedOutOfToleranceInRollingReviewItemLevel];
ALTER TABLE [Exams] DROP CONSTRAINT [DF_Exams_DeafultMaxConsecutiveCIsThatCanBeMarkedOutOfTolerance];
ALTER TABLE [Exams] DROP CONSTRAINT [DF_Exams_DefaultNumberOfCIsInRollingReview];
ALTER TABLE [Exams] DROP CONSTRAINT [DF_Exams_DefaultMaximumCIsMarkedOutOfToleranceInRollingReview];
ALTER TABLE [Exams] DROP CONSTRAINT [DF_Exams_DefaultMaximumCumulativeErrorWithinCompetenceRunItemLevel];
ALTER TABLE [Exams] DROP CONSTRAINT [DF_Exams_DeafultMaxConsecutiveCIsThatCanBeMarkedOutOfToleranceItemLevel];
ALTER TABLE [Exams] DROP CONSTRAINT [DF_Exams_AutoCIReview];
ALTER TABLE [Exams] DROP CONSTRAINT [DF_Exams_DefaultAllowAnnotationsInMarking];
ALTER TABLE [Exams] DROP CONSTRAINT [DF_Exams_DefaultNoOfCIsRequired];
ALTER TABLE [Exams] DROP CONSTRAINT [DF_Exams_DefaultTotalItemsInCompetencyRun];
ALTER TABLE [Exams] DROP CONSTRAINT [DF_Exams_DefaultCorrectItemsWithinToleranceWithinCompetenceRunDefault];
ALTER TABLE [Exams] DROP CONSTRAINT [DF_Exams_DefaultMaximumCumulativeErrorWithinCompetenceRun];
ALTER TABLE [Exams] DROP CONSTRAINT [DF_Exams_DefaultProbabilityOfGettingCI];
ALTER TABLE [Exams] DROP CONSTRAINT [DF_Exams_DefaultMaximumResponseBeforePresentingCI];
ALTER TABLE [Exams] DROP CONSTRAINT [DF_Exams_ExternalID];
ALTER TABLE [GroupDefinitions] DROP CONSTRAINT [DF_GroupDefinitions_IsAutomaticGroup];
ALTER TABLE [GroupDefinitions] DROP CONSTRAINT [DF_GroupDefinitions_IsFeedbackOnDailyCompetencyCheck];
ALTER TABLE [GroupDefinitions] DROP CONSTRAINT [DF_GroupDefinitions_IsFeedbackOnOngoingCompetencyCheck];
ALTER TABLE [Items] DROP CONSTRAINT [DF_Items_MaximumCIsMarkedOutOfToleranceInRollingReview];
ALTER TABLE [Items] DROP CONSTRAINT [DF_Items_TotalMark];
ALTER TABLE [Items] DROP CONSTRAINT [DF_Items_MaximumCumulativeErrorWitihnQualificationRun];
ALTER TABLE [Items] DROP CONSTRAINT [DF_Items_MaxConsecutiveCIsThatCanBeMarkedOutOfTolerance];
ALTER TABLE [Items] DROP CONSTRAINT [DF_Items_Marking Method];
ALTER TABLE [Items] DROP CONSTRAINT [DF_Items_MarkingType];
ALTER TABLE [Items] DROP CONSTRAINT [DF_Items_CIMarkingTolerance];
ALTER TABLE [Moderations] DROP CONSTRAINT [DF_Moderations_IsActual];
ALTER TABLE [QuotaManagement] DROP CONSTRAINT [DF_QuotaManagement_GroupId];
ALTER TABLE [QuotaManagement] DROP CONSTRAINT [DF__QuotaMana__Numbe__531EB17F];
ALTER TABLE [QuotaManagement] DROP CONSTRAINT [DF_QuotaManagement_MarkingSuspended];
ALTER TABLE [QuotaManagement] DROP CONSTRAINT [DF_QuotaManagement_NumberMarked];
ALTER TABLE [QuotaManagement] DROP CONSTRAINT [DF_QuotaManagement_NumItemsParked];
ALTER TABLE [QuotaManagement] DROP CONSTRAINT [DF_QuotaManagement_Within1CIofSuspension];
ALTER TABLE [UniqueGroupResponses] DROP CONSTRAINT [DF_UniqueGroupResponses_DefaultScriptType];
ALTER TABLE [UniqueGroupResponses] DROP CONSTRAINT [DF_UniqueGroupResponses_IsEscalated];
ALTER TABLE [UniqueResponseDocuments] DROP CONSTRAINT [DF_UniqueResponseDocuments_ExternalDocID];
ALTER TABLE [UniqueResponseDocuments] DROP CONSTRAINT [DF_UniqueResponseDocuments_UploadedDate];
ALTER TABLE [UniqueResponseDocuments] DROP CONSTRAINT [DF_UniqueResponseDocuments_FileName];
ALTER TABLE [UserCompetenceStatus] DROP CONSTRAINT [DF_UserCompetenceStatus_GroupDefinitionID];
ALTER TABLE [UserCompetenceStatus] DROP CONSTRAINT [DF_UserQualifiedStatus_Qualified];

/*select  
	t.name
  , dc.name [DCNAME]
  , 'ALTER TABLE ' + QUOTENAME(t.name) + ' DROP CONSTRAINT ' + quotename(dc.name) + ';'
--, kc.name [KCNAME]
from sys.tables t
inner join sys.objects o
on o.object_id = t.object_id
inner join sys.default_constraints dc
on dc.parent_object_id = o.object_id
--inner join sys.key_constraints kc
--on kc.parent_object_id = o.object_id
order by 1
*/


--select distinct
--	 object_name(object_id)
--	 , row_count
--	 , *
--from sys.dm_db_partition_stats st 
--where object_id > 100
--order by 2 desc
