/*Notes: This script creates indexes on the newly 
    renamed tables to bring them inline with the originals.*/

/*Create indexes?*/
CREATE NONCLUSTERED INDEX [FK_ExamSessionID]
    ON [dbo].[WAREHOUSE_ExamSessionAvailableItemsTable]([ExamSessionID] ASC);

CREATE NONCLUSTERED INDEX [examSessionId_NCI]
    ON [dbo].[WAREHOUSE_ExamSessionItemResponseTable]([WAREHOUSEExamSessionID] ASC);

CREATE NONCLUSTERED INDEX [idx_NC_WAREHOUSE_ExamSessionItemResponseTable_WAREHOUSEExamSessionID_ItemID_ItemVersion]
    ON [dbo].[WAREHOUSE_ExamSessionItemResponseTable]([WAREHOUSEExamSessionID] ASC, [ItemID] ASC, [ItemVersion] ASC);

CREATE NONCLUSTERED INDEX [idx_WAREHOUSE_ExamSessionItemResponseTable_WAREHOUSEExamSessionID]
    ON [dbo].[WAREHOUSE_ExamSessionItemResponseTable]([WAREHOUSEExamSessionID] ASC)
    INCLUDE([ItemID], [ItemResponseData]);

CREATE NONCLUSTERED INDEX [_dta_index_WAREHOUSE_ExamSessionTable_6_317244185__K2_K4]
    ON [dbo].[WAREHOUSE_ExamSessionTable]([ExamSessionID] ASC, [WAREHOUSEUserID] ASC);

CREATE NONCLUSTERED INDEX [idx_WAREHOUSE_ExamSessionTable_KeyCode]
    ON [dbo].[WAREHOUSE_ExamSessionTable]([KeyCode] ASC)
    INCLUDE([ID]);

CREATE NONCLUSTERED INDEX [idx_WAREHOUSE_ExamSessionTable_warehouseTime_WarehouseExamStatePreviousExamState]
    ON [dbo].[WAREHOUSE_ExamSessionTable]([warehouseTime] ASC, [WarehouseExamState] ASC, [PreviousExamState] ASC);

CREATE UNIQUE NONCLUSTERED INDEX [IX_WAREHOUSE_ExamSessionTable]
    ON [dbo].[WAREHOUSE_ExamSessionTable]([ExamSessionID] ASC);

CREATE NONCLUSTERED INDEX [IX_WAREHOUSE_ExamSessionTable_WarehouseScheduledExamID_NCI]
    ON [dbo].[WAREHOUSE_ExamSessionTable]([WAREHOUSEScheduledExamID] ASC)
    INCLUDE([completionDate]);

CREATE NONCLUSTERED INDEX [idx_NC_WAREHOUSE_ExamSessionTable_Shreded_CI]
    ON [dbo].[WAREHOUSE_ExamSessionTable_Shreded]([originatorId] ASC, [qualificationLevel] ASC, [submittedDate] ASC, [reMarkStatus] ASC, [WarehouseExamState] ASC, [originalGrade] ASC)
    INCLUDE([actualDuration], [closeValue], [examName], [examSessionId], [ExportToSecureMarker], [passValue], [previousExamState], [scheduledDurationValue]);

CREATE NONCLUSTERED INDEX [idx_WAREHOUSE_ExamSessionTable_Shreded_CandidateRef]
    ON [dbo].[WAREHOUSE_ExamSessionTable_Shreded]([candidateRef] ASC);

CREATE NONCLUSTERED INDEX [idx_WAREHOUSE_ExamSessionTable_Shreded_CentreName]
    ON [dbo].[WAREHOUSE_ExamSessionTable_Shreded]([centreName] ASC);

CREATE NONCLUSTERED INDEX [idx_WAREHOUSE_ExamSessionTable_Shreded_ExamName]
    ON [dbo].[WAREHOUSE_ExamSessionTable_Shreded]([examName] ASC);

CREATE NONCLUSTERED INDEX [idx_WAREHOUSE_ExamSessionTable_Shreded_ExamResult]
    ON [dbo].[WAREHOUSE_ExamSessionTable_Shreded]([examResult] ASC);

CREATE NONCLUSTERED INDEX [idx_WAREHOUSE_ExamSessionTable_Shreded_ForeName]
    ON [dbo].[WAREHOUSE_ExamSessionTable_Shreded]([foreName] ASC);

CREATE NONCLUSTERED INDEX [idx_WAREHOUSE_ExamSessionTable_Shreded_Grade]
    ON [dbo].[WAREHOUSE_ExamSessionTable_Shreded]([grade] ASC);

CREATE NONCLUSTERED INDEX [idx_WAREHOUSE_ExamSessionTable_Shreded_qualificationid_WarehouseExamStatesubmittedDate]
    ON [dbo].[WAREHOUSE_ExamSessionTable_Shreded]([qualificationid] ASC, [WarehouseExamState] ASC, [submittedDate] ASC)
    INCLUDE([closeValue], [examSessionId], [passValue], [previousExamState], [qualificationLevel]);

CREATE NONCLUSTERED INDEX [idx_WAREHOUSE_ExamSessionTable_Shreded_QualificationLevel_centreId_NCI]
    ON [dbo].[WAREHOUSE_ExamSessionTable_Shreded]([qualificationLevel] ASC, [centreId] ASC);

CREATE NONCLUSTERED INDEX [idx_WAREHOUSE_ExamSessionTable_Shreded_QualificationName]
    ON [dbo].[WAREHOUSE_ExamSessionTable_Shreded]([qualificationName] ASC);

CREATE NONCLUSTERED INDEX [idx_WAREHOUSE_ExamSessionTable_Shreded_ScheduledDurationValue]
    ON [dbo].[WAREHOUSE_ExamSessionTable_Shreded]([scheduledDurationValue] ASC);

CREATE NONCLUSTERED INDEX [idx_WAREHOUSE_ExamSessionTable_Shreded_SurName]
    ON [dbo].[WAREHOUSE_ExamSessionTable_Shreded]([surName] ASC);

CREATE NONCLUSTERED INDEX [idx_WAREHOUSE_ExamSessionTable_Shreded_WarehouseExamStatesubmittedDate_grade]
    ON [dbo].[WAREHOUSE_ExamSessionTable_Shreded]([WarehouseExamState] ASC, [submittedDate] ASC, [grade] ASC)
    INCLUDE([closeValue], [examSessionId], [passValue], [previousExamState], [qualificationid], [qualificationLevel]);

CREATE NONCLUSTERED INDEX [IX_WAREHOUSE_ExamSessionTable_Shreded_Keycode]
    ON [dbo].[WAREHOUSE_ExamSessionTable_Shreded]([KeyCode] ASC);

CREATE NONCLUSTERED INDEX [qualificationID]
    ON [dbo].[WAREHOUSE_ExamSessionTable_Shreded]([qualificationid] ASC);

CREATE NONCLUSTERED INDEX [submittedDate]
    ON [dbo].[WAREHOUSE_ExamSessionTable_Shreded]([submittedDate] DESC)
    INCLUDE([centreName], [closeValue], [examName], [examSessionId], [foreName], [grade], [passValue], [previousExamState], [qualificationName], [surName], [WarehouseExamState]);

CREATE NONCLUSTERED INDEX [warehousetime]
    ON [dbo].[WAREHOUSE_ExamSessionTable_Shreded]([warehouseTime] ASC);

CREATE UNIQUE NONCLUSTERED INDEX [IX_WAREHOUSE_ExamSessionTable_ShrededItems]
    ON [dbo].[WAREHOUSE_ExamSessionTable_ShrededItems]([examSessionId] ASC, [ItemRef] ASC);

CREATE NONCLUSTERED INDEX [idx_WAREHOUSE_ScheduledExamsTable_qualificationLevel_NCI]
    ON [dbo].[WAREHOUSE_ScheduledExamsTable]([qualificationLevel] ASC);

CREATE NONCLUSTERED INDEX [idx_WAREHOUSE_ScheduledExamsTable_ScheduledExamID_OriginatorID]
    ON [dbo].[WAREHOUSE_ScheduledExamsTable]([ScheduledExamID] ASC, [OriginatorID] ASC);

CREATE NONCLUSTERED INDEX [IX_WAREHOUSE_ScheduledExamsTable_ID_WAREHOUSECentreID_NCI]
    ON [dbo].[WAREHOUSE_ScheduledExamsTable]([ID] ASC, [WAREHOUSECentreID] ASC);

CREATE NONCLUSTERED INDEX [IX_WAREHOUSE_ScheduledExamsTable_WarehouseCentreID_NCI]
    ON [dbo].[WAREHOUSE_ScheduledExamsTable]([WAREHOUSECentreID] ASC);

