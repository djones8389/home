--exec sp_msforeachdb '

--use [?];

--if (db_ID() > 4)

--select ''?''
--	,t.name
--	, CAST(((SUM(total_pages) * 8192) / 1048576.0) AS FLOAT) [KB-Per-Table]
--from sys.partitions P
--inner join sys.tables T
--on t.object_id = p.object_id
--inner join sys.allocation_units  A
--on a.container_id in (p.hobt_id, p.partition_id)
--	group by t.name;

--'

USE [PPD_BCIntegration_SecureAssess]

select
	t.name
	, CAST(((SUM(total_pages) * 8192) / 1048576.0) AS FLOAT)*1024 [Reserved Allocations Per Table-KB]	
	
from sys.partitions P
inner join sys.tables T
on t.object_id = p.object_id
inner join sys.allocation_units  A
on a.container_id in (p.hobt_id, p.partition_id)
	where t.name = 'WAREHOUSE_ExamSessionDocumentTable'
	group by t.name;


SELECT
OBJECT_NAME(i.OBJECT_ID) AS TableName,
i.name AS IndexName,
i.index_id AS IndexID,
8 * SUM(a.used_pages) AS 'Indexsize(KB)'
FROM sys.indexes AS i
JOIN sys.partitions AS p ON p.OBJECT_ID = i.OBJECT_ID AND p.index_id = i.index_id
JOIN sys.allocation_units AS a ON a.container_id = p.partition_id
where OBJECT_NAME(i.OBJECT_ID) = 'WAREHOUSE_ExamSessionDocumentTable'
GROUP BY i.OBJECT_ID,i.index_id,i.name
ORDER BY OBJECT_NAME(i.OBJECT_ID),i.index_id


SELECT i.[name] AS IndexName
    ,SUM(s.[used_page_count]) * 8 AS IndexSizeKB
FROM sys.dm_db_partition_stats AS s
INNER JOIN sys.indexes AS i ON s.[object_id] = i.[object_id]
    AND s.[index_id] = i.[index_id]
	where OBJECT_NAME(i.OBJECT_ID) = 'WAREHOUSE_ExamSessionDocumentTable'
GROUP BY i.[name]
ORDER BY i.[name]
GO






SELECT [DatabaseName]
    ,[ObjectId]
    ,[ObjectName]
    ,[IndexId]
    ,[IndexDescription]
    ,CONVERT(DECIMAL(16, 1), (SUM([avg_record_size_in_bytes] * [record_count]) / (1024.0 * 1024))) AS [IndexSize(MB)]
    ,[lastupdated] AS [StatisticLastUpdated]
    ,[AvgFragmentationInPercent]
FROM (
    SELECT DISTINCT DB_Name(Database_id) AS 'DatabaseName'
        ,OBJECT_ID AS ObjectId
        ,Object_Name(Object_id) AS ObjectName
        ,Index_ID AS IndexId
        ,Index_Type_Desc AS IndexDescription
        ,avg_record_size_in_bytes
        ,record_count
        ,STATS_DATE(object_id, index_id) AS 'lastupdated'
        ,CONVERT([varchar](512), round(Avg_Fragmentation_In_Percent, 3)) AS 'AvgFragmentationInPercent'
    FROM sys.dm_db_index_physical_stats(db_id(), NULL, NULL, NULL, 'detailed')
    WHERE OBJECT_ID IS NOT NULL
        AND Avg_Fragmentation_In_Percent <> 0
    ) T
GROUP BY DatabaseName
    ,ObjectId
    ,ObjectName
    ,IndexId
    ,IndexDescription
    ,lastupdated
    ,AvgFragmentationInPercent


select
	t.name
	, rows
	, a.type
	, a.total_pages
	, a.used_pages
	, a.data_pages
from sys.partitions P
inner join sys.tables T
on t.object_id = p.object_id
inner join sys.allocation_units  A
on a.container_id in (p.hobt_id, p.partition_id)

where t.name = 'ExamSessionTable'



select
	t.name
	, rows
	, a.type
	, a.total_pages
	, a.used_pages
	, a.data_pages
from sys.partitions P
inner join sys.tables T
on t.object_id = p.object_id
inner join sys.allocation_units  A
on a.container_id in (p.hobt_id, p.partition_id)
where partition_id = '72057594038845440'


select reserved_page_count, *
from sys.dm_db_partition_stats
where partition_id = '72057594038845440'








