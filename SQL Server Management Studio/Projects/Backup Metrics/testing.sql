DECLARE @Backups TABLE(logDate datetime, ProcessInfo nvarchar(20), text nvarchar(2000))
INSERT @Backups
EXEC sp_readerrorlog 0, 1, 'Back'

DECLARE @T TABLE(logDate datetime,Metrics varchar(1000));

INSERT INTO @T 
SELECT logDate
	, SUBSTRING(text, 0, CHARINDEX(', last',text))

FROM @Backups
where processInfo = 'Backup'
	AND text not like '%Differential%'
	AND text not like '%Log%'

SELECT DISTINCT
	A.DBName
	, A.Pages
	, compressed_backup_size
	, DATEDIFF(SECOND, backup_start_date, backup_finish_date) as [Duration]
	, backup_start_date
	, backup_finish_date
	, CASE WHEN b.name = 'CommVault Galaxy Backup' THEN 'Rackspace Backup' ELSE 'Maintenance Backup' END AS [Backup Type]
	, is_compressed
FROM
(
SELECT  
 SUBSTRING(Metrics, CHARINDEX(':', Metrics)+2, CHARINDEX(',', Metrics)-CHARINDEX(':', Metrics)-2) [DBName]
, SUBSTRING(Metrics, CHARINDEX(',', Metrics)+34, 8) [Duration]
, SUBSTRING(Metrics, CHARINDEX('umped:', Metrics)+6,  CHARINDEX(', first', Metrics)-CHARINDEX('umped:', Metrics)-6) [Pages]
, REPLACE(SUBSTRING(Metrics, CHARINDEX('LSN', Metrics)+5, 30),':','') [First LSN]

FROM @T 
) A

INNER JOIN msdb.dbo.backupset b 
	ON b.database_name = A.DBName				
	AND REPLACE(a.[First LSN], '0', '') =  REPLACE(b.first_lsn, '0', '')
INNER JOIN msdb.dbo.backupmediafamily c
	ON c.media_set_id = b.media_set_id 
INNER JOIN msdb.dbo.backupmediaset d
	on d.media_set_id = b.media_set_id

Where database_name not in ('model','master','tempdb','msdb')

ORDER BY 1;




PRINT 'Starting backup compressed: ' + CONVERT(nvarchar(100),GETDATE(),113);
BACKUP DATABASE [PRV_BritishCouncil_ItemBank] TO DISK = N'T:\Backup\PRV_BritishCouncil_ItemBank_Compressed.bak' WITH NAME = N'PRV_BritishCouncil_ItemBank'
	, COPY_ONLY, NOFORMAT, NOINIT, SKIP, NOREWIND, NOUNLOAD, STATS = 10, COMPRESSION;
PRINT 'Finishing backup compressed: ' + CONVERT(nvarchar(100),GETDATE(),113);
GO


PRINT 'Starting backup uncompressed: ' + CONVERT(nvarchar(100),GETDATE(),113);
BACKUP DATABASE [PRV_BritishCouncil_ItemBank] TO DISK = N'T:\Backup\PRV_BritishCouncil_ItemBank_Uncompressed.bak' WITH NAME = N'PRV_BritishCouncil_ItemBank'
	, COPY_ONLY, NOFORMAT, NOINIT, SKIP, NOREWIND, NOUNLOAD, STATS = 10, NO_COMPRESSION;
PRINT 'Finishing backup uncompressed: ' + CONVERT(nvarchar(100),GETDATE(),113);
GO

/*
Starting backup uncompressed: 01 Feb 2016 13:07:18:967
10 percent processed.
20 percent processed.
30 percent processed.
40 percent processed.
50 percent processed.
60 percent processed.
70 percent processed.
80 percent processed.
90 percent processed.
Processed 2599640 pages for database 'PRV_BritishCouncil_ItemBank', file 'BRITISHCOUNCIL_ItemBank' on file 1.
100 percent processed.
Processed 7 pages for database 'PRV_BritishCouncil_ItemBank', file 'BRITISHCOUNCIL_ItemBank_log' on file 1.
BACKUP DATABASE successfully processed 2599647 pages in 165.146 seconds (122.980 MB/sec).
Backed upuncompressed: 01 Feb 2016 13:10:04:263
The backup set on file 1 is valid.
Done the restore verify only: 01 Feb 2016 13:10:42:773




PRINT 'Starting backup uncompressed: ' + CONVERT(nvarchar(100),GETDATE(),113);



PRINT 'Backed upuncompressed: ' + CONVERT(nvarchar(100),GETDATE(),113);

declare @backupSetId as int
select @backupSetId = position from msdb..backupset where database_name=N'PRV_BritishCouncil_ItemBank' and backup_set_id=(select max(backup_set_id) from msdb..backupset where database_name=N'PRV_BritishCouncil_ItemBank' )
if @backupSetId is null begin raiserror(N'Verify failed. Backup information for database ''PRV_BritishCouncil_ItemBank'' not found.', 16, 1) end
RESTORE VERIFYONLY FROM  DISK = N'T:\Backup\PRV_BritishCouncil_ItemBank.bak' WITH  FILE = @backupSetId,  NOUNLOAD,  NOREWIND;

PRINT 'Done the restore verify only: ' + CONVERT(nvarchar(100),GETDATE(),113);


*/
