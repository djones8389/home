DECLARE @MyBackup smallint = 50; --In Gigabytes
DECLARE @IsCompressed bit = 0;  


SELECT 
	 CONVERT(varchar, DATEADD(SECOND,X, 0), 114) [AverageBackUpTime]
FROM
(

SELECT 
	(Duration/[AvBackup-GB]) * @MyBackup [X]
FROM
(
	SELECT 	
		 AVG(CONVERT(DECIMAL(15,2),compressed_backup_size))/1000/1024/1024 'AvBackup-GB'
		, AVG(CONVERT(DECIMAL(15,2),Pages)) 'AvPages'
		, AVG(Duration) 'Duration'
		, CASE WHEN @IsCompressed = 1 THEN 'Compressed' WHEN @IsCompressed = 0 THEN 'Uncompressed' ELSE 'N/A' END AS [Compressed?]
	FROM (

		SELECT 
			ROW_NUMBER() OVER(PARTITION BY DBName ORDER BY backup_start_date desc) R 
			  ,[DBName]
			  ,[Pages]
			  ,compressed_backup_size
			  ,[Duration] 
			  ,[backup_start_date]
			  ,[backup_finish_date]
			  ,[Backup Type]
			  ,[is_compressed]
		  FROM [PSCollector].[dbo].[Backup Metrics]
			where [Backup Type] = 'Maintenance Backup'
				and is_compressed = @IsCompressed
	) A

	where R = 1
		and [Duration] > 0

) B


) C








/*

SELECT CONVERT(NVARCHAR(MAX),DATEDIFF(Minute, 
			GETDATE(),(SELECT DATEADD(SECOND, 90, GETDATE())) ), 114)

SELECT DATEDIFF(SECOND, GETDATE(), DATEADD(SECOND, 933, GETDATE()))
SELECT DATEADD(SECOND, 60, GETDATE()) 
SELECT CONVERT(NVARCHAR(50),GETDATE(), 114)
SELECT CONVERT(NVARCHAR(50),GETDATE(), 114)
SELECT CONVERT(varchar, DATEADD(SECOND, 933, 0), 114)



	and DBName in ('EAL_SecureAssess','SkillsFirst_SecureAssess')

	R
	, DBName
	, Pages
	, x
	, y
	, x/y
	, compressed_backup_size
	, [Duration in seconds]
    , [backup_start_date]
    , [backup_finish_date]
    , [Backup Type]
    , [is_compressed]
	cast(compressed_backup_size as bigint)/1024/1024 / [Duration in seconds] 'Time it takes to backup, in seconds, per megabyte'
	,cast(compressed_backup_size as bigint)/1000/1024 'Megaybytes'
	,Pages/[Duration in seconds] 'How many seconds per page to backup'
	,Pages
	,[Duration in seconds]
*/