DECLARE @SizeOfDB smallint;  --GB
DECLARE @IsCompressed bit = 1;   --0 or 1




SELECT 
	
	CASE WHEN @IsCompressed = 1 THEN AVG(x) END AS 'TEST X'
	, CASE WHEN @IsCompressed = 1 THEN AVG(y) END AS 'TEST Y'
	, CASE WHEN @IsCompressed = 1 THEN AVG(x) END AS 'TEST X'
	, CASE WHEN @IsCompressed = 1 THEN AVG(y) END AS 'TEST Y'

	--R
	--, DBName
	--, Pages
	--, x
	--, y
	--, x/y
	--, compressed_backup_size
	--, [Duration in seconds]
 --   , [backup_start_date]
 --   , [backup_finish_date]
 --   , [Backup Type]
 --   , [is_compressed]
	
FROM (

SELECT 
	ROW_NUMBER() OVER(PARTITION BY DBName ORDER BY backup_start_date desc) R 
      ,[DBName]
	  , [x] =  CONVERT(DECIMAL(15,2),compressed_backup_size) / CONVERT(DECIMAL(15,2),Pages)	  
	  , [y] = (CONVERT(DECIMAL(15,2),compressed_backup_size) / CONVERT(DECIMAL(15,2),Pages))/[Duration]
      ,[Pages]
	  ,compressed_backup_size
      ,[Duration] as 'Duration in seconds'
      ,[backup_start_date]
      ,[backup_finish_date]
      ,[Backup Type]
      ,[is_compressed]
  FROM [PSCollector].[dbo].[Backup Metrics]
	where [Backup Type] = 'Maintenance Backup'
) A

where R = 1
	and [Duration in seconds] > 0
	--and DBName in ('EAL_SecureAssess','SkillsFirst_SecureAssess')













	--cast(compressed_backup_size as bigint)/1024/1024 / [Duration in seconds] 'Time it takes to backup, in seconds, per megabyte'
	--,cast(compressed_backup_size as bigint)/1000/1024 'Megaybytes'
	--,Pages/[Duration in seconds] 'How many seconds per page to backup'
	--,Pages
	--,[Duration in seconds]
