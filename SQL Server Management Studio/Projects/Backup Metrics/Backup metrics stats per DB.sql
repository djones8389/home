select [db] 
	, avg([avgPages]) [avgPages]
	, avg([avgcompressed_backup_size]) [avgcompressed_backup_size]
	, avg([avgbytesperpage]) [avgbytesperpage]
FROM (
select  SUBSTRING(dbname, charindex('_',dbname)+1, len(dbname)) [db]
	,avg([Pages]) [avgPages]
	,avg([compressed_backup_size])  [avgcompressed_backup_size]
	,avg([bytesperpage]) [avgbytesperpage]
FROM (

SELECT  [DBName]
      ,(cast ([Pages] as int)) [Pages]
	  ,cast([compressed_backup_size] as bigint)  [compressed_backup_size]
	  ,cast([compressed_backup_size] as bigint) / cast(pages as bigint) [bytesperpage]
	  ,ROW_NUMBER() OVER(Partition by [DBName] order by pages desc) R
  FROM [PSCollector].[dbo].[Backup Metrics]
  where [is_compressed] = 1
	and pages > 1000
) A
where R = 1
	group by dbname
) B
	group by [db]



Select *
FROM [PSCollector].[dbo].[Backup Metrics]

24195062
24128712

66350
8090
6457320
6449230

