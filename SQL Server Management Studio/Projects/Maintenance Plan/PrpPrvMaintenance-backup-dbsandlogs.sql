USE MASTER;

USE master
GO

SET NOCOUNT ON


DECLARE @Kb float
DECLARE @PageSize float
DECLARE @SQL varchar(2000)
--DECLARE @Dynamic nvarchar(MAX)='';

SELECT @Kb = 1024.0
SELECT @PageSize=v.low/@Kb FROM master..spt_values v WHERE v.number=1 AND v.type='E'

IF OBJECT_ID('tempdb.dbo.#FileSize') IS NOT NULL DROP TABLE #FileSize
IF OBJECT_ID('tempdb.dbo.#FileStats') IS NOT NULL  DROP TABLE #FileStats
IF OBJECT_ID('tempdb.dbo.#LogSpace') IS NOT NULL DROP TABLE #LogSpace

CREATE TABLE #FileSize (
      DatabaseName sysname,
      FileName sysname,
      FileSize int,
      FileGroupName sysname,
      LogicalName sysname
)

CREATE TABLE #FileStats (
      FileID int,
      FileGroup int,
      TotalExtents int,
      UsedExtents int,
      LogicalName sysname,
      FileName nchar(520)
)

CREATE TABLE #LogSpace (
      DatabaseName sysname,
      LogSize float,
      SpaceUsedPercent float,
      Status bit
)

INSERT #LogSpace EXEC ('DBCC sqlperf(logspace)')

DECLARE @DatabaseName sysname

DECLARE cur_Databases CURSOR FAST_FORWARD FOR
      SELECT DatabaseName = [name] FROM sys.databases where state_desc = 'ONLINE' ORDER BY DatabaseName
OPEN cur_Databases
FETCH NEXT FROM cur_Databases INTO @DatabaseName
WHILE @@FETCH_STATUS = 0
  BEGIN
      SET @SQL = '
                        USE [' + @DatabaseName + '];
                        DBCC showfilestats;
                        INSERT #FileSize (DatabaseName, FileName, FileSize, FileGroupName, LogicalName)
                        SELECT ''' +@DatabaseName + ''', filename, size, ISNULL(FILEGROUP_NAME(groupid),''LOG''), [name]
                        FROM dbo.sysfiles sf;
                        '

      INSERT #FileStats EXECUTE (@SQL)
      FETCH NEXT FROM cur_Databases INTO @DatabaseName
  END

CLOSE cur_Databases
DEALLOCATE cur_Databases
SET NOCOUNT ON



   /*   DatabaseName = fsi.DatabaseName,
      --FileGroupName = fsi.FileGroupName,
      LogicalName = RTRIM(fsi.LogicalName),
      FileName = RTRIM(fsi.FileName),
      FileSize = CAST(fsi.FileSize*@PageSize/@Kb as decimal(15,2)),
      UsedSpace = CAST(ISNULL((fs.UsedExtents*@PageSize*8.0/@Kb), fsi.FileSize*@PageSize/@Kb * ls.SpaceUsedPercent/100.0) as decimal(15,2)),
       CASE WHEN RTRIM(fsi.FileName) like '%.mdf%' THEN	
		  CONVERT(Nvarchar(MAX), CONVERT(DECIMAL(7,0), ROUND(CAST(ISNULL((fs.UsedExtents*@PageSize*8.0/@Kb), fsi.FileSize*@PageSize/@Kb * ls.SpaceUsedPercent/100.0) as decimal(15)) *1.05, 0)))
			ELSE  CONVERT(Nvarchar(MAX), CONVERT(DECIMAL(7,0), ROUND(CAST(ISNULL((fs.UsedExtents*@PageSize*8.0/@Kb), fsi.FileSize*@PageSize/@Kb * ls.SpaceUsedPercent/100.0) as decimal(15)) +100, 0)))		
					END AS 	'NewUsedSpace-5%',
      FreeSpace = CAST(ISNULL(((fsi.FileSize - UsedExtents*8.0)*@PageSize/@Kb), (100.0-ls.SpaceUsedPercent)/100.0 * fsi.FileSize*@PageSize/@Kb) as decimal(15,2)),
      [FreeSpace %] = CAST(ISNULL(((fsi.FileSize - UsedExtents*8.0) / fsi.FileSize * 100.0), 100-ls.SpaceUsedPercent) as decimal(15,2)),
	  CAST(fsi.FileSize*@PageSize/@Kb as decimal(15,2)) - CONVERT(Nvarchar(MAX), CONVERT(DECIMAL(7,0), ROUND(CAST(ISNULL((fs.UsedExtents*@PageSize*8.0/@Kb), fsi.FileSize*@PageSize/@Kb * ls.SpaceUsedPercent/100.0) as decimal(15)) +100, 0))) --LDF Space we're freeing up
	*/	
	
--SELECT @Dynamic +=CHAR(13) +

DECLARE @Dynamic TABLE (

	DBCCSTATEMENT NVARCHAR(MAX)
)

INSERT @Dynamic
SELECT      
		CASE WHEN RTRIM(fsi.FileName) like '%.ldf%' 
		THEN
		'USE ' + fsi.DatabaseName + ';  DBCC SHRINKFILE (''' + RTRIM(fsi.LogicalName) + + '''' + ',' + CONVERT(Nvarchar(MAX), CONVERT(DECIMAL(7,0), ROUND(CAST(ISNULL((fs.UsedExtents*@PageSize*8.0/@Kb), fsi.FileSize*@PageSize/@Kb * ls.SpaceUsedPercent/100.0) as decimal(15))+ 100, 0))) + ');'
		ELSE 
		'USE ' + fsi.DatabaseName + ';  DBCC SHRINKFILE (''' + RTRIM(fsi.LogicalName) + + '''' + ',' +  CONVERT(Nvarchar(MAX), CONVERT(DECIMAL(7,0), ROUND(CAST(ISNULL((fs.UsedExtents*@PageSize*8.0/@Kb), fsi.FileSize*@PageSize/@Kb * ls.SpaceUsedPercent/100.0) as decimal(15))*1.05, 0))) + ');'
		END AS 	DBCCStatement
	
FROM #FileSize fsi
LEFT JOIN #FileStats fs
      ON fs.FileName = fsi.FileName
LEFT JOIN #LogSpace ls
      ON ls.DatabaseName = fsi.DatabaseName

where 
	(
	RTRIM(fsi.FileName) like '%.mdf%'
	AND CONVERT(Nvarchar(MAX), CONVERT(DECIMAL(7,0), ROUND(CAST((fs.UsedExtents*@PageSize*8.0/@Kb) as decimal(15))*1.05, 0))) <  CAST(fsi.FileSize*@PageSize/@Kb as decimal(15,2))
	and fsi.DatabaseName not in ('tempdb','model','msdb', 'master')
	AND CAST((fsi.FileSize - UsedExtents*8.0)*@PageSize/@Kb as decimal(15,2)) > 500  --Only do >500MB to make it worthwhile
	)
	      
    OR 
	(
	RTRIM(fsi.FileName) like '%.ldf%'
	AND CAST(ISNULL((fs.UsedExtents*@PageSize*8.0/@Kb), fsi.FileSize*@PageSize/@Kb * ls.SpaceUsedPercent/100.0) as decimal(15,2)) <  CAST(fsi.FileSize*@PageSize/@Kb as decimal(15,2))
	and fsi.DatabaseName not in ('tempdb','model','msdb', 'master')
	AND CAST(fsi.FileSize*@PageSize/@Kb as decimal(15,2)) > 100 --Where it's using more than 100MB of space on disk
	--AND DIFFERENCE(CAST(fsi.FileSize*@PageSize/@Kb as decimal(15,2)), CONVERT(Nvarchar(MAX), CONVERT(DECIMAL(7,0), ROUND(CAST(ISNULL((fs.UsedExtents*@PageSize*8.0/@Kb), fsi.FileSize*@PageSize/@Kb * ls.SpaceUsedPercent/100.0) as decimal(15)) +100, 0)))) > 10		
	AND (CAST(fsi.FileSize*@PageSize/@Kb as decimal(15,2)) - CONVERT(Nvarchar(MAX), CONVERT(DECIMAL(7,0), ROUND(CAST(ISNULL((fs.UsedExtents*@PageSize*8.0/@Kb), fsi.FileSize*@PageSize/@Kb * ls.SpaceUsedPercent/100.0) as decimal(15)) +100, 0)))) > 250
	)


DECLARE Shrink Cursor FOR
SELECT DBCCSTATEMENT
FROM @Dynamic

DECLARE @SQLToRun NVARCHAR(MAX);

OPEN Shrink

FETCH NEXT FROM Shrink INTO @SQLToRun

WHILE @@FETCH_STATUS = 0
BEGIN

EXEC(@SQLToRun);

FETCH NEXT FROM Shrink INTO @SQLToRun

END
CLOSE Shrink;
DEALLOCATE Shrink;