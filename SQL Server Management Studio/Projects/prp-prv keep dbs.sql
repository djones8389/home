select name
from sys.databases
where name in ('PRV_DEMO_CPProjectAdmin','PRV_DEMO_ItemBank','PRV_DEMO_OpenAssessAdmin','PRV_DEMO_SecureAssess','PRV_DEMO_SecureMarker','PRV_Evolve_CPProjectAdmin','PRV_Evolve_ItemBank','PRV_Evolve_OpenAssessAdmin','PRV_Evolve_SecureAssess','PRV_NCFE_CPProjectAdmin','PRV_NCFE_ItemBank','PRV_NCFE_SecureAssess','PRV_OCR_ContentProducer','PRV_OCR_ItemBank','PRV_OCR_SecureAssess','PRV_SkillsFirst_ContentProducer','PRV_SkillsFirst_ItemBank','PRV_SkillsFirst_SecureAssess','PRV_SQA_CPProjectAdmin','PRV_SQA_ItemBank','PRV_SQA_SecureAssess','PRV_WJEC_ContentProducer','PRV_WJEC_ItemBank','PRV_WJEC_OpenAssess','PRV_WJEC_SecureAssess','PPD_AAT_ContentProducer','PPD_AAT_ItemBank','PPD_AAT_SecureAssess','PPD_AAT_SecureMarker','PPD_AQA_ContentProducer','PPD_AQA_ItemBank','PPD_AQA_SecureAssess','PPD_NCFE_CPProjectAdmin','PPD_NCFE_Itembank','PPD_NCFE_SecureAssess','PPD_OCR_ContentProducer','PPD_OCR_ItemBank','PPD_OCR_SecureAssess','PPD_OcrDataManagement','STG_AQA_ContentProducer','STG_AQA_ItemBank','STG_AQA_SecureAssess','STG_EAL_ContentProducer','STG_EAL_ItemBank','STG_EAL_SecureAssess','STG_NCFE_CPProjectAdmin','STG_NCFE_Itembank','STG_NCFE_SecureAssess','STG_SQA_CPProjectAdmin','STG_SQA_ItemBank','STG_SQA_SecureAssess','STG_SQA2_OpenAssess','STG_WJEC_ContentProducer','STG_WJEC_ItemBank','STG_WJEC_OpenAssess','STG_WJEC_SecureAssess')
order by 1;

use master

SELECT
    db.name AS DBName,
    type_desc AS FileType,
    Physical_Name AS Location,mf.size/128 as Size_in_MB
FROM
    sys.master_files mf
INNER JOIN 
    sys.databases db ON db.database_id = mf.database_id
where db.name in ('PRV_DEMO_CPProjectAdmin','PRV_DEMO_ItemBank','PRV_DEMO_OpenAssessAdmin','PRV_DEMO_SecureAssess','PRV_DEMO_SecureMarker','PRV_Evolve_CPProjectAdmin','PRV_Evolve_ItemBank','PRV_Evolve_OpenAssessAdmin','PRV_Evolve_SecureAssess','PRV_NCFE_CPProjectAdmin','PRV_NCFE_ItemBank','PRV_NCFE_SecureAssess','PRV_OCR_ContentProducer','PRV_OCR_ItemBank','PRV_OCR_SecureAssess','PRV_SkillsFirst_ContentProducer','PRV_SkillsFirst_ItemBank','PRV_SkillsFirst_SecureAssess','PRV_SQA_CPProjectAdmin','PRV_SQA_ItemBank','PRV_SQA_SecureAssess','PRV_WJEC_ContentProducer','PRV_WJEC_ItemBank','PRV_WJEC_OpenAssess','PRV_WJEC_SecureAssess','PPD_AAT_ContentProducer','PPD_AAT_ItemBank','PPD_AAT_SecureAssess','PPD_AAT_SecureMarker','PPD_AQA_ContentProducer','PPD_AQA_ItemBank','PPD_AQA_SecureAssess','PPD_NCFE_CPProjectAdmin','PPD_NCFE_Itembank','PPD_NCFE_SecureAssess','PPD_OCR_ContentProducer','PPD_OCR_ItemBank','PPD_OCR_SecureAssess','PPD_OcrDataManagement','STG_AQA_ContentProducer','STG_AQA_ItemBank','STG_AQA_SecureAssess','STG_EAL_ContentProducer','STG_EAL_ItemBank','STG_EAL_SecureAssess','STG_NCFE_CPProjectAdmin','STG_NCFE_Itembank','STG_NCFE_SecureAssess','STG_SQA_CPProjectAdmin','STG_SQA_ItemBank','STG_SQA_SecureAssess','STG_SQA2_OpenAssess','STG_WJEC_ContentProducer','STG_WJEC_ItemBank','STG_WJEC_OpenAssess','STG_WJEC_SecureAssess')
and type_desc = 'ROWS'
	ORDER BY 1 desc
