IF OBJECT_ID ('tempdb..#Stats') IS NOT NULL DROP TABLE #Stats

CREATE TABLE #Stats (

	fileid tinyint
	, [filegroup] tinyint
	, totalExtents int
	, usedExtents int
	, Name nvarchar(100)
	,[FileName] nvarchar(200)

)

INSERT #Stats
exec sp_MSforeachdb '

use [?];


if (''?'' like ''SHA_%'')

DBCC SHOWFILESTATS


'


SELECT SUM(totalExtents)*64		[totalExtents-KB]
,	 SUM(usedExtents)*64		 [usedExtents-KB]
,	 (SUM(totalExtents)*64)/1024 [totalExtents-MB]
,	 (SUM(usedExtents)*64)/1024	  [usedExtents-MB]
FROM #Stats