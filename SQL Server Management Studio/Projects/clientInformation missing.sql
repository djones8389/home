use master

create table #resultHolder (

	dbName sysname
	, [counter] int
)

INSERT #resultHolder
exec sp_MSforeachdb '

	use [?]

	if(''?'' like ''%SecureAssess%'')

	SET QUOTED_IDENTIFIER ON;

	BEGIN

	select ''?'' [DB]
		, count(ID) [Count]
	from WAREHOUSE_ExamSessionTable (NOLOCK)
	where clientinformation is not null
		and clientinformation.value(''data(clientInformation/systemConfiguration/secureClient/Application)[1]'',''nvarchar(100)'') IS NOT NULL
		and clientinformation.value(''data(clientInformation/systemConfiguration/secureClient/Application)[1]'',''nvarchar(100)'') != ''not available''

	END


'

select *
from #resultHolder order by 1



--select 
--	count(ID)
--from WAREHOUSE_ExamSessionTable (NOLOCK)
--where clientinformation is not null
--	and clientinformation.value('data(clientInformation/systemConfiguration/secureClient/Application)[1]','nvarchar(100)') IS NOT NULL
--	and clientinformation.value('data(clientInformation/systemConfiguration/secureClient/Application)[1]','nvarchar(100)') != 'not available'
