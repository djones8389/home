use master

IF OBJECT_ID('tempdb..#ResultHolder') IS NOT NULL DROP TABLE #ResultHolder;

--DECLARE @LogSize TABLE (
--	DBName sysname
--	, logSizeMB real
--	, [LogSpace%] real
--	,[Status] bit
--)

--INSERT @LogSize
--exec ('dbcc sqlperf(logspace)')

CREATE TABLE #ResultHolder
(
	DBName sysname NULL
	, [Data File Size - KB] float NULL
	--, [Log File Size - KB] float NULL
	, [Used_pages - KB] float NULL
	, [Total_pages - KB] float NULL
	, [total_pages] float NULL
	--, [LogSpace%] real NULL
)

INSERT #ResultHolder (DBName, [Data File Size - KB], [Used_pages - KB], [Total_pages - KB], [total_pages])
exec sp_msforeachdb '

use [?];

if(db_ID() > 4)

	BEGIN

		select Data.DBName
			, Data.[Data File Size - KB]
			, [Used_pages - KB] 
			, [Total_pages - KB]
			, [total_pages]
		FROM (
		select db_name() [DBName]
			, SUM(cast ((size * 8) as float)) [Data File Size - KB]
		from sys.master_files
		where database_id = db_ID()
			and type_desc = ''ROWS''
		) Data
		INNER JOIN (

		select db_name() [DBName]
			, cast ((size * 8) as float) [Log File Size - KB]
		from sys.master_files
		where database_id = db_ID()
			and type_desc = ''Log''

		) [Log]
		on [Data].DBName = [Log].DBName

		INNER JOIN (

		SELECT db_name() [DBName]
			,CAST(((SUM(used_pages) * 8192) / 1048576.0) AS FLOAT) [Used_pages - KB] 
			,CAST(((SUM(total_pages) * 8192) / 1048576.0) AS FLOAT) [Total_pages - KB] 
			,sum(total_pages) [total_pages]
		FROM sys.allocation_units

	) [DataPages]
		ON [DataPages].DBName = [Data].DBName


	END

'

select b.*
	, total_pages/[Diff-MB]
FROM (
SELECT A.DBName
	, a.[Data File Size - KB]
	, a.[Total_pages - KB]*1024 [Total_pages - KB]
	, a.[Used_pages - KB]*1024 [Used_pages - KB]
	,([Data File Size - KB] - ([Total_pages - KB]*1024))/1024 [Diff-MB]
	,total_pages
FROM #ResultHolder A
--where dbName like '%[_]SecureAssess'
where dbname in ('PRV_OCR_SecureAssess','PPD_OCR_SecureAssess')
) b
order by total_pages--([Data File Size - KB] - ([Total_pages - KB]*1024))/1024 desc

--use PPD_AAT_SecureAssess
--dbcc showfilestats

