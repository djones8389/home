SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

select  distinct
		count(ExternalSessionID) [NoOfExams]
		, q.Name
		, cev2.State
from CandidateExamVersions CEV
INNER JOIN  CandidateExamVersionStateLookup CEV2
on cev2.ID = ExamSessionState
LEFT JOIN CandidateResponses as CR ON CR.CandidateExamVersionID = CEV.ID
LEFT JOIN UniqueResponses as UR ON UR.ID = CR.UniqueResponseID 
LEFT JOIN Items as I on I.ID = UR.itemId
LEFT JOIN ExamVersions EV on EV.ID = CEV.ExamVersionID
LEFT JOIN Exams E on E.ID = EV.ExamID
LEFT JOIN Qualifications Q on Q.ID = E.QualificationID

where ExamSessionState in (1,2)
	AND Q.NAME IN ('Practice Qualification','Practice Qualification (AQ2016)')
	and IsFlagged = 0
group by  q.Name
	, cev2.State
ORDER BY 1;

