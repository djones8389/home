SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

use ICAEW_SecureAssess

--CREATE DATABASE [ICAEW_TomGData];

SELECT DISTINCT ItemID
INTO #ItemIDs
FROM WAREHOUSE_ExamSessionItemResponseTable r (READUNCOMMITTED)
INNER JOIN WAREHOUSE_ExamSessionTable e (READUNCOMMITTED)
ON e.ID = r.WAREHOUSEExamSessionID
ORDER by ItemID

CREATE CLUSTERED INDEX [ItemIDs] on #ItemIDs(itemID)

/* Create tables
SELECT '
	CREATE TABLE [ICAEW_TomGData].[dbo].[TomGStats_'+ItemID+'] (
	
	XMLCombo XML
);
'
FROM #ItemIDs
*/




SELECT ';with '+ItemID+' as (
select e.KeyCode, r.ID, ItemID, ItemResponseData
from WAREHOUSE_ExamSessionItemResponseTable r (READUNCOMMITTED)
inner join WAREHOUSE_ExamSessionTable e (READUNCOMMITTED)
on e.ID = r.WAREHOUSEExamSessionID
--where itemID = '''+itemid+'''
for xml path(''responses'')
)

INSERT [ICAEW_TomGData].[dbo].[TomGStats_'+ItemID+']
select *
from '+ItemID+'

'

FROM #ItemIDs

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;


select e.KeyCode, r.ID, ItemID, ItemResponseData
from WAREHOUSE_ExamSessionItemResponseTable r
inner join WAREHOUSE_ExamSessionTable e
on e.ID = r.WAREHOUSEExamSessionID
where itemID = '368P1000'
for xml path('responses')

