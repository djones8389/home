use master
RESTORE DATABASE [Performance5_SecureAssess] FROM DISK = N'D:\Backup\Performance3_SecureAssess.2018.03.08.bak' 
WITH FILE = 1, NOUNLOAD, NOREWIND, REPLACE, STATS = 10
, MOVE 'Data' TO N'E:\Data\Performance5_SecureAssess.mdf'
, MOVE 'Log' TO N'E:\Log\Performance5_SecureAssess.ldf'; 


declare @table table (
	DBNAME sysname
	, logsizemb decimal (8,2)
	, logspaceusedmb decimal (8,2)
	, status bit
)

insert @table
exec('dbcc sqlperf(logspace)')

select *
from @table
where DBNAME = 'Performance5_SecureAssess'


--75.55469	96.13342


use [Performance5_SecureAssess]

dbcc shrinkfile('Log',10);