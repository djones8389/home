select name, collation_name
from sys.databases
where database_id > 4
	and name like '%DataWarehouse%'
order by 1

use master

--exec sp_renamedb 'Nbmereplica_SurpassDataWarehouse_new','Nbmereplica_SurpassDataWarehouse'
--exec sp_renamedb 'TemplateUSEast_SurpassDataWarehouse_new','TemplateUSEast_SurpassDataWarehouse'


--drop database [Performance3_SurpassDataWarehouse_OLD];
--drop database [TemplateUSEast_SurpassDataWarehouse_oldCollation];
--drop database [Nbmereplica_SurpassDataWarehouse];

--exec sp_renamedb 'Nbmereplica_SurpassDataWarehouse1','Nbmereplica_SurpassDataWarehouse'
