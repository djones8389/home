DECLARE @SourceDB sysname = 'Nbmereplica_SurpassDataWarehouse';
DECLARE @TargetDB sysname = 'Nbmereplica_SurpassDataWarehouse1';


use Nbmereplica_SurpassDataWarehouse

if OBJECT_ID ('tempdb..#table') is not null drop table #table;

--Store tables with data.  Exclude staging tables--

select t.name [TableName]
	, schema_id
	, c.name [Col]
	, c.object_id
	, c.is_identity
into #table
from sys.tables  t
inner join sys.columns c
on c.object_id = t.object_id
where t.name in (
	select  
		 OBJECT_NAME(object_Id)
	from sys.dm_db_partition_stats
	where OBJECT_NAME(object_Id) not like 'sys%'
		and OBJECT_NAME(object_Id) not like 'stg_%'
		and row_count > 0
)

SELECT TableName
	, [Insert Statement]
FROM (
	Select a.*
		, ROW_NUMBER() OVER (PARTITION BY [TableName] ORDER BY [Insert Statement] desc) R
	from (
		select distinct  a.[TableName]
			, case when is_identity = 1 then 'SET IDENTITY_INSERT ' +quotename(@TargetDB)+'.'+quotename(schema_name(a.schema_id)) + '.' + quotename(a.[TableName]) + ' ON;' + ' INSERT '+quotename(@TargetDB)+'.'+quotename(schema_name(a.schema_id)) + quotename([TableName]) + ' ('+b.ColumnList + ')'
				+ ' SELECT' + ' '+b.ColumnList + '  FROM '+quotename(@SourceDB)+'.'+quotename(schema_name(a.schema_id)) + '.' + quotename(a.[TableName])
				else ' INSERT '+quotename(@TargetDB)+'.'+quotename(schema_name(a.schema_id)) + '.' +  quotename(a.[TableName]) + ' ('+b.ColumnList + ')'
					+ ' SELECT' + ' '+b.ColumnList + '  FROM '+quotename(@SourceDB)+'.'+quotename(schema_name(a.schema_id)) + '.' + quotename(a.[TableName])
				end as [Insert Statement]
		from #table a
		inner join (
			SELECT 
			  object_id,
			  STUFF((
				SELECT ', ' + quotename([Col])
				FROM #table
				WHERE (object_id = Results.object_id) 
				FOR XML PATH(''),TYPE).value('(./text())[1]','VARCHAR(MAX)')
			  ,1,2,'') AS ColumnList
			FROM #table Results
			GROUP BY object_id
		) b
		on a.object_id = b.object_id
	) a
) b
where r = 1
group by TableName,[Insert Statement]
order by TableName
