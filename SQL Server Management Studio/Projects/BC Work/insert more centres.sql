SET IDENTITY_INSERT [Centres_ID] OFF 

INSERT [Centres_ID](CentreName, CentreCode, AddressLine1, AddressLine2)
SELECT distinct
	BC.[CentreName]
      ,BC.[CentreCode]
      ,BC.[AddressLine1]
      ,BC.[AddressLine2]
   --   ,BC.[County]
   --   ,BC.[Country]
   --   ,BC.[PostCode]
   --   ,BC.[town]
   --   ,BC.[Retired]
	  --,C.CandidateID [centreAdminUser]
  FROM [BC_TPM_Extract].[dbo].[BigCentres] BC
  LEFT join Candidates C
  on C.FirstName = BC.[Forename]
	and c.LastName = BC.Surname
	--where Centrename = 'AbazShehu'

EXCEPT

SELECt  [CentreName]
    ,[CentreCode]
    ,[AddressLine1]
    ,[AddressLine2]
    --,[County]
    --,[Country]
    --,[PostCode]
    --,[Town]
    --,[Retired]
    --,[centreAdminUser]
FROM [BC_TPM_Extract].[dbo].[Centres_ID]
--where Centrename = 'AbazShehu'