DELETE A
FROM (
SELECT *
	, ROW_NUMBER () OVER (PARTITION BY ID, forename, surname, middlename, dob, gender, addressline1, addressline2, cast(town as nvarchar(MAX)), cast(country as nvarchar(MAX)), postcode, telephone, cast(email as nvarchar(MAX)), AccountCreationDate,AccountExpiryDate ORDER BY ID) R
FROM Candidates --#resultHolder
) A
where R = 1
ORDER BY 1;


SELECT *
INTO [Candidates_With_Duplicates]
FROM Candidates


UPDATE Candidates
set AddressLine1 = REPLACE(AddressLine1, ',','')
	, AddressLine2 = REPLACE(AddressLine2, ',','')
	, CandidateRef = REPLACE(CandidateRef, ',','')
	, [Forename] = REPLACE([Forename], ',','')
	, MiddleName = REPLACE(MiddleName, ',','')
	, Surname = REPLACE(Surname, ',','')

UPDATE Candidates
set AddressLine1 = REPLACE(AddressLine1, '"','')
	, AddressLine2 = REPLACE(AddressLine2, '"','')
	, CandidateRef = REPLACE(CandidateRef, '"','')
	, [Forename] = REPLACE([Forename], '"','')
	, MiddleName = REPLACE(MiddleName, '"','')
	, Surname = REPLACE(Surname, '"','')


begin tran
	UPDATE Candidates
	Set Country = cast(D.ID as nvarchar(MAX))
	FROM Candidates C
	INNER JOIN CountryLookUpTable D
	On cast(D.Country as nvarchar(MAX)) = cast(C.Country as nvarchar(MAX))
	Where ISNUMERIC(cast(C.Country as nvarchar(max))) = 0

	UPDATE Candidates
	Set County =cast(D.ID as nvarchar(MAX))
	FROM Candidates C
	INNER JOIN CountyLookUpTable D
	On cast(D.County as nvarchar(MAX)) = cast(C.County as nvarchar(MAX))
	Where ISNUMERIC(cast(C.County as nvarchar(max))) = 0
rollback