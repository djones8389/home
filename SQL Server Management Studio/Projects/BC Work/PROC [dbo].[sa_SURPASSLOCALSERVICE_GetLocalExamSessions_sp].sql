USE [Bpec_SecureAssess]
GO
/****** Object:  StoredProcedure [dbo].[sa_SURPASSLOCALSERVICE_GetLocalExamSessions_sp]    Script Date: 15/05/2017 11:32:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROC [dbo].[sa_SURPASSLOCALSERVICE_GetLocalExamSessions_sp]
	@CommsKey CHAR(13),
	@PageSize INT
AS
BEGIN
	SET NOCOUNT ON

	SELECT TOP (@PageSize)
		EST.Id AS Id, 
		EST.ScheduledExamId AS ScheduledExamId,
		EST.UserId AS UserId,
		EST.StructureXml AS StructureXml,
		EST.KeyCode AS KeyCode,
		EST.PinNumber AS PinNumber,
		EST.PreviousExamState AS PreviousExamState,
		EST.ToolsXml AS ToolsXml,
		EST.ExamStateInformation AS ExamStateInformation,
		EST.ScheduledDurationXml AS ScheduledDurationXml,
		EST.ScheduledDuration AS ScheduledDuration,
		EST.SrDefault AS SrDefault,
		EST.SrMaximum AS SrMaximum,
		EST.DefaultDuration AS DefaultDuration,
		EST.AdvanceDownload AS AdvanceDownload,
		EST.DownloadInformation AS DownloadInformation,
		EST.ClientInformation AS ClientInformation,
		EST.AllowPackageDelivery AS AllowPackageDelivery,
		EST.ContainsBtlOffice AS ContainsBtlOffice,
		EST.AttemptAutoSubmitForAwaitingUpload AS AttemptAutoSubmitForAwaitingUpload,
		EST.AwaitingUploadGracePeriodInDays AS AwaitingUploadGracePeriodInDays,
		EST.AutomaticCreatePin AS AutomaticCreatePin,
		EST.MarkingAutoVoidPeriod AS MarkingAutoVoidPeriod,
		EST.AutoVoidDate AS AutoVoidDate,
		EST.DeliveryMechanism AS DeliveryMechanism,
		EST.CertifiedForTablet AS CertifiedForTablet,
		EST.IsProjectBased AS IsProjectBased,
		EST.ExpiryDate AS ExpiryDate,
		EST.UserAssociationEnabled AS UserAssociationEnabled,
		EST.UserAssociationMarkerAssignable AS UserAssociationMarkerAssignable,
		EST.UserAssociationMarkerRequired AS UserAssociationMarkerRequired,
		EST.UserAssociationModeratorAssignable AS UserAssociationModeratorAssignable,
		EST.UserAssociationModeratorRequired AS UserAssociationModeratorRequired,
		EST.UserAssociationRestrictUsers AS UserAssociationRestrictUsers,
		EST.PreCachingDefault AS PreCachingDefault,
		EST.PreCachingAvailable AS PreCachingAvailable,
		EST.ValidationCount AS ValidationCount,
		EST.ItemBankStyleProfileID AS ItemBankStyleProfileID,
		EST.ItemBankStyleProfileVersion AS ItemBankStyleProfileVersion,
		EST.SecureAssessStyleProfileID AS SecureAssessStyleProfileID,
		EST.EnableLogging AS EnableLogging,
		EST.ScaleScoreID AS ScaleScoreId,
		EST.DurationMode AS DurationMode,
		EST.EnableCandidateBreak AS EnableCandidateBreak,
		EST.CandidateBreakStyle AS CandidateBreakStyle,
		EST.ScheduledBreakType AS ScheduledBreakType,
		EST.BreakPooledTime AS BreakPooledTime,
		EST.MaximumNumberOfBreaksPerSection AS MaximumNumberOfBreaksPerSection,
		EST.TotalBreakTime AS TotalBreakTime,
		EST.TotalBreakTimeUsed AS TotalBreakTimeUsed,
		EST.IsUnlimitedBreaks AS IsUnlimitedBreaks,
		EST.CandidateBreakExtraDurationType AS CandidateBreakExtraDurationType,
		EST.CandidateBreakExtraDuration AS CandidateBreakExtraDuration,
		EST.NumberOfExtraBreaksPerSection AS NumberOfExtraBreaksPerSection,		
		EST.CandidateBreakExtraBreakReasonType AS CandidateBreakExtraBreakReasonType,
		EST.CandidateBreakExtraDurationReasonText AS CandidateBreakExtraDurationReasonText,
		EST.CandidateBreakExtraDurationReasonOther AS CandidateBreakExtraDurationReasonOther,
		EST.DeliveryPlatform AS DeliveryPlatform,
		EST.EnableLOBoundaries AS EnableLOBoundaries,
		EST.LearningOutcomesThreshold AS LearningOutcomesThreshold
		
	FROM dbo.ExamSessionTable AS EST
	INNER JOIN dbo.ScheduledExamsTable AS SCET ON SCET.ID = EST.ScheduledExamID
	INNER JOIN dbo.CentreTable AS CT ON CT.ID = SCET.CentreID
	WHERE 
		CT.CommsKey = @CommsKey AND 
		CT.OfflineEnabled = 1 AND
		CT.Retired = 0 AND
		EST.ExamState = 3 AND -- ready for download state
		EST.IsOffline = 1
	ORDER BY SCET.ScheduledStartDateTime, SCET.ActiveStartTime ASC
END

