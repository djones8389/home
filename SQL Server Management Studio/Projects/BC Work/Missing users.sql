; with Users as (
  select ID, CandidateRef, forename, surname, Middlename from [dbo].[xxUserTable]-- where CandidateRef = '3467330'
  UNION
  select UserId, CandidateRef, forename, surname, Middlename from [dbo].[xxWAREHOUSE_UserTable] --where CandidateRef = '3467330'
  )

set identity_insert [Candidates2] on

INSERT [dbo].[Candidates2] (CandidateRef, uln, FirstName, MiddleName, LastName, Gender, AddressLine1, AddressLine2, Town, PostCode, email, Telephone, DOB, AccountExpiryDate)
select Users.CandidateRef 
	,Users.uln
	, Missing.FirstName
	, Missing.MiddleName
	, Missing.LastName
	, Users.Gender
	, Users.AddressLine1
	, Users.AddressLine2
	, Users.Town
	, Users.PostCode
	, Users.email
	, Users.Telephone
	, Users.DOB
	, Users.AccountExpiryDate
into #table
FROM (
	SELECT  cs.[CandidateID]
		,cs.[CandidateScheduleID]
		,[SurpassCandidateId]
		,cs.[CandidateRef]
		,cs.[FirstName]
		,cs.[LastName]
		,cs.[MiddleName]
	FROM [BC_TPM_Extract].[dbo].[CandidateSchedules] cs
	left join Candidates c on c.CandidateID = cs.CandidateID  
	where cs.CandidateID is null
) Missing
left join (
  select ID, CandidateRef, uln, Forename, MiddleName, Surname, Gender, AddressLine1, AddressLine2, Town,  PostCode, email, Telephone,  DOB, AccountExpiryDate from [dbo].[xxUserTable]-- where CandidateRef = '3467330'
  UNION
  select UserId, CandidateRef, uln, Forename, MiddleName, Surname, Gender, AddressLine1, AddressLine2, Town, PostCode, email, Telephone,  DOB, AccountExpiryDate from [dbo].[xxWAREHOUSE_UserTable] --where CandidateRef = '3467330'
)  Users
 on Missing.SurpassCandidateId = Users.ID
	and missing.FirstName = users.Forename
	and missing.LastName = users.Surname
	
set identity_insert [Candidates2] off






INSERT [dbo].[Candidates2] (CandidateRef, uln, FirstName, MiddleName, LastName, Gender, AddressLine1, AddressLine2, Town, PostCode, email, Telephone, DOB, AccountExpiryDate)
select CandidateRef, uln, FirstName, MiddleName, LastName, Gender, AddressLine1, AddressLine2, Town, PostCode, email, Telephone, DOB, AccountExpiryDate
from #table



select * from [Candidates2]




select ID, CandidateRef, uln, Forename, MiddleName, Surname, Gender, AddressLine1, AddressLine2, Town,  PostCode, email, Telephone,  DOB, AccountExpiryDate from [dbo].[xxUserTable] where Forename = 'AbdulSamad'
  UNION
select UserId, CandidateRef, uln, Forename, MiddleName, Surname, Gender, AddressLine1, AddressLine2, Town, PostCode, email, Telephone,  DOB, AccountExpiryDate from [dbo].[xxWAREHOUSE_UserTable]  where Forename = 'AbdulSamad'