Use master;

DECLARE @DefaultLocation TABLE (
     Value NVARCHAR(100)  
     , Data NVARCHAR(100)
) 

INSERT @DefaultLocation
EXEC  master.dbo.xp_instance_regread 
 N'HKEY_LOCAL_MACHINE', N'Software\Microsoft\MSSQLServer\MSSQLServer',N'BackupDirectory'

DECLARE @Location nvarchar(100) = 'E:\Backup'
--DECLARE @Location nvarchar(100) = (SELECT Data from @DefaultLocation)

SELECT N'BACKUP DATABASE ['+name+N'] TO DISK = N''' +  @Location + '\' + NAME + '.' + CONVERT(VARCHAR(10), GETDATE(), 102) + '.bak'' WITH NAME = N'''+name+N'- Database Backup'', COPY_ONLY, NOFORMAT, NOINIT, SKIP, COMPRESSION, NOREWIND, NOUNLOAD, STATS = 10;'  
FROM sys.databases 
where database_id > 4
ORDER BY name;


BACKUP DATABASE [BC_TPM_Extract] TO DISK = N'E:\Backup\BC_TPM_Extract.2017.03.30-1.bak' WITH NAME = N'BC_TPM_Extract- Database Backup', COPY_ONLY, NOFORMAT, NOINIT, SKIP, COMPRESSION, NOREWIND, NOUNLOAD, STATS = 10;
