use BC_TPM_Extract

select *
into CandidateSchedules_2_Backup
from CandidateSchedules_2



DELETE A
--SELECT A.*
FROM (
SELECT *
	, ROW_NUMBER () OVER (PARTITION BY ID, forename, surname, middlename, dob, gender, addressline1, addressline2, cast(town as nvarchar(MAX)), cast(country as nvarchar(MAX)), postcode, telephone, cast(email as nvarchar(MAX)), AccountCreationDate,AccountExpiryDate ORDER BY ID) R
FROM Candidates
) A
where R > 1
--ORDER BY 1;


--SELECT *
--INTO [Candidates_With_Duplicates]
--FROM Candidates


UPDATE CandidateSchedules
set  SurpassCandidateRef = REPLACE(SurpassCandidateRef, ',','')
	, FirstName = REPLACE(FirstName, ',','')
	, MiddleName = REPLACE(MiddleName, ',','')
	, LastName = REPLACE(LastName, ',','')

UPDATE CandidateSchedules
set SurpassCandidateRef = REPLACE(SurpassCandidateRef, '"','')
	, FirstName = REPLACE(FirstName, '"','')
	, MiddleName = REPLACE(MiddleName, '"','')
	, LastName = REPLACE(LastName, '"','')




--SELECT A.*
--FROM (
--SELECT ID
--	, COUNTRY
--	, County
--	--, ROW_NUMBER() OVER(PARTITION BY ID , COUNTRY , County order by County DESC) R
--	, ROW_NUMBER () OVER (PARTITION BY ID, forename, surname, middlename, dob, gender, addressline1, addressline2, cast(town as nvarchar(MAX)),  postcode, telephone, cast(email as nvarchar(MAX)), AccountCreationDate,AccountExpiryDate ORDER BY ID) R 
--FROM Candidates
--) A
--where R > 1
--order by 1