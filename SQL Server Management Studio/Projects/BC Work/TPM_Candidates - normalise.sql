--select *
--INTO [New_Candidates]
--FROM (
--select   ROW_NUMBER() OVER (
--					 ORDER BY CandidateID) NewCandidateID
--					 ,*
--from [dbo].[Candidates]
--) a  
USE BC_TPM_Extract

select *
from [dbo].[CandidateSchedules] CS
inner join [dbo].[Candidates] C
on Cs.[SurpassCandidateRef] = c.candidateref
	and c.firstname = cs.firstname
	and c.middlename = cs.middlename
	and c.lastname = cs.lastname
	and c.dob = cs.birthdate


	--create clustered index [pk_ScheduledPackageCandidateId] on CandidateSchedules_New (ScheduledPackageCandidateId)
/****** Script for SelectTopNRows command from SSMS  ******/
--SELECT *
--  FROM [BC_TPM_Extract].[dbo].[CandidateTests]
--where FirstName = 'Martin'
--	and LastName = 'Lowder'

--/****** Script for SelectTopNRows command from SSMS  ******/
--SELECT *
--  FROM [BC_TPM_Extract].[dbo].xxScheduledPackageCandidates
--where FirstName = 'Martin'
--	and LastName = 'Lowder'