select CS.ScheduledPackage_ScheduledPackageId
	, tp.[NewCentreID]
from CandidateSchedules CS
inner join CandidateExams CE on CE.CandidateScheduleID = CS.ScheduledPackageCandidateId
inner join TestPackages_New tp on tp.ScheduledPackageID = CE.CandidateScheduleID

--inner join [ScheduledPackageCentreLookup] SC on SC.[ScheduledPackageId] = tp.ScheduledPackageID 
inner join [New_Centres] C on c.[NewCentreID] = tp.[NewCentreID]

--INNER JOIN Centres C ON c.CentreID = tp.CentreID 
--	and c.CentreID = sc.[CenterId]
--	and c.CentreCode = sc.centerref
--	and c.CentreName = sc.CenterName
INNER JOIN Qualifications q ON q.[QualificationID] = tp.QualificationID
INNER JOIN Packages p ON p.packageID = tp.Packageid
INNER JOIN PackageExams pe ON PE.PackageExamId = CE.PackageExamId
INNER JOIN Candidates CA ON cA.[NewCandidateID] = CS.[NewCandidateID]
WHERE  [ScheduledExamRef] in ('26M7LL01') --('DWT8UN01','3VFEH301','4YQBMB01','NR5NSK01','55FHP501','KEPCHM01','DWT8UN01','BG69C401','XAN43M01','RPRDZT01','735JS701','QQ8JRK01','BYX4AR01','HQSQC901','7FLSRZ01','2P8ZGZ01','NGH4VR01','32DWV601','JVNZTC01','P7TEW501','W6EAVD01','HG8BH401','J943YN01','PWP5ZE01','C8K7CK01','D9URYL01','6HFYAH01','FCMLTU01','VRCH8301','72FVFC01','HGXXB401','C65GDC01','26M7LL01','QJRPAW01','BUYWG301','6TDJPP01')
	order by [ScheduledExamRef]

begin tran


select CS.ScheduledPackage_ScheduledPackageId
	, tp.[NewCentreID]
	,b.*
from CandidateSchedules CS
inner join CandidateExams CE on CE.CandidateScheduleID = CS.ScheduledPackageCandidateId
inner join TestPackages_New tp on tp.ScheduledPackageID = CE.CandidateScheduleID
inner join [New_Centres] C on c.[NewCentreID] = tp.[NewCentreID]
inner join ScheduledPackageCentreLookup b on tp.ScheduledPackageID = b.[ScheduledPackageId]
WHERE  [ScheduledExamRef] in ('26M7LL01') 

select *
from  ScheduledPackageCentreLookup
where ScheduledPackageId = 293


UPDATE TestPackages_New
set [NewCentreID] = NC.newcentreid
FROM TestPackages_New A
inner join ScheduledPackageCentreLookup b 
on a.ScheduledPackageID = b.[ScheduledPackageId]
inner join [New_Centres] nc
on b.[CenterName] = NC.[CentreName]
	and b.Centerref = nc.centrecode

select CS.ScheduledPackage_ScheduledPackageId
	, tp.[NewCentreID]
from CandidateSchedules CS
inner join CandidateExams CE on CE.CandidateScheduleID = CS.ScheduledPackageCandidateId
inner join TestPackages_New tp on tp.ScheduledPackageID = CE.CandidateScheduleID
inner join [New_Centres] C on c.[NewCentreID] = tp.[NewCentreID]
WHERE  [ScheduledExamRef] in ('26M7LL01') 

rollback



--UPDATE [New_Centres] 
--set [NewCentreID] = l.centreid
--FROM [New_Centres] NC
--INNER JOIN  ScheduledPackageCentreLookup L
--on L.[CenterName] = NC.[CentreName]
--	and l.Centreref = nc.centrecode

--select * 
--from ScheduledPackageCentreLookup 
--where ScheduledPackageId = 73

--select *
--from TestPackages_New
--where centreid = 34




--	select * from New_centres where [CentreName] = 'British Council - Warsaw'

--select * from [dbo].[ScheduledPackageCentreLookup]  where [CenterId] in (40,65)


--select  C.*
--from New_Centres NC
--INNER JOIN xxCentreTable C 
--	ON c.CentreCode = NC.centrecode
--	and c.CentreName = NC.CentreName



SELECT *
FROM [New_Centres]
where centreid = 34

select *
from TestPackages
where centreid = 34

--drop table TestPackages_New
--select distinct tp.*
--	, Newcentreid
--into [TestPackages_New]
--from [TestPackages] tp
--inner join CandidateSchedules cs
--on tp.ScheduledPackageID = CS.ScheduledPackageCandidateId
--inner join [ScheduledPackageCentreLookup] L
--on l.scheduledpackageid = tp.scheduledpackageid
--	and l.centerid = tp.CentreID
--inner join [New_Centres] NC
--on nc.centreid = l.centerid 
--	and nc.centrename = l.[CenterName]
--where tp.[ScheduledPackageID] = 293


select *
from [ScheduledPackageCentreLookup] sc
where sc.[CenterId] = 34