SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

select  Spc.ScheduledPackageCandidateId [CandidateScheduleID]	
	,ScheduledExamRef
	, QualificationId
	, QualificationName
	, QualificationRef
	--, SurpassExamId
	--, SurpassExamRef
from ScheduledPackageCandidateExams as SPCE

	inner join ScheduledPackageCandidates as SPC
	on SPC.ScheduledPackageCandidateId =  SPCE.Candidate_ScheduledPackageCandidateId
	inner join ScheduledPackages as SP
     on SP.ScheduledPackageId =  SPC.ScheduledPackage_ScheduledPackageId
    inner join [dbo].[PackageExams] pe
    on pe.[PackageExamId] = SPCE.[PackageExamId]
    inner join [dbo].[Packages] pack
    on pack.[PackageId] = pe.[PackageId]
--where ScheduledExamRef in ('QSWC4701','DYTU3U01','8VZMYD01','REZ3Q401','2DPDNF01','BJNVRG01','HEA2M201','43L97Y01','HWA6CE01')
where QualificationId in (
	122,
	136,
	167,
	172,
	235
	)

drop table #test

select	distinct
	QualificationId
	, QualificationName
	, QualificationRef
into #test
FROM [dbo].[ScheduledPackages]
--WHERE QualificationId in (103,136)

SELECT a.qualificationid
FROM (
select ROW_NUMBER () OVER (partition by  qualificationid order by qualificationid) r
	,*
FROM #test
) a
where R > 1








select 

	QualificationId
	--, QualificationName
	--, QualificationRef
FROM [dbo].[ScheduledPackages]
--WHERE QualificationId = 136
group by QualificationId
	, QualificationName
	, QualificationRef
having count(*) > 1


/****** Script for SelectTopNRows command from SSMS  ******/
SELECT   [QualificationID]
     -- ,count( [QualificationID])
  FROM [ScheduledPackages]
  group by  [QualificationID]
  having count( [QualificationID]) > 1
  order by 1
