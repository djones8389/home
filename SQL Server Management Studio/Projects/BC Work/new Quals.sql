/****** Script for SelectTopNRows command from SSMS  ******/
SELECT DISTINCT [QualificationId]
      ,[QualificationName]
      ,[QualificationRef]
  FROM [BC_TPM_Extract].[dbo].[QualCorrectInfo]
  
  order by 1

  select 
	distinct QualificationID	
	  , QualificationName
	  , QualificationRef
  from [dbo].[Qualifications]
  where QualificationID in (122,136,167,172,235)

  begin tran

  DELETE from [dbo].[Qualifications] where QualificationID = '122' and QualificationName = 'BTLTEST1' and QualificationRef = 'BTLTEST'
  DELETE from [dbo].[Qualifications] where QualificationID = '136' and QualificationName = 'Mixed Delivery Familiarisation Writing Speaking Listening' and QualificationRef = 'November 2012'
  DELETE from [dbo].[Qualifications] where QualificationID = '167' and QualificationName = 'AustriaAMS' and QualificationRef = 'AustriaAMS'
  DELETE from [dbo].[Qualifications] where QualificationID = '172' and QualificationName = 'Aptis  Advanced  MALAYSIA' and QualificationRef = 'Aptis Adv Malaysia'
  DELETE from [dbo].[Qualifications] where QualificationID = '235' and QualificationName = 'TRIAL CG0916 GVRL' and QualificationRef = 'CG0316 GVRL'



INSERT [dbo].[Qualifications] (QualificationID, QualificationName,QualificationRef)
VALUES (104,'BTLTEST1','BTLTEST')
	,(105,'Mixed Delivery Familiarisation Writing Speaking Listening','November 2012')
	,(106,'AustriaAMS','AustriaAMS')
	,(107,'Aptis  Advanced  MALAYSIA','Aptis Adv Malaysia')
	,(108,'TRIAL CG0916 GVRL','CG0316 GVRL')--,(109,'',''),(110,'',''),(113,'',''),(114,'',''),(115,'','')

	rollback