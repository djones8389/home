select C.CandidateRef, Keycode, Gender
from [BC_TPM_Extract].[dbo].CandidateSchedules CS
inner join CandidateExams CE on CE.CandidateScheduleID = CS.CandidateScheduleID
left join [BC_TPM_Extract].[dbo].[Candidates] C
on C.CandidateID = CS.CandidateID
where Gender is null
	 order by 2


SELECT *
FROM [BC_TPM_Extract_old].[dbo].[Candidates]
where FirstName in ('MOHAMED AMINE') and LastName = 'KIMAOUI'  --,'ZAKARIA','MON SWE')

SELECT *
FROM [BC_TPM_Extract].[dbo].[Candidates] 
where Gender IS NULL

SELECT *
FROM [BC_TPM_Extract_old].[dbo].[Candidates] Old
INNER JOIN [BC_TPM_Extract].[dbo].[Candidates] New
On Old.FirstName = New.FirstName
	and old.LastName = New.LastName
where New.Gender is null
	and old.Gender is not null

UPDATE [BC_TPM_Extract].[dbo].[Candidates] 
set Gender = old.Gender
	, dob = old.DOB
FROM [BC_TPM_Extract].[dbo].[Candidates] New
INNER JOIN [BC_TPM_Extract_old].[dbo].[Candidates] Old
On Old.FirstName = New.FirstName
	and old.LastName = New.LastName
where New.Gender is null
	and old.Gender is not null





	UPDATE [BC_TPM_Extract].[dbo].[Candidates]
set gender = m.gender

select m.gender, c.Gender
FROM [BC_TPM_Extract].[dbo].[Candidates] C
INNER JOIN [BC_TPM_Extract].[dbo].CandidateSchedules CS
on C.CandidateID = CS.CandidateID
inner join CandidateExams CE 
on CE.CandidateScheduleID = CS.CandidateScheduleID
INNER JOIN [dbo].[MissingGender] M
on M.CandidateRef = C.CandidateRef
	and M.forename = C.FirstName
	and m.surname = C.LastName
where c.Gender is null