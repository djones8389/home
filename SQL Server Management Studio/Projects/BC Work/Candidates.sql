/****** Script for SelectTopNRows command from SSMS  ******/
SELECT distinct c.*
  FROM [BC_TPM_Extract].[dbo].[Candidates_Full] c
  INNER JOIN dbo.CandidateSchedules cs
  ON cs.SurpassCandidateId = c.ID
  ORDER by 1;
  
  SELECT  *
  FROM [Candidates_Full]
  ORDER by 1;

  SELECT *
  INTO [Candidates_Full_16052017]
  FROM [Candidates_Full]

  --UPDATE [dbo].[Candidates_Full]
  --SET Retired = 0
  --WHERE Retired IS null

  --UPDATE [dbo].[Candidates_Full]
  --SET EthnicOriginID = '0'
  --WHERE CAST(EthnicOriginID AS NVARCHAR(MAX)) = 'Not Known / Not Provided'



  DELETE A
  FROM (
  SELECT 
	ROW_NUMBER() OVER (PARTITION BY  ID, CandidateRef, Forename, Surname, Middlename, DOB, Gender, AddressLine1, AddressLine2, CAST(Town AS NVARCHAR(MAX)), CAST(County AS NVARCHAR(MAX)), CAST(Country AS NVARCHAR(MAX)), PostCode, Telephone, CAST(Email AS NVARCHAR(MAX)), CAST(EthnicOriginID AS NVARCHAR(MAX)), AccountCreationDate, AccountExpiryDate, Retired order BY id) r
	,ID, CandidateRef, Forename, Surname, Middlename, DOB, Gender, AddressLine1, AddressLine2, Town, County, Country, PostCode, Telephone, Email, EthnicOriginID, AccountCreationDate, AccountExpiryDate, Retired
  FROM [dbo].[Candidates_Full]
  ) A
  WHERE r > 1

