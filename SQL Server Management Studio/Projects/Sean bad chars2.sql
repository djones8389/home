use master

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

IF OBJECT_ID('tempdb..#Results') IS NOT NULL DROP TABLE #Results;

CREATE TABLE #Results ( 
	Customer nvarchar(MAX) 
	, ItemText nvarchar(max)
	, ItemID nvarchar(max)
	, ASCIIChar nvarchar(max)
);


DECLARE @DYNAMIC NVARCHAR(MAX) = ''

SELECT @DYNAMIC +=CHAR(13) + '
	select a.*
	FROM (
	select
		'''+name+''' DB
		, [text]
		, ItemKey
		, case
			  when [text]  like ''%'' + CHAR(1) + ''%''  then 1 when [text]  like ''%'' + CHAR(1) + ''%''  then 1    
			  when [text]  like ''%'' + CHAR(2) + ''%''  then 2 when [text]  like ''%'' + CHAR(2) + ''%''  then 2    
			  when [text]  like ''%'' + CHAR(3) + ''%''  then 3 when [text]  like ''%'' + CHAR(3) + ''%''  then 3    
			  when [text]  like ''%'' + CHAR(4) + ''%''  then 4 when [text]  like ''%'' + CHAR(4) + ''%''  then 4    
			  when [text]  like ''%'' + CHAR(5) + ''%''  then 5 when [text]  like ''%'' + CHAR(5) + ''%''  then 5    
			  when [text]  like ''%'' + CHAR(6) + ''%''  then 6 when [text]  like ''%'' + CHAR(6) + ''%''  then 6    
			  when [text]  like ''%'' + CHAR(7) + ''%''  then 7 when [text]  like ''%'' + CHAR(7) + ''%''  then 7    
			  when [text]  like ''%'' + CHAR(8) + ''%''  then 8 when [text]  like ''%'' + CHAR(8) + ''%''  then 8    
			  when [text]  like ''%'' + CHAR(9) + ''%''  then 9 when [text]  like ''%'' + CHAR(9) + ''%''  then 9    
			  when [text]  like ''%'' + CHAR(10) + ''%''  then 10 when [text]  like ''%'' + CHAR(10) + ''%''  then 10    
			  when [text]  like ''%'' + CHAR(11) + ''%''  then 11 when [text]  like ''%'' + CHAR(11) + ''%''  then 11    
			  when [text]  like ''%'' + CHAR(12) + ''%''  then 12 when [text]  like ''%'' + CHAR(12) + ''%''  then 12    
			  when [text]  like ''%'' + CHAR(13) + ''%''  then 13 when [text]  like ''%'' + CHAR(13) + ''%''  then 13    
			  when [text]  like ''%'' + CHAR(14) + ''%''  then 14 when [text]  like ''%'' + CHAR(14) + ''%''  then 14    
			  when [text]  like ''%'' + CHAR(15) + ''%''  then 15 when [text]  like ''%'' + CHAR(15) + ''%''  then 15    
			  when [text]  like ''%'' + CHAR(16) + ''%''  then 16 when [text]  like ''%'' + CHAR(16) + ''%''  then 16    
			  when [text]  like ''%'' + CHAR(17) + ''%''  then 17 when [text]  like ''%'' + CHAR(17) + ''%''  then 17    
			  when [text]  like ''%'' + CHAR(18) + ''%''  then 18 when [text]  like ''%'' + CHAR(18) + ''%''  then 18    
			  when [text]  like ''%'' + CHAR(19) + ''%''  then 19 when [text]  like ''%'' + CHAR(19) + ''%''  then 19    
			  when [text]  like ''%'' + CHAR(20) + ''%''  then 20 when [text]  like ''%'' + CHAR(20) + ''%''  then 20    
			  when [text]  like ''%'' + CHAR(21) + ''%''  then 21 when [text]  like ''%'' + CHAR(21) + ''%''  then 21    
			  when [text]  like ''%'' + CHAR(22) + ''%''  then 22 when [text]  like ''%'' + CHAR(22) + ''%''  then 22    
			  when [text]  like ''%'' + CHAR(23) + ''%''  then 23 when [text]  like ''%'' + CHAR(23) + ''%''  then 23    
			  when [text]  like ''%'' + CHAR(24) + ''%''  then 24 when [text]  like ''%'' + CHAR(24) + ''%''  then 24    
			  when [text]  like ''%'' + CHAR(25) + ''%''  then 25 when [text]  like ''%'' + CHAR(25) + ''%''  then 25    
			  when [text]  like ''%'' + CHAR(26) + ''%''  then 26 when [text]  like ''%'' + CHAR(26) + ''%''  then 26    
			  when [text]  like ''%'' + CHAR(27) + ''%''  then 27 when [text]  like ''%'' + CHAR(27) + ''%''  then 27    
			  when [text]  like ''%'' + CHAR(28) + ''%''  then 28 when [text]  like ''%'' + CHAR(28) + ''%''  then 28    
			  when [text]  like ''%'' + CHAR(29) + ''%''  then 29 when [text]  like ''%'' + CHAR(29) + ''%''  then 29    
			  when [text]  like ''%'' + CHAR(30) + ''%''  then 30 when [text]  like ''%'' + CHAR(30) + ''%''  then 30    
			  when [text]  like ''%'' + CHAR(31) + ''%''  then 31 when [text]  like ''%'' + CHAR(31) + ''%''  then 31    
	  		else ''0''
		End as [Char]
		from '+name+'.dbo.DimOptions
	) a
	where Char != 0
'
from sys.databases
where name like '%SurpassDataWarehouse'

INSERT #Results
EXEC(@DYNAMIC)


SELECT *
FROM #Results




--declare @COMMAND NVARCHAR(MAX) = '
--USE [?];
--if(''?'' like ''%Aatpreview_SurpassDataWarehouse%'') BEGIN 	select a.* 	FROM ( 	select	db_name() DB, [text], ItemKey, case when [text] like ''%'' + CHAR(1) + ''%'' then 1 when [text] like ''%''+CHAR(1)+''%'' then 1  when [text] like ''%'' + CHAR(2)+''%'' then 2 when [text] like ''%''+CHAR(2)+''%'' then 2  when [text] like ''%''+CHAR(3)+''%'' then 3 when [text] like ''%''+CHAR(3)+''%'' then 3  when [text] like ''%''+CHAR(4)+''%'' then 4 when [text] like ''%''+CHAR(4)+''%'' then 4  when [text] like ''%''+CHAR(5)+''%'' then 5 when [text] like ''%''+CHAR(5)+''%'' then 5  when [text] like ''%''+CHAR(6)+''%'' then 6 when [text] like ''%''+CHAR(6)+''%'' then 6  when [text] like ''%''+CHAR(7)+''%'' then 7 when [text] like ''%''+CHAR(7)+''%'' then 7  when [text] like ''%''+CHAR(8)+''%'' then 8 when [text] like ''%''+CHAR(8)+''%'' then 8  when [text] like ''%''+CHAR(9)+''%'' then 9 when [text] like ''%''+CHAR(9)+''%'' then 9  when [text] like ''%''+CHAR(10)+''%'' then 10 when [text] like ''%''+CHAR(10)+''%'' then 10  when [text] like ''%''+CHAR(11)+''%'' then 11 when [text] like ''%''+CHAR(11)+''%'' then 11  when [text] like ''%''+CHAR(12)+''%'' then 12 when [text] like ''%''+CHAR(12)+''%'' then 12  when [text] like ''%''+CHAR(13)+''%'' then 13 when [text] like ''%''+CHAR(13)+''%'' then 13  when [text] like ''%''+CHAR(14)+''%'' then 14 when [text] like ''%''+CHAR(14)+''%'' then 14  when [text] like ''%''+CHAR(15)+''%'' then 15 when [text] like ''%''+CHAR(15)+''%'' then 15  when [text] like ''%''+CHAR(16)+''%'' then 16 when [text] like ''%''+CHAR(16)+''%'' then 16  when [text] like ''%''+CHAR(17)+''%'' then 17 when [text] like ''%''+CHAR(17)+''%'' then 17  when [text] like ''%''+CHAR(18)+''%'' then 18 when [text] like ''%''+CHAR(18)+''%'' then 18  when [text] like ''%''+CHAR(19)+''%'' then 19 when [text] like ''%''+CHAR(19)+''%'' then 19  when [text] like ''%''+CHAR(20)+''%'' then 20 when [text] like ''%''+CHAR(20)+''%'' then 20  when [text] like ''%''+CHAR(21)+''%'' then 21 when [text] like ''%''+CHAR(21)+''%'' then 21  when [text] like ''%''+CHAR(22)+''%'' then 22 when [text] like ''%''+CHAR(22)+''%'' then 22  when [text] like ''%''+CHAR(23)+''%'' then 23 when [text] like ''%''+CHAR(23)+''%'' then 23  when [text] like ''%''+CHAR(24)+''%'' then 24 when [text] like ''%''+CHAR(24)+''%'' then 24  when [text] like ''%''+CHAR(25)+''%'' then 25 when [text] like ''%''+CHAR(25)+''%'' then 25  when [text] like ''%''+CHAR(26)+''%'' then 26 when [text] like ''%''+CHAR(26)+''%'' then 26  when [text] like ''%''+CHAR(27)+''%'' then 27 when [text] like ''%''+CHAR(27)+''%'' then 27  when [text] like ''%''+CHAR(28)+''%'' then 28 when [text] like ''%''+CHAR(28)+''%'' then 28  when [text] like ''%''+CHAR(29)+''%'' then 29 when [text] like ''%''+CHAR(29)+''%'' then 29  when [text] like ''%''+CHAR(30)+''%'' then 30 when [text] like ''%''+CHAR(30)+''%'' then 30  when [text] like ''%''+CHAR(31)+''%'' then 31 when [text] like ''%''+CHAR(31)+''%'' then 31 	else ''0''	End as [Char] from DimOptions ) a where Char != 0 END
--'

----INSERT #Results
--exec sp_msforeachdb @Command



/*declare @sql nvarchar(MAX) = ''

SELECT @SQL +=CHAR(13) + 
'select ''rcpch'' as ''Client'', [text] as [CHAR'+cast(r as nvarchar(10))+'] FROM  rcpch_surpassdatawarehouse..DimOptions WHERE [text] like ''%'' + CHAR('+cast(r as nvarchar(10)) +')+''%'' UNION'
from (
select top 31  ROW_NUMBER() OVER(partition by totcpu order by totcpu) R
from master.sys.syslogins
) a

SELECT @SQL = (
	SELECT SUBSTRING(@SQL, 0, LEN(@SQL)-5)
)

 
exec(@SQL)

--DECLARE myCursor CURSOR FOR
--SELECT top 1 Name
--from sys.databases
--where name like '%SurpassDataWarehouse'

--DECLARE @DB sysname

--OPEN myCursor

--FETCH NEXT FROM myCursor INTO @DB

--WHILE @@Fetch_Status = 0


--BEGIN

--SELECT @SQL = REPLACE(@SQL, 'select ''', 


--FETCH NEXT FROM myCursor INTO @DB

--END
--CLOSE myCursor
--DEALLOCATE myCursor



USE RCPCH_SUrpassDataWarehouse

SELECT 
	case when [text]  like '%' + CHAR(9) + '%'  then '9'
		 when [text]  like '%' + CHAR(10) + '%'  then '10'		 
	else 'n'
	End as [Chars]
FROM DimOptions
Where Text = 'Give IV DDAVP and oral tranexamic acid '






	else ''n''
	End as [Chars]


select
 '  
	 when [text]  like ''%'' + CHAR('+cast(R as varchar(2))+') + ''%''  then '+cast(R as varchar(2))+'
		 when [text]  like ''%'' + CHAR('+cast(R as varchar(2))+') + ''%''  then '+cast(R as varchar(2))+'		 

	'
from (
select top 31  ROW_NUMBER() OVER(partition by totcpu order by totcpu) R
from master.sys.syslogins
) a

*/
