--cd C:\Program Files (x86)\Red Gate\SQL Compare 12 
DECLARE @Path nvarchar(100) = 'D:\DemoSchemaCompare'
DECLARE @Password nvarchar(100) = 'PASSWORDPLACEHOLDER'

select 'sqlcompare /server1:172.16.100.162 /db1:'+name+' /userName1:davej /password1:'+@Password+' /server2:172.16.100.226 /db2:'+name+' /userName2:davej /password2:'+@Password+' 
/include:identical /exclude:role /exclude:user /exclude:XmlSchemaCollection 
/report:"'+@Path+'\Demo R12 Mirror - Live '+substring(name,CHARINDEX('_',name)+1, 20)+'.html" /reportType:Interactive'  
from sys.databases
where name like 'Demo%'



/*
	SELECT 'sqlcompare /server1:172.16.100.162 /db1:Demo_AnalyticsManagement /userName1:davej /password1:PASSWORD123 /server2:172.16.100.226 /db2:Demo_AnalyticsManagement /userName2:davej /password2:PASSWORD123 /exclude:role /exclude:user /exclude:XmlSchemaCollection /report:"D:\Demo R12 Mirror - Live AnalyticsManagement.html" /reportType:Simple'
	UNION
	SELECT 'sqlcompare /server1:172.16.100.162 /db1:Demo_ContentAuthor /userName1:davej /password1:PASSWORD123 /server2:172.16.100.226 /db2:Demo_ContentAuthor /userName2:davej /password2:PASSWORD123 /exclude:role /exclude:user /exclude:XmlSchemaCollection /report:"D:\Demo R12 Mirror - Live ContentAuthor.html" /reportType:Simple'
	UNION
	SELECT 'sqlcompare /server1:172.16.100.162 /db1:Demo_ItemBank /userName1:davej /password1:PASSWORD123 /server2:172.16.100.226 /db2:Demo_ItemBank /userName2:davej /password2:PASSWORD123 /exclude:role /exclude:user /exclude:XmlSchemaCollection /report:"D:\Demo R12 Mirror - Live ItemBank.html" /reportType:Simple'
	UNION
	SELECT 'sqlcompare /server1:172.16.100.162 /db1:Demo_SecureAssess /userName1:davej /password1:PASSWORD123 /server2:172.16.100.226 /db2:Demo_SecureAssess /userName2:davej /password2:PASSWORD123 /exclude:role /exclude:user /exclude:XmlSchemaCollection /report:"D:\Demo R12 Mirror - Live SecureAssess.html" /reportType:Simple'
	UNION
	SELECT 'sqlcompare /server1:172.16.100.162 /db1:Demo_SecureMarker /userName1:davej /password1:PASSWORD123 /server2:172.16.100.226 /db2:Demo_SecureMarker /userName2:davej /password2:PASSWORD123 /exclude:role /exclude:user /exclude:XmlSchemaCollection /report:"D:\Demo R12 Mirror - Live SecureMarker.html" /reportType:Simple'
	UNION
	SELECT 'sqlcompare /server1:172.16.100.162 /db1:Demo_SurpassDataWarehouse /userName1:davej /password1:PASSWORD123 /server2:172.16.100.226 /db2:Demo_SurpassDataWarehouse /userName2:davej /password2:PASSWORD123 /exclude:role /exclude:user /exclude:XmlSchemaCollection /report:"D:\Demo R12 Mirror - Live SurpassDataWarehouse.html" /reportType:Simple'
	UNION
	SELECT 'sqlcompare /server1:172.16.100.162 /db1:Demo_SurpassManagement /userName1:davej /password1:PASSWORD123 /server2:172.16.100.226 /db2:Demo_SurpassManagement /userName2:davej /password2:PASSWORD123 /exclude:role /exclude:user /exclude:XmlSchemaCollection /report:"D:\Demo R12 Mirror - Live SurpassManagement.html" /reportType:Simple'  

*/