--select 
--	top 100 *
--	,	cast(StartDate as date)
--from [dbo].[DBGrowthMetrics]
--where Instance = '311619-WEB3'
--	and database_name = 'wjec_secureassess'


SELECT 
	A.Instance [server_name]
	 ,A.[database_name]
	, [StartDate]
	, SUM(a.data_kb) + SUM(a.index_size) [total]
	, SUM(a.data_kb) data_kb
	, SUM(a.index_size) index_size
FROM (
SELECT 
     [Instance]
      , [database_name]
	  ,	cast(StartDate as date) StartDate
      , cast(replace([data_kb], 'KB','') as float) [data_kb]
      , cast(replace([index_size], 'KB','') as float) [index_size]
  FROM [PSCollector].[dbo].[DBGrowthMetrics]
   where Instance = '311619-WEB3'
	and database_name = 'wjec_secureassess'
	and cast(StartDate as date) > '2017-04-01'

) A

group by 
	A.Instance
	, A.[database_name]
	, a.StartDate
order by 1,2,3