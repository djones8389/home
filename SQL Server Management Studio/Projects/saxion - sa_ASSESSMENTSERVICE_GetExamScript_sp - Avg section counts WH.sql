SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;   

use rgu_SecureAssess

--if object_id('tempdb..#resultset') is not null drop table #resultset

--create table #resultset (
--	daterange nvarchar(100)
--	, examCount int	
--)

--INSERT #resultset
select ISNULL(AVG(f),0)  'Avg. No. Of Sections > 2018-01-29'
	, count(examsessionid) [ExamCount]
from (
select count([SectionID]) F, a.examsessionid
from [dbo].[WAREHOUSE_ExamSectionsTable] a
inner join warehouse_examsessiontable_shreded b
on a.examsessionid = b.examsessionid
where b.started > '2018-01-29'
group by a.examsessionid
) g



select ISNULL(AVG(f),0)  ['Avg. No. Of Sections 2018-01-22' - '2018-01-29']
	, count(examsessionid) [ExamCount]
from (
select count([SectionID]) F, a.examsessionid
from [dbo].[WAREHOUSE_ExamSectionsTable] a
inner join warehouse_examsessiontable_shreded b
on a.examsessionid = b.examsessionid
where  b.started between '2018-01-22' and '2018-01-29'
group by a.examsessionid
) g



select ISNULL(AVG(f),0)  ['Avg. No. Of Sections 2018-01-15' - '2018-01-22']
	, count(examsessionid) [ExamCount]
from (
select count([SectionID]) F, a.examsessionid
from [dbo].[WAREHOUSE_ExamSectionsTable] a
inner join warehouse_examsessiontable_shreded b
on a.examsessionid = b.examsessionid
where b.started between '2018-01-15' and '2018-01-22'
group by a.examsessionid
) g



select ISNULL(AVG(f),0)  ['Avg. No. Of Sections 2018-01-08' - '2018-01-15']
	, count(examsessionid) [ExamCount]
from (
select count([SectionID]) F, a.examsessionid
from [dbo].[WAREHOUSE_ExamSectionsTable] a
inner join warehouse_examsessiontable_shreded b
on a.examsessionid = b.examsessionid
where  b.started between '2018-01-08' and '2018-01-15'
group by a.examsessionid
) g



select ISNULL(AVG(f),0)  ['Avg. No. Of Sections 2018-01-01' - '2018-01-08']
	, count(examsessionid) [ExamCount]
from (
select count([SectionID]) F, a.examsessionid
from [dbo].[WAREHOUSE_ExamSectionsTable] a
inner join warehouse_examsessiontable_shreded b
on a.examsessionid = b.examsessionid
where b.started between '2018-01-01' and '2018-01-08'
group by a.examsessionid
) g



select ISNULL(AVG(f),0)  ['Avg. No. Of Sections 2017-12-24' - '2017-12-31']
	, count(examsessionid) [ExamCount]
from (
select count([SectionID]) F, a.examsessionid
from [dbo].[WAREHOUSE_ExamSectionsTable] a
inner join warehouse_examsessiontable_shreded b
on a.examsessionid = b.examsessionid
where b.started between '2017-12-24' and '2017-12-31'
group by a.examsessionid
) g

select ISNULL(AVG(f),0)  ['Avg. No. Of Sections 2017-12-17' - '2017-12-24']
	, count(examsessionid) [ExamCount]
from (
select count([SectionID]) F, a.examsessionid
from [dbo].[WAREHOUSE_ExamSectionsTable] a
inner join warehouse_examsessiontable_shreded b
on a.examsessionid = b.examsessionid
where b.started between '2017-12-17' and '2017-12-24'
group by a.examsessionid
) g


select ISNULL(AVG(f),0)  ['Avg. No. Of Sections 2017-12-10' - '2017-12-17']
	, count(examsessionid) [ExamCount]
from (
select count([SectionID]) F, a.examsessionid
from [dbo].[WAREHOUSE_ExamSectionsTable] a
inner join warehouse_examsessiontable_shreded b
on a.examsessionid = b.examsessionid
where b.started between '2017-12-10' and '2017-12-17'
group by a.examsessionid
) g


--select started
--from warehouse_examsessiontable_shreded b
--where b.started > '2018-01-29'
