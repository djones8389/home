--MIGRATE QUESTIONS

Select KeyCode
from WAREHOUSE_ExamSessionTable
where id = 3368


USE Saxion_SecureAssess

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

;WITH itemlist0 AS (
    SELECT EST.ID
        ,t.c.value('@id', 'nvarchar(50)') CPID
        ,t.c.value('@version', 'int') Ver
        ,CAST(t.c.exist('p') AS INT) PExist
		--,resultDataFull
    FROM 
		WAREHOUSE_ExamSessionTable EST
		CROSS APPLY EST.resultDataFull.nodes('assessmentDetails/assessment/section/item') t(c)
    WHERE 
		--EST.warehouseTime > '1900-01-01 00:00:00'
		--AND EST.warehouseTime < '2050-01-01 00:00:00'
		EST.warehouseTime > '1900-01-01 00:00:00.000'    
		AND EST.warehouseTime < '2050-01-01 00:00:00.000'  
		AND EST.warehouseExamState = 5
		and est.id = 3368
)
,itemlist AS (
	SELECT 
		CPID
		, Ver
		, CHARINDEX('P', CPID) ix
		, MAX(ID) MaxID
	FROM 
		itemlist0
	WHERE 
		PExist = 1
	GROUP BY 
		CPID, Ver

	UNION ALL

	SELECT 
		CPID
		, Ver
		, CHARINDEX('P', CPID) ix
		, MAX(ID) MaxID
	FROM 
		itemlist0
	GROUP BY 
		CPID, Ver
	HAVING 
		SUM(PExist) = 0
),EST AS (
	SELECT 
		ID
		,CONVERT(NVARCHAR(50), CONVERT(VARCHAR(MAX), t.c.query('data(@id)'))) CPID
		,CONVERT(NVARCHAR(50), CONVERT(VARCHAR(MAX), t.c.query('data(@version)'))) CPVersion
		,CONVERT(INT, CONVERT(VARCHAR(MAX), t.c.query('data(@type)'))) CPType
		,CONVERT(NVARCHAR(100), CONVERT(NVARCHAR(MAX), t.c.query('data(@name)'))) QuestionName
		,CONVERT(VARCHAR(MAX), t.c.query('data(@totalMark)')) TotalMark
		,CONVERT(VARCHAR(MAX), t.c.query('data(@markingType)')) MarkingTypeKey
		,t.c.query('.') ItemXML
	FROM 
		WAREHOUSE_ExamSessionTable EST
		CROSS APPLY EST.resultDataFull.nodes('assessmentDetails/assessment/section/item') t(c)
)
SELECT 
	IL.CPID
	,SUBSTRING(IL.CPID, 1, IL.ix-1) ProjectKey
	,SUBSTRING(IL.CPID, IL.ix+1, LEN(IL.CPID) - IL.ix) ItemKey
	,IL.Ver ItemVersion
	,EST.CPType
	,EST.QuestionName
	--,CASE ISNUMERIC(EST.TotalMark)
	--	WHEN 0 THEN NULL
	--	ELSE CAST(EST.TotalMark AS FLOAT)
	--END TotalMark
	--,CONVERT(NVARCHAR(MAX), CPAT.ItemXML.query('/P/metaData/HTML/markingCriteria')) MarkingCriteria
	--,CASE When ISNUMERIC(EST.MarkingTypeKey) = 1 Then Convert(int,EST.MarkingTypeKey) Else NULL END MarkingTypeKey
	--,ISNULL(CPAT.CorrectAnswers, '') DerivedCorrectAnswer
	--,ISNULL(CPAT.ShortCorrectAnswers, '') ShortDerivedCorrectAnswer
	--,ISNULL(CPAT.QuestionTypes, 'Not Known') QuestionTypes
	--,CASE 
	--	WHEN CPAT.ItemXML IS NULL THEN 0
	--	ELSE 1
	--END HasCPAuditEntry
	,ISNULL(CPAT.ItemXML, EST.ItemXML) ItemXML
	--,0 Priority
	,CPAT.ItemXML CPATItemXML
	, EST.ItemXML ESTItemXML
	,0 Priority
	,1 Source
	,CASE 
		WHEN CPAT.ItemXML.value('count(/P/S/C[@typ=10]/I[@wei>0])','int')>1 THEN 1	--weighted
		WHEN CPAT.ItemXML.value('count(/P/S/C[@typ=10]/M/CB)','int')>0 THEN 2		--combination
		ELSE NULL
	END MarkType
FROM 
	EST
	JOIN itemlist IL ON EST.ID = IL.MaxID
        AND EST.CPID = IL.CPID
        AND EST.CPVersion = IL.Ver
	LEFT JOIN CPAuditTable CPAT ON EST.CPID = CPAT.ItemID
		AND EST.CPVersion = CPAT.ItemVersion
		--where IL.CPID = '381P631'

		--select top 10 * from #sample

SELECT   a.*
FROM CPAuditTable A 
inner join #sample b
on a.ItemID = b.cpid
	and a.ItemVersion = b.ItemVersion
where a.itemid in ('381P613','381P611','381P633','381P618','381P630','381P609','381P632','381P617','381P608','381P612','381P631','381P699','381P615','381P616')

