use demo_contentauthor;

if object_id('tempdb..#ItemTable') is not null drop table #ItemTable;

;with LiveItems as (
select i.id ItemID, s.id [SubjectID], s.title [SubjectName]
from items I
inner join WorkflowStatuses w on i.WorkFlowStatusID = w.id
inner join Subjects s on s.id = i.subjectid
where isPublishable = 1	and isDeleted = 1
) 

SELECT A.*
INTO #ItemTable
FROM (
	SELECT [SubjectID]
		, [SubjectName]
		, SpecialItems.id
		, [Type]
		, (SELECT COUNT(1) FROM LiveItems) TotalCount
	FROM LiveItems
	INNER JOIN (
		select id, 'MatchingBoxesItems' [Type] from [dbo].[MatchingBoxesItems] union 
		select id, 'SelectFromAListItems' [Type] from [dbo].[SelectFromAListItems] union 
		select id, 'FillInTheBlankItems' [Type] from [dbo].[FillInTheBlankItems] union 
		select id, 'DragAndDropItems' [Type] from [dbo].[DragAndDropItems] union
		select id, 'EquationEntryItems' [Type] from [dbo].[EquationEntryItems] union
		select id, 'HotspotItems' [Type] from [dbo].[HotspotItems] union
		select id, 'VoiceCaptureItems' [Type] from [dbo].[VoiceCaptureItems] 
	) SpecialItems
	ON LiveItems.ItemID = SpecialItems.ID
) A 

SELECT B.*
	, cast(cast((cast([RowCount] as float)/cast(TotalCount as float))*100 as decimal(8,2)) as varchar(10)) + ' %' [% of Items]
FROM (
	SELECT [SubjectID]
		, [SubjectName]
		, ID [ItemID]
		, [type]
		, TotalCount
		, (SELECT @@ROWCOUNT) [RowCount]
	FROM #ItemTable IT
) B
ORDER BY 1;