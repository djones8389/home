--Candidate--

DECLARE @esid int = 1559085

select foreName
	, surName
	, Username
from WAREHOUSE_UserTable WUT
inner join WAREHOUSE_ExamSessionTable WEST
on west.WAREHOUSEUserID = WUT.ID
where WEST.ID = @esid

--Marker--

select DISTINCT
		--warehouseexamsessionid,
		--a.b.value('data(userId)[1]','int') userID,
		a.b.value('data(userForename)[1]','nvarchar(100)') userForename
		,a.b.value('data(userSurname)[1]','nvarchar(100)') userSurname
		, Username
	from warehouse_examsessionitemresponsetable

	cross apply markerresponsedata.nodes ('entries/entry') a(b)

	inner join WAREHOUSE_UserTable
	on WAREHOUSE_UserTable.UserId = a.b.value('data(userId)[1]','int') 

	where warehouseexamsessionid = @esid
		and  a.b.value('data(userId)[1]','int') not in ('0', '-1')

--Moderator--

SELECT --[Warehoused_ExamSessionID],
		Forename
		, Surname
		, Username
		--, ChangeDateSubmitted
		--, [OriginalExamDuration]
		--, [NewExamDuration]
		--, Reason
FROM [WAREHOUSE_ExamSessionDurationAuditTable]
INNER JOIN [WAREHOUSE_UserTable]
on [WAREHOUSE_UserTable].ID = [WAREHOUSE_ExamSessionDurationAuditTable].Warehoused_UserID
where [Warehoused_ExamSessionID] = @esid

