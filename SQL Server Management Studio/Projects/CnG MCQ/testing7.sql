Use PRV_Evolve_CPProjectAdmin

SET NOCOUNT ON;

IF OBJECT_ID('tempdb..##Questions') IS NOT NULL DROP TABLE ##Questions;
IF OBJECT_ID('tempdb..##Answers') IS NOT NULL DROP TABLE ##Answers;

select 
	substring(Q.ID, 0, charindex('S', Q.ID)+2) UpToScene
	, SUBSTRING(Q.ID,charindex('C',Q.ID)+1, (Len(Q.ID)-charindex('I',Q.ID)))  C
	, cast(Q.Question.query('ItemValue/TEXTFORMAT/P//FONT/.//text()') as nvarchar(MAX)) Question 
INTO ##Questions
from (
		select ITT.ID	
			, CAST(ItemValue AS xml)  Question
		from [ItemTextBoxTable] ITT WITH (READUNCOMMITTED)
) Q

CREATE NONCLUSTERED INDEX [IX_NC] on ##QUESTIONS (UpToScene,C) INCLUDE(Question);

SELECT  ROW_NUMBER() OVER(PARTITION BY ParentID ORDER BY ID, [asC]) AS [N],
	 ParentID,
	 [asC] AS [AnswerAlias], 
	CAST(ItemXml.query('.//text()') AS nvarchar(max)) AS [AnswerText]
INTO ##Answers
FROM (
 SELECT ID,
	  ParentID,
	  [asC],
	  CAST(ItemValue AS xml) AS [ItemXml]
 FROM dbo.ItemMultipleChoiceTable WITH (READUNCOMMITTED)
) AS [A];

CREATE NONCLUSTERED INDEX [IX] ON [dbo].[##Answers] ([ParentID]) INCLUDE ([N],[AnswerAlias],[AnswerText]);

--SELECT  * FROM ##Answers WHERE n > 4 ORDER BY n desc

SELECT  
	B.parentID
	,(
		SELECT Question + '  '
		from ##QUESTIONS Q2
		where q2.UpToScene = b.UpToScene
			and (cast(b.c as int) - cast(q2.c as int)) in (1,2)
		for xml path ('')
	) Question
	,B.Answer1
	,B.Answer2
	,B.Answer3
	,B.Answer4
	,B.Answer5
	,B.Answer6
FROM (
		SELECT p.ParentID
			, substring(ParentID, 0, charindex('S',ParentID)+2) UpToScene
			, SUBSTRING(ParentID,charindex('C',ParentID)+1, (Len(ParentID)-charindex('I',ParentID))) C
		,[1] AS [Answer1], [2] AS [Answer2], [3] AS [Answer3], [4] AS [Answer4], [5] AS [Answer5], [6] AS [Answer6]
			
		FROM (
			SELECT ParentID, N, AnswerAlias + N': ' + AnswerText AS [AnswerText]
			FROM ##Answers
		) AS [S]
		PIVOT (
			MAX(AnswerText)
			FOR N IN ([1], [2], [3], [4], [5],[6])
		) AS [P]
)  B
WHERE Answer5 IS NOT NULL

-- 04 09 total

-- 32 seconds to execute the report