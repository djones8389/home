set transaction isolation level read uncommitted

Use PRV_Evolve_CPProjectAdmin

IF OBJECT_ID('tempdb..##Questions') IS NOT NULL DROP TABLE ##Questions;
IF OBJECT_ID('tempdb..##Answers') IS NOT NULL DROP TABLE ##Answers;

select Q.ID
	, cast(Q.Question.query('ItemValue/TEXTFORMAT/P//FONT/.//text()') as nvarchar(MAX)) Question 
INTO ##Questions
from (
		select ITT.ID	
			, CAST(ItemValue AS xml)  Question
		from [ItemTextBoxTable] ITT WITH (READUNCOMMITTED)
) Q

CREATE CLUSTERED INDEX [IX] ON ##QUESTIONS (id);

SELECT  ROW_NUMBER() OVER(PARTITION BY ParentID ORDER BY ID, [asC]) AS [N],
	 ParentID,
	 [asC] AS [AnswerAlias], 
	CAST(ItemXml.query('.//text()') AS nvarchar(max)) AS [AnswerText]
INTO ##Answers
FROM (
 SELECT ID,
	  ParentID,
	  [asC],
	  CAST(ItemValue AS xml) AS [ItemXml]
 FROM dbo.ItemMultipleChoiceTable WITH (READUNCOMMITTED)
) AS [X];

CREATE NONCLUSTERED INDEX [IX] ON [dbo].[##Answers] ([ParentID]) INCLUDE ([N],[AnswerAlias],[AnswerText]);

--UpToHere: 03:30

SELECT p.ParentID, [1] AS [Answer1], [2] AS [Answer2], [3] AS [Answer3], [4] AS [Answer4], [5] AS [Answer5]
INTO #answers2
FROM (
 SELECT ParentID, N, AnswerAlias + N': ' + AnswerText AS [AnswerText]
 FROM ##Answers
) AS [S]
PIVOT (
 MAX(AnswerText)
 FOR N IN ([1], [2], [3], [4], [5])
) AS [P]

--Further:  00:02

	
	
SELECT  
	b.parentID
	,(
		SELECT Question + '  '
		from ##QUESTIONS Q2
		where substring(Q2.ID, 0, charindex('S', Q2.ID)+2) = substring(b.ParentID, 0, charindex('S',b.ParentID)+2)
			and (cast(SUBSTRING(b.ParentID,charindex('C',b.ParentID)+1, (Len(b.ParentID)-charindex('I',b.ParentID)))  as int) - cast(SUBSTRING(q2.ID,charindex('C',q2.ID)+1, (Len(q2.ID)-charindex('I',q2.ID))) as int)) in (1,2)
			--and Q2.id like '344P1045%'
		for xml path ('')
	) Question
	,b.Answer1
	,b.Answer2
	,b.Answer3
	,b.Answer4
	,b.Answer5
FROM (
		select *
		from #answers2 
		--where parentid like '344P1045%'
) B

--This takes..over 2 min. v v slow. only 34 rows done..