select STG.ExamSessionKey
,	STG.CPID [StagingCPID]
,   lIVE.CPID [LiveCPID]
,   STG.CPVersion [StagingCPVersion]
,   LIVE.CPVersion [LiveCPVersion]
,   stg.Mark [StagingMark]
,   LIVE.Mark [LiveMark]
,   stg.ViewingTime [StagingViewingTime]
,   live.ViewingTime [LiveViewingTime]
,   est.viewingTime [SecureAssessViewingTime]
,ISNULL(
	CASE ISNUMERIC(markerUserMark) 
		WHEN 1 THEN  CAST(markerUserMark AS float) 
		ELSE NULL
	END /
	CASE ISNUMERIC(totalMark) 
		WHEN 1 THEN	
			CASE CAST(totalMark AS float) 
				WHEN 0 THEN 1
				ELSE CAST(totalMark AS float)
			END
		ELSE NULL
	END
	, CASE ISNUMERIC(userMark) 
		WHEN 1 THEN  CAST(userMark AS float) 
		ELSE NULL
	END 
) [SecureAssessMark]
from etl.stg_FactQuestionResponses STG
INNER JOIN dbo.FactQuestionResponses LIVE
on Live.ExamSessionKey = stg.ExamSessionKey
	and STG.CPID = LIVE.CPID
	and STG.CPVersion != LIVE.CPVersion
INNER JOIN (
	SELECT 
		EST.ID ESTID
		,ROW_NUMBER() Over(Partition by EST.ID order by EST.ID) ItemPresentationOrder 
		,T1.Section.value('data(@id)[1]','int') SectionID
		,T2.item.value('data(@id)[1]','nvarchar(50)') CPID
		,T2.item.value('data(@markerUserMark)[1]','nvarchar(50)') markerUserMark
		,T2.item.value('data(@userMark)[1]','nvarchar(50)') userMark
		,T2.item.value('data(@totalMark)[1]','nvarchar(50)') totalMark
		,T2.item.value('data(@viewingTime)[1]','int') viewingTime
		,T2.item.value('data(@userAttempted)[1]','bit') userAttempted
		,T2.item.value('data(@version)[1]','int') CPVersion
		,~T2.item.value('data(@nonScored)[1]','bit') Scored
	FROM PRV_BritishCouncil_SecureAssess.dbo.WAREHOUSE_ExamSessionTable EST  			
		CROSS APPLY EST.resultData.nodes('/exam/section') as T1(Section)
		CROSS APPLY T1.Section.nodes('item') as T2(item)
		
	WHERE 
		EST.warehouseTime > '1900-01-01 00:00:00.000'    
		AND EST.warehouseTime < '2050-01-01 00:00:00.000'    
		AND EST.warehouseExamState = 1 AND PreviousExamState <> 10
) EST
ON EST.ESTID = STG.ExamSessionKey
	and est.CPID = STG.CPID
	and est.CPVersion = stg.CPVersion

--DELETE FROM [FactMarkerResponse] where ExamSessionKey in (1450859,1450833,1451089)
--DELETE FROM [dbo].[FactQuestionResponses] where ExamSessionKey in (1450859,1450833,1451089)