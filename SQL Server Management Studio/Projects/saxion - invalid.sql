select distinct s.Title
	, s.ProjectId
	, AssessmentName
	, ExternalReference
From (
SELECT DISTINCT REPLACE(REPLACE(cast(a.b.query('.') as NVARCHAR(MAX)), '<XPath>@ID="',''), '"</XPath>','') ItemID
	, at.AssessmentName	
	, at.ExternalReference
FROM Saxion_ItemBank..Assessmenttable AT (NOLOCK)
		
CROSS APPLY AssessmentRules.nodes('PaperRules//XPath') a(b)

where isvalid = 0
	and REPLACE(REPLACE(cast(a.b.query('.') as NVARCHAR(MAX)), '<XPath>@ID="',''), '"</XPath>','') not like '<XPath>%'
) a
inner join Saxion_ContentAuthor..Subjects s (NOLOCK)
on s.ProjectId = substring(a.ItemID, 0, charindex('P',ItemId))

order by 1
