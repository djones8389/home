SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

USE AAT_SecureAssess;

--declare @itemsWithDates table (ItemId nvarchar(20), itemVersion int)

--insert into @itemsWithDates
--select ItemID, ItemVersion
--from CPAuditTable
--where ItemXML.exist('P/S/C/I/extension/t/r/c[@dt]') = 1

select cp.ItemID, cp.ItemVersion, replace(replace(convert(nvarchar(max), cp.ItemXML), CHAR(10), ''), CHAR(13), '')
from (	select ItemID, ItemVersion
		from CPAuditTable
		where ItemXML.exist('P/S/C/I/extension/t/r/c[@dt]') = 1
) i
inner join CPAuditTable cp
on cp.ItemID = i.itemId 
	and cp.ItemVersion = i.itemVersion





