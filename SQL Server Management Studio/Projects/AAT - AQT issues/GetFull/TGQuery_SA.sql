if object_id('tempdb..#Items') is not null drop table #Items;

--select [ItemID]
--into #Items
--from [430327-AAT-SQL2\SQL2].[AAT_CPProjectAdmin].[dbo].[Datepicker]

--create clustered index [itemID] on #Items (itemID);

select ItemID, ItemVersion
into #Items
from CPAuditTable
where ItemXML.exist('P/S/C/I/extension/t/r/c[@dt]') = 1

create clustered index [itemID] on #Items (ItemID, ItemVersion);

select wesirt.ID
	, wesirt.WAREHOUSEExamSessionID
	, wesirt.ItemID
	, wesirt.ItemVersion
	, wesirt.ItemResponseData	
from WAREHOUSE_ExamSessionItemResponseTable wesirt
inner join #Items CP
on CP.[ItemID] = wesirt.ItemId
	and cp.itemversion = wesirt.itemversion

--where  Itemresponsedata.exist('p/s/c[@typ=20]') = 1;
--where ItemXML.exist('P/S/C/I/extension/t/r/c[@dt]') = 1