use AAT_SecureAssess

SELECT 
	qualificationName
	, examVersionName
	, examVersionRef
	, KeyCode
	, ItemRef
	, ItemVersion
	, cast((WESTI.userMark*100)/WESTI.TotalMark as float) [MarkAwarded]
	, warehouseTime
FROM WAREHOUSE_ExamSessionTable_ShrededItems WESTI WITH (READUNCOMMITTED)

INNER JOIN WAREHOUSE_ExamSessionTable_Shreded WESTS WITH (READUNCOMMITTED)
on WESTS.examSessionID = WESTI.examSessionID

where ItemRef = '974P1252'
	and previousExamState != 10
order by warehouseTime;
























/*
SELECT ID
	, KeyCode
	, resultData
	, ExportToSecureMarker
FROM WAREHOUSE_ExamSessionTable
where Keycode = 'yttm7ha6'

SELECT *
FROM WAREHOUSE_ExamSessionTable_ShrededItems
where ExamSessionID = 2685600
	and ItemRef = '974P1252'
*/
