SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

declare @StartDate datetime
	,@EndDate datetime
	, @examStates varchar(50) = '6'
	
set @StartDate='2016-02-01 00:00:00'
set @EndDate='2016-02-24 00:00:00'



if OBJECT_ID('tempdb..#esids')is not null drop table #esids;

create table #esids (
	ID int
);	

declare @singleState varchar(50) = (select @examStates);

if(@singleState not like '%,%')

	BEGIN

	declare @myStartDate datetime = (select @StartDate);
	declare @myEndDate datetime = (select @EndDate);	

		declare @myString varchar(max) = '
			insert #esids(ID)
			SELECT ID
			FROM WAREHOUSE_ExamSessionTable 
			where  WarehouseExamState != 1
			and cast(EXAMSTATECHANGEAUDITXML.value(''data(exam/stateChange[newStateID=(@examStates)]/changeDate)[1]'',''datetime'') as DATE)  between ''@StartDate'' and ''@EndDate''

			'

			select @myString  = replace(replace(replace(@myString , '@examStates', @singleState), '@StartDate',@myStartDate), '@EndDate',@myEndDate)



		declare @myString2 varchar(max) = '
			UNION
			SELECT ID
			FROM WAREHOUSE_ExamSessionTable 
			where  WarehouseExamState = 1
			and warehouseTime > cast(GETDATE() as date)
			and cast(EXAMSTATECHANGEAUDITXML.value(''data(exam/stateChange[newStateID=(@examStates)]/changeDate)[1]'',''datetime'') as DATE)  between ''@StartDate'' and ''@EndDate''

			'

			select @myString2  = replace(replace(replace(@myString2 , '@examStates', @singleState), '@StartDate',@myStartDate), '@EndDate',@myEndDate)
			
			exec(@myString + @myString2)
	
	END

ELSE

	BEGIN

		insert #esids(ID)
		SELECT ID
		FROM WAREHOUSE_ExamSessionTable 
		where  WarehouseExamState != 1
			and cast(EXAMSTATECHANGEAUDITXML.value('data(exam/stateChange[newStateID=(@examStates)]/changeDate)[1]','datetime') as DATE)  between @StartDate and @EndDate
		union
		select ID
		from WAREHOUSE_ExamSessionTable
		where WarehouseExamState = 1
			and warehouseTime > cast(GETDATE() as date)
			and cast(EXAMSTATECHANGEAUDITXML.value('data(exam/stateChange[newStateID=(@examStates)]/changeDate)[1]','datetime') as DATE)  between @StartDate and @EndDate;

	END


	select * from #esids