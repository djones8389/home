use master
GO

DECLARE @Dynamic nvarchar(max)='';

select @Dynamic +=CHAR(13) +'kill ' + convert(varchar(5),spid)
from sys.sysprocesses s
where dbid > 4
	and  db_name(dbid) like 'GDPRTEST%'
 
exec(@Dynamic);
GO

RESTORE DATABASE [GDPRTest_AnalyticsManagement]
FROM DISK = N'T:\Backup\Professional.10012018.bak'
WITH FILE = 1
	,NOUNLOAD
	,NOREWIND
	,REPLACE
	,STATS = 10
	,MOVE 'Data' TO N'S:\DATA\GDPRTest_AnalyticsManagement.mdf'
	,MOVE 'Log' TO N'L:\LOGS\GDPRTest_AnalyticsManagement.ldf';
GO

DECLARE @Dynamic nvarchar(max)='';

select @Dynamic +=CHAR(13) +'kill ' + convert(varchar(5),spid)
from sys.sysprocesses s
where dbid > 4
	and  db_name(dbid) like 'GDPRTEST%'
 
exec(@Dynamic);
GO

RESTORE DATABASE [GDPRTest_ContentAuthor]
FROM DISK = N'T:\Backup\Professional.10012018.bak'
WITH FILE = 2
	,NOUNLOAD
	,NOREWIND
	,REPLACE
	,STATS = 10
	,MOVE 'Data' TO N'S:\DATA\GDPRTest_ContentAuthor.mdf'
	,MOVE 'Log' TO N'L:\LOGS\GDPRTest_ContentAuthor.ldf';
GO

DECLARE @Dynamic nvarchar(max)='';

select @Dynamic +=CHAR(13) +'kill ' + convert(varchar(5),spid)
from sys.sysprocesses s
where dbid > 4
	and  db_name(dbid) like 'GDPRTEST%'
 
exec(@Dynamic);
GO

RESTORE DATABASE [GDPRTest_ItemBank]
FROM DISK = N'T:\Backup\Professional.10012018.bak'
WITH FILE = 3
	,NOUNLOAD
	,NOREWIND
	,REPLACE
	,STATS = 10
	,MOVE 'Data' TO N'S:\DATA\GDPRTest_ItemBank.mdf'
	,MOVE 'Log' TO N'L:\LOGS\GDPRTest_ItemBank.ldf';
GO

DECLARE @Dynamic nvarchar(max)='';

select @Dynamic +=CHAR(13) +'kill ' + convert(varchar(5),spid)
from sys.sysprocesses s
where dbid > 4
	and  db_name(dbid) like 'GDPRTEST%'
	and spid <> @@SPID

exec(@Dynamic);
GO

RESTORE DATABASE [GDPRTest_SecureAssess]
FROM DISK = N'T:\Backup\Professional.10012018.bak'
WITH FILE = 4
	,NOUNLOAD
	,NOREWIND
	,REPLACE
	,STATS = 10
	,MOVE 'Data' TO N'S:\DATA\GDPRTest_SecureAssess.mdf'
	,MOVE 'Log' TO N'L:\LOGS\GDPRTest_SecureAssess.ldf';
GO

DECLARE @Dynamic nvarchar(max)='';

select @Dynamic +=CHAR(13) +'kill ' + convert(varchar(5),spid)
from sys.sysprocesses s
where dbid > 4
	and  db_name(dbid) like 'GDPRTEST%'
 
exec(@Dynamic);
GO

RESTORE DATABASE [GDPRTest_SecureMarker]
FROM DISK = N'T:\Backup\Professional.10012018.bak'
WITH FILE = 5
	,NOUNLOAD
	,NOREWIND
	,REPLACE
	,STATS = 10
	,MOVE 'Data' TO N'S:\DATA\GDPRTest_SecureMarker.mdf'
	,MOVE 'Log' TO N'L:\LOGS\GDPRTest_SecureMarker.ldf';
GO

DECLARE @Dynamic nvarchar(max)='';

select @Dynamic +=CHAR(13) +'kill ' + convert(varchar(5),spid)
from sys.sysprocesses s
where dbid > 4
	and  db_name(dbid) like 'GDPRTEST%'
 
exec(@Dynamic);
GO

RESTORE DATABASE [GDPRTest_SurpassDataWarehouse]
FROM DISK = N'T:\Backup\Professional.10012018.bak'
WITH FILE = 6
	,NOUNLOAD
	,NOREWIND
	,REPLACE
	,STATS = 10
	,MOVE 'Data' TO N'S:\DATA\GDPRTest_SurpassDataWarehouse.mdf'
	,MOVE 'Log' TO N'L:\LOGS\GDPRTest_SurpassDataWarehouse.ldf';
GO

DECLARE @Dynamic nvarchar(max)='';

select @Dynamic +=CHAR(13) +'kill ' + convert(varchar(5),spid)
from sys.sysprocesses s
where dbid > 4
	and  db_name(dbid) like 'GDPRTEST%'
 
exec(@Dynamic);
GO

RESTORE DATABASE [GDPRTest_SurpassManagement]
FROM DISK = N'T:\Backup\Professional.10012018.bak'
WITH FILE = 7
	,NOUNLOAD
	,NOREWIND
	,REPLACE
	,STATS = 10
	,MOVE 'Data' TO N'S:\DATA\GDPRTest_SurpassManagement.mdf'
	,MOVE 'Log' TO N'L:\LOGS\GDPRTest_SurpassManagement.ldf';

GO

DECLARE @Dynamic nvarchar(max)='';

select @Dynamic +=CHAR(13) +'kill ' + convert(varchar(5),spid)
from sys.sysprocesses s
where dbid > 4
	and  db_name(dbid) like 'GDPRTEST%'
 
exec(@Dynamic);
GO



--DECLARE @Orphaned TABLE (
--	DBName nvarchar(200)
--	, userName nvarchar(100)
--)

--INSERT @Orphaned
--exec sp_MSforeachdb '

--use [?];

--if (''?'' like ''GDPRTest%'')
--BEGIN
--select DB_NAME()
--	,name
--from sysusers
--    where issqluser = 1 
--    and   (sid is not null and sid <> 0x0)
--    --and   (len(sid) <= 16)
--    and   suser_sname(sid) is null
--	and name != ''dbo''
--    order by name
--END
--'

--DECLARE @DynamicDrop nvarchar(MAX)=''

--SELECT @DynamicDrop+=CHAR(13) +'USE '  + QUOTENAME(DBNAME) + 'DROP USER ' + QUOTENAME(userName) + ';'
--FROM @Orphaned 

--EXEC(@DynamicDrop);


--GO

USE [GDPRTest_SecureAssess]; IF NOT EXISTS (SELECT 1 FROM sys.database_principals where name = 'cms_user') CREATE USER [cms_user]FOR LOGIN [cms_login]; exec sp_addrolemember'db_owner', 'cms_user' 
USE [GDPRTest_SecureAssess]; IF NOT EXISTS (SELECT 1 FROM sys.database_principals where name = 'GDPRTest_SecureAssessUser') CREATE USER [GDPRTest_SecureAssessUser]FOR LOGIN [GDPRTest_SecureAssessUser]; exec sp_addrolemember'db_owner', 'GDPRTest_SecureAssessUser' 
USE [GDPRTest_SecureAssess]; IF NOT EXISTS (SELECT 1 FROM sys.database_principals where name = 'SecureAssessUser') CREATE USER [SecureAssessUser]FOR LOGIN [SecureAssessUser]; exec sp_addrolemember'db_owner', 'SecureAssessUser' 
USE [GDPRTest_SecureAssess]; IF NOT EXISTS (SELECT 1 FROM sys.database_principals where name = 'IntegrationUser') CREATE USER [IntegrationUser]FOR LOGIN [IntegrationUser]; exec sp_addrolemember'db_owner', 'IntegrationUser' 
USE [GDPRTest_SecureAssess]; IF NOT EXISTS (SELECT 1 FROM sys.database_principals where name = 'GDPRTest_ETLUser') CREATE USER [GDPRTest_ETLUser]FOR LOGIN [GDPRTest_ETLUser]; exec sp_addrolemember'db_datareader', 'GDPRTest_ETLUser' 
USE [GDPRTest_ItemBank]; IF NOT EXISTS (SELECT 1 FROM sys.database_principals where name = 'cms_user') CREATE USER [cms_user]FOR LOGIN [cms_login]; exec sp_addrolemember'db_owner', 'cms_user' 
USE [GDPRTest_ItemBank]; IF NOT EXISTS (SELECT 1 FROM sys.database_principals where name = 'GDPRTest_ItemBankUser') CREATE USER [GDPRTest_ItemBankUser]FOR LOGIN [GDPRTest_ItemBankUser]; exec sp_addrolemember'db_owner', 'GDPRTest_ItemBankUser' 
USE [GDPRTest_ItemBank]; IF NOT EXISTS (SELECT 1 FROM sys.database_principals where name = 'ItemBank') CREATE USER [ItemBank]FOR LOGIN [ItemBank]; exec sp_addrolemember'db_owner', 'ItemBank' 
USE [GDPRTest_ItemBank]; IF NOT EXISTS (SELECT 1 FROM sys.database_principals where name = 'GDPRTest_ETLUser') CREATE USER [GDPRTest_ETLUser]FOR LOGIN [GDPRTest_ETLUser]; exec sp_addrolemember'ETLRole', 'GDPRTest_ETLUser' 
USE [GDPRTest_ContentAuthor]; IF NOT EXISTS (SELECT 1 FROM sys.database_principals where name = 'cms_user') CREATE USER [cms_user]FOR LOGIN [cms_login]; exec sp_addrolemember'db_owner', 'cms_user' 
USE [GDPRTest_ContentAuthor]; IF NOT EXISTS (SELECT 1 FROM sys.database_principals where name = 'GDPRTest_ContentAuthorUser') CREATE USER [GDPRTest_ContentAuthorUser]FOR LOGIN [GDPRTest_ContentAuthorUser]; exec sp_addrolemember'db_owner', 'GDPRTest_ContentAuthorUser' 
USE [GDPRTest_ContentAuthor]; IF NOT EXISTS (SELECT 1 FROM sys.database_principals where name = 'GDPRTest_ETLUser') CREATE USER [GDPRTest_ETLUser]FOR LOGIN [GDPRTest_ETLUser]; exec sp_addrolemember'db_datareader', 'GDPRTest_ETLUser' 
USE [GDPRTest_SurpassManagement]; IF NOT EXISTS (SELECT 1 FROM sys.database_principals where name = 'cms_user') CREATE USER [cms_user]FOR LOGIN [cms_login]; exec sp_addrolemember'db_owner', 'cms_user' 
USE [GDPRTest_SurpassManagement]; IF NOT EXISTS (SELECT 1 FROM sys.database_principals where name = 'GDPRTest_SurpassManagementUser') CREATE USER [GDPRTest_SurpassManagementUser]FOR LOGIN [GDPRTest_SurpassManagementUser]; exec sp_addrolemember'db_owner', 'GDPRTest_SurpassManagementUser' 
USE [GDPRTest_SurpassManagement]; IF NOT EXISTS (SELECT 1 FROM sys.database_principals where name = 'GDPRTest_ETLUser') CREATE USER [GDPRTest_ETLUser]FOR LOGIN [GDPRTest_ETLUser]; exec sp_addrolemember'db_datareader', 'GDPRTest_ETLUser' 
USE [GDPRTest_AnalyticsManagement]; IF NOT EXISTS (SELECT 1 FROM sys.database_principals where name = 'cms_user') CREATE USER [cms_user]FOR LOGIN [cms_login]; exec sp_addrolemember'db_owner', 'cms_user' 
USE [GDPRTest_AnalyticsManagement]; IF NOT EXISTS (SELECT 1 FROM sys.database_principals where name = 'GDPRTest_AnalyticsManagementUser') CREATE USER [GDPRTest_AnalyticsManagementUser]FOR LOGIN [GDPRTest_AnalyticsManagementUser]; exec sp_addrolemember'db_owner', 'GDPRTest_AnalyticsManagementUser' 
USE [GDPRTest_SurpassDataWarehouse]; IF NOT EXISTS (SELECT 1 FROM sys.database_principals where name = 'cms_user') CREATE USER [cms_user]FOR LOGIN [cms_login]; exec sp_addrolemember'db_owner', 'cms_user' 
USE [GDPRTest_SurpassDataWarehouse]; IF NOT EXISTS (SELECT 1 FROM sys.database_principals where name = 'GDPRTest_ETLUser') CREATE USER [GDPRTest_ETLUser]FOR LOGIN [GDPRTest_ETLUser]; exec sp_addrolemember'db_owner', 'GDPRTest_ETLUser' 
USE [GDPRTest_SecureMarker]; IF NOT EXISTS (SELECT 1 FROM sys.database_principals where name = 'cms_user') CREATE USER [cms_user]FOR LOGIN [cms_login]; exec sp_addrolemember'db_owner', 'cms_user' 
USE [GDPRTest_SecureMarker]; IF NOT EXISTS (SELECT 1 FROM sys.database_principals where name = 'GDPRTest_SecureMarkerUser') CREATE USER [GDPRTest_SecureMarkerUser]FOR LOGIN [GDPRTest_SecureMarkerUser]; exec sp_addrolemember'db_owner', 'GDPRTest_SecureMarkerUser' 

GO



USE [GDPRTest_SecureAssess];  DROP USER [IntegrationUser];
USE [GDPRTest_ItemBank];  DROP USER [ItemBank];
USE [GDPRTest_AnalyticsManagement];  DROP USER [Professional_AnalyticsManagementUser];
USE [GDPRTest_ContentAuthor];  DROP USER [Professional_ContentAuthorUser];
USE [GDPRTest_ContentAuthor];  DROP USER [Professional_ETLUser];
USE [GDPRTest_SurpassManagement];  DROP USER [Professional_ETLUser];
USE [GDPRTest_SurpassDataWarehouse];  DROP USER [Professional_ETLUser];
USE [GDPRTest_ItemBank];  DROP USER [Professional_ETLUser];
--USE [GDPRTest_ItemBank];  DROP USER [Professional_ETLUser];
USE [GDPRTest_SecureAssess];  DROP USER [Professional_ETLUser];
--USE [GDPRTest_ItemBank];  DROP USER [Professional_ETLUser];
USE [GDPRTest_ItemBank];  DROP USER [Professional_ItemBankUser];
USE [GDPRTest_SecureAssess];  DROP USER [Professional_SecureAssessUser];
USE [GDPRTest_SecureMarker];  DROP USER [Professional_SecureMarkerUser];
USE [GDPRTest_SurpassManagement];  DROP USER [Professional_SurpassManagementUser];
USE [GDPRTest_SecureAssess];  DROP USER [SecureAssessUser];

GO