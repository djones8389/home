--DROP TABLE ##ExamsToVoid

CREATE TABLE ##ExamsToVoid (
	 ID INT
	, Thread tinyint NULL
);

INSERT ##ExamsToVoid
SELECT EST.ID, NULL
FROM ExamSessionTable as EST (NOLOCK)
 
  Inner join ExamStateChangeAuditTable as ESCAT (NOLOCK)
  on ESCAT.ExamSessionID = est.ID
	and NewState = examState
     
where examState = 18
	and StateChangeDate < DATEADD(Month, -6, GETDATE())
		ORDER BY EST.ID;
		
		
CREATE INDEX [IX_Temp] ON [##ExamsToVoid] (
	 ID
	, Thread
)

select id, examstate , attemptAutoSubmitForAwaitingUpload, AwaitingUploadStateChangingTime,AwaitingUploadGracePeriodInDays
from examsessiontable 
where id in (
	select ID 
	FROM ##ExamsToVoid 
	--where thread = 1
)

--UPDATE examsessiontable 
--set attemptAutoSubmitForAwaitingUpload = 0
--where id in (
--	select ID 
--	FROM ##ExamsToVoid 
--	where thread = 1
--)





UPDATE TOP (50) ##ExamsToVoid
set thread = 1
where thread IS NULL;

UPDATE TOP (50) EXAMSESSIONTABLE
set attemptAutoSubmitForAwaitingUpload = 0
FROM EXAMSESSIONTABLE A 
INNER JOIN ##ExamsToVoid B
ON A.ID = B.ID
where thread IS NOT NULL
	and examState = 18;




select count(id), examstate
from examsessiontable (NOLOCK)
group by examstate
order by 2









DECLARE @rows INT
DECLARE @ids TABLE (id INT)

BEGIN TRANSACTION

        UPDATE TOP (50) ##ExamsToVoid
        SET thread = 1
        OUTPUT deleted.id
        INTO @ids
        WHERE thread IS NULL

        SET @rows = @@ROWCOUNT

COMMIT TRANSACTION


WHILE @rows > 0
BEGIN
        BEGIN TRANSACTION
    
		UPDATE ExamSessionTable
		SET attemptAutoSubmitForAwaitingUpload = 0
		WHERE ID IN (
						SELECT ID
						FROM @ids
						)	
						
	   COMMIT TRANSACTION

        BEGIN TRANSACTION

            UPDATE TOP (50) ##ExamsToVoid
            SET thread = 1
            OUTPUT deleted.id
            INTO @ids
            WHERE thread IS NULL

            SET @rows = @@ROWCOUNT

        COMMIT TRANSACTION
END


