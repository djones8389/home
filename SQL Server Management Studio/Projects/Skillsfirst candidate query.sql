use PRV_SkillsFirst_SecureAssess

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

if OBJECT_ID ('tempdb..#CSV_Identity')is not null drop table #CSV_Identity

CREATE TABLE #CSV_Identity (
    CentreName NVARCHAR(100)
	, CandidateRef NVARCHAR(100)
	, ForeName NVARCHAR(50)
	, SurName NVARCHAR(50)
	, QualRef NVARCHAR(50)
	)
	 

--Insert data	
BULK INSERT #CSV_Identity
FROM 'D:\skillsfirst.csv' WITH (
		FIELDTERMINATOR = ','
		, ROWTERMINATOR = '\n'
		)
GO

--Remove duplicates
DELETE A
FROM (
select *
	,ROW_NUMBER() OVER (
			PARTITION BY CandidateRef
			, QualRef ORDER BY CandidateRef
			) R 
from #CSV_Identity 
) A
where r > 1


SELECT CSV.CentreName
	, CSV.CandidateRef
	, CSV.ForeName
	, CSV.SurName
	, CSV.QualRef
	, exams.accountcreationdate
	, exams.warehousetime
	, case when Exams.candidateref is null  then 0 else 1 end as [SatExam?]
from #CSV_Identity CSV

LEFT JOIN  (

select centrename, candidateref, forename, surname, SUBSTRING(ib.qualificationRef,0,CHARINDEX('_',ib.qualificationRef)) qualificationRef, ut.accountcreationdate, null warehousetime
from ScheduledExamsTable scet 
LEFT join examsessiontable est on est.Scheduledexamid = scet.id
LEFT join centretable ct on ct.id = scet.centreid
LEFT join UserTable ut on ut.id = est.userid
LEFT join IB3QualificationLookup ib on ib.id = scet.qualificationId
inner join examstatechangeaudittable escat on escat.examsessionid = est.id
	and newstate = 6

UNION

SELECT centrename, wests.candidateref, wests.forename, wests.surname, SUBSTRING(wscet.qualificationRef,0,CHARINDEX('_',wscet.qualificationRef)) qualificationRef, wut.accountcreationdate, wests.warehousetime
from warehouse_examsessiontable_shreded wests
LEFT join warehouse_examsessiontable west on west.id = wests.examsessionid
LEFT join WAREHOUSE_ScheduledExamsTable wscet on wscet.id = west.WAREHOUSEScheduledExamID
LEFT join warehouse_usertable wut on wut.id = WEST.WAREHOUSEUserID
where  ExamStateChangeAuditXml.exist('exam/stateChange[newStateID="6"]') = 1
	--and ExamStateChangeAuditXml.value('data(exam/stateChange[newStateID=6]/changeDate)[1]','datetime') between '2015-01-01 00:00:01' and '2015-12-31 23:59:59'

) Exams

	on exams.centrename = csv.centrename
		and exams.candidateref = csv.candidateref
		and exams.forename = csv.forename
		and exams.surname = csv.surname
		and exams.qualificationRef = csv.QualRef

		where csv.candidateref = '1079746'

		select accountcreationdate
		from usertable
		where candidateref = '1079746'