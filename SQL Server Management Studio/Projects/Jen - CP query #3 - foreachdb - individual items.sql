use master

if object_id('tempdb..##Global') is not null drop table ##Global;

CREATE TABLE ##Global (

	[Client] sysname
	, subjectID int
	, [SubjectName] nvarchar(200)
	, itemID int
	, [Type] nvarchar(200)
	, [% of Items] nvarchar(20)
	, TotalCountOfLiveItems int
	, [RowCountOfSpecialItems] int
)

exec sp_msforeachdb '

use [?];

if(''?'' like ''%_ContentAuthor%'' and ''?'' <> ''Nbme_ContentAuthor'')

BEGIN

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	if object_id(''tempdb..#ItemTable'') is not null drop table #ItemTable;

	;with LiveItems as (
	select i.id ItemID, s.id [SubjectID], s.title [SubjectName]
	from items I
	inner join WorkflowStatuses w on i.WorkFlowStatusID = w.id
	inner join Subjects s on s.id = i.subjectid
	where isPublishable = 1	and isDeleted = 1
	) 

	SELECT A.*
	INTO #ItemTable
	FROM (
		SELECT [SubjectID]
			, [SubjectName]
			, SpecialItems.id
			, [Type]
			, (SELECT COUNT(1) FROM LiveItems) TotalCount
		FROM LiveItems
		INNER JOIN (
			select id, ''MatchingBoxesItems'' [Type] from [dbo].[MatchingBoxesItems] union 
			select id, ''SelectFromAListItems'' [Type] from [dbo].[SelectFromAListItems] union 
			select id, ''FillInTheBlankItems'' [Type] from [dbo].[FillInTheBlankItems] union 
			select id, ''DragAndDropItems'' [Type] from [dbo].[DragAndDropItems] union
			select id, ''EquationEntryItems'' [Type] from [dbo].[EquationEntryItems] union
			select id, ''HotspotItems'' [Type] from [dbo].[HotspotItems] union
			select id, ''VoiceCaptureItems'' [Type] from [dbo].[VoiceCaptureItems] 
		) SpecialItems
		ON LiveItems.ItemID = SpecialItems.ID
	) A 

	INSERT ##Global
	SELECT db_name() [Client]
		, B.[SubjectID]
		, B.[SubjectName]
		, B.[ItemID]
		, B.[type]
		, cast(cast((cast([RowCount] as float)/cast(TotalCount as float))*100 as decimal(8,2)) as varchar(10)) + '' %'' [% of Items]
		, TotalCount
		, [RowCount]
	FROM (
		SELECT [SubjectID]
			, [SubjectName]
			, ID [ItemID]
			, [type]
			, TotalCount
			, (SELECT @@ROWCOUNT) [RowCount]
		FROM #ItemTable IT
	) B
END

'
SELECT SUBSTRING(Client, 0, CHARINDEX('_',Client)) Client
	,*
FROM ##Global
order by 1;