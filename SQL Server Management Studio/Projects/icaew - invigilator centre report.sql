select distinct
	u.UserName
	, c.name [CentreName]

from users u
inner join UserCentreSubjectRoles usc
on usc.userid = u.id
inner join Roles r
on r.id = usc.RoleId
inner join RolePermissions rp
on rp.RoleId = r.Id
inner join [Permissions] p
on p.Id = rp.PermissionId
inner join Centres c
on c.id = usc.centreid
where p.name = 'Invigilate'
order by username;