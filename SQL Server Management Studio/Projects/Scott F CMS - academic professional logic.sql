SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
			
Declare @StartDate DATE = '01 Sep 2012' --(select [startDate] from ##ReportParams)
Declare @EndDate DATE = '01 Sep 2013' --(select [endDate] from ##ReportParams)
Declare @Licence tinyint = 2

if OBJECT_ID ('tempdb..##Live') IS NOT NULL DROP TABLE ##Live;
if OBJECT_ID ('tempdb..##Warehouse') IS NOT NULL DROP TABLE ##Warehouse;

CREATE TABLE ##Live (ExamSessionID int, userID int);
	INSERT ##Live (ExamSessionID, UserID)
	SELECT ExamSessionTable.ID ExamSessionID
		, ExamSessionTable.UserID
	FROM  (
	select ExamSessionID
	from ExamStateChangeAuditTable
	where ExamStateChangeAuditTable.NewState = 9
		and StateChangeDate between @StartDate and @EndDate 
		group by ExamSessionID
	) State9
	INNER JOIN ExamSessionTable
	on ExamSessionTable.ID = State9.ExamSessionID
	where exists (	
	select 1
	from ExamStateChangeAuditTable
	where ExamStateChangeAuditTable.ExamSessionID = State9.ExamSessionID
	and ExamStateChangeAuditTable.NewState = 6
	);

CREATE TABLE ##Warehouse  (ExamSessionID int, userID int);
	INSERT ##Warehouse (ExamSessionID, UserID)
	SELECT WAREHOUSE_ExamSessionTable.ExamSessionID
		, WAREHOUSE_ExamSessionTable_Shreded.userId
	from WAREHOUSE_ExamSessionTable_Shreded
	inner join WAREHOUSE_ExamSessionTable
	on WAREHOUSE_ExamSessionTable.ID = WAREHOUSE_ExamSessionTable_Shreded.examSessionId
	where [started] > @StartDate	
		and submittedDate < @EndDate;


DECLARE @SQL1 NVARCHAR(MAX) = ''
SELECT @SQL1 = '
;WITH Academic AS (
	select userId from ##Live
	UNION
	select userId from ##Warehouse
)
	select db_name() [Client]
		, count(userId) [ExamCount]
	from Academic
'
DECLARE @SQL2 NVARCHAR(MAX) = ''
SELECT @SQL2 = '
	;With Professional as (
	select ExamSessionID from ##Live
	UNION
	select ExamSessionID from ##Warehouse
	)

	select db_name() [Client]
		, count(ExamSessionID) [ExamCount]
	from Professional
'
if(@Licence=1)
	exec(@SQL1)
if(@Licence=2)
	exec(@SQL2)






--Professional AS (
--	SELECT ExamSessionID FROM LIVE
--	UNION
--	SELECT ExamSessionID FROM Warehouse
--),



/*
		select *
			from UserTable
			where ID = 58

			select *
			from WAREHOUSE_ExamSessionTable_Shreded
			where UserId = 58

*/


/*
The logic says �when Academic then distinct exams by candidate� �e.g. (Candidate sits 10 exams, this counts as 1)
               �when Professional then distinct exams� -e.g. (Candidate sits 10 exams, this counts as 10)

*/
