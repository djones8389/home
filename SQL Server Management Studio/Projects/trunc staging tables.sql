--USE [Ocrdry_SurpassDataWarehouse]
--GO

--/****** Object:  Index [PK_stg_FactExamSessions]    Script Date: 21/07/2017 08:43:57 ******/
--ALTER TABLE [ETL].[stg_FactExamSessions] ADD  CONSTRAINT [PK_stg_FactExamSessions] PRIMARY KEY CLUSTERED 
--(
--	[ExamSessionKey] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
--GO


--SELECT * FROM  [ETL].[stg_FactExamSessions] WHERE ISNULL(UseForMerge,1) = 1

/*
SELECT 'TRUNCATE TABLE ' + QUOTENAME(SCHEMA_NAME(schema_id)) + '.' + QUOTENAME(NAME) + ';'
FROM sys.tables
ORDER BY NAME

TRUNCATE TABLE [ETL].[stg_CandidateTagValues];
TRUNCATE TABLE [ETL].[stg_DimCentres];
TRUNCATE TABLE [ETL].[stg_DimCombinationOptions];
TRUNCATE TABLE [ETL].[stg_DimCombinations];
TRUNCATE TABLE [ETL].[stg_DimComponents];
TRUNCATE TABLE [ETL].[stg_DimContentUsers];
TRUNCATE TABLE [ETL].[stg_DimContentUsersRoles];
TRUNCATE TABLE [ETL].[stg_DimEnemyQuestions];
TRUNCATE TABLE [ETL].[stg_DimExamVersionVersions];
TRUNCATE TABLE [ETL].[stg_DimMediaItems];
TRUNCATE TABLE [ETL].[stg_DimNumericalEntryAnswers];
TRUNCATE TABLE [ETL].[stg_DimOptions];
TRUNCATE TABLE [ETL].[stg_DimPageMetadata];
TRUNCATE TABLE [ETL].[stg_DimProjectGroups];
TRUNCATE TABLE [ETL].[stg_DimProjects];
TRUNCATE TABLE [ETL].[stg_DimQualifications];
TRUNCATE TABLE [ETL].[stg_DimQuestionMetadata];
TRUNCATE TABLE [ETL].[stg_DimQuestions];
TRUNCATE TABLE [ETL].[stg_DimQuestions_position];
TRUNCATE TABLE [ETL].[stg_DimQuestions_tmp];
TRUNCATE TABLE [ETL].[stg_DimQuestionStems];
TRUNCATE TABLE [ETL].[stg_DimScenes];
TRUNCATE TABLE [ETL].[stg_DimSections];
TRUNCATE TABLE [ETL].[stg_DimSectionSelectors];
TRUNCATE TABLE [ETL].[stg_ExamSessionXML];
TRUNCATE TABLE [ETL].[stg_FactClientInformation];
TRUNCATE TABLE [ETL].[stg_FactComponentResponses];
TRUNCATE TABLE [ETL].[stg_FactExamSessions];
TRUNCATE TABLE [ETL].[stg_FactExamSessionsStateAudit];
TRUNCATE TABLE [ETL].[stg_FactMarkerResponses];
TRUNCATE TABLE [ETL].[stg_FactOptionResponses];
TRUNCATE TABLE [ETL].[stg_FactQuestionResponses];
TRUNCATE TABLE [ETL].[stg_FactTestPackageMarks];
TRUNCATE TABLE [ETL].[stg_LearningOutcomeResults];
TRUNCATE TABLE [ETL].[stg_LearningOutcomes];
TRUNCATE TABLE [ETL].[stg_ScaleScoreItems];
TRUNCATE TABLE [ETL].[stg_ScaleScores];
TRUNCATE TABLE [ETL].[stg_TagTypes];
TRUNCATE TABLE [ETL].[stg_TagValues];
*/

sp_whoisactive