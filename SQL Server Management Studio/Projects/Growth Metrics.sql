SELECT [server_name]
      ,[database_name]
      ,[table_name]
      ,substring(rtrim([reserved_kb]), 0, CHARINDEX('KB',rtrim([reserved_kb]))) [reserved_kb]
	  ,substring(rtrim([data_kb]), 0, CHARINDEX('KB',rtrim([data_kb]))) [data_kb]
	  ,substring(rtrim([index_size]), 0, CHARINDEX('KB',rtrim([index_size]))) [index_size]
	  ,substring(rtrim([unused_kb]), 0, CHARINDEX('KB',rtrim([unused_kb]))) [unused_kb]
      ,cast([CollectionDate] as date) [date]
  FROM [PSCollector].[dbo].[DBGrowthMetrics]

  where table_name != '_MigrationScripts'
	and database_name = 'AAT_SecureAssess'
  group by [server_name]
      ,[database_name]
      ,[table_name]
      ,[rows]
	  ,[reserved_kb]
	  ,[data_kb]
	  ,[index_size]
	  ,[unused_kb]
	  ,cast([CollectionDate] as date)
