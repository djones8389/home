SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

SELECT 
	at.assessmentname,
	CASE ScoreBoundaries.value('data(ScoreBoundaries/@showScoreBoundaryColumn)[1]','bit')
		WHEN '0'
			THEN 'Show ''Percentage '' Column'
		ELSE 'Show ''Result'' Column'
		END AS [Candidate Score Boundaries]
	, [Higher Marking Item Boundaries]
	, [Low Marking Item Boundaries]	
	,RequiresSecureClient [Requires SecureClient]
	,CASE convert(NVARCHAR(20), SecureClientOperationMode)
		WHEN '0'
			THEN 'Unlocked'
		WHEN '1'
			THEN 'Lockdown'
		ELSE convert(NVARCHAR(20), SecureClientOperationMode)
		END AS [SecureClient Mode:]
	/*Test Availability*/
	,[AdvanceDownload] AS 'AdvanceDownload'
	,[DurationMode] AS 'DurationMode'
	,CONVERT(varchar(11), ValidFromDate, 106)  AS 'Test Date Window: Valid From:'    
	,CONVERT(varchar(11), agt.ExpiryDate, 106)   AS 'Test Date Window: Expires:'    
	,CONVERT(varchar(8), StartTime, 114)   AS 'Daily Test Window: Start Time'    
	,CONVERT(varchar(8), EndTime, 114)    AS 'Daily Test Window: End Time'    
	,agt.RequiresVersionAvailability
	,AdvanceContentDownloadTimespan [Advance content download timespan (hours)]
	, case when AnnotationVersion = '0' then '0' else '1' end as 'Annotation Used/Altered?'
	,ISNULL(CONVERT(varchar(11), AT.ValidFrom , 106),'')  AS 'Test Version Date  Valid From:'          
	,ISNULL(CONVERT(varchar(11), AT.ExpiryDate, 106),'')   AS 'Test Version Date  Expires:'    
FROM AssessmentTable AT
INNER JOIN AssessmentGroupTable AGT ON AGT.id = AT.AssessmentGroupID
INNER JOIN AssessmentStatusLookupTable ASLT ON ASLT.id = AT.AssessmentStatus
INNER JOIN (

SELECT  
	X.ID
	,x.[Higher Marking Item Boundaries]	
	,x.[Low Marking Item Boundaries]
FROM (

select ID
	 ,(
		select	
			f.g.value('@description','nvarchar(20)') 
			+ ' '
		    + f.g.value('@value','nvarchar(20)')  
		    + '%' 
			+  case f.g.value('@modifier','nvarchar(20)') 
				when 'lt' then ' Less than ' 
				when 'gt' then ' or higher ' 
			end
		from AssessmentGroupTable Y
		cross apply ScoreBoundaries.nodes('ScoreBoundaries/score') f(g)
		where y.id = z.id
			and f.g.value('@higherBoundarySet','bit') = '1'
		for xml path(''), type
			) as [Higher Marking Item Boundaries]
	 ,(
		select	
			f.g.value('@description','nvarchar(20)') 
			+ ' '
		    + f.g.value('@value','nvarchar(20)')  
		    + '%' 
			+  case f.g.value('@modifier','nvarchar(20)') 
				when 'lt' then ' Less than ' 
				when 'gt' then ' or higher ' 
			end
		from AssessmentGroupTable V
		cross apply ScoreBoundaries.nodes('ScoreBoundaries/score') f(g)
		where V.id = z.id
			and f.g.value('@higherBoundarySet','bit') = '0'
		for xml path(''), type
			) as [Low Marking Item Boundaries]
from AssessmentGroupTable Z

) X		

) ABT

on aBt.ID = agt.ID
WHERE ExternalReference NOT LIKE '%BTL%' 	AND AT.AssessmentName  NOT LIKE '%BTL%'