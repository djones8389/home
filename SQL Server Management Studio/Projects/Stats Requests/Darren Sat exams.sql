USE master;

IF OBJECT_ID('tempDB..##DistintCands') IS NOT NULL DROP TABLE ##DistintCands;

CREATE TABLE ##DistintCands (
	
	ServerName nvarchar(255)
	, DBName nvarchar(255)
	, forename nvarchar(255)
	, surname nvarchar(255)
	, candidateref nvarchar(255)
	, dob smalldatetime
	, centreName nvarchar(255)
	, qualificationName nvarchar(255)
	, examName nvarchar(255)
)

INSERT ##DistintCands
exec sp_MSforeachdb '

use [?];

SET QUOTED_IDENTIFIER ON;

if(DB_Name() like ''%SecureAssess%'' OR DB_Name() like ''%OpenAssess%'')

BEGIN

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

SELECT
	@@SERVERNAME ServerName
	, db_Name()  DBName
	, forename
	, surname
	, candidateref
	, dob
	, centreName
	, qualificationName
	, examName
FROM WAREHOUSE_UserTable
INNER JOIN (
		select 
			ID
			, WAREHOUSE_ExamSessionTable.WAREHOUSEUserID
			, centreName
			, qualificationName
			, examName
		from WAREHOUSE_ExamSessionTable 
		INNER JOIN WAREHOUSE_ExamSessionTable_Shreded
		ON WAREHOUSE_ExamSessionTable_Shreded.examSessionID = WAREHOUSE_ExamSessionTable.ID
		where ExamStateChangeAuditXml.exist(''exam/stateChange[newStateID=6]'') = 1
	) SatExams
	on SatExams.WAREHOUSEUserID = WAREHOUSE_UserTable.ID;

END
'