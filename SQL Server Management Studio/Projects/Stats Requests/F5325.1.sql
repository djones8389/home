--/****** Script for SelectTopNRows command from SSMS  ******/
--SELECT *
--  FROM [ExamTypes] ET
  
--LEFT JOIN ExamGradeDescriptions EGD
--on ET.ExamTypeId = EGD.ExamGradeId
--	and  ET.ExamTypeId = EGD.ExamTypeId
--INNER JOIN PackageGrades PG
--ON PG.ExamGradeId = ET.ExamTypeId
--	AND PG.ExamGradeId  = EGD.ExamGradeId

--SELECT * FROM PackageGrades



--INNER JOIN [dbo].[PackageGrades] PG 
--	ON PG.PackageId = P.PackageId
----	AND PE.ExamTypeId = PG.ExamGradeId
--LEFT JOIN  [dbo].[ExamGrades] EG 
--	ON EG.Id = PG.ExamGradeId

--INNER JOIN ExamGradeDescriptions EGD
--on EGD.ExamGradeId = EG.Id
--INNER JOIN ExamTypes ET
--on ET.ExamTypeId = EGD.ExamTypeId

SELECT 
	pe.Name
	, Spc.FirstName
	, spc.LastName
	, SPC.SurpassCandidateRef
	, P.NAME
	, ScheduledExamRef
	, eg.Code
FROM ScheduledPackageCandidateExams AS SPCE
INNER JOIN ScheduledPackageCandidates AS SPC ON SPC.ScheduledPackageCandidateId = SPCE.Candidate_ScheduledPackageCandidateId
INNER JOIN ScheduledPackages AS SP ON SP.ScheduledPackageId = SPC.ScheduledPackage_ScheduledPackageId
INNER JOIN [dbo].[PackageExams] pe ON pe.[PackageExamId] = SPCE.[PackageExamId]
INNER JOIN [dbo].[Packages] P ON P.[PackageId] = pe.[PackageId]
INNER JOIN ExamTypes ET ON ET.ExamTypeId = PE.ExamTypeId
LEFT JOIN ExamGradeDescriptions EGD on ET.ExamTypeId = EGD.ExamTypeId
	AND ET.ExamTypeId = EGD.ExamGradeId
LEFT JOIN ExamGrades EG
on EG.Id = EGD.ExamGradeId
WHERE P.name = 'Kerala - Four Skills Package'
	And SurpassCandidateRef = 'FP_00009932A'