SELECT 
	pe.Name
	, Spc.FirstName
	, spc.LastName
	, SPC.SurpassCandidateRef
	, P.NAME
	--, EG.Code
	, ScheduledExamRef
	--,pack.[SurpassQualificationId]
	--, pg.*
	 ,p.*
	--,SP.DateCreated
	--,SP.PackageDate
	--,SP.StartDate
	--,SP.StatusValue
	--,ScheduledExamRef
FROM ScheduledPackageCandidateExams AS SPCE
INNER JOIN ScheduledPackageCandidates AS SPC ON SPC.ScheduledPackageCandidateId = SPCE.Candidate_ScheduledPackageCandidateId
INNER JOIN ScheduledPackages AS SP ON SP.ScheduledPackageId = SPC.ScheduledPackage_ScheduledPackageId
INNER JOIN [dbo].[PackageExams] pe ON pe.[PackageExamId] = SPCE.[PackageExamId]
INNER JOIN [dbo].[Packages] P ON P.[PackageId] = pe.[PackageId]


--INNER JOIN [dbo].[PackageGrades] PG 
--	ON PG.PackageId = P.PackageId
----	AND PE.ExamTypeId = PG.ExamGradeId
--LEFT JOIN  [dbo].[ExamGrades] EG 
--	ON EG.Id = PG.ExamGradeId

--INNER JOIN ExamGradeDescriptions EGD
--on EGD.ExamGradeId = EG.Id
--INNER JOIN ExamTypes ET
--on ET.ExamTypeId = EGD.ExamTypeId

WHERE P.name = 'Kerala - Four Skills Package'
	And SurpassCandidateRef = 'FP_00009932A'

--ExamGrades.ID = PackageGrades.ExamGradeID

