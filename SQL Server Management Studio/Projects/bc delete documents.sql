SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

--;with Live as (

--select sum(DATALENGTH(esdt.Document))/1024/1024 [MB]
--from ScheduledExamsTable scet
--inner join ExamSessionTable est
--on est.ScheduledExamID = scet.id
--inner join ExamSessionDocumentTable esdt
--on esdt.examSessionId = est.ID
--inner join IB3QualificationLookup ib
--on ib.ID = scet.qualificationId
--where ExportToSecureMarker = 1
--	and scet.ScheduledStartDateTime  > DATEADD(MONTH, -12, getdate())
--	and (qualificationName != 'International Comparisons Research') -- AND examName != 'Speaking')

--)

--select DATEADD(MONTH, -6, getdate())

select sum(cast(DATALENGTH(esdt.Document) as bigint))/1024/1024/1024 [GB]
from Warehouse_ScheduledExamsTable scet
inner join Warehouse_ExamSessionTable est
on est.WAREHOUSEScheduledExamID = scet.id
inner join Warehouse_ExamSessionDocumentTable esdt
on esdt.warehouseExamSessionID = est.ID
where ExportToSecureMarker = 1
	and scet.ScheduledStartDateTime  < DATEADD(MONTH, -6, getdate())
	and (qualificationName != 'International Comparisons Research')

--100 gb

select sum(cast(DATALENGTH(esdt.Document) as bigint))/1024/1024/1024 [GB]
from Warehouse_ScheduledExamsTable scet
inner join Warehouse_ExamSessionTable est
on est.WAREHOUSEScheduledExamID = scet.id
inner join Warehouse_ExamSessionDocumentTable esdt
on esdt.warehouseExamSessionID = est.ID
where ExportToSecureMarker = 1
	and scet.ScheduledStartDateTime  < DATEADD(MONTH, -12, getdate())
	and (qualificationName != 'International Comparisons Research')

select sum(cast(DATALENGTH(esdt.Document) as bigint))/1024/1024/1024 [GB]
from Warehouse_ScheduledExamsTable scet
inner join Warehouse_ExamSessionTable est
on est.WAREHOUSEScheduledExamID = scet.id
inner join Warehouse_ExamSessionDocumentTable esdt
on esdt.warehouseExamSessionID = est.ID
where ExportToSecureMarker = 1
	and scet.ScheduledStartDateTime  < DATEADD(MONTH, -18, getdate())
	and (qualificationName != 'International Comparisons Research')


select sum(cast(DATALENGTH(esdt.Document) as bigint))/1024/1024/1024 [GB]
from Warehouse_ScheduledExamsTable scet
inner join Warehouse_ExamSessionTable est
on est.WAREHOUSEScheduledExamID = scet.id
inner join Warehouse_ExamSessionDocumentTable esdt
on esdt.warehouseExamSessionID = est.ID
where ExportToSecureMarker = 1
	and scet.ScheduledStartDateTime  < DATEADD(MONTH, -24, getdate())
	and (qualificationName != 'International Comparisons Research')

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

select sum(cast(DATALENGTH(esdt.Document) as bigint))/1024/1024/1024 [GB]
from Warehouse_ScheduledExamsTable scet
inner join Warehouse_ExamSessionTable est
on est.WAREHOUSEScheduledExamID = scet.id
inner join Warehouse_ExamSessionDocumentTable esdt
on esdt.warehouseExamSessionID = est.ID
where ExportToSecureMarker = 1
	and scet.ScheduledStartDateTime  < DATEADD(MONTH, -30, getdate())
	and (qualificationName != 'International Comparisons Research')


select sum(cast(DATALENGTH(esdt.Document) as bigint))/1024/1024/1024 [GB]
from Warehouse_ScheduledExamsTable scet
inner join Warehouse_ExamSessionTable est
on est.WAREHOUSEScheduledExamID = scet.id
inner join Warehouse_ExamSessionDocumentTable esdt
on esdt.warehouseExamSessionID = est.ID
where ExportToSecureMarker = 1
	and scet.ScheduledStartDateTime  < DATEADD(MONTH, -36, getdate())
	and (qualificationName != 'International Comparisons Research')


select sum(cast(DATALENGTH(esdt.Document) as bigint))/1024/1024/1024 [GB]
from Warehouse_ScheduledExamsTable scet
inner join Warehouse_ExamSessionTable est
on est.WAREHOUSEScheduledExamID = scet.id
inner join Warehouse_ExamSessionDocumentTable esdt
on esdt.warehouseExamSessionID = est.ID
where ExportToSecureMarker = 1
	and scet.ScheduledStartDateTime  < DATEADD(MONTH, -42, getdate())
	and (qualificationName != 'International Comparisons Research')


select sum(cast(DATALENGTH(esdt.Document) as bigint))/1024/1024/1024 [GB]
from Warehouse_ScheduledExamsTable scet
inner join Warehouse_ExamSessionTable est
on est.WAREHOUSEScheduledExamID = scet.id
inner join Warehouse_ExamSessionDocumentTable esdt
on esdt.warehouseExamSessionID = est.ID
where ExportToSecureMarker = 1
	and scet.ScheduledStartDateTime  < DATEADD(MONTH, -48, getdate())
	and (qualificationName != 'International Comparisons Research')