SET NOCOUNT ON;

IF OBJECT_ID('tempdb..#ExamXML') IS NOT NULL DROP TABLE #ExamXML
--IF OBJECT_ID('tempdb..#Item') IS NOT NULL DROP TABLE #Item
IF OBJECT_ID('tempdb..#WAREHOUSE_ExamSessionItemResponseTable') IS NOT NULL DROP TABLE #WAREHOUSE_ExamSessionItemResponseTable

CREATE TABLE #ExamXML (	
	ID int
	,[StructureXML] XML
	,ResultData XML
	,[ResultDataFull] XML
) 

CREATE TABLE #WAREHOUSE_ExamSessionItemResponseTable (
	[ID] [int],
	[WAREHOUSEExamSessionID] [int],
	[ItemID] [nvarchar](15),
	[ItemVersion] [int] ,
	[ItemResponseData] [xml] ,
	[MarkerResponseData] [xml]
)

BULK INSERT #WAREHOUSE_ExamSessionItemResponseTable
from 'C:\Users\977496-davej\Desktop\ExamSessionItemResponseTable - Copy.csv'
with (fieldterminator = ',', rowterminator = '\n')
go


INSERT #ExamXML(id,  StructureXML, ResultData, ResultDataFull) 
VALUES (
763
,'<assessmentDetails>
  <assessmentID>8</assessmentID>
  <qualificationID>103</qualificationID>
  <qualificationName>Construction Specifications Institute</qualificationName>
  <qualificationReference>CSI_</qualificationReference>
  <assessmentGroupName>Construction Documents Technologist</assessmentGroupName>
  <assessmentGroupID>1</assessmentGroupID>
  <assessmentGroupReference>CDT</assessmentGroupReference>
  <assessmentName>Domestic Form 1</assessmentName>
  <validFromDate>08 Feb 2016</validFromDate>
  <expiryDate>08 Feb 2026</expiryDate>
  <startTime>00:00:00</startTime>
  <endTime>23:59:59</endTime>
  <duration>135</duration>
  <defaultDuration>135</defaultDuration>
  <scheduledDuration>
    <value>135</value>
    <reason></reason>
  </scheduledDuration>
  <sRBonus>180</sRBonus>
  <sRBonusMaximum>240</sRBonusMaximum>
  <externalReference>6038039021</externalReference>
  <passLevelValue>75</passLevelValue>
  <passLevelType>1</passLevelType>
  <status>2</status>
  <testFeedbackType>
    <passFail>0</passFail>
    <percentageMark>0</percentageMark>
    <allowSummaryFeedback>0</allowSummaryFeedback>
    <summaryFeedbackType>1</summaryFeedbackType>
    <itemSummary>0</itemSummary>
    <itemReview>0</itemReview>
    <itemReviewCorrectAnswer>0</itemReviewCorrectAnswer>
    <itemFeedback>0</itemFeedback>
    <printableSummary>0</printableSummary>
    <candidateDetails>0</candidateDetails>
    <feedbackByReference>0</feedbackByReference>
    <allowFeedbackDuringExam>0</allowFeedbackDuringExam>
    <allowFeedbackDuringSummary>0</allowFeedbackDuringSummary>
  </testFeedbackType>
  <itemFeedback>0</itemFeedback>
  <allowCalculator>0</allowCalculator>
  <lastInUseDate>29 Feb 2016 09:40:57</lastInUseDate>
  <completedScriptReview>0</completedScriptReview>
  <assessment currentSection="0" totalTime="0" currentTime="0">
    <intro id="0" name="Introduction" currentItem="">
      <item id="5001P3414" name="CDT Introduction Page" totalMark="0" version="11" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="4" LO="" unit="" quT="0" />
      <item id="5001P4848" name="Tutorial - Intro" totalMark="0" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="4" LO="" unit="" quT="0" />
      <item id="5001P4850" name="Tutorial - Mouse" totalMark="0" version="2" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="4" LO="" unit="" quT="0" />
      <item id="5001P4853" name="Tutorial - Navigation" totalMark="0" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="4" LO="" unit="" quT="0" />
      <item id="5001P4855" name="Tutorial - Time" totalMark="0" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="4" LO="" unit="" quT="0" />
      <item id="5001P4857" name="Tutorial - Scroll" totalMark="0" version="2" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="4" LO="" unit="" quT="0" />
      <item id="5001P4859" name="Tutorial - Multiple Choice" totalMark="0" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="4" LO="" unit="" quT="0" />
      <item id="5001P4862" name="Tutorial - Flags" totalMark="0" version="2" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="4" LO="" unit="" quT="0" />
      <item id="5001P4864" name="Tutorial - Highlight" totalMark="0" version="2" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="4" LO="" unit="" quT="0" />
      <item id="5001P4866" name="Tutorial - Strikeout" totalMark="0" version="7" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="4" LO="" unit="" quT="0" />
      <item id="5001P4868" name="Tutorial - Ending Exam" totalMark="0" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="4" LO="" unit="" quT="0" />
      <item id="5001P4871" name="Tutorial - Conclusion" totalMark="0" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="4" LO="" unit="" quT="0" />
    </intro>
    <outro id="0" name="Finish" currentItem="">
      <item id="5001P3402" name="Finish Page" totalMark="0" version="14" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="2" LO="" unit="" quT="0" />
    </outro>
    <tools id="0" name="Tools" currentItem="" />
    <section id="1" name="1" passLevelValue="0" passLevelType="1" itemsToMark="0" currentItem="" fixed="1">
      <item id="5001P2781" name="IT002379B" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="" unit="" quT="10" nonScored="1" />
      <item id="5001P2903" name="IT130504A" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="" unit="" quT="10" nonScored="1" />
      <item id="5001P2661" name="CSI00027A" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Fundamentals" unit="" quT="10" />
      <item id="5001P2887" name="IT130479" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction Documents" unit="" quT="10" />
      <item id="5001P2703" name="CSI00171" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="" unit="" quT="10" nonScored="1" />
      <item id="5001P2856" name="IT130421" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction Documents" unit="" quT="10" />
      <item id="5001P2875" name="IT130457" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction." unit="" quT="10" />
      <item id="5001P2959" name="IT215041" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Lifecycle Activities" unit="" quT="10" />
      <item id="5001P2707" name="CSI00182" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Design Process" unit="" quT="10" />
      <item id="5001P2712" name="CSI00210A" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction." unit="" quT="10" />
      <item id="5001P2762" name="IT002278" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="" unit="" quT="10" nonScored="1" />
      <item id="5001P2914" name="IT131044" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Procurement" unit="" quT="10" />
      <item id="5001P2908" name="IT130510" totalMark="1" version="6" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction." unit="" quT="10" />
      <item id="5001P2757" name="IT002210A" totalMark="1" version="7" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="" unit="" quT="10" nonScored="1" />
      <item id="5001P2786" name="IT002396" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction." unit="" quT="10" />
      <item id="5001P2844" name="IT003413" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="" unit="" quT="10" nonScored="1" />
      <item id="5001P2666" name="CSI00033" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction Documents" unit="" quT="10" />
      <item id="5001P2926" name="IT131089" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction Documents" unit="" quT="10" />
      <item id="5001P2783" name="IT002387A" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Lifecycle Activities" unit="" quT="10" />
      <item id="5001P2688" name="CSI00093" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Design Process" unit="" quT="10" />
      <item id="5001P2941" name="IT138769" totalMark="1" version="6" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Project Concept and Delivery" unit="" quT="10" />
      <item id="5001P2713" name="CSI00216" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction Documents" unit="" quT="10" />
      <item id="5001P2904" name="IT130505" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction Documents" unit="" quT="10" />
      <item id="5001P2723" name="CSI00311" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Procurement" unit="" quT="10" />
      <item id="5001P2933" name="IT131114" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction Documents" unit="" quT="10" />
      <item id="5001P2951" name="IT214566" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction Documents" unit="" quT="10" />
      <item id="5001P2843" name="IT003412" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="" unit="" quT="10" nonScored="1" />
      <item id="5001P2909" name="IT130511" totalMark="1" version="6" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction." unit="" quT="10" />
      <item id="5001P2697" name="CSI00111B" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction Documents" unit="" quT="10" />
      <item id="5001P2720" name="CSI00230B" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction Documents" unit="" quT="10" />
      <item id="5001P2867" name="IT130439A" totalMark="1" version="6" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Project Concept and Delivery" unit="" quT="10" />
      <item id="5001P2860" name="IT130426" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Preconstruction" unit="" quT="10" />
      <item id="5001P2853" name="IT130418A" totalMark="1" version="6" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Project Concept and Delivery" unit="" quT="10" />
      <item id="5001P2972" name="IT215098A" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="" unit="" quT="10" nonScored="1" />
      <item id="5001P2870" name="IT130449" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction Documents" unit="" quT="10" />
      <item id="5001P2955" name="IT214577" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction Documents" unit="" quT="10" />
      <item id="5001P2977" name="IT215540B" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Lifecycle Activities" unit="" quT="10" />
      <item id="5001P2949" name="IT214552" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction." unit="" quT="10" />
      <item id="5001P2850" name="IT130258A" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction Documents" unit="" quT="10" />
      <item id="5001P2806" name="IT002718" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Fundamentals" unit="" quT="10" />
      <item id="5001P2714" name="CSI00223" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction Documents" unit="" quT="10" />
      <item id="5001P2689" name="CSI00094" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Fundamentals" unit="" quT="10" />
      <item id="5001P2779" name="IT002378" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Lifecycle Activities" unit="" quT="10" />
      <item id="5001P2737" name="CSI00611" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction Documents" unit="" quT="10" />
      <item id="5001P2660" name="CSI00026" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Preconstruction" unit="" quT="10" />
      <item id="5001P2922" name="IT131081" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction." unit="" quT="10" />
      <item id="5001P2975" name="IT215128A" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="" unit="" quT="10" nonScored="1" />
      <item id="5001P2889" name="IT130485" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction Documents" unit="" quT="10" />
      <item id="5001P2930" name="IT131095" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction Documents" unit="" quT="10" />
      <item id="5001P2924" name="IT131087A" totalMark="1" version="6" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Project Concept and Delivery" unit="" quT="10" />
      <item id="5001P2715" name="CSI00225" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Fundamentals" unit="" quT="10" />
      <item id="5001P2751" name="IT002197A" totalMark="1" version="7" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="" unit="" quT="10" nonScored="1" />
      <item id="5001P2655" name="CSI00023B" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction Documents" unit="" quT="10" />
      <item id="5001P2910" name="IT130512" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction." unit="" quT="10" />
      <item id="5001P2902" name="IT130503" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction Documents" unit="" quT="10" />
      <item id="5001P2920" name="IT131069" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Design Process" unit="" quT="10" />
      <item id="5001P2863" name="IT130432A" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction Documents" unit="" quT="10" />
      <item id="5001P2708" name="CSI00190A" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Procurement" unit="" quT="10" />
      <item id="5001P2857" name="IT130422B" totalMark="1" version="6" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Project Concept and Delivery" unit="" quT="10" />
      <item id="5001P2805" name="IT002717" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Fundamentals" unit="" quT="10" />
      <item id="5001P2854" name="IT130419A" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Fundamentals" unit="" quT="10" />
      <item id="5001P2728" name="CSI00467A" totalMark="1" version="6" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Project Concept and Delivery" unit="" quT="10" />
      <item id="5001P2826" name="IT003386" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Design Process" unit="" quT="10" />
      <item id="5001P2896" name="IT130493A" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Procurement" unit="" quT="10" />
      <item id="5001P2874" name="IT130456" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction." unit="" quT="10" />
      <item id="5001P2906" name="IT130508" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Fundamentals" unit="" quT="10" />
      <item id="5001P2809" name="IT002921A" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Design Process" unit="" quT="10" />
      <item id="5001P2654" name="CSI00022A" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="" unit="" quT="10" nonScored="1" />
      <item id="5001P2684" name="CSI00079" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Lifecycle Activities" unit="" quT="10" />
      <item id="5001P2748" name="IT002183A" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="" unit="" quT="10" nonScored="1" />
      <item id="5001P2923" name="IT131086" totalMark="1" version="6" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Project Concept and Delivery" unit="" quT="10" />
      <item id="5001P2956" name="IT214580" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Preconstruction" unit="" quT="10" />
      <item id="5001P2892" name="IT130489" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction Documents" unit="" quT="10" />
      <item id="5001P2877" name="IT130462" totalMark="1" version="6" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction." unit="" quT="10" />
      <item id="5001P2971" name="IT215097A" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Procurement" unit="" quT="10" />
      <item id="5001P2746" name="IT000152" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Design Process" unit="" quT="10" />
      <item id="5001P2749" name="IT002189A" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="" unit="" quT="10" nonScored="1" />
      <item id="5001P2893" name="IT130490A" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction Documents" unit="" quT="10" />
      <item id="5001P2960" name="IT215044A" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Fundamentals" unit="" quT="10" />
      <item id="5001P2678" name="CSI00067A" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Procurement" unit="" quT="10" />
      <item id="5001P2829" name="IT003389" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Design Process" unit="" quT="10" />
      <item id="5001P2760" name="IT002276A" totalMark="1" version="6" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="" unit="" quT="10" nonScored="1" />
      <item id="5001P2664" name="CSI00028A" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="" unit="" quT="10" nonScored="1" />
      <item id="5001P2859" name="IT130425" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction Documents" unit="" quT="10" />
      <item id="5001P2830" name="IT003390" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Design Process" unit="" quT="10" />
      <item id="5001P2929" name="IT131093" totalMark="1" version="6" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Project Concept and Delivery" unit="" quT="10" />
      <item id="5001P2945" name="IT214538" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Preconstruction" unit="" quT="10" />
      <item id="5001P2928" name="IT131091" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Preconstruction" unit="" quT="10" />
      <item id="5001P2732" name="CSI00501" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Fundamentals" unit="" quT="10" />
      <item id="5001P2741" name="IT000084" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Fundamentals" unit="" quT="10" />
      <item id="5001P2882" name="IT130473" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction Documents" unit="" quT="10" />
      <item id="5001P2789" name="IT002401A" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Preconstruction" unit="" quT="10" />
      <item id="5001P2973" name="IT215099A" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Procurement" unit="" quT="10" />
      <item id="5001P2820" name="IT003380" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Design Process" unit="" quT="10" />
      <item id="5001P2690" name="CSI00098A" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction Documents" unit="" quT="10" />
      <item id="5001P2888" name="IT130484" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction Documents" unit="" quT="10" />
      <item id="5001P2744" name="IT000139A" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction." unit="" quT="10" />
      <item id="5001P2740" name="IT000044" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Fundamentals" unit="" quT="10" />
      <item id="5001P2872" name="IT130453" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction." unit="" quT="10" />
      <item id="5001P2885" name="IT130476" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="" unit="" quT="10" nonScored="1" />
      <item id="5001P2838" name="IT003404" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="" unit="" quT="10" nonScored="1" />
      <item id="5001P2944" name="IT214533" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Fundamentals" unit="" quT="10" />
      <item id="5001P2785" name="IT002390" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="" unit="" quT="10" nonScored="1" />
      <item id="5001P2782" name="IT002380" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Lifecycle Activities" unit="" quT="10" />
      <item id="5001P2962" name="IT215050" totalMark="1" version="6" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Project Concept and Delivery" unit="" quT="10" />
      <item id="5001P2818" name="IT003378" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Design Process" unit="" quT="10" />
      <item id="5001P2766" name="IT002317A" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Design Process" unit="" quT="10" />
      <item id="5001P2790" name="IT002409A" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction." unit="" quT="10" />
      <item id="5001P2822" name="IT003382" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Design Process" unit="" quT="10" />
      <item id="5001P2702" name="CSI00170" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction." unit="" quT="10" />
      <item id="5001P2698" name="CSI00112" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Procurement" unit="" quT="10" />
      <item id="5001P2694" name="CSI00104B" totalMark="1" version="6" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Project Concept and Delivery" unit="" quT="10" />
      <item id="5001P2925" name="IT131088A" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="" unit="" quT="10" nonScored="1" />
      <item id="5001P2801" name="IT002499" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="" unit="" quT="10" nonScored="1" />
      <item id="5001P2800" name="IT002486" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction." unit="" quT="10" />
      <item id="5001P2915" name="IT131045A" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction." unit="" quT="10" />
      <item id="5001P2758" name="IT002275A" totalMark="1" version="6" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Procurement" unit="" quT="10" />
      <item id="5001P2868" name="IT130441" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Procurement" unit="" quT="10" />
      <item id="5001P2851" name="IT130415" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction Documents" unit="" quT="10" />
      <item id="5001P2652" name="CSI00021" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction Documents" unit="" quT="10" />
    </section>
    <section id="2" name="S" passLevelValue="0" passLevelType="1" itemsToMark="0" currentItem="" fixed="1">
      <item id="5001P4221" name="Survey Intro" totalMark="0" version="2" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="3" LO="" unit="" quT="0" />
      <item id="5001P4189" name="Survey 01" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="" unit="" quT="10" nonScored="1" />
      <item id="5001P4192" name="Survey 02" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="" unit="" quT="10" nonScored="1" />
      <item id="5001P4196" name="Survey 03" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="" unit="" quT="10" nonScored="1" />
      <item id="5001P4201" name="Survey 04" totalMark="1" version="2" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="" unit="" quT="10" nonScored="1" />
      <item id="5001P4203" name="Survey 05" totalMark="1" version="2" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="" unit="" quT="10" nonScored="1" />
      <item id="5001P4205" name="Survey 06" totalMark="1" version="2" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="" unit="" quT="10" nonScored="1" />
      <item id="5001P4207" name="Survey 07" totalMark="1" version="2" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="" unit="" quT="10" nonScored="1" />
      <item id="5001P4209" name="Survey 08" totalMark="1" version="2" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="" unit="" quT="10" nonScored="1" />
      <item id="5001P4211" name="Survey 09" totalMark="1" version="2" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="" unit="" quT="10" nonScored="1" />
      <item id="5001P4213" name="Survey 10" totalMark="1" version="2" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="" unit="" quT="10" nonScored="1" />
      <item id="5001P4215" name="Survey 11" totalMark="1" version="2" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="" unit="" quT="10" nonScored="1" />
      <item id="5001P6156" name="Survey 12" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="" unit="" quT="11" nonScored="1" />
    </section>
  </assessment>
  <isValid>1</isValid>
  <isPrintable>0</isPrintable>
  <qualityReview>0</qualityReview>
  <numberOfGenerations>1</numberOfGenerations>
  <requiresInvigilation>0</requiresInvigilation>
  <advanceContentDownloadTimespan>120</advanceContentDownloadTimespan>
  <automaticVerification>1</automaticVerification>
  <offlineMode>0</offlineMode>
  <requiresValidation>0</requiresValidation>
  <requiresSecureClient>1</requiresSecureClient>
  <examType>0</examType>
  <advanceDownload>0</advanceDownload>
  <gradeBoundaryData>
    <gradeBoundaries passLevelType="1" adjustedGradeBoundaries="0">
      <grade modifier="lt" value="75" description="Fail" />
      <grade modifier="gt" value="75" description="Pass" />
    </gradeBoundaries>
  </gradeBoundaryData>
  <autoViewExam>0</autoViewExam>
  <autoProgressItems>0</autoProgressItems>
  <confirmationText>
    <confirmationText>This examination is confidential and proprietary. It is made available to you, the examinee, solely for the purpose of assessing your proficiency. You are expressly prohibited from disclosing, discussing, publishing, reproducing, or transmitting this exam, or its content, in whole or in part, in any form or by any means, verbal or written, electronic or mechanical, for any purpose.

To proceed, you must accept the terms of this agreement.</confirmationText>
  </confirmationText>
  <requiresConfirmationCheck>1</requiresConfirmationCheck>
  <allowCloseWithoutSubmit>0</allowCloseWithoutSubmit>
  <useSecureMarker>0</useSecureMarker>
  <annotationVersion>0</annotationVersion>
  <allowPackagingDelivery>1</allowPackagingDelivery>
  <scoreBoundaryData>
    <scoreBoundaries showScoreBoundaryColumn="0">
      <score modifier="lt" value="40" description="Not met" higherBoundarySet="1" />
      <score modifier="lt" value="40" description="Not met" higherBoundarySet="0" />
      <score modifier="gt" value="40" description="Close to met" higherBoundarySet="1" />
      <score modifier="gt" value="40" description="Met" higherBoundarySet="0" />
      <score modifier="gt" value="45" description="Met" higherBoundarySet="1" />
      <score modifier="gt" value="80" description="Exceeded" higherBoundarySet="1" />
    </scoreBoundaries>
  </scoreBoundaryData>
  <showCandidateReportResultColumn>0</showCandidateReportResultColumn>
  <showCandidateReportPercentageColumn>1</showCandidateReportPercentageColumn>
  <certifiedAccessible>0</certifiedAccessible>
  <markingProgressVisible>1</markingProgressVisible>
  <markingProgressMode>0</markingProgressMode>
  <secureClientOperationMode>1</secureClientOperationMode>
  <examDeliverySystemStyleType>delivery_flat</examDeliverySystemStyleType>
  <markingAutoVoidPeriod>999</markingAutoVoidPeriod>
  <showPageRequiresScrollingAlert>1</showPageRequiresScrollingAlert>
  <project ID="5001" />
</assessmentDetails>'
,'<exam passMark="75" passType="1" originalPassMark="75" originalPassType="1" totalMark="100" userMark="0" userPercentage="0" passValue="0" originalPassValue="0" totalTimeSpent="0" closeBoundaryType="0" closeBoundaryValue="0" grade="Fail" originalGrade="Fail">
	<gradeBoundaryData>
		<gradeBoundaries passLevelType="1" adjustedGradeBoundaries="0">
			<grade modifier="lt" value="75" description="Fail" />
			<grade modifier="gt" value="75" description="Pass" />
		</gradeBoundaries>
	</gradeBoundaryData>
	<section id="1" passMark="0" passType="1" name="1" totalMark="100" userMark="0" userPercentage="0" passValue="1" totalTimeSpent="0" itemsToMark="0">
		<item id="5001P2781" name="IT002379B" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="5" type="1" nonScored="1" />
		<item id="5001P2903" name="IT130504A" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="5" type="1" nonScored="1" />
		<item id="5001P2661" name="CSI00027A" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="5" type="1" nonScored="0" />
		<item id="5001P2887" name="IT130479" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="4" type="1" nonScored="0" />
		<item id="5001P2703" name="CSI00171" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="5" type="1" nonScored="1" />
		<item id="5001P2856" name="IT130421" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="4" type="1" nonScored="0" />
		<item id="5001P2875" name="IT130457" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="5" type="1" nonScored="0" />
		<item id="5001P2959" name="IT215041" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="4" type="1" nonScored="0" />
		<item id="5001P2707" name="CSI00182" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="4" type="1" nonScored="0" />
		<item id="5001P2712" name="CSI00210A" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="5" type="1" nonScored="0" />
		<item id="5001P2762" name="IT002278" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="5" type="1" nonScored="1" />
		<item id="5001P2914" name="IT131044" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="4" type="1" nonScored="0" />
		<item id="5001P2908" name="IT130510" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="6" type="1" nonScored="0" />
		<item id="5001P2757" name="IT002210A" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="7" type="1" nonScored="1" />
		<item id="5001P2786" name="IT002396" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="5" type="1" nonScored="0" />
		<item id="5001P2844" name="IT003413" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="5" type="1" nonScored="1" />
		<item id="5001P2666" name="CSI00033" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="4" type="1" nonScored="0" />
		<item id="5001P2926" name="IT131089" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="4" type="1" nonScored="0" />
		<item id="5001P2783" name="IT002387A" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="4" type="1" nonScored="0" />
		<item id="5001P2688" name="CSI00093" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="4" type="1" nonScored="0" />
		<item id="5001P2941" name="IT138769" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="6" type="1" nonScored="0" />
		<item id="5001P2713" name="CSI00216" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="5" type="1" nonScored="0" />
		<item id="5001P2904" name="IT130505" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="4" type="1" nonScored="0" />
		<item id="5001P2723" name="CSI00311" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="4" type="1" nonScored="0" />
		<item id="5001P2933" name="IT131114" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="4" type="1" nonScored="0" />
		<item id="5001P2951" name="IT214566" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="4" type="1" nonScored="0" />
		<item id="5001P2843" name="IT003412" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="5" type="1" nonScored="1" />
		<item id="5001P2909" name="IT130511" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="6" type="1" nonScored="0" />
		<item id="5001P2697" name="CSI00111B" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="5" type="1" nonScored="0" />
		<item id="5001P2720" name="CSI00230B" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="4" type="1" nonScored="0" />
		<item id="5001P2867" name="IT130439A" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="6" type="1" nonScored="0" />
		<item id="5001P2860" name="IT130426" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="5" type="1" nonScored="0" />
		<item id="5001P2853" name="IT130418A" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="6" type="1" nonScored="0" />
		<item id="5001P2972" name="IT215098A" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="5" type="1" nonScored="1" />
		<item id="5001P2870" name="IT130449" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="5" type="1" nonScored="0" />
		<item id="5001P2955" name="IT214577" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="4" type="1" nonScored="0" />
		<item id="5001P2977" name="IT215540B" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="5" type="1" nonScored="0" />
		<item id="5001P2949" name="IT214552" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="5" type="1" nonScored="0" />
		<item id="5001P2850" name="IT130258A" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="4" type="1" nonScored="0" />
		<item id="5001P2806" name="IT002718" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="4" type="1" nonScored="0" />
		<item id="5001P2714" name="CSI00223" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="4" type="1" nonScored="0" />
		<item id="5001P2689" name="CSI00094" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="4" type="1" nonScored="0" />
		<item id="5001P2779" name="IT002378" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="5" type="1" nonScored="0" />
		<item id="5001P2737" name="CSI00611" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="4" type="1" nonScored="0" />
		<item id="5001P2660" name="CSI00026" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="4" type="1" nonScored="0" />
		<item id="5001P2922" name="IT131081" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="5" type="1" nonScored="0" />
		<item id="5001P2975" name="IT215128A" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="5" type="1" nonScored="1" />
		<item id="5001P2889" name="IT130485" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="4" type="1" nonScored="0" />
		<item id="5001P2930" name="IT131095" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="4" type="1" nonScored="0" />
		<item id="5001P2924" name="IT131087A" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="6" type="1" nonScored="0" />
		<item id="5001P2715" name="CSI00225" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="4" type="1" nonScored="0" />
		<item id="5001P2751" name="IT002197A" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="7" type="1" nonScored="1" />
		<item id="5001P2655" name="CSI00023B" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="4" type="1" nonScored="0" />
		<item id="5001P2910" name="IT130512" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="5" type="1" nonScored="0" />
		<item id="5001P2902" name="IT130503" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="4" type="1" nonScored="0" />
		<item id="5001P2920" name="IT131069" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="5" type="1" nonScored="0" />
		<item id="5001P2863" name="IT130432A" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="5" type="1" nonScored="0" />
		<item id="5001P2708" name="CSI00190A" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="4" type="1" nonScored="0" />
		<item id="5001P2857" name="IT130422B" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="6" type="1" nonScored="0" />
		<item id="5001P2805" name="IT002717" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="4" type="1" nonScored="0" />
		<item id="5001P2854" name="IT130419A" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="4" type="1" nonScored="0" />
		<item id="5001P2728" name="CSI00467A" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="6" type="1" nonScored="0" />
		<item id="5001P2826" name="IT003386" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="4" type="1" nonScored="0" />
		<item id="5001P2896" name="IT130493A" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="4" type="1" nonScored="0" />
		<item id="5001P2874" name="IT130456" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="5" type="1" nonScored="0" />
		<item id="5001P2906" name="IT130508" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="4" type="1" nonScored="0" />
		<item id="5001P2809" name="IT002921A" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="4" type="1" nonScored="0" />
		<item id="5001P2654" name="CSI00022A" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="5" type="1" nonScored="1" />
		<item id="5001P2684" name="CSI00079" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="4" type="1" nonScored="0" />
		<item id="5001P2748" name="IT002183A" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="5" type="1" nonScored="1" />
		<item id="5001P2923" name="IT131086" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="6" type="1" nonScored="0" />
		<item id="5001P2956" name="IT214580" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="4" type="1" nonScored="0" />
		<item id="5001P2892" name="IT130489" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="4" type="1" nonScored="0" />
		<item id="5001P2877" name="IT130462" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="6" type="1" nonScored="0" />
		<item id="5001P2971" name="IT215097A" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="5" type="1" nonScored="0" />
		<item id="5001P2746" name="IT000152" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="4" type="1" nonScored="0" />
		<item id="5001P2749" name="IT002189A" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="5" type="1" nonScored="1" />
		<item id="5001P2893" name="IT130490A" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="4" type="1" nonScored="0" />
		<item id="5001P2960" name="IT215044A" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="4" type="1" nonScored="0" />
		<item id="5001P2678" name="CSI00067A" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="4" type="1" nonScored="0" />
		<item id="5001P2829" name="IT003389" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="4" type="1" nonScored="0" />
		<item id="5001P2760" name="IT002276A" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="6" type="1" nonScored="1" />
		<item id="5001P2664" name="CSI00028A" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="5" type="1" nonScored="1" />
		<item id="5001P2859" name="IT130425" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="5" type="1" nonScored="0" />
		<item id="5001P2830" name="IT003390" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="4" type="1" nonScored="0" />
		<item id="5001P2929" name="IT131093" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="6" type="1" nonScored="0" />
		<item id="5001P2945" name="IT214538" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="4" type="1" nonScored="0" />
		<item id="5001P2928" name="IT131091" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="4" type="1" nonScored="0" />
		<item id="5001P2732" name="CSI00501" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="4" type="1" nonScored="0" />
		<item id="5001P2741" name="IT000084" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="4" type="1" nonScored="0" />
		<item id="5001P2882" name="IT130473" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="4" type="1" nonScored="0" />
		<item id="5001P2789" name="IT002401A" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="5" type="1" nonScored="0" />
		<item id="5001P2973" name="IT215099A" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="4" type="1" nonScored="0" />
		<item id="5001P2820" name="IT003380" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="4" type="1" nonScored="0" />
		<item id="5001P2690" name="CSI00098A" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="4" type="1" nonScored="0" />
		<item id="5001P2888" name="IT130484" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="4" type="1" nonScored="0" />
		<item id="5001P2744" name="IT000139A" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="5" type="1" nonScored="0" />
		<item id="5001P2740" name="IT000044" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="4" type="1" nonScored="0" />
		<item id="5001P2872" name="IT130453" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="5" type="1" nonScored="0" />
		<item id="5001P2885" name="IT130476" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="5" type="1" nonScored="1" />
		<item id="5001P2838" name="IT003404" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="5" type="1" nonScored="1" />
		<item id="5001P2944" name="IT214533" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="4" type="1" nonScored="0" />
		<item id="5001P2785" name="IT002390" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="5" type="1" nonScored="1" />
		<item id="5001P2782" name="IT002380" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="4" type="1" nonScored="0" />
		<item id="5001P2962" name="IT215050" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="6" type="1" nonScored="0" />
		<item id="5001P2818" name="IT003378" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="4" type="1" nonScored="0" />
		<item id="5001P2766" name="IT002317A" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="4" type="1" nonScored="0" />
		<item id="5001P2790" name="IT002409A" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="5" type="1" nonScored="0" />
		<item id="5001P2822" name="IT003382" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="4" type="1" nonScored="0" />
		<item id="5001P2702" name="CSI00170" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="5" type="1" nonScored="0" />
		<item id="5001P2698" name="CSI00112" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="4" type="1" nonScored="0" />
		<item id="5001P2694" name="CSI00104B" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="6" type="1" nonScored="0" />
		<item id="5001P2925" name="IT131088A" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="5" type="1" nonScored="1" />
		<item id="5001P2801" name="IT002499" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="5" type="1" nonScored="1" />
		<item id="5001P2800" name="IT002486" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="5" type="1" nonScored="0" />
		<item id="5001P2915" name="IT131045A" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="5" type="1" nonScored="0" />
		<item id="5001P2758" name="IT002275A" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="6" type="1" nonScored="0" />
		<item id="5001P2868" name="IT130441" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="5" type="1" nonScored="0" />
		<item id="5001P2851" name="IT130415" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="4" type="1" nonScored="0" />
		<item id="5001P2652" name="CSI00021" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="4" type="1" nonScored="0" />
	</section>
	<section id="2" passMark="0" passType="1" name="S" totalMark="0" userMark="0" userPercentage="0" passValue="1" totalTimeSpent="0" itemsToMark="0">
		<item id="5001P4221" name="Survey Intro" totalMark="0" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="2" type="3" nonScored="0" />
		<item id="5001P4189" name="Survey 01" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="5" type="1" nonScored="1" />
		<item id="5001P4192" name="Survey 02" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="4" type="1" nonScored="1" />
		<item id="5001P4196" name="Survey 03" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="3" type="1" nonScored="1" />
		<item id="5001P4201" name="Survey 04" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="2" type="1" nonScored="1" />
		<item id="5001P4203" name="Survey 05" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="2" type="1" nonScored="1" />
		<item id="5001P4205" name="Survey 06" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="2" type="1" nonScored="1" />
		<item id="5001P4207" name="Survey 07" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="2" type="1" nonScored="1" />
		<item id="5001P4209" name="Survey 08" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="2" type="1" nonScored="1" />
		<item id="5001P4211" name="Survey 09" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="2" type="1" nonScored="1" />
		<item id="5001P4213" name="Survey 10" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="2" type="1" nonScored="1" />
		<item id="5001P4215" name="Survey 11" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="2" type="1" nonScored="1" />
		<item id="5001P6156" name="Survey 12" totalMark="1" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="0" version="3" type="1" nonScored="1" />
	</section>
</exam>
'
,'<assessmentDetails>
	<assessmentID>8</assessmentID>
	<qualificationID>103</qualificationID>
	<qualificationName>Construction Specifications Institute</qualificationName>
	<qualificationReference>CSI_</qualificationReference>
	<assessmentGroupName>Construction Documents Technologist</assessmentGroupName>
	<assessmentGroupID>1</assessmentGroupID>
	<assessmentGroupReference>CDT</assessmentGroupReference>
	<assessmentName>Domestic Form 1</assessmentName>
	<validFromDate>08 Feb 2016</validFromDate>
	<expiryDate>08 Feb 2026</expiryDate>
	<startTime>00:00:00</startTime>
	<endTime>23:59:59</endTime>
	<duration>135</duration>
	<defaultDuration>135</defaultDuration>
	<scheduledDuration>
		<value>135</value>
		<reason />
	</scheduledDuration>
	<sRBonus>180</sRBonus>
	<sRBonusMaximum>240</sRBonusMaximum>
	<externalReference>6038039021</externalReference>
	<passLevelValue>75</passLevelValue>
	<passLevelType>1</passLevelType>
	<status>2</status>
	<testFeedbackType>
		<passFail>0</passFail>
		<percentageMark>0</percentageMark>
		<allowSummaryFeedback>0</allowSummaryFeedback>
		<summaryFeedbackType>1</summaryFeedbackType>
		<itemSummary>0</itemSummary>
		<itemReview>0</itemReview>
		<itemReviewCorrectAnswer>0</itemReviewCorrectAnswer>
		<itemFeedback>0</itemFeedback>
		<printableSummary>0</printableSummary>
		<candidateDetails>0</candidateDetails>
		<feedbackByReference>0</feedbackByReference>
		<allowFeedbackDuringExam>0</allowFeedbackDuringExam>
		<allowFeedbackDuringSummary>0</allowFeedbackDuringSummary>
	</testFeedbackType>
	<itemFeedback>0</itemFeedback>
	<allowCalculator>0</allowCalculator>
	<lastInUseDate>29 Feb 2016 09:40:57</lastInUseDate>
	<completedScriptReview>0</completedScriptReview>
	<assessment currentSection="0" totalTime="0" currentTime="0" originalPassMark="75" originalPassType="1" passMark="75" passType="1" totalMark="100" userMark="0" userPercentage="0" passValue="0" originalGrade="Fail">
		<intro id="0" name="Introduction" currentItem="">
			<item id="5001P3414" name="CDT Introduction Page" totalMark="0" version="11" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="4" LO="" unit="" quT="0" />
			<item id="5001P4848" name="Tutorial - Intro" totalMark="0" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="4" LO="" unit="" quT="0" />
			<item id="5001P4850" name="Tutorial - Mouse" totalMark="0" version="2" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="4" LO="" unit="" quT="0" />
			<item id="5001P4853" name="Tutorial - Navigation" totalMark="0" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="4" LO="" unit="" quT="0" />
			<item id="5001P4855" name="Tutorial - Time" totalMark="0" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="4" LO="" unit="" quT="0" />
			<item id="5001P4857" name="Tutorial - Scroll" totalMark="0" version="2" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="4" LO="" unit="" quT="0" />
			<item id="5001P4859" name="Tutorial - Multiple Choice" totalMark="0" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="4" LO="" unit="" quT="0" />
			<item id="5001P4862" name="Tutorial - Flags" totalMark="0" version="2" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="4" LO="" unit="" quT="0" />
			<item id="5001P4864" name="Tutorial - Highlight" totalMark="0" version="2" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="4" LO="" unit="" quT="0" />
			<item id="5001P4866" name="Tutorial - Strikeout" totalMark="0" version="7" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="4" LO="" unit="" quT="0" />
			<item id="5001P4868" name="Tutorial - Ending Exam" totalMark="0" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="4" LO="" unit="" quT="0" />
			<item id="5001P4871" name="Tutorial - Conclusion" totalMark="0" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="4" LO="" unit="" quT="0" />
		</intro>
		<outro id="0" name="Finish" currentItem="">
			<item id="5001P3402" name="Finish Page" totalMark="0" version="14" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="2" LO="" unit="" quT="0" />
		</outro>
		<tools id="0" name="Tools" currentItem="" />
		<section id="1" name="1" passLevelValue="0" passLevelType="1" itemsToMark="0" currentItem="" fixed="1" totalMark="100" userMark="0" userPercentage="0" passValue="1">
			<item id="5001P2781" name="IT002379B" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="" unit="" quT="10" nonScored="1">
				<p cs="1" ua="1" id="5001P2781" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="1" sl="1" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2903" name="IT130504A" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="" unit="" quT="10" nonScored="1">
				<p cs="1" ua="1" id="5001P2903" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="1" sl="1" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2661" name="CSI00027A" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Fundamentals" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2661" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="1" sl="1" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2887" name="IT130479" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction Documents" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2887" um="0">
					<s id="1" ua="1" um="0">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="0">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="1" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="1" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2703" name="CSI00171" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="" unit="" quT="10" nonScored="1">
				<p cs="1" ua="1" id="5001P2703" um="0">
					<s id="1" ua="1" um="0">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="0">
							<i id="1" ca="1" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="1" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2856" name="IT130421" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction Documents" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2856" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="1" sl="1" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2875" name="IT130457" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction." unit="" quT="10">
				<p cs="1" ua="1" id="5001P2875" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="1" sl="1" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2959" name="IT215041" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Lifecycle Activities" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2959" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="1" sl="1" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2707" name="CSI00182" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Design Process" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2707" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="1" sl="1" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2712" name="CSI00210A" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction." unit="" quT="10">
				<p cs="1" ua="1" id="5001P2712" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="1" sl="1" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2762" name="IT002278" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="" unit="" quT="10" nonScored="1">
				<p cs="1" ua="1" id="5001P2762" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="1" sl="1" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2914" name="IT131044" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Procurement" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2914" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="1" sl="1" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2908" name="IT130510" totalMark="1" version="6" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction." unit="" quT="10">
				<p cs="1" ua="1" id="5001P2908" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="1" sl="1" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2757" name="IT002210A" totalMark="1" version="7" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="" unit="" quT="10" nonScored="1">
				<p cs="1" ua="1" id="5001P2757" um="0">
					<s id="1" ua="1" um="0">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="0">
							<i id="1" ca="1" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="1" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2786" name="IT002396" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction." unit="" quT="10">
				<p cs="1" ua="1" id="5001P2786" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="1" sl="1" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2844" name="IT003413" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="" unit="" quT="10" nonScored="1">
				<p cs="1" ua="1" id="5001P2844" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="1" sl="1" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2666" name="CSI00033" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction Documents" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2666" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="1" sl="1" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2926" name="IT131089" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction Documents" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2926" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="1" sl="1" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2783" name="IT002387A" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Lifecycle Activities" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2783" um="0">
					<s id="1" ua="1" um="0">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="0">
							<i id="1" ca="1" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="1" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2688" name="CSI00093" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Design Process" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2688" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="1" sl="1" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2941" name="IT138769" totalMark="1" version="6" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Project Concept and Delivery" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2941" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="1" sl="1" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2713" name="CSI00216" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction Documents" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2713" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="1" sl="1" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2904" name="IT130505" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction Documents" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2904" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="1" sl="1" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2723" name="CSI00311" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Procurement" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2723" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="1" sl="1" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2933" name="IT131114" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction Documents" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2933" um="0">
					<s id="1" ua="1" um="0">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="0">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="1" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="1" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2951" name="IT214566" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction Documents" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2951" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="1" sl="1" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2843" name="IT003412" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="" unit="" quT="10" nonScored="1">
				<p cs="1" ua="1" id="5001P2843" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="1" sl="1" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2909" name="IT130511" totalMark="1" version="6" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction." unit="" quT="10">
				<p cs="1" ua="1" id="5001P2909" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="1" sl="1" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2697" name="CSI00111B" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction Documents" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2697" um="0">
					<s id="1" ua="1" um="0">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="0">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="1" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="1" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2720" name="CSI00230B" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction Documents" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2720" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="1" sl="1" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2867" name="IT130439A" totalMark="1" version="6" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Project Concept and Delivery" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2867" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="1" sl="1" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2860" name="IT130426" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Preconstruction" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2860" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="1" sl="1" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2853" name="IT130418A" totalMark="1" version="6" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Project Concept and Delivery" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2853" um="0">
					<s id="1" ua="1" um="0">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="0">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="1" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="1" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2972" name="IT215098A" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="" unit="" quT="10" nonScored="1">
				<p cs="1" ua="1" id="5001P2972" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="1" sl="1" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2870" name="IT130449" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction Documents" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2870" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="1" sl="1" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2955" name="IT214577" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction Documents" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2955" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="1" sl="1" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2977" name="IT215540B" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Lifecycle Activities" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2977" um="0">
					<s id="1" ua="1" um="0">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="0">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="1" ac="C" struckOut="0" />
							<i id="4" ca="1" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2949" name="IT214552" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction." unit="" quT="10">
				<p cs="1" ua="1" id="5001P2949" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="1" sl="1" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2850" name="IT130258A" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction Documents" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2850" um="0">
					<s id="1" ua="1" um="0">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="0">
							<i id="1" ca="1" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="1" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2806" name="IT002718" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Fundamentals" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2806" um="0">
					<s id="1" ua="1" um="0">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="0">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="1" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="1" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2714" name="CSI00223" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction Documents" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2714" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="1" sl="1" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2689" name="CSI00094" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Fundamentals" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2689" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="1" sl="1" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2779" name="IT002378" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Lifecycle Activities" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2779" um="0">
					<s id="1" ua="1" um="0">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="0">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="1" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="1" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2737" name="CSI00611" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction Documents" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2737" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="1" sl="1" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2660" name="CSI00026" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Preconstruction" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2660" um="0">
					<s id="1" ua="1" um="0">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="0">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="1" ac="B" struckOut="0" />
							<i id="3" ca="1" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2922" name="IT131081" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction." unit="" quT="10">
				<p cs="1" ua="1" id="5001P2922" um="0">
					<s id="1" ua="1" um="0">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="0">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="1" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="1" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2975" name="IT215128A" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="" unit="" quT="10" nonScored="1">
				<p cs="1" ua="1" id="5001P2975" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="1" sl="1" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2889" name="IT130485" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction Documents" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2889" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="1" sl="1" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2930" name="IT131095" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction Documents" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2930" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="1" sl="1" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2924" name="IT131087A" totalMark="1" version="6" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Project Concept and Delivery" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2924" um="0">
					<s id="1" ua="1" um="0">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="0">
							<i id="1" ca="0" sl="1" ac="A" struckOut="0" />
							<i id="2" ca="1" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2715" name="CSI00225" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Fundamentals" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2715" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="1" sl="1" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2751" name="IT002197A" totalMark="1" version="7" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="" unit="" quT="10" nonScored="1">
				<p cs="1" ua="1" id="5001P2751" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="1" sl="1" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2655" name="CSI00023B" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction Documents" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2655" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="1" sl="1" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2910" name="IT130512" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction." unit="" quT="10">
				<p cs="1" ua="1" id="5001P2910" um="0">
					<s id="1" ua="1" um="0">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="0">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="1" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="1" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2902" name="IT130503" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction Documents" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2902" um="0">
					<s id="1" ua="1" um="0">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="0">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="1" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="1" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2920" name="IT131069" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Design Process" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2920" um="0">
					<s id="1" ua="1" um="0">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="0">
							<i id="1" ca="1" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="1" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2863" name="IT130432A" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction Documents" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2863" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="1" sl="1" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2708" name="CSI00190A" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Procurement" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2708" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="1" sl="1" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2857" name="IT130422B" totalMark="1" version="6" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Project Concept and Delivery" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2857" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="1" sl="1" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2805" name="IT002717" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Fundamentals" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2805" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="1" sl="1" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2854" name="IT130419A" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Fundamentals" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2854" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="1" sl="1" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2728" name="CSI00467A" totalMark="1" version="6" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Project Concept and Delivery" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2728" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="1" sl="1" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2826" name="IT003386" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Design Process" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2826" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="1" sl="1" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2896" name="IT130493A" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Procurement" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2896" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="1" sl="1" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2874" name="IT130456" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction." unit="" quT="10">
				<p cs="1" ua="1" id="5001P2874" um="0">
					<s id="1" ua="1" um="0">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="0">
							<i id="1" ca="1" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="1" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2906" name="IT130508" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Fundamentals" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2906" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="1" sl="1" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2809" name="IT002921A" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Design Process" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2809" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="1" sl="1" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2654" name="CSI00022A" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="" unit="" quT="10" nonScored="1">
				<p cs="1" ua="1" id="5001P2654" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="1" sl="1" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2684" name="CSI00079" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Lifecycle Activities" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2684" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="1" sl="1" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2748" name="IT002183A" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="" unit="" quT="10" nonScored="1">
				<p cs="1" ua="1" id="5001P2748" um="0">
					<s id="1" ua="1" um="0">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="0">
							<i id="1" ca="0" sl="1" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="1" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2923" name="IT131086" totalMark="1" version="6" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Project Concept and Delivery" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2923" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="1" sl="1" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2956" name="IT214580" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Preconstruction" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2956" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="1" sl="1" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2892" name="IT130489" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction Documents" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2892" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="1" sl="1" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2877" name="IT130462" totalMark="1" version="6" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction." unit="" quT="10">
				<p cs="1" ua="1" id="5001P2877" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="1" sl="1" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2971" name="IT215097A" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Procurement" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2971" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="1" sl="1" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2746" name="IT000152" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Design Process" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2746" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="1" sl="1" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2749" name="IT002189A" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="" unit="" quT="10" nonScored="1">
				<p cs="1" ua="1" id="5001P2749" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="1" sl="1" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2893" name="IT130490A" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction Documents" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2893" um="0">
					<s id="1" ua="1" um="0">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="0">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="1" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="1" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2960" name="IT215044A" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Fundamentals" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2960" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="1" sl="1" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2678" name="CSI00067A" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Procurement" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2678" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="1" sl="1" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2829" name="IT003389" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Design Process" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2829" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="1" sl="1" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2760" name="IT002276A" totalMark="1" version="6" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="" unit="" quT="10" nonScored="1">
				<p cs="1" ua="1" id="5001P2760" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="1" sl="1" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2664" name="CSI00028A" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="" unit="" quT="10" nonScored="1">
				<p cs="1" ua="1" id="5001P2664" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="1" sl="1" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2859" name="IT130425" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction Documents" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2859" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="1" sl="1" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2830" name="IT003390" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Design Process" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2830" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="1" sl="1" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2929" name="IT131093" totalMark="1" version="6" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Project Concept and Delivery" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2929" um="0">
					<s id="1" ua="1" um="0">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="0">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="1" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="1" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2945" name="IT214538" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Preconstruction" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2945" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="1" sl="1" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2928" name="IT131091" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Preconstruction" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2928" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="1" sl="1" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2732" name="CSI00501" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Fundamentals" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2732" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="1" sl="1" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2741" name="IT000084" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Fundamentals" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2741" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="1" sl="1" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2882" name="IT130473" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction Documents" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2882" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="1" sl="1" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2789" name="IT002401A" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Preconstruction" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2789" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="1" sl="1" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2973" name="IT215099A" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Procurement" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2973" um="0">
					<s id="1" ua="1" um="0">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="0">
							<i id="1" ca="1" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="1" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2820" name="IT003380" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Design Process" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2820" um="0">
					<s id="1" ua="1" um="0">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="0">
							<i id="1" ca="0" sl="1" ac="A" struckOut="0" />
							<i id="2" ca="1" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2690" name="CSI00098A" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction Documents" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2690" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="1" sl="1" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2888" name="IT130484" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction Documents" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2888" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="1" sl="1" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2744" name="IT000139A" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction." unit="" quT="10">
				<p cs="1" ua="1" id="5001P2744" um="0">
					<s id="1" ua="1" um="0">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="0">
							<i id="1" ca="1" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="1" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2740" name="IT000044" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Fundamentals" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2740" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="1" sl="1" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2872" name="IT130453" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction." unit="" quT="10">
				<p cs="1" ua="1" id="5001P2872" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="1" sl="1" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2885" name="IT130476" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="" unit="" quT="10" nonScored="1">
				<p cs="1" ua="1" id="5001P2885" um="0">
					<s id="1" ua="1" um="0">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="0">
							<i id="1" ca="1" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="1" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2838" name="IT003404" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="" unit="" quT="10" nonScored="1">
				<p cs="1" ua="1" id="5001P2838" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="1" sl="1" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2944" name="IT214533" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Fundamentals" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2944" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="1" sl="1" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2785" name="IT002390" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="" unit="" quT="10" nonScored="1">
				<p cs="1" ua="1" id="5001P2785" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="1" sl="1" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2782" name="IT002380" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Lifecycle Activities" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2782" um="0">
					<s id="1" ua="1" um="0">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="0">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="1" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="1" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2962" name="IT215050" totalMark="1" version="6" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Project Concept and Delivery" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2962" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="1" sl="1" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2818" name="IT003378" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Design Process" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2818" um="0">
					<s id="1" ua="1" um="0">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="0">
							<i id="1" ca="0" sl="1" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="1" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2766" name="IT002317A" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Design Process" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2766" um="0">
					<s id="1" ua="1" um="0">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="0">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="1" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="1" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2790" name="IT002409A" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction." unit="" quT="10">
				<p cs="1" ua="1" id="5001P2790" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="1" sl="1" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2822" name="IT003382" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Design Process" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2822" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="1" sl="1" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2702" name="CSI00170" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction." unit="" quT="10">
				<p cs="1" ua="1" id="5001P2702" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="1" sl="1" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2698" name="CSI00112" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Procurement" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2698" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="1" sl="1" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2694" name="CSI00104B" totalMark="1" version="6" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Project Concept and Delivery" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2694" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="1" sl="1" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2925" name="IT131088A" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="" unit="" quT="10" nonScored="1">
				<p cs="1" ua="1" id="5001P2925" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="1" sl="1" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2801" name="IT002499" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="" unit="" quT="10" nonScored="1">
				<p cs="1" ua="1" id="5001P2801" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="1" sl="1" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2800" name="IT002486" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction." unit="" quT="10">
				<p cs="1" ua="1" id="5001P2800" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="1" sl="1" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2915" name="IT131045A" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction." unit="" quT="10">
				<p cs="1" ua="1" id="5001P2915" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="1" sl="1" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2758" name="IT002275A" totalMark="1" version="6" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Procurement" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2758" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="0" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="1" sl="1" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2868" name="IT130441" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Procurement" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2868" um="0">
					<s id="1" ua="1" um="0">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="0">
							<i id="1" ca="1" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="1" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2851" name="IT130415" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction Documents" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2851" um="0">
					<s id="1" ua="1" um="0">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="0">
							<i id="1" ca="0" sl="1" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="1" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P2652" name="CSI00021" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="Construction Documents" unit="" quT="10">
				<p cs="1" ua="1" id="5001P2652" um="0">
					<s id="1" ua="1" um="0">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="0">
							<i id="1" ca="1" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="1" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
		</section>
		<section id="2" name="S" passLevelValue="0" passLevelType="1" itemsToMark="0" currentItem="" fixed="1" totalMark="0" userMark="0" userPercentage="0" passValue="1">
			<item id="5001P4221" name="Survey Intro" totalMark="0" version="2" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="3" LO="" unit="" quT="0">
				<p cs="1" ua="0" id="5001P4221" um="0">
					<s id="1" ua="0" um="0">
						<c id="2" typ="1">
							<i id="1" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P4189" name="Survey 01" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="" unit="" quT="10" nonScored="1">
				<p cs="1" ua="1" id="5001P4189" um="0">
					<s id="1" ua="1" um="0">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="0">
							<i id="1" ca="1" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="1" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
							<i id="5" ca="0" sl="0" ac="E" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P4192" name="Survey 02" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="" unit="" quT="10" nonScored="1">
				<p cs="1" ua="1" id="5001P4192" um="0">
					<s id="1" ua="1" um="0">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="0">
							<i id="1" ca="1" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
							<i id="5" ca="0" sl="1" ac="E" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P4196" name="Survey 03" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="" unit="" quT="10" nonScored="1">
				<p cs="1" ua="1" id="5001P4196" um="0">
					<s id="1" ua="1" um="0">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="0">
							<i id="1" ca="1" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="1" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
							<i id="5" ca="0" sl="0" ac="E" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P4201" name="Survey 04" totalMark="1" version="2" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="" unit="" quT="10" nonScored="1">
				<p cs="1" ua="1" id="5001P4201" um="0">
					<s id="1" ua="1" um="0">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="0">
							<i id="1" ca="1" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="1" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
							<i id="5" ca="0" sl="0" ac="E" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P4203" name="Survey 05" totalMark="1" version="2" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="" unit="" quT="10" nonScored="1">
				<p cs="1" ua="1" id="5001P4203" um="1">
					<s id="1" ua="1" um="1">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="1">
							<i id="1" ca="1" sl="1" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
							<i id="5" ca="0" sl="0" ac="E" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P4205" name="Survey 06" totalMark="1" version="2" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="" unit="" quT="10" nonScored="1">
				<p cs="1" ua="1" id="5001P4205" um="0">
					<s id="1" ua="1" um="0">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="0">
							<i id="1" ca="1" sl="0" ac="A" struckOut="0" x="145" y="317" wid="795" hei="60" labelId="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" x="145" y="242" wid="795" hei="60" labelId="0" />
							<i id="3" ca="0" sl="1" ac="C" struckOut="0" x="145" y="392" wid="795" hei="60" labelId="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" x="145" y="167" wid="795" hei="60" labelId="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P4207" name="Survey 07" totalMark="1" version="2" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="" unit="" quT="10" nonScored="1">
				<p cs="1" ua="1" id="5001P4207" um="0">
					<s id="1" ua="1" um="0">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="0">
							<i id="1" ca="1" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="1" ac="D" struckOut="0" />
							<i id="5" ca="0" sl="0" ac="E" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P4209" name="Survey 08" totalMark="1" version="2" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="" unit="" quT="10" nonScored="1">
				<p cs="1" ua="1" id="5001P4209" um="0">
					<s id="1" ua="1" um="0">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="0">
							<i id="1" ca="1" sl="1" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="1" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
							<i id="5" ca="0" sl="0" ac="E" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P4211" name="Survey 09" totalMark="1" version="2" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="" unit="" quT="10" nonScored="1">
				<p cs="1" ua="1" id="5001P4211" um="0">
					<s id="1" ua="1" um="0">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="0">
							<i id="1" ca="1" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="1" ac="D" struckOut="0" />
							<i id="5" ca="0" sl="0" ac="E" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P4213" name="Survey 10" totalMark="1" version="2" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="" unit="" quT="10" nonScored="1">
				<p cs="1" ua="1" id="5001P4213" um="0">
					<s id="1" ua="1" um="0">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="0">
							<i id="1" ca="1" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="0" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
							<i id="5" ca="0" sl="1" ac="E" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P4215" name="Survey 11" totalMark="1" version="2" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="" unit="" quT="10" nonScored="1">
				<p cs="1" ua="1" id="5001P4215" um="0">
					<s id="1" ua="1" um="0">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="10" um="0">
							<i id="1" ca="1" sl="0" ac="A" struckOut="0" />
							<i id="2" ca="0" sl="1" ac="B" struckOut="0" />
							<i id="3" ca="0" sl="0" ac="C" struckOut="0" />
							<i id="4" ca="0" sl="0" ac="D" struckOut="0" />
							<i id="5" ca="0" sl="0" ac="E" struckOut="0" />
						</c>
					</s>
				</p>
			</item>
			<item id="5001P6156" name="Survey 12" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" type="1" LO="" unit="" quT="11" nonScored="1">
				<p cs="1" ua="1" id="5001P6156" um="0">
					<s id="1" ua="1" um="0">
						<c id="2" typ="1">
							<i id="1" />
						</c><c ie="1" wei="1" ua="1" id="1" typ="11" um="0">
							<i id="1" ca="This is the correct answer.">none</i>
						</c>
					</s>
				</p>
			</item>
		</section>
	</assessment>
	<isValid>1</isValid>
	<isPrintable>0</isPrintable>
	<qualityReview>0</qualityReview>
	<numberOfGenerations>1</numberOfGenerations>
	<requiresInvigilation>0</requiresInvigilation>
	<advanceContentDownloadTimespan>120</advanceContentDownloadTimespan>
	<automaticVerification>1</automaticVerification>
	<offlineMode>0</offlineMode>
	<requiresValidation>0</requiresValidation>
	<requiresSecureClient>1</requiresSecureClient>
	<examType>0</examType>
	<advanceDownload>0</advanceDownload>
	<gradeBoundaryData>
		<gradeBoundaries passLevelType="1" adjustedGradeBoundaries="0">
			<grade modifier="lt" value="75" description="Fail" />
			<grade modifier="gt" value="75" description="Pass" />
		</gradeBoundaries>
	</gradeBoundaryData>
	<autoViewExam>0</autoViewExam>
	<autoProgressItems>0</autoProgressItems>
	<confirmationText>
		<confirmationText>This examination is confidential and proprietary. It is made available to you, the examinee, solely for the purpose of assessing your proficiency. You are expressly prohibited from disclosing, discussing, publishing, reproducing, or transmitting this exam, or its content, in whole or in part, in any form or by any means, verbal or written, electronic or mechanical, for any purpose. To proceed, you must accept the terms of this agreement.</confirmationText>
	</confirmationText>
	<requiresConfirmationCheck>1</requiresConfirmationCheck>
	<allowCloseWithoutSubmit>0</allowCloseWithoutSubmit>
	<useSecureMarker>0</useSecureMarker>
	<annotationVersion>0</annotationVersion>
	<allowPackagingDelivery>1</allowPackagingDelivery>
	<scoreBoundaryData>
		<scoreBoundaries showScoreBoundaryColumn="0">
			<score modifier="lt" value="40" description="Not met" higherBoundarySet="1" />
			<score modifier="lt" value="40" description="Not met" higherBoundarySet="0" />
			<score modifier="gt" value="40" description="Close to met" higherBoundarySet="1" />
			<score modifier="gt" value="40" description="Met" higherBoundarySet="0" />
			<score modifier="gt" value="45" description="Met" higherBoundarySet="1" />
			<score modifier="gt" value="80" description="Exceeded" higherBoundarySet="1" />
		</scoreBoundaries>
	</scoreBoundaryData>
	<showCandidateReportResultColumn>0</showCandidateReportResultColumn>
	<showCandidateReportPercentageColumn>1</showCandidateReportPercentageColumn>
	<certifiedAccessible>0</certifiedAccessible>
	<markingProgressVisible>1</markingProgressVisible>
	<markingProgressMode>0</markingProgressMode>
	<secureClientOperationMode>1</secureClientOperationMode>
	<examDeliverySystemStyleType>delivery_flat</examDeliverySystemStyleType>
	<markingAutoVoidPeriod>999</markingAutoVoidPeriod>
	<showPageRequiresScrollingAlert>1</showPageRequiresScrollingAlert>
	<project ID="5001" />
</assessmentDetails>'
);

--select * from #ExamXML
--select * from #WAREHOUSE_ExamSessionItemResponseTable

select examSessionId, KeyCode, warehouseTime
from WAREHOUSE_ExamSessionTable_Shreded WESTS (NOLOCK)
INNER JOIN (

	select warehouseExamSessionID 
	from #WAREHOUSE_ExamSessionItemResponseTable (NOLOCK)
	where ItemResponseData.exist('p[@um>0]') = 1
)  WESIRT
ON WESTS.ExamSessioNID = WESIRT.warehouseExamSessionID
where userMark = 0 
	and userPercentage = 0;





select *
from WAREHOUSE_ExamSessionTable_Shreded WESTS (NOLOCK)
INNER JOIN (

	select warehouseExamSessionID 
	from #WAREHOUSE_ExamSessionItemResponseTable (NOLOCK)
	where ItemResponseData.exist('p[@um>0]') = 1
	
)  WESIRT
ON WESTS.ExamSessioNID = WESIRT.warehouseExamSessionID
where previousExamState != 10
	and userMark = 0 
	and userPercentage = 0
	order by examSessionId desc;