DECLARE @command1 nvarchar(1000) = '

USE [?];

select ''?''  as DBName
		, u.name as ''User''
		, (select SUSER_SNAME (u.sid)) as ''Login''
		, r.name  as RoleName
		--, u.*
from sys.database_role_members RM 
	right join sys.database_principals U on U.principal_id = RM.member_principal_id
	left join sys.database_principals R on R.principal_id = RM.role_principal_id
where u.type<>''R''
	and U.type = ''S''
	and r.name IS NOT NULL
	and ''?'' not in (''msdb'',''tempdb'',''model'',''master'')
'

DECLARE @UserLogins TABLE (DBName nvarchar(150), userName nvarchar(150), loginName nvarchar(150), RoleName nvarchar(50))
INSERT @UserLogins
EXEC sp_MSforeachdb  @command1

DECLARE @prefix nvarchar(4) = 'PRV_'


SELECT 'IF(SUSER_ID('+QUOTENAME(@prefix + A.Dbname + '_' + p.name,'''')+') IS NULL)BEGIN CREATE LOGIN '+QUOTENAME(@prefix + A.Dbname + '_' + p.name)
   +
       CASE WHEN p.type_desc = 'SQL_LOGIN'
            THEN ' WITH PASSWORD = '+CONVERT(NVARCHAR(MAX),L.password_hash,1)+' HASHED'
            ELSE ' FROM WINDOWS'
       END + ';/*'+p.type_desc+'*/ END;
       use [' + DBNAME +']
       '
       
       
       COLLATE SQL_Latin1_General_CP1_CI_AS
  FROM sys.server_principals AS p
  LEFT JOIN sys.sql_logins AS L
    ON p.principal_id = L.principal_id
  INNER JOIN @UserLogins A
  On A.loginName COLLATE SQL_Latin1_General_CP1_CI_AS = P.name
WHERE p.type_desc IN ('SQL_LOGIN','WINDOWS_GROUP','WINDOWS_LOGIN')
   AND p.name NOT IN ('SA')
   AND p.name NOT LIKE '##%##'
   --AND A.DBName like '%@prefix%'
   ;

