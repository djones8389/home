use icaew_secureassess

select wesirt.*	
	,
	KeyCode
	, warehouseTime
from warehouse_examsessiontable west (READUNCOMMITTED)
inner join warehouse_examsessionitemresponsetable wesirt (READUNCOMMITTED)
on west.id = wesirt.warehouseexamsessionid
where keycode = 'XD7JJTF6'
	(itemid = '368P1000' and itemversion = '134')
	or
	(itemid = '368P1053' and itemversion = '135')
	or
	(itemid = '368P1054' and itemversion = '136')
	or
	(itemid = '368P1055' and itemversion = '137')

--select top 1000 ItemResponseData
--from warehouse_examsessionitemresponsetable (READUNCOMMITTED)
--order by ID DESC