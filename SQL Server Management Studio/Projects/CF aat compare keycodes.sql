if OBJECT_ID('tempdb..#cf') is not null drop table #cf;
if OBJECT_ID('tempdb..#dj') is not null drop table #dj;

create table #cf (
	keycode nvarchar(10)
);
create table #dj (
	keycode nvarchar(10)
);

bulk insert #dj
from 'C:\Users\davej\desktop\DJKeycodes.csv'
with(rowterminator = '\n',fieldterminator =',')

bulk insert #cf
from 'C:\Users\davej\desktop\cfKeycodes.csv'
with(rowterminator = '\n',fieldterminator =',')

select *
from #cf
where keycode not in (
	select keycode
	from #dj
)