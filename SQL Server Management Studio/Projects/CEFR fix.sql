SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

;WITH EXAMS
AS
(
SELECT
       pe.Name 'ComponentName'
       , SPC.FirstName 'FirstName'
       , SPC.LastName 'LastName'
       , SPC.SurpassCandidateRef 'CandidateRef'
	   , SPC.ScheduledPackageCandidateId
	   , SPCE.ShouldIncludeCEFR
	   , pe.IsAdjustComponentScoreBoundariesActivated
	   , CASE WHEN SPCE.IsCompleted = 1 THEN 1 ELSE 0 END 'IsFinished'
       , CASE WHEN SPCE.IsCompleted = 1 AND SPCE.ShouldIncludeScale = 1 AND SPCE.Score IS NOT NULL THEN SPCE.Score else 0 end 'ComponentScaleScore'
       , CASE WHEN SPCE.IsCompleted = 1 AND SPCE.ShouldIncludeScale = 1 AND pe.ScaleValue IS NOT NULL THEN pe.ScaleValue else P.DefaultScaleValue end 'ComponentMaxScaleScore'
       , CASE WHEN SPCE.ShouldIncludeFinalScore = 1 AND SPCE.ShouldIncludeScale = 1 THEN 1 ELSE 0 END 'ShouldIncludeFinalScore'
       , SP.CenterName 'Centre'
       , CONVERT(VARCHAR(10),SPCE.DateCompleted,103) 'Completed'
       , CASE WHEN SPC.IsVoided = 1 THEN 'Voided'
				WHEN SPC.IsCompleted = 1 THEN 'Completed'
				else 'InProgress' END 'State'

	   ,SPC.[DateCompleted]
	   ,SPCE.ScheduledExamRef
	,SP.[ScheduledPackageId] 
	,P.[PackageId] 
	,(CASE WHEN (SPCE.[ScheduledPackageCandidateExamId] IS NULL) THEN CAST(NULL AS int) ELSE 1 END) 'HasSpce'
	,pe.[PackageExamId]
	,ET.[ExamTypeId] 
	,SPCE.[ScheduledPackageCandidateExamId]
   
FROM [dbo].ScheduledPackageCandidateExams AS SPCE
INNER JOIN [dbo].ScheduledPackageCandidates AS SPC ON SPC.ScheduledPackageCandidateId = SPCE.Candidate_ScheduledPackageCandidateId
INNER JOIN [dbo].ScheduledPackages AS SP ON SP.ScheduledPackageId = SPC.ScheduledPackage_ScheduledPackageId
INNER JOIN [dbo].[PackageExams] pe ON pe.PackageExamId = SPCE.[PackageExamId]
INNER JOIN [dbo].[Packages] P ON P.[PackageId] = pe.[PackageId]
INNER JOIN [dbo].ExamTypes ET ON ET.ExamTypeId = pe.ExamTypeId
--where sp.ScheduledPackageID = 40
--where ScheduledExamRef = '7TS35U01'
),
Grammas
AS (
SELECT ScheduledPackageCandidateId, ComponentScaleScore, pe.MagicNumber, pe.MagicScaleIncrease
FROM EXAMS 
INNER JOIN [dbo].[PackageExams] pe ON pe.PackageExamId = EXAMS.[PackageExamId]
WHERE EXAMS.ExamTypeId = 5
),
MagicNumberScores
AS (
SELECT
       EXAMS.ScheduledPackageCandidateExamId
       , CASE WHEN EXAMS.ExamTypeId = 5 OR Grammas.MagicNumber IS NULL OR Grammas.ComponentScaleScore < Grammas.MagicNumber THEN EXAMS.ComponentScaleScore 
	          WHEN (EXAMS.ComponentScaleScore + (EXAMS.ComponentScaleScore * Grammas.MagicScaleIncrease / 100)) > EXAMS.ComponentMaxScaleScore THEN EXAMS.ComponentMaxScaleScore
		 ELSE (EXAMS.ComponentScaleScore + (EXAMS.ComponentScaleScore * Grammas.MagicScaleIncrease / 100)) END 'Score'   
FROM EXAMS
LEFT JOIN Grammas ON Grammas.ScheduledPackageCandidateId = EXAMS.ScheduledPackageCandidateId
),
ExamGrades
AS (
 SELECT EXAMS.ScheduledPackageCandidateExamId, EG.Code, ROUND(MagicNumberScores.Score, 0) Score, 
 EGD.DefaultMinScore, SB.Value AS BoundaryDefaultMinScore, 
 ROW_NUMBER() OVER(PARTITION BY EXAMS.ScheduledPackageCandidateExamId ORDER BY EXAMS.ScheduledPackageCandidateExamId, PG.[Order]) as [Order]
 FROM EXAMS 
 INNER JOIN [dbo].[PackageExams] pe ON pe.PackageExamId = EXAMS.[PackageExamId]
 INNER JOIN [dbo].[PackageGrades] PG ON PG.[PackageId] = pe.[PackageId]
 INNER JOIN [dbo].ExamGrades EG ON EG.Id = PG.ExamGradeId
 INNER JOIN [dbo].ExamGradeDescriptions EGD ON EGD.ExamTypeId = pe.ExamTypeId AND EGD.ExamGradeId = EG.Id
 LEFT JOIN [dbo].PackageExamScoreBoundaries SB ON (SB.PackageExamId = EXAMS.[PackageExamId] AND SB.ExamGradeId = EG.Id AND EXAMS.IsAdjustComponentScoreBoundariesActivated = 1)
 INNER JOIN MagicNumberScores ON MagicNumberScores.ScheduledPackageCandidateExamId = EXAMS.ScheduledPackageCandidateExamId
),
FullExamGrades
AS (

	SELECT EG1.*, EG2.DefaultMinScore DefaultMinScoreNext, EG2.BoundaryDefaultMinScore BoundaryDefaultMinScoreNext
	FROM ExamGrades EG1
	LEFT JOIN ExamGrades EG2 
		ON EG2.ScheduledPackageCandidateExamId = EG1.ScheduledPackageCandidateExamId AND EG2.[Order] = EG1.[Order] + 1
),
ExamCefrs
AS (
	SELECT ScheduledPackageCandidateExamId, Code, Score
	FROM (
			SELECT ScheduledPackageCandidateExamId, Code, Score,
					ROW_NUMBER() OVER(PARTITION BY ScheduledPackageCandidateExamId ORDER BY ScheduledPackageCandidateExamId, [Order]) as num
			FROM FullExamGrades
			WHERE (BoundaryDefaultMinScore IS NOT NULL AND Score >= BoundaryDefaultMinScore AND (Score < BoundaryDefaultMinScoreNext OR BoundaryDefaultMinScoreNext IS NULL)) 
			OR (BoundaryDefaultMinScore IS NULL AND Score >= DefaultMinScore AND (Score < DefaultMinScoreNext OR DefaultMinScoreNext IS NULL))			
		) A
	WHERE num = 1
) ,
PACKS
AS (
SELECT
       EXAMS.ScheduledPackageCandidateId
       , CASE WHEN (SUM(EXAMS.IsFinished) > 0 OR (SPC.IsCompleted = 1 AND SPC.IsVoided = 0)) AND P.ShouldIncludeScale = 1 AND SPC.PackageScore IS NOT NULL THEN SPC.PackageScore else 0 end 'TestPackageScaleScore'
       , CASE WHEN (SUM(EXAMS.IsFinished) > 0  OR (SPC.IsCompleted = 1 AND SPC.IsVoided = 0)) AND P.ShouldIncludeScale = 1 AND SUM(EXAMS.ShouldIncludeFinalScore) > 0 THEN SUM(EXAMS.ComponentMaxScaleScore) else 0 end 'TestPackageMaxScaleScore'

   
FROM EXAMS
INNER JOIN [dbo].ScheduledPackageCandidates AS SPC ON SPC.ScheduledPackageCandidateId = EXAMS.ScheduledPackageCandidateId
INNER JOIN [dbo].ScheduledPackages AS SP ON SP.ScheduledPackageId = SPC.ScheduledPackage_ScheduledPackageId
INNER JOIN [dbo].[Packages] P ON P.[PackageId] = SP.PackageId
GROUP BY EXAMS.ScheduledPackageCandidateId, SPC.IsCompleted, SPC.IsVoided, P.ShouldIncludeScale, SPC.PackageScore
)
,
PACKSGROUPED
AS (
SELECT 'Package' Keycode, ComponentName, FirstName, LastName, CandidateRef, 
'' CEFR,
'' ComponentScaleScore, '' ComponentMaxScaleScore, 
CAST(TestPackageScaleScore as varchar(10)) TestPackageScaleScore, 
CAST(TestPackageMaxScaleScore as varchar(10)) TestPackageMaxScaleScore, 
Centre, Completed, [State],
EXAMS.[DateCompleted], 
EXAMS.[PackageId], 
EXAMS.[ScheduledPackageId], 
EXAMS.[ScheduledPackageCandidateId],  
0 HasSpce, 
0 [PackageExamId], 
0 [ExamTypeId], 
0 [ScheduledPackageCandidateExamId], 
ROW_NUMBER() OVER(PARTITION BY EXAMS.[ScheduledPackageCandidateId] ORDER BY EXAMS.HasSpce ASC, EXAMS.[PackageExamId] ASC, EXAMS.[ExamTypeId] ASC, EXAMS.[ScheduledPackageCandidateExamId] ASC) as num

FROM EXAMS
LEFT JOIN ExamCefrs ON ExamCefrs.ScheduledPackageCandidateExamId = EXAMS.ScheduledPackageCandidateExamId
JOIN PACKS ON PACKS.ScheduledPackageCandidateId = EXAMS.ScheduledPackageCandidateId
--WHERE EXAMS.[State] = 'Completed'
)
,
Results
AS (
SELECT Keycode, ComponentName, FirstName, LastName, CandidateRef, CEFR, ComponentScaleScore, ComponentMaxScaleScore, 
TestPackageScaleScore, TestPackageMaxScaleScore, Centre, Completed, [State], [DateCompleted], 
[PackageId], [ScheduledPackageId], [ScheduledPackageCandidateId],  HasSpce, [PackageExamId], [ExamTypeId], [ScheduledPackageCandidateExamId] 
FROM PACKSGROUPED WHERE num = 1
UNION ALL
SELECT EXAMS.ScheduledExamRef Keycode, ComponentName, FirstName, LastName, CandidateRef, 
CASE
	when (pe.ShouldIncludeCEFR = 0 OR EXAMS.ShouldIncludeCEFR = 0) then ''
	else ExamCefrs.Code	end CEFR,
CAST(ComponentScaleScore as varchar(10)) ComponentScaleScore, 
CAST(ComponentMaxScaleScore as varchar(10)) ComponentMaxScaleScore, 
'' TestPackageScaleScore, '' TestPackageMaxScaleScore, Centre, Completed, [State],
EXAMS.[DateCompleted], 
EXAMS.[PackageId], 
EXAMS.[ScheduledPackageId], 
EXAMS.[ScheduledPackageCandidateId],  
EXAMS.HasSpce, 
EXAMS.[PackageExamId], 
EXAMS.[ExamTypeId], 
EXAMS.[ScheduledPackageCandidateExamId]
FROM EXAMS
JOIN [dbo].[PackageExams] pe ON pe.PackageExamId = EXAMS.[PackageExamId]
LEFT JOIN ExamCefrs ON ExamCefrs.ScheduledPackageCandidateExamId = EXAMS.ScheduledPackageCandidateExamId
JOIN PACKS ON PACKS.ScheduledPackageCandidateId = EXAMS.ScheduledPackageCandidateId
--WHERE EXAMS.[State] = 'Completed'
)



SELECT * 
--into [CEFR].dbo.[CEFR]
FROM Results
ORDER BY 
[DateCompleted] DESC, 
[PackageId] ASC, 
[ScheduledPackageId] ASC, 
[ScheduledPackageCandidateId] ASC,  
HasSpce ASC, 
[PackageExamId] ASC, 
[ExamTypeId] ASC, 
[ScheduledPackageCandidateExamId] ASC
