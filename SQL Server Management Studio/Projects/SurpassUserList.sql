exec sp_msforeachdb '

use [?];

IF(''?'' like ''CPProjectAdmin'' or ''?'' like ''%ContentProducer%'')

select @@ServerName as [ServerName]
	, ''?'' as [DatabaseName]
	, username
	, forename	
	, surname
	, Email
	, CASE UserDisabled WHEN ''0'' THEN ''Active'' ELSE ''Inactive'' END as [Status]
from dbo.UserTable (NOLOCK)
'


exec sp_msforeachdb '

use [?];

if(''?'' LIKE N''%SecureAssess%'')

select @@ServerName as [ServerName]
	, ''?'' as [DatabaseName]
	, Username
	, Forename
	, Surname
	, Email
	, CASE Retired WHEN ''0'' THEN ''Active'' ELSE ''Inactive'' END as [Status]
from Usertable  (NOLOCK)
'