USE AAT_ItemBank

SELECT B.*
	, A.QualificationName
	, A.AssessmentName
	, A.ExternalReference
FROM (	
SELECT QT.QualificationName
	, AssessmentName
	, ExternalReference
	, REPLACE(REPLACE(cast(a.b.query('.') as NVARCHAR(MAX)), '<XPath>@ID="',''), '"</XPath>','') ItemID
FROM AAT_ItemBank.dbo.AssessmentTable AT  (READUNCOMMITTED)
INNER JOIN  AAT_ItemBank.dbo.AssessmentGroupTable AGT (READUNCOMMITTED) ON AGT.ID = AT.AssessmentGroupID
INNER JOIN  AAT_ItemBank.dbo.QualificationTable QT (READUNCOMMITTED) ON QT.ID = AGT.QualificationID
CROSS APPLY AssessmentRules.nodes('PaperRules//XPath') a(b)
where REPLACE(REPLACE(cast(a.b.query('.') as NVARCHAR(MAX)), '<XPath>@ID="',''), '"</XPath>','') not like '%xpath%'
) A
INNER JOIN [430327-AAT-SQL2\SQL2].[AAT_CPProjectAdmin].dbo.Datepicker B
on B.ItemID = A.ItemID;