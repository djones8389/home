--SELECT *
--INTO [Backup_UserQualificationsTable_02122015]
--FROM UserQualificationsTable;

--SELECT *
--INTO [Backup_CentreQualificationsTable_02122015]
--FROM CentreQualificationsTable;

IF OBJECT_ID('tempdb..#changesmade') IS NOT NULL DROP TABLE #changesmade

create table #changesmade
(
	statement nvarchar(MAX)
	, userID int
	, CentreID int
	, qualID int
	--, CQN nvarchar(10)
)

DECLARE @QualTable TABLE (QualID INT)
DECLARE @UserTable TABLE (UserID INT, CentreID INT)

INSERT @QualTable
select ID
from IB3QualificationLookup
where QualificationName in ('Indirect Tax (L3CBE2014)');

INSERT @UserTable
SELECT distinct ut.ID, CT.ID
FROM CENTRETABLE CT
INNER JOIN AssignedUserRolesTable AURT
on AURT.CentreID = CT.ID
INNER JOIN UserTable UT
on UT.ID = AURT.UserID
WHERE CentreName IN ('AAT - Self - Studier (Internal)','AAT - Withdrawn Training Provider Student','Accountancy Learning','Accountancy Training Solutions Limited','Accrington & Rossendale College','Adult Community Learning - Brentwood','Adult Community Learning - Chelmsford','Adult Community Learning - Clacton','Adult Community Learning - Colchester','Adult Community Learning - Harlow','Adult Community Learning - Witham','Al-Ghad Training Institute','All Inclusive Training','Al-Moalem Institute','Alton College','Andover College','Anglia Professional Training','Aplus Training - Exeter College','Ashby Training Limited','Aspire Achieve Advance Ltd - Birmingham','Aspire Achieve Advance Ltd - Chelmsford','Aspire Achieve Advance Ltd - Liverpool','Aspire Achieve Advance Ltd - Milton Keynes','Aspire Achieve Advance Ltd - Norwich','Aspire Achieve Advance Ltd - Watford','Aylesbury College','Babington Group - Derby','Babington Group - Leeds','Babington Group - Lichfield','Babington Group - Newcastle Under Lyme','Babington Group - Nottingham','Babington Group - Sheffield','Bahrain Institute of Banking and Finance','Barking & Dagenham College','Barnsley College','Birmingham Metropolitan College (MB)','Birmingham Metropolitan College (SC)','Birmingham Metropolitan College (ST)','Blackburn College','Bolton College','Boston College','BPP Professional Education - Birmingham','BPP Professional Education - Bristol','BPP Professional Education - DL','BPP Professional Education - Leeds','BPP Professional Education - Liverpool','BPP Professional Education - London','BPP Professional Education - Maidstone','BPP Professional Education - Manchester','BPP Professional Education - Milton Keynes','BPP Professional Education - Reading','Burton and South Derbyshire College','Cambridge Regional College','Canterbury College','Cardiff and Vale College - Barry site','Carshalton College','Cheadle and Marple Sixth Form College','CIPFA Education and Training Centre - Edinburgh','Cirencester College','City of Bristol College - College Green','Coleg Llandrillo','Coleg Menai','Crunch Academy','Darlington College','Distance Learning College & Training','Do not select - AAT Test Centre Do Not attach real students here','Eagle Education and Training','Ealing, Hammersmith and West London College � Ealing College','East Kent College','East Riding College','East Surrey College','EMA Training Limited','Fareham College','First Intuition - Cambridge','First Intuition - Distance Learning','First Intuition - Maidstone','Franklin College','Gold Edge Training Limited','Gower College Swansea','Grimsby Institute of Higher Education','Havering College','Hopwood Hall College','iCount Training - Chester','iCount Training - Manchester','ICS Learn','Ideal Schools','Kaplan Financial - Birmingham','Kaplan Financial - Bristol','Kaplan Financial - Cambridge','Kaplan Financial - Distance Learning Centre','Kaplan Financial - Glasgow','Kaplan Financial - Grimsby','Kaplan Financial - Hull','Kaplan Financial - Leeds','Kaplan Financial - Leicester','Kaplan Financial - Live On Line','Kaplan Financial - Liverpool','Kaplan Financial - London Bridge','Kaplan Financial - London Hammersmith','Kaplan Financial - London Islington','Kaplan Financial - Manchester','Kaplan Financial - Milton Keynes','Kaplan Financial - Newcastle Upon Tyne','Kaplan Financial - Norwich','Kaplan Financial - Nottingham','Kaplan Financial - Reading','Kaplan Financial - Sheffield','Kaplan Financial - Southampton','LAGAT','Lakes College','Merthyr Tydfil College','MGM Training Academy Ltd (Rotherham)','Milton Keynes College','Newcastle Under Lyme College','Nishkam Civic Association','Norfolk Adult Education','North Hertfordshire College','North Lindsey College','North Nottinghamshire College','Oldham College','Open Study College','Performance Through People','Peterborough Regional College','Premier Books Limited t/a Premier Training','Preston College','Pro Institute','Professional Accountancy Tutors','Rankuke Investment (PTY) Limited','Richmond Upon Thames College','Rotherham College - Town Centre Campus','Salford City College','Score Training Institute','SKS Accountancy Training Academy','South & City College Birmingham (SBC)','South Gloucestershire and Stroud College','Southampton City College','Southport College','St Helens College','Strodes College','Sutton College of Learning for Adults (SCOLA)','The Learning Community Ltd','The Training Place of Excellence','Timely Training Solutions (TTS) Ltd','Total People','Training 2000 - Blackburn','Unattached student AAC code','Walsall College','Warrington Collegiate','West Cheshire College','West Suffolk College','Westminster Kingsway College - Victoria','White Horse Training Ltd','Wirral Metropolitan College','YH Training Services Ltd - Doncaster','YH Training Services Ltd - Huddersfield','YH Training Services Ltd - Hull','YH Training Services Ltd - Scarborough','YH Training Services Ltd - York');


BEGIN TRY

    BEGIN TRANSACTION 
	merge into CentreQualificationsTable as TGT
	using (SELECT DISTINCT CentreID,QualID
			FROM @UserTable 
			CROSS APPLY @QualTable 
			) as SRC
		on TGT.QualificationID = SRC.QualID
			and TGT.CentreID = SRC.CentreID
	WHEN NOT MATCHED BY TARGET THEN
	insert(CentreID, QualificationID)
	values(SRC.CentreID, SRC.QualID)
	
	 
	OUTPUT $action, inserted.CentreID, inserted.QualificationID --INTO #changesmade;
		INTO #changesmade([statement], CentreID, QualID);
		
    COMMIT TRANSACTION


    BEGIN TRANSACTION 
		merge into UserQualificationsTable as TGT
		using (SELECT DISTINCT UserID, QualID
				FROM @UserTable
				CROSS APPLY @QualTable
				) as SRC
			on TGT.QualificationID = SRC.QualID
				and TGT.Userid = SRC.UserID
		WHEN NOT MATCHED BY TARGET THEN
		insert(UserID, QualificationID)
		values(SRC.UserID, SRC.QualID)
		
		OUTPUT $action, inserted.UserID, inserted.QualificationID --INTO #changesmade;
		INTO #changesmade([statement], UserID, QualID);
		
    COMMIT TRANSACTION
END TRY

BEGIN CATCH
    SELECT 
        ERROR_NUMBER() AS ErrorNumber
        ,ERROR_SEVERITY() AS ErrorSeverity
        ,ERROR_STATE() AS ErrorState
        ,ERROR_PROCEDURE() AS ErrorProcedure
        ,ERROR_LINE() AS ErrorLine
        ,ERROR_MESSAGE() AS ErrorMessage;

    IF @@TRANCOUNT > 0
        ROLLBACK TRANSACTION;
END CATCH;
IF @@TRANCOUNT > 0
    COMMIT TRANSACTION;
GO




SELECT * FROM #changesmade