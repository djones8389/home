/****** Script for SelectTopNRows command from SSMS  ******/
SELECT  count(*)
	  , datename(MONTH, [RescheduleDate])
  FROM [BritishCouncil_TestPackage].[dbo].[RescheduleDetails]
  where [RescheduleDate] > DATEADD(Month, -7, getDATE())
  group by datename(MONTH, [RescheduleDate])
