declare @centres nvarchar(902) = (select substring(
									(
										select  ',' + cast(CentreKey as varchar(100))
										from DimCentres
										for xml path ('')
									), 2, 5000
									)
									)
declare @subjects nvarchar(902) = (
	select QualificationKey
	from DimQualifications
	where QualificationName = 'The Association of Taxation Technicians'
)

declare @datefrom datetime = '2018-02-27';
declare @dateto datetime = '2018-03-28';

SELECT 
	ES.ExamSessionKey
	,ROW_NUMBER() OVER (PARTITION BY ExamVersionKey ORDER BY ES.UserMarks ASC, ES.ExamSessionKey ASC) N
	,NTILE(3) OVER(PARTITION BY ES.ExamVersionKey ORDER BY ES.UserMarks DESC, ES.ExamSessionKey ASC) Inx
	,ES.CentreKey
	,ES.QualificationKey
	,ES.ExamKey
	,ES.ExamVersionKey
	,ES.UserMarks
	,T.FullDateAlternateKey + ES.CompletionTime  CompletionDateTime
	,ES.Pass
	,CASE WHEN EXISTS (SELECT * FROM FactExamSessionAudits FESA WHERE FESA.ExamSessionKey = ES.ExamSessionKey AND FESA.IsTemp = 0) THEN 1 ELSE 0 END isRescored
FROM	 
	dbo.FactExamSessions AS ES
	INNER JOIN dbo.DimTime AS T
		 ON ES.CompletionDateKey = T.TimeKey
WHERE	 
	(T.FullDateAlternateKey + ES.CompletionTime) BETWEEN @datefrom AND @dateto
	AND	 ES.FinalExamState <> 10
	AND	 ES.ExcludeFromReporting <> 1
	AND	 ES.CentreKey IN (SELECT Value FROM dbo.fn_ParamsToList(@centres,0))
	AND	 ES.QualificationKey IN (SELECT Value FROM dbo.fn_ParamsToList(@subjects,0))
OPTION (MAXRECURSION 0)
