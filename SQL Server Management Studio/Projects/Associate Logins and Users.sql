DECLARE @command1 nvarchar(1000) = '

USE [?];

select ''?''  as DBName
		, u.name as ''User''
		, (select SUSER_SNAME (u.sid)) as ''Login''
		, r.name  as RoleName
		--, u.*
from sys.database_role_members RM 
	right join sys.database_principals U on U.principal_id = RM.member_principal_id
	left join sys.database_principals R on R.principal_id = RM.role_principal_id
where u.type<>''R''
	and U.type = ''S''
	and r.name IS NOT NULL
	and ''?'' not in (''msdb'',''tempdb'',''model'',''master'')
'

DECLARE @UserLogins TABLE (DBName nvarchar(150), userName nvarchar(150), loginName nvarchar(150), RoleName nvarchar(50))
INSERT @UserLogins
EXEC sp_MSforeachdb  @command1

--IF NOT EXISTS Create Login []
--USE [DBName] Create User [] for Login []
--EXEC sp_addrolemember 'RoleName' 'UserName'


SELECT *--'USE [' + DBName + '] CREATE USER [' + userName + '] FOR LOGIN [' + loginName +']; EXEC sp_addrolemember '''+ RoleName +''','''+ userName  +''''
FROM @UserLogins

where username not in ('yuriyb','kevinh') 
order by DBName
--USE [AAT_ContentProducer] CREATE USER [cpuser_aat_r11] FOR LOGIN [cpuser_aat_r11]; EXEC sp_addrolemember 'db_owner','cpuser_aat_r11'