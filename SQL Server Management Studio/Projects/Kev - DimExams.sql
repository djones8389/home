USE STG_SQA2_SecureAssess

SELECT DISTINCT
     WSCET.[ExamID] ExamKey
     ,WSCET.[examName] ExamName
     ,WEST.StructureXML.value('data(/assessmentDetails/assessmentGroupReference)[1]','nVarchar(100)') ExamReference
     ,WEST.StructureXML.value('data(/assessmentDetails/validFromDate)[1]','smalldatetime') ValidFromDate
     ,WEST.StructureXML.value('data(/assessmentDetails/expiryDate)[1]','smalldatetime') ValidToDate 
     ,WSCET.qualificationId QualificationKey 
FROM [dbo].[WAREHOUSE_ScheduledExamsTable] WSCET (NOLOCK)
     INNER JOIN [dbo].[WAREHOUSE_ExamSessionTable] WEST (NOLOCK)   
           ON WEST.WAREHOUSEScheduledExamID = WSCET.ID 

WHERE ExamID = 689

USE STG_SQA2_SurpassDataWarehouse

SELECT * 
FROM DimExams (NOLOCK) 
where ExamKey = 689