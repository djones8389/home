use PRV_WJEC_SecureAssess

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

if OBJECT_ID('tempdb..#AsciiCheck') IS NOT NULL DROP TABLE #AsciiCheck;
IF OBJECT_ID('tempdb..#esidholder')IS NOT NULL DROP TABLE #esidholder;

CREATE TABLE #esidholder (
	esid int
);

INSERT #esidholder
SELECT ID
from WAREHOUSE_ExamSessionTable (NOLOCK)
where warehouseTime > DATEADD(MONTH, -12, GETDATE())
	 AND warehouseExamState = 1 
	 AND PreviousExamState <> 10;
	  
CREATE CLUSTERED INDEX [IX_Clustered] on #esidholder (ESID);

CREATE TABLE #AsciiCheck (
	
	[Character] char(1)
	, [ASCII Val] int
	, esid int 
	, itemID nvarchar(50) 
);


DECLARE myTest CURSOR FOR

SELECT 
	b.c.value('.','nvarchar(500)')
	, esid
	, ItemID
FROM [WAREHOUSE_ExamSessionItemResponseTable] 
INNER JOIN #esidholder A
on a.esid = WAREHOUSEExamSessionID
Cross apply MarkerResponseData.nodes('/entries/entry[userId !=-1]/comment') b(c)
	where b.c.value('.','nvarchar(500)') != '';


DECLARE @String nvarchar(1000), @esid int, @itemID nvarchar(50);

OPEN myTest;

FETCH NEXT FROM myTest INTO @String, @esid , @itemID

WHILE @@FETCH_STATUS = 0

BEGIN
	
		declare @min tinyint = 1
		declare @max int = (select LEN(@String)+1)

		while (@min < @max)

		BEGIN	

			INSERT #AsciiCheck([Character], [ASCII Val], esid, itemID)
			select substring(@String, @min,1), ASCII((SELECT substring(@String, @min,1))), @esid, @itemID
		
			select @min = @min + 1

		END

FETCH NEXT FROM myTest INTO @String, @esid , @itemID

END
CLOSE myTest;
DEALLOCATE myTest;

select * from #AsciiCheck	
	where [ASCII Val] < 32