/****** Script for SelectTopNRows command from SSMS  ******/
SELECT TOP 1000 [ExamSessionKey]
      ,[SEQNO]
      ,[ExamStateId]
      ,[StateChangeDate]
  FROM [SurpassDataWarehouse_Populated].[dbo].[FactExamSessionsStateAudit]
  where ExamSessionKey = 142

--ExamSessionKey	SEQNO	ExamStateId	StateChangeDate
--142	7	11	2014-06-10 11:24:46.047

--UPDATE [FactExamSessionsStateAudit]
--set ExamStateId = 10
--	, StateChangeDate = '2016-10-10 11:24:46.047'
--where ExamSessionKey = 142
--	and SEQNO = 7


DECLARE @StartDate datetime = '01 Oct 2016 00:00:00'
DECLARE @EndDate datetime = '31 Oct 2016 23:59:59'

CREATE TABLE ##Params (
   startDate datetime
	, endDate datetime
);

INSERT ##Params(startDate, endDate)
select @StartDate, @EndDate;

drop table ##Params


exec sp_executesql N'

DECLARE @MYSTRING nvarchar(max) = ''''

SELECT @MYSTRING = ''	

		SELECT ExamSessionKey as [Value]
		FROM [dbo].[FactExamSessionsStateAudit]
		where  CAST(FactExamSessionsStateAudit.StateChangeDate AS DATE) 
			BETWEEN ''''''+CONVERT(VARCHAR(10),StartDate, 120)+'''''' and ''''''+CONVERT(VARCHAR(10),EndDate, 120)+'''''' 
				and ExamStateID IN (6,10)
			
''
from ##Params

SELECT @MYSTRING = REPLACE(@MYSTRING, ''ExamStateID IN (6,10)'', ''ExamStateID IN (6)'')
EXEC(@MYSTRING)

'







/*


exec sp_executesql N'

DECLARE @MYSTRING nvarchar(max) = ''''

SELECT @MYSTRING = ''

SELECT ExamSessionKey as [Value]
FROM [dbo].[FactExamSessionsStateAudit]
where  CAST(FactExamSessionsStateAudit.StateChangeDate AS DATE) 
	BETWEEN CAST(@StartDate AS DATE) AND CAST(@EndDate AS DATE)
		and ExamStateID IN (6,10)''

EXEC(@MYSTRING)
'
,N'@StartDate datetime,@EndDate datetime'

,@StartDate='2016-10-01 00:00:00',@EndDate='2016-10-31 00:00:00'


exec sp_executesql N'

DECLARE @MYSTRING nvarchar(max) = ''''

SELECT @MYSTRING = ''	

		SELECT ExamSessionKey as [Value]
		FROM [dbo].[FactExamSessionsStateAudit]
		where  CAST(FactExamSessionsStateAudit.StateChangeDate AS DATE) 
			BETWEEN ''''''+CONVERT(VARCHAR(10),StartDate, 120)+'''''' and ''''''+CONVERT(VARCHAR(10),EndDate, 120)+'''''' 
				and ExamStateID IN (6,10)
			
''
from ##Params

SELECT @MYSTRING = REPLACE(@MYSTRING, ''ExamStateID IN (6,10)'', ''ExamStateID IN (6)'')
EXEC(@MYSTRING)

'



exec sp_executesql N'

DECLARE @MYSTRING nvarchar(max) = ''''

SELECT @MYSTRING = ''

	SELECT ExamSessionKey as [Value]
	FROM [dbo].[FactExamSessionsStateAudit]
	where  CAST(FactExamSessionsStateAudit.StateChangeDate AS DATE) 
		BETWEEN ''''2016-10-01 00:00:00'''' AND ''''2016-10-31 00:00:00''''
			and ExamStateID IN (6,10)
		''
	SELECT @MYSTRING = REPLACE(@MYSTRING, ''ExamStateID IN (6,10)'', ''ExamStateID IN (6)'')
	EXEC(@MYSTRING)

'
,N'@StartDate datetime,@EndDate datetime'
,@StartDate='2016-10-01 00:00:00',@EndDate='2016-10-31 00:00:00'



*/