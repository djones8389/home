SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

set statistics io on

select 
	candidateRef
	, KeyCode
	, ExternalReference
	, a.Section
	--, a.Item 
	, a.[Question No.]
	, sum(a.mark) mark
	, sum(a.maxmark) maxMark
FROM (
	SELECT
		candidateRef
		, KeyCode
		, ExternalReference
	    , exam.section.value('@id','tinyint') Section
		, section.item.value('@id','nvarchar(20)') Item
		, item.mark.value('@mark','tinyint') mark
		, item.mark.value('@maxMark','tinyint') maxMark
		,DENSE_RANK() OVER (PARTITION BY exam.section.value('@id','tinyint') ORDER BY exam.section.value('@id','tinyint'), section.item.value('@id','nvarchar(20)')) AS [Question No.]
	FROM WAREHOUSE_ExamSessionTable_Shreded
	cross apply resultData.nodes('exam/section') exam(section)
	cross apply exam.section.nodes('item') section(item)
	cross apply section.item.nodes('mark') item(mark)
	--where examSessionId = 162465
	WHERE ExternalReference in ('14M03O106','13M04O120','16M12O205','17M01O206')
				AND warehouseTime > '01 Jan 2016' -- BETWEEN '2017-02-20' AND getDATE()
				AND PreviousExamState != 10
) a
group by 
	candidateRef
	, KeyCode
	, ExternalReference
	, Section
	, item
	, [Question No.]



	/*
	WHERE ExamSessionID IN (
			SELECT examSessionId
			FROM WAREHOUSE_ExamSessionTable_Shreded
			WHERE ExternalReference = '14M03O106'
				AND warehouseTime BETWEEN '2017-04-26'
					AND '2017-05-22'
				AND PreviousExamState != 10
			)
			*/