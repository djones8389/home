Use master;

DECLARE @DefaultLocation TABLE (
     Value NVARCHAR(100)  
     , Data NVARCHAR(100)
) 

INSERT @DefaultLocation
EXEC  master.dbo.xp_instance_regread 
 N'HKEY_LOCAL_MACHINE', N'Software\Microsoft\MSSQLServer\MSSQLServer',N'BackupDirectory'

--DECLARE @Location nvarchar(100) = 'T:\Backup'
DECLARE @Location nvarchar(100) = (SELECT Data from @DefaultLocation)

SELECT N'BACKUP DATABASE ['+name+N'] TO DISK = N''' +  @Location + '\' + NAME + '.' + CONVERT(VARCHAR(10), GETDATE(), 102) + '.bak'' WITH NAME = N'''+name+N'- Database Backup'', COPY_ONLY, NOFORMAT, NOINIT, SKIP, COMPRESSION, NOREWIND, NOUNLOAD, STATS = 10;'  
FROM sys.databases 
where database_id > 4
ORDER BY name;


BACKUP DATABASE [Demo_SecureAssess] TO DISK = N'T:\Backup\Demo_SecureAssess.2018.03.16.bak' 
	WITH NAME = N'Demo_SecureAssess- Database Backup'
	, COPY_ONLY, NOFORMAT, NOINIT, SKIP, COMPRESSION, NOREWIND, NOUNLOAD, STATS = 10;

BACKUP DATABASE [Demo_SecureAssess] TO DISK = N'T:\Backup\Demo_SecureAssess.2018.03.16_DIFF.bak' 
	WITH NAME = N'Demo_SecureAssess- Database Backup'
	, DIFFERENTIAL, COPY_ONLY, NOFORMAT, NOINIT, SKIP, COMPRESSION, NOREWIND, NOUNLOAD, STATS = 10;