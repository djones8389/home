CREATE TABLE #Results (
	[Database] sysname
	, [ExamSessionID] int
	, ExamState tinyint
	, ItemID nvarchar(20)
)

DECLARE @Dynamic nvarchar(MAX)='';

SELECT @Dynamic+=CHAR(13)+ 'use'+ quotename(name)+';

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

SELECT db_name() [Database]
		, est.id [ExamSessionID]
		, ExamState
		, ItemID
FROM ExamSessionTable as EST
CROSS APPLY structurexml.nodes(''assessmentDetails/assessment/section/item'') a(b)

INNER JOIN ExamSessionItemResponseTable AS esirt 
ON est.ID = esirt.ExamSessionID
	and esirt.itemid = a.b.value(''data(@id)[1]'',''nvarchar(20)'')

WHERE MarkerResponseData IS NULL
	and examState in (15,16)
	and a.b.value(''data(@markingType)[1]'',''tinyint'') = 1;'
FROM sys.databases
where name like '%[_]SecureAssess'

INSERT #Results
EXEC(@Dynamic);

SELECT *
FROM #Results
ORDER BY 1