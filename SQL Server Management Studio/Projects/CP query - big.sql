SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

SELECT ProjectName
	, ProjectID
	, PageID
	, SceneID
	, Height
	, PageStatus
	, max(itemCount) [ComponentCount]
FROM (

	SELECT ProjectName
		, ProjectID
		, PageID
		, SceneID
		, Height
		, PageStatus
		, ROW_NUMBER() OVER(PARTITION BY ProjectID, PageID, SceneID ORDER BY FullPageID) as ItemCount
	FROM (
	SELECT SUBSTRING(IG.ID,0,CHARINDEX('P',IG.ID)) [ProjectID]
		, SUBSTRING(SUBSTRING(IG.ID,CHARINDEX('P',IG.ID)+1,LEN(IG.ID)),0,CHARINDEX('S',SUBSTRING(IG.ID,CHARINDEX('P',IG.ID),LEN(IG.ID)))-1) [PageID]
		, SUBSTRING(IG.ID,CHARINDEX('S',IG.ID),2) SceneID
		, IG.ID as FullPageID
		, PL.Name AS ProjectName
		, hei as Height
		, StatusList.Status as PageStatus
	FROM ProjectListTable PL
		CROSS APPLY ProjectStructureXml.nodes('Pro//Pag') s(x)
		INNER JOIN PageTable P
			ON PL.ID = P.ParentID
			AND CAST(pl.ID AS nvarchar(10)) + 'p' + s.x.value('@ID','nvarchar(10)') = p.ID
		INNER JOIN ItemGapfillTable IG
			ON P.ID = SUBSTRING(IG.ID,0,CHARINDEX('S',IG.ID))
		INNER JOIN (
			SELECT ID
				,a.b.value('@val','smallint') [val]
				,a.b.value('.[1]','nvarchar(100)') [Status]
			FROM ProjectListTable
			CROSS APPLY ProjectDefaultXml.nodes('Defaults/StatusList/Status') a(b)
				) StatusList
			ON StatusList.ID = PL.ID
			AND val = s.x.value('@sta','smallint')
		WHERE muL = 1
		AND PL.Name NOT LIKE '%BTL%'
	) A
	GROUP BY ProjectName
		, ProjectID
		, PageID
		, SceneID
		, Height
		, PageStatus
		, FullPageID
) Total


group by
	ProjectName
	, ProjectID
	, PageID
	, SceneID
	, Height
	, PageStatus
order by ProjectID
		, PageID