declare @StartDate datetime = '2017-04-10'
declare @EndDate datetime = '2017-04-12'

SELECT [Instance]
	, [database_name]
	,[CollectionDate]
	, SUM(reserved_kb) reserved_kb
	, SUM([data_kb]) [data_kb]
	, SUM([index_size]) [index_size]
	, SUM([unused_kb]) [unused_kb]
FROM (

SELECT [Instance]
      ,[database_name]
      ,[table_name]
      --,[rows]
      ,sum(cast(replace([reserved_kb], 'KB','') as int)) [reserved_kb]
      ,sum(cast(replace([data_kb], 'KB','') as int))  [data_kb]
      ,sum(cast(replace([index_size], 'KB','') as int)) [index_size]
      ,sum(cast(replace([unused_kb], 'KB','') as int)) [unused_kb]
      ,cast([StartDate] as date) [CollectionDate]
  FROM [PSCollector].[dbo].[DBGrowthMetrics]
  where [Instance] in ('430069-AAT-SQL\SQL1', '430327-AAT-SQL2\SQL2')
	and [StartDate] > @StartDate
	and [StartDate] < @EndDate  
	
	group by [Instance]
      ,[database_name]
      ,[table_name]
      ,[rows]
	  ,cast([StartDate] as date) 
) A
group by [Instance]
	, [database_name]
	,[CollectionDate]

	order by 1