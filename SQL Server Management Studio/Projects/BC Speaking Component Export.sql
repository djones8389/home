SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

IF OBJECT_ID('tempdb..#test') IS NOT NULL DROP TABLE #test;

SELECT --TOP (8000)
	DISTINCT
	wests.examSessionId
	,wests.qualificationName
	, wests.examName
	, wests.Country
	, SUBSTRING(wesdt.itemId, 0, CHARINDEX('S',wesdt.itemId)) itemId	
	, wesirt.ItemMark
	, wests.userMark
	, wests.userPercentage
INTO #test
	--SUM(CAST(DATALENGTH(wesdt.Document) AS BIGINT))/1024/1024 [MB]
FROM dbo.WAREHOUSE_ExamSessionTable_Shreded wests
INNER JOIN [WAREHOUSE_ExamSessionDocumentTable] wesdt
ON wesdt.warehouseExamSessionID = wests.examSessionId
INNER JOIN dbo.WAREHOUSE_ExamSessionItemResponseTable wesirt
ON wesirt.WAREHOUSEExamSessionID = wesdt.warehouseExamSessionID
	AND wesirt.ItemID = SUBSTRING(wesdt.itemId, 0, CHARINDEX('S',wesdt.itemId))
WHERE wests.qualificationName = 'International Comparisons Research'
	--AND wests.examName = 'Speaking'
	--order by newid()

--6652 distinct ESID's
--26258 distinct components

SELECT a.*
	, b.resultData
FROM  #test a
INNER JOIN dbo.WAREHOUSE_ExamSessionTable b
ON a.examSessionId = b.ID
WHERE a.examSessionId = 1263375