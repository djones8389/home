declare @dynamic varchar(MAX) = '';
select @dynamic += CHAR(13) + 'kill ' + convert(varchar(5),spid)
from sys.sysprocesses s
where dbid > 4
	and  db_name(dbid) = 'SandboxWelshGov_AnalyticsManagement'
exec(@dynamic)
go


declare @dynamic varchar(MAX) = '';
select @dynamic += CHAR(13) + 'kill ' + convert(varchar(5),spid)
from sys.sysprocesses s
where dbid > 4
	and  db_name(dbid) = 'SandboxWelshGov_ContentAuthor'
exec(@dynamic)
go


declare @dynamic varchar(MAX) = '';
select @dynamic += CHAR(13) + 'kill ' + convert(varchar(5),spid)
from sys.sysprocesses s
where dbid > 4
	and  db_name(dbid) = 'SandboxWelshGov_ItemBank'
exec(@dynamic)
go


declare @dynamic varchar(MAX) = '';
select @dynamic += CHAR(13) + 'kill ' + convert(varchar(5),spid)
from sys.sysprocesses s
where dbid > 4
	and  db_name(dbid) = 'SandboxWelshGov_SecureAssess'
exec(@dynamic)
go


declare @dynamic varchar(MAX) = '';
select @dynamic += CHAR(13) + 'kill ' + convert(varchar(5),spid)
from sys.sysprocesses s
where dbid > 4
	and  db_name(dbid) = 'SandboxWelshGov_SurpassDataWarehouse'
exec(@dynamic)
go


declare @dynamic varchar(MAX) = '';
select @dynamic += CHAR(13) + 'kill ' + convert(varchar(5),spid)
from sys.sysprocesses s
where dbid > 4
	and  db_name(dbid) = 'SandboxWelshGov_SurpassManagement'
exec(@dynamic)
go





USE [SandboxWelshGov_SecureAssess]; IF NOT EXISTS (SELECT 1 FROM sys.database_principals where name = 'SandboxWelshGov_SecureAssessUser') CREATE USER [SandboxWelshGov_SecureAssessUser]FOR LOGIN [SandboxWelshGov_SecureAssessUser]; exec sp_addrolemember'db_owner', 'SandboxWelshGov_SecureAssessUser' 
USE [SandboxWelshGov_SecureAssess]; IF NOT EXISTS (SELECT 1 FROM sys.database_principals where name = 'SandboxWelshGov_ETLUser') CREATE USER [SandboxWelshGov_ETLUser]FOR LOGIN [SandboxWelshGov_ETLUser]; exec sp_addrolemember'db_datareader', 'SandboxWelshGov_ETLUser' 
USE [SandboxWelshGov_SecureAssess]; IF NOT EXISTS (SELECT 1 FROM sys.database_principals where name = 'Templateempty_ETLUser') CREATE USER [Templateempty_ETLUser]FOR LOGIN [TemplateEmpty_ETLUser]; exec sp_addrolemember'db_datareader', 'Templateempty_ETLUser' 
USE [SandboxWelshGov_ItemBank]; IF NOT EXISTS (SELECT 1 FROM sys.database_principals where name = 'Templateempty_ETLUser') CREATE USER [Templateempty_ETLUser]FOR LOGIN [TemplateEmpty_ETLUser]; exec sp_addrolemember'ETLRole', 'Templateempty_ETLUser' 
USE [SandboxWelshGov_ItemBank]; IF NOT EXISTS (SELECT 1 FROM sys.database_principals where name = 'SandboxWelshGov_ETLUser') CREATE USER [SandboxWelshGov_ETLUser]FOR LOGIN [SandboxWelshGov_ETLUser]; exec sp_addrolemember'ETLRole', 'SandboxWelshGov_ETLUser' 
USE [SandboxWelshGov_ItemBank]; IF NOT EXISTS (SELECT 1 FROM sys.database_principals where name = 'SandboxWelshGov_ItemBankUser') CREATE USER [SandboxWelshGov_ItemBankUser]FOR LOGIN [SandboxWelshGov_ItemBankUser]; exec sp_addrolemember'db_owner', 'SandboxWelshGov_ItemBankUser' 
USE [SandboxWelshGov_ItemBank]; IF NOT EXISTS (SELECT 1 FROM sys.database_principals where name = 'Templateempty_ETLUser') CREATE USER [Templateempty_ETLUser]FOR LOGIN [TemplateEmpty_ETLUser]; exec sp_addrolemember'db_datareader', 'Templateempty_ETLUser' 
USE [SandboxWelshGov_ItemBank]; IF NOT EXISTS (SELECT 1 FROM sys.database_principals where name = 'Templateempty_ETLUser') CREATE USER [Templateempty_ETLUser]FOR LOGIN [TemplateEmpty_ETLUser]; exec sp_addrolemember'db_datawriter', 'Templateempty_ETLUser' 
USE [SandboxWelshGov_ContentAuthor]; IF NOT EXISTS (SELECT 1 FROM sys.database_principals where name = 'SandboxWelshGov_ContentAuthorUser') CREATE USER [SandboxWelshGov_ContentAuthorUser]FOR LOGIN [SandboxWelshGov_ContentAuthorUser]; exec sp_addrolemember'db_owner', 'SandboxWelshGov_ContentAuthorUser' 
USE [SandboxWelshGov_ContentAuthor]; IF NOT EXISTS (SELECT 1 FROM sys.database_principals where name = 'Templateempty_ETLUser') CREATE USER [Templateempty_ETLUser]FOR LOGIN [TemplateEmpty_ETLUser]; exec sp_addrolemember'db_datareader', 'Templateempty_ETLUser' 
USE [SandboxWelshGov_ContentAuthor]; IF NOT EXISTS (SELECT 1 FROM sys.database_principals where name = 'SandboxWelshGov_ETLUser') CREATE USER [SandboxWelshGov_ETLUser]FOR LOGIN [SandboxWelshGov_ETLUser]; exec sp_addrolemember'db_datareader', 'SandboxWelshGov_ETLUser' 
USE [SandboxWelshGov_SurpassManagement]; IF NOT EXISTS (SELECT 1 FROM sys.database_principals where name = 'SandboxWelshGov_SurpassManagementUser') CREATE USER [SandboxWelshGov_SurpassManagementUser]FOR LOGIN [SandboxWelshGov_SurpassManagementUser]; exec sp_addrolemember'db_owner', 'SandboxWelshGov_SurpassManagementUser' 
USE [SandboxWelshGov_SurpassManagement]; IF NOT EXISTS (SELECT 1 FROM sys.database_principals where name = 'Templateempty_ETLUser') CREATE USER [Templateempty_ETLUser]FOR LOGIN [TemplateEmpty_ETLUser]; exec sp_addrolemember'db_datareader', 'Templateempty_ETLUser' 
USE [SandboxWelshGov_SurpassManagement]; IF NOT EXISTS (SELECT 1 FROM sys.database_principals where name = 'SandboxWelshGov_ETLUser') CREATE USER [SandboxWelshGov_ETLUser]FOR LOGIN [SandboxWelshGov_ETLUser]; exec sp_addrolemember'db_datareader', 'SandboxWelshGov_ETLUser' 
USE [SandboxWelshGov_AnalyticsManagement]; IF NOT EXISTS (SELECT 1 FROM sys.database_principals where name = 'cms_user') CREATE USER [cms_user]FOR LOGIN [cms_login]; exec sp_addrolemember'db_owner', 'cms_user' 
USE [SandboxWelshGov_AnalyticsManagement]; IF NOT EXISTS (SELECT 1 FROM sys.database_principals where name = 'SandboxWelshGov_AnalyticsManagementUser') CREATE USER [SandboxWelshGov_AnalyticsManagementUser]FOR LOGIN [SandboxWelshGov_AnalyticsManagementUser]; exec sp_addrolemember'db_owner', 'SandboxWelshGov_AnalyticsManagementUser' 
USE [SandboxWelshGov_SurpassDataWarehouse]; IF NOT EXISTS (SELECT 1 FROM sys.database_principals where name = 'cms_user') CREATE USER [cms_user]FOR LOGIN [cms_login]; exec sp_addrolemember'db_owner', 'cms_user' 
USE [SandboxWelshGov_SurpassDataWarehouse]; IF NOT EXISTS (SELECT 1 FROM sys.database_principals where name = 'SandboxWelshGov_ETLUser') CREATE USER [SandboxWelshGov_ETLUser]FOR LOGIN [SandboxWelshGov_ETLUser]; exec sp_addrolemember'db_owner', 'SandboxWelshGov_ETLUser' 