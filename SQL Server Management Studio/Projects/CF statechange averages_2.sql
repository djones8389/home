SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

use OCR_SecureAssess

	SELECT STATE1.ExamSessionID
		--, STATE1.NewState
	    , STATE1.StateChangeDate [State1]
		, State2.StateChangeDate [State2]
		--, State2.NewState
		--, SCET.ActiveStartTime
		--, cast(DATEADD(minute, ActiveStartTime, 0) AS TIME(0)) AS [StartTime]
		, DATEADD(DAY, -(AdvanceContentDownloadTimespanInHours/24), DATEADD(MINUTE, ActiveStartTime, ScheduledStartDateTime)) [ShouldUnlock]
		--, SCET.ActiveEndTime
		, SCET.ScheduledStartDateTime
		, SCET.ScheduledEndDateTime
		, daysinadvancetounlock
		, unlockForWholeDay
		, (DATEDIFF(SECOND,isnull(STATE1.StateChangeDate,0), isnull(STATE2.StateChangeDate,0))) [NoOfSecondsToChangeState]
	FROM (
	SELECT [ExamSessionID]
		  ,[NewState]
		  ,[StateChangeDate]
	FROM [dbo].[ExamStateChangeAuditTable]
	where NewState = 1
	) STATE1
	INNER JOIN (
	SELECT  [ExamSessionID]
		  ,[NewState]
		  ,[StateChangeDate]
	FROM [dbo].[ExamStateChangeAuditTable]
	where NewState = 2
	) STATE2
	on State1.ExamSessionID = State2.ExamSessionID
	INNER JOIN ExamSessionTable EST 
	ON EST.ID = State1.ExamSessionID	
	INNER JOIN ScheduledExamsTable SCET
	ON SCET.ID = EST.ScheduledExamID

	order by (DATEDIFF(SECOND,isnull(STATE1.StateChangeDate,0), isnull(STATE2.StateChangeDate,0))) desc;

	--where DATEDIFF(MINUTE, State2.StateChangeDate, DATEADD(DAY, -(AdvanceContentDownloadTimespanInHours/24), DATEADD(MINUTE, ActiveStartTime, ScheduledStartDateTime))) > 2

	--where State1.ExamSessionID = 3844

	SELECT *
	FROM ExamStateChangeAuditTable
	where ExamSessionID in (232734,232785,232786)
		and NewState in (1,2,3,4)