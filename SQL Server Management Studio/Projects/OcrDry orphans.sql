USE [OCRDRY_AnalyticsManagement]DROP USER [OCR_AnalyticsManagementUser];
USE [OCRDRY_ContentAuthor]DROP USER [OCR_ContentAuthorUser];
USE [OCRDRY_ContentAuthor]DROP USER [OCR_ETLUser];
USE [OCRDRY_SurpassDataWarehouse]DROP USER [OCR_ETLUser];
USE [OCRDRY_SurpassManagement]DROP USER [OCR_ETLUser];
USE [OCRDRY_SurpassManagement]DROP USER [OCR_SurpassManagementUser];

USE [ocrdry_AnalyticsManagement];  --needed

CREATE USER [ocrdry_AnalyticsManagementUser] FOR LOGIN [ocrdry_AnalyticsManagementUser]

exec sp_addrolemember 'db_owner','ocrdry_AnalyticsManagementUser'


USE [ocrdry_ContentAuthor];  --needed

CREATE USER [ocrdry_ContentAuthorUser] FOR LOGIN [ocrdry_ContentAuthorUser]
CREATE USER [ocrdry_ETLUser] FOR LOGIN [ocrdry_ETLUser]

exec sp_addrolemember 'db_owner','ocrdry_ContentAuthorUser'
exec sp_addrolemember 'db_datareader', 'ocrdry_ETLUser'


USE [ocrdry_ItemBank]; --not needed

CREATE USER [ocrdry_ItemBankUser] FOR LOGIN [ocrdry_ItemBankUser]
CREATE USER [ocrdry_ETLUser] FOR LOGIN [ocrdry_ETLUser]

--exec sp_addrolemember 'db_owner','ocrdry_ItemBankUser'
--exec sp_addrolemember 'ETLRole','ocrdry_ETLUser'
exec sp_addrolemember 'db_datawriter','ocrdry_ETLUser'
exec sp_addrolemember 'db_datareader','ocrdry_ETLUser'

USE [ocrdry_SecureAssess]; --needed

CREATE USER [ocrdry_SecureAssessUser] FOR LOGIN [ocrdry_SecureAssessUser]
CREATE USER [ocrdry_ETLUser] FOR LOGIN [ocrdry_ETLUser]

exec sp_addrolemember 'db_owner','ocrdry_SecureAssessUser'
exec sp_addrolemember 'db_datareader','ocrdry_ETLUser'

USE [ocrdry_SurpassDataWarehouse];  --needed

CREATE USER [ocrdry_ETLUser] FOR LOGIN [ocrdry_ETLUser]

exec sp_addrolemember 'db_owner','ocrdry_ETLUser'

USE [ocrdry_SurpassManagement]; --needed

CREATE USER [ocrdry_SurpassManagementUser] FOR LOGIN [ocrdry_SurpassManagementUser]
CREATE USER [ocrdry_ETLUser] FOR LOGIN [ocrdry_ETLUser]
CREATE USER [ocrdry_SecureAssessUser] FOR LOGIN [ocrdry_SecureAssessUser]

		exec sp_addrolemember 'db_owner','ocrdry_SurpassManagementUser'
		exec sp_addrolemember 'db_owner','ocrdry_SecureAssessUser'
		exec sp_addrolemember 'db_datareader','ocrdry_ETLUser'