USE [master];
GO

CREATE PROCEDURE dbo.usp_mapNetworkDrive
AS
BEGIN
       EXEC XP_CMDSHELL 'net use S: /delete';
       EXEC XP_CMDSHELL 'net use S: \\pre-prod-web-1\shared /persistent:yes';
       EXEC XP_CMDSHELL 'net use';
END
GO

EXEC sp_procoption @ProcName = 'usp_mapNetworkDrive'   
    ,@OptionName = 'startup'   
    ,@OptionValue = 'on';
GO
