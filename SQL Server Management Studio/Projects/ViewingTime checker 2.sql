IF OBJECT_ID('tempdb..##DATALENGTH') IS NOT NULL DROP TABLE ##DATALENGTH;

CREATE TABLE ##DATALENGTH (
	client nvarchar(1000)
	, ExamSessionID int
	, KeyCode nvarchar(10)
	, [Exam Duration] int
	, ItemID nvarchar(20)
	, [Item ViewingTime] int
	, examName nvarchar(200)
	, ExternalReference nvarchar(200)
	, warehouseTime datetime
);

DECLARE @dynamic nvarchar(MAX)='';

select @dynamic +=CHAR(13)+ '
use '+QUOTENAME([NAME])+'

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

INSERT ##DATALENGTH
SELECT db_name() CLient
	,WEST.ID [ExamSessionID]
	, wests.KeyCode [Keycode]
	, West.Duration [Exam Duration]
	, west.itemID [ItemID]	
	, west.viewingTime [Item ViewingTime]
	, wests.examName
	, wests.ExternalReference
	, wests.warehouseTime
FROM (
	  SELECT D.*
	  FROM (
			SELECT C.ID
				, C.itemID
				, C.[Duration]
				, MAX(viewingTime) viewingTime
				FROM (
				SELECT ID
					, a.b.value(''@id'',''nvarchar(100)'') itemID
					, StructureXML.value(''data(assessmentDetails/scheduledDuration/value/text())[1]'',''bigint'') [Duration]
					, a.b.value(''@viewingTime'',''bigint'')/1000/60 viewingTime
				FROM WAREHOUSE_ExamSessionTable
				cross apply resultData.nodes(''exam/section/item'') a(b)
			) C
			group by C.ID
				, itemID
				, [Duration]
		) D
		where viewingTime > Duration
) WEST
INNER JOIN WAREHOUSE_ExamSessionTable_Shreded WESTS
ON WESTS.examSessionId = WEST.ID

ORDER BY wests.warehouseTime DESC;
'
from sys.databases
where state_desc = 'ONLINE'
	and name like '%[_]SecureAssess';

exec(@dynamic);



select *
from ##DATALENGTH
order by 1






