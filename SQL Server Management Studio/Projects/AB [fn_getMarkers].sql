USE [SurpassLocal3]
GO

/****** Object:  UserDefinedFunction [dbo].[fn_getMarkers]    Script Date: 18/07/2017 15:33:02 ******/
DROP FUNCTION [dbo].[fn_getMarkers]
GO

/****** Object:  UserDefinedFunction [dbo].[fn_getMarkers]    Script Date: 18/07/2017 15:33:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[fn_getMarkers] (@examSessionId INT)
RETURNS nvarchar(max)
AS
BEGIN

	-- filter out exams that have not been human marked first
	IF	(
			(SELECT COUNT(*) FROM  [WAREHOUSE_ExamSessionItemResponseTable]
			WHERE Convert(nvarchar(max),[MarkerResponseData].query('count(/entries/entry/userId)')) != '0'
			AND [WAREHOUSEExamSessionId] = @examSessionId)
			= 0
		)
		BEGIN
			RETURN 'C'
		END

	DECLARE @Return nvarchar(max)

	SELECT @Return = COALESCE(@Return + '</id><id>', '<markers><id>') + CONVERT(varchar(max),[MarkerResponseData].query('/entries/entry[last()]/userId/text()'))
	FROM [WAREHOUSE_ExamSessionItemResponseTable]
	WHERE [WAREHOUSEExamSessionID] = @examSessionId
	AND Convert(nvarchar(max),[MarkerResponseData].query('count(/entries/entry/userId)')) != '0'

	SET @Return = @Return + '</id></markers>'

	DECLARE @myMarkersXml	xml
	SET @myMarkersXml = @Return

	SET @Return = NULL
 
	SELECT @Return=coalesce(@Return + ', ', '') + Username
	FROM [UserTable]
		INNER JOIN
	(SELECT DISTINCT
	ParamValues.ID.value('.','int') as 'id'
	FROM @myMarkersXml.nodes('/markers/id') as ParamValues(ID)) AS dMarkerIds
		ON dMarkerIds.id = [UserTable].[id]

	RETURN (@Return)
END
GO


