/****** Script for SelectTopNRows command from SSMS  ******/
SELECT CASE 
	   WHEN Server_Name = '343553-WEB5' then 'AQA'
   	   WHEN Server_Name = '408302-AQAREPOR' then 'AQA'
	   WHEN Server_Name = '430069-AAT-SQL\SQL1' then 'AAT'
	   WHEN Server_Name = '430327-AAT-SQL2\SQL2' then 'AAT'
	   WHEN Server_Name = '403252-ACTVARIE' then 'Actuaries'
	   WHEN Server_Name = '430088-BC-SQL\SQL1' then 'BritishCouncil'
	   WHEN Server_Name = '430326-BC-SQL2\SQL2' then 'BritishCouncil'
	   WHEN Server_Name = '420304-EAL2-LIV' then 'EAL'
	   WHEN Server_Name = '553092-PRDSQL' then 'Editions Live'
	   WHEN Server_Name = '524778-SQL' then 'Editios Mirror'
	   WHEN Server_Name = '407710-WEB3' then 'NCFE'
	   WHEN Server_Name = '425732-OCR-SQL\SQL1' then 'OCR'
	   WHEN Server_Name = '430325-OCR-SQL2\SQL2' then 'OCR'
	   WHEN Server_Name = '704276-PREVSTA1' then 'PRP-PRV'
	   WHEN Server_Name = '338301-WEB4' then 'Skillsfirst'
	   WHEN Server_Name = '335657-APP1' then 'SQA'
	   WHEN Server_Name = '335658-APP2' then 'SQA'
	   WHEN Server_Name = '311619-WEB3' then 'WJEC'
		    ELSE 'Server_Name'
		END AS 'Server_Name'
      ,[database_Name]
      ,[user_forename]
      ,[user_surname]
      ,[user_username]
      ,[user_disabled]
      ,[user_email]
  FROM [PSCollector].[dbo].[AllSurpassUsers]
  
  where 
  (
      (
	    user_forename = 'fathima'
			and 
		 user_surname = 'Ismail'
		)
	or user_email = 'fathima.ismail@btl.com'
  )
  
  
	OR
	(
	(user_forename = 'Sean'
			and user_surname = 'Greenwood'
		)
	or user_email = 'sean.Greenwood@btl.com'
	
	
	)
	
	order by 3,1