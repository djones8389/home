DECLARE @DiskSpace TABLE (
	Drive CHAR(1)
	,Space INT
	)

INSERT @Diskspace
EXEC master..xp_fixeddrives

DECLARE @DefaultData TABLE (
	Value NVARCHAR(50)
	,Location NVARCHAR(50)
	)
	


INSERT @DefaultData
EXECUTE xp_instance_regread N'HKEY_LOCAL_MACHINE',N'Software\Microsoft\MSSQLServer\MSSQLServer',N'DefaultData'

Declare @Dynamic NVARCHAR(MAX) = '';

if(select value from @DefaultData) IS NULL  Select 'Default Locations need defining!!!'  

--INSERT @DefaultData
--Values('DriveLetter','D:\')			

	SELECT @Dynamic += CHAR(13) + 
	 
		--B.name AS TableName
		--, C.name AS IndexName
		--, C.fill_factor AS IndexFillFactor
		--, D.rows AS RowsCount
		--A.avg_fragmentation_in_percent
		--, A.page_count
		'ALTER INDEX ' + C.NAME + ' ON ' + 'dbo' + '.' + B.NAME + ' REBUILD;'
		--, (E.used_page_count * 8) IndexInKB
		--,(E.used_page_count * 8) / 1024 IndexInMB
	FROM sys.dm_db_index_physical_stats(DB_ID(), NULL, NULL, NULL, NULL) A
	INNER JOIN sys.objects B ON A.object_id = B.object_id
	INNER JOIN sys.indexes C ON B.object_id = C.object_id
		AND A.index_id = C.index_id
	INNER JOIN sys.partitions D ON B.object_id = D.object_id
		AND A.index_id = D.index_id
	INNER JOIN sys.dm_db_partition_stats E ON E.object_id = b.object_id
		AND e.index_id = c.index_id
	WHERE C.index_id > 0
		AND A.avg_fragmentation_in_percent > 30
		AND ((E.used_page_count * 8) / 1024) < (
			SELECT (
					SELECT [SPACE]
					FROM @Diskspace
					WHERE Drive = (
							SELECT SUBSTRING(Location, 0, CHARINDEX(':', Location))
							FROM @DefaultData
							)
					) - 5120 --5GB buffer
			)
	ORDER BY E.used_page_count DESC


	PRINT(@Dynamic);