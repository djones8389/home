USE [PRV_BritishCouncil_SurpassDataWarehouse_2]

declare @ExamVersionKey nvarchar(4)
	,@CentreKey nvarchar(3)
	,@StartDate datetime
	,@EndDate datetime
SET @ExamVersionKey=N'1364'
SET @CentreKey=N'113']
SET @StartDate='24/01/2016 00:00:00'
SET @EndDate='25/04/2016 00:00:00'		

--select * from DimExamVersions where ExamVersionKey = 1364
--select * from DimCentres where CentreKey = 113

DECLARE @Examversions table(ExamVersionKey int)--PRIMARY KEY CLUSTERED - produces lower elapsed time but higher CPU
DECLARE @Centres table(CentreKey int )--PRIMARY KEY CLUSTERED

INSERT INTO @Examversions
SELECT value FROM dbo.fn_ParamsToList(@ExamVersionKey, 0) OPTION(MAXRECURSION 0)


INSERT INTO @Centres
SELECT value FROM dbo.fn_ParamsToList(@CentreKey, 0) OPTION(MAXRECURSION 0)

--;WITH ES AS (
	SELECT 
		distinct DQu.CPID
		
	FROM
		FactExamSessions FES
		JOIN DimTime DT ON FES.CompletionDateKey = DT.TimeKey
		JOIN @Examversions EV ON FES.ExamVersionKey = EV.ExamVersionKey
		JOIN @Centres C ON FES.CentreKey = C.CentreKey
		JOIN FactQuestionResponses FQR	ON    FQR.ExamSessionKey = FES.ExamSessionKey
		JOIN DimQuestions DQu 	ON    DQu.CPID = FQR.CPID AND 
			  DQu.CPVersion = FQR.CPVersion
	WHERE 
		FES.FinalExamState<>10
		AND DT.FullDateAlternateKey BETWEEN @StartDate AND @EndDate
		and FES.ExamSessionKey in (1451059,1450693,1450791,1450838,1450864,1451086,1450656,1450700,1450988,1451110,1450620,1450674,1450696,1451201,1451014,1451044,1451104,1451108,1450901,1450910,1450931,1450980,1451002,1450852,1450868,1450872,1450894,1450795,1450826,1450830,1450707,1450717,1450725,1450730,1450774)



