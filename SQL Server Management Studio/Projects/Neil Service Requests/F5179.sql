/*
5.	For each R11 and R12 client the number of exams taken per day January, for each day since they started using the system
*/



use master;

IF OBJECT_ID ('tempdb..#Exams') IS NOT NULL DROP TABLE #Exams;

CREATE TABLE #Exams (
	
	ServerName nvarchar(250)
	,DBName nvarchar(250)
	, CalendarYear smallint
	, MonthNumberOfYear tinyint
	, DayNumberOfMonth tinyint
	, ExamCounter int

)

INSERT #Exams
exec sp_msforeachdb '

use [?];

if(''?'' like ''%SurpassDataWarehouse%'' or ''?'' like ''%SurpassAnalytics'')

BEGIN
	SELECT @@SERVERNAME ServerName,
			DB_NAME() DBName,
			DimTime.CalendarYear,
			DimTime.MonthNumberOfYear,
			DimTime.DayNumberOfMonth,
			COUNT(FactExamSessions.ExamSessionKey) AS [CountofExams]
	FROM dbo.FactExamSessions
	INNER JOIN dbo.DimTime
	ON FactExamSessions.CompletionDateKey = DimTime.TimeKey
	INNER JOIN [dbo].[FactExamSessionsStateAudit]
	ON [dbo].[FactExamSessionsStateAudit].ExamSessionKey = FactExamSessions.ExamSessionKey
	where ExamStateId = 6
	GROUP BY DimTime.CalendarYear,
					DimTime.MonthNumberOfYear,
					DimTime.DayNumberOfMonth
	ORDER BY DimTime.CalendarYear,
					DimTime.MonthNumberOfYear,
					DimTime.DayNumberOfMonth;
END

'
SELECT *
FROM #Exams