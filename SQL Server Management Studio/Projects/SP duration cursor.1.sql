
        
            if OBJECT_ID ('tempdb..#fileholder') is not null drop table #fileholder;

            declare @fileholder table (
	            [Path] nvarchar(200)
	            , [depth] tinyint
	            , [file] tinyint
            );

            INSERT @fileholder
            EXEC master.sys.xp_dirtree 'D:\',1,1;

            SELECT [path]
            into #fileholder
            FROM @fileholder
            where [file] = 1
	            and RIGHT([path],4) = '.trc';
	
            DECLARE spDurations CURSOR FOR
            select
            'select ''' +
               path + '''
	            , ObjectName
	            , Duration
            from ::fn_trace_gettable(''' + 'D:\' + path + ''',1)'
            from #fileholder;

            DECLARE @Path nvarchar(200);

            OPEN spDurations;

            FETCH NEXT FROM spDurations INTO @Path;

            WHILE @@FETCH_STATUS = 0

            BEGIN

	            EXEC(@Path);
	            FETCH NEXT FROM spDurations INTO @Path;
            END

            CLOSE spDurations;
            DEALLOCATE spDurations;