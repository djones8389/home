SELECT A.Instance
	, [Date]
	, SUM(Whole_Database_KB)/1024/1024 [GB]
FROM (
select Instance
	, database_name
	, cast(startDate as date) [Date]
	, sum((cast(replace([data_kb], 'KB','') as float) + cast(replace([index_size], 'KB','') as float))) Whole_Database_KB
from DBGrowthMetrics
where Instance = 'PRD-SQL-1'
	and database_name <> 'Connect_SecureAssess'
	--and cast(startDate as date) in ('2018-05-14','2018-05-08','2018-04-30')
	group by Instance
		, database_name
		, cast(startDate as date)
) A

group by  A.Instance, [Date]
order by 2

--5 week = 27GB normal clients = ~5GB per week


--1146.88 gb free at the moment


/*

per week:

welsh gov	80.82
others		5

call it 85gb per week

13.48235294117647 weeks

*/