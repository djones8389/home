-- Enable advanced options.  
sp_configure 'show advanced options', 1 ;  
GO  
RECONFIGURE ;  
GO  
-- Enable EKM provider  
sp_configure 'EKM provider enabled', 1 ;  
GO  
RECONFIGURE ;  
GO  
-- Create a cryptographic provider, which we have chosen to call "AzureKeyVault_EKM_Prov," based on an EKM provider  

CREATE CRYPTOGRAPHIC PROVIDER AzureKeyVault_EKM_Prov   
FROM FILE = 'C:\Program Files\SQL Server Connector for Microsoft Azure Key Vault\Microsoft.AzureKeyVaultService.EKM.dll' ;  
GO  

-- Create a credential that will be used by system administrators.  
CREATE CREDENTIAL sa_ekm_tde_cred   
WITH IDENTITY = 'DJKeyVault123',   
SECRET = '28d2357cb89b47478e8f4d309352dea9SECRET_DBEngine'   
FOR CRYPTOGRAPHIC PROVIDER AzureKeyVault_EKM_Prov ;  
GO  

-- Add the credential to a high privileged user such as your   
-- own domain login in the format [DOMAIN\login].  
ALTER LOGIN [SALTSWHARF\DaveJ]
ADD CREDENTIAL sa_ekm_tde_cred ;  
GO  

-- create an asymmetric key stored inside the EKM provider  
USE master ;  
GO  

CREATE ASYMMETRIC KEY [ekm_login_key]
FROM PROVIDER [AzureKeyVault_EKM_Prov]  
WITH PROVIDER_KEY_NAME = 'DJKeyVault123',   
CREATION_DISPOSITION = OPEN_EXISTING   
GO  

 

-- Create a credential that will be used by the Database Engine.  
CREATE CREDENTIAL ekm_tde_cred   
WITH IDENTITY = 'Identity2'   
, SECRET = 'jeksi84&sLksi01@s'   
FOR CRYPTOGRAPHIC PROVIDER AzureKeyVault_EKM_Prov ;  

-- Add a login used by TDE, and add the new credential to the login.  
CREATE LOGIN EKM_Login   
FROM ASYMMETRIC KEY ekm_login_key ;  
GO  
ALTER LOGIN EKM_Login   
ADD CREDENTIAL ekm_tde_cred ;  
GO  

-- Create the database encryption key that will be used for TDE.  
USE AdventureWorks2012 ;  
GO  
CREATE DATABASE ENCRYPTION KEY  
WITH ALGORITHM  = AES_128  
ENCRYPTION BY SERVER ASYMMETRIC KEY ekm_login_key ;  
GO  

-- Alter the database to enable transparent data encryption.  
ALTER DATABASE AdventureWorks2012   
SET ENCRYPTION ON ;  
GO  