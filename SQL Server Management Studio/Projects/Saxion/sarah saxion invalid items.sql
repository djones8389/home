use testdeploy3_itembank

SELECT DISTINCT 
	a.[ProjectID]
	, Title
	, QualificationName
	, AssessmentName
	, ExternalReference
	, count(DISTINCT ItemID) [Count]
FROM (
SELECT distinct 
	SUBSTRING(REPLACE(REPLACE(cast(a.b.query('.') as NVARCHAR(MAX)), '<XPath>@ID="',''), '"</XPath>',''), 0, CHARINDEX('P', REPLACE(REPLACE(cast(a.b.query('.') as NVARCHAR(MAX)), '<XPath>@ID="',''), '"</XPath>',''))) as [ProjectID]
	, REPLACE(REPLACE(cast(a.b.query('.') as NVARCHAR(MAX)), '<XPath>@ID="',''), '"</XPath>','') ItemID
	, QualificationName
	, AssessmentName
	, ExternalReference
FROM AssessmentTable AT (READUNCOMMITTED)
		
CROSS APPLY AssessmentRules.nodes('PaperRules//XPath') a(b)

INNER JOIN AssessmentGroupTable AGT 
on AGT.ID = AT.AssessmentGroupID

INNER JOIN QualificationTable QT
ON QT.ID = AGT.QualificationID

where AT.IsValid = 0
	and REPLACE(REPLACE(cast(a.b.query('.') as NVARCHAR(MAX)), '<XPath>@ID="',''), '"</XPath>','') not like '<XPath>%'
	and SUBSTRING(REPLACE(REPLACE(cast(a.b.query('.') as NVARCHAR(MAX)), '<XPath>@ID="',''), '"</XPath>',''), 0, CHARINDEX('P', REPLACE(REPLACE(cast(a.b.query('.') as NVARCHAR(MAX)), '<XPath>@ID="',''), '"</XPath>',''))) < 5000
) A

INNER JOIN (
	select distinct projectid, Title
	from [TestDeploy3_ContentAuthor].dbo.Subjects (READUNCOMMITTED)
	where ProjectId in (2557) --381,2651,2599,2571,2579,2645,2646,2601,2577,2516,2588,2627,2655,2628,2647,2634,2650,2578,2585,2557)
) B
on a.[ProjectID] = b.ProjectId
group by a.[ProjectID]
	, Title
	, QualificationName
	, AssessmentName
	, ExternalReference
order by 6 desc;