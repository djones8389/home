--use [SANDBOX_Saxion_Current_ItemBank]
select *
	--, ROW_NUMBER() OVER (
	--	partition by QualificationName
	--, AssessmentName
	--, ExternalReference
	--, publishstatus 
	--	order by QualificationName
	--, AssessmentName
	--, ExternalReference
	--, publishstatus ) R
FROM (

SELECT distinct 
	QualificationName
	, AssessmentName
	, ExternalReference
	, REPLACE(REPLACE(cast(a.b.query('.') as NVARCHAR(MAX)), '<XPath>@ID="',''), '"</XPath>','') ItemID
FROM AssessmentTable AT (READUNCOMMITTED)
		
CROSS APPLY AssessmentRules.nodes('PaperRules//XPath') a(b)

INNER JOIN AssessmentGroupTable AGT 
on AGT.ID = AT.AssessmentGroupID

INNER JOIN QualificationTable QT
ON QT.ID = AGT.QualificationID

where at.IsValid = 0
	and at.assessmentstatus = 2
	and REPLACE(REPLACE(cast(a.b.query('.') as NVARCHAR(MAX)), '<XPath>@ID="',''), '"</XPath>','') not like '<XPath>%'
	and SUBSTRING(REPLACE(REPLACE(cast(a.b.query('.') as NVARCHAR(MAX)), '<XPath>@ID="',''), '"</XPath>',''), 0, CHARINDEX('P', REPLACE(REPLACE(cast(a.b.query('.') as NVARCHAR(MAX)), '<XPath>@ID="',''), '"</XPath>',''))) = 2562

) A
inner join (
select ProjectId
		--, ContentProducerItemId
		, s.Title
		, i.PublishStatus
		, i.id
		, case 
			when cast(ContentProducerItemId as nvarchar(20)) is null 
				then cast(ProjectId as varchar(20)) + 'p' + cast(i.Id as varchar(20))
				else  cast(ProjectId as varchar(20)) + 'p' + cast(ContentProducerItemId as nvarchar(20))
				end as ContentProducerItemId
from TestDeploy3_ContentAuthor.dbo.Items i
inner join TestDeploy3_ContentAuthor.dbo.Subjects S
on S.id = i.subjectid
where ProjectId in (2562)
	and PublishStatus <> 0 
	--and ContentProducerItemId is not null
) b
on a.itemid = b.ContentProducerItemId




select ProjectId,  case 
			when cast(ContentProducerItemId as nvarchar(20)) is null 
				then cast(ProjectId as varchar(20)) + 'p' + cast(i.Id as varchar(20))
				else  cast(ProjectId as varchar(20)) + 'p' + cast(ContentProducerItemId as nvarchar(20))
				end as ContentProducerItemId, s.Title, (cast(ProjectId as varchar(10)) + 'P' + cast(ContentProducerItemId as varchar(10))) ItemID, i.PublishStatus, i.id
from TestDeploy3_ContentAuthor.dbo.Items i
inner join TestDeploy3_ContentAuthor.dbo.Subjects S
on S.id = i.subjectid
where ProjectId in (2562)
	--and ContentProducerItemId is not null
	--and PublishStatus = 0
	order by 2