SELECT C.ProjectID
	, C.ItemID
	, C.URL
	, C.R
FROM (
	SELECT B.*
		,'https://testdeploy3.btlsurpass.com/#ItemAuthoring/Subject/'+cast([subjectid] as varchar(10))+'/Item/Edit/'+cast([ItemAuthoringID]  as varchar(10))+ '' [URL]
	FROM (
		SELECT A.*
			, ROW_NUMBER() OVER(PARTITION BY [ProjectID] ORDER BY ItemID) R
		FROM (
		SELECT distinct 
			SUBSTRING(REPLACE(REPLACE(cast(a.b.query('.') as NVARCHAR(MAX)), '<XPath>@ID="',''), '"</XPath>',''), 0, CHARINDEX('P', REPLACE(REPLACE(cast(a.b.query('.') as NVARCHAR(MAX)), '<XPath>@ID="',''), '"</XPath>',''))) as [ProjectID]
			, REPLACE(REPLACE(cast(a.b.query('.') as NVARCHAR(MAX)), '<XPath>@ID="',''), '"</XPath>','') ItemID
		FROM AssessmentTable AT (READUNCOMMITTED)
		
		CROSS APPLY AssessmentRules.nodes('PaperRules//XPath') a(b)

		where IsValid = 0
			and REPLACE(REPLACE(cast(a.b.query('.') as NVARCHAR(MAX)), '<XPath>@ID="',''), '"</XPath>','') not like '<XPath>%'
			and SUBSTRING(REPLACE(REPLACE(cast(a.b.query('.') as NVARCHAR(MAX)), '<XPath>@ID="',''), '"</XPath>',''), 0, CHARINDEX('P', REPLACE(REPLACE(cast(a.b.query('.') as NVARCHAR(MAX)), '<XPath>@ID="',''), '"</XPath>',''))) < 5000
		) A
	) B
	LEFT JOIN (

		select ProjectId, ContentProducerItemId, s.Title, (cast(ProjectId as varchar(10)) + 'P' + cast(ContentProducerItemId as varchar(10))) ItemID, i.PublishStatus, i.id as ItemAuthoringID, s.id [subjectid]
		from Saxion_ContentAuthor.dbo.Items i (READUNCOMMITTED)
		inner join Saxion_ContentAuthor.dbo.Subjects S (READUNCOMMITTED)
		on S.id = i.subjectid
		where --PublishStatus = 2 --published pages 
			 ContentProducerItemId is not null
	) CA
	on CA.ProjectId = b.ProjectID
		and CA.ItemID = b.ItemID
	where CA.itemid is not null
) C
where URL is not null
	and  R = 1
order by ProjectID, R

--,'https://testdeploy3.btlsurpass.com/#ItemAuthoring/Subject/'+cast([subjectid] as varchar(10))+'/Item/Edit/'+cast([ItemAuthoringID]  as varchar(10))+ '' [URL]