SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

use Saxion_ItemBank

SELECT 		
	QualificationName
	, AssessmentName
	, ExternalReference
FROM (
	select distinct [current].*
	from (
	select AssessmentName, AssessmentGroupID,ExternalReference, isvalid, AssessmentStatus, CreationDate
	from saxion_itembank..assessmenttable
	where isvalid = 0
		and AssessmentStatus = 2
		
		
	) [current]
	inner join (
	select AssessmentName, AssessmentGroupID,ExternalReference, isvalid, AssessmentStatus
	from [Saxion_Itembank_2017-11-08]..assessmenttable
	where isvalid = 1
		and AssessmentStatus = 2
		--and assessmentname = '01 Gio valkeneers h3 en h4'
	) old
	on old.AssessmentName = [current].AssessmentName
	 and old.ExternalReference = [current].ExternalReference
	 and old.IsValid <> [current].IsValid	
) AT
INNER JOIN AssessmentGroupTable AGT  (READUNCOMMITTED)
on AGT.ID = AT.AssessmentGroupID

INNER JOIN QualificationTable QT (READUNCOMMITTED)
ON QT.ID = AGT.QualificationID

order by 1,2,3



select *
from saxion_contentauthor..subjects
where projectid = 2640

select *
from saxion_contentauthor..items
where name = 'VPK2_VPK_SP_08'



