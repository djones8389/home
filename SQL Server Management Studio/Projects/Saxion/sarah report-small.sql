SELECT A.*
FROM (
select DISTINCT
	 ProjectId
	, Title
	, ContentProducerItemId
	,'https://saxion.surpass.com/#ItemAuthoring/Subject/'+cast([subjectid] as varchar(10))+'/Item/Edit/'+cast(i.Id  as varchar(10))+ '' [URL]
	, ROW_NUMBER() OVER(PARTITION BY ProjectId ORDER BY ContentProducerItemId) R
from Saxion_ContentAuthor.dbo.Items i (READUNCOMMITTED)
inner join Saxion_ContentAuthor.dbo.Subjects S (READUNCOMMITTED)
on S.id = i.subjectid
where PublishStatus = 2 --published pages 
		and ContentProducerItemId is not null
	
) A
where R = 1
order by projectid, r