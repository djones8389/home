create table #exams (
	QualificationName nvarchar(MAX)
	, AssessmentName nvarchar(MAX)
	, ExternalReference nvarchar(MAX)
	, ItemID nvarchar(MAX)
	, ItemStatus nvarchar(MAX)

)

bulk insert #exams
from 'C:\Users\davej\Desktop\PrePostDeployment_WithWorkFlowStatuses.csv'
with (fieldterminator = ',', rowterminator='\n')

select *
from #exams
where ItemStatus <> 4
order by 1,2,3