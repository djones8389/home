SELECT 	
	 QualificationName
	, AssessmentName
	, ExternalReference
	, Title [Subject]
	, ib.ProjectID
	, ib.ItemID
	, ItemAuthoringID
	,'https://testdeploy3.btlsurpass.com/#ItemAuthoring/Subject/'+cast([subjectid] as varchar(10))+'/Item/Edit/'+cast([ItemAuthoringID]  as varchar(10))+ '' [URL]
FROM (
	SELECT distinct 
		SUBSTRING(REPLACE(REPLACE(cast(a.b.query('.') as NVARCHAR(MAX)), '<XPath>@ID="',''), '"</XPath>',''), 0, CHARINDEX('P', REPLACE(REPLACE(cast(a.b.query('.') as NVARCHAR(MAX)), '<XPath>@ID="',''), '"</XPath>',''))) as [ProjectID]
		, REPLACE(REPLACE(cast(a.b.query('.') as NVARCHAR(MAX)), '<XPath>@ID="',''), '"</XPath>','') ItemID
		, QualificationName
		, AssessmentName
		, ExternalReference
	FROM AssessmentTable AT (READUNCOMMITTED)
		
	CROSS APPLY AssessmentRules.nodes('PaperRules//XPath') a(b)

	INNER JOIN AssessmentGroupTable AGT 
	on AGT.ID = AT.AssessmentGroupID

	INNER JOIN QualificationTable QT
	ON QT.ID = AGT.QualificationID

	where AT.IsValid = 0
		and REPLACE(REPLACE(cast(a.b.query('.') as NVARCHAR(MAX)), '<XPath>@ID="',''), '"</XPath>','') not like '<XPath>%'
		--and SUBSTRING(REPLACE(REPLACE(cast(a.b.query('.') as NVARCHAR(MAX)), '<XPath>@ID="',''), '"</XPath>',''), 0, CHARINDEX('P', REPLACE(REPLACE(cast(a.b.query('.') as NVARCHAR(MAX)), '<XPath>@ID="',''), '"</XPath>',''))) = 2557
		and at.AssessmentStatus = 2 --live assessments
		--and QualificationName = 'AGZ_FYS'
) IB

LEFT JOIN (

	select ProjectId, ContentProducerItemId, s.Title, (cast(ProjectId as varchar(10)) + 'P' + cast(ContentProducerItemId as varchar(10))) ItemID, i.PublishStatus, i.id [ItemAuthoringID]	
		, s.Id [subjectid]
	from TestDeploy3_ContentAuthor.dbo.Items i
	inner join TestDeploy3_ContentAuthor.dbo.Subjects S
	on S.id = i.subjectid
	--where ProjectId = 2515

) CA
ON CA.ProjectId = IB.ProjectID
	and CA.ItemID = IB.ItemID
where  PublishStatus = 0
order by 1;

--171 of their assessments are invalid, due to items being withdrawn in ItemAuthoring. Publishing should fix these


	--and  ExternalReference = 'AGZ_34667_OR2FYS_KW4_1516_1ekans'

	--AGZ_34667_OR2FYS_KW4_1516_1ekans

	/*
	ib.ItemID is null
	and ca.ItemID is not null
	*/