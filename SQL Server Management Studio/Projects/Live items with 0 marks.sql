IF OBJECT_ID('tempdb..##DATALENGTH') IS NOT NULL DROP TABLE ##DATALENGTH;

CREATE TABLE ##DATALENGTH (
	Client nvarchar(1000)
	, ItemCount int
);

DECLARE @dynamic nvarchar(MAX)='';

select @dynamic +=CHAR(13)+ '
use '+QUOTENAME([NAME])+'

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

INSERT ##DATALENGTH
select db_name() [Client]
	, count(1) [Item Count]
from (
SELECT [ProjectID]
      ,[ItemRef]
      ,max([Version]) [Version]
from [ItemTable] NOLOCK
where TotalMark = 0
group by [ProjectID]
      ,[ItemRef]
) a
inner join [ItemTable] b (NOLOCK)
on a.ProjectID = b.ProjectID 
	and a.ItemRef = b.ItemRef
	and a.Version = b.Version
where b.TotalMark = 0
'
from sys.databases
where state_desc = 'ONLINE'
	and name like '%[_]ItemBank';

exec(@dynamic);



select *
from ##DATALENGTH
order by 1








