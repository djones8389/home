SELECT COUNT(WAREHOUSE_UserTable.ID)
FROM WAREHOUSE_UserTable
LEFT JOIN (

	SELECT WAREHOUSEUserID FROM WAREHOUSE_ExamSessionTable
	UNION
	SELECT AssignedMarkerUserID FROM WAREHOUSE_ExamSessionTable
	UNION
	SELECT AssignedModeratorUserID FROM WAREHOUSE_ExamSessionTable
	UNION
	SELECT WAREHOUSECreatedBy FROM WAREHOUSE_ScheduledExamsTable
	UNION
	--IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS where COLUMN_NAME = 'Warehoused_UploadingUserID') 
		SELECT Warehoused_UploadingUserID FROM WAREHOUSE_ExamSessionDocumentInfoTable
	UNION
	SELECT Warehoused_UserID FROM WAREHOUSE_ExamSessionDurationAuditTable
	UNION
	SELECT 
	  y.value('userId[1]', 'int') AS UserId
	FROM WAREHOUSE_ExamSessionItemResponseTable
	CROSS APPLY MarkerResponseData.nodes('entries/entry[not(userId/text()=-1)]') x(y)

) A
 on A.WAREHOUSEUserID = WAREHOUSE_UserTable.ID

WHERE A.WAREHOUSEUserID IS NULL;

--WHERE ID NOT IN 

--sp_whoisactive

--SELECT * FROM INFORMATION_SCHEMA.COLUMNS where COLUMN_NAME = 'Warehoused_UploadingUserID'