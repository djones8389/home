CREATE TABLE [WAREHOUSE_ExamSessionTable_Shreded_Data] (
	examSessionID int
	,WAREHOUSEScheduledExamID  int
    ,WAREHOUSEUserID  int
    ,examId  int
    ,userId  int
	,voidJustificationLookupTableId int
	,[started] datetime
	,submitted datetime
	,resultSampled bit
	,automaticVerification bit
    ,validFromDate  datetime
    ,expiryDate  datetime
    ,defaultDuration int
    ,scheduledDurationReason nvarchar(MAX)
	,passMark decimal (6,3)
    ,totalMark decimal (6,3)
    ,passType int
    ,totalTimeSpent int
	,itemDataFull xml
 CONSTRAINT [PK_WAREHOUSE_ExamSessionTable_Shreded_Data] PRIMARY KEY CLUSTERED 
(
	[examSessionId] ASC
)
)


INSERT [WAREHOUSE_ExamSessionTable_Shreded_Data](examSessionID, itemDataFull)
select WAREHOUSE_ExamSessionTable_Shreded.examSessionId
	, convert(xml,'<itemData>' + convert(nvarchar(MAX),(SELECT WAREHOUSE_ExamSessionTable.resultDataFull.query('/assessmentDetails[1]/assessment[1]//item'))) + '</itemData>')
FROM WAREHOUSE_ExamSessionTable
INNER JOIN WAREHOUSE_ExamSessionTable_Shreded ON WAREHOUSE_ExamSessionTable.ID = WAREHOUSE_ExamSessionTable_Shreded.examSessionId
where  WAREHOUSE_ExamSessionTable_Shreded.examSessionId between 1 and 3230;



--UPDATE WAREHOUSE_ExamSessionTable_Shreded
--SET itemDataFull = convert(xml,'<itemData>' + convert(nvarchar(MAX),(SELECT WAREHOUSE_ExamSessionTable.resultDataFull.query('/assessmentDetails[1]/assessment[1]//item'))) + '</itemData>')
--FROM WAREHOUSE_ExamSessionTable
--INNER JOIN WAREHOUSE_ExamSessionTable_Shreded ON WAREHOUSE_ExamSessionTable.ID = WAREHOUSE_ExamSessionTable_Shreded.examSessionId
--where  WAREHOUSE_ExamSessionTable_Shreded.examSessionId between 1 and 3230;