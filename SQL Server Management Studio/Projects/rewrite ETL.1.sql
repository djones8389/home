if OBJECT_ID('tempdb..#DQ') is not null drop table #DQ;

SELECT
	CPID
	, TotalMark
INTO #DQ
FROM dbo.DimQuestions
where cpid = '869P3078'
--group by CPID;

CREATE CLUSTERED INDEX [IX_CPID] ON [#DQ] (CPID);
	


--UPDATE dbo.DimQuestions
--SET FacilityValue = ISNULL(FV, 0.5)  --isnull, should it be if 0 then 0.5...  ?
SELECT b.*
FROM DimQuestions DC
INNER JOIN (


SELECT COALESCE(FMRmark.CPID, FQRmark.CPID)  CPID
	,FMRmark.FMRmark
	,FQRmark.FQRmark
	--, AVG(FMRmark.FMRmark, FQRmark.FQRmark) FV
	--,AVG(ISNULL(FMRmark.FMRmark, FQRmark.FQRmark)) FV

FROM (
 	SELECT DQ.CPID
		, CASE WHEN SUM(COALESCE(DQ.TotalMark, 0)) = 0
				THEN 0
				ELSE SUM(COALESCE(FMR.AssignedMark, 0))/ SUM(COALESCE(DQ.TotalMark, 0))
				END AS FMRmark
	FROM #DQ DQ
	LEFT JOIN FactMarkerResponse FMR
	on fmr.CPID = DQ.CPID
	group by  DQ.CPID
) FMRmark
LEFT JOIN (
 		SELECT DQ.CPID
			, CASE WHEN SUM(COALESCE(DQ.TotalMark, 0)) = 0
					THEN 0
					ELSE SUM(COALESCE(FQR.mark, 0))/ SUM(COALESCE(DQ.TotalMark, 0))
					END AS FQRmark
		FROM #DQ DQ
		LEFT JOIN FactQuestionResponses FQR
		on FQR.CPID = DQ.CPID
		group by  DQ.CPID
) FQRmark
	on FMRmark.CPID = FQRmark.CPID

) B 

on B.CPID = DC.CPID
group by B.CPID



	--select  dq.cpid
	--	,	SUM(assignedMark) / SUM(TotalMark)
	--FROM dbo.DimQuestions DQ
	--LEFT JOIN FactMarkerResponse FMR
	--on fmr.CPID = DQ.CPID
	--where dq.cpid = '869P3078'
	--group by dq.cpid


	--select  dq.cpid
	--	,	SUM(mark) / SUM(TotalMark)
	--FROM dbo.DimQuestions DQ
	--LEFT JOIN FactQuestionResponses FMR
	--on fmr.CPID = DQ.CPID
	--where dq.cpid = '869P3078'
	--group by dq.cpid


--drop table #test
--create table #test (cpid varchar(100), mark1 float, mark2 float)
--insert #test
--values--('869P3078',	'0.425',	'0.2125')
--	('900P123',	'10',	'110')


--select * from #test

--select
--	cpid
--	, avg(ISNULL(mark1, mark2))
--from #test
--group by cpid




