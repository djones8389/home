USE [PPD_BCIntegration_SecureMarker]
GO
/****** Object:  UserDefinedFunction [dbo].[ParamsToList]    Script Date: 16/05/2016 14:14:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[ParamsToList] (@Parameters varchar(max))
returns @result TABLE (Value varchar(max))
AS  
begin
     DECLARE @TempList table
          (
          Value varchar(max)
          )

     DECLARE @Value varchar(max), @Pos int

     SET @Parameters = LTRIM(RTRIM(@Parameters))+ ','
     SET @Pos = CHARINDEX(',', @Parameters, 1)

     IF REPLACE(@Parameters, ',', '') <> ''
     BEGIN
          WHILE @Pos > 0
          BEGIN
               SET @Value = LTRIM(RTRIM(LEFT(@Parameters, @Pos - 1)))
               IF @Value <> ''
               BEGIN
                    INSERT INTO @TempList (Value) VALUES (@Value) --Use Appropriate conversion
               END
               SET @Parameters = RIGHT(@Parameters, LEN(@Parameters) - @Pos)
               SET @Pos = CHARINDEX(',', @Parameters, 1)

          END
     END    
     INSERT @result
     SELECT value
        FROM @TempList
     RETURN
END    


GO
/****** Object:  UserDefinedFunction [dbo].[ParamsToThreeColumnTable]    Script Date: 16/05/2016 14:14:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[ParamsToThreeColumnTable] (@Parameters varchar(max))
returns @result TABLE (Value1 varchar(max), Value2 varchar(max), Value3 varchar(max))
AS  
begin
     DECLARE @TempList table
          (
          Value1 varchar(max),
          Value2 varchar(max),
          Value3 varchar(max)
          )

     DECLARE @Value1 varchar(max)
     DECLARE @Value2 varchar(max)
     DECLARE @Value3 varchar(max)
     
     DECLARE @Pos int

     SET @Parameters = LTRIM(RTRIM(@Parameters))+ ','
     SET @Pos = CHARINDEX(',', @Parameters, 1)

     IF REPLACE(@Parameters, ',', '') <> ''
     BEGIN
          WHILE @Pos > 0
          BEGIN
               SET @Value1 = LTRIM(RTRIM(LEFT(@Parameters, @Pos - 1)))
               SET @Parameters = RIGHT(@Parameters, LEN(@Parameters) - @Pos)
               SET @Pos = CHARINDEX(',', @Parameters, 1)
               SET @Value2 = LTRIM(RTRIM(LEFT(@Parameters, @Pos - 1)))
               SET @Parameters = RIGHT(@Parameters, LEN(@Parameters) - @Pos)
               SET @Pos = CHARINDEX(',', @Parameters, 1)
               SET @Value3 = LTRIM(RTRIM(LEFT(@Parameters, @Pos - 1)))
               SET @Parameters = RIGHT(@Parameters, LEN(@Parameters) - @Pos)
               SET @Pos = CHARINDEX(',', @Parameters, 1)
               IF (@Value1 <> '' AND @Value2 <> '' AND @Value3 <> '')
               BEGIN
                    INSERT INTO @TempList (Value1, Value2, Value3) VALUES (@Value1, @Value2, @Value3) --Use Appropriate conversion
               END  
               
          END
     END    
     INSERT @result
     SELECT Value1, Value2, Value3
        FROM @TempList
     RETURN
END


GO
/****** Object:  UserDefinedFunction [dbo].[sm_checkGroupAndItemsMarkedInTolerance_fn]    Script Date: 16/05/2016 14:14:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Anton Burdyugov
-- Create date: 28/06/2013
-- Description:	Check all items in some group marked in tolerance
-- =============================================
CREATE FUNCTION [dbo].[sm_checkGroupAndItemsMarkedInTolerance_fn]
(
	-- Add the parameters for the function here
	@assignedGroupMarkId     INT
   ,@uniqueGroupResponse     INT
   ,@groupDeviation          DECIMAL(18,10)
   ,@groupTolerance          DECIMAL(18,10)
   ,@isAutoGroup			 BIT = NULL 
)
RETURNS BIT
AS
BEGIN
    -- Declare the return variable here
    DECLARE @myCheck BIT=0
    
    -- Add the T-SQL statements to compute the return value here
    
    
    IF	(@isAutoGroup IS NULL)
    BEGIN
		SET @isAutoGroup = (SELECT GD.IsAutomaticGroup FROM dbo.GroupDefinitions GD 
			INNER JOIN dbo.UniqueGroupResponses UGR ON UGR.ID = @uniqueGroupResponse AND UGR.GroupDefinitionID = GD.ID)
    END
    
    IF (@groupDeviation > @groupTolerance) -- group marked out of tolerance
    BEGIN
        SET @myCheck = 1
    END
    ELSE 
    IF (
			-- if autogroup check only group tolerance
			@isAutoGroup = 0 AND
           -- group marked within tolerance, so we need the same check on item level
           EXISTS
           (
               SELECT *
               FROM   dbo.AssignedItemMarks AIM
                      INNER JOIN dbo.UniqueResponses UR
                           ON  UR.ID = AIM.UniqueResponseId
                      INNER JOIN dbo.Items I
                           ON  I.ID = UR.itemId
                      INNER JOIN dbo.UniqueGroupResponseLinks UGRL
                           ON  UGRL.UniqueResponseId = UR.id
                      INNER JOIN dbo.UniqueGroupResponses UGR
                           ON  UGR.Id = UGRL.UniqueGroupResponseID
                      INNER JOIN dbo.GroupDefinitionItems GDI
                           ON  GDI.itemId = UR.itemId
                               AND GDI.GroupID = UGR.GroupDefinitionID
                               AND GDI.ViewOnly = 0
               WHERE  AIM.MarkingDeviation>I.CIMarkingTolerance -- the main check
                      AND AIM.GroupMarkId = @assignedGroupMarkId
                      AND UGRL.UniqueGroupResponseID = @uniqueGroupResponse
           )
       )
    BEGIN
        SET @myCheck = 1
    END
    
    -- Return the result of the function
    RETURN @myCheck
END




GO
/****** Object:  UserDefinedFunction [dbo].[sm_checkGroupCIsExceeded_fn]    Script Date: 16/05/2016 14:14:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Alexey Kharitonov
-- Create date: 29/05/2014
-- Description:	Check number of CIs for group is exceeded.
-- =============================================
CREATE FUNCTION [dbo].[sm_checkGroupCIsExceeded_fn]
(
	-- Add the parameters for the function here
	@groupID INT
)
RETURNS BIT
AS
BEGIN
	-- Return the result of the function
	RETURN(SELECT CONVERT(BIT,
					CASE WHEN 
							(SELECT COUNT(ID)
								FROM UniqueGroupResponses
								WHERE   CI=1
									AND GroupDefinitionID=GD.ID)>=GD.NoOfCIsRequired 
						THEN 1 
					ELSE 
						0 
					END) 
		   FROM GroupDefinitions GD
		   WHERE ID=@groupID)
END


GO
/****** Object:  UserDefinedFunction [dbo].[sm_checkIsUniqueGroupResponseInConfirmedScript]    Script Date: 16/05/2016 14:14:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Vitaly Bevzik
-- Create date: 12/30/2013
-- Description:	Checks Is Unique Group Response In ConfirmedS cript
-- =============================================
CREATE FUNCTION [dbo].[sm_checkIsUniqueGroupResponseInConfirmedScript]
(
	-- Add the parameters for the function here
	@uniqueGroupResponseId BIGINT
)
RETURNS BIT
AS
BEGIN
	-- Declare the return variable here
	DECLARE @result BIT = 0

	-- Add the T-SQL statements to compute the return value here
	IF EXISTS (
	SELECT * FROM dbo.CandidateGroupResponses CGR
		INNER JOIN dbo.CandidateExamVersions CEV on CEV.ID = CGR.CandidateExamVersionID
		WHERE CGR.UniqueGroupResponseID = @uniqueGroupResponseId
		AND CEV.ExamSessionState in (6,7))
	BEGIN
		SET @result = 1
	END

	-- Return the result of the function
	RETURN @result

END


GO
/****** Object:  UserDefinedFunction [dbo].[sm_checkUser1CiawayFromSuspend]    Script Date: 16/05/2016 14:14:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Vitaly Bevzik
-- Create date: 22/8/2013
-- Description:	Checks Examiner 1 CI away from being suspended
-- =============================================
CREATE FUNCTION [dbo].[sm_checkUser1CiawayFromSuspend]
(
	-- Add the parameters for the function here
	@userId INT,
	@groupId INT
)
RETURNS BIT
AS
BEGIN
	-- Declare the return variable here
	DECLARE @isUser1CiawayFromSuspend BIT

			-- calculate the last AGM.ID a CI was marked within tolerance by this user for this item
        DECLARE @lastCiAgmId BIGINT
				
        SELECT TOP 1
                @lastCiAgmId = AGM.ID
        FROM    [dbo].[AssignedGroupMarks] AGM
                INNER JOIN [dbo].[UniqueGroupResponses] UGR ON AGM.UniqueGroupResponseId = UGR.[id]
                INNER JOIN [dbo].GroupDefinitions GD ON UGR.GroupDefinitionID = GD.[ID]
        WHERE   AGM.userId = @userId
                AND UGR.GroupDefinitionID = @groupId
                AND AGM.markingMethodId IN ( 2, 3 )
                AND AGM.disregarded = 0
                AND dbo.sm_checkGroupAndItemsMarkedInTolerance_fn(AGM.id,
                                                              UGR.id,
                                                              AGM.MarkingDeviation,
                                                              GD.CIMarkingTolerance, GD.IsAutomaticGroup) = 0
        ORDER BY [timestamp] DESC					
		
		-- Get examiner CIs marked out of tolerance since last CG
		DECLARE @userCIsOutOfTol INT = (				
        SELECT  COUNT(*)
        FROM    [dbo].[AssignedGroupMarks] AGM
                INNER JOIN [dbo].[UniqueGroupResponses] UGR ON AGM.UniqueGroupResponseId = UGR.[id]
                INNER JOIN [dbo].GroupDefinitions GD ON UGR.GroupDefinitionID = GD.[ID]
        WHERE   AGM.userId = @userId
                AND AGM.markingMethodId IN ( 2, 3 )
                AND UGR.GroupDefinitionID = @groupId
                AND AGM.ID > ISNULL(@lastCiAgmId, 0)
                AND AGM.disregarded = 0
                AND dbo.sm_checkGroupAndItemsMarkedInTolerance_fn(AGM.id,
                                                              UGR.id,
                                                              AGM.MarkingDeviation,
                                                              GD.CIMarkingTolerance, GD.IsAutomaticGroup) = 1)
           
		-- get count of how many CIs examiner has marked out of tolerance within the rolling review period
		DECLARE @numberofItemsWithinRollingReview INT = (SELECT NumberOfCIsInRollingReview FROM GroupDefinitions WHERE ID = @groupId)
		DECLARE @maxConsecutiveCIsThatCanBeMarkedOutOfTolerance INT = (SELECT MaxConsecutiveCIsThatCanBeMarkedOutOfTolerance FROM GroupDefinitions WHERE ID = @groupId)
		DECLARE @maxCIsMarkedOutOfToleranceInRollingReview INT = (SELECT MaximumCIsMarkedOutOfToleranceInRollingReview FROM GroupDefinitions WHERE ID = @groupId)
		 
		DECLARE @numOfCIsMarkedOutOfTolinRollingReview INT = (                                                              
		SELECT  COUNT(x.[markingDeviation])
        FROM    ( SELECT TOP ( @numberofItemsWithinRollingReview )
                            agm.ID AS [AgmId] ,
                            UGR.ID AS [UGRId] ,
                            [markingDeviation] ,
                            [CIMarkingTolerance],
                            GD.IsAutomaticGroup
                  FROM      [dbo].[AssignedGroupMarks] AGM
                            INNER JOIN [dbo].[UniqueGroupResponses] UGR ON AGM.UniqueGroupResponseId = UGR.[id]
                            INNER JOIN [dbo].GroupDefinitions GD ON UGR.GroupDefinitionID = GD.[ID]
                  WHERE     AGM.userId = @userId
                            AND AGM.markingMethodId IN ( 2, 3 )
                            AND UGR.GroupDefinitionID = @groupId
                            AND AGM.disregarded = 0
                  ORDER BY Timestamp DESC
                ) x
        WHERE   dbo.sm_checkGroupAndItemsMarkedInTolerance_fn([AgmId], [UGRId],
                                                              [markingDeviation],
                                                              [CIMarkingTolerance], IsAutomaticGroup) = 1 )
    
    IF(@maxConsecutiveCIsThatCanBeMarkedOutOfTolerance <= @userCIsOutOfTol OR  @maxCIsMarkedOutOfToleranceInRollingReview <= @numOfCIsMarkedOutOfTolinRollingReview)  
    SET @isUser1CiawayFromSuspend = 1
    ELSE
    SET @isUser1CiawayFromSuspend = 0                                                     
                                                        
	-- Return the result of the function
	RETURN @isUser1CiawayFromSuspend

END


GO
/****** Object:  UserDefinedFunction [dbo].[sm_getAffectedUGRs_fn_OLD]    Script Date: 16/05/2016 14:14:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

Create FUNCTION [dbo].[sm_getAffectedUGRs_fn_OLD]
(
	-- Add the parameters for the function here
	@groupId INT,
	@userId INT
	
)
RETURNS @returnTable TABLE
        (
            UgrId BIGINT
        )
AS

BEGIN
    
    INSERT INTO @returnTable(UgrId)
    select CandidateGroupResponses.UniqueGroupResponseID AS UgrId
	from CandidateGroupResponses
	INNER JOIN 
	(
		SELECT *
		FROM dbo.sm_getAffectedScripts_fn(@groupId, @userId)
	) AS AffectedScriptIds
	ON CandidateGroupResponses.CandidateExamVersionID = AffectedScriptIds.ScriptId

    RETURN 
    
END


GO
/****** Object:  UserDefinedFunction [dbo].[sm_getAffectedUGRsCount_fn_OLD]    Script Date: 16/05/2016 14:14:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[sm_getAffectedUGRsCount_fn_OLD]
(
	@groupId INT,
	@userId INT
)
RETURNS INT
AS
BEGIN
		   
	RETURN 
	(
		SELECT COUNT(CGR.UniqueGroupResponseID)
		from CandidateGroupResponses CGR
		INNER JOIN UniqueGroupResponses UGR
		ON UGR.ID = CGR.UniqueGroupResponseID
		INNER JOIN 
			(
				SELECT *
				FROM dbo.sm_getAffectedScripts_fn(@groupId, @userId)
			) AS AffectedScriptIds
		ON CGR.CandidateExamVersionID = AffectedScriptIds.ScriptId
		WHERE UGR.GroupDefinitionID = @groupId
	)
END


GO
/****** Object:  UserDefinedFunction [dbo].[sm_getExamPermissionsForScriptReview_fn]    Script Date: 16/05/2016 14:14:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE FUNCTION [dbo].[sm_getExamPermissionsForScriptReview_fn]
(
	@userId INT
)
RETURNS @returnTable TABLE 
    (
		PermExamID INT NOT NULL,
		IsAllowReview BIT,
		IsAllowModerate BIT,
		IsAllowConfirm BIT,
		IsAllowVip BIT,
		IsAllowFlag BIT
    )
AS

BEGIN
    IF (
           EXISTS
           (
               SELECT r.ID
               FROM   dbo.AssignedUserRoles aur
                      INNER JOIN dbo.Roles r ON  r.ID = aur.RoleID
                      INNER JOIN dbo.RolePermissions rp ON  aur.RoleID = rp.RoleID
                      INNER JOIN dbo.Permissions p ON  rp.PermissionID = p.ID
               WHERE  aur.ExamID = 0
                      AND aur.UserID = @userID
                      AND p.GlobalSupport = 1
                      AND rp.PermissionID = 27 -- Script Review                 
           )
       )
    BEGIN
        INSERT @returnTable
        SELECT e.ID         AS 'PermExamID'
			   ,null
			   ,null
			   ,null
			   ,null
			   ,null
        FROM   dbo.Exams e
        WHERE  e.ID<>0
    END
    ELSE
    BEGIN
        INSERT @returnTable
        SELECT DISTINCT
               aur.ExamID AS 'PermExamID'
               ,null
			   ,null
			   ,null
			   ,null
			   ,null
        FROM   dbo.AssignedUserRoles aur
               INNER JOIN dbo.RolePermissions rp ON  aur.RoleID = rp.RoleID
        WHERE  aur.ExamID<>0
               AND aur.UserID = @userID
               AND rp.PermissionID = 27 -- Script Review
    END
    
    UPDATE RT
    SET IsAllowReview = (
						SELECT(CASE WHEN ((SELECT COUNT(*)
						FROM dbo.RolePermissions RP
						INNER JOIN dbo.AssignedUserRoles AUR ON Rp.RoleID = AUR.RoleID
						WHERE AUR.UserID = @userId AND (RT.PermExamID = AUR.ExamID OR AUR.ExamID = 0) AND RP.PermissionID = 35) -- Script Review View Items
						> 0) THEN 1  ELSE 0 END)), 
        IsAllowModerate = (SELECT(CASE WHEN ((SELECT COUNT(*)
						FROM dbo.RolePermissions RP
						INNER JOIN dbo.AssignedUserRoles AUR ON Rp.RoleID = AUR.RoleID
						WHERE AUR.UserID = @userId AND (RT.PermExamID = AUR.ExamID OR AUR.ExamID = 0) AND RP.PermissionID = 40) -- Script Review Moderate Script
						> 0) THEN 1  ELSE 0 END)), 
		IsAllowFlag = (SELECT(CASE WHEN ((SELECT COUNT(*)
						FROM dbo.RolePermissions RP
						INNER JOIN dbo.AssignedUserRoles AUR ON Rp.RoleID = AUR.RoleID
						WHERE AUR.UserID = @userId AND (RT.PermExamID = AUR.ExamID OR AUR.ExamID = 0) AND RP.PermissionID = 41) -- Script Review Set Flag
						> 0) THEN 1  ELSE 0 END)),
        IsAllowVip = (SELECT(CASE WHEN ((SELECT COUNT(*)
						FROM dbo.RolePermissions RP
						INNER JOIN dbo.AssignedUserRoles AUR ON Rp.RoleID = AUR.RoleID
						WHERE AUR.UserID = @userId AND (RT.PermExamID = AUR.ExamID OR AUR.ExamID = 0) AND RP.PermissionID = 30) -- Script Review Set VIP         
						> 0) THEN 1  ELSE 0 END)),
        IsAllowConfirm = (SELECT(CASE WHEN ((SELECT COUNT(*)
						FROM dbo.RolePermissions RP
						INNER JOIN dbo.AssignedUserRoles AUR ON Rp.RoleID = AUR.RoleID
						WHERE AUR.UserID = @userId AND (RT.PermExamID = AUR.ExamID OR AUR.ExamID = 0) AND RP.PermissionID = 29) -- Script Review Confirm         
						> 0) THEN 1  ELSE 0 END))
		FROM @returnTable RT
		
    RETURN
END


GO
/****** Object:  UserDefinedFunction [dbo].[sm_getIsNewItem_fn]    Script Date: 16/05/2016 14:14:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Anton Burdyugov
-- Create date: 29/04/2013
-- Description:	Determines whether or not an item is new.
-- =============================================
CREATE FUNCTION [dbo].[sm_getIsNewItem_fn]
(
	-- Add the parameters for the function here	
	@itemId INT
)
RETURNS INT
AS
BEGIN
	-- Declare the return variable here
	DECLARE @isNew BIT = 1 -- true

	-- Add the T-SQL statements to compute the return value here	
	IF
	(
		EXISTS
		(
			Select TOP 1 *
				from GroupDefinitionItems 
				inner join GroupDefinitions 
				on GroupDefinitions.Id = GroupDefinitionItems.GroupID
				where 
					GroupDefinitionItems.ItemID = @itemId 
					and 
					GroupDefinitions.IsAutomaticGroup = 0 -- false
		)
	)
		BEGIN
			SET @isNew = 0 -- false
		END	

	-- Return the result of the function
	RETURN @isNew

END


GO
/****** Object:  UserDefinedFunction [dbo].[sm_getMarkingExaminerReport_fn]    Script Date: 16/05/2016 14:14:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO






CREATE FUNCTION [dbo].[sm_getMarkingExaminerReport_fn]
(
	-- Add the parameters for the function here
	@userId						INT
	,@startDate					DATETIME
	,@endDate					DATETIME
)
RETURNS @returnTable TABLE
        (
            ID INT
           ,NAME NVARCHAR(MAX)
           ,Qualification NVARCHAR(100)
           ,Exam NVARCHAR(200)
           ,ExamVersion NVARCHAR(MAX)
           ,Total INT
           ,ControlItems INT
           ,UniqueResponses INT
           ,FirstName NVARCHAR(50)
           ,LastName NVARCHAR(50)
           ,UserId INT
        )
AS
BEGIN
	
	DECLARE @examIds TABLE (ExamId INT)
	INSERT INTO @examIds
		SELECT ExamId FROM dbo.AssignedUserRoles AUR
			INNER JOIN dbo.RolePermissions RP ON AUR.RoleID = RP.RoleID
			WHERE RP.PermissionID = 53 AND AUR.UserID = @userId
			
	DECLARE @isGlobal BIT = 0
	if (EXISTS(SELECT ExamId FROM @examIds WHERE ExamId = 0))
		SET @isGlobal = 1
	
    ;WITH cteMRV AS (
        SELECT MRV.ID						AS 'Id'
              ,MRV.QualificationId			AS 'QualificationId'
              ,MRV.ExamId					AS 'ExamId'
			  ,STUFF(
                   (SELECT
                        ', ' + EV2.Name
                        FROM   dbo.MarkingReport_view  MRV2
						JOIN dbo.CandidateGroupResponses CGR2
							ON (CGR2.UniqueGroupResponseID = MRV2.UniqueGroupResponseId
								AND TIMESTAMP BETWEEN @startDate AND @endDate 
								AND MRV2.EscalatedWithoutMark = 0			
								AND MRV2.ID = MRV.ID
								AND MRV2.QualificationId = MRV.QualificationId
								AND MRV2.ExamId = MRV.ExamId)
						JOIN dbo.CandidateExamVersions CEV2 
							ON (CEV2.ID = CGR2.CandidateExamVersionID)
						JOIN dbo.ExamVersions EV2 
							ON (EV2.ID = CEV2.ExamVersionID)
                        GROUP BY EV2.Name
                        ORDER BY EV2.Name
                        FOR XML PATH(''), TYPE
                   ).value('.','nvarchar(max)')
                   ,1,2, ''
              )					            AS 'ExamVersion'
              ,COUNT(MRV.GroupDefinitionID) AS 'Total'
              ,SUM(MRV.ControlItem)			AS 'ControlItems'
              ,SUM(MRV.UniqueResponse)		AS 'UniqueResponses'
              ,MRV.Forename					AS 'FirstName'
              ,MRV.Surname					AS 'LastName'
              ,MRV.UserId					AS 'UserId'
        FROM   dbo.MarkingReport_view  MRV
		WHERE  TIMESTAMP BETWEEN @startDate AND @endDate
			AND (@isGlobal = 1 OR MRV.ExamId IN (SELECT ExamId FROM @examIds))
			AND MRV.EscalatedWithoutMark = 0
        GROUP BY
               MRV.ID
              ,MRV.QualificationId
              ,MRV.ExamId
              ,MRV.UserId
              ,MRV.Forename
              ,MRV.Surname
    )
    
    INSERT INTO @returnTable
      (
        ID
       ,NAME
       ,Qualification
       ,Exam
       ,ExamVersion
       ,Total
       ,ControlItems
       ,UniqueResponses
       ,FirstName
       ,LastName
       ,UserId
      )
    SELECT MRV.ID
          ,i.ExternalItemName
          ,q.Name
          ,e.Name
          ,MRV.ExamVersion as Name
          ,MRV.Total
          ,MRV.ControlItems
          ,MRV.UniqueResponses
          ,MRV.FirstName
          ,MRV.LastName
          ,MRV.UserId
    FROM   cteMRV MRV
           INNER JOIN Items i ON  i.ID = MRV.ID
           INNER JOIN dbo.Qualifications q ON  q.ID = MRV.QualificationId
           INNER JOIN dbo.Exams e ON  e.ID = MRV.ExamId    
    RETURN
END




GO
/****** Object:  UserDefinedFunction [dbo].[sm_getMarkingReportItems_fn]    Script Date: 16/05/2016 14:14:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO






CREATE FUNCTION [dbo].[sm_getMarkingReportItems_fn]
(
	-- Add the parameters for the function here
	@userId        INT
   ,@startDate     DATETIME
   ,@endDate       DATETIME
)
RETURNS @returnTable TABLE
        (
            ID INT
           ,NAME NVARCHAR(MAX)
           ,Qualification NVARCHAR(100)
           ,Exam NVARCHAR(200)
           ,ExamVersion NVARCHAR(MAX)
           ,Total INT
           ,ControlItems INT
           ,UniqueResponses INT
        )
AS
BEGIN

     WITH cteMRV AS (
        SELECT MRV.ID						AS 'Id'
              ,MRV.QualificationId			AS 'QualificationId'
              ,MRV.ExamId					AS 'ExamId'
			  ,STUFF(
                   (SELECT
                        ', ' + EV2.Name
                        FROM   dbo.MarkingReport_view  MRV2
						JOIN dbo.CandidateGroupResponses CGR2
							ON (CGR2.UniqueGroupResponseID = MRV2.UniqueGroupResponseId
								AND TIMESTAMP BETWEEN @startDate AND @endDate 
								AND MRV2.EscalatedWithoutMark = 0	
								AND MRV2.ID = MRV.ID
								AND MRV2.QualificationId = MRV.QualificationId
								AND MRV2.ExamId = MRV.ExamId)
						JOIN dbo.CandidateExamVersions CEV2 
							ON (CEV2.ID = CGR2.CandidateExamVersionID)
						JOIN dbo.ExamVersions EV2 
							ON (EV2.ID = CEV2.ExamVersionID)
                        GROUP BY EV2.Name
                        ORDER BY EV2.Name
                        FOR XML PATH(''), TYPE
                   ).value('.','nvarchar(max)')
                   ,1,2, ''
              )					            AS 'ExamVersion'
              ,COUNT(MRV.GroupDefinitionID) AS 'Total'
              ,SUM(MRV.ControlItem)			AS 'ControlItems'
              ,SUM(MRV.UniqueResponse)		AS 'UniqueResponses'
        FROM   dbo.MarkingReport_view  MRV
		WHERE  UserId = @userId AND TIMESTAMP BETWEEN @startDate AND @endDate
		AND MRV.EscalatedWithoutMark = 0
        GROUP BY
               ID
              ,QualificationId
              ,ExamId
    )

    
    INSERT INTO @returnTable
      (
        ID
       ,NAME
       ,Qualification
       ,Exam
       ,ExamVersion
       ,Total
       ,ControlItems
       ,UniqueResponses
      )
    SELECT MRV.ID
          ,i.ExternalItemName
          ,q.Name
          ,e.Name
          ,MRV.ExamVersion as Name
          ,MRV.Total
          ,MRV.ControlItems
          ,MRV.UniqueResponses
    FROM   cteMRV MRV
           INNER JOIN Items i ON  i.ID = MRV.ID
           INNER JOIN dbo.Qualifications q ON  q.ID = MRV.QualificationId
           INNER JOIN dbo.Exams e ON  e.ID = MRV.ExamId    
    RETURN
END




GO
/****** Object:  UserDefinedFunction [dbo].[sm_getQuotaForUserForExam_fn]    Script Date: 16/05/2016 14:14:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[sm_getQuotaForUserForExam_fn]
    (
      -- Add the parameters for the function here
      @examId INT,
      @userId INT
    )
RETURNS INT
AS 
    BEGIN
	-- Declare the return variable here
        DECLARE @myQuota INT

	-- Add the T-SQL statements to compute the return value here

        IF ( EXISTS ( SELECT    UserId
                      FROM      dbo.QuotaManagement QM
                                INNER JOIN dbo.GroupDefinitions GD ON QM.GroupId = GD.ID
                      WHERE     GD.ExamID = @examId
                                AND QM.UserId = @userId
                                AND QM.AssignedQuota = -1
                                AND EXISTS ( SELECT *
                                             FROM   GroupDefinitionStructureCrossRef CR
                                                    INNER JOIN ExamVersionStructures EVS ON EVS.ID = CR.ExamVersionStructureID
                                             WHERE  CR.Status = 0
                                                    AND CR.GroupDefinitionID = GD.ID
                                                    AND EVS.StatusID = 0 ) ) ) 
            BEGIN
                SET @myQuota = -1
            END
        ELSE 
            BEGIN
                SELECT  @myQuota = ISNULL(SUM(QM.AssignedQuota), 0)
                FROM    dbo.QuotaManagement QM
                        INNER JOIN dbo.GroupDefinitions GD ON QM.GroupId = GD.ID                       
                WHERE   GD.ExamID = @examId
                        AND QM.UserId = @userId
                        AND EXISTS ( SELECT *
                                             FROM   GroupDefinitionStructureCrossRef CR
                                                    INNER JOIN ExamVersionStructures EVS ON EVS.ID = CR.ExamVersionStructureID
                                             WHERE  CR.Status = 0
                                                    AND CR.GroupDefinitionID = GD.ID
                                                    AND EVS.StatusID = 0 )
            END

	-- Return the result of the function
        RETURN @myQuota

    END


GO
/****** Object:  UserDefinedFunction [dbo].[sm_getUserQuotaForGroup_fn]    Script Date: 16/05/2016 14:14:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Vitaly Bevzik
-- Create date: 19/09/2013
-- Description:	Gets a user's assigned quota for an item.
-- =============================================
CREATE FUNCTION [dbo].[sm_getUserQuotaForGroup_fn]
(
	-- Add the parameters for the function here	
	@groupID INT ,
    @userID INT
)
RETURNS INT
AS
BEGIN
	-- Declare the return variable here
	DECLARE @quota int = 0

	 IF ( EXISTS ( SELECT    Q.AssignedQuota
                      FROM      dbo.QuotaManagement Q
                      WHERE     Q.UserId = @userID
                                AND Q.GroupId = @groupID ) ) 
            BEGIN
                set @quota = (SELECT  ISNULL(Q.AssignedQuota, 0)
                FROM    dbo.QuotaManagement Q
                WHERE   Q.UserId = @userID
                        AND Q.GroupId = @groupID)
            END       

	-- Return the result of the function
	RETURN @quota

END



GO
/****** Object:  UserDefinedFunction [dbo].[getExamsWithRestrictions_fn]    Script Date: 16/05/2016 14:14:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE FUNCTION [dbo].[getExamsWithRestrictions_fn]
(	
	@userId INT,
	@defaultMarkingRestriction BIT
)
RETURNS TABLE
WITH SCHEMABINDING
AS
RETURN 
(
	SELECT E.ID AS ExamId
              , ISNULL(UE.MarkingRestriction, ISNULL(E.IsMarkOneItemPerScript ,@defaultMarkingRestriction)) AS IsMarkingRestrictionEnableForExam
              , E.Name as Name
              , E.QualificationID as QualificationID
			FROM dbo.Exams E
			LEFT JOIN dbo.UserExamUnrestrictedExaminer_view AS UE ON UE.ExamId = E.ID AND UserId = @userId
)







GO
/****** Object:  UserDefinedFunction [dbo].[getAffectedScripts_fn]    Script Date: 16/05/2016 14:14:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[getAffectedScripts_fn]
(	
	@userId INT
)
RETURNS TABLE
WITH SCHEMABINDING
AS
RETURN 
(
	SELECT ACEV.CandidateExamVersionId AS AffectedScript
			FROM   dbo.CandidateExamVersions CEV
				 INNER JOIN dbo.AffectedCandidateExamVersions ACEV
					  ON  ACEV.CandidateExamVersionId = CEV.ID
			WHERE  ACEV.UserId = @userId
				 AND ACEV.GroupDefinitionID IN (SELECT GroupDefinitionID FROM dbo.ActiveGroups_view)
			GROUP BY ACEV.CandidateExamVersionId
)

GO
/****** Object:  UserDefinedFunction [dbo].[getAffectedUniqueResponses_fn]    Script Date: 16/05/2016 14:14:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE FUNCTION [dbo].[getAffectedUniqueResponses_fn]
(	
	@userId INT,
	@tokenStoreID INT
)
RETURNS TABLE
WITH SCHEMABINDING
AS
RETURN 
(
	SELECT COUNT(CGR1.UniqueGroupResponseID) as ResponsesCount
				, GD.ID AS GroupId
			FROM dbo.GroupDefinitions GD
			JOIN dbo.UniqueGroupResponses UGR 
				ON  (UGR.GroupDefinitionID = GD.ID
					AND UGR.IsEscalated = 0 
					AND UGR.CI = 0
					AND UGR.confirmedMark IS NULL		
					AND (UGR.[tokenid] IS NULL OR UGR.[tokenid] = @tokenStoreID))
			JOIN dbo.CandidateGroupResponses CGR1 ON CGR1.UniqueGroupResponseID = UGR.ID
		WHERE CGR1.CandidateExamVersionID IN (SELECT AffectedScript FROM dbo.getAffectedScripts_fn(@userId))
		GROUP BY GD.ID
)

GO
/****** Object:  UserDefinedFunction [dbo].[getAvailableUniqueGroupResponses_fn]    Script Date: 16/05/2016 14:14:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE FUNCTION [dbo].[getAvailableUniqueGroupResponses_fn]
(	
	@tokenStoreID INT
)
RETURNS TABLE
WITH SCHEMABINDING
AS
RETURN 
(
	SELECT UniqueGroupResponseId
		, InsertionDate
		, TokenId
		, GroupDefinitionID
		, VIP
		FROM dbo.NotMarkedResponses_view
		WHERE TokenId IS NULL
				OR TokenId = @tokenStoreID
)



GO
/****** Object:  UserDefinedFunction [dbo].[getUserExamsWithMarkingPermissions_fn]    Script Date: 16/05/2016 14:14:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE FUNCTION [dbo].[getUserExamsWithMarkingPermissions_fn]
(	
	@userId INT
)
RETURNS @result TABLE(ExamId INT)
WITH SCHEMABINDING
AS
BEGIN 
	DECLARE @userExamPermissions TABLE (ExamId INT)
	INSERT INTO @userExamPermissions
		SELECT ExamId
			FROM dbo.UserExamExaminer_view
			WHERE UserId = @userId
			GROUP BY ExamId

	IF EXISTS (SELECT ExamId FROM @userExamPermissions WHERE ExamId = 0)
		BEGIN
			INSERT INTO @result VALUES(0)
		END
	ELSE
		BEGIN
			INSERT INTO @result
				SELECT ExamId FROM @userExamPermissions
		END
	RETURN
END



GO
/****** Object:  UserDefinedFunction [dbo].[getGroupDefinitionsJoined_fn]    Script Date: 16/05/2016 14:14:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[getGroupDefinitionsJoined_fn]
(	
	@userId INT,
	@tokenStoreId INT
)
RETURNS TABLE
WITH SCHEMABINDING
AS
RETURN 
(
	SELECT GD.ID as ID
		, GD.Name as Name
		, GD.ExamID as ExamId
		, E.IsMarkOneItemPerScript as IsMarkOneItemPerScript
		, GD.IsReleased as IsReleased
	FROM dbo.GroupDefinitions GD
		JOIN dbo.Exams E ON GD.ExamID = E.ID
		JOIN dbo.getUserExamsWithMarkingPermissions_fn(@userId) ER ON (ER.ExamId = 0 OR ER.ExamId = GD.ExamID)
		WHERE GD.IsReleased = 0 OR GD.IsReleased = 1 -- we have to get groups with any state but Not Released
			AND GD.ID IN (SELECT GroupDefinitionID FROM dbo.ActiveGroups_view)
)


GO
/****** Object:  UserDefinedFunction [dbo].[getGroupSourceTable_fn]    Script Date: 16/05/2016 14:14:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[getGroupSourceTable_fn]
(	
	@userId INT,
	@tokenStoreID INT,
	@unlimitedOnly BIT
)
RETURNS TABLE
WITH SCHEMABINDING
AS
RETURN 
(
	SELECT GD.ID
            , GD.Name AS GroupName
            , GD.ExamID
            , UGR.InsertionDate
            , UGR.UniqueGroupResponseId
            , GD.IsMarkOneItemPerScript 
            , CAST(ISNULL(UGR.VIP ,0) AS FLOAT) AS VIP
            , CASE (QM.MarkingSuspended) WHEN 1 THEN 1 ELSE 0 END AS Suspended
            , GD.IsReleased AS 'IsReleased'
	FROM   dbo.QuotaManagement QM
            JOIN dbo.getGroupDefinitionsJoined_fn(@userId, @tokenStoreID) GD ON GD.ID = QM.GroupId
            LEFT JOIN dbo.getAvailableUniqueGroupResponses_fn(@tokenStoreID) AS UGR ON UGR.GroupDefinitionID = QM.GroupId
	WHERE  QM.UserId = @userId
			AND (@unlimitedOnly = 1 AND QM.AssignedQuota = -1 OR @unlimitedOnly = 0)
)








GO
/****** Object:  UserDefinedFunction [dbo].[getGroupAggregatedSourceForMarking_fn]    Script Date: 16/05/2016 14:14:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[getGroupAggregatedSourceForMarking_fn]
(	
	@userId INT,
	@tokenStoreID INT
)
RETURNS TABLE
WITH SCHEMABINDING
AS
RETURN 
(
	SELECT 
	T.ID AS GroupId
	, GroupName
	, ExamID
	, MIN(InsertionDate) AS EarliestDate
	, IsReleased
	, COUNT(UniqueGroupResponseId) AS NumResponses
	, ISNULL( MAX(CAST(Suspended as INT)), 0) AS Suspended
	, CASE WHEN COUNT(UniqueGroupResponseId) = 0 THEN 0 ELSE SUM(VIP)/CAST(COUNT(UniqueGroupResponseId) AS FLOAT) END AS VIP
	FROM dbo.getGroupSourceTable_fn(@userId, @tokenStoreID, 0) T
	INNER JOIN dbo.ManualMarkedGroups_view MMG
	ON T.ID = MMG.ID 
	GROUP BY ExamID
			, T.ID
			, T.GroupName
			, T.IsReleased
			, T.IsMarkOneItemPerScript
)


GO
/****** Object:  UserDefinedFunction [dbo].[getGroupsWithAffectedResponsesForMarking_fn]    Script Date: 16/05/2016 14:14:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- User Defined Function

CREATE FUNCTION [dbo].[getGroupsWithAffectedResponsesForMarking_fn]
(	
	@userId INT,
	@tokenStoreID INT
)
RETURNS TABLE
WITH SCHEMABINDING
AS
RETURN 
(
	SELECT GAS.GroupId
				, GroupName
				, ExamID
				, EarliestDate
				, NumResponses
				, IsReleased
				, VIP
				, Suspended
				, ResponsesCount as AffectedUniqueResponses
				, NumResponses - ISNULL(ResponsesCount, 0) as ResponsesDif
			FROM dbo.getGroupAggregatedSourceForMarking_fn(@userId, @tokenStoreID) AS GAS
				LEFT JOIN dbo.getAffectedUniqueResponses_fn(@userId, @tokenStoreID) AS AUR ON AUR.GroupId = GAS.GroupId
)

GO
/****** Object:  UserDefinedFunction [dbo].[sm_MARKINGSERVICE_GetAllGroupsForUser_fn]    Script Date: 16/05/2016 14:14:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[sm_MARKINGSERVICE_GetAllGroupsForUser_fn]
(
	@userId INT,
	@tokenStoreID INT,
	@defaultMarkingRestriction BIT
)
RETURNS TABLE 
RETURN
(
	SELECT Q.Name AS Qualification,
		   Q.ID AS QualificationId,
		   EWR.Name AS Exam,
		   EWR.ExamID,
		   T.GroupId AS ID,
		   T.GroupName AS ItemName,
		   QM.NumberMarked AS Marked,
		   (
			   CASE 
					WHEN (QM.[assignedquota] = -1) THEN -1
					WHEN ((QM.[assignedquota] - QM.NumberMarked) > 0) THEN (QM.[assignedquota] - QM.NumberMarked)
					ELSE 0
			   END
		   ) AS RemainingQuota,
		   T.NumResponses AS UnmarkedUGRsCount,
		   (
			   CASE EWR.IsMarkingRestrictionEnableForExam
					WHEN 1 THEN (T.ResponsesDif)
					ELSE T.NumResponses
			   END
		   ) AS UGRsAvailableToMarkCount,
		   CAST(
			   CASE WHEN (
				   (CASE EWR.IsMarkingRestrictionEnableForExam
						WHEN 1 THEN (T.ResponsesDif)
						ELSE T.NumResponses
				   END) > 0 
				   AND T.Suspended = 0
				   AND (CASE 
							WHEN (QM.[assignedquota] = -1) THEN -1
							WHEN ((QM.[assignedquota] - QM.NumberMarked) > 0) THEN (QM.[assignedquota] - QM.NumberMarked)
							ELSE 0
					   END) != 0
				   ) THEN 1 ELSE 0 END AS BIT
		   ) AS HasToMark,
		   CONVERT(BIT, T.Suspended) AS IsSuspended,
		   T.IsReleased AS IsReleased,
		   T.VIP AS VIP,
		   T.EarliestDate AS EarliestDate
	       
	FROM [dbo].[getGroupsWithAffectedResponsesForMarking_fn](@userId, @tokenStoreID) AS T
		INNER JOIN dbo.getExamsWithRestrictions_fn(@userId, @defaultMarkingRestriction) AS EWR ON  EWR.ExamID = T.ExamId
		INNER JOIN dbo.Qualifications AS Q ON  EWR.QualificationID = Q.ID
		INNER JOIN QuotaManagement AS QM ON (T.GroupId = QM.GroupId AND QM.UserId = @userId)
	-- if the user didn't mark the group and the user doesn't have quota for the group => we shouldn't show the group
	-- otherwice if the user has quota for the group or marked the group already => we have to show such group
	WHERE (QM.NumberMarked != 0 OR QM.AssignedQuota != 0)
)



GO
/****** Object:  UserDefinedFunction [dbo].[getCentreNameAndCodeForUniqueGroupResponse_fn]    Script Date: 16/05/2016 14:14:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE FUNCTION [dbo].[getCentreNameAndCodeForUniqueGroupResponse_fn]
(	
	@uniqueGroupResponseId INT
)
RETURNS TABLE
WITH SCHEMABINDING
AS
RETURN 
(
	SELECT CentreName
		, CentreCode
		, Id as UniqueGroupResponseId
		FROM [dbo].[GroupCentreCandidate_view]
		WHERE Id = @uniqueGroupResponseId
)




GO
/****** Object:  UserDefinedFunction [dbo].[sm_getExamPermissionsForEscalations_fn]    Script Date: 16/05/2016 14:14:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[sm_getExamPermissionsForEscalations_fn]
(
	@userId INT
)
RETURNS TABLE 
WITH SCHEMABINDING
RETURN
    
SELECT aur.ExamID AS 'ID'
FROM   dbo.AssignedUserRoles aur
       INNER JOIN dbo.RolePermissions rp ON  aur.RoleID = rp.RoleID
       INNER JOIN dbo.Permissions p ON  rp.PermissionID = p.ID
WHERE  aur.ExamID<>0
       AND aur.UserID = @userId
       AND rp.PermissionID = 45 -- Escalations (tab)
		       
		UNION

SELECT e.ID       AS 'ID'
FROM   dbo.Exams     e
WHERE  e.ID<>0
       AND EXISTS
           (
               SELECT aur.RoleID
               FROM   dbo.AssignedUserRoles aur
                      INNER JOIN dbo.RolePermissions rp ON  aur.RoleID = rp.RoleID
                      INNER JOIN dbo.Permissions p ON  rp.PermissionID = p.ID
               WHERE  aur.ExamID = 0
                      AND aur.UserID = @userId
                      AND p.GlobalSupport = 1
                      AND rp.PermissionID = 45 -- Escalations (tab)
           )
    


GO
/****** Object:  UserDefinedFunction [dbo].[getGroupDefinitionsForEscalations_fn]    Script Date: 16/05/2016 14:14:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE FUNCTION [dbo].[getGroupDefinitionsForEscalations_fn]
(	
	@userId INT
)
RETURNS TABLE
WITH SCHEMABINDING
AS
RETURN 
(
	SELECT [ID]
	,[Name]
	,[ExamID]
	FROM dbo.GroupDefinitions GD
	WHERE EXISTS 
		(
			SELECT GDS.Status 
			FROM dbo.GroupDefinitionStructureCrossRef GDS 
			WHERE GDS.GroupDefinitionID = GD.ID 
				AND (GDS.Status = 0 OR GDS.Status = 2)
		) 
	AND EXISTS (SELECT ID FROM dbo.sm_getExamPermissionsForEscalations_fn(@userId) WHERE ID = GD.ExamID)
)




GO
/****** Object:  UserDefinedFunction [dbo].[sm_EscalatedUGRs_fn]    Script Date: 16/05/2016 14:14:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE FUNCTION [dbo].[sm_EscalatedUGRs_fn]
(
	@userId INT
)
RETURNS TABLE
RETURN
(
	SELECT
		QE.Qualification, 
		QE.QualificationId, 
		QE.Exam, 
		QE.ExamId, 
		EVNC.VersionNames AS 'ExamVersion', 
		0 AS 'ExamVersionId', 
		GD.Name AS 'ItemName', 
		GD.ID AS 'ItemId', 
		(
			SELECT U.Surname + ' ' + U.Forename FROM dbo.Users U WHERE U.ID = EUGR.ParkedUserID
		) AS 'Examiner',
		EUGR.ParkedUserID AS 'ExaminerId',
		EUGR.ParkedDate AS 'DateEscalated',
		EUGR.ID AS 'ID',
		(
			SELECT RTRIM(UGRPRCR.ParkedReasonId) + ', '
			FROM dbo.UniqueGroupResponseParkedReasonCrossRef UGRPRCR
			WHERE UGRPRCR.UniqueGroupResponseId = EUGR.ID
			FOR XML PATH('')
		) AS 'ReasonIds',	
		(
			SELECT RTRIM(PRL.Name) + ', '
			FROM dbo.ParkedReasonsLookup PRL
			INNER JOIN dbo.UniqueGroupResponseParkedReasonCrossRef UGRPRCR ON UGRPRCR.ParkedReasonId = PRL.ID
			WHERE UGRPRCR.UniqueGroupResponseId = EUGR.ID
			FOR XML PATH('')
		) AS 'Issues',
		( 
			SELECT DISTINCT RTRIM(CentreName) + ', '
			FROM dbo.getCentreNameAndCodeForUniqueGroupResponse_fn(EUGR.ID)
			FOR XML PATH('')
				
		) AS 'Centres',
        (
            SELECT DISTINCT RTRIM(CentreCode) + ', '
            FROM dbo.getCentreNameAndCodeForUniqueGroupResponse_fn(EUGR.ID)
            FOR XML PATH('')
		) AS 'CentresCodes',
		(
			SELECT COUNT(CGR.CandidateExamVersionID)
			FROM dbo.CandidateGroupResponses CGR
			WHERE CGR.UniqueGroupResponseID = EUGR.ID
		) AS 'CountOfScripts',
		-- returns empty string when UGR contains more then one Script, for Filters work correctly
		EUGR.CandidateName AS 'CandidateName',
		EUGR.CandidateRef AS 'CandidateRef',
		EUGR.CentreName AS 'CentreName',
		EUGR.CentreCode AS 'CentreCode',
		EUGR.ScriptId AS 'ScriptId',
		(
			CASE WHEN 0 < 
				(
					SELECT COUNT(*)
					FROM dbo.RolePermissions RP
					INNER JOIN dbo.AssignedUserRoles AUR ON Rp.RoleID = AUR.RoleID
					WHERE AUR.UserID = @userId 
						AND (AUR.ExamID = GD.ExamID OR AUR.ExamID = 0) 
						AND RP.PermissionID = 46
				)
			THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT) END
		) AS 'IsAllowViewWholeScript'
		 
	FROM dbo.getGroupDefinitionsForEscalations_fn(@userId) GD
		INNER JOIN dbo.EscalatedUniqueGroupResponsesForCandidateCentre_view EUGR ON GD.ID = EUGR.GroupDefinitionID
		INNER JOIN dbo.QualificationExams_view QE ON QE.ExamId = GD.ExamId
		INNER JOIN dbo.ExamVersionNamesConcatenated_view EVNC ON EVNC.ExamId = GD.ExamID AND EVNC.GroupId = GD.ID
	WHERE EXISTS 
		(
			SELECT GDS.GroupDefinitionID
			FROM dbo.GroupDefinitionStructureCrossRef GDS 
			WHERE GDS.GroupDefinitionID = GD.ID 
			AND (GDS.Status = 0 OR GDS.Status = 2)
		) 
	AND EXISTS (SELECT ID FROM dbo.sm_getExamPermissionsForEscalations_fn(@userId) WHERE ID = GD.ExamID)

)


GO
/****** Object:  UserDefinedFunction [dbo].[getGroupAggregatedSourceForHome_fn]    Script Date: 16/05/2016 14:14:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[getGroupAggregatedSourceForHome_fn]
(	
	@userId INT,
	@tokenStoreID INT
)
RETURNS TABLE
WITH SCHEMABINDING
AS
RETURN 
(
	SELECT ID as GroupId
				, GroupName
				, ExamID
				, MIN(InsertionDate) AS EarliestDate
				, COUNT(UniqueGroupResponseId) AS NumResponses
				, SUM(VIP)/CAST(COUNT(UniqueGroupResponseId) AS FLOAT) as VIP
				, ISNULL(MAX(Suspended),0) AS Suspended
			FROM dbo.getGroupSourceTable_fn(@userId, @tokenStoreID, 1)
			WHERE IsReleased = 1 -- for Home we have to get only groups with Released state
			GROUP BY ExamID
					,ID
					,GroupName
					,IsMarkOneItemPerScript
			HAVING COUNT(UniqueGroupResponseId) > 0
)





GO
/****** Object:  UserDefinedFunction [dbo].[getGroupsWithAffectedResponsesForHome_fn]    Script Date: 16/05/2016 14:14:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- User Defined Function

CREATE FUNCTION [dbo].[getGroupsWithAffectedResponsesForHome_fn]
(	
	@userId INT,
	@tokenStoreID INT
)
RETURNS TABLE
WITH SCHEMABINDING
AS
RETURN 
(
	SELECT GAS.GroupId
				, GroupName
				, ExamID
				, EarliestDate
				, NumResponses
				, VIP
				, Suspended
				, ResponsesCount as AffectedUniqueResponses
				, NumResponses - ISNULL(ResponsesCount, 0) as ResponsesDif
			FROM dbo.getGroupAggregatedSourceForHome_fn(@userId, @tokenStoreID) AS GAS
				LEFT JOIN dbo.getAffectedUniqueResponses_fn(@userId, @tokenStoreID) AS AUR ON AUR.GroupId = GAS.GroupId
)

GO
/****** Object:  UserDefinedFunction [dbo].[sm_getAffectedScripts_fn_OLD]    Script Date: 16/05/2016 14:14:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[sm_getAffectedScripts_fn_OLD]
(
	-- Add the parameters for the function here
	@groupId INT,
	@userId INT
	
)
RETURNS TABLE
AS
    
RETURN

select CGR.CandidateExamVersionID AS ScriptId
from CandidateGroupResponses CGR
INNER JOIN UniqueGroupResponses UGR
ON UGR.ID = CGR.UniqueGroupResponseID
where UGR.GroupDefinitionID != @groupId 
AND exists 
(
	select *
	from 
	(
		select TOP (1) 
			AGM.MarkingMethodId AS MM, 
			AGM.UserId AS UI,
			AGM.IsEscalated AS IE
		from AssignedGroupMarks AGM
		where AGM.UniqueGroupResponseId = CGR.UniqueGroupResponseID
		order by AGM.Timestamp desc
	) AS T
	where ((T.MM != 14 AND T.MM != 11) OR (T.MM = 11 AND T.IE = 1 )) AND T.UI = @userId
)


GO
/****** Object:  UserDefinedFunction [dbo].[sm_getQuotaManagementInfoForUser_fn]    Script Date: 16/05/2016 14:14:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[sm_getQuotaManagementInfoForUser_fn]
(
	@examId INT
)
RETURNS TABLE
AS
    
RETURN
(

	SELECT  QM.UserId							AS 'UserId',
			ISNULL(SUM(QM.NumberMarked), 0)		AS 'NumberMarked',
			(CASE
				WHEN 
					ISNULL(SUM(CAST(QM.MarkingSuspended AS INT)), 0) > 0
				THEN 1
				ELSE 0
			END)								AS 'MarkerSuspended',
			(CASE
				WHEN 
					ISNULL(SUM(CAST(QM.Within1CIofSuspension AS INT)), 0) > 0
				THEN 1
				ELSE 0
			END)								AS 'MarkerCloseToBeingSuspended',
			(CASE
				WHEN 
					ISNULL(SUM(QM.NumItemsParked), 0) > 0
				THEN 1
				ELSE 0
			END)								AS 'Parked',
			ISNULL(SUM(QM.NumberSuspended), 0)	AS 'NumberSuspended'
		
	FROM    QuotaManagement QM
			INNER JOIN GroupDefinitions GD ON QM.GroupId = GD.ID
	WHERE   GD.ExamID = @examId  
			AND
			EXISTS 
			(
				SELECT	* 
				FROM	GroupDefinitionStructureCrossRef GDSCR
						INNER JOIN ExamVersionStructures EVS ON EVS.ID = GDSCR.ExamVersionStructureID			
				WHERE	GDSCR.Status = 0 
						AND GDSCR.GroupDefinitionID = GD.ID 
						AND EVS.StatusID = 0
			)  
	GROUP BY QM.UserId    
)


GO
/****** Object:  UserDefinedFunction [dbo].[sm_hasUserRealMarkedResponsesForExam_fn]    Script Date: 16/05/2016 14:14:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE FUNCTION [dbo].[sm_hasUserRealMarkedResponsesForExam_fn]
(
	@examId INT
)
RETURNS TABLE
AS
 
RETURN
(
    SELECT U.ID  AS 'UserId'
          ,CAST(
               CASE 
                    WHEN SUM(
                             CASE 
                                  WHEN AGM.IsConfirmedMark=1
									   AND UGR.CI=0
								  THEN 1 ELSE 0 
                             END
                         )>0 THEN 1
                    ELSE 0
               END AS BIT
           )     AS 'hasRealMarkedResponses'
    FROM   Users U
           INNER JOIN dbo.AssignedGroupMarks AGM ON  U.ID = AGM.UserId
           INNER JOIN dbo.UniqueGroupResponses UGR ON  AGM.UniqueGroupResponseId = UGR.ID
           INNER JOIN dbo.GroupDefinitions GD ON  GD.ID = UGR.GroupDefinitionID
    WHERE  GD.ExamID = @examId
           AND EXISTS (
                   SELECT *
                   FROM   dbo.GroupDefinitionStructureCrossRef GDSCR
                          INNER JOIN dbo.ExamVersionStructures EVS ON  EVS.ID = GDSCR.ExamVersionStructureID
                   WHERE  GDSCR.Status = 0
                          AND GDSCR.GroupDefinitionID = GD.ID
                          AND EVS.StatusID = 0
               )
    GROUP BY
           U.ID
)




GO
/****** Object:  UserDefinedFunction [dbo].[sm_MANAGEEXAMINERS_GetExaminers_fn]    Script Date: 16/05/2016 14:14:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[sm_MANAGEEXAMINERS_GetExaminers_fn]
(
	@examId INT
)
RETURNS TABLE 
RETURN
(
	SELECT ID,
       Forename,
       Surname,
       (
           SELECT (RTRIM(NAME))
           FROM   Roles
           WHERE  Id IN (SELECT RoleId
                         FROM   AssignedUserRoles
                         WHERE  UserId = Users.ID
                                AND AssignedUserRoles.ExamID = @examId) FOR XML PATH('')
       )  AS RolesNames,
       (
           SELECT (RTRIM(RoleId) + ',')
           FROM   AssignedUserRoles
           WHERE  UserId = ID
                  AND AssignedUserRoles.ExamID = @examId FOR XML PATH('')
       )  AS Roles,
       (
           CASE 
                WHEN EXISTS(
                         SELECT RoleID
                         FROM   AssignedUserRoles
                                INNER JOIN Roles
                                     ON  AssignedUserRoles.RoleId = Roles.ID
                         WHERE  AssignedUserRoles.UserID = Users.ID
                                AND Roles.TrainingRole = 1
                                AND AssignedUserRoles.ExamID = @examId
                     ) THEN 1
                ELSE 0
           END
       )  AS Training,
       Retired,
       InternalUser
FROM   Users
)



GO
