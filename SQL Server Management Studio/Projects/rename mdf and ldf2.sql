--EXECUTE xp_instance_regread N'HKEY_LOCAL_MACHINE',N'Software\Microsoft\MSSQLServer\MSSQLServer',N'DefaultData', @DefaultDataLoc OUTPUT;
--EXECUTE xp_instance_regread N'HKEY_LOCAL_MACHINE',N'Software\Microsoft\MSSQLServer\MSSQLServer',N'DefaultLog', @DefaultLogLoc OUTPUT;
DECLARE @setOffline nvarchar(MAX) = '';
DECLARE @AlterLocation nvarchar(MAX) = '';
DECLARE @moveLog nvarchar(MAX) = '';
DECLARE @setOnline nvarchar(MAX) = '';

DECLARE @DefaultDataLoc NVARCHAR(1000)
	   , @DefaultLogLoc NVARCHAR(1000);

select @DefaultDataLoc = CONVERT(SYSNAME, SERVERPROPERTY('instancedefaultdatapath'))
	, @DefaultLogLoc = CONVERT(SYSNAME, SERVERPROPERTY('instancedefaultlogpath'))

--select @DefaultDataLoc, @DefaultLogLoc

--DECLARE @Tables TABLE(DBName nvarchar(100), LogName nvarchar(250), LogFileName nvarchar(250),FileName nvarchar(250))
--INSERT @Tables
SELECT D.Name 
	  , F.Name
	  , F.physical_name
	  ,  case f.type_desc when 'ROWS' then   @DefaultDataLoc + SUBSTRING(F.physical_name,LEN(REVERSE(SUBSTRING(REVERSE(F.physical_name), CHARINDEX('\',REVERSE(F.physical_name)), LEN(F.physical_name))))+1,LEN(F.physical_name))
					 when 'LOG' then   @DefaultLogLoc + SUBSTRING(F.physical_name,LEN(REVERSE(SUBSTRING(REVERSE(F.physical_name), CHARINDEX('\',REVERSE(F.physical_name)), LEN(F.physical_name))))+1,LEN(F.physical_name))
	END as [alter file]
FROM sys.master_files as F
inner join sys.databases as D
on D.database_id = F.database_id 
where D.name in ('SecureAssess_Populated')



SELECT @setOffline += char(13) +  
	'ALTER DATABASE [' + DBName + '] SET OFFLINE;'
FROM @Tables 

SELECT @AlterLocation += char(13) + '
	ALTER DATABASE [' + DBName + '] MODIFY FILE (NAME = ' + LogName  + ', NewName='''+ FileName +''');'
FROM @Tables 

SELECT @setOnline += char(13) +  
	'ALTER DATABASE [' + DBName + '] SET ONLINE;'
FROM @Tables 


SELECT @moveLog += char(13) +  
	 'EXECUTE xp_cmdshell ''move /Y '+  LogFileName + ' '''+ FileName +''''
FROM @Tables 


PRINT(@setOffline);
PRINT(@AlterLocation);
PRINT(@moveLog);
PRINT(@setOnline);


declare @dynamicTest varchar(1000) = ''


select @dynamicTest +=CHAR(13) + '

select' + db
from (

select distinct  db
from #test
) A



print(@dynamicTest)