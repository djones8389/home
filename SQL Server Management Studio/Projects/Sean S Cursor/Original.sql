--USE [SecureAssess_Populated]

IF OBJECT_ID('tempdb..#FlagForReview') IS NOT NULL
	DROP TABLE #FlagForReview;

DECLARE @ExamSessionIDs TABLE (ESID INT);

INSERT @ExamSessionIDs
SELECT EST.ID
FROM ExamSessionTable EST
CROSS APPLY StructureXML.nodes('assessmentDetails/assessment/section') A(Structure)
INNER JOIN ExamSessionItemResponseTable ESIRT
	ON EST.ID = ESIRT.ExamSessionID
		AND a.Structure.value('(item/@id)[1]', 'nvarchar(10)') = ESIRT.ItemID
WHERE a.Structure.value('(item/@userAttempted)[1]', 'int') != ESIRT.ItemResponseData.value('(/p/@ua)[1]', 'int');

SELECT EST.ID
	, CAST(StructureXML AS XML) AS StructureXML
	, ItemResponseData
--INTO #FlagForReview
FROM ExamSessionTable EST
CROSS APPLY StructureXML.nodes('assessmentDetails/assessment/section') A(Structure)
INNER JOIN ExamSessionItemResponseTable ESIRT
	ON EST.ID = ESIRT.ExamSessionID
		AND A.Structure.value('(item/@id)[1]', 'nvarchar(10)') = ESIRT.ItemID
WHERE EST.ID IN (166333)
		SELECT ESID
		FROM @ExamSessionIDs
		);

select ID
	,cast(StructureXML as xml) StructureXML
FROM (
select distinct ID, cast(StructureXML as nvarchar(MAX)) StructureXML
from #FlagForReview
) A

DECLARE STRUCTUREXML CURSOR
FOR
SELECT ExamSessionID
	, ItemID
	, ItemResponseData.value('(/p/@ua)[1]', 'tinyint') AS userAttempted
FROM ExamSessionItemResponseTable
WHERE ExamSessionID IN (
		SELECT ESID
		FROM @ExamSessionIDs
		);

DECLARE @ESID INT
	, @ItemID NVARCHAR(10)
	, @UA INT;

OPEN STRUCTUREXML;

FETCH NEXT
FROM STRUCTUREXML
INTO @ESID
	, @ItemID
	, @UA;

WHILE @@FETCH_STATUS = 0
BEGIN
	PRINT @ItemID
	UPDATE #FlagForReview
	SET StructureXML.modify('replace value of (/assessmentDetails/assessment/section/item[@id = sql:variable("@ItemID")]/@userAttempted)[1] with sql:variable("@UA")')
	WHERE ID = @ESID

	FETCH NEXT
	FROM STRUCTUREXML
	INTO @ESID
		, @ItemID
		, @UA
END

CLOSE STRUCTUREXML;

DEALLOCATE STRUCTUREXML;

SELECT distinct #FlagForReview.ID, cast(#FlagForReview.ItemResponseData as nvarchar(MAX))
FROM #FlagForReview


select ID
	,cast(StructureXML as xml) StructureXML
FROM (
select distinct ID, cast(StructureXML as nvarchar(MAX)) StructureXML
from #FlagForReview
) A



--SELECT *
--	, StructureXML.value('data(/assessmentDetails/assessment/section/item/@userAttempted)[1]', 'tinyint') AS StructureXML
--	, ItemResponseData.value('(/p/@ua)[1]', 'int')
--FROM #FlagForReview;

--UPDATE ExamSessionTable
--SET StructureXML = B.StructureXML
--FROM ExamSessionTable AS A
--INNER JOIN #FlagForReview AS B
--	ON A.ID = B.ID
--WHERE A.ID = B.ID
--	AND A.ID IN (
--		SELECT ESID
--		FROM @ExamSessionIDs
--		);

--DROP TABLE #FlagForReview;