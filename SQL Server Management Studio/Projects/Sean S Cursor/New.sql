--USE [SecureAssess_Populated]

IF OBJECT_ID('tempdb..#FlagForReview') IS NOT NULL DROP TABLE #FlagForReview;
IF OBJECT_ID('tempdb..#StructureXML') IS NOT NULL DROP TABLE #StructureXML;

SELECT EST.ID
	,   ItemID
	,   ItemResponseData.value('(/p/@ua)[1]', 'tinyint') [ItemResponse]
INTO #FlagForReview
FROM ExamSessionTable EST
CROSS APPLY StructureXML.nodes('assessmentDetails/assessment/section/item') A(Structure)
INNER JOIN ExamSessionItemResponseTable ESIRT
	ON EST.ID = ESIRT.ExamSessionID
		AND a.Structure.value('(@id)[1]', 'nvarchar(10)') = ESIRT.ItemID
WHERE a.Structure.value('(@userAttempted)[1]', 'int') != ESIRT.ItemResponseData.value('(/p/@ua)[1]', 'int');

SELECT
	ExamSessionTable.ID
	, cast(StructureXML as XML) StructureXML
INTO #StructureXML
FROM ExamSessionTable
INNER JOIN #FlagForReview
on #FlagForReview.id = ExamSessionTable.ID;

select ID
	,cast(StructureXML as xml) StructureXML
FROM (
select distinct ID, cast(StructureXML as nvarchar(MAX)) StructureXML
from #StructureXML
) A


DECLARE STRUCTUREXML CURSOR
FOR
SELECT ID
	,   ItemID
	,   [ItemResponse]
FROM #FlagForReview

DECLARE @ESID INT, @ItemID NVARCHAR(10), @UA tinyint;

OPEN STRUCTUREXML;

FETCH NEXT
FROM STRUCTUREXML
INTO @ESID
	, @ItemID
	, @UA;

WHILE @@FETCH_STATUS = 0
BEGIN

	UPDATE #StructureXML
	SET StructureXML.modify('replace value of (/assessmentDetails/assessment/section/item[@id = sql:variable("@ItemID")]/@userAttempted)[1] with sql:variable("@UA")')
	FROM #StructureXML
	INNER JOIN #FlagForReview
	on #FlagForReview.id = #StructureXML.ID
	WHERE #StructureXML.ID = @ESID

	FETCH NEXT
	FROM STRUCTUREXML
	INTO @ESID
		, @ItemID
		, @UA
END

CLOSE STRUCTUREXML;

DEALLOCATE STRUCTUREXML;

SELECT distinct #FlagForReview.ID, #FlagForReview.ItemID
FROM #FlagForReview
INNER JOIN #StructureXML
on #StructureXML.id = #FlagForReview.ID

select ID
	,cast(StructureXML as xml) StructureXML
FROM (
select distinct ID, cast(StructureXML as nvarchar(MAX)) StructureXML
from #StructureXML
) A


/*
UPDATE ExamSessionTable
SET StructureXML = B.StructureXML
FROM ExamSessionTable A
INNER JOIN #StructureXML B
on A.id = b.ID
*/



--DROP TABLE #FlagForReview;
--DROP TABLE #StructureXML;



--SELECT * FROM #FlagForReview
--select * from #StructureXML