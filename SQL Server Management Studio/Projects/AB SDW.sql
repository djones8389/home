SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

SELECT ExamSessionTable.ID,
	CentreTable.CentreCode,
	CentreTable.CentreName,
	IB3QualificationLookup.QualificationRef,
	IB3QualificationLookup.QualificationName,
	ScheduledExamsTable.ExamRef AS [ExamReference],
	ScheduledExamsTable.Examname,
	ExamSessionTable.KeyCode,
	StartedTime.StateChangeDate AS [StartedDate],
	CompletedTime.StateChangeDate AS [CompletedDate],
	UserTable.CandidateRef,
	UserTable.Surname,
	UserTable.Forename,
	CASE WHEN VoidedTime.ID IS NULL THEN 0 ELSE 1 END AS [Voided],
	VoidJustificationLookupTable.Name AS [VoidReason]
FROM dbo.ScheduledExamsTable
INNER JOIN dbo.IB3QualificationLookup
ON ScheduledExamsTable.QualificationID = IB3QualificationLookup.ID
INNER JOIN dbo.CentreTable
ON ScheduledExamsTable.CentreID = CentreTable.ID
INNER JOIN dbo.ExamSessionTable
ON ScheduledExamsTable.ID = ExamSessionTable.ScheduledExamID
INNER JOIN dbo.UserTable
ON ExamSessionTable.UserID = UserTable.ID
INNER JOIN dbo.ExamStateChangeAuditTable
ON ExamSessionTable.ID = ExamStateChangeAuditTable.ExamSessionID
AND ExamStateChangeAuditTable.NewState IN (@ExamStates)
AND CAST(ExamStateChangeAuditTable.StateChangeDate AS date) BETWEEN CAST(@StartDate AS date) AND CAST(@EndDate AS date)
LEFT JOIN dbo.ExamStateChangeAuditTable AS [StartedTime]
ON ExamSessionTable.ID = StartedTime.ExamSessionID
	AND StartedTime.NewState = 6
LEFT JOIN (
	SELECT COUNT(ExamStateChangeAuditTable.ID) AS [No],
		ExamStateChangeAuditTable.ExamSessionID,
		ExamStateChangeAuditTable.NewState,
		MIN(ExamStateChangeAuditTable.StateChangeDate) AS [StateChangeDate]
	FROM dbo.ExamStateChangeAuditTable
	WHERE ExamStateChangeAuditTable.NewState = 9
	GROUP BY ExamStateChangeAuditTable.ExamSessionID,
		ExamStateChangeAuditTable.NewState) AS [CompletedTime]
ON ExamSessionTable.ID = CompletedTime.ExamSessionID
LEFT JOIN dbo.ExamStateChangeAuditTable AS [VoidedTime]
ON ExamSessionTable.ID = VoidedTime.ExamSessionID
AND VoidedTime.NewState = 10
LEFT JOIN dbo.VoidJustificationLookupTable
ON VoidedTime.StateInformation.value('(stateChangeInformation/reason)[1]', 'int') = VoidJustificationLookupTable.ID

UNION

SELECT 
	WAREHOUSE_ExamSessionTable.ExamSessionID,
	WAREHOUSE_CentreTable.CentreCode,
	WAREHOUSE_CentreTable.CentreName,
	IB3QualificationLookup.QualificationRef,
	IB3QualificationLookup.QualificationName,
	WAREHOUSE_ScheduledExamsTable.ExternalReference AS [ExamReference],
	WAREHOUSE_ScheduledExamsTable.Examname,
	WAREHOUSE_ExamSessionTable.KeyCode,
	WAREHOUSE_ExamSessionTable_Shreded.[started],
	completionDate,
	WAREHOUSE_UserTable.CandidateRef,
	WAREHOUSE_UserTable.Surname,
	WAREHOUSE_UserTable.Forename,
	CASE WHEN VoidedTime.ID IS NULL THEN 0 ELSE 1 END AS [Voided],
	VoidedTime.name AS [VoidReason]
FROM WAREHOUSE_ScheduledExamsTable
INNER JOIN dbo.IB3QualificationLookup
ON WAREHOUSE_ScheduledExamsTable.QualificationID = IB3QualificationLookup.ID
INNER JOIN WAREHOUSE_CentreTable
ON WAREHOUSE_CentreTable.ID = WAREHOUSE_ScheduledExamsTable.WAREHOUSECentreID
INNER JOIN WAREHOUSE_ExamSessionTable
on WAREHOUSE_ExamSessionTable.WAREHOUSEScheduledExamID = WAREHOUSE_ScheduledExamsTable.ID
INNER JOIN WAREHOUSE_UserTable
on WAREHOUSE_UserTable.ID = WAREHOUSE_ExamSessionTable.WAREHOUSEUserID
INNER JOIN WAREHOUSE_ExamSessionTable_Shreded
on WAREHOUSE_ExamSessionTable_Shreded.examSessionId = WAREHOUSE_ExamSessionTable.ID
LEFT JOIN dbo.VoidJustificationLookupTable VoidedTime
ON VoidedTime.ID = WAREHOUSE_ExamSessionTable_Shreded.examStateInformation.value('(stateChangeInformation/reason)[1]', 'int')
where WAREHOUSE_ExamSessionTable.ID IN (@SDWESID)



--select top 10 examsessionid, examStateInformation
--from WAREHOUSE_ExamSessionTable_Shreded
--where PreviousExamState = 10
--	and examSessionId = 1321576
	
--select top 10 id,ExamStateChangeAuditXml
--from WAREHOUSE_ExamSessionTable
--where PreviousExamState = 10
--	and id = 1321576