use Saxion_SurpassDataWarehouse

DECLARE @ExpandVersions int
,@centres nvarchar(65),@subjects nvarchar(4),@dateStartRange nvarchar(19),@dateEndRange nvarchar(19),@examVersion nvarchar(4),@CPID nvarchar(4000),@showScored nvarchar(1),@showNonScored nvarchar(1),@showUntagged nvarchar(4000),@tagGroups nvarchar(4000),@tagValues nvarchar(4000)
set @centres=N'40,50,34,49,36,35,44,41,37,30,39,47,42,45,54,38,48,53,46,43,52,33' set @subjects=N'1135'set @dateStartRange=N'2017-09-10 00:00:00' set @dateEndRange=N'2018-03-08 23:59:59'
set @examVersion=N'8916' set @CPID=NULL set @showScored=N'1'set @showNonScored=N'1'set @showUntagged=NULL set @tagGroups=NULL set @tagValues=NULL

IF @CPID = '' SET @CPID = NULL
IF @CPID IS NOT NULL SET @ExpandVersions = 1 ELSE SET @ExpandVersions = 0

IF OBJECT_ID('tempdb..#ES') IS NOT NULL DROP TABLE #ES
CREATE TABLE #ES(ExamSessionKey int
		,CentreKey int
		,QualificationKey int
		,ExamKey int
		,ExamVersionKey int
		,UserMarks float
		,Percentage float
		,CompletionDateTime datetime
		,isRescored bit)
INSERT INTO #ES
SELECT 
	ES.ExamSessionKey
	,ES.CentreKey
	,ES.QualificationKey
	,ES.ExamKey
	,ES.ExamVersionKey
	,ES.UserMarks
	,CASE WHEN ES.TotalMarksAvailable>0 THEN ES.UserMarks/ES.TotalMarksAvailable ELSE ES.UserMarks END
	,T.FullDateAlternateKey + ES.CompletionTime  CompletionDateTime
	,CASE WHEN EXISTS (SELECT * FROM FactExamSessionAudits FESA WHERE FESA.ExamSessionKey = ES.ExamSessionKey AND FESA.IsTemp = 0) THEN 1 ELSE 0 END isRescored
FROM	 
	dbo.FactExamSessions AS ES
	INNER JOIN dbo.DimTime AS T
		 ON ES.CompletionDateKey = T.TimeKey
WHERE
	(T.FullDateAlternateKey + ES.CompletionTime) BETWEEN @dateStartRange AND @dateEndRange
	AND ES.FinalExamState <> 10
	AND ES.ExcludeFromReporting <> 1
	AND ES.CentreKey IN (SELECT Value FROM dbo.fn_ParamsToList(@centres,0))
	AND ES.QualificationKey IN (SELECT Value FROM dbo.fn_ParamsToList(@subjects,0))
	AND ES.ExamVersionKey = @examVersion
	AND (
		(@showUntagged IS NULL AND @tagGroups IS NULL AND @tagValues IS NULL) -- no filter on Candidate Tags
		OR (
			(@showUntagged = 1 AND NOT EXISTS (SELECT * FROM CandidateTagValues CTV WHERE ES.CandidateKey = CTV.CandidateKey)) --untagged
			OR EXISTS(
				SELECT * 
				FROM 
					CandidateTagValues CTV 
					JOIN TagValues TV ON CTV.TagValueKey = TV.TagValueKey
				WHERE 
					ES.CandidateKey = CTV.CandidateKey
					AND (
						TV.TagTypeKey IN (SELECT Value FROM dbo.fn_ParamsToList(@tagGroups,0))
						OR CTV.TagValueKey IN (SELECT Value FROM dbo.fn_ParamsToList(@tagValues,0))
					)
			)
		)
	)
OPTION (MAXRECURSION 0)

IF OBJECT_ID('tempdb..#Items') IS NOT NULL DROP TABLE #Items
CREATE TABLE #Items(SessionCount int, Inx int, Inx5 int, ExamSessionKey int, ExamVersionKey int, CPID nvarchar(50), CPVersion int, OriginalVersion int, UserMarks float, MarkerMark float, FQRMark float, QuestionMarks float, QuestionTotalMarks float, QuestionName nvarchar(100), QuestionTypeKey int, CAQuestionTypeKey int, ViewingTime int, Attempted float, TestOrder int, CAId int, CompletionDateTime datetime, ModifiedBy int, Percentage float, IsDichotomous bit, Scored int, isRescored bit  PRIMARY KEY CLUSTERED (ExamSessionKey, CPID))

;WITH items AS (
-------------Query for NULL @unit and @LO -----------
	SELECT	
		NULL SessionCount
		,NULL Inx --NTILE(3) OVER(PARTITION BY ES.ExamVersionKey, QR.CPID, Q.CPVersion ORDER BY UserMarks DESC, QR.ExamSessionKey ASC) Inx
		,NULL Inx5 --,NTILE(5) OVER(PARTITION BY ES.ExamVersionKey, QR.CPID, Q.CPVersion ORDER BY UserMarks DESC, QR.ExamSessionKey ASC) Inx5
		,ES.ExamSessionKey
		,ES.ExamVersionKey
		,QR.CPID
		,QR.CPVersion CPVersion --IF @ExpandVersions=0 it will be overwritten below
		,QR.CPVersion OriginalVersion
		,ES.UserMarks
		,(SELECT TOP 1 AssignedMark FROM dbo.FactMarkerResponse MR WHERE MR.ExamSessionKey = QR.ExamSessionKey AND MR.CPID = QR.CPID ORDER BY SEQNO DESC) AssM
		,QR.Mark FQRMark
		,NULL AssM2
		,NULL QuestionTotalMarks
		,NULL QuestionName
		,NULL QuestionTypeKey
		,NULL CAQuestionTypeKey
		,NULL CAId
		,QR.ViewingTime
		,QR.Attempted
		,QR.ItemPresentationOrder
		,ES.CompletionDateTime
		,NULL ModifiedByUserKey
		,Percentage
		,NULL IsDichotomous
		,QR.Scored
		,isRescored
	FROM	
	#ES ES
	INNER JOIN	FactQuestionResponses QR
		 ON ES.ExamSessionKey = QR.ExamSessionKey
		 AND (@CPID IS NULL OR QR.CPID = @CPID)
		 AND( 
			(@showScored = 1 AND @showNonScored = 1)
			OR (@showNonScored = 1 AND QR.Scored = 0)
			OR (@showScored = 1 AND QR.Scored = 1)
		 )
) -- items contains 40 distinct items

select distinct examsessionkey from #ES
INSERT INTO #Items 
SELECT 
	SessionCount
	,Inx
	,Inx5
	,ExamSessionKey
	,ExamVersionKey
	,CPID
	,CPVersion
	,OriginalVersion
	,UserMarks
	,AssM
	,FQRMark
	,NULL QuestionMarks
	,QuestionTotalMarks
	,QuestionName
	,QuestionTypeKey
	,CAQuestionTypeKey
	,ViewingTime
	,Attempted	
	,ItemPresentationOrder
	,CAId
	,CompletionDateTime
	,ModifiedByUserKey
	,Percentage
	,IsDichotomous
	,Scored
	,isRescored
FROM items

select distinct I.CPID CPID1
	,I.CPVersion
from #Items i

-- #items contains 40 distinct items


IF @ExpandVersions = 0 BEGIN
	;WITH Versions AS (
		SELECT CPID, MAX(CPVersion) MaxVersion
		FROM #Items
		GROUP BY CPID
	)
	UPDATE #Items 
	SET 
		CPVersion = V.MaxVersion
	FROM 
		#Items I 
		JOIN Versions V ON I.CPID = V.CPID
END

select distinct I.CPID CPID2
	,I.CPVersion
from #Items i
where cpid = '5909P442622'

;WITH items2 AS (
	SELECT 
		I.ExamSessionKey
		,I.CPID
		,ROW_NUMBER() OVER(PARTITION BY ExamVersionKey, I.CPID, I.CPVersion ORDER BY Percentage DESC, ExamSessionKey ASC) Inx
		,NTILE(5) OVER(PARTITION BY ExamVersionKey, I.CPID, I.CPVersion ORDER BY UserMarks DESC, ExamSessionKey ASC) Inx5
		,Q.TotalMark * I.FQRMark AssM2
		,Q.TotalMark QuestionTotalMarks
		,Q.QuestionName
		,Q.QuestionTypeKey
		,Q.CAQuestionTypeKey
		,CASE WHEN Q.CAQuestionTypeKey IS NOT NULL THEN FirstVersion.ExternalId ELSE NULL END CAId
		,Q.ModifiedByUserKey
		,CASE 
			WHEN 
				Q.CAQuestionTypeKey IN (2,4,5,7,9,18)	--MCQ, Either/Or, Numerical Entry, Short Answer, Select from a list, Hotspot
				AND Q.MarkingTypeKey=0				--ComputerMarked
				AND I.MarkerMark IS NULL			--Hadn't been overriden
				THEN 1 
			ELSE 0 
		 END IsDichotomous
	FROM 
		#Items I
		JOIN DimQuestions Q 
			ON I.CPID = Q.CPID
			AND I.CPVersion = Q.CPVersion
		OUTER APPLY (SELECT TOP 1 ExternalId FROM DimQuestions DQ2 WHERE DQ2.CPID = I.CPID ORDER BY CPVersion ASC) FirstVersion

)  -- 40 items in items2

select distinct cpid--, cpversion
from items2
where QuestionTotalMarks = 0



UPDATE I
SET
	 Inx = I2.Inx
	,Inx5 = I2.Inx5
	,QuestionMarks = 
		CASE WHEN isRescored = 1 
			THEN I2.AssM2
			ELSE ISNULL(I.MarkerMark, I2.AssM2)
		END
	,QuestionTotalMarks = I2.QuestionTotalMarks
	,QuestionName = I2.QuestionName
	,QuestionTypeKey = I2.QuestionTypeKey
	,CAQuestionTypeKey = I2.CAQuestionTypeKey
	,CAId = I2.CAId
	,ModifiedBy = I2.ModifiedByUserKey
	,IsDichotomous = I2.IsDichotomous
FROM 
	#Items I 
	JOIN items2 I2 
		ON I.CPID = I2.CPID
		AND I.ExamSessionKey = I2.ExamSessionKey

select distinct I.CPID CPID3
	,I.CPVersion
from #Items i  -- has 40


DELETE FROM #Items WHERE QuestionTotalMarks=0

select distinct I.CPID CPID4
	,I.CPVersion
from #Items i -- has 10

;WITH SessionCounts AS (
	SELECT 
		ExamVersionKey
		, CPID
		, CPVersion
		, COUNT(DISTINCT ExamSessionKey) SessionCount 
	FROM #Items
	GROUP BY ExamVersionKey, CPID, CPVersion
)
UPDATE #Items 
SET 
	SessionCount = SC.SessionCount
FROM 
	#Items I 
	JOIN SessionCounts SC ON 
		I.ExamVersionKey = SC.ExamVersionKey
		AND I.CPID = SC.CPID
		AND I.CPVersion = SC.CPVersion


		
;WITH FV_Top AS (
	SELECT 
		CPID
		,CPVersion
		,AVG(QuestionMarks/QuestionTotalMarks) FV
	FROM 
		#Items 
	WHERE 
		Inx<=ROUND(SessionCount/3., 0)
	GROUP BY 
		CPID
		,CPVersion
), FV_Bottom AS (
	SELECT 
		CPID
		,CPVersion
		,AVG(QuestionMarks/QuestionTotalMarks) FV
	FROM 
		#Items 
	WHERE 
		Inx>=SessionCount-ROUND(SessionCount/3., 0)+1
	GROUP BY 
		CPID
		,CPVersion
), FVQs AS (
	SELECT 
		Inx5 Inx
		,CPID
		,CPVersion
		,AVG(QuestionMarks/QuestionTotalMarks) FV
	FROM #Items
	GROUP BY 
		Inx5
		,CPID
		,CPVersion
)
, PearsonsR_raw AS (
	SELECT 
		CPID
		,CPVersion
		,(UserMarks - AVG(UserMarks) OVER(PARTITION BY CPID,CPVersion)) dx
		,(QuestionMarks - AVG(QuestionMarks) OVER(PARTITION BY CPID,CPVersion)) dy
	FROM #Items
), PearsonsR AS (
	SELECT 
		CPID
		,CPVersion
		,SUM(dx*dy)/SQRT(SUM(dx*dx)*SUM(dy*dy)) R
	FROM PearsonsR_raw
	GROUP BY CPID,CPVersion
	HAVING SQRT(SUM(dx*dx)*SUM(dy*dy))<>0
)
SELECT (
	SELECT 
		ESI.ExamKey AS [@Key]
		,DE.ExamReference AS [@Ref]
		,DE.ExamName AS [@Name]
		,ESI.ExamSessionKey AS [@SessionKey]
		,ESI.CompletionDateTime AS [@Date]
		,ESI.CentreKey AS [Centre/@Key]
		,DC.CentreCode AS [Centre/@Ref]
		,DC.CentreName AS [Centre/@Name]
		,ESI.QualificationKey AS [Qualification/@Key]
		,DQ.QualificationRef AS [Qualification/@Ref]
		,DQ.QualificationName AS [Qualification/@Name]
	FROM 
		#ES ESI
		JOIN DimExams DE
			ON DE.ExamKey = ESI.ExamKey
		JOIN DimCentres DC
			ON DC.CentreKey = ESI.CentreKey
		JOIN DimQualifications DQ
			ON DQ.QualificationKey = ESI.QualificationKey
	WHERE ESI.ExamSessionKey = (
		SELECT MAX(ExamSessionKey) 
		FROM 
			#Items ESII 
		WHERE 
			ESII.CompletionDateTime = (
				SELECT MAX(ESO.CompletionDateTime) 
				FROM #Items ESO 
				/*WHERE
					(CPID = @CPID OR @CPID IS NULL)
					AND (CPVersion = @CPVersion OR @ExpandVersions=0)
					*/
			)
	)	
	FOR XML PATH('LastUsedExam'), TYPE
)
,( 
	SELECT * FROM (
		SELECT 
			1 AS [Tag]
			,NULL AS [Parent]
			,NULL AS [Items!1] -- root element
			,NULL AS [Item!2!CPID]
			,NULL AS [Item!2!CPVersion]
			,NULL AS [Item!2!VersionCount]
			,NULL AS [Item!2!CAID]
			,NULL AS [Item!2!Name]
			,NULL AS [Item!2!Type]
			,NULL AS [Item!2!QuestionTypeKey]
			,NULL AS [Item!2!AuthoringQuestionTypeKey]
			,NULL AS [Item!2!TotalMark]
			,NULL AS [Item!2!AverageMark]
			,NULL AS [Item!2!PercentUnAnswered]
			,NULL AS [Item!2!FacilityValue]
			,NULL AS [Item!2!DI]
			,NULL AS [Item!2!AttemptedCount]
			,NULL AS [Item!2!ViewedUnattemptedCount]
			,NULL AS [Item!2!NotViewedCount]
			,NULL AS [Item!2!AverageViewingTime]
			,NULL AS [Item!2!TestOrder]
			,NULL AS [Item!2!TestOrderIsFixed]
			,NULL AS [Item!2!LastModifierUserId]
			,NULL AS [Item!2!LastModifierFirstName]
			,NULL AS [Item!2!LastModifierLastName]
			,NULL AS [Item!2!PearsonsR]
			,NULL AS [Item!2!IsDichotomous]
			,NULL AS [Item!2!Scored]
			,NULL AS [Item!2!ScoredChanged]
			,NULL AS [Quintile!3!Quintile]
			,NULL AS [Quintile!3!FacilityValue]

		UNION ALL

		SELECT 
			2
			,1
			,NULL
			,I.CPID
			,I.CPVersion
			,COUNT(DISTINCT I.OriginalVersion) VersionCount
			,I.CAId
			,I.QuestionName 
			,DQT.QuestionType 
			,I.QuestionTypeKey
			,I.CAQuestionTypeKey
			,AVG(I.QuestionTotalMarks)
			,AVG(I.QuestionMarks) 
			,(1 - (SUM(CAST(I.Attempted AS FLOAT)) / COUNT(I.CPID)))
			,AVG(I.QuestionMarks/I.QuestionTotalMarks) 
			,(FV1.FV - FV3.FV)
			--,COUNT(I.CPID)
			,CAST(SUM(I.Attempted) AS int) [Item!2!AttemptedCount]
			,SUM(CASE WHEN I.Attempted = 0 AND I.ViewingTime>0 THEN 1 ELSE 0 END) [Item!2!ViewedUnattemptedCount]
			,SUM(CASE WHEN I.Attempted = 0 AND I.ViewingTime=0 THEN 1 ELSE 0 END) [Item!2!NotViewedCount]
			,AVG(CAST (I.ViewingTime as bigint))
			,MIN(I.TestOrder)
			--,CASE WHEN VAR(I.TestOrder)<>0 THEN -1 ELSE MIN(I.TestOrder) END 
			,CASE WHEN VAR(I.TestOrder)<>0 THEN 0 ELSE 1 END OrderIsFixed
			,I.ModifiedBy AS [Item!2!LastModifierUserId]
			,DCU.FirstName AS [Item!2!LastModifierFirstName]
			,DCU.LastName AS [Item!2!LastModifierLastName]
			,R.R AS [Item!2!PearsonsR]
			,MIN(IsDichotomous+0) AS [Item!2!IsDichotomous]
			,CASE WHEN ISNULL(VAR(Scored),0)=0 THEN MIN(Scored) ELSE NULL END [Item!2!Scored]
			,CASE WHEN ISNULL(VAR(Scored),0)=0 THEN 0 ELSE 1 END [Item!2!ScoredChanged]
			,NULL
			,NULL
		FROM #Items I
			JOIN DimQuestionTypes DQT 
				ON DQT.QuestionTypeKey = I.QuestionTypeKey
			LEFT JOIN FV_Top FV1 
				ON I.CPID = FV1.CPID
				AND I.CPVersion = FV1.CPVersion
			LEFT JOIN FV_Bottom FV3 
				ON I.CPID = FV3.CPID
				AND I.CPVersion = FV3.CPVersion
			LEFT JOIN DimContentUsers DCU
				ON DCU.UserKey = I.ModifiedBy
			LEFT JOIN PearsonsR R
				ON R.CPID = I.CPID 
				AND R.CPVersion = I.CPVersion
		GROUP BY 
			I.CPID
			,I.CPVersion
			,I.CAId
			,I.QuestionName
			,DQT.QuestionType
			,I.QuestionTypeKey
			,(FV1.FV - FV3.FV)
			,I.CAQuestionTypeKey
			,I.ModifiedBy 
			,DCU.FirstName
			,DCU.LastName
			,R.R
		UNION ALL

		SELECT 
			3 AS [Tag]
			,2 AS [Parent]
			,NULL AS [Items!1]
			,FVQs.CPID AS [Item!2!CPID]
			,FVQs.CPVersion AS [Item!2!CPVersion]
			,NULL AS [Item!2!VersionCount]
			,NULL AS [Item!2!CAID]
			,NULL AS [Item!2!Name]
			,NULL AS [Item!2!Type]
			,NULL AS [Item!2!QuestionTypeKey]
			,NULL AS [Item!2!authQuestionTypeKey]
			,NULL AS [Item!2!TotalMark]
			,NULL AS [Item!2!AverageMark]
			,NULL AS [Item!2!PercentUnAnswered]
			,NULL AS [Item!2!FacilityValue]
			,NULL AS [Item!2!DI]
			,NULL AS [Item!2!AttemptedCount]
			,NULL AS [Item!2!ViewedUnattemptedCount]
			,NULL AS [Item!2!NotViewedCount]
			,NULL AS [Item!2!AverageViewingTime]
			,NULL AS [Item!2!TestOrder]
			,NULL AS [Item!2!TestOrderIsFixed]
			,NULL AS [Item!2!LastModifierUserId]
			,NULL AS [Item!2!LastModifierFirstName]
			,NULL AS [Item!2!LastModifierLastName]
			,NULL AS [Item!2!PearsonsR]
			,NULL AS [Item!2!IsDichotomous]
			,NULL AS [Item!2!Scored]
			,NULL AS [Item!2!ScoredChanged]
			,FVQs.Inx 
			,FVQs.FV AS [@FacilityValue]
		FROM FVQs

	) t
	ORDER BY [Item!2!CPID], [Item!2!CPVersion], Tag, [Quintile!3!Quintile]
	FOR XML EXPLICIT, TYPE
)
FOR XML PATH('Report')
OPTION (RECOMPILE)

--select * from #ES
--select * from #Items

--IF OBJECT_ID('tempdb..#ES') IS NOT NULL DROP TABLE #ES
--IF OBJECT_ID('tempdb..#Items') IS NOT NULL DROP TABLE #Items
