	SELECT A.ID
		, b.c.value('.','nvarchar(MAX)')	CheckedInComment
		, ROW_NUMBER() OVER(partition by a.id order by (SELECT 1)) R
	FROM (
		SELECT id, cast(itemvalue as xml) itemValue
		FROM Pagetable
	) A
	cross apply itemValue.nodes('ItemValue/metaData/HISTORY/USER/checkedInCmt') b(c)

	where b.c.value('.','nvarchar(MAX)') <> ''


SELECT A.ID
	--, itemValue.value('data(/ItemValue/metaData/HISTORY/USER/checkedInCmt[(text())])[1]','varchar(200)')
	--, itemValue.value('/ItemValue/metaData/HISTORY/USER/checkedInCmt [last()]/text())','varchar(200)')
	, itemValue.value('/ItemValue/metaData/HISTORY/USER/checkedInCmt[last()]/text()','varchar(200)')
	, itemValue
FROM (
	SELECT id, cast(itemvalue as xml) itemValue
	FROM Pagetable
) A


--where itemValue.exist('/ItemValue/metaData/HISTORY/USER/checkedInCmt') = 1

--[(text())]