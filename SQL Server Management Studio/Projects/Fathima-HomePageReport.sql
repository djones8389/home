exec sp_MSforeachdb '

USE [?];

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

IF(''?'' like ''%ContentProducer'' OR ''?'' like ''%CPProjectAdmin'' )

SELECT DISTINCT
	 DB_NAME() as ''Database''
	  , Forename
	  , Surname
	  , Username
FROM  dbo.Permission P
  
  INNER JOIN dbo.RolePermission RP
  on RP.PermissionID = P.ID
  
  INNER JOIN dbo.UserRole UR
  on UR.RoleID = RP.RoleID
  
  INNER JOIN dbo.UserTable UT
  on UT.UserID = UR.UserID
  
WHERE P.UniqueName = ''EDIT_HOME_PAGE''
	AND Email not like ''%btl%''
	And AccountLocked = ''0''
'


exec sp_MSforeachdb '

USE [?];

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

IF(''?'' like ''%ItemBank'')

SELECT DB_NAME()    as ''Database''  
  ,[FirstName]
  ,[LastName]
  ,[UserName]      
FROM dbo.UserTable UT
INNER JOIN  dbo.RoleLookupTable RLT
on RLT.Role = UT.Role

where RLT.Name = ''Administrator''
	AND Email not like ''%btl%''
	And AccountLocked = ''0''
'

exec sp_MSforeachdb '

USE [?];

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

IF(''?'' like ''%SecureAssess'')

SELECT DB_NAME() as ''Database''     
  ,Forename
  ,Surname
  ,[UserName]
from dbo.AssignedUserRolesTable AURT
INNER JOIN RolesTable RT
on RT.ID = AURT.RoleID
INNER JOIN RolePermissionsTable RPT
on RPT.RoleID = RT.ID
INNER JOIN PermissionsTable PT
on PT.ID = RPT.PermissionID
INNER JOIN UserTable UT
on UT.ID = AURT.UserID
where PT.Name = ''WelcomePageEditor''
and UT.Retired =''0''
	AND Email not like ''%btl%''
	AND AccountExpiryDate > GETDATE()
'


exec sp_MSforeachdb '

USE [?];

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

IF(''?'' like ''%SecureMarker'' )

SELECT DB_NAME()  as ''Database''   
  ,Forename
  ,Surname
  ,[UserName]
from dbo.AssignedUserRoles AURT
INNER JOIN Roles RT
on RT.ID = AURT.RoleID
INNER JOIN RolePermissions RPT
on RPT.RoleID = RT.ID
INNER JOIN Permissions PT
on PT.ID = RPT.PermissionID
INNER JOIN Users UT
on UT.ID = AURT.UserID
where PT.Name =''Welcome Message Editor''
	AND Email not like ''%btl%''
	AND ExpiryDate > GETDATE()
'