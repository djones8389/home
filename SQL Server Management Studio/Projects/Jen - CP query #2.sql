SELECT A.ItemID
	, 'Scenes' [Setting]
FROM (
select substring(id,0,charindex('s',id)) ItemID
	, max(substring(id,charindex('s',id)+1,10)) Scene
from SceneTable
where substring(id,charindex('s',id)+1,10) <> 1
group by substring(id,0,charindex('s',id))
) A

UNION ALL

SELECT DISTINCT substring(id,0,charindex('s',id)) ItemID
	, 'Scene Conditions' [Setting]
FROM ItemSceneConditionTable

UNION ALL

SELECT DISTINCT substring(id,0,charindex('s',id)) ItemID 
	, 'Scene Timer' [Setting]
from SceneTable 
where sct <> 0   

UNION ALL

select DISTINCT substring(id,0,charindex('s',id)) ItemID 
	 , 'Shared Answer Sets' [Setting] 
from [ComponentTable]
where shA IS NOT NULL
	and shA <> ''

UNION ALL

SELECT ItemID
	, 'RightToLeft' [Setting]
FROM (
	select DISTINCT substring(id,0,charindex('s',id)) ItemID from [ItemVideoTable] where rtl = 1 union
	select DISTINCT substring(id,0,charindex('s',id)) ItemID from [ItemTextSelectorTable] where rtl = 1 union
	select DISTINCT substring(id,0,charindex('s',id)) ItemID from [ItemTextBoxTable] where rtl = 1 union
	select DISTINCT substring(id,0,charindex('s',id)) ItemID from [ItemReorderingTable] where rtl = 1 union
	select DISTINCT substring(id,0,charindex('s',id)) ItemID from [ItemPicklistTable] where rtl = 1 union
	select DISTINCT substring(id,0,charindex('s',id)) ItemID from [ItemMultipleChoiceTable] where rtl = 1 union
	select DISTINCT substring(id,0,charindex('s',id)) ItemID from [ItemImageMapTable] where rtl = 1 union
	select DISTINCT substring(id,0,charindex('s',id)) ItemID from [ItemHotSpotTable] where rtl = 1 union
	select DISTINCT substring(id,0,charindex('s',id)) ItemID from [ItemGraphicTable] where rtl = 1 union
	select DISTINCT substring(id,0,charindex('s',id)) ItemID from [ItemGapfillTable] where rtl = 1 union
	select DISTINCT substring(id,0,charindex('s',id)) ItemID from [ItemDragAndDropTable] where rtl = 1 union
	select DISTINCT substring(id,0,charindex('s',id)) ItemID from [ItemDocumentTable] where rtl = 1 union
	select DISTINCT substring(id,0,charindex('s',id)) ItemID from [ItemAudioRecorderTable] where rtl = 1 
) A