use master

declare @mytable table (
	db sysname
	, esid int
)

insert @mytable
exec sp_msforeachdb '

SET QUOTED_IDENTIFIER ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

use [?];

if(''?'' like ''%SecureAssess'')

BEGIN

	SELECT distinct db_name()
		, warehouse_examsessionitemresponsetable.warehouseexamsessionid
	FROM warehouse_examsessionitemresponsetable
	where warehouseexamsessionid in (
		select examsessionid
		from warehouse_examsessiontable_shreded 
		where usermark = 0
			and previousexamstate <> 10
			and warehousetime > DATEADD(MONTH, -6, getdate())
	)
	and itemresponsedata.exist(''p[@um>0]'') = 1
END

'

SELECT *
FROM @mytable
ORDER BY 1;