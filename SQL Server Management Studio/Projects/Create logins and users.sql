SELECT DISTINCT

	'IF NOT EXISTS (SELECT 1 FROM Sys.sql_logins where name =''' + Name + '_Login' + ''')  CREATE LOGIN [' + Name + '_Login] WITH PASSWORD = ''Password'';' +
    'USE ' + QUOTENAME(Name) + ';  IF EXISTS (SELECT 1 FROM sys.database_principals where name = '''+Name + '_User'') DROP USER ' + Name + '_User' + ' 
	
	CREATE USER ' + Name + '_User' + ' FOR LOGIN ' + Name + '_Login' +
    '; EXEC sp_addrolemember ''db_owner'',  ''' + Name + '_User''  ' 

from sys.databases
where name like '%PRV_BCG%';


/*exec sp_defaultdb @loginame = 'Tom.Hatton', @defdb = 'PRV_AAT_ContentProducer'*/

DECLARE @command1 nvarchar(1000) = '

USE [?];

select ''?''  as DBName
		, u.name as ''User''
		, (select SUSER_SNAME (u.sid)) as ''Login''
		, r.name  as RoleName
		--, u.*
from sys.database_role_members RM 
	right join sys.database_principals U on U.principal_id = RM.member_principal_id
	left join sys.database_principals R on R.principal_id = RM.role_principal_id
where u.type<>''R''
	and U.type = ''S''
	and r.name IS NOT NULL
	and ''?'' not in (''msdb'',''tempdb'',''model'',''master'')
'


DECLARE @UserLogins TABLE (DBName nvarchar(150), userName nvarchar(150), loginName nvarchar(150), RoleName nvarchar(50))
INSERT @UserLogins
EXEC sp_MSforeachdb  @command1


SELECT DISTINCT 'USE '  + QUOTENAME(DBNAME) +  ' DROP USER ' + QUOTENAME(Username) ';'
FROM @UserLogins
where dbname like '%prv_bcguilds%'
	and username like '%britishcouncil%'


USE [PRV_BCGuilds_SecureAssess] DROP USER [PRV_BritishCouncil_ETL_User]
USE [PRV_BCGuilds_SecureMarker] DROP USER [PRV_BritishCouncil_ETL_User]
USE [PRV_BCGuilds_SurpassDataWarehouse] DROP USER [PRV_BritishCouncil_ETL_User]
USE [PRV_BCGuilds_TestPackage] DROP USER [PRV_BritishCouncil_ETL_User]


--USE [STG_SQA2_SurpassDataWarehouse] DROP USER [yuriyb]
*/