use ICAEW_SecureAssess
	
set transaction isolation level read uncommitted;

	SELECT WESIRT.ID	
		, WESIRT.WAREHOUSEExamSessionID
		, Keycode
		, ItemID
		, itemresponsedata
	INTO #itemids
	FROM WAREHOUSE_ExamSessionItemResponseTable wesirt (READUNCOMMITTED)
	inner join WAREHOUSE_ExamSessionTable west (READUNCOMMITTED)
	on west.id = wesirt.warehouseexamsessionid
	where warehouseTime > '2017-09-10'
		and  ItemResponseData.exist('p/s/c[@typ=20]') = 1
	

	CREATE CLUSTERED INDEX [ID] on [#itemids](ID);

	--ColWidthStorer
	
	select ID 
		, a.b.value('@w','int') [ColWidth]
		, ROW_NUMBER() over (partition by ID, WAREHOUSEExamSessionID, itemid order by (SELECT 1)) ColNo
	INTO [DJ_ColWidthLookup]
	FROM  #itemids

	CROSS APPLY ItemResponseData.nodes('p/s/c/i/t/r/c[@w]') a(b)

	CREATE CLUSTERED INDEX [ColWidth] on [DJ_ColWidthLookup](ID);
	CREATE NONCLUSTERED INDEX [ColWidth2] on [DJ_ColWidthLookup](ID, ColNo,ColWidth);

	

	
	select 
		c.ID
		, KeyCode
		, ItemID
		, C.ColNo
		, ColWidth.ColWidth
		, C.RowNumber
		, C.RowHeight
		, C.Response		
	FROM (
		select b.ID
			, ROW_NUMBER() over(partition by ID, RowNumber order by (SELECT 1)) ColNo
			, RowNumber
			, RowHeight
			, Response
		FROM (
			SELECT A.*
				, n.f.query('.').value('data(.)[1]','nvarchar(MAX)') Response
			FROM (
				SELECT ID
					, a.b.value('@h[1]','nvarchar(max)') [RowHeight]
					, a.b.value('.','nvarchar(max)') FullRowResponse
					, a.b.query('.').query('r/c') x
					, ROW_NUMBER() over (partition by id, warehouseexamsessionid, itemID order by (SELECT 1)) RowNumber
				FROM  #itemids
			
				CROSS APPLY ItemResponseData.nodes('p/s/c/i/t/r') a(b)
			) A	

			CROSS APPLY x.nodes('c') n(f)

			WHERE FullRowResponse <> ''

		) b
	) c
	LEFT JOIN [DJ_MergeExcluder] Exclude
	on Exclude.ID =  C.ID
		and Exclude.ColNum = C.ColNo
		and Exclude.RowNum = C.RowNumber
	INNER JOIN [DJ_ColWidthLookup] ColWidth
	on ColWidth.ID = C.ID
		and ColWidth.ColNo = C.ColNo
    INNER JOIN #itemids I
	ON I.ID = C.ID
	where Response <> ''
		and Exclude.ID IS NULL





		/*

	SELECT A.*
	FROM (
		SELECT ID
			, len(a.b.value('.','nvarchar(max)')) [RowTextLength]
			, a.b.value('@h[1]','nvarchar(max)') [RowHeight]
			, a.b.value('.','nvarchar(max)') [Response]
			, a.b.query('.')e
			--,c.d.query('.') j
			, ROW_NUMBER() over (partition by id, warehouseexamsessionid, itemID order by (SELECT 1)) RowNumber
		FROM  #itemids

		CROSS APPLY ItemResponseData.nodes('p/s/c/i/t/r') a(b)
		--CROSS APPLY a.b.nodes('c') c(d)
	) A
	WHERE [Response] <> ''


	--Row numbers working

	SELECT A.*
	FROM (
		SELECT ID
			, len(a.b.value('.','nvarchar(max)')) [RowTextLength]
			, a.b.value('@h[1]','nvarchar(max)') [RowHeight]
			, a.b.value('.','nvarchar(max)') [FullResponse]
			, a.b.query('.').value('data(//text)[1]','nvarchar(MAX)') Col1
			, a.b.query('.').value('data(//text)[2]','nvarchar(MAX)') Col2
			, a.b.query('.').value('data(//text)[3]','nvarchar(MAX)') Col3
			, a.b.query('.').value('data(//text)[4]','nvarchar(MAX)') Col4
			, a.b.query('.').value('data(//text)[5]','nvarchar(MAX)') Col5
			, a.b.query('.').value('data(//text)[6]','nvarchar(MAX)') Col6
			, a.b.query('.').value('data(//text)[7]','nvarchar(MAX)') Col7
			, a.b.query('.').value('data(//text)[8]','nvarchar(MAX)') Col8
			, ROW_NUMBER() over (partition by id, warehouseexamsessionid, itemID order by (SELECT 1)) RowNumber
		FROM  #itemids

		CROSS APPLY ItemResponseData.nodes('p/s/c/i/t/r') a(b)

	) A
	WHERE [FullResponse] <> ''



	--Col numbers working

	select D.*
	FROM (
	SELECT ID	
		, c.d.value('.','nvarchar(max)') [ColText]
		--, len(c.d.value('.','nvarchar(max)')) [ColTextLength]
		,  ROW_NUMBER() OVER(partition by ID, WAREHOUSEExamSessionID, itemid, CAST(c.d.query('..') as nvarchar(MAX)) ORDER BY (SELECT 1)) [ColNo] 
	FROM #itemids

	CROSS APPLY ItemResponseData.nodes('p/s/c/i/t/r/c') c(d)

		WHERE ISNULL(c.d.value('./@typ', 'tinyint'), 3) = 3 --filters WP
	) D	
	where [ColText] <> ''


	*/
