SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;


use ICAEW_SecureAssess
SELECT WESIRT.ID	
	, WESIRT.WAREHOUSEExamSessionID
	, WESIRT.ItemID
	, c.d.value('.','nvarchar(max)') [ColText]
	, len(c.d.value('.','nvarchar(max)')) [ColTextLength]
	, ROW_NUMBER() OVER(partition by WESIRT.ID, WESIRT.WAREHOUSEExamSessionID, WESIRT.itemid,CAST(c.d.query('..') as nvarchar(MAX)) ORDER BY CAST(c.d.query('..') as nvarchar(MAX))) [ColNo] 
	, ItemResponseData
FROM  warehouse_examsessionItemResponseTable WESIRT (NOLOCK)
	
inner join WAREHOUSE_ExamSessionTable west on west.id = wesirt.warehouseexamsessionid

CROSS APPLY  WESIRT.ItemResponseData.nodes('p/s/c/i/t/r/c') c(d)

	WHERE ISNULL(c.d.value('./@typ', 'tinyint'), 3) = 3 --filters WP
		and c.d.value('.','nvarchar(max)') <> ''	
		and WESIRT.itemid = '368P1037'
		and WEST.keycode = 'LFW4HWF6'



SELECT wesirt.warehouseexamsessionid		
		, ItemID
		, a.b.value('@w','int') [ColWidth]
		, ROW_NUMBER() over (order by (SELECT 1)) ColNo
FROM  warehouse_examsessionItemResponseTable WESIRT (NOLOCK)
	
inner join WAREHOUSE_ExamSessionTable west on west.id = wesirt.warehouseexamsessionid
	
CROSS APPLY WESIRT.ItemResponseData.nodes('p/s/c/i/t/r/c[@w]') a(b)

WHERE WESIRT.itemid = '368P1037'
		and WEST.keycode = 'LFW4HWF6'
