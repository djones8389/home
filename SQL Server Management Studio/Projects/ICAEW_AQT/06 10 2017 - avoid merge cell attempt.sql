SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

use ICAEW_SecureAssess

IF OBJECT_ID('tempdb..#itemids') is not null DROP TABLE #itemids;

--31 secs to populate

SELECT WESIRT.ID	
	, WESIRT.WAREHOUSEExamSessionID
	, Keycode
	, ItemID
	, itemresponsedata
INTO #itemids
FROM WAREHOUSE_ExamSessionItemResponseTable wesirt (READUNCOMMITTED)
inner join WAREHOUSE_ExamSessionTable west (READUNCOMMITTED)
on west.id = wesirt.warehouseexamsessionid
where warehouseTime > '2017-09-10'
	and  ItemResponseData.exist('p/s/c[@typ=20]') = 1;       

CREATE CLUSTERED INDEX [PK]	on #itemids (ID);
--where keycode = 'M489VCF6' and itemid = '368P1048'

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

SELECT [FULL].ID
	, KeyCode
	, ColCheck.ItemID
	, ColCheck.ColNo
	, ColText
	
FROM (

SELECT ID	
	, c.d.value('.','nvarchar(max)') [ColText]
	, len(c.d.value('.','nvarchar(max)')) [ColTextLength]
	, ROW_NUMBER() OVER(partition by ID, WAREHOUSEExamSessionID, itemid,CAST(c.d.query('..') as nvarchar(MAX)) ORDER BY CAST(c.d.query('..') as nvarchar(MAX))) [ColNo] 
	--, ItemResponseData
FROM #itemids

CROSS APPLY ItemResponseData.nodes('p/s/c/i/t/r/c') c(d)

	WHERE ISNULL(c.d.value('./@typ', 'tinyint'), 3) = 3 --filters WP
		and c.d.value('.','nvarchar(max)') <> ''	
		--and itemid = '368P1048' and keycode = 'M489VCF6'
		--and itemid = '368P1037' and keycode = 'LFW4HWF6'
		
) [FULL]

INNER JOIN (

SELECT ID
	   , warehouseexamsessionid		
	   , keycode
	   , ItemID
	   , a.b.value('@w','int') [ColWidth]
	   , ROW_NUMBER() over (partition by ID, WAREHOUSEExamSessionID, itemid order by (SELECT 1)) ColNo
FROM  #itemids

CROSS APPLY ItemResponseData.nodes('p/s/c/i/t/r/c[@w]') a(b)

--where itemid = '368P1048' and keycode = 'M489VCF6'
--where itemid = '368P1037' and keycode = 'LFW4HWF6'

) ColCheck

on [FULL].ID = ColCheck.ID
	and [FULL].ColNo = ColCheck.ColNo

INNER JOIN (
	--row numbers split out
	SELECT A.*
	FROM (
		SELECT ID
			, a.b.value('.','nvarchar(max)') [Response]
			, ROW_NUMBER() over (partition by id, warehouseexamsessionid, itemID order by (SELECT 1)) RowNumber
		FROM  #itemids

		CROSS APPLY ItemResponseData.nodes('p/s/c/i/t/r') a(b)
		--where itemid = '368P1037' and keycode = 'LFW4HWF6'
	) A
	WHERE [Response] <> ''
) RowInfo
on RowInfo.ID = ColCheck.ID
	and substring(RowInfo.Response, 0, 4) = substring([FULL].ColText, 0, 4)

LEFT JOIN (

	SELECT ID
		, a.b.value('@rows','nvarchar(100)')+1 [MergeRow]
	FROM  #itemids
	CROSS APPLY ItemResponseData.nodes('//merge/m') a(b)
	--where itemid = '368P1037' and keycode = 'LFW4HWF6'
) IgnoreMerge

on IgnoreMerge.ID = RowInfo.ID
	and IgnoreMerge.MergeRow = RowInfo.RowNumber
where ColTextLength > (ColWidth/4)*2	
	and IgnoreMerge.ID is null;