use icaew_secureAssess

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

--select distinct examsessionid
--from examstateChangeaudittable
--where statechangedate > '2017-09-12'

SELECT esirt.ExamSessionID		
		, ItemID
		, sum(a.b.value('@w','int')) [ColWidth]
FROM  ExamSessionItemResponseTable ESIRT  
	
CROSS APPLY ESIRT.ItemResponseData.nodes('p/s/c/i/t/r/c[@w]') a(b)

where esirt.examsessionid in (
	select distinct examsessionid
	from examstateChangeaudittable
	where statechangedate > '2017-09-12'
	)

group by 
		esirt.ID
		, ExamSessionID
		, ItemID
order by [ColWidth] desc


sp_whoisactive


use ICAEW_SecureAssess

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

SELECT EST.ID, ItemID, ItemResponseData
FROM ExamSessionTable EST
INNER JOIN ExamStateChangeAuditTable ESCAT WITH (NOLOCK) on ESCAT.ExamSessionID = EST.ID and NewState = 6
INNER JOIN ExamSessionItemResponseTable ESIRT  WITH (NOLOCK) ON ESIRT.ExamSessionID = EST.ID
where StateChangeDate > '2017-09-12 00:00:01'
	and ItemResponseData.exist('p/s/c[@typ=20]') = 1
	and EST.examState in (15,16,18,13)

/*
SELECT WEST.ExamSessionID, ItemID, ItemResponseData
FROM Warehouse_ExamSessionTable WEST
INNER JOIN WAREHOUSE_ExamSessionItemResponseTable WESIRT ON WESIRT.WAREHOUSEExamSessionID = WEST.ID
where warehouseTime > '2017-09-12 00:00:01'
	and ItemResponseData.exist('p/s/c[@typ=20]') = 1

--2615

*/