SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

use ICAEW_SecureAssess

	SELECT WESIRT.ID	
		, WESIRT.WAREHOUSEExamSessionID
		, ItemID
		, c.d.value('.','nvarchar(max)') [ColText]
		, ROW_NUMBER() OVER(partition by WESIRT.ID, WESIRT.WAREHOUSEExamSessionID, itemid,CAST(c.d.query('..') as nvarchar(MAX)) ORDER BY CAST(c.d.query('..') as nvarchar(MAX))) [ColNo] 
	FROM   WAREHOUSE_ExamSessionItemResponseTable WESIRT (NOLOCK)

	CROSS APPLY  WESIRT.ItemResponseData.nodes('p/s/c/i/t/r/c') c(d)

	--where  WAREHOUSEExamSessionID = 9836 and itemid in ('368P1041', '368P998')
		WHERE ISNULL(c.d.value('./@typ', 'tinyint'), 3) = 3 --filters WP
			and id = 77743
			and  c.d.value('.','nvarchar(max)')  <> ''
			--where itemid = '368p1037'
			--	and keycode = '9RPXW8F6'