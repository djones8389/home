SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

SELECT [Col].*
	, [RowHeight-Total]
FROM (
select WAREHOUSEExamSessionID
	, Keycode
	, ItemID
	, sum([ColumnWidth]) [ColumnWidth-Total]
from (
	select WAREHOUSEExamSessionID
		, Keycode
		, ItemID
		, a.b.value('@w','smallint') ColumnWidth
	from WAREHOUSE_ExamSessionItemResponseTable

	CROSS APPLY ItemResponseData.nodes('p/s//c[@w]') a(b)

	INNER JOIN (
		SELECT ID, KeyCode
		FROM WAREHOUSE_ExamSessionTable
		where KeyCode in ('V47MWVF6','CTRC97F6','YD9WG9F6','HYDGY3F6','F8X4KKF6')
     ) Keycodes
	  on Keycodes.id = WAREHOUSE_ExamSessionItemResponseTable.WAREHOUSEExamSessionID
  ) a
	group by 
		WAREHOUSEExamSessionID
		, Keycode
		, ItemID
) [Col]

INNER JOIN (
	select WAREHOUSEExamSessionID
		, Keycode
		, ItemID
		, sum([RowHeight-Total]) [RowHeight-Total]
	from (
	select WAREHOUSEExamSessionID
		, Keycode
		, ItemID
		, a.b.value('@h','smallint') [RowHeight-Total]
	from WAREHOUSE_ExamSessionItemResponseTable 

	CROSS APPLY ItemResponseData.nodes('p/s/c/i/t/r[@h]') a(b)

	INNER JOIN (
		SELECT ID, KeyCode
		FROM WAREHOUSE_ExamSessionTable
		where KeyCode in ('V47MWVF6','CTRC97F6','YD9WG9F6','HYDGY3F6','F8X4KKF6')
     ) Keycodes
	  on Keycodes.id = WAREHOUSE_ExamSessionItemResponseTable.WAREHOUSEExamSessionID
  ) a
	group by 
		WAREHOUSEExamSessionID
		, Keycode
		, ItemID
)  [Row]
on [Col].WAREHOUSEExamSessionID = [Row].WAREHOUSEExamSessionID
	and [Col].ItemID = [Row].ItemID;