use icaew_secureAssess

if OBJECT_ID('tempdb..#WarehouseExamSessionItemResponseTable') is not null drop table #WarehouseExamSessionItemResponseTable;

select WESIRT.*
INTO #WarehouseExamSessionItemResponseTable
FROM Warehouse_ExamSessionItemResponseTable WESIRT  WITH (NOLOCK)

INNER JOIN Warehouse_ExamSessionTable WEST  WITH (NOLOCK)
ON WEST.ID = WESIRT.WarehouseExamSessionID

where ItemResponseData.exist('p/s/c[@typ=20]') = 1
	and warehouseTime > DATEADD(WEEK, -2, GETDATE())
	--and Keycode = 'RVTFQHF6'


create clustered index [ix_examsessionid] on #WarehouseExamSessionItemResponseTable(WarehouseExamSessionID);
create nonclustered index [ix_examsessionid2] on #WarehouseExamSessionItemResponseTable(WarehouseExamSessionID,itemid);

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

SELECT WEST.id
	, Wesirt.ItemID
	, Keycode
	, c.d.value('.','nvarchar(max)') [text]
	, BiggestCol.ColWidth
	--, LEN(a.b.value('data(c/text)[1]','nvarchar(max)'))
	--, a.b.query('.')
	--, a.b.value('data(c/@typ)[1]','tinyint')
	, ItemResponseData
	--- ,a.b.value('data(c/@typ)[1]','tinyint') 
--INTO [DJ_WideColStorage2]

FROM  #WarehouseExamSessionItemResponseTable WESIRT  WITH (NOLOCK)

INNER JOIN Warehouse_ExamSessionTable WEST WITH (NOLOCK)
ON WEST.ID = WESIRT.WarehouseExamSessionID

CROSS APPLY WESIRT.ItemResponseData.nodes('p/s/c/i/t/r') a(b)
CROSS APPLY a.b.nodes('c/text') c(d)

INNER JOIN (
	SELECT Wesirt.WarehouseExamSessionID		
		, ItemID
		, sum(a.b.value('@w','int')) [ColWidth]
	FROM  #WarehouseExamSessionItemResponseTable WESIRT  
			
	CROSS APPLY WESIRT.ItemResponseData.nodes('p/s/c/i/t/r/c[@w]') a(b)

	group by 
			Wesirt.ID
			, WarehouseExamSessionID
				, ItemID
	) BiggestCol
	
	on BiggestCol.WarehouseExamSessionID = West.ID
		and BiggestCol.ItemID = WESIRT.ItemID

--where LEN(a.b.value('data(c/text)[1]','nvarchar(max)')) > 420
where --LEN(a.b.value('data(c/text)[1]','nvarchar(max)')) > [ColWidth]
	  --a.b.value('data(c/@typ)[1]','tinyint') != 4
	 KeyCode = '6TRMYJF6'
	 and wesirt.itemid = '368P1041'
	 --and a.b.value('data(c/@typ)[1]','tinyint') != 4
	 and  c.d.value('.','nvarchar(max)') like '%8667%'