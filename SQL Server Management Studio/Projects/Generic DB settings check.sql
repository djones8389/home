DECLARE @AutoGrow TABLE
(
	Name nvarChar(100)
	, fileID tinyint
	, filename nvarchar(1000)
	, filegroup nvarchar(100)
	, size nvarchar(100)
	, maxSize nvarchar(100)
	, growth  nvarchar(20)
	, usage nvarchar(30)

)
INSERT @AutoGrow
exec sp_MSforeachdb '

use [?];
IF (db_ID() > 4)
EXEC sp_helpfile  

'
select a.Name
	, SUSER_SNAME(owner_sid) [DB-Owner]
	, 'ALTER AUTHORIZATION ON database::'+quotename(a.name)+' TO sa;' [Update]
	--, 'ALTER AUTHORIZATION ON database::'+ quotename(a.name)+' TO '+SUSER_SNAME(owner_sid) + ';' [Rollback]
	--, compatibility_level
	--, collation_name
	--, user_access
	--, user_access_desc
	--, a.state_desc
	--, recovery_model_desc
	--, type_desc
	,c.growth
from sys.databases A
INNER JOIN sys.master_files B
on a.database_id = b.database_id
INNER JOIN @AutoGrow C
on b.physical_name COLLATE SQL_Latin1_General_CP1_CI_AS = c.filename
--INNER JOIN sys.syslogins S
--on s.sid = owner_sid
where a.database_id > 4
	and a.name like 'ppd_ocr%'
	and type_desc = 'ROWS'
	order by 1;


--ALTER AUTHORIZATION ON database::[PPD_OCR_ContentProducer] TO sa;
--ALTER AUTHORIZATION ON database::[PPD_OCR_ItemBank] TO sa;
--ALTER AUTHORIZATION ON database::[PPD_OCR_SecureAssess] TO sa;
--ALTER AUTHORIZATION ON database::[PPD_OcrDataManagement] TO sa;

/*UPDATE
ALTER AUTHORIZATION ON database::[E2E_EAL_ItemBank] TO sa;
ALTER AUTHORIZATION ON database::[E2E_EAL_SecureAssess] TO sa;
ALTER AUTHORIZATION ON database::[E2E_EAL_SurpassDataWarehouse] TO sa;
ALTER AUTHORIZATION ON database::[PPD_AAT_ContentProducer] TO sa;
ALTER AUTHORIZATION ON database::[PPD_AAT_ItemBank] TO sa;
ALTER AUTHORIZATION ON database::[PPD_AAT_SecureAssess] TO sa;
ALTER AUTHORIZATION ON database::[PPD_AAT_SecureMarker] TO sa;
ALTER AUTHORIZATION ON database::[PPD_AAT_SurpassDataWarehouse] TO sa;
ALTER AUTHORIZATION ON database::[PPD_BCGuildsIntegration_SecureAssess] TO sa;
ALTER AUTHORIZATION ON database::[PPD_BCGuildsIntegration_SecureMarker] TO sa;
ALTER AUTHORIZATION ON database::[PPD_BCGuildsIntegration_SurpassDataWarehouse] TO sa;
ALTER AUTHORIZATION ON database::[PPD_BCGuildsIntegration_TestPackage] TO sa;
ALTER AUTHORIZATION ON database::[PPD_BCIntegration_CPProjectAdmin] TO sa;
ALTER AUTHORIZATION ON database::[PPD_BCIntegration_eFlex] TO sa;
ALTER AUTHORIZATION ON database::[PPD_BCIntegration_EPCAdaptor] TO sa;
ALTER AUTHORIZATION ON database::[PPD_BCIntegration_ItemBank] TO sa;
ALTER AUTHORIZATION ON database::[PPD_BCIntegration_LocalScan] TO sa;
ALTER AUTHORIZATION ON database::[PPD_BCIntegration_PhoneExamSystemContext] TO sa;
ALTER AUTHORIZATION ON database::[PPD_BCIntegration_SecureAssess] TO sa;
ALTER AUTHORIZATION ON database::[PPD_BCIntegration_SecureAssess] TO sa;
ALTER AUTHORIZATION ON database::[PPD_BCIntegration_SecureMarker] TO sa;
ALTER AUTHORIZATION ON database::[PPD_BCIntegration_SurpassDataWarehouse] TO sa;
ALTER AUTHORIZATION ON database::[PPD_BCIntegration_TestPackage] TO sa;
ALTER AUTHORIZATION ON database::[PPD_NCFE_CPProjectAdmin] TO sa;
ALTER AUTHORIZATION ON database::[PPD_NCFE_Itembank] TO sa;
ALTER AUTHORIZATION ON database::[PPD_NCFE_SecureAssess] TO sa;
ALTER AUTHORIZATION ON database::[PRV_BCGuilds_SecureAssess] TO sa;
ALTER AUTHORIZATION ON database::[PRV_BCGuilds_SecureMarker] TO sa;
ALTER AUTHORIZATION ON database::[PRV_BCGuilds_SurpassDataWarehouse] TO sa;
ALTER AUTHORIZATION ON database::[PRV_BCGuilds_TestPackage] TO sa;
ALTER AUTHORIZATION ON database::[PRV_BritishCouncil_CPProjectAdmin] TO sa;
ALTER AUTHORIZATION ON database::[PRV_BritishCouncil_eFlex] TO sa;
ALTER AUTHORIZATION ON database::[PRV_BritishCouncil_EPCAdaptor] TO sa;
ALTER AUTHORIZATION ON database::[PRV_BritishCouncil_ItemBank] TO sa;
ALTER AUTHORIZATION ON database::[PRV_BritishCouncil_LocalScan] TO sa;
ALTER AUTHORIZATION ON database::[PRV_BritishCouncil_PhoneAssess] TO sa;
ALTER AUTHORIZATION ON database::[PRV_BritishCouncil_SecureAssess] TO sa;
ALTER AUTHORIZATION ON database::[PRV_BritishCouncil_SecureMarker] TO sa;
ALTER AUTHORIZATION ON database::[PRV_BritishCouncil_SurpassDataWarehouse_2] TO sa;
ALTER AUTHORIZATION ON database::[PRV_BritishCouncil_TestPackageManager] TO sa;
ALTER AUTHORIZATION ON database::[PRV_DEMO_CPProjectAdmin] TO sa;
ALTER AUTHORIZATION ON database::[PRV_DEMO_ItemBank] TO sa;
ALTER AUTHORIZATION ON database::[PRV_DEMO_OpenAssessAdmin] TO sa;
ALTER AUTHORIZATION ON database::[PRV_DEMO_SecureAssess] TO sa;
ALTER AUTHORIZATION ON database::[PRV_DEMO_SecureMarker] TO sa;
ALTER AUTHORIZATION ON database::[PRV_Demo_SurpassDataWarehouse] TO sa;
ALTER AUTHORIZATION ON database::[PRV_EAL_CPProjectAdmin] TO sa;
ALTER AUTHORIZATION ON database::[PRV_EAL_ItemBank] TO sa;
ALTER AUTHORIZATION ON database::[PRV_EAL_SecureAssess] TO sa;
ALTER AUTHORIZATION ON database::[PRV_Evolve_CPProjectAdmin] TO sa;
ALTER AUTHORIZATION ON database::[PRV_Evolve_ItemBank] TO sa;
ALTER AUTHORIZATION ON database::[PRV_Evolve_OpenAssessAdmin] TO sa;
ALTER AUTHORIZATION ON database::[PRV_Evolve_SecureAssess] TO sa;
ALTER AUTHORIZATION ON database::[PRV_NCFE_CPProjectAdmin] TO sa;
ALTER AUTHORIZATION ON database::[PRV_NCFE_Itembank] TO sa;
ALTER AUTHORIZATION ON database::[PRV_NCFE_SecureAssess] TO sa;
ALTER AUTHORIZATION ON database::[PRV_OCR_ASPState] TO sa;
ALTER AUTHORIZATION ON database::[PRV_OCR_ContentProducer] TO sa;
ALTER AUTHORIZATION ON database::[PRV_OCR_DataManagement] TO sa;
ALTER AUTHORIZATION ON database::[PRV_OCR_ItemBank] TO sa;
ALTER AUTHORIZATION ON database::[PRV_OCR_OpenAssess] TO sa;
ALTER AUTHORIZATION ON database::[PRV_OCR_SecureAssess] TO sa;
ALTER AUTHORIZATION ON database::[PRV_OCR_SurpassDataWarehouse] TO sa;
ALTER AUTHORIZATION ON database::[PRV_SQA_BTLSurpassAudit] TO sa;
ALTER AUTHORIZATION ON database::[PRV_SQA_CPProjectAdmin] TO sa;
ALTER AUTHORIZATION ON database::[PRV_SQA_ItemBank] TO sa;
ALTER AUTHORIZATION ON database::[PRV_SQA_SecureAssess] TO sa;
ALTER AUTHORIZATION ON database::[PRV_SQA_SurpassDataWarehouse] TO sa;
ALTER AUTHORIZATION ON database::[PRV_Standard_CPProjectAdmin] TO sa;
ALTER AUTHORIZATION ON database::[PRV_Standard_Itembank] TO sa;
ALTER AUTHORIZATION ON database::[PRV_Standard_OpenAssessAdmin] TO sa;
ALTER AUTHORIZATION ON database::[PRV_Standard_SecureAssess] TO sa;
ALTER AUTHORIZATION ON database::[PRV_Standard_SecureMarker] TO sa;
ALTER AUTHORIZATION ON database::[PRV_WJEC_ContentProducer] TO sa;
ALTER AUTHORIZATION ON database::[PRV_WJEC_ItemBank] TO sa;
ALTER AUTHORIZATION ON database::[PRV_WJEC_OpenAssess] TO sa;
ALTER AUTHORIZATION ON database::[PRV_WJEC_SecureAssess] TO sa;
ALTER AUTHORIZATION ON database::[SANDBOX_AAT_SecureAssess] TO sa;
ALTER AUTHORIZATION ON database::[SANDBOX_CandG_ContentProducer_DI] TO sa;
ALTER AUTHORIZATION ON database::[SANDBOX_Evolve_ContentProducer] TO sa;
ALTER AUTHORIZATION ON database::[SANDBOX_Evolve_ItemBank] TO sa;
ALTER AUTHORIZATION ON database::[SANDBOX_Evolve_ItemBank_TOM] TO sa;
ALTER AUTHORIZATION ON database::[SANDBOX_Evolve_SecureAssess] TO sa;
ALTER AUTHORIZATION ON database::[SANDBOX_PPD_AAT_SecureMarker] TO sa;
ALTER AUTHORIZATION ON database::[SANDBOX_PRVUAT_AAT_ContentProducer] TO sa;
ALTER AUTHORIZATION ON database::[SANDBOX_PRVUAT_AAT_SecureAssess] TO sa;
ALTER AUTHORIZATION ON database::[SHA_BIS_CPProjectAdmin_11.0] TO sa;
ALTER AUTHORIZATION ON database::[SHA_BIS_ItemBank_11.0] TO sa;
ALTER AUTHORIZATION ON database::[SHA_BIS_SecureAssess_11.0] TO sa;
ALTER AUTHORIZATION ON database::[SHA_BIS_SurpassDataWarehouse] TO sa;
ALTER AUTHORIZATION ON database::[SHA_BTLGCC_CP_BTL_Arabic_Project_Copy] TO sa;
ALTER AUTHORIZATION ON database::[SHA_BTLGCC_CP_BTL_Test_Project] TO sa;
ALTER AUTHORIZATION ON database::[SHA_BTLGCC_CP_TESTcb20062013Onsc] TO sa;
ALTER AUTHORIZATION ON database::[SHA_BTLGCC_CP_TESTCB20062013Pap] TO sa;
ALTER AUTHORIZATION ON database::[SHA_CCEA_ContentProducer] TO sa;
ALTER AUTHORIZATION ON database::[SHA_CCEA_ItemBank] TO sa;
ALTER AUTHORIZATION ON database::[SHA_DEMO_CP_BTL_Arabic_Project] TO sa;
ALTER AUTHORIZATION ON database::[SHA_QEYADAH_CP_abc1234565789] TO sa;
ALTER AUTHORIZATION ON database::[SHA_QEYADAH_CP_BTLTestOnscreen1109] TO sa;
ALTER AUTHORIZATION ON database::[SHA_QEYADAH_CP_BTLTestPaper1109] TO sa;
ALTER AUTHORIZATION ON database::[SHA_QEYADAH_CP_cbonscreen22032013testlivedeploy] TO sa;
ALTER AUTHORIZATION ON database::[SHA_QEYADAH_CP_cbpaperdeploytest22032013] TO sa;
ALTER AUTHORIZATION ON database::[SHA_QEYADAH_CP_christest] TO sa;
ALTER AUTHORIZATION ON database::[SHA_QEYADAH_CP_grfrggfg] TO sa;
ALTER AUTHORIZATION ON database::[SHA_QEYADAH_CP_HB_10Sep2012] TO sa;
ALTER AUTHORIZATION ON database::[SHA_QEYADAH_CP_testdeploy22032013cb] TO sa;
ALTER AUTHORIZATION ON database::[SHA_QEYADAH_CP_Tuesdayaft11092012] TO sa;
ALTER AUTHORIZATION ON database::[SHA_QEYADAH_CPProjectAdmin_10.0] TO sa;
ALTER AUTHORIZATION ON database::[SHA_QEYADAH_ItemBank_10.0] TO sa;
ALTER AUTHORIZATION ON database::[SHA_QEYADAH_SecureAssess_10.0] TO sa;
ALTER AUTHORIZATION ON database::[STG_AQA_ContentProducer] TO sa;
ALTER AUTHORIZATION ON database::[STG_AQA_ItemBank] TO sa;
ALTER AUTHORIZATION ON database::[STG_AQA_SecureAssess] TO sa;
ALTER AUTHORIZATION ON database::[STG_AQA_SurpassDataWarehouse] TO sa;
ALTER AUTHORIZATION ON database::[STG_BC2_ContentProducer] TO sa;
ALTER AUTHORIZATION ON database::[STG_BC2_eFlex] TO sa;
ALTER AUTHORIZATION ON database::[STG_BC2_EPCAdapter] TO sa;
ALTER AUTHORIZATION ON database::[STG_BC2_ItemBank] TO sa;
ALTER AUTHORIZATION ON database::[STG_BC2_LocalScan] TO sa;
ALTER AUTHORIZATION ON database::[STG_BC2_PhoneAssess] TO sa;
ALTER AUTHORIZATION ON database::[STG_BC2_SecureAssess] TO sa;
ALTER AUTHORIZATION ON database::[STG_BC2_SecureAssess] TO sa;
ALTER AUTHORIZATION ON database::[STG_BC2_SecureMarker] TO sa;
ALTER AUTHORIZATION ON database::[STG_BC2_SurpassDataWarehouse] TO sa;
ALTER AUTHORIZATION ON database::[STG_BC2_TestPackageManager] TO sa;
ALTER AUTHORIZATION ON database::[STG_BCGuilds_SecureAssess] TO sa;
ALTER AUTHORIZATION ON database::[STG_BCGuilds_SecureMarker] TO sa;
ALTER AUTHORIZATION ON database::[STG_BCGuilds_TestPackage] TO sa;
ALTER AUTHORIZATION ON database::[STG_EAL_ContentProducer] TO sa;
ALTER AUTHORIZATION ON database::[STG_EAL_ItemBank] TO sa;
ALTER AUTHORIZATION ON database::[STG_EAL_SecureAssess] TO sa;
ALTER AUTHORIZATION ON database::[STG_SANDBOX_SQA_CPProjectAdmin] TO sa;
ALTER AUTHORIZATION ON database::[STG_SQA2_ContentProducer] TO sa;
ALTER AUTHORIZATION ON database::[STG_SQA2_ItemBank] TO sa;
ALTER AUTHORIZATION ON database::[STG_SQA2_OpenAssess] TO sa;
ALTER AUTHORIZATION ON database::[STG_SQA2_SecureAssess] TO sa;
ALTER AUTHORIZATION ON database::[STG_SQA2_SurpassDataWarehouse] TO sa;
ALTER AUTHORIZATION ON database::[STG_WJEC_ContentProducer] TO sa;
ALTER AUTHORIZATION ON database::[STG_WJEC_ItemBank] TO sa;
ALTER AUTHORIZATION ON database::[STG_WJEC_OpenAssess] TO sa;
ALTER AUTHORIZATION ON database::[STG_WJEC_SecureAssess] TO sa;
*/

/*ROLLBACK

ALTER AUTHORIZATION ON database::[E2E_EAL_ItemBank] TO sa;
ALTER AUTHORIZATION ON database::[E2E_EAL_SecureAssess] TO sa;
ALTER AUTHORIZATION ON database::[E2E_EAL_SurpassDataWarehouse] TO NT AUTHORITY\SYSTEM;
ALTER AUTHORIZATION ON database::[PPD_AAT_ContentProducer] TO sa;
ALTER AUTHORIZATION ON database::[PPD_AAT_ItemBank] TO davej;
ALTER AUTHORIZATION ON database::[PPD_AAT_SecureAssess] TO davej;
ALTER AUTHORIZATION ON database::[PPD_AAT_SecureMarker] TO sa;
ALTER AUTHORIZATION ON database::[PPD_AAT_SurpassDataWarehouse] TO LON\977496-abrocklesby;
ALTER AUTHORIZATION ON database::[PPD_BCGuildsIntegration_SecureAssess] TO davej;
ALTER AUTHORIZATION ON database::[PPD_BCGuildsIntegration_SecureMarker] TO davej;
ALTER AUTHORIZATION ON database::[PPD_BCGuildsIntegration_SurpassDataWarehouse] TO davej;
ALTER AUTHORIZATION ON database::[PPD_BCGuildsIntegration_TestPackage] TO davej;
ALTER AUTHORIZATION ON database::[PPD_BCIntegration_CPProjectAdmin] TO LON\977496-DaveJ;
ALTER AUTHORIZATION ON database::[PPD_BCIntegration_eFlex] TO davej;
ALTER AUTHORIZATION ON database::[PPD_BCIntegration_EPCAdaptor] TO davej;
ALTER AUTHORIZATION ON database::[PPD_BCIntegration_ItemBank] TO LON\977496-DaveJ;
ALTER AUTHORIZATION ON database::[PPD_BCIntegration_LocalScan] TO davej;
ALTER AUTHORIZATION ON database::[PPD_BCIntegration_PhoneExamSystemContext] TO davej;
ALTER AUTHORIZATION ON database::[PPD_BCIntegration_SecureAssess] TO LON\977496-DaveJ;
ALTER AUTHORIZATION ON database::[PPD_BCIntegration_SecureAssess] TO LON\977496-DaveJ;
ALTER AUTHORIZATION ON database::[PPD_BCIntegration_SecureMarker] TO sa;
ALTER AUTHORIZATION ON database::[PPD_BCIntegration_SurpassDataWarehouse] TO davej;
ALTER AUTHORIZATION ON database::[PPD_BCIntegration_TestPackage] TO sa;
ALTER AUTHORIZATION ON database::[PPD_NCFE_CPProjectAdmin] TO davej;
ALTER AUTHORIZATION ON database::[PPD_NCFE_Itembank] TO davej;
ALTER AUTHORIZATION ON database::[PPD_NCFE_SecureAssess] TO davej;
ALTER AUTHORIZATION ON database::[PRV_BCGuilds_SecureAssess] TO LON\977496-DaveJ;
ALTER AUTHORIZATION ON database::[PRV_BCGuilds_SecureMarker] TO LON\977496-DaveJ;
ALTER AUTHORIZATION ON database::[PRV_BCGuilds_SurpassDataWarehouse] TO davej;
ALTER AUTHORIZATION ON database::[PRV_BCGuilds_TestPackage] TO LON\977496-DaveJ;
ALTER AUTHORIZATION ON database::[PRV_BritishCouncil_CPProjectAdmin] TO LON\977496-abrocklesby;
ALTER AUTHORIZATION ON database::[PRV_BritishCouncil_eFlex] TO LON\977496-DaveJ;
ALTER AUTHORIZATION ON database::[PRV_BritishCouncil_EPCAdaptor] TO LON\977496-DaveJ;
ALTER AUTHORIZATION ON database::[PRV_BritishCouncil_ItemBank] TO LON\977496-abrocklesby;
ALTER AUTHORIZATION ON database::[PRV_BritishCouncil_LocalScan] TO LON\977496-DaveJ;
ALTER AUTHORIZATION ON database::[PRV_BritishCouncil_PhoneAssess] TO davej;
ALTER AUTHORIZATION ON database::[PRV_BritishCouncil_SecureAssess] TO davej;
ALTER AUTHORIZATION ON database::[PRV_BritishCouncil_SecureMarker] TO LON\977496-DaveJ;
ALTER AUTHORIZATION ON database::[PRV_BritishCouncil_SurpassDataWarehouse_2] TO LON\977496-abrocklesby;
ALTER AUTHORIZATION ON database::[PRV_BritishCouncil_TestPackageManager] TO sa;
ALTER AUTHORIZATION ON database::[PRV_DEMO_CPProjectAdmin] TO LON\977496-abrocklesby;
ALTER AUTHORIZATION ON database::[PRV_DEMO_ItemBank] TO LON\977496-abrocklesby;
ALTER AUTHORIZATION ON database::[PRV_DEMO_OpenAssessAdmin] TO LON\977496-abrocklesby;
ALTER AUTHORIZATION ON database::[PRV_DEMO_SecureAssess] TO LON\977496-abrocklesby;
ALTER AUTHORIZATION ON database::[PRV_DEMO_SecureMarker] TO LON\977496-abrocklesby;
ALTER AUTHORIZATION ON database::[PRV_Demo_SurpassDataWarehouse] TO LON\977496-abrocklesby;
ALTER AUTHORIZATION ON database::[PRV_EAL_CPProjectAdmin] TO LON\977496-abrocklesby;
ALTER AUTHORIZATION ON database::[PRV_EAL_ItemBank] TO LON\977496-abrocklesby;
ALTER AUTHORIZATION ON database::[PRV_EAL_SecureAssess] TO LON\977496-abrocklesby;
ALTER AUTHORIZATION ON database::[PRV_Evolve_CPProjectAdmin] TO LON\977496-abrocklesby;
ALTER AUTHORIZATION ON database::[PRV_Evolve_ItemBank] TO LON\977496-abrocklesby;
ALTER AUTHORIZATION ON database::[PRV_Evolve_OpenAssessAdmin] TO davej;
ALTER AUTHORIZATION ON database::[PRV_Evolve_SecureAssess] TO davej;
ALTER AUTHORIZATION ON database::[PRV_NCFE_CPProjectAdmin] TO LON\977496-abrocklesby;
ALTER AUTHORIZATION ON database::[PRV_NCFE_Itembank] TO LON\977496-abrocklesby;
ALTER AUTHORIZATION ON database::[PRV_NCFE_SecureAssess] TO LON\977496-abrocklesby;
ALTER AUTHORIZATION ON database::[PRV_OCR_ASPState] TO davej;
ALTER AUTHORIZATION ON database::[PRV_OCR_ContentProducer] TO davej;
ALTER AUTHORIZATION ON database::[PRV_OCR_DataManagement] TO davej;
ALTER AUTHORIZATION ON database::[PRV_OCR_ItemBank] TO davej;
ALTER AUTHORIZATION ON database::[PRV_OCR_OpenAssess] TO davej;
ALTER AUTHORIZATION ON database::[PRV_OCR_SecureAssess] TO davej;
ALTER AUTHORIZATION ON database::[PRV_OCR_SurpassDataWarehouse] TO davej;
ALTER AUTHORIZATION ON database::[PRV_SQA_BTLSurpassAudit] TO LON\977496-abrocklesby;
ALTER AUTHORIZATION ON database::[PRV_SQA_CPProjectAdmin] TO LON\977496-abrocklesby;
ALTER AUTHORIZATION ON database::[PRV_SQA_ItemBank] TO LON\977496-abrocklesby;
ALTER AUTHORIZATION ON database::[PRV_SQA_SecureAssess] TO LON\977496-abrocklesby;
ALTER AUTHORIZATION ON database::[PRV_SQA_SurpassDataWarehouse] TO LON\977496-abrocklesby;
ALTER AUTHORIZATION ON database::[PRV_Standard_CPProjectAdmin] TO LON\977496-abrocklesby;
ALTER AUTHORIZATION ON database::[PRV_Standard_Itembank] TO LON\977496-abrocklesby;
ALTER AUTHORIZATION ON database::[PRV_Standard_OpenAssessAdmin] TO sa;
ALTER AUTHORIZATION ON database::[PRV_Standard_SecureAssess] TO LON\977496-abrocklesby;
ALTER AUTHORIZATION ON database::[PRV_Standard_SecureMarker] TO sa;
ALTER AUTHORIZATION ON database::[PRV_WJEC_ContentProducer] TO LON\977496-abrocklesby;
ALTER AUTHORIZATION ON database::[PRV_WJEC_ItemBank] TO LON\977496-abrocklesby;
ALTER AUTHORIZATION ON database::[PRV_WJEC_OpenAssess] TO davej;
ALTER AUTHORIZATION ON database::[PRV_WJEC_SecureAssess] TO LON\977496-abrocklesby;
ALTER AUTHORIZATION ON database::[SANDBOX_AAT_SecureAssess] TO davej;
ALTER AUTHORIZATION ON database::[SANDBOX_CandG_ContentProducer_DI] TO davej;
ALTER AUTHORIZATION ON database::[SANDBOX_Evolve_ContentProducer] TO LON\977496-abrocklesby;
ALTER AUTHORIZATION ON database::[SANDBOX_Evolve_ItemBank] TO LON\977496-abrocklesby;
ALTER AUTHORIZATION ON database::[SANDBOX_Evolve_ItemBank_TOM] TO LON\977496-abrocklesby;
ALTER AUTHORIZATION ON database::[SANDBOX_Evolve_SecureAssess] TO LON\977496-abrocklesby;
ALTER AUTHORIZATION ON database::[SANDBOX_PPD_AAT_SecureMarker] TO davej;
ALTER AUTHORIZATION ON database::[SANDBOX_PRVUAT_AAT_ContentProducer] TO davej;
ALTER AUTHORIZATION ON database::[SANDBOX_PRVUAT_AAT_SecureAssess] TO LON\977496-abrocklesby;
ALTER AUTHORIZATION ON database::[SHA_BIS_CPProjectAdmin_11.0] TO sa;
ALTER AUTHORIZATION ON database::[SHA_BIS_ItemBank_11.0] TO sa;
ALTER AUTHORIZATION ON database::[SHA_BIS_SecureAssess_11.0] TO LON\977496-DaveJ;
ALTER AUTHORIZATION ON database::[SHA_BIS_SurpassDataWarehouse] TO LON\977496-abrocklesby;
ALTER AUTHORIZATION ON database::[SHA_BTLGCC_CP_BTL_Arabic_Project_Copy] TO LON\977496-DaveJ;
ALTER AUTHORIZATION ON database::[SHA_BTLGCC_CP_BTL_Test_Project] TO LON\977496-DaveJ;
ALTER AUTHORIZATION ON database::[SHA_BTLGCC_CP_TESTcb20062013Onsc] TO LON\977496-DaveJ;
ALTER AUTHORIZATION ON database::[SHA_BTLGCC_CP_TESTCB20062013Pap] TO LON\977496-DaveJ;
ALTER AUTHORIZATION ON database::[SHA_CCEA_ContentProducer] TO LON\977496-zohaibg;
ALTER AUTHORIZATION ON database::[SHA_CCEA_ItemBank] TO LON\977496-zohaibg;
ALTER AUTHORIZATION ON database::[SHA_DEMO_CP_BTL_Arabic_Project] TO LON\977496-DaveJ;
ALTER AUTHORIZATION ON database::[SHA_QEYADAH_CP_abc1234565789] TO LON\977496-DaveJ;
ALTER AUTHORIZATION ON database::[SHA_QEYADAH_CP_BTLTestOnscreen1109] TO LON\977496-DaveJ;
ALTER AUTHORIZATION ON database::[SHA_QEYADAH_CP_BTLTestPaper1109] TO LON\977496-DaveJ;
ALTER AUTHORIZATION ON database::[SHA_QEYADAH_CP_cbonscreen22032013testlivedeploy] TO LON\977496-DaveJ;
ALTER AUTHORIZATION ON database::[SHA_QEYADAH_CP_cbpaperdeploytest22032013] TO LON\977496-DaveJ;
ALTER AUTHORIZATION ON database::[SHA_QEYADAH_CP_christest] TO LON\977496-DaveJ;
ALTER AUTHORIZATION ON database::[SHA_QEYADAH_CP_grfrggfg] TO LON\977496-DaveJ;
ALTER AUTHORIZATION ON database::[SHA_QEYADAH_CP_HB_10Sep2012] TO LON\977496-DaveJ;
ALTER AUTHORIZATION ON database::[SHA_QEYADAH_CP_testdeploy22032013cb] TO LON\977496-DaveJ;
ALTER AUTHORIZATION ON database::[SHA_QEYADAH_CP_Tuesdayaft11092012] TO LON\977496-DaveJ;
ALTER AUTHORIZATION ON database::[SHA_QEYADAH_CPProjectAdmin_10.0] TO LON\977496-DaveJ;
ALTER AUTHORIZATION ON database::[SHA_QEYADAH_ItemBank_10.0] TO LON\977496-DaveJ;
ALTER AUTHORIZATION ON database::[SHA_QEYADAH_SecureAssess_10.0] TO LON\977496-DaveJ;
ALTER AUTHORIZATION ON database::[STG_AQA_ContentProducer] TO LON\977496-abrocklesby;
ALTER AUTHORIZATION ON database::[STG_AQA_ItemBank] TO LON\977496-abrocklesby;
ALTER AUTHORIZATION ON database::[STG_AQA_SecureAssess] TO LON\977496-abrocklesby;
ALTER AUTHORIZATION ON database::[STG_AQA_SurpassDataWarehouse] TO davej;
ALTER AUTHORIZATION ON database::[STG_BC2_ContentProducer] TO LON\977496-DaveJ;
ALTER AUTHORIZATION ON database::[STG_BC2_eFlex] TO davej;
ALTER AUTHORIZATION ON database::[STG_BC2_EPCAdapter] TO LON\977496-DaveJ;
ALTER AUTHORIZATION ON database::[STG_BC2_ItemBank] TO LON\977496-DaveJ;
ALTER AUTHORIZATION ON database::[STG_BC2_LocalScan] TO LON\977496-DaveJ;
ALTER AUTHORIZATION ON database::[STG_BC2_PhoneAssess] TO LON\977496-DaveJ;
ALTER AUTHORIZATION ON database::[STG_BC2_SecureAssess] TO LON\977496-DaveJ;
ALTER AUTHORIZATION ON database::[STG_BC2_SecureAssess] TO LON\977496-DaveJ;
ALTER AUTHORIZATION ON database::[STG_BC2_SecureMarker] TO LON\977496-DaveJ;
ALTER AUTHORIZATION ON database::[STG_BC2_SurpassDataWarehouse] TO LON\977496-DaveJ;
ALTER AUTHORIZATION ON database::[STG_BC2_TestPackageManager] TO LON\977496-DaveJ;
ALTER AUTHORIZATION ON database::[STG_BCGuilds_SecureAssess] TO LON\977496-DaveJ;
ALTER AUTHORIZATION ON database::[STG_BCGuilds_SecureMarker] TO LON\977496-DaveJ;
ALTER AUTHORIZATION ON database::[STG_BCGuilds_TestPackage] TO LON\977496-DaveJ;
ALTER AUTHORIZATION ON database::[STG_EAL_ContentProducer] TO LON\977496-DaveJ;
ALTER AUTHORIZATION ON database::[STG_EAL_ItemBank] TO LON\977496-DaveJ;
ALTER AUTHORIZATION ON database::[STG_EAL_SecureAssess] TO LON\977496-DaveJ;
ALTER AUTHORIZATION ON database::[STG_SANDBOX_SQA_CPProjectAdmin] TO LON\977496-DaveJ;
ALTER AUTHORIZATION ON database::[STG_SQA2_ContentProducer] TO LON\977496-DaveJ;
ALTER AUTHORIZATION ON database::[STG_SQA2_ItemBank] TO LON\977496-DaveJ;
ALTER AUTHORIZATION ON database::[STG_SQA2_OpenAssess] TO LON\977496-DaveJ;
ALTER AUTHORIZATION ON database::[STG_SQA2_SecureAssess] TO LON\977496-DaveJ;
ALTER AUTHORIZATION ON database::[STG_SQA2_SurpassDataWarehouse] TO LON\977496-DaveJ;
ALTER AUTHORIZATION ON database::[STG_WJEC_ContentProducer] TO LON\977496-DaveJ;
ALTER AUTHORIZATION ON database::[STG_WJEC_ItemBank] TO LON\977496-DaveJ;
ALTER AUTHORIZATION ON database::[STG_WJEC_OpenAssess] TO davej;
ALTER AUTHORIZATION ON database::[STG_WJEC_SecureAssess] TO LON\977496-DaveJ;

*/
	

















--DECLARE @NewFileSize nvarchar(10) = '25MB'

--SELECT 
--'ALTER DATABASE [' + D.Name + ']
--	MODIFY FILE ( NAME =''' + F.Name + ''', FILEGROWTH =' + @NewFileSize + ' )'
--	,A.growth
--	, d.name
--FROM @AutoGrow  A
--    INNER JOIN sys.master_files as F
--    on F.physical_name COLLATE SQL_Latin1_General_CP1_CI_AS = A.filename
		
--    INNER JOIN sys.databases as D
--	on D.database_id  = F.database_id 