DISABLE TRIGGER [dbo].[WarehouseExamState_Trigger] ON  [dbo].[WAREHOUSE_ExamSessionTable];

/*Create Backup Tables, for rollback option*/

SELECT 
	ID
	, StructureXML
	, resultData
	, resultDataFull
INTO [DE19467_Rollback_Examsessions]
FROM dbo.Warehouse_ExamSessionTable
WHERE ID IN (1530093,1530092,1530088,1530097,1530086,1530083,1530096,1530094,1530089,1530221,1530091,1530081,1530087,1530219,1530080,1530090,1530220,1530095,1530085,1530084,1530082);

SELECT *
INTO [DE19467_Rollback_Exams_WAREHOUSE_ExamSessionTable_ShrededItems]
FROM dbo.WAREHOUSE_ExamSessionTable_ShrededItems
WHERE examSessionId IN (1530093,1530092,1530088,1530097,1530086,1530083,1530096,1530094,1530089,1530221,1530091,1530081,1530087,1530219,1530080,1530090,1530220,1530095,1530085,1530084,1530082);

SELECT *
INTO [DE19467_Rollback_Exams_WAREHOUSE_ExamSessionItemResponseTable]
FROM dbo.WAREHOUSE_ExamSessionItemResponseTable
WHERE WAREHOUSEExamSessionID IN (1530093,1530092,1530088,1530097,1530086,1530083,1530096,1530094,1530089,1530221,1530091,1530081,1530087,1530219,1530080,1530090,1530220,1530095,1530085,1530084,1530082);


/*Incorrect scores*/

select
	examsessionid
	, keycode
	, usermark
	, userpercentage
FROM dbo.WAREHOUSE_ExamSessionTable_Shreded
WHERE examsessionid IN (1530093,1530092,1530088,1530097,1530086,1530083,1530096,1530094,1530089,1530221,1530091,1530081,1530087,1530219,1530080,1530090,1530220,1530095,1530085,1530084,1530082);

if OBJECT_ID('tempdb..#TMP_Marks_Table') is not null drop table #TMP_Marks_Table;

CREATE TABLE [#TMP_Marks_Table](
	[ExamSessionId] [int] NOT NULL,
	[ItemID] [nvarchar](50) NULL,
	[TotalMark] [float] NULL,
	[UserMark] [float] NULL
);

INSERT INTO #TMP_Marks_Table(ExamSessionId, ItemID, totalMark, UserMark )	
SELECT ID, itemID, totalMark, userMark
FROM
(
	SELECT ID, 
				item.node.value('@id[1]','nvarchar(50)') as itemID,
				item.node.value('@totalMark[1]','float') as totalMark,
				item.node.value('@userMark[1]','float') as userMark
	FROM dbo.WAREHOUSE_ExamSessionTable
	CROSS APPLY StructureXML.nodes('//item') item(node)
	WHERE ID IN (1530093,1530092,1530088,1530097,1530086,1530083,1530096,1530094,1530089,1530221,1530091,1530081,1530087,1530219,1530080,1530090,1530220,1530095,1530085,1530084,1530082)
) X
WHERE userMark > totalMark;	

DECLARE @ExamsessionId INT;
DECLARE @SectionId INT;
DECLARE @StructureXML XML;
DECLARE @ResultData XML;
DECLARE @ResultDataFULL XML;
DECLARE @ItemID VARCHAR(50);
DECLARE @ItemTotalMark float = 0;
DECLARE @SectionTotalMark float = 0;
DECLARE @SectionUserMarks float = 0;
DECLARE @SectionUserPercentage float;
DECLARE @ExamTotalMark float = 0;
DECLARE @ExamUserMarks float = 0;
DECLARE @ExamUserPercentage decimal(8,3) = 0;

DECLARE Exams_cursor CURSOR FAST_FORWARD FOR 
	SELECT DISTINCT ExamsessionId FROM #TMP_Marks_Table;

OPEN Exams_cursor;

FETCH NEXT FROM Exams_cursor INTO @ExamsessionId;

WHILE @@FETCH_STATUS = 0
BEGIN
	SELECT @StructureXML = StructurexML
		,@ResultData = resultData
		,@ResultDataFull = resultDataFull
	FROM dbo.WAREHOUSE_ExamSessionTable
	WHERE ID = @ExamsessionId;
	
	DECLARE Items_cursor CURSOR FAST_FORWARD FOR
		SELECT ItemID, totalMark FROM #TMP_Marks_Table
		WHERE ExamSessionId = @ExamsessionId;	

	OPEN Items_cursor;

	FETCH NEXT FROM Items_cursor INTO @ItemID, @ItemTotalMark;

	WHILE @@FETCH_STATUS = 0
	BEGIN
	
		--PRINT '@ItemID'  + @ItemID +  ' ,@ItemTotalMark: ' + Convert(Varchar, @ItemTotalMark)

		SET @StructureXML.modify('replace value of (//item[@id=sql:variable("@ItemID")]/@userMark)[1] with ("1")')
        SET @ResultData.modify('replace value of (//item[@id=sql:variable("@ItemID")]/@userMark)[1] with ("1")')         
        SET @ResultDataFull.modify('replace value of (//item[@id=sql:variable("@ItemID")]/@userMark)[1] with ("1")')

		SET @ResultData.modify('replace value of (//item[@id=sql:variable("@ItemID")]/@actualUserMark )[1] with sql:variable("@ItemTotalMark") ')		
				
		FETCH NEXT FROM Items_cursor INTO @ItemID, @ItemTotalMark;
	END

	CLOSE Items_cursor;
	DEALLOCATE Items_cursor;

	DECLARE Sections_cursor CURSOR FAST_FORWARD FOR
		SELECT section.node.value('@id[1]','int') as sectionID
		FROM @StructureXML.nodes('//section') section(node);
	OPEN Sections_cursor;	
	FETCH NEXT FROM Sections_cursor INTO @sectionId;	
	
	WHILE @@FETCH_STATUS = 0
	BEGIN
		
		SELECT @SectionUserMarks = @ResultData.value('sum(/exam/section[@id=sql:variable("@sectionId")]/item/@userMark)', 'float');
		SELECT @SectionTotalMark = @ResultData.value('(/exam/section[@id=sql:variable("@sectionId")]/@totalMark)[1]', 'float');		
		
		SET @SectionUserPercentage = (@SectionUserMarks / @SectionTotalMark) * 100;
		
		--------------- ResultData		
		SET @ResultData.modify('replace value of (/exam/section[@id=sql:variable("@sectionId")]/@userMark )[1] with  sql:variable("@SectionUserMarks")');
		SET @ResultData.modify('replace value of (/exam/section[@id=sql:variable("@sectionId")]/@userPercentage )[1] with  sql:variable("@SectionUserPercentage")');
				
		--------------- ResultData Full
		SET @ResultDataFull.modify('replace value of (/assessmentDetails/assessment/section[@id=sql:variable("@sectionId")]/@userMark )[1] with  sql:variable("@SectionUserMarks")');
		SET @ResultDataFull.modify('replace value of (/assessmentDetails/assessment/section[@id=sql:variable("@sectionId")]/@userPercentage )[1] with  sql:variable("@SectionUserPercentage")');		
		FETCH NEXT FROM Sections_cursor INTO @sectionId;	
	END

	CLOSE Sections_cursor;
	DEALLOCATE Sections_cursor;

	SELECT @ExamTotalMark = @ResultData.value('(/exam[1]/@totalMark)[1]', 'float');
	SELECT @ExamUserMarks = @ResultData.value('sum(/exam/section/@userMark)', 'float');		
	SET @ExamUserPercentage = (@ExamUserMarks / @ExamTotalMark) * 100;

	--------------- ResultData	
	SET @ResultData.modify('replace value of (/exam/@userMark)[1] with  sql:variable("@ExamUserMarks")');
	SET @ResultData.modify('replace value of (/exam/@userPercentage )[1] with  sql:variable("@ExamUserPercentage")');
	
	--------------- ResultData Full
	SET @ResultDataFull.modify('replace value of (assessmentDetails/assessment/@userMark )[1] with  sql:variable("@ExamUserMarks")');
	SET @ResultDataFull.modify('replace value of (assessmentDetails/assessment/@userPercentage )[1] with  sql:variable("@ExamUserPercentage")');
	
	UPDATE dbo.WAREHOUSE_ExamSessionTable
	SET StructureXML = @StructureXML
		,resultData = @ResultData
		,resultDataFull = @ResultDataFULL
	WHERE ID = @ExamsessionId;

	UPDATE dbo.WAREHOUSE_ExamSessionTable_Shreded
	SET structureXml = @StructureXml
		,resultData = @ResultData
	WHERE examSessionId = @ExamsessionId;

	UPDATE SHRD
	SET userMark = 1
	FROM WAREHOUSE_ExamSessionTable_ShrededItems SHRD
	JOIN #TMP_Marks_Table TMP ON SHRD.examSessionId = TMP.ExamSessionId AND SHRD.ItemRef = TMP.ItemID;

	UPDATE SHRD
	SET examPercentage = @ExamUserPercentage
	FROM WAREHOUSE_ExamSessionTable_ShrededItems SHRD
	JOIN #TMP_Marks_Table TMP ON SHRD.examSessionId = TMP.ExamSessionId;
	
	FETCH NEXT FROM Exams_cursor INTO @ExamsessionId;
END

CLOSE Exams_cursor;
DEALLOCATE Exams_cursor;

UPDATE ItemResponses
SET MarkerResponseData.modify('replace value of (/entries/entry[last()]/assignedMark/text())[1] with (1)')
FROM dbo.WAREHOUSE_ExamSessionItemResponseTable ItemResponses
JOIN #TMP_Marks_Table TMP ON ItemResponses.WAREHOUSEExamSessionID = TMP.ExamSessionId
	AND ItemResponses.ItemID = TMP.ItemID;		

DROP TABLE #TMP_Marks_Table;

ENABLE TRIGGER [dbo].[WarehouseExamState_Trigger] ON  [dbo].[WAREHOUSE_ExamSessionTable];

PRINT 'COMPLETED!!'

/*Review scores and compare to the originals which you ran earlier*/

select
	examsessionid
	, keycode
	, usermark
	, userpercentage
	, resultData
FROM dbo.WAREHOUSE_ExamSessionTable_Shreded
WHERE examsessionid IN (1530093,1530092,1530088,1530097,1530086,1530083,1530096,1530094,1530089,1530221,1530091,1530081,1530087,1530219,1530080,1530090,1530220,1530095,1530085,1530084,1530082);