USE [master]
RESTORE DATABASE [F3845_Data_DryRunFix]
	 FROM  DISK = N'D:\Data.bak' WITH  FILE = 1,  MOVE N'F3845_Data' TO N'D:\Data\F3845_Data_DryRunFix.mdf'
		,  MOVE N'F3845_Data_log' TO N'D:\Log\F3845_Data_DryRunFix.ldf',  NOUNLOAD,  REPLACE,  STATS = 5

GO


