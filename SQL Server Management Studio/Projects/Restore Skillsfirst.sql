/*
T:\FTP\Skillsfirst\Skillsfirst.CPIBSDW.2016.12.13.bak
T:\FTP\Skillsfirst\SecureAssess.bak
*/

--RESTORE DATABASE [PRV_SkillsFirst_SecureAssess] FROM DISK = N'T:\FTP\Skillsfirst\SecureAssess.bak' WITH FILE = 1, NOUNLOAD, NOREWIND, REPLACE, STATS = 10, MOVE 'SkillsFirst_SecureAssess_9.0' TO N'S:\DATA\PRV_SkillsFirst_SecureAssess.mdf', MOVE 'SkillsFirst_SecureAssess_9.0_log' TO N'L:\LOGS\PRV_SkillsFirst_SecureAssess.ldf'; 
--RESTORE DATABASE [PRV_SkillsFirst_ContentProducer] FROM DISK = N'T:\FTP\Skillsfirst\Skillsfirst.CPIBSDW.2016.12.13.bak' WITH FILE = 1, NOUNLOAD, NOREWIND, REPLACE, MEDIAPASSWORD = 's_!85WrenukEtrut+4f5a#-a&ac*aKUs', STATS = 10, MOVE 'Data' TO N'S:\DATA\PRV_SkillsFirst_ContentProducer.mdf', MOVE 'Log' TO N'L:\LOGS\PRV_SkillsFirst_ContentProducer.ldf'; 
--RESTORE DATABASE [PRV_SkillsFirst_ItemBank] FROM DISK = N'T:\FTP\Skillsfirst\Skillsfirst.CPIBSDW.2016.12.13.bak' WITH FILE = 2, NOUNLOAD, NOREWIND, REPLACE, MEDIAPASSWORD = 's_!85WrenukEtrut+4f5a#-a&ac*aKUs', STATS = 10, MOVE 'SkillsFirst_ItemBank_9.0' TO N'S:\DATA\PRV_SkillsFirst_ItemBank.mdf', MOVE 'SkillsFirst_ItemBank_9.0_log' TO N'L:\LOGS\PRV_SkillsFirst_ItemBank.ldf'; 
--RESTORE DATABASE [PRV_SkillsFirst_SurpassDataWarehouse] FROM DISK = N'T:\FTP\Skillsfirst\Skillsfirst.CPIBSDW.2016.12.13.bak' WITH FILE = 3, NOUNLOAD, NOREWIND, REPLACE, MEDIAPASSWORD = 's_!85WrenukEtrut+4f5a#-a&ac*aKUs', STATS = 10, MOVE 'Data' TO N'S:\DATA\PRV_SkillsFirst_SurpassDataWarehouse.mdf', MOVE 'Log' TO N'L:\LOGS\PRV_SkillsFirst_SurpassDataWarehouse.ldf'; 


IF NOT EXISTS (SELECT 1 FROM Sys.sql_logins where name ='PRV_SkillsFirst_ItemBank_Login') 
	CREATE LOGIN [PRV_SkillsFirst_ItemBank_Login] WITH PASSWORD = '4qC7S1f6u0DsToeufuGPpjvQ6bhx65S4';  
	USE [PRV_SkillsFirst_ItemBank];  	
	IF EXISTS (
		SELECT 1
		FROM sys.database_principals
		WHERE NAME ='PRV_SkillsFirst_ItemBank_User'
		)
	DROP USER [PRV_SkillsFirst_ItemBank_User];
	
	CREATE USER [PRV_SkillsFirst_ItemBank_User] FOR LOGIN [PRV_SkillsFirst_ItemBank_Login]; EXEC sp_addrolemember 'db_owner','PRV_SkillsFirst_ItemBank_User'
	
	

--IF NOT EXISTS (SELECT 1 FROM Sys.sql_logins where name ='PRV_SkillsFirst_SurpassDataWarehouse_Login') 
--	CREATE LOGIN [PRV_SkillsFirst_SurpassDataWarehouse_Login] WITH PASSWORD = 'Password';  
--	USE [PRV_SkillsFirst_SurpassDataWarehouse];  	
--	IF EXISTS (
--		SELECT 1
--		FROM sys.database_principals
--		WHERE NAME ='PRV_SkillsFirst_SurpassDataWarehouse_User'
--		)
--	DROP USER [PRV_SkillsFirst_SurpassDataWarehouse_User];
	
--	CREATE USER [PRV_SkillsFirst_SurpassDataWarehouse_User] FOR LOGIN [PRV_SkillsFirst_SurpassDataWarehouse_Login]; EXEC sp_addrolemember 'db_owner','PRV_SkillsFirst_SurpassDataWarehouse_User'
	

IF NOT EXISTS (SELECT 1 FROM Sys.sql_logins where name ='PRV_SkillsFirst_ContentProducer_Login') 
	CREATE LOGIN [PRV_SkillsFirst_ContentProducer_Login] WITH PASSWORD = 'zpGjMOMPnbb0bYdCdoYaOfqRbqbarWYI';  
	USE [PRV_SkillsFirst_ContentProducer];  	
	IF EXISTS (
		SELECT 1
		FROM sys.database_principals
		WHERE NAME ='PRV_SkillsFirst_ContentProducer_User'
		)
	DROP USER [PRV_SkillsFirst_ContentProducer_User];
	
	CREATE USER [PRV_SkillsFirst_ContentProducer_User] FOR LOGIN [PRV_SkillsFirst_ContentProducer_Login]; EXEC sp_addrolemember 'db_owner','PRV_SkillsFirst_ContentProducer_User'
	

IF NOT EXISTS (SELECT 1 FROM Sys.sql_logins where name ='PRV_SkillsFirst_SecureAssess_Login') 
	CREATE LOGIN [PRV_SkillsFirst_SecureAssess_Login] WITH PASSWORD = 'fVTbpXFvaVjnKKJokDbxmwCVVlMm5zUQ';  
	USE [PRV_SkillsFirst_SecureAssess];  	
	IF EXISTS (
		SELECT 1
		FROM sys.database_principals
		WHERE NAME ='PRV_SkillsFirst_SecureAssess_User'
		)
	DROP USER [PRV_SkillsFirst_SecureAssess_User];
	
	CREATE USER [PRV_SkillsFirst_SecureAssess_User] FOR LOGIN [PRV_SkillsFirst_SecureAssess_Login]; EXEC sp_addrolemember 'db_owner','PRV_SkillsFirst_SecureAssess_User'
	
	