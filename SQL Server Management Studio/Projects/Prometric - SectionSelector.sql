/*
SELECT agt.ID
	,agt.Name
	, at.AssessmentName
	, at.DurationMode
	, at.AssessmentRules
	, AssessmentDuration
	, at.ID
FROM AssessmentGroupTable AGT (READUNCOMMITTED)
INNER JOIN AssessmentTable AT (READUNCOMMITTED)
ON AT.AssessmentGroupID = AGT.ID
WHERE AGT.NAme = 'Mark Testing'
*/

--Get broken exams

SELECT G.[ID]
	, AGT.[Name]
	, AT.[AssessmentName]
	, AT.[AssessmentDuration]
	, G.[AssessmentDuration Should Be]
INTO #Exams
FROM (
SELECT ID
	,  a.b.value('data(@RequiredSections)[1]','int') *  a.b.value('data(@Duration)[1]','int') + c.d.value('data(@Duration)[1]','int') * a.b.value('data(@RequiredSections)[1]','int') [AssessmentDuration Should Be]
from AssessmentTable 
CROSS APPLY AssessmentRules.nodes('PaperRules/Section[@Name="Section Selector"]') a(b)
CROSS APPLY a.b.nodes('Section') c(d)
) G
INNER JOIN  AssessmentTable AT
on AT.ID = G.ID

INNER JOIN AssessmentGroupTable AGT
ON AT.AssessmentGroupID = AGT.ID

WHERE AT.AssessmentDuration != G.[AssessmentDuration Should Be]

GROUP BY G.[ID]
	, AGT.[Name]
	, AT.[AssessmentName]
	, AT.[AssessmentDuration]
	, G.[AssessmentDuration Should Be];


--Check broken exams

SELECT *
FROM #Exams;

--Fix broken exams
--**Note - this will cause the Assessment Trigger to be fired **

UPDATE AssessmentTable
set AssessmentDuration = b.[AssessmentDuration Should Be]
FROM AssessmentTable A
INNER JOIN #Exams B
on B.ID = A.ID;


DROP TABLE #Exams;

