use master

declare @DriveSpace table (
	Drive Char(1)
	, [MB Free] real	
);
INSERT @DriveSpace
exec ('xp_fixeddrives');

select  distinct 
	@@SERVERNAME [Instance]
	 , substring(physical_name, 0, charindex(':',physical_name)) [Drive]
	 , case type_desc
		when 'LOG' then 'Log'
		when 'ROWS' then 'Data'
		end as [Type]
	, B.[MB Free]
from sys.master_files A
INNER JOIN @DriveSpace B
ON substring(physical_name, 0, charindex(':',physical_name)) = b.Drive
where database_id > 4;



DECLARE @ErrorLogHolder TABLE (
	LogDate datetime
	,ProcessInfo varchar(100)
	, [Text] nvarchar(1000)
);

INSERT @ErrorLogHolder
exec ('xp_readerrorlog 0,1,''Logon''');

SELECT *
FROM @ErrorLogHolder
where LogDate > DATEADD(DAY, -7, getDATE());