select 
	 IB3QualificationLookup.QualificationName [Qualification]
	 ,examName [Assessment Name]
	 ,Forename [Forename]
	 ,Surname [Surname]
	 ,CandidateRef [Membership no.]
	 ,ScheduledDuration [Duration]
	 ,case originalGrade when 'Fail' then 'Not Yet Competent' when 'Pass' then 'Competent' else originalGrade end as [Original Result]
	 ,case adjustedGrade when 'Fail' then 'Not Yet Competent' when 'Pass' then 'Competent' else originalGrade end as [Adjusted Result]
	 ,(resultData.value('data(exam/@userMark)[1]','float') / resultData.value('data(exam/@totalMark)[1]','float')) * 100 [Percentage]
	 ,resultData.value('data(exam/@userMark)[1]','float') [Mark]
	 ,CentreName [Centre Name]
	 ,CentreCode [Centre Code]
	 ,ExamStateChangeAuditTable.StateChangeDate
	 ,(dbo.fn_GetUserRelationForExamSession(dbo.ExamSessionTable.ID, 1)) [Assessor]
	 ,(dbo.fn_GetUserRelationForExamSession(dbo.ExamSessionTable.ID, 2)) [Internal Verifier]
from ScheduledExamsTable 
inner join ExamSessionTable on ExamSessionTable.scheduledExamID = ScheduledExamsTable.id
inner join UserTable on UserTable.id = ExamSessionTable.UserId
inner join CentreTable on CentreTable.id = ScheduledExamsTable.CentreID
inner join IB3QualificationLookup on IB3QualificationLookup.id = ScheduledExamsTable.qualificationId
inner join (
		select examsessionID
			, StateChangeDate
		from ExamStateChangeAuditTable
		where NewState = 9
	) ExamStateChangeAuditTable
	on ExamStateChangeAuditTable.ExamSessionID = ExamSessionTable.ID
where examState = 16
