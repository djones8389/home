USE STG_BCGuilds_SecureAssess

select resultdata
from warehouse_examsessiontable
where keycode = '9PMXGL99'

--<item id="3598P3679" name="AGM_V8_Speaking Task 1" totalMark="5" userMark="0" actualUserMark="0" markingType="1" markerUserMark="" viewingTime="69780" userAttempted="1" version="9" 

USE STG_BCGuilds_SecureMarker;

--select *
--from CandidateExamVersions
--where keycode like '9pm%'



SELECT  CEV.ID	
	, I.ExternalItemID
	, i.Version
	, i.TotalMark
	, ExternalItemName
	, agm.MarkingDeviation
	, UGRL.*
	, ugr.*
FROM CandidateExamVersions as CEV 

INNER JOIN CandidateResponses as CR
ON CR.CandidateExamVersionID = CEV.ID

INNER JOIN UniqueResponses as UR
ON UR.ID = CR.UniqueResponseID 

Inner Join Items as I
on I.ID = ur.itemId

INNER JOIN [dbo].[ExamVersionItems] EVI
on EVI.CandidateExamVersionID = CEV.ID
	AND EVI.ItemID = I.ID

left join UniqueGroupResponseLinks as UGRL
on UGRL.UniqueGroupResponseID = UR.id

left join UniqueGroupResponses as UGR
on UGR.ID = UGRL.UniqueGroupResponseID

left join AssignedGroupMarks as AGM
on AGM.UniqueGroupResponseId = UGR.ID

left join UniqueResponseDocuments as URD
ON URD.UniqueResponseID = UR.id 
	

where keycode = '9PMXGL99'





SELECT Keycode
, ExamSessionState
, ExternalItemID
, ExternalItemName
, ugr.CheckSum
, ur.checksum
, agm.*
FROM CandidateExamVersions as CEV 

INNER JOIN CandidateResponses as CR
ON CR.CandidateExamVersionID = CEV.ID

INNER JOIN UniqueResponses as UR
ON UR.ID = CR.UniqueResponseID 

Inner Join Items as I
on I.ID = ur.itemId

INNER JOIN [dbo].[ExamVersionItems] EVI
on EVI.CandidateExamVersionID = CEV.ID
	AND EVI.ItemID = I.ID

left join UniqueGroupResponseLinks as UGRL
on UGRL.UniqueGroupResponseID = UR.id

left join UniqueGroupResponses as UGR
on UGR.ID = UGRL.UniqueGroupResponseID

left join AssignedGroupMarks as AGM
on AGM.UniqueGroupResponseId = UGR.ID

where UGRL.UniqueResponseId IS NULL
	and keycode = '9PMXGL99'



/*

SELECT i.*
FROM [ExamVersionItems]  EVI

inner join items I
on I.ID = evi.ItemID

inner join CandidateExamVersions CEV
on CEV.ID = evi.CandidateExamVersionID
	
INNER JOIN CandidateResponses as CR
ON CR.CandidateExamVersionID = CEV.ID

INNER JOIN UniqueResponses as UR
ON UR.ID = CR.UniqueResponseID 
	
LEFT join UniqueGroupResponseLinks as UGRL
on UGRL.UniqueGroupResponseID = UR.id

where evi.CandidateExamVersionID  = 120
	order by ExternalItemID

*/