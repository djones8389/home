      SELECT ID AS ProjectID
            ,NAME AS ProjectName
            ,page.value('@ID', 'int') AS PageID
            ,page.value('@sta', 'int') AS PageStatus
            ,page.value('@TotalMark', 'nvarchar(1000)') AS TotalMark -- This sometime contains non numeric values        
            ,page.value('@ItemSpec', 'int') AS ItemSpecificationNo
            ,page.value('@Topic', 'NVARCHAR(1000)') AS Topic
            ,page.value('@YearItemUsedIn', 'nvarchar(1000)') AS YearItemUsedIn           
			,page.query('.')
      --page.query('.'),     
      FROM dbo.ProjectListTable (READUNCOMMITTED)
      CROSS APPLY ProjectStructureXml.nodes('//Pag') pages(page)
      WHERE ID IN (1097)
            AND page.value('local-name((..)[1])', 'nvarchar(1000)') <> 'Rec'
            AND ISNULL(page.value('@markingSchemeFor', 'int'),-1) = -1      
            --page.value('@markingSchemeFor', 'int') = -1
			--and ISNUMERIC(page.value('@ItemSpec', 'varchar(1000)')) = 0

	  SELECT name
		  ,page.value('@ID', 'int')
		  ,page.value('@ItemSpec', 'varchar(1000)') AS ItemSpecificationNo
	  FROM dbo.ProjectListTable (READUNCOMMITTED)
	  CROSS APPLY ProjectStructureXml.nodes('//Pag') pages(page)
      WHERE ID IN (1097)
		and page.value('@ID', 'int') = '2700'