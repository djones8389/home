--2013	4

select count(ID) [CountOfDocs]
	,  count(distinct warehouseExamSessionID) [CountOfExams]
	,  count(ID) / count(distinct warehouseExamSessionID) 
from WAREHOUSE_ExamSessionDocumentTable
where YEAR(UPLOADDATE) = 2013
	and MONTH(UPLOADDATE) = 4


select 
	SUM(DATALENGTH(Document))
	, warehouseExamSessionID
from WAREHOUSE_ExamSessionDocumentTable
where YEAR(UPLOADDATE) = 2013
	and MONTH(UPLOADDATE) = 4

	group by warehouseExamSessionID
	order by 1 asc;