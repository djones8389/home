--SELECT C.YearOfUpload
--	, C.MonthOfUpload
--	, C.CountOfDocs
--	, CONVERT(BIGINT,SUM([Min. Doc Size Per Exam])/CountOfDocs) [Min. Doc Size Per Exam]
--	, CONVERT(BIGINT,SUM([Max. Doc Size Per Exam])/CountOfDocs) [Max. Doc Size Per Exam]
--	, CONVERT(BIGINT,SUM([Avg. Doc Size Per Exam])/CountOfDocs) [Avg. Doc Size Per Exam]
--FROM (

SELECT 
	A.YearOfUpload
	, A.MonthOfUpload
	, CountOfDocs
	, CONVERT(BIGINT,B.[Min. Doc Size Per Exam]) [Min. Doc Size Per Exam]
	, CONVERT(BIGINT,b.[Max. Doc Size Per Exam]) [Max. Doc Size Per Exam]
	, CONVERT(BIGINT,b.[Avg. Doc Size Per Exam]) [Avg. Doc Size Per Exam]
	--, CONVERT(BIGINT,SUM([DATALENGTH])) [SizeOfData]
	--, CONVERT(BIGINT,SUM([DATALENGTH]))
FROM (
select
	COUNT(ID) CountOfDocs
	,MONTH(uploadDate) MonthOfUpload
	,YEAR(uploadDate) YearOfUpload
from WAREHOUSE_ExamSessionDocumentTable
group by MONTH(uploadDate),YEAR(uploadDate)
)  A
	
INNER JOIN (
	select 
		 CONVERT(BIGINT,SUM(DATALENGTH(Document))) [DataLength]
		, MIN(DATALENGTH(Document)) [Min. Doc Size Per Exam]
		, MAX(DATALENGTH(Document)) [Max. Doc Size Per Exam]
		, AVG(DATALENGTH(Document)) [Avg. Doc Size Per Exam]
		, MONTH(uploadDate) MonthOfUpload
		, YEAR(uploadDate) YearOfUpload
		, warehouseExamSessionID
	FROM WAREHOUSE_ExamSessionDocumentTable
	group by warehouseExamSessionID
	, MONTH(uploadDate)
		, YEAR(uploadDate)
) B

ON A.MonthOfUpload = B.MonthOfUpload
	and A.YearOfUpload = B.YearOfUpload

GROUP BY 	A.YearOfUpload
			, A.MonthOfUpload
			, CountOfDocs
			, B.[Min. Doc Size Per Exam]
			, b.[Max. Doc Size Per Exam]
			, b.[Avg. Doc Size Per Exam]
--) C
--	GROUP BY YearOfUpload
--		, MonthOfUpload
--		, CountOfDocs

ORDER BY 1,2