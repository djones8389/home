select 'drop table ' + QUOTENAME(name)
 , 'select * from ' + QUOTENAME(name) + ' WITH (NOLOCK) order by 1'
from sys.tables

/*
drop table [F11082_1_ExamCount]
drop table [F11082_2_CountryCount]
drop table [F11082_3_CentreCount]
drop table [F11082_4_CompMarkedItems]
drop table [F11082_5_HumanMarkedItems]
drop table [F11082_6_LiveCandidates]


*/

select * from [F11082_1_ExamCount] WITH (NOLOCK) order by 1
select * from [F11082_2_CountryCount] WITH (NOLOCK) order by 1
select * from [F11082_3_CentreCount] WITH (NOLOCK) order by 1
select * from [F11082_4_CompMarkedItems] WITH (NOLOCK) order by 1
select * from [F11082_5_HumanMarkedItems] WITH (NOLOCK) order by 1
select * from [F11082_6_LiveCandidates] WITH (NOLOCK) order by 1
