SELECT a.Instance
, a.database_name
, SUM(rows) [NoOfRows]
, SUM([TotalKB])/1024 [Total_MB]
, SUM([TotalKB])/1024/1024  [Total_GB]
,[StartDate]
FROM (
SELECT 
	[Instance]
      ,[database_name]
	  --,table_name
	  , rows
      ,cast(replace([data_kb], 'KB','') as float) + cast(replace([index_size], 'KB','') as float) [TotalKB]
      ,cast([StartDate] as date) [StartDate]
  FROM [PSCollector].[dbo].[DBGrowthMetrics]
  where database_name = 'aat_SecureAssess'
	AND cast([StartDate] as date) > '2017-09-01'
	AND table_name IN ('ExamSessionDocumentTable','WAREHOUSE_ExamSessionDocumentTable')
) a
GROUP BY a.Instance, a.database_name,  [StartDate]
ORDER BY 1,2
--ORDER BY cast([StartDate] as date), table_name


SELECT a.Instance
, a.database_name
, SUM(rows) [NoOfRows]
, SUM([TotalKB])/1024 [Total_MB]
, SUM([TotalKB])/1024/1024  [Total_GB]
,[StartDate]
FROM (
SELECT 
	[Instance]
      ,[database_name]
	  --,table_name
	  , rows
      ,cast(replace([data_kb], 'KB','') as float) + cast(replace([index_size], 'KB','') as float) [TotalKB]
      ,cast([StartDate] as date) [StartDate]
  FROM [PSCollector].[dbo].[DBGrowthMetrics]
  where database_name = 'aat_SecureAssess'
	AND cast([StartDate] as date) > '2017-09-01'
	AND table_name IN ('ExamSessionTable','WAREHOUSE_ExamSessionTable')
) a
GROUP BY a.Instance, a.database_name,  [StartDate]
ORDER BY 1,2
--ORDER BY cast([StartDate] as date), table_name


SELECT b.*
 , b.total/1024 MB
 , b.total/1024/1024 GB
FROM (
	SELECT 
		A.Instance
		, A.[database_name]
		,[StartDate]
		, SUM(a.data_kb) + SUM(a.index_size) [total]
		--, SUM(a.data_kb) data_kb
		--, SUM(a.index_size) index_size
	FROM (
	SELECT 
		[Instance]
		  ,[database_name]
		  ,cast(replace([data_kb], 'KB','') as float) [data_kb]
		  ,cast(replace([index_size], 'KB','') as float) [index_size]
		  ,cast([StartDate] as nvarchar(16)) [StartDate]
	  FROM [PSCollector].[dbo].[DBGrowthMetrics]
	  WHERE database_name = 'AAT_ItemBank'
	) A

	where  [StartDate] IN ('2018-01-02 07:57','2017-04-11 09:56')
	GROUP BY A.Instance
			, A.[database_name]
		,[StartDate]

) b