USE [PRV_Evolve_ItemBank] 

SELECT TOP 1000 AGT.[ID]
      ,[QualificationID]
      ,[Name]
	  , at.ID
	  , at.AssessmentRules
  FROM [dbo].[AssessmentGroupTable] AGT
  INNER JOIN AssessmentTable AT
  ON AGT.ID = AT.AssessmentGroupID
  where [Name] like '%6008%'

  declare @ExamIDs table (
	ID INT
  )
  insert @ExamIDs
  select 601,602,603,604,605,606,608,609,611,614,615,616,617,690,701,702,703,705,706,709,710,711,790