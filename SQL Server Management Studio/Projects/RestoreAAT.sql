RESTORE FILELISTONLY FROM DISK = 'T:\Backup\PPD.AAT.SecureAssess.2016.02.11.bak'

kill 55

USE [master]

RESTORE DATABASE [SANDBOX_AAT_SecureAssess_R11.6] 
FROM  DISK = 'T:\Backup\PPD.AAT.SecureAssess.2016.02.11.bak'
WITH  FILE = 1,  MOVE N'Data' TO N'E:\Data\SANDBOX_AAT_SecureAssess_R11.6.mdf',  
			MOVE N'Log' TO N'L:\Logs\SANDBOX_AAT_SecureAssess_R11.6.ldf'
, RECOVERY, NOUNLOAD, NOREWIND,  STATS = 5, REPLACE

GO

