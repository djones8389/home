/****** Script for SelectTopNRows command from SSMS  ******/
SELECT TOP (1000) [ID]
      ,[Instance]
      ,[database_name]
      ,[table_name]
      ,[rows]
      ,(cast(replace([data_kb],'kb','') as float) + cast(replace([index_size],'kb','') as float) )  KB
	  ,(cast(replace([data_kb],'kb','') as float) + cast(replace([index_size],'kb','') as float) )/1024  MB
      ,cast(replace([data_kb],'kb','') as float) [data_kb]
      ,cast(replace([index_size],'kb','') as float)  [index_size]
      ,[unused_kb]
      ,[StartDate]
      ,[EndDate]
  FROM [PSCollector].[dbo].[DBGrowthMetrics]
  where database_name = 'BRITISHCOUNCIL_SecureAssess'
	and StartDate > '2017-07-04'
	order by  (cast(replace([data_kb],'kb','') as float) + cast(replace([index_size],'kb','') as float) )  desc