select west.id
	, wests.examsessionid
from warehouse_examsessiontable west
left join warehouse_examsessiontable_shreded wests
on west.id = wests.examsessionid 
where keycode = '4PFNP84J'

declare @examSessionId int = 234163

    select
		Shreded.examName as 'ExamName',
		Shreded.examRef as 'ExamReference',
		WEST.ExternalReference as 'ExternalReference',
		Shreded.examId as 'ExamId',
		Shreded.examVersionName as 'ExamVersionName',
		Shreded.examVersionRef as 'ExamVersionReference',
		Shreded.examVersionId as 'ExamVersionId',
		Centres.centreCode as 'CentreReference',
		Shreded.centreId as 'CentreId',
		Shreded.qualificationRef as 'SubjectReference',
		WEST.KeyCode as 'Keycode',
		Shreded.grade as 'Grade',
		Shreded.started as 'StartedDate',
		Shreded.submittedDate as 'SubmittedDate',
		Shreded.foreName as 'CandidateForename',
		Users.Middlename as 'CandidateMiddleName',
		Shreded.surName as 'CandidateSurname',
		Users.gender as 'CandidateGender',
		Users.candidateRef as 'CandidateReference',
		Users.UserId as 'CandidateId',
		CASE WHEN ISNULL(Shreded.passType,0) = 0 THEN Shreded.passMark 
			 ELSE (Shreded.passMark * Shreded.totalMark)/100 
		END 'PassMark',
		Shreded.totalMark as 'AvailableMark',
		Shreded.userMark as 'UserMark',
		Shreded.userPercentage as 'UserPercentage',	
		Shreded.passType as 'PassMarkType',
		Shreded.qualificationName as 'QualificationName',
		Shreded.warehouseTime as 'WarehouseTime',
		Shreded.automaticVerification as 'ResultReleased',
        Shreded.resultSampled as 'ResultSampled',
		WEST.ScaleScore as 'ScaleScore',
		WEST.WarehouseScaleScoreID as 'ScaleScoreId',
		WEST.EnableLOBoundaries as 'LoBoundariesEnabled',
		WEST.LearningOutcomesThreshold as 'LoThreshold',
		WEST.CustomRoundingEnabled as 'CustomRoundingEnabled',
		WEST.CustomRoundingDigits as 'CustomRoundingDigits',
		WEST.DurationMode,
		WEST.IsManuallySubmittedInLocal,
		WEST.SectionPoolData as 'SectionPoolData',
		Shreded.WarehouseExamState as 'ExamState',
        (select resultDataFull.query('/assessmentDetails[1]/assessment[1]/section') for xml path('sectionData'), type) as 'ItemData',
		WEST.resultDataFull.value('(/assessmentDetails/assessment/@passValue)[1]', 'bit') as 'Pass',
		(select ItemResponses.Comments as 'text', ItemResponses.ItemID as 'itemId'
			from dbo.WAREHOUSE_ExamSessionItemResponseTable as ItemResponses 
			where ItemResponses.WAREHOUSEExamSessionID = WEST.ID and ItemResponses.Comments IS NOT NULL for XML path('comment'), root('comments'), type) as 'ItemComments',
		(select ItemResponses.MarkerResponseData.query('/entries/entry/markerUserMark/mark'), ItemResponses.ItemID as 'itemId'
			from dbo.WAREHOUSE_ExamSessionItemResponseTable as ItemResponses 
			where ItemResponses.WAREHOUSEExamSessionID = WEST.ID for XML path('item'), root('marks'), type) as 'MarkResponse',
		(select LOBoundaries.Name as '@name',
				LOBoundaries.[Default] as '@isDefault',
				LOBoundaries.Value as '@passPercentage', 
				LOResults.LOName as 'loResult/@name', 
				LOResults.IsPass as 'loResult/@pass', 
				LOResults.Mark as 'loResult/@mark', 
				LOResults.TotalMark as 'loResult/@totalMark',
				LOResults.Percentage as 'loResult/@percentageMark', 
				LOResults.IsLoMetThreshold as 'loResult/@thresholdExceeded'
			from dbo.WAREHOUSE_LOBoundaryTable as LOBoundaries
			left join dbo.WAREHOUSE_LearningOutcomeResultsTable as LOResults
				on LOResults.WarehouseLoBoundaryId = LOBoundaries.ID
			where LOBoundaries.WarehouseExamSessionId = @examSessionId for XML path('loBoundary'), root('loBoundaries'), type) as 'LOBoundaries',
		(select sectionSelector.SectionSelectorID as 'id',
				sectionSelector.Name as 'name',
				sectionSelector.Description as 'description',
				sectionSelector.SelectorScreenDuration as 'duration',
				sectionSelector.RequiredSections as 'requiredSections',
				section.SectionID as 'id', 
				section.SectionSelected as 'sectionSelected'
		from [dbo].[WAREHOUSE_ExamSectionSelectorTable] as sectionSelector
		left join [dbo].[WAREHOUSE_ExamSectionsTable] as section
		on section.ExamSessionID = sectionSelector.ExamSessionID 
		   AND section.SectionSelectorID = sectionSelector.SectionSelectorID
		where sectionSelector.ExamSessionID = @examSessionId
		for XML AUTO, root('sectionSelectors'), type) as 'SectionSelectorsData',
		
		(SELECT 
		   section.[SectionID] as 'id',
		   section.[StartTime] as 'startTime',
		   section.[EndTime] as 'endTime'    
		FROM [dbo].[WAREHOUSE_ExamSessionTable] west
		INNER JOIN dbo.WAREHOUSE_ExamSectionsTable section  
		ON section.ExamSessionID = west.ID
		where west.ID = @examSessionId AND section.SectionID <> 0
		for XML AUTO, root('sectionDatedata'), type) as 'SectionDateData',
		
		Shreded.resultData.value('(/exam/@userTheta)[1]', 'nvarchar(max)') as 'UserTheta',
		
		CASE WHEN ISNULL(Shreded.passType,0) = 2 THEN Shreded.passMark 
			 ELSE NULL 
		END 'PassIrt',
		(SELECT resultDataFull.query('/assessmentDetails[1]/irtDomain')) AS 'IrtDomainData'
				
    from dbo.WAREHOUSE_ExamSessionTable_Shreded as Shreded
	inner join dbo.WAREHOUSE_ExamSessionTable as WEST
		on Shreded.examSessionId = WEST.ID
	inner join dbo.WAREHOUSE_ScheduledExamsTable AS WSET
		on Shreded.WAREHOUSEScheduledExamID = WSET.ID
    inner join dbo.WAREHOUSE_UserTable as Users
		on Users.ID = WEST.WAREHOUSEUserID
	inner join dbo.WAREHOUSE_CentreTable as Centres
		on Centres.ID = WSET.WAREHOUSECentreID
    where  Shreded.examSessionId = @examSessionId