SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

select 
       SUBSTRING(ITT.ID, 0, CHARINDEX('P', ITT.ID)) [ProjectID]
       , PLT.[Name] [Project Name]
       , SUBSTRING(PT.ID, CHARINDEX('P', PT.ID)+1,LEN(PT.ID)) [PageID]
       , ITT.ID [FullID]
     --  , CONVERT(xml, ITT.ItemValue) html
INTO [ItemHolder]
from ItemTextBoxTable ITT

inner join ProjectListTable PLT
on PLT.id = SUBSTRING(ITT.ID, 0, CHARINDEX('P', ITT.ID))

CROSS APPLY Projectstructurexml.nodes('Pro//Pag') Pro(Pag)

inner join PageTable PT
on PT.ID = SUBSTRING(ITT.ID, 0, CHARINDEX('S', ITT.ID))
	and pt.ParentID = plt.ID
	and SUBSTRING(PT.ID, CHARINDEX('P', PT.ID)+1,LEN(PT.ID)) = Pro.Pag.value('@ID','int')

where CONVERT(xml, ITT.ItemValue).exist('ItemValue/P') = 1 
	and
	Pro.Pag.value('@sta','smallint') between 

		PublishSettingsXml.value('data(PublishConfig/StatusLevelFrom)[1]','smallint') 
			and 
			PublishSettingsXml.value('data(PublishConfig/StatusLevelTo)[1]','smallint')


