USE [PPD_NCFE_SecureAssess];  

/*Find duplicates*/

SELECT *
FROM (

SELECT 
	ROW_NUMBER() OVER ( PARTITION BY 	
	 UserId
	, CandidateRef
	, Forename
	, Surname 
	, version
	ORDER BY 	
	 UserId
	, CandidateRef
	, Forename
	, Surname 
	, version
	
	) R
	, UserId
	, CandidateRef
	, Forename
	, Surname
	, version
FROM WAREHOUSE_UserTable
) A

where R > 1

--74718


SELECT  COUNT(Userid)
FROM WAREHOUSE_UserTable

SELECT distinct  Userid
FROM WAREHOUSE_UserTable






/*

Find unused

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	
SELECT @@Servername [ServerName]
 	, DB_Name() [DBName]
	, COUNT(ID) [Count]
FROM WAREHOUSE_UserTable
LEFT JOIN (

 
   SELECT WAREHOUSEUserID FROM WAREHOUSE_ExamSessionTable 
	UNION
   SELECT AssignedMarkerUserID FROM WAREHOUSE_ExamSessionTable 
   UNION 
   SELECT AssignedModeratorUserID FROM WAREHOUSE_ExamSessionTable 
   UNION 
   SELECT WAREHOUSECreatedBy FROM WAREHOUSE_ScheduledExamsTable
    UNION 
   SELECT Warehoused_UploadingUserID FROM WAREHOUSE_ExamSessionDocumentInfoTable 
   UNION 
   SELECT Warehoused_UserID FROM WAREHOUSE_ExamSessionDurationAuditTable 
   UNION
    SELECT 
	  y.value('userId[1]', 'int') AS UserId
	FROM WAREHOUSE_ExamSessionItemResponseTable
	CROSS APPLY MarkerResponseData.nodes('entries/entry[not(userId/text()=-1)]') x(y)  
) A
 on A.WAREHOUSEUserID = WAREHOUSE_UserTable.id

WHERE A.WAREHOUSEUserID IS NULL;

*/