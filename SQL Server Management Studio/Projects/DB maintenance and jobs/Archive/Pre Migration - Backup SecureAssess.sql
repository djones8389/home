USE msdb;
GO

EXEC dbo.sp_add_job
    @job_name = N'Pre-Migration - Backup SecureAssess' ;
GO



DECLARE @BackupSecureAssess nvarchar(MAX);
SELECT @BackupSecureAssess = 'DECLARE @BackupSecureAssess nvarchar(MAX) = '''';

DECLARE @BackupLocation nvarchar(MAX) = N''C:\Users\DaveJ\Desktop\BackupTest'';

SELECT @BackupSecureAssess +=CHAR(13) +  ''BACKUP DATABASE [''+ Name +''] TO DISK ='''''' + @BackupLocation + ''\'' + Name + '''' +  CONVERT(VARCHAR(10), GETDATE(), 102) + ''.bak'''' WITH COPY_ONLY, COMPRESSION, NOFORMAT, NOINIT, SKIP, COMPRESSION, NOREWIND, NOUNLOAD; ''
from sys.databases
where name like ''%SecureAssess%''
EXEC(@BackupSecureAssess)

'

EXEC sp_add_jobstep
    @job_name = N'Pre-Migration - Backup SecureAssess',
    @step_name = N'Backup SecureAssess',
    @subsystem = N'TSQL',
    @database_name = N'msdb',
    @on_success_action = 3,
    @on_fail_action = 2,
    @command = @BackupSecureAssess, 
    @retry_attempts = 0,
    @retry_interval = 0;
GO

DECLARE @SetRecovery nvarchar(MAX);

SELECT @SetRecovery = 'DECLARE @SetRecovery nvarchar(MAX) = '''';

SELECT @SetRecovery +=CHAR(13) +  ''ALTER DATABASE [''+ Name +''] SET RECOVERY SIMPLE WITH NO_WAIT;''
from sys.databases
where name like ''%SecureAssess%''
EXEC(@SetRecovery)

'

EXEC sp_add_jobstep
    @job_name = N'Pre-Migration - Backup SecureAssess',
    @step_name = N'Set Recovery to SIMPLE',
    @subsystem = N'TSQL',
    @database_name = N'msdb',
    @on_success_action = 1,
    @on_fail_action = 2,
    @command = @SetRecovery, 
    @retry_attempts = 0,
    @retry_interval = 0;
GO

EXEC dbo.sp_add_jobserver
    @job_name = N'Pre-Migration - Backup SecureAssess';
GO

