USE msdb;
GO

EXEC dbo.sp_add_job
    @job_name = N'Pre-Migration - Backup SecureAssess' ;
GO

DECLARE @CreateSABackupDevice nvarchar(MAX)= '';

SELECT @CreateSABackupDevice = '

DECLARE @BackupDefault TABLE (Data nvarchar(max))
INSERT @BackupDefault
VALUES (''E:\Backup\Pre-Migration'')

--DECLARE @BackupDefault TABLE (Value nvarchar(30), Data nvarchar(max))
--INSERT @BackupDefault
--EXEC  master.dbo.xp_instance_regread 
--N''HKEY_LOCAL_MACHINE'', N''Software\Microsoft\MSSQLServer\MSSQLServer'',N''BackupDirectory''


DECLARE @BackupToDefaultLoc nvarchar(MAX) = '''';

SELECT @BackupToDefaultLoc += CHAR(13) + ''

IF NOT EXISTS (SELECT 1 FROM sys.backup_devices where Name = N''''SecureAssess'''')

EXEC master.dbo.sp_addumpdevice  @devtype = N''''disk'''', @logicalname = N''''SecureAssess'''', @physicalname ='''''' + Data + ''\'' + ''Pre-Migration-SecureAssess.bak'''''' 
FROM @BackupDefault

EXEC(@BackupToDefaultLoc)


'

EXEC sp_add_jobstep
    @job_name = N'Pre-Migration - Backup SecureAssess',
    @step_name = N'Create Backup Device',
    @subsystem = N'TSQL',
    @database_name = N'msdb',
    @on_success_action = 3,
    @on_fail_action = 2,
    @command = @CreateSABackupDevice, 
    @retry_attempts = 0,
    @retry_interval = 0;
GO


DECLARE @BackupSecureAssess nvarchar(MAX) = '';

SELECT @BackupSecureAssess = 'DECLARE @BackupSecureAssess nvarchar(MAX) = '''';

SELECT @BackupSecureAssess +=CHAR(13) +  ''BACKUP DATABASE [''+ Name +''] TO [''+ ''SecureAssess'' +''] WITH COPY_ONLY, COMPRESSION, NOFORMAT, NOINIT, SKIP, NOREWIND, NOUNLOAD; ''
from sys.databases
where name like ''%SecureAssess''
EXEC(@BackupSecureAssess)

'

EXEC sp_add_jobstep
    @job_name = N'Pre-Migration - Backup SecureAssess',
    @step_name = N'Backup SecureAssess to Device',
    @subsystem = N'TSQL',
    @database_name = N'msdb',
    @on_success_action = 3,
    @on_fail_action = 2,
    @command = @BackupSecureAssess, 
    @retry_attempts = 0,
    @retry_interval = 0;
GO



DECLARE @SetRecovery nvarchar(MAX);

SELECT @SetRecovery = 'DECLARE @SetRecovery nvarchar(MAX) = '''';

SELECT @SetRecovery +=CHAR(13) +  ''ALTER DATABASE [''+ Name +''] SET RECOVERY SIMPLE WITH NO_WAIT;''
from sys.databases
where name like ''%SecureAssess''
EXEC(@SetRecovery)

'

EXEC sp_add_jobstep
    @job_name = N'Pre-Migration - Backup SecureAssess',
    @step_name = N'Set Recovery to SIMPLE',
    @subsystem = N'TSQL',
    @database_name = N'msdb',
    @on_success_action = 1,
    @on_fail_action = 2,
    @command = @SetRecovery, 
    @retry_attempts = 0,
    @retry_interval = 0;
GO

EXEC dbo.sp_add_jobserver
    @job_name = N'Pre-Migration - Backup SecureAssess';
GO

