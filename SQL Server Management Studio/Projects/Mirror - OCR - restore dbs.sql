/*
T:\BACKUP\OcrDataManagement.2017.06.20.bak
T:\BACKUP\OCR_ContentProducer.2017.06.20.bak
T:\BACKUP\OCR_ItemBank.2017.06.20.bak
T:\BACKUP\OCR_SecureAssess.2017.06.20.bak
*/

DECLARE @KILLER NVARCHAR(MAX) = ''
select @KILLER +=CHAR(13) + 'kill ' + convert(varchar(5),spid)
from sys.sysprocesses s
where dbid > 4
	 and  db_name(dbid) like 'Sax%'
exec(@KILLER)
Go

DROP DATABASE [Saxion_AnalyticsManagement];
DECLARE @KILLER NVARCHAR(MAX) = ''
select @KILLER +=CHAR(13) + 'kill ' + convert(varchar(5),spid)
from sys.sysprocesses s
where dbid > 4
	 and  db_name(dbid) like 'Sax%'
exec(@KILLER)
Go
DROP DATABASE [Saxion_ContentAuthor];
DECLARE @KILLER NVARCHAR(MAX) = ''
select @KILLER +=CHAR(13) + 'kill ' + convert(varchar(5),spid)
from sys.sysprocesses s
where dbid > 4
	 and  db_name(dbid) like 'Sax%'
exec(@KILLER)
Go
DROP DATABASE [Saxion_ContentProducer];
DECLARE @KILLER NVARCHAR(MAX) = ''
select @KILLER +=CHAR(13) + 'kill ' + convert(varchar(5),spid)
from sys.sysprocesses s
where dbid > 4
	 and  db_name(dbid) like 'Sax%'
exec(@KILLER)
Go
DROP DATABASE [Saxion_ItemBank];
DECLARE @KILLER NVARCHAR(MAX) = ''
select @KILLER +=CHAR(13) + 'kill ' + convert(varchar(5),spid)
from sys.sysprocesses s
where dbid > 4
	 and  db_name(dbid) like 'Sax%'
exec(@KILLER)
Go
DROP DATABASE [Saxion_SecureAssess];
DECLARE @KILLER NVARCHAR(MAX) = ''
select @KILLER +=CHAR(13) + 'kill ' + convert(varchar(5),spid)
from sys.sysprocesses s
where dbid > 4
	 and  db_name(dbid) like 'Sax%'
exec(@KILLER)
Go
DROP DATABASE [Saxion_SecureMarker];
DECLARE @KILLER NVARCHAR(MAX) = ''
select @KILLER +=CHAR(13) + 'kill ' + convert(varchar(5),spid)
from sys.sysprocesses s
where dbid > 4
	 and  db_name(dbid) like 'Sax%'
exec(@KILLER)
Go
DROP DATABASE [Saxion_SurpassDataWarehouse];
DECLARE @KILLER NVARCHAR(MAX) = ''
select @KILLER +=CHAR(13) + 'kill ' + convert(varchar(5),spid)
from sys.sysprocesses s
where dbid > 4
	 and  db_name(dbid) like 'Sax%'
exec(@KILLER)
Go
DROP DATABASE [Saxion_SurpassManagement];
DECLARE @KILLER NVARCHAR(MAX) = ''
select @KILLER +=CHAR(13) + 'kill ' + convert(varchar(5),spid)
from sys.sysprocesses s
where dbid > 4
	 and  db_name(dbid) like 'Sax%'
exec(@KILLER)
Go

RESTORE DATABASE [Ocr_DataManagement] FROM DISK = N'T:\BACKUP\OcrDataManagement.2017.06.20.bak' WITH FILE = 1, NOUNLOAD, NOREWIND, REPLACE, MEDIAPASSWORD = 's_!85WrenukEtrut+4f5a#-a&ac*aKUs', STATS = 10, MOVE 'Data' TO N'S:\DATA\OcrDataManagement.mdf', MOVE 'Log' TO N'L:\LOGS\OcrDataManagement.ldf'; 
DECLARE @KILLER NVARCHAR(MAX) = ''
select @KILLER +=CHAR(13) + 'kill ' + convert(varchar(5),spid)
from sys.sysprocesses s
where dbid > 4
	 and  db_name(dbid) like 'OCR%'
EXEC(@KILLER)
Go
--RESTORE DATABASE [OCR_ContentProducer] FROM DISK = N'T:\BACKUP\OCR_ContentProducer.2017.06.20.bak' WITH FILE = 1, NOUNLOAD, NOREWIND, REPLACE, MEDIAPASSWORD = 's_!85WrenukEtrut+4f5a#-a&ac*aKUs', STATS = 10, MOVE 'Data' TO N'S:\DATA\OCR_ContentProducer.mdf', MOVE 'Log' TO N'L:\LOGS\OCR_ContentProducer.ldf'; 
--DECLARE @KILLER NVARCHAR(MAX) = ''
--select @KILLER +=CHAR(13) + 'kill ' + convert(varchar(5),spid)
--from sys.sysprocesses s
--where dbid > 4
--	 and  db_name(dbid) like 'OCR%'
--EXEC(@KILLER)
--Go
RESTORE DATABASE [OCR_ItemBank] FROM DISK = N'T:\BACKUP\OCR_ItemBank.2017.06.20.bak' WITH FILE = 1, NOUNLOAD, NOREWIND, REPLACE, MEDIAPASSWORD = 's_!85WrenukEtrut+4f5a#-a&ac*aKUs', STATS = 10, MOVE 'Data' TO N'S:\DATA\OCR_ItemBank.mdf', MOVE 'Log' TO N'L:\LOGS\OCR_ItemBank.ldf'; 
DECLARE @KILLER NVARCHAR(MAX) = ''
select @KILLER +=CHAR(13) + 'kill ' + convert(varchar(5),spid)
from sys.sysprocesses s
where dbid > 4
	 and  db_name(dbid) like 'OCR%'
EXEC(@KILLER)
Go
RESTORE DATABASE [OCR_SecureAssess] FROM DISK = N'T:\BACKUP\OCR_SecureAssess.2017.06.20.bak' WITH FILE = 1, NOUNLOAD, NOREWIND, REPLACE, MEDIAPASSWORD = 's_!85WrenukEtrut+4f5a#-a&ac*aKUs', STATS = 10, MOVE 'Data' TO N'S:\DATA\OCR_SecureAssess.mdf', MOVE 'Log' TO N'L:\LOGS\OCR_SecureAssess.ldf'; 
DECLARE @KILLER NVARCHAR(MAX) = ''
select @KILLER +=CHAR(13) + 'kill ' + convert(varchar(5),spid)
from sys.sysprocesses s
where dbid > 4
	 and  db_name(dbid) like 'OCR%'
EXEC(@KILLER)
Go

/*
DECLARE @KILLER NVARCHAR(MAX) = ''
select @KILLER +=CHAR(13) + 'kill ' + convert(varchar(5),spid)
from sys.sysprocesses s
where dbid > 4
	 and  db_name(dbid) like 'OCR%'
EXEC(@KILLER)
Go


SET NOCOUNT ON; 

DECLARE @BackupFile NVARCHAR(255), 
            @Suffix NVARCHAR(10),
            @Prefix NVARCHAR(10),
			@MediaPassword NVARCHAR(100),
			@DefaultDataLoc NVARCHAR(512),
            @DefaultLogLoc NVARCHAR(512),
			@SQL NVARCHAR(4000),
			@Password nvarchar(35);

SET @Password = 's_!85WrenukEtrut+4f5a#-a&ac*aKUs';
SET @BackupFile = 'T:\BACKUP\OCR_SecureAssess.2017.06.20.bak';
SET @Suffix = N'';
SET @Prefix = N'';

BEGIN TRY
	RESTORE FILELISTONLY FROM DISK = @BackupFile 
	SET @MediaPassword = ''
END TRY
BEGIN CATCH
	SET @MediaPassword = 'MEDIAPASSWORD = '''+ @Password+ ''''
END CATCH


EXECUTE xp_instance_regread N'HKEY_LOCAL_MACHINE',N'Software\Microsoft\MSSQLServer\MSSQLServer',N'DefaultData', @DefaultDataLoc OUTPUT;
EXECUTE xp_instance_regread N'HKEY_LOCAL_MACHINE',N'Software\Microsoft\MSSQLServer\MSSQLServer',N'DefaultLog', @DefaultLogLoc OUTPUT;

IF (@DefaultDataLoc IS NULL OR @DefaultLogLoc IS NULL) Select 'Default Locations need defining!!!'
	--set @DefaultDataLoc= 'S:\Data'
	--set @DefaultLogLoc = 'L:\Log'

DECLARE @Header TABLE (
      BackupName NVARCHAR(128)
      ,BackupDescription NVARCHAR(255)
      ,BackupType SMALLINT
      ,ExpirationDate DATETIME
      ,Compressed BIT
      ,Position SMALLINT
      ,DeviceType TINYINT
      ,UserName NVARCHAR(128)
      ,ServerName NVARCHAR(128)
      ,DatabaseName NVARCHAR(128)
      ,DatabaseVersion INT
      ,DatabaseCreationDate DATETIME
      ,BackupSize NUMERIC(20, 0)
      ,FirstLSN NUMERIC(25, 0)
      ,LastLSN NUMERIC(25, 0)
      ,CheckpointLSN NUMERIC(25, 0)
      ,DatabaseBackupLSN NUMERIC(25, 0)
      ,BackupStartDate DATETIME
      ,BackupFinishDate DATETIME
      ,SortOrder SMALLINT
      ,CodePage SMALLINT
      ,UnicodeLocaleId INT
      ,UnicodeComparisonStyle INT
      ,CompatibilityLevel TINYINT
      ,SoftwareVendorId INT
      ,SoftwareVersionMajor INT
      ,SoftwareVersionMinor INT
      ,SoftwareVersionBuild INT
      ,MachineName NVARCHAR(128)
      ,Flags INT
      ,BindingID UNIQUEIDENTIFIER
      ,RecoveryForkID UNIQUEIDENTIFIER
      ,Collation NVARCHAR(128)
      ,FamilyGUID UNIQUEIDENTIFIER
      ,HasBulkLoggedData BIT
      ,IsSnapshot BIT
      ,IsReadOnly BIT
      ,IsSingleUser BIT
      ,HasBackupChecksums BIT
      ,IsDamaged BIT
      ,BeginsLogChain BIT
      ,HasIncompleteMetaData BIT
      ,IsForceOffline BIT
      ,IsCopyOnly BIT
      ,FirstRecoveryForkID UNIQUEIDENTIFIER
      ,ForkPointLSN NUMERIC(25, 0) NULL
      ,RecoveryModel NVARCHAR(60)
      ,DifferentialBaseLSN NUMERIC(25, 0) NULL
      ,DifferentialBaseGUID UNIQUEIDENTIFIER
      ,BackupTypeDescription NVARCHAR(60)
      ,BackupSetGUID UNIQUEIDENTIFIER NULL
      ,CompressedBackupSize NUMERIC(20, 0)
      );

IF @MediaPassword = '' 
	INSERT INTO @Header
	EXECUTE (N'RESTORE HEADERONLY FROM DISK = N''' + @BackupFile + ''' ');
ELSE 
	INSERT INTO @Header
	EXECUTE (N'RESTORE HEADERONLY FROM DISK = N''' + @BackupFile + ''' WITH ' +@MediaPassword+'');



DECLARE Header CURSOR
FOR
SELECT Position
      ,DatabaseName
FROM @Header;

DECLARE @File NVARCHAR(22),
      @DatabaseName NVARCHAR(128),
      @RestoreSize BIGINT = 0;

OPEN Header;

FETCH NEXT
FROM Header
INTO @File
      ,@DatabaseName;

WHILE @@FETCH_STATUS = 0
BEGIN
      DECLARE @FileList TABLE (
            LogicalName NVARCHAR(128)
            ,PhysicalName NVARCHAR(260)
            ,[Type] CHAR(1)
            ,FileGroupName NVARCHAR(128)
            ,Size NUMERIC(20, 0)
            ,MaxSize NUMERIC(20, 0)
            ,FileID BIGINT
            ,CreateLSN NUMERIC(25, 0)
            ,DropLSN NUMERIC(25, 0) NULL
            ,UniqueID UNIQUEIDENTIFIER
            ,ReadOnlyLSN NUMERIC(25, 0) NULL
            ,ReadWriteLSN NUMERIC(25, 0) NULL
            ,BackupSizeInBytes BIGINT
            ,SourceBlockSize INT
            ,FileGroupID INT
            ,LogGroupGUID UNIQUEIDENTIFIER NULL
            ,DifferentialBaseLSN NUMERIC(25, 0) NULL
            ,DifferentialBaseGUID UNIQUEIDENTIFIER
            ,IsReadOnly BIT
            ,IsPresent BIT
            ,TDEThumbPrint VARBINARY(32)
            );

IF @MediaPassword = '' 
      INSERT INTO @FileList
      EXECUTE (N'RESTORE FILELISTONLY FROM DISK = N''' + @BackupFile + ''' WITH FILE =  ' + @File +  ';');
ELSE
	  INSERT INTO @FileList
      EXECUTE (N'RESTORE FILELISTONLY FROM DISK = N''' + @BackupFile + ''' WITH FILE =  ' + @File + ','+ @MediaPassword +';');	

      DECLARE @DataFileName NVARCHAR(128)
            ,@LogFileName NVARCHAR(128);

IF @MediaPassword = ''       
      SET @SQL = N'RESTORE DATABASE [' + @Prefix + @DatabaseName + @Suffix +N'] FROM DISK = N''' + @BackupFile + ''' WITH FILE = ' + @File + ', NOUNLOAD, NOREWIND, REPLACE, STATS = 10,';
ELSE
      SET @SQL = N'RESTORE DATABASE [' + @Prefix + @DatabaseName + @Suffix +N'] FROM DISK = N''' + @BackupFile + ''' WITH FILE = ' + @File + ', NOUNLOAD, NOREWIND, REPLACE, ' + @MediaPassword + ', STATS = 10,';


      DECLARE DataFiles CURSOR FOR
            SELECT LogicalName
            FROM @FileList
            WHERE [Type] = N'D';

      OPEN DataFiles;
      
      FETCH NEXT FROM DataFiles INTO @DataFileName;
      WHILE @@FETCH_STATUS = 0
      BEGIN
            SET @SQL = @SQL + N' MOVE '''+@DataFileName+''' TO N'''+@DefaultDataLoc + '\' +@Prefix+@DatabaseName+@Suffix+'.mdf'',';
            FETCH NEXT FROM DataFiles INTO @DataFileName;
      END
      
      CLOSE DataFiles;
      DEALLOCATE DataFiles;

      DECLARE LogFiles CURSOR FOR
           SELECT LogicalName
            FROM @FileList
            WHERE [Type] = N'L';
      
      OPEN LogFiles;
      
      FETCH NEXT FROM LogFiles INTO @LogFileName;
      WHILE @@FETCH_STATUS = 0
      BEGIN
            SET @SQL = @SQL + N' MOVE '''+@LogFileName+''' TO N'''+@DefaultLogLoc + '\' + @Prefix+@DatabaseName+@Suffix+'.ldf''; '
            FETCH NEXT FROM LogFiles INTO @LogFileName;
      END
            
      CLOSE LogFiles;
      DEALLOCATE LogFiles;

      SELECT @RestoreSize = @RestoreSize + SUM(Size)
      FROM @FileList;

      --SET @SQL = LEFT(@SQL, (LEN(@SQL) - 1)) + ';';

      PRINT @SQL;

      DELETE
      FROM @FileList;

      FETCH NEXT
      FROM Header
      INTO @File
            ,@DatabaseName;
END

CLOSE Header;

DEALLOCATE Header;

--PRINT '--' + CONVERT(NVARCHAR(10),(((@RestoreSize / 1000) / 1000) / 1000))	+N' GB of storage needed';
*/







