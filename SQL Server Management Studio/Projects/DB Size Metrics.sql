select Data.DBName
	, Data.[Data File Size - KB]
	, [Log].[Log File Size - KB]
	,[DataPages].total_pages
	, [DataPages].used_pages
FROM (
select db_name() [DBName]
	, cast ((size * 8) / 1024 as float) [Data File Size - KB]
from sys.master_files
where database_id = db_ID()
	and type_desc = 'ROWS'
) Data
INNER JOIN (

select db_name() [DBName]
	, cast ((size * 8) as float) [Log File Size - KB]
from sys.master_files
where database_id = db_ID()
	and type_desc = 'Log'

) [Log]
on [Data].DBName = [Log].DBName
INNER JOIN (

	SELECT db_name() [DBName]
		,CAST(((SUM(used_pages) * 8192) / 1048576.0) AS FLOAT) [used_pages] 
		,CAST(((SUM(total_pages) * 8192) / 1048576.0) AS FLOAT) [total_pages] 
	FROM sys.allocation_units
) [DataPages]
ON [DataPages].DBName = [Data].DBName


DECLARE @LogSize TABLE (
	DBName sysname
	, logSizeMB real
	, [LogSpace%] real
	,[Status] bit
	)

INSERT @LogSize
exec ('dbcc sqlperf(logspace)')

select *
from @LogSize



SELECT db_name() [DBName]
	,CAST(((SUM(used_pages) * 8192) / 1048576.0) AS FLOAT) [used_pages] 
	,CAST(((SUM(total_pages) * 8192) / 1048576.0) AS FLOAT) [total_pages] 
FROM sys.allocation_units

SELECT CAST(((SUM(used_pages) * 8192) / 1048576.0) AS FLOAT) [used_pages] FROM sys.allocation_units