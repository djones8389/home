CREATE TABLE ##Users (
 server_name SYSNAME,
 database_Name SYSNAME,
 user_forename NVARCHAR(100),
 user_surname NVARCHAR(100),
 user_username NVARCHAR(100),
 user_disabled BIT,
 user_email NVARCHAR(100)
);

EXECUTE sp_MSforeachdb
N'
USE [?];

IF (SELECT SUM(1) FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME IN (N''UserTable'', N''Users'') AND COLUMN_NAME IN (N''Forename'', N''FirstName'', N''Surname'', N''LastName'', N''Email'',N''Retired'', N''IsDisabled'',N''Disabled'',N''UserDisabled'',N''Username'')) > 3
BEGIN
 DECLARE @tSQL NVARCHAR(MAX);
 SELECT @tSQL = N''

SELECT @@SERVERNAME
	, N''''?''''
	, ''+
  CASE 
   WHEN EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME IN (N''UserTable'', N''Users'') AND COLUMN_NAME = N''Forename'') THEN N''Forename''
   ELSE N''FirstName''
  END+N''
  , ''
  +CASE 
   WHEN EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME IN (N''UserTable'', N''Users'') AND COLUMN_NAME = N''Surname'') THEN N''Surname''
   ELSE N''LastName''
  END+N''
    , ''
  +CASE 
   WHEN EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME IN (N''UserTable'', N''Users'') AND COLUMN_NAME = N''Username'') THEN N''Username''
   ELSE N''Username''
  END+N''
  , ''
  +CASE 
   WHEN EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = N''UserTable'' AND COLUMN_NAME = N''UserDisabled'') THEN N''UserDisabled''
   ELSE N''Retired''
  END
  +N''
  , email FROM dbo.''
  +CASE
   WHEN EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = N''UserTable'') THEN N''UserTable''
   ELSE N''Users''
  END
  
  +N'' WHERE email IS NOT NULL '';
 
 BEGIN TRY
  INSERT INTO ##Users EXECUTE(@tSQL);
 END TRY
 BEGIN CATCH
 END CATCH
END
';

SELECT * FROM ##Users;

DROP TABLE ##Users;