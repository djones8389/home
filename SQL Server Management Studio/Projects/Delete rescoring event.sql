DECLARE @rescoringeventid smallint = (
	SELECT [Id]
	FROM [Windesheim_SurpassDataWarehouse].[dbo].[RescoringEvents]
	where subjectname = 'Bedrijfstakanalyse (FSMvM2.BTA)'
)

  delete from RescoringEventItems where rescoringeventid = @rescoringeventid  
  delete from RescoringHistory where rescoringeventid = @rescoringeventid
  delete from [RescoringEventToScaleScores] where rescoringeventid = @rescoringeventid
  delete from [RescoringEvents] where id = @rescoringeventid