SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

SET STATISTICS IO ON

SELECT
	WEST.id
	,  KeyCode
	,  b.ItemId [XML ItemID]
	,  WESDT.itemId [DocumentTable ItemID]
	,  WarehouseExamState
	,  warehouseTime
FROM WAREHOUSE_ExamSessionDocumentTable WESDT
INNER JOIN (
	SELECT ID
		,   KeyCode
		,  WarehouseExamState
		,  warehouseTime
	FROM WAREHOUSE_ExamSessionTable
	where WarehouseExamState = 5
	) WEST
on WEST.id = WESDT.warehouseExamSessionID
LEFT JOIN (
	select
		  WAREHOUSE_ExamSessionItemResponseTable.WAREHOUSEExamSessionID AS ExamSessionId,
		  WAREHOUSE_ExamSessionItemResponseTable.ItemID + 'S' +
		  x.value('../../../@id', 'VARCHAR(2)') + 'C' +
		  x.value('../../@id', 'VARCHAR(2)') + 'T' +
		  x.value('../../@id', 'VARCHAR(2)') AS ItemId,
		  x.value('../@att', 'int') AS numAttempts
	from WAREHOUSE_ExamSessionItemResponseTable
	cross apply WAREHOUSE_ExamSessionItemResponseTable.ItemResponseData.nodes('p/s/c/i/data') AS p(x)

) B
	on WESDT.warehouseExamSessionID = B.ExamSessionId
		and WESDT.itemId = b.ItemId;



	--where WAREHOUSEExamSessionID = 16625
/*
select *
from WAREHOUSE_ExamSessionDocumentTable
where WAREHOUSE_ExamSessionDocumentTable.warehouseExamSessionID = 16625
*/
--select myAudioResponses.ExamSessionId, myAudioResponses.ItemId, numAttempts
--from myAudioResponses
--left join WAREHOUSE_ExamSessionDocumentTable
--on 
--      WAREHOUSE_ExamSessionDocumentTable.WAREHOUSEExamSessionID = myAudioResponses.ExamSessionId
--      AND
--      WAREHOUSE_ExamSessionDocumentTable.itemId = myAudioResponses.ItemId
--where documentName IS NULL
