USE [Saxion_SecureMarker]
GO
/****** Object:  StoredProcedure [dbo].[sm_STATEMANAGEMENT_GetMarkingCompleteExamSessionsForExport_sp]    Script Date: 03/02/2017 11:03:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =================================================================================
-- Author:		Dave Dixon
-- Create date: 27/05/2011
-- Description:	sproc to select Candidate Exam Versions (sessions) to export to SecureAssess
--				as Result Pending (State 3 - Marking Complete)
-- Modified:	Tom Gomersall
-- Date:		04/09/2012
-- Description:	Added marked metadata response.

-- Modified:	Anton Burdyugov
-- Date:		01/10/2013
-- Description:	Moderator changes support added. Corrected marked metadata response determination.

-- Modified:	George Chernov
-- Date:		01/10/2013
-- Description:	Return 10 top scripts
-- =================================================================================
--ALTER PROCEDURE [dbo].[sm_STATEMANAGEMENT_GetMarkingCompleteExamSessionsForExport_sp]
--AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
    SET NOCOUNT ON;
	--CLB9YN6P
    
    DECLARE @TempTable TABLE
		(
			CEV_Id INT,
			CEV_ExternalSessionID INT,
			CEV_Keycode NVARCHAR(12),
			CEV_VIP BIT,
			CEV_DateSubmitted DATETIME
		)
		
	INSERT INTO @TempTable
	SELECT distinct TOP 10 
		CEV.ID,
		CEV.ExternalSessionID,
		CEV.Keycode,
		CEV.VIP,
		CEV.DateSubmitted
	FROM   dbo.CandidateExamVersions CEV
           INNER JOIN dbo.CandidateGroupResponses CGR ON  CGR.CandidateExamVersionID = CEV.ID          
           INNER JOIN dbo.UniqueGroupResponses UGR ON  UGR.ID = CGR.UniqueGroupResponseID
    WHERE  CEV.ExamSessionState = 3 
		--and   Keycode = 'XBH3GL6P'
		   AND UGR.GroupDefinitionID IN 
			(
				SELECT  MAX(UGR2.GroupDefinitionID) 
				FROM ..CandidateGroupResponses CGR2
					INNER JOIN dbo.UniqueGroupResponses UGR2 ON UGR2.ID = CGR2.UniqueGroupResponseID
					INNER JOIN dbo.UniqueGroupResponseLinks UGRL2 ON UGRL2.UniqueGroupResponseID = UGR2.ID
					INNER JOIN dbo.GroupDefinitionStructureCrossRef GDSCR ON GDSCR.GroupDefinitionID = UGR2.GroupDefinitionID
					INNER JOIN dbo.ExamVersionStructures EVS ON  EVS.ID = GDSCR.ExamVersionStructureID				
				WHERE   CGR2.CandidateExamVersionID = CEV.ID
				    AND GDSCR.Status = 0 -- 0 = Active
					AND EVS.StatusID = 0 -- 0 = Released
				GROUP BY UGRL2.UniqueResponseId
			)	
    ORDER BY
           CEV.VIP DESC
          ,CEV.DateSubmitted ASC
    
	;WITH ExamsToExport AS 
	(
		SELECT TT.CEV_ID														AS 'ID'
			  ,TT.CEV_ExternalSessionID											AS 'ExternalSessionID'
			  ,TT.CEV_Keycode													AS 'Keycode'
			  ,TT.CEV_VIP													    AS 'VIP'
			  ,TT.CEV_DateSubmitted												AS 'DateSubmitted'
			  ,I.ExternalItemID													AS 'ExternalItemID'
			  ,MS.version														AS 'MarkSchemeVersion'      
			  ,ISNULL(M.Mark, AIM.AssignedMark)									AS 'confirmedMark'
			  ,ISNULL(M.markedMetadataResponse, AIM.markedMetadataResponse)		AS 'MarkedMetadataResponse'
			  ,ISNULL(M.Timestamp, ISNULL(AGM.timestamp, TT.CEV_DateSubmitted))	AS 'dateMarked'
			  ,ISNULL(U1.Username ,ISNULL(U.Username ,'unknown'))				AS 'Username'
			  ,(CASE 
					WHEN M.ID IS NOT NULL THEN ISNULL(M.AnnotationData, '')
					ELSE ISNULL(AIM.AnnotationData, '')
				END)															AS 'annotationData'
			  , (SELECT CAST(
					CASE WHEN EXISTS(
								SELECT * 
								  FROM dbo.UploadedFilesDetails ufd
							INNER JOIN dbo.UniqueResponseDocuments urd ON ufd.UniqueResponseDocumentID = urd.ID
								 WHERE urd.UniqueResponseID = UR.ID and ufd.UploadedByUserID IS NOT NULL) THEN 1 
						ELSE 0 
					END 
				AS BIT))														AS 'hasAdditionalFiles'

			  ,ROW_NUMBER() OVER (PARTITION BY TT.CEV_ExternalSessionID, I.ExternalItemID ORDER BY ISNULL(M.Timestamp, ISNULL(AGM.timestamp, TT.CEV_DateSubmitted	)) DESC) AS RowNumber
          
		FROM   @TempTable TT
			   INNER JOIN dbo.CandidateGroupResponses CGR ON  CGR.CandidateExamVersionID = TT.CEV_ID
			   ----------                    
			   INNER JOIN dbo.UniqueGroupResponses UGR ON  UGR.ID = CGR.UniqueGroupResponseID
			   INNER JOIN dbo.GroupDefinitionItems GDI ON  GDI.GroupID = UGR.GroupDefinitionID
						AND GDI.ViewOnly = 0 -- only for marking
			   ----------                        
			   INNER JOIN dbo.CandidateResponses CR ON  CR.CandidateExamVersionID = TT.CEV_ID
			   INNER JOIN dbo.UniqueResponses UR ON  UR.id = CR.UniqueResponseId AND UR.itemId = GDI.ItemID
			   INNER JOIN dbo.Items I ON  I.ID = UR.itemId
			   LEFT JOIN dbo.AssignedGroupMarks AGM ON  AGM.UniqueGroupResponseId = CGR.UniqueGroupResponseID AND AGM.IsConfirmedMark = 1
			   LEFT JOIN dbo.AssignedItemMarks AIM ON  AIM.UniqueResponseId = CR.UniqueResponseId AND AIM.GroupMarkId = AGM.ID           
			   ----------
			   LEFT JOIN dbo.Moderations M ON M.UniqueResponseId = UR.id AND M.CandidateExamVersionId = TT.CEV_ID AND M.IsActual = 1
			   ----------		                        
			   LEFT JOIN dbo.Users U ON  AGM.userId = U.id
			   LEFT JOIN dbo.Users U1 ON  U1.id = M.UserId
			   LEFT JOIN dbo.MarkSchemes MS ON  UR.markSchemeId = MS.id	                        
		WHERE  UGR.GroupDefinitionID IN 
				(
					SELECT  MAX(UGR2.GroupDefinitionID) 
					FROM ..CandidateGroupResponses CGR2
						INNER JOIN dbo.UniqueGroupResponses UGR2 ON UGR2.ID = CGR2.UniqueGroupResponseID
						INNER JOIN dbo.UniqueGroupResponseLinks UGRL2 ON UGRL2.UniqueGroupResponseID = UGR2.ID
						INNER JOIN dbo.GroupDefinitionStructureCrossRef GDSCR ON GDSCR.GroupDefinitionID = UGR2.GroupDefinitionID
						INNER JOIN dbo.ExamVersionStructures EVS ON  EVS.ID = GDSCR.ExamVersionStructureID				
					WHERE   CGR2.CandidateExamVersionID = TT.CEV_ID
						AND GDSCR.Status = 0 -- 0 = Active
						AND EVS.StatusID = 0 -- 0 = Released
					GROUP BY UGRL2.UniqueResponseId
				)
	)
	SELECT *
	FROM ExamsToExport
	WHERE RowNumber = 1

	ORDER BY 
			VIP DESC,
			ExternalSessionID
END

