USE SANDBOX_Evolve_ItemBank_TOM

--DYNAMIC--

if OBJECT_ID('tempdb..#MyTest') IS NOT NULL DROP TABLE #MyTest;

SELECT *
into #MyTest
FROM AssessmentAuditTable NOLOCK

--SELECT *
--from #MyTest;

declare @Dynamic nvarchar(max)=''
select @Dynamic +=CHAR(13) + 
	 'UPDATE #MyTest
	 set Assessment.modify(''replace value of (Assessment/ID/text())[1] with ("'+cast(AssessmentID as nvarchar(10))+'")'')
	 WHERE ID = '+cast(ID as nvarchar(10))+'
	 ' 
from #MyTest;
EXEC(@Dynamic)

--SELECT *
--from #MyTest;

