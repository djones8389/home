use master

use ReportServer

select distinct 
	OBJECT_NAME(object_id) [Name]
	, row_count
into #original
from sys.dm_db_partition_stats
where OBJECT_NAME(object_id) not like 'sys%'



use [Report1ReportServer]

select distinct 
	OBJECT_NAME(object_id) [Name]
	, row_count
into #new
from sys.dm_db_partition_stats
where OBJECT_NAME(object_id) not like 'sys%'


select * 
	, 'INSERT [ReportServer].[dbo].'+quotename(o.name) + ' SELECT * FROM [Report1ReportServer].[dbo].'+quotename(o.name)
from #original o
inner join #new n
on o.name = n.Name
where o.row_count <> n.row_count

INSERT [ReportServer].[dbo].[ExecutionLogStorage] SELECT * FROM [Report1ReportServer].[dbo].[ExecutionLogStorage]
INSERT [ReportServer].[dbo].[Notifications] SELECT * FROM [Report1ReportServer].[dbo].[Notifications]
--INSERT [ReportServer].[dbo].[Policies] SELECT * FROM [Report1ReportServer].[dbo].[Policies]
INSERT [ReportServer].[dbo].[PolicyUserRole] SELECT * FROM [Report1ReportServer].[dbo].[PolicyUserRole]
INSERT [ReportServer].[dbo].[ReportSchedule] SELECT * FROM [Report1ReportServer].[dbo].[ReportSchedule]
INSERT [ReportServer].[dbo].[Roles] SELECT * FROM [Report1ReportServer].[dbo].[Roles]
INSERT [ReportServer].[dbo].[Schedule] SELECT * FROM [Report1ReportServer].[dbo].[Schedule]
INSERT [ReportServer].[dbo].[SecData] SELECT * FROM [Report1ReportServer].[dbo].[SecData]
INSERT [ReportServer].[dbo].[ServerParametersInstance] SELECT * FROM [Report1ReportServer].[dbo].[ServerParametersInstance]
INSERT [ReportServer].[dbo].[Subscriptions] SELECT * FROM [Report1ReportServer].[dbo].[Subscriptions]
INSERT [ReportServer].[dbo].[Users] SELECT * FROM [Report1ReportServer].[dbo].[Users]