begin tran

--https://stackoverflow.com/questions/3498499/transfer-subscription-reporting-service

	DECLARE @Default_User varchar(50) 
	SELECT @Default_User = (select UserID FROM ReportServer.dbo.Users WHERE UserName = 'SALTSWHARF\DaveJ')

	INSERT INTO ReportServer.dbo.Subscriptions(SubscriptionID, OwnerID, Report_OID,  Locale, InactiveFlags, ExtensionSettings, ModifiedByID, ModifiedDate, [Description], LastStatus, EventType, MatchData, LastRunTime, [Parameters], DataSettings, DeliveryExtension, Version)
	SELECT 
		--cSource.Path,
		--uSource.UserName,
		SubscriptionID,
		--u.UserName,
		--LastStatus,
		COALESCE(uTarget.UserID, @Default_User) AS OwnerID,
		cTarget.ItemID,
		Locale, InactiveFlags, ExtensionSettings,
		@Default_User AS ModifiedByID,
		 GETDATE(),
		sSource.[Description], LastStatus, EventType, MatchData, LastRunTime, [Parameters], DataSettings, DeliveryExtension, Version

	FROM Report1ReportServer.dbo.Subscriptions sSource
		LEFT JOIN Report1ReportServer.dbo.Catalog cSource ON cSource.ItemId = sSource.Report_OID
		LEFT JOIN Report1ReportServer.dbo.Users uSource ON sSource.OwnerID = uSource.UserID
		LEFT JOIN ReportServer.dbo.Catalog cTarget ON cTarget.Path = cSource.Path
		LEFT JOIN ReportServer.dbo.Users uTarget ON uTarget.UserName = uSource.UserName
	WHERE sSource.SubscriptionID NOT IN (SELECT SubscriptionID FROM ReportServer.dbo.Subscriptions)
		and cTarget.ItemID is not null

	--select * from Report1ReportServer.dbo.Users 

	INSERT INTO [ReportServer].dbo.Schedule(ScheduleID, Name, StartDate, Flags, NextRunTime, LastRunTime, EndDate, RecurrenceType, MinutesInterval, DaysInterval, WeeksInterval, DaysOfWeek, DaysOfMonth, [Month], MonthlyWeek, State, LastRunStatus, ScheduledRunTimeout, EventType, EventData, Type, ConsistancyCheck, Path, CreatedById)
	SELECT
	ScheduleID, Name, StartDate, Flags, NextRunTime, LastRunTime, EndDate, RecurrenceType, MinutesInterval, DaysInterval, WeeksInterval, DaysOfWeek, DaysOfMonth, [Month], MonthlyWeek, State, LastRunStatus, ScheduledRunTimeout, EventType, EventData, Type, ConsistancyCheck, Path, 
	COALESCE(uTarget.UserID, @Default_User) AS CreatedById
	FROM [Report1ReportServer].dbo.Schedule s
	INNER JOIN [Report1ReportServer].dbo.Users uSource
	ON s.CreatedById = uSource.UserID
	LEFT JOIN [ReportServer].dbo.Users uTarget
	ON uSource.UserName = uTarget.UserName
	WHERE ScheduleID NOT IN (SELECT ScheduleID FROM [ReportServer].dbo.Schedule)
		and Name <> 'Test Schedule'
	

	INSERT INTO [ReportServer].dbo.ReportSchedule(ScheduleID, ReportID, SubscriptionID, ReportAction)
	SELECT
		rsSource.ScheduleID, cTarget.ItemID, rsSource.SubscriptionID, rsSource.ReportAction
	FROM [Report1ReportServer].dbo.ReportSchedule rsSource
	INNER JOIN [ReportServer].dbo.Schedule sTarget
	ON rsSource.ScheduleID = sTarget.ScheduleID
	INNER JOIN [Report1ReportServer].dbo.Catalog cSource
	On cSource.ItemID = rsSource.ReportID
	INNER JOIN [ReportServer].dbo.Catalog cTarget
	ON cSource.Path = cTarget.Path
	LEFT JOIN [ReportServer].dbo.ReportSchedule rsTarget
	ON  rsSource.ScheduleID = rsTarget.ScheduleID
		AND rsSource.ReportID = rsTarget.ReportID
		AND rsSource.SubscriptionID = rsTarget.SubscriptionID
	WHERE rsTarget.ReportID IS NULL

rollback

/*

select * from [Subscriptions]

update [Subscriptions]
set ownerid = (select UserID FROM ReportServer.dbo.Users WHERE UserName = 'SALTSWHARF\DaveJ')

select 'kill ' + convert(varchar(5),spid)
	,db_name(dbid)
from sys.sysprocesses s
where dbid > 4
	--and  db_name(dbid) = 'SaxionDefect_ItemBank'
order by 2; 


use master
RESTORE DATABASE [ReportServer] 
FROM DISK = N'D:\Backup\ReportServer.2017.11.17-beforesubscriptionchange.bak'
WITH FILE = 1
	,NOUNLOAD
	,NOREWIND
	,REPLACE
	,STATS = 10
	,MOVE 'ReportServer' TO N'E:\Data\2008R2\ReportServer.mdf'
	,MOVE 'ReportServer_log' TO N'E:\Log\2008R2\ReportServer.ldf';


BACKUP DATABASE [ReportServer] TO DISK = N'D:\Backup\ReportServer.2017.11.17-beforesubscriptionchange.bak'
	 WITH NAME = N'ReportServer- Database Backup', COPY_ONLY, NOFORMAT, NOINIT, SKIP, COMPRESSION, NOREWIND, NOUNLOAD, STATS = 10;

	 */