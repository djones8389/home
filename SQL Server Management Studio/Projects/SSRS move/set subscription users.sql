--Set correctly

	UPDATE [ReportServer].[dbo].[Subscriptions] 
	set OwnerID = b.OwnerID
	FROM [ReportServer].[dbo].[Subscriptions]  A
	INNER JOIN [Report1ReportServer].[dbo].[Subscriptions] B
	on a.SubscriptionID =  b.SubscriptionID

--Set to DaveJ

	UPDATE [Subscriptions]
	set ownerid = (select UserID FROM ReportServer.dbo.Users WHERE UserName = 'SALTSWHARF\DaveJ')

/*
select a.OwnerID
	, b.OwnerID
from (
SELECT [SubscriptionID]
      ,[OwnerID]
      ,[Report_OID]
      ,[Locale]
      ,[InactiveFlags]
      ,[ExtensionSettings]
      ,[ModifiedByID]
      ,[ModifiedDate]
      ,[Description]
      ,[LastStatus]
      ,[EventType]
      ,[MatchData]
      ,[LastRunTime]
      ,[Parameters]
      ,[DataSettings]
      ,[DeliveryExtension]
      ,[Version]
      ,[ReportZone]
  FROM [ReportServer].[dbo].[Subscriptions] 
  ) a

  inner join (
	  SELECT [SubscriptionID]
		  ,[OwnerID]
		  ,[Report_OID]
		  ,[Locale]
		  ,[InactiveFlags]
		  ,[ExtensionSettings]
		  ,[ModifiedByID]
		  ,[ModifiedDate]
		  ,[Description]
		  ,[LastStatus]
		  ,[EventType]
		  ,[MatchData]
		  ,[LastRunTime]
		  ,[Parameters]
		  ,[DataSettings]
		  ,[DeliveryExtension]
		  ,[Version]
		  ,[ReportZone]
	  FROM [Report1ReportServer].[dbo].[Subscriptions]

  ) b
  on a.SubscriptionID =  b.SubscriptionID
*/