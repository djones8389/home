select top 10
	west.ID
	, keycode
	, clientinformation.value('(clientInformation/systemConfiguration/secureClient/CurrentVersion)[1]','nvarchar(100)') SecureClientVersion
	, downloadinformation.value('data(downloads/information/machineName)[1]','nvarchar(100)') PCName
	, clientinformation.value('(clientInformation/systemConfiguration/identification/macAddress)[1]','nvarchar(100)') macAddress
	, clientinformation.value('(clientInformation/systemConfiguration/identification/ipAddress)[1]','nvarchar(100)') [PC IP Address]
	, downloadinformation.value('data(downloads/information/clientIP)[1]','nvarchar(100)') [Site IP	Address]
	, CentreName
	, CentreCode
	, warehouseTime
	, clientInformation
from WAREHOUSE_ExamSessionTable west
inner join WAREHOUSE_ScheduledExamsTable wscet on wscet.ID = west.WAREHOUSEScheduledExamID
inner join WAREHOUSE_CentreTable wct on wct.id = wscet.WAREHOUSECentreID

where PreviousExamState <> 10
	and clientinformation.exist('clientInformation/systemConfiguration[@web=0]') = 1
order by 1 desc


SELECT DISTINCT examsessionkey
	, [identification].value('data(identification/macAddress)[1]','nvarchar(MAX)') MacAddress
FROM [AAT_SurpassDataWarehouse].[dbo].[FactClientInformation]


--63446 = distinct on MAC only
--1435161 = distinct on ESID and MAC