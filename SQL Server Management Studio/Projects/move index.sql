use [PRV_BritishCouncil_SecureAssess]

ALTER DATABASE [PRV_BritishCouncil_SecureAssess]
	ADD FILEGROUP [SECONDARY]
GO

ALTER DATABASE [PRV_BritishCouncil_SecureAssess]
ADD FILE (

	NAME = PRV_BritishCouncil_SecureAssess_WAREHOUSE_ExamSessionItemResponseTable
	, FILENAME = 'S:\PRV_BritishCouncil_SecureAssess_WAREHOUSE_ExamSessionItemResponseTable.ndf'
    , SIZE = 5000MB
    , MAXSIZE = 100000MB
    , FILEGROWTH = 500MB

)
TO FILEGROUP [Secondary];
GO


CREATE UNIQUE CLUSTERED INDEX [PK_WAREHOUSEExamSessionItemResponseTable_1] ON [dbo].[WAREHOUSE_ExamSessionItemResponseTable] 
(
	[ID] ASC
)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, DROP_EXISTING = ON)
ON [Secondary] 
 


 sp_spaceused 'WAREHOUSE_ExamSessionItemResponseTable'