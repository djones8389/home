declare @table table (
	DBName sysname
	, LogSizeMB decimal(8,2)
	, LogSpaceUsed decimal(8,2)
	, Status bit
)

insert @table
exec('dbcc sqlperf(logspace)');

select c.*
	, 'USE '+DBName+'; DBCC SHRINKFILE ('''+[LogicalName]+''','+cast((FLOOR(LogSpaceUsed)+10) as nvarchar(100))+');' [Leave10MB]
from (
select a.DBName
	, a.LogSpaceUsed
	, b.name [LogicalName]
from @table a
inner join sys.master_files b
on a.DBName = db_name(b.database_id)
where database_id > 4
	and type_desc ='LOG'
	and (a.LogSizeMB - LogSpaceUsed) > 10 /*more than 10mb wasted?*/
) c
order by 1;
