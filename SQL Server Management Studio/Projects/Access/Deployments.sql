USE [MASTER];
GO

IF NOT EXISTS (
	SELECT 1 
	FROM syslogins 
	WHERE [NAME] = 'PRE-PROD-SQL-1\Deployments'
	)

CREATE LOGIN [PRE-PROD-SQL-1\Deployments] FROM WINDOWS WITH DEFAULT_DATABASE=[master]
ALTER SERVER ROLE [dbcreator] ADD MEMBER [PRE-PROD-SQL-1\Deployments]

GRANT CONNECT ANY DATABASE TO [PRE-PROD-SQL-1\Deployments];
GRANT VIEW ANY DATABASE TO [PRE-PROD-SQL-1\Deployments];
GRANT VIEW ANY DEFINITION TO [PRE-PROD-SQL-1\Deployments];
GRANT VIEW SERVER STATE TO [PRE-PROD-SQL-1\Deployments];
GRANT ALTER TRACE TO [PRE-PROD-SQL-1\Deployments];
GRANT ALTER ANY LOGIN TO [PRE-PROD-SQL-1\Deployments];


GRANT EXECUTE ON OBJECT::sp_whoisactive TO [PRE-PROD-SQL-1\Deployments];  

USE [MSDB];

exec sp_addrolemember 'SQLAgentOperatorRole','PRE-PROD-SQL-1\Deployments'
exec sp_addrolemember 'SQLAgentReaderRole','PRE-PROD-SQL-1\Deployments'
exec sp_addrolemember 'SQLAgentUserRole','PRE-PROD-SQL-1\Deployments'


--exec sp_msforeachdb '

--use [?];

--if (DB_name() in (
--	select name
--	from sys.databases
--	where user_access_desc = ''MULTI_USER''
--		and state_desc = ''ONLINE''
--		and database_id > 4)
--	)

--BEGIN

--	IF NOT EXISTS (
--	SELECT 1
--	FROM sys.database_principals
--	WHERE NAME = ''PRE-PROD-SQL-1\Deployments''
--	)

--	CREATE USER [PRE-PROD-SQL-1\Deployments] FOR LOGIN [PRE-PROD-SQL-1\Deployments]
--	exec sp_addrolemember ''db_datareader'',''PRE-PROD-SQL-1\Deployments''
--	exec sp_addrolemember ''db_datawriter'',''PRE-PROD-SQL-1\Deployments''

--	GRANT VIEW DEFINITION ON SCHEMA::[dbo] TO [PRE-PROD-SQL-1\Deployments]
--	DENY DELETE TO [PRE-PROD-SQL-1\Deployments]
--	DENY INSERT TO [PRE-PROD-SQL-1\Deployments]

--END

--'
