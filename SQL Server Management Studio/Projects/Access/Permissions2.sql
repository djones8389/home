--Drop
/*
exec sp_msforeachdb '

use [?];

if (DB_ID()>4)

BEGIN

	IF EXISTS (
	SELECT 1
	FROM sys.database_principals
	WHERE NAME = ''pre-prod-sql-1\AccessTestGroup''
	)

	DROP USER [pre-prod-sql-1\AccessTestGroup]

END

'
*/


--Add

use msdb

exec sp_addrolemember 'SQLAgentOperatorRole','pre-prod-sql-1\AccessTestGroup'
exec sp_addrolemember 'SQLAgentReaderRole','pre-prod-sql-1\AccessTestGroup'
exec sp_addrolemember 'SQLAgentUserRole','pre-prod-sql-1\AccessTestGroup'

use master;

GRANT CONNECT ANY DATABASE TO [PRE-PROD-SQL-1\AccessTestGroup];
GRANT VIEW ANY DATABASE TO [PRE-PROD-SQL-1\AccessTestGroup];
GRANT VIEW ANY DEFINITION TO [PRE-PROD-SQL-1\AccessTestGroup];
GRANT VIEW SERVER STATE TO [PRE-PROD-SQL-1\AccessTestGroup];
GRANT ALTER TRACE TO [PRE-PROD-SQL-1\AccessTestGroup];



exec sp_msforeachdb '

use [?];

if (DB_ID()>4)

BEGIN

	IF NOT EXISTS (
	SELECT 1
	FROM sys.database_principals
	WHERE NAME = ''pre-prod-sql-1\AccessTestGroup''
	)

	CREATE USER [pre-prod-sql-1\AccessTestGroup] FOR LOGIN [pre-prod-sql-1\AccessTestGroup]
	exec sp_addrolemember ''db_datareader'',''pre-prod-sql-1\AccessTestGroup''
	exec sp_addrolemember ''db_datawriter'',''pre-prod-sql-1\AccessTestGroup''

	GRANT VIEW DEFINITION ON SCHEMA::[dbo] TO [pre-prod-sql-1\AccessTestGroup]
	DENY DELETE TO [pre-prod-sql-1\AccessTestGroup]
	DENY INSERT TO [pre-prod-sql-1\AccessTestGroup]

END

'