CREATE TABLE #SchedulesToDelete 
(
	ID INT
	, Deleted bit NULL
)

INSERT #SchedulesToDelete(ID)
select A.ScheduledPackageCandidateId
FROM (


select spc.ScheduledPackageCandidateId
	, count(scheduledexamref) as KeycodeCount
from ScheduledPackageCandidateExams as SPCE

	inner join ScheduledPackageCandidates as SPC
	on SPC.ScheduledPackageCandidateId =  SPCE.Candidate_ScheduledPackageCandidateId
	
where  spc.DateCompleted < '01 Jan 2015'
	and spce.DateCompleted < '01 Jan 2015'
  group by   spc.ScheduledPackageCandidateId
) A 

INNER JOIN (

select spc.ScheduledPackageCandidateId
	, count(scheduledexamref) as VoidedKeycodeCount
from ScheduledPackageCandidateExams as SPCE

	inner join ScheduledPackageCandidates as SPC
	on SPC.ScheduledPackageCandidateId =  SPCE.Candidate_ScheduledPackageCandidateId
	
where 
	 spc.DateCompleted < '01 Jan 2015'
	and spce.DateCompleted < '01 Jan 2015'
	and spc.isvoided = 1
	and spce.isvoided = 1
	group by   spc.ScheduledPackageCandidateId
) B
on A.ScheduledPackageCandidateId = B.ScheduledPackageCandidateId
where KeycodeCount = VoidedKeycodeCount

create clustered index [IX] on [#SchedulesToDelete]
(
	ID
)

select * from #SchedulesToDelete order by 1

BEGIN TRAN

UPDATE TOP (1000) #SchedulesToDelete
set Deleted = 0;

DELETE [ScheduledPackageCandidateExams]
FROM [ScheduledPackageCandidateExams]
INNER JOIN #SchedulesToDelete on #SchedulesToDelete.ID = Candidate_ScheduledPackageCandidateId
where Deleted = 0;

DELETE [dbo].[ScheduledPackageCandidates]
FROM [ScheduledPackageCandidates]
INNER JOIN #SchedulesToDelete
on #SchedulesToDelete.ID = ScheduledPackageCandidateId
where Deleted = 0;

DELETE [dbo].[ScheduledPackages]
FROM [dbo].[ScheduledPackages] SP
INNER JOIN [dbo].[ScheduledPackageCandidates] SPC ON SPC.ScheduledPackage_ScheduledPackageId = SP.ScheduledPackageId
INNER JOIN #SchedulesToDelete on #SchedulesToDelete.ID = SPC.ScheduledPackageCandidateId
where  Deleted = 0;

DELETE [dbo].[RescheduleDetails]
  FROM [dbo].[RescheduleDetails] RD
  INNER JOIN [dbo].[ScheduledPackageCandidateExams] SPCE ON SPCE.ScheduledPackageCandidateExamId = RD.ScheduledPackageCandidateExamId
  INNER JOIN #SchedulesToDelete on #SchedulesToDelete.ID = SPCE.Candidate_ScheduledPackageCandidateId
  WHERE  Deleted = 0;

UPDATE #SchedulesToDelete
set Deleted = 1
where Deleted = 0; 
 
 
select * from #SchedulesToDelete
  
ROLLBACK  

begin tran

DELETE [dbo].[ScheduledPackageCandidateExamItems]
  FROM [dbo].[ScheduledPackageCandidateExamItems] SPCEI 
  INNER JOIN [dbo].[ScheduledPackageCandidateExams] SPCE 
  ON SPCEI.CandidateExam_ScheduledPackageCandidateExamId = SPCE.ScheduledPackageCandidateExamId
  INNER JOIN [ScheduledPackageCandidates]
  ON ScheduledPackageCandidateId =  SPCE.Candidate_ScheduledPackageCandidateId 
  INNER JOIN #SchedulesToDelete
  on #SchedulesToDelete.ID = ScheduledPackageCandidateId
  WHERE  Deleted = 0;
rollback

/*
ScheduledPackageCandidateId	KeycodeCount	VoidedKeycodeCount
260547	2	1
*/


--select spc.isvoided, spce.isvoided
--	, scheduledexamref
--, spc.datecompleted
--, spce.datecompleted
--from ScheduledPackageCandidateExams as SPCE

--	inner join ScheduledPackageCandidates as SPC
--	on SPC.ScheduledPackageCandidateId =  SPCE.Candidate_ScheduledPackageCandidateId
	
--where ScheduledPackageCandidateId= 258180