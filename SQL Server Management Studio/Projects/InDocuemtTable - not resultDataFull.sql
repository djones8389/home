select a.warehouseExamSessionID
	, a.itemID
	, a.uploadDate
from Warehouse_ExamSessionDocumenttable A
LEFT JOIN
(

select 
      WAREHOUSE_ExamSessionItemResponseTable.WAREHOUSEExamSessionID AS ExamSessionId
	, WAREHOUSE_ExamSessionItemResponseTable.ItemID + 'S' +
		  x.value('../../../@id', 'VARCHAR(2)') + 'C' +
		  x.value('../../@id', 'VARCHAR(2)') + 'T' +
		  x.value('../../@id', 'VARCHAR(2)') AS ItemId
     ,x.value('../@att', 'int') AS numAttempts
from WAREHOUSE_ExamSessionItemResponseTable
cross apply WAREHOUSE_ExamSessionItemResponseTable.ItemResponseData.nodes('p/s/c/i/data') AS p(x)

) B

ON A.ItemID = b.ItemId
	and a.warehouseExamSessionID = b.ExamSessionId

INNER JOIN (

	SELECT 
		a.b.value('@id','nvarchar(15)') itemID
		,a.b.value('@totalMark','tinyint') totalMark
	FROM WAREHOUSE_ExamSessionTable
	CROSS APPLY ResultData.nodes('exam/section/item') a(b)

) C

ON SUBSTRING(a.itemID, 0, CHARINDEX('S', a.itemID)) = C.itemID

where b.ItemId IS NULL
	and c.totalMark > 0


ORDER BY 1 DESC;