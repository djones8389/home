SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
SET NOCOUNT ON;

IF OBJECT_ID('tempdb..#Questions') IS NOT NULL DROP TABLE #Questions;
IF OBJECT_ID('tempdb..#Answers') IS NOT NULL DROP TABLE #Answers;

CREATE TABLE #Questions (
	ItemID VARCHAR(20)
	, Question VARCHAR(max)
	, Answers VARCHAR(max)
);

INSERT #Questions(ItemID,Question)
select distinct 
	itt.itemID
	, itt.Question
FROM ItemMultipleChoiceTable MCQ
INNER JOIN  (
	select a.itemID
		, a.Question
	from (
		select distinct substring(ID, 0, charindex('S',ID)) [ItemID]	
			, cast(ITT.ItemValue as xml).value('data(ItemValue/TEXTFORMAT/P/FONT)[1]','nvarchar(1000)') Question
			, ROW_NUMBER() OVER(PARTITION BY substring(ITT.ID, 0, charindex('S',ITT.ID)) ORDER BY ITT.Y ASC) R
		from [ItemTextBoxTable] ITT
	) a 
	where r = 1
) ITT
on ITT.itemID = substring(MCQ.ID, 0, charindex('S',MCQ.ID));


SELECT 
	substring(ID, 0, charindex('S',ID)) [ItemID]
	,[asc]
	,cast(mcq.ItemValue as xml).value('data(ItemValue/TEXTFORMAT/P/FONT)[1]','nvarchar(1000)') Answers
INTO #Answers
FROM [dbo].[ItemMultipleChoiceTable] mcq

CREATE CLUSTERED INDEX [IX_ItemID] on #Questions (itemid);
CREATE CLUSTERED INDEX [IX_ItemID] on #Answers (itemid);

DECLARE @string nvarchar(max) = '';

SELECT @string +=CHAR(13) + '

	UPDATE #Questions
	SET Answers = 	
	(
		SELECT  SUBSTRING(
		(
		SELECT N'', ''+QUOTENAME([answers])
		FROM #Answers
		where itemid  = '''+itemid+'''
		order by [asc]
		FOR XML PATH('''')
		), 2, 10000)
	)
	WHERE itemID = '''+itemid+'''
'
FROM (
	SELECT ItemID
	FROM #Questions
) Q

exec(@string);


select *
from #Questions