select uts.*
from [dbo].[UserExamHistoryCompletionDateTrackerTable] uts
where userid in (
	select id
	from usertable
	where candidateref = '20227393'

)

select count(ignoreresittime)
	,ignoreresittime
from [UserExamHistoryCompletionDateTrackerTable]
group by ignoreresittime

select west.warehousetime, west.resultdata, west.previousexamstate, structurexml, examstatechangeauditxml, west.exporttosecuremarker
from warehouse_examsessiontable_shreded wests
inner join warehouse_examsessiontable west on west.id = wests.examsessionid
where candidateref = '20227393'
	order by west.warehousetime

select agt.id, name, status, at.*
from AAT_ItemBank..AssessmentGroupTable AGT
inner join aat_itembank..assessmenttable at
on at.assessmentgroupid = agt.id
where qualificationid = 236
	and agt.status = 2

select at.*
from aat_itembank..assessmenttable at
where at.id = 4180

select qt.id, qualificationname
from AAT_ItemBank..Qualificationtable qt
where qt.id = 236