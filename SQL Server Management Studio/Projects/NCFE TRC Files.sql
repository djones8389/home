USE SPDurationMetrics


INSERT [BCTraceFiles]
select 
	ServerName
	, ObjectName
	, Duration
	, 'BTLSURPASSAUDIT_NCFE' [DB]
from ::fn_trace_gettable ('D:\New Folder\BTLSURPASSAUDIT_NCFE.trc',1000)

INSERT [BCTraceFiles]
select 
	ServerName
	, ObjectName
	, Duration
	, 'NCFE_CPProjectAdmin' [DB]
from ::fn_trace_gettable ('D:\New Folder\NCFE_CPProjectAdmin.trc',31)

INSERT [BCTraceFiles]
select 
	ServerName
	, ObjectName
	, Duration
	, 'NCFE_Itembank' [DB]
from ::fn_trace_gettable ('D:\New Folder\NCFE_Itembank.trc',31)

INSERT [BCTraceFiles]
select 
	ServerName
	, ObjectName
	, Duration
	, 'NCFE_SecureAssess' [DB]
from ::fn_trace_gettable ('D:\New Folder\NCFE_SecureAssess.trc',100)

INSERT [BCTraceFiles]
select 
	ServerName
	, ObjectName
	, Duration
	, 'NCFE_SurpassDataWarehouse' [DB]
from ::fn_trace_gettable ('D:\New Folder\NCFE_SurpassDataWarehouse.trc',31)

--INSERT [BCTraceFiles]
--select 
--	ServerName
--	, ObjectName
--	, Duration
--	, 'AQA_SecureMarker_10.0' [DB]
--from ::fn_trace_gettable ('D:\New Folder\AQA_SecureMarker_10.0.trc',31)


--INSERT [BCTraceFiles]
--select 
--	ServerName
--	, ObjectName
--	, Duration
--	, 'AQA_SurpassDataWarehouse' [DB]
--from ::fn_trace_gettable ('D:\New Folder\AQA_SurpassDataWarehouse.trc',31)


--INSERT [BCTraceFiles]
--select 
--	ServerName
--	, ObjectName
--	, Duration
--	, 'BTLSURPASSAUDIT_AQA.' [DB]
--from ::fn_trace_gettable ('D:\New Folder\BTLSURPASSAUDIT_AQA.trc',31)