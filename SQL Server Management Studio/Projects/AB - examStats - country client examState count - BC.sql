use master

if OBJECT_ID('tempdb..#examstats') is not null drop table #examstats;

create table #examstats (
	
	 ClientName sysname
	, PreviousExamState tinyint
	, ExamCount int
	
	, Country nvarchar(200)
)

bulk insert #examstats
from 'D:\CGSA query.csv'
--from 'D:\BC.csv'
with (fieldterminator=',',rowterminator='\n')

select * from #examstats

update #examstats
set ClientName = REPLACE(clientname, 'SANDBOX_BRITISHCOUNCIL_SecureAssess','BRITISHCOUNCIL_SecureAssess')

INSERT PSCollector..Country_Client_ExamCount_ExamState
select * from #examstats