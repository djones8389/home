/*
USE master;
	  
	CREATE MASTER KEY ENCRYPTION BY PASSWORD = 'iQI`M*A\1i#ymO3\27Rr';  

USE master;  

	CREATE CRYPTOGRAPHIC PROVIDER AzureKeyVault_EKM_Prov  
	FROM FILE = 'C:\Program Files\SQL Server Connector for Microsoft Azure Key Vault\Microsoft.AzureKeyVaultService.EKM.dll';  

USE master;  

	CREATE CREDENTIAL Azure_EKM_TDE_cred
	WITH IDENTITY = 'MyKey',   
	SECRET = 'eee8c4bff1444ddb9ad5e8ed6e7a3dfaxHWE/OK9EopNRdIfkBOmcTyUy38+1ATDOdd5zLFiKBw='   
	FOR CRYPTOGRAPHIC PROVIDER AzureKeyVault_EKM_Prov ;  


/* Modify the login to assign the cryptographic provider credential */  

	ALTER LOGIN TDELogin  
	ADD CREDENTIAL Azure_EKM_TDE_cred;  

/* Modify the login to assign a non cryptographic provider credential */   

	ALTER LOGIN TDELogin  
	WITH CREDENTIAL = Azure_EKM_TDE_cred;  
	GO  

*/



USE [TDE_Database];  
CREATE CERTIFICATE MyServerCert 
	WITH SUBJECT = 'My DEK Certificate';  
go  

USE [TDE_Database];  
GO  

CREATE DATABASE ENCRYPTION KEY  
WITH ALGORITHM = AES_128  
ENCRYPTION BY SERVER CERTIFICATE MyServerCert;  
GO  
ALTER DATABASE [TDE_Database] SET ENCRYPTION ON;  
GO  

	
USE [TDE_Database];  
GO  
CREATE ASYMMETRIC KEY EKM_askey1   
FROM PROVIDER AzureKeyVault_EKM_Prov  
WITH   
ALGORITHM = RSA_2048,   
CREATION_DISPOSITION = OPEN_EXISTING  
, PROVIDER_KEY_NAME  = 'MyKey' ;  
GO 