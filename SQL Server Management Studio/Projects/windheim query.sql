use Windesheim_Secureassess

select ID
	, surname
	, CandidateRef
	, SpecialRequirements.value('data(specialRequirements/level)[1]','nvarchar(200)') DDA
	, SpecialRequirements
	, IsCandidate
from UserTable
where LEN(cast(SpecialRequirements as nvarchar(MAX))) > 22

select count(ID)
from UserTable
where IsCandidate = 1