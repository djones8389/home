Declare @CandidateForeName nvarchar(100) = 'Redact';
Declare @CandidateSurName nvarchar(100) = 'Me';

Declare @NewForeName nvarchar(100) = 'NewFirstName';
Declare @NewSurName nvarchar(100) = 'NewSurname';
Declare @NewDOB smalldatetime = '1900-01-01 00:00:00'
	

IF EXISTS(SELECT 1 FROM [dbo].[UserTable] WHERE forename = @CandidateForeName and surname = @CandidateSurName)
	BEGIN

		UPDATE [UserTable]
		set foreName = @NewForeName
			, surName = @NewSurName
			, DOB = @NewDOB
		where forename = @CandidateForeName and surname = @CandidateSurName 

	END

IF EXISTS(SELECT 1 FROM [dbo].[WAREHOUSE_UserTable] WHERE forename = @CandidateForeName and surname = @CandidateSurName)
	BEGIN

		UPDATE [WAREHOUSE_UserTable]
		set foreName = @NewForeName
			, surName = @NewSurName
			, DOB = @NewDOB
		where forename = @CandidateForeName and surname = @CandidateSurName 

	END

IF EXISTS(SELECT 1 FROM [dbo].[ExamSessionTable_Shredded] WHERE forename = @CandidateForeName and surname = @CandidateSurName)
	BEGIN

		UPDATE [ExamSessionTable_Shredded]
		set foreName = @NewForeName
			, surName = @NewSurName
		where forename = @CandidateForeName and surname = @CandidateSurName 

	END

IF EXISTS(SELECT 1 FROM [dbo].[WAREHOUSE_ExamSessionTable_Shreded] WHERE forename = @CandidateForeName and surname = @CandidateSurName)
	BEGIN

		UPDATE [WAREHOUSE_ExamSessionTable_Shreded]
		set foreName = @NewForeName
			, surName = @NewSurName
			, dateOfBirth = @NewDOB
		where forename = @CandidateForeName and surname = @CandidateSurName
	 
	END
