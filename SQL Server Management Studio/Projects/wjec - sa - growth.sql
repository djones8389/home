SELECT A.Instance
	, [Date]
	, round(SUM(Whole_Database_KB)/1024/1024,0) [GB]
FROM (
select Instance
	, database_name
	, cast(startDate as date) [Date]
	, sum((cast(replace([data_kb], 'KB','') as float) + cast(replace([index_size], 'KB','') as float))) Whole_Database_KB
from DBGrowthMetrics
where database_name = 'WJEC_SecureAssess'
	--and cast(startDate as date) in ('2018-05-14','2018-05-08','2018-04-30')
	group by Instance
		, database_name
		, cast(startDate as date)
) A
group by A.Instance, [Date]
order by 2