--Use master;

--DECLARE @DefaultLocation TABLE (
--     Value NVARCHAR(100)  
--     , Data NVARCHAR(100)
--) 

--INSERT @DefaultLocation
--EXEC  master.dbo.xp_instance_regread 
-- N'HKEY_LOCAL_MACHINE', N'Software\Microsoft\MSSQLServer\MSSQLServer',N'BackupDirectory'

--DECLARE @Location nvarchar(100) = 'https://btlr12prdhub.blob.core.windows.net/backupcontainer'
----DECLARE @Location nvarchar(100) = (SELECT Data from @DefaultLocation)

--SELECT N'BACKUP DATABASE ['+name+N'] TO URL = N''' +  @Location + '/' + name + '.bak'' WITH NAME = N'''+name+N'- Database Backup'', COPY_ONLY, NOFORMAT, NOINIT, SKIP, COMPRESSION, NOREWIND, NOUNLOAD, STATS = 10;'  
--FROM sys.databases 
--where database_id > 4
--ORDER BY name desc;
use master
RESTORE HEADERONLY
FROM URL = N'https://btlr12prdhub.blob.core.windows.net/backupcontainer/Windesheim_SurpassManagement.bak'



BACKUP DATABASE [Windesheim_SurpassManagement] TO URL = N'https://btlr12prdhub.blob.core.windows.net/backupcontainer/Windesheim_SurpassManagement.bak' WITH NAME = N'Windesheim_SurpassManagement- Database Backup', COPY_ONLY, NOFORMAT, NOINIT, SKIP, COMPRESSION, NOREWIND, NOUNLOAD, STATS = 10;
BACKUP DATABASE [Windesheim_SurpassDataWarehouse] TO URL = N'https://btlr12prdhub.blob.core.windows.net/backupcontainer/Windesheim_SurpassDataWarehouse.bak' WITH NAME = N'Windesheim_SurpassDataWarehouse- Database Backup', COPY_ONLY, NOFORMAT, NOINIT, SKIP, COMPRESSION, NOREWIND, NOUNLOAD, STATS = 10;
BACKUP DATABASE [Windesheim_SecureAssess] TO URL = N'https://btlr12prdhub.blob.core.windows.net/backupcontainer/Windesheim_SecureAssess.bak' WITH NAME = N'Windesheim_SecureAssess- Database Backup', COPY_ONLY, NOFORMAT, NOINIT, SKIP, COMPRESSION, NOREWIND, NOUNLOAD, STATS = 10;
BACKUP DATABASE [Windesheim_ItemBank] TO URL = N'https://btlr12prdhub.blob.core.windows.net/backupcontainer/Windesheim_ItemBank.bak' WITH NAME = N'Windesheim_ItemBank- Database Backup', COPY_ONLY, NOFORMAT, NOINIT, SKIP, COMPRESSION, NOREWIND, NOUNLOAD, STATS = 10;
BACKUP DATABASE [Windesheim_ContentAuthor] TO URL = N'https://btlr12prdhub.blob.core.windows.net/backupcontainer/Windesheim_ContentAuthor.bak' WITH NAME = N'Windesheim_ContentAuthor- Database Backup', COPY_ONLY, NOFORMAT, NOINIT, SKIP, COMPRESSION, NOREWIND, NOUNLOAD, STATS = 10;
BACKUP DATABASE [Windesheim_AnalyticsManagement] TO URL = N'https://btlr12prdhub.blob.core.windows.net/backupcontainer/Windesheim_AnalyticsManagement.bak' WITH NAME = N'Windesheim_AnalyticsManagement- Database Backup', COPY_ONLY, NOFORMAT, NOINIT, SKIP, COMPRESSION, NOREWIND, NOUNLOAD, STATS = 10;

