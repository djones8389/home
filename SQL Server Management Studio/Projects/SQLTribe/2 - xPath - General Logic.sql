USE [PRV_SADB];

/*Only takes the top value,  the first node it comes to*/

SELECT 
 	examsessionid
 	, itemid
	, markerResponseData.value('data(/entries/entry/assignedMark)[1]','tinyint') [assignedMark]  
	, markerResponseData
FROM dbo.ExamSessionItemResponseTable
where examSessionid = 705877
	and itemID = '365P1335';
	--and markerResponseData.value('data(/entries/entry[last()]/assignedMark)[1]','tinyint') = 0;


/*......Unless you specify "Last"*/

SELECT 
 	examsessionid
 	, itemid
	, markerResponseData.value('data(/entries/entry[last()]/assignedMark)[1]','tinyint')  [assignedMark] 	  
	, markerResponseData
FROM dbo.ExamSessionItemResponseTable
where examSessionid = 1728
	and itemID = '3624P3720'
	and markerResponseData.value('data(/entries/entry[last()]/assignedMark)[1]','tinyint')  = '0'
	--and markerResponseData.value('data(/entries/entry[last()]/assignedMark)[1]','tinyint') = 4;
	

/*This gives you both values for ALL assignedMark nodes*/

SELECT
 	examsessionid
 	, itemid
	, markerResponseData.query('data(entries/entry/assignedMark)')  [assignedMark] 
	, markerResponseData
FROM dbo.ExamSessionItemResponseTable
where examSessionid = 104606
	and itemID = '3624P3720'
	--and markerResponseData.query('data(entries/entry/assignedMark)') = '0'
	AND  CONVERT(nvarchar(100),markerResponseData.query('data(entries/entry/assignedMark)')) = '0';
	


select top 10 ID
	, StructureXML
	, StructureXML.query('data(assessmentDetails/assessmentName)[1]')
	, StructureXML.value('(assessmentDetails/assessmentName)[1]','nvarchar(100)') as assessmentName
from ExamSessionTable
where id = 104606
	and StructureXML.query('data(assessmentDetails/assessmentName)[1]')= 'Pilot Version 1'




SELECT ExamSessionID
	, ItemID
	, MarkerResponseData.value('count(entries/entry)','int')
FROM ExamSessionItemResponseTable
where MarkerResponseData.exist('entries/entry[2]') = 1
	--where MarkerResponseData.value('count(entries/entry)','int') > 1