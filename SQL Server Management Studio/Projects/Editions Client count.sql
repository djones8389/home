select b.a
, count(b.a)
from (
select
	 substring(d.name,0,charindex('_',d.name)) a
from sys.databases d
where state_desc = 'online'

)  b
group by  b.a
having count(*) > 4
order by 1