DECLARE @OldStatus nvarchar(100) = 'Published';
DECLARE @NewStatus nvarchar(100) = 'Published (In live use)';
DECLARE @PRINTString nvarchar(max) = '';

DECLARE @myTable TABLE (
	ProjectID smallint
	, ProjectName nvarchar(150)
	, CurrentStatus tinyint
	, NewStatus tinyint
)			

INSERT @myTable	
select ID
	, Name
	, a.b.value('(.)[1]','nvarchar(100)') as ProjectPublish_StatusLevelTo
	, c.d.value('@id[1]','nvarchar(100)') as DesiredProject_PublishStatus	
from ProjectListTable as PLT

CROSS APPLY PublishSettingsXml.nodes('PublishConfig/StatusLevelTo') a(b)
CROSS APPLY ProjectDefaultXml.nodes('Defaults/StatusList/Status') c(d)

where  IsAppointeeManagement = 1
		and Name like '%National%'
		and c.d.value('(.)[1]','nvarchar(100)') = @NewStatus
		and a.b.value('(.)[1]','nvarchar(100)') != c.d.value('@id[1]','nvarchar(100)');
		

SELECT @PRINTString += CHAR(13) + '
UPDATE ProjectListTable
set PublishSettingsXml.modify(''replace value of(/PublishConfig/StatusLevelTo/text())[1] with "'+ cast(NewStatus as nvarchar(MAX)) +'"'')
where ID = ' + cast(ProjectID as nvarchar(MAX)) +  ';
'
from @myTable 

PRINT(@PRINTString)

SELECT A.*, OutputMedium FROM @myTable A inner join ProjectListTable on ProjectListTable.ID = A.ProjectID
	order by 2



--UPDATE ProjectListTable
--set PublishSettingsXml.modify('replace value of(/PublishConfig/StatusLevelTo/text())[1] with "3"')
--where ID = 965;

--UPDATE ProjectListTable
----set PublishSettingsXml.modify('replace value of(/PublishConfig/StatusLevelTo/text())[1] with "4"') --WORKS
--set PublishSettingsXml.modify('replace value of(/PublishConfig/StatusLevelTo/text())[1] with "3"')
--where ID = 959
--SELECT PublishSettingsXml, PublishSettingsXml.query('(/PublishConfig/StatusLevelTo[1]/text())')
--FROM ProjectListTable 
--where ID = 959



--set ResultData.modify('replace value of(/exam/section/item[@id = sql:variable("@ItemID")]/@userMark)[1] with sql:variable("@totalMarkItem")')




--select ID
--	, Name
--	, a.b.value('(.)[1]','nvarchar(100)') as ProjectPublish_StatusLevelTo
--	, c.d.value('@id[1]','nvarchar(100)') as CurrentProject_PublishStatus	
--from ProjectListTable as PLT

--CROSS APPLY PublishSettingsXml.nodes('PublishConfig/StatusLevelTo') a(b)
--CROSS APPLY ProjectDefaultXml.nodes('Defaults/StatusList/Status') c(d)

--where ID IN (959,960,961,962,963,964,965,967,975,980,992,1002,1005,1007,1008,1009,1010,1011,1012,1014,1015,1018,1019,1026,1027,1029)
--		and a.b.value('(.)[1]','nvarchar(100)') = c.d.value('@id[1]','nvarchar(100)')
--		and c.d.value('(.)[1]','nvarchar(100)') = @OldStatus;

