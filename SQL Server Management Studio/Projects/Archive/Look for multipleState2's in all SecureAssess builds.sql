--Look for multipleState2's in all SecureAssess builds--

exec sp_MSforeachdb N'SET QUOTED_IDENTIFIER ON;
use [?];
if(''?'' LIKE N''%SecureAssess'')
	  SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
      select N''?'', count(ID) from dbo.Warehouse_ExamSessionTable
       WHERE WAREHOUSE_ExamSessionTable.ExamStateChangeAuditXml.value(''count(exam/stateChange[newStateID/text()=2])'', ''int'') > 1;
'
