use master;

DECLARE @NewLogLocation nvarchar(100) = 'D:\SQL SA DB\MSSQL\DATA\LOG'

DECLARE @AlterLocation nvarchar(MAX) = '';
DECLARE @moveLog nvarchar(MAX) = '';
DECLARE @setOnline nvarchar(MAX) = '';
DECLARE @TakeOffline nvarchar(MAX) = '';

DECLARE @Tables TABLE(DBName nvarchar(100), LogFileName nvarchar(100), OriginalLocation nvarchar(250))
INSERT @Tables
SELECT D.Name, F.Name, F.physical_name
FROM sys.master_files as F
inner join sys.databases as D
on D.database_id = F.database_id 
where D.name = 'SA_Trunk'
	and type_desc = 'LOG'

SELECT @TakeOffline += char(13) + 
	'ALTER DATABASE [' + DBName + '] SET OFFLINE;
'
FROM @Tables 

SELECT @AlterLocation += char(13) + 
	'ALTER DATABASE [' + DBName + '] MODIFY FILE (NAME = ' + LogFileName  + ', Filename=''' + @NewLogLocation + '\'+ LogFileName + '.ldf'');
'
FROM @Tables


SELECT @setOnline += char(13) +  
	'use master;  ALTER DATABASE [' + DBName + '] SET ONLINE;'
FROM @Tables 


SELECT @moveLog += char(13) +  
	 'EXECUTE xp_cmdshell ''copy "'+  OriginalLocation + '" "'+ @NewLogLocation + '\' + LogFileName + '.ldf' + '"'''
FROM @Tables 

PRINT(@TakeOffline);
PRINT(@AlterLocation);
/*PRINT(@moveLog);*/
PRINT(@setOnline);


/*
D:\SQL SA DB\MSSQL\DATA\SA_Trunk.mdf
D:\SQL SA DB\MSSQL\DATA\SA_Trunk_log.ldf
*/

--ALTER DATABASE [SA_Trunk] SET OFFLINE;
--ALTER DATABASE [SA_Trunk] MODIFY FILE (NAME = SA_Trunk_log, Filename='D:\SQL SA DB\MSSQL\DATA\LOG\SA_Trunk_log.ldf');
--use master;  ALTER DATABASE [SA_Trunk] SET ONLINE;

DECLARE @Location nvarchar(100) = 'I:\Backup'

SELECT N'BACKUP DATABASE ['+name+N'] TO DISK = N''' +  @Location + '\' + 'NCFE.ALL' + CONVERT(VARCHAR(10), GETDATE(), 102) + '.bak''WITH NAME = N'''+name+N'- Database Backup'', COPY_ONLY, COMPRESSION, NOFORMAT, NOINIT, SKIP, COMPRESSION, NOREWIND, NOUNLOAD, STATS = 10;'  
FROM sys.databases 
where name like 'UAT_BC2_SecureMarker%'

BACKUP DATABASE [UAT_BC2_SecureMarker_READONLY] 
	TO DISK = N'I:\Backup\UAT_BC2_SecureMarker_READONLY.2015.09.25.bak' 
		WITH NAME = N'UAT_BC2_SecureMarker_READONLY- Database Backup', COPY_ONLY, COMPRESSION, NOFORMAT, NOINIT, SKIP, COMPRESSION, NOREWIND, NOUNLOAD, STATS = 10;

--ALTER DATABASE [UAT_BC2_SecureMarker_READONLY] SET OFFLINE;