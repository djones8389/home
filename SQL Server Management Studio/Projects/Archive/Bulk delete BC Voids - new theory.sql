IF OBJECT_ID('tempdb..#AffectedExams') IS NOT NULL DROP TABLE #AffectedExams;

SELECT  WEST.ID as ExamSessionID, WSCET.ID as ScheduledID, '' as BeenDeleted
INTO #AffectedExams
FROM WAREHOUSE_ExamSessionTable as WEST (NOLOCK)

  Inner Join WAREHOUSE_ScheduledExamsTable as WSCET (NOLOCK)
  on WSCET.ID = WEST.WAREHOUSEScheduledExamID
  
 where PreviousExamState = 10 
	and warehouseTime <= DATEADD (month , -6 , GETDATE())
	and reMarkStatus != 2		--Check this
  order by warehouseTime asc;



IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[temp_WAREHOUSE_ExamSessionAvailableItemsTable]') AND type in (N'U'))
DROP TABLE [dbo].[temp_WAREHOUSE_ExamSessionAvailableItemsTable];


CREATE TABLE [dbo].[temp_WAREHOUSE_ExamSessionAvailableItemsTable](
	[ID] [int] NOT NULL,
	[ExamSessionID] [int] NOT NULL,
	[ItemID] [nvarchar](50) NOT NULL,
	[ItemVersion] [int] NOT NULL,
	[ItemXML] [xml] NOT NULL
);


--CREATE NONCLUSTERED INDEX [temp_FK_ExamSessionID] ON [dbo].[temp_WAREHOUSE_ExamSessionAvailableItemsTable] 
--(
--	[ExamSessionID] ASC
--)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
--GO


INSERT INTO  [dbo].[temp_WAREHOUSE_ExamSessionAvailableItemsTable]
select * FROM [WAREHOUSE_ExamSessionAvailableItemsTable]
where [WAREHOUSE_ExamSessionAvailableItemsTable].ExamSessionID NOT in (
		select ExamSessionID FROM #AffectedExams
		)


TRUNCATE TABLE [dbo].[WAREHOUSE_ExamSessionAvailableItemsTable]


SET IDENTITY_INSERT [dbo].WAREHOUSE_ExamSessionAvailableItemsTable ON

INSERT INTO  [dbo].[WAREHOUSE_ExamSessionAvailableItemsTable] (ID, ExamSessionID, ItemID, ItemVersion, ItemXML)
select * FROM [dbo].[temp_WAREHOUSE_ExamSessionAvailableItemsTable]

SET IDENTITY_INSERT [dbo].WAREHOUSE_ExamSessionAvailableItemsTable OFF


