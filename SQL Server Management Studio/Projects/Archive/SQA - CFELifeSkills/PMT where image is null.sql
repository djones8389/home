IF OBJECT_ID ('tempdb..#SharedLibStructure') IS NOT NULL DROP TABLE #ProjectStructure;

DECLARE @MyItems TABLE (id int, name nvarchar(200), projectid int, SLProjectID int)
insert into @MyItems(ID, name, projectid, SLProjectID)

select  Items.id
	 ,  A.id			
	 , A.name
	 , A.Projectid

from (

select id, name, projectid				--Find where image is null--
from ProjectManifestTable as PMT with (NOLOCK)
where image is null
	and location not like '%background%'
	--and projectid = 942
) A

--LEFT join   --Inner = 108 Items. Left = 319 Items.

--(
--select b.projectid						--Where exists in SLib
--	, a.b.value ('@id', 'int') as  ItemID
--from
--(
--	select projectid
--		, cast(Structurexml as xml) as Structurexml
--	from SharedLibraryTable as SLT with (NOLOCK)
--	where projectid = 942
--	) B
--	cross apply Structurexml.nodes('//item') a(b)
--) B 

--on A.ID = b.Itemid
--	and a.projectid = b.projectid

		
LEFT JOIN								--Find on any pages
(
      SELECT ID, ali
      FROM ItemCustomQuestionTable with (NOLOCK)
      UNION
      SELECT ID, ali
      FROM ItemGraphicTable with (NOLOCK)
      UNION
      SELECT ID, ali
      FROM ItemVideoTable with (NOLOCK)
      UNION
      SELECT ID, ali
      FROM ItemHotSpotTable with (NOLOCK)
) Items
ON 
      Items.ID LIKE CONVERT(NVARCHAR(10), A.ProjectId) + 'P%' 
      AND 
      (
            CONVERT(NVARCHAR(50), A.ID) = ali
            OR
            SUBSTRING(A.name, 0, LEN(A.name) - 3) = ali
      )

	  AND SUBSTRING(Items.ID, 0, CHARINDEX('P', Items.ID)) = A.ProjectID

--WHERE  Items.id IS NULL				--Where image is not on any pages
--	and A.ProjectID = 942;
	