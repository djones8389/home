USE UAT_BC_SecureAssess

SELECT WAREHOUSE_ExamSessionTable.ID, KeyCode -- TOP 1000 ItemResponseData.query('p/s/c/i/data'), *
FROM WAREHOUSE_ExamSessionItemResponseTable

inner join WAREHOUSE_ExamSessionTable 
on WAREHOUSE_ExamSessionTable.ID = WAREHOUSE_ExamSessionItemResponseTable.WAREHOUSEExamSessionID

LEFT JOIN WAREHOUSE_ExamSessionDocumentTable
ON WAREHOUSE_ExamSessionDocumentTable.warehouseExamSessionID = WAREHOUSE_ExamSessionItemResponseTable.WAREHOUSEExamSessionID
      AND
      SUBSTRING(WAREHOUSE_ExamSessionDocumentTable.itemId, 0, CHARINDEX('S', WAREHOUSE_ExamSessionDocumentTable.itemId)) = WAREHOUSE_ExamSessionItemResponseTable.ItemID
--WHERE ItemResponseData.exist('p/s/c/i/data') = 1 AND WAREHOUSE_ExamSessionDocumentTable.ID IS NULL
where WAREHOUSE_ExamSessionTable.ID in (11653,17414,17414,17414,21532,21532,53336,78663,81784,91131,93389,95256,99753,123556,135972,135972,138568,147193,190304,239335,239335,239335,239335,239806,239806,239806,239806,493256,493256,493256,493256,495482,582754)
	and  ItemResponseData.exist('p/s/c/i/data') = 1 AND WAREHOUSE_ExamSessionDocumentTable.ID IS NULL