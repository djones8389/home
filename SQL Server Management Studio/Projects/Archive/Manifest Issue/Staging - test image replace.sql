select ROW_NUMBER() over (order by name) rownum
				  , ID
		          , ProjectId	   
		          , name       
                  , Image
     into #TempTable
     from SQA_CPProjectAdmin..ProjectManifestTable

where ID IN ('8','10','11','12','14','16','18','20','27','66','136','181','238','240','614','618','1204')


delete
from #TempTable
where rownum not in (   
      
      select rownum
      from 
      #TempTable as R
      inner join
            (
            select  name, MAX(rownum) as RN
                  from #TempTable               
                  
                  group by name
                  
            ) X
      on r.rownum = X.RN
);

/*
update UAT_SQA_ContentProducer..ProjectManifestTable
set image = (select Image from #TempTable where #TempTable.name = a.name and #TempTable.ID = a.ID and #TempTable.ProjectId = a.ProjectId)
from UAT_SQA_ContentProducer..ProjectManifestTable a

inner join #TempTable b
	on a.projectid = b.projectid and a.name = b.name and a.id = b.id
*/

drop table #TempTable;
