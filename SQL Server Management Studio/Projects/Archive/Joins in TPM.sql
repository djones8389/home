select 
	SurpassQualificationId, SurpassExamId, PE.PackageId, SurpassExamRef
		, PE.Name, P.Name, PE.PackageExamId, PE.SurpassExamRef
	

 from ScheduledPackageCandidateExams as SPCE

	inner join ScheduledPackageCandidates as SPC
	on SPC.ScheduledPackageCandidateId =  SPCE.Candidate_ScheduledPackageCandidateId
	
	inner join PackageExams as PE
	on PE.PackageExamId = SPCE.PackageExamId

	inner join Packages as P
	on P.PackageId = PE.PackageId
	
	WHERE ScheduledExamRef  = '4EG9NP99'
	
	ORDER BY SPCE.DateCompleted DESC;
	
	
	
select 
	distinct SurpassQualificationId, SurpassExamId, PE.PackageId, PE.Name, SurpassExamRef
		, P.Name, PE.PackageExamId, PE.SurpassExamRef, SurpassCandidateRef

 from ScheduledPackageCandidateExams as SPCE

	inner join ScheduledPackageCandidates as SPC
	on SPC.ScheduledPackageCandidateId =  SPCE.Candidate_ScheduledPackageCandidateId
	
	inner join PackageExams as PE
	on PE.PackageExamId = SPCE.PackageExamId

	inner join Packages as P
	on P.PackageId = PE.PackageId
	
	where SurpassExamId in ('42','43','44','45','59','162','181','182','183','184','256','257','258','271','296','303','330','379')
		or SurpassQualificationId in (119,141,147,167,169,172,184)
		order by SurpassQualificationId, SurpassExamId