use OCR_SecureAssess_11_2

SELECT 

--WEST.ID
--WEST.ExamSessionID
--, WEST.KeyCode
--, WUT.Forename
--, WUT.Surname
--, WUT.CandidateRef
--, WCT.CentreName
--, WSCET.examName
--, WEST.completionDate
--, WESTS.ExternalReference
--, west.ExamStateChangeAuditXml
--, wests.ExternalReference
--, wests.examName
--, CreatedDateTime
--, west.ExamStateChangeAuditXml.value('(/exam/stateChange/changeDate)[1]', 'datetime') as state1
--, west.ExamStateChangeAuditXml.value('(/exam/stateChange/changeDate)[2]', 'datetime') as state2
--, west.ExamStateChangeAuditXml.value('(/exam/stateChange/changeDate)[3]', 'datetime') as state3
--, west.ExamStateChangeAuditXml.value('(/exam/stateChange/changeDate)[4]', 'datetime') as state4
--, west.ExamStateChangeAuditXml.value('(/exam/stateChange/changeDate)[5]', 'datetime') as state6
--, west.ExamStateChangeAuditXml.value('(/exam/stateChange/changeDate)[6]', 'datetime') as state9
--, west.ExamStateChangeAuditXml.value('(/exam/stateChange/changeDate)[7]', 'datetime') as state11
--, west.ExamStateChangeAuditXml.value('(/exam/stateChange/changeDate)[8]', 'datetime') as state12

wests.*

 FROM WAREHOUSE_ExamSessionTable as WEST

  Inner Join WAREHOUSE_ScheduledExamsTable as WSCET
  on WSCET.ID = WEST.WAREHOUSEScheduledExamID
  
  Inner Join WAREHOUSE_UserTable as WUT
  on WUT.ID = WEST.WAREHOUSEUserID

  Inner Join WAREHOUSE_CentreTable as WCT
  on WCT.ID = WSCET.WAREHOUSECentreID

  Inner Join WAREHOUSE_ExamSessionTable_Shreded as WESTS
  on WESTS.examSessionId = WEST.ID

where WUT.CandidateRef = '2617' 
	and wests.ExternalReference = 'NQEL_Annual_2014_a_04439_301_4'



SELECT  U.UserID
		,u.CandidateRef
        --,S.ExamID
        ,S.ExamVersionID
        --,E.ExamSessionID
        --, AGT.AllowVersionRecycling
        --, AGT.MinResitTimeInDays
FROM  WAREHOUSE_ExamSessionTable E
	INNER JOIN WAREHOUSE_ScheduledExamsTable S
	ON E.WAREHOUSEScheduledExamID = S.ID
	INNER JOIN WAREHOUSE_UserTable U
	ON E.WAREHOUSEUserID = U.ID
	Inner Join [OCR_ItemBank_11.2]..AssessmentTable as AT
	on AT.ID = examVersionId
	Inner join [OCR_ItemBank_11.2]..AssessmentGroupTable as AGT
	on AGT.ID = AT.AssessmentGroupID
	
WHERE E.ExamSessionID  IN
                (
                SELECT  ExamSessionID
                FROM  UserExamHistoryVersionTrackerTable
                )
AND	E.ExamStateChangeAuditXML.exist('/exam/stateChange[newStateID = 6]') = 1	--Where exam has definitely been presented to a candidate--
and AGT.AllowVersionRecycling = 0	--Where IB does not allow duplicates to be sat--
and E.warehouseTime > '20 Feb 2014' --The date on which I started the job running again--
and U.CandidateRef <> ''			--Omit Users--
	group by userID, S.ExamVersionID, CandidateRef
		having COUNT(*) > 1 
		
order by UserId



select * from WAREHOUSE_ExamSessionTable as west
	
	inner join WAREHOUSE_ExamSessionTable_Shreded as wests
	on wests.examSessionId = west.ID

where examVersionId in (1802,1307,2197)
	and candidateRef in ('0dab91566c8a5459dafb7ac1b86263a53','2617','AUTO7f085ba3924a45c3b924ceb1667e792d')
	order by WAREHOUSEUserID



---@ignoreVersionHistory---
---@recycling---

use [OCR_ItemBank_11.2]
select MinResitTimeInDays, AllowVersionRecycling, * from [OCR_ItemBank_11.2]..AssessmentGroupTable 
	--where Name = '301 Personal Responsibilities Level 3'
where AllowVersionRecycling = 0
--	 Ref = 'NQEL_Annual_2014_a_04439_301_4'
	
use [OCR_ItemBank_11.2]
SELECT c.name AS ColName, t.name AS TableName
FROM sys.columns c
    JOIN sys.tables t ON c.object_id = t.object_id
WHERE c.name LIKE '%resit%'
