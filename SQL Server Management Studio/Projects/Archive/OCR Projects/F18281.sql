--select Forename, Surname,	CandidateRef
--	from UserTable
--	where Forename = 'Fern'
	
--select Forename, Surname,	CandidateRef
--	from WAREHOUSE_UserTable
--	where Forename = 'Fern'
--		order by CandidateRef
		
		
		--1402--
SELECT EST.ID, EST.KeyCode, EST.examState, UT.Forename, UT.Surname, CT.CentreName, SCET.examName, examVersionRef
  FROM ExamSessionTable as EST

  Inner Join ScheduledExamsTable as SCET
  on SCET.ID = EST.ScheduledExamID
  
  Inner Join UserTable as UT
  on UT.ID = EST.UserID

  Inner Join CentreTable as CT
  on CT.ID = SCET.CentreID
where ut.CandidateRef = '1402'
	order by examVersionRef;

SELECT WEST.ID, WEST.KeyCode, WUT.Forename, WUT.Surname, WUT.CandidateRef, WCT.CentreName, WSCET.examName, WEST.completionDate
	, WESTS.ExternalReference
FROM WAREHOUSE_ExamSessionTable as WEST

  Inner Join WAREHOUSE_ScheduledExamsTable as WSCET
  on WSCET.ID = WEST.WAREHOUSEScheduledExamID
  
  Inner Join WAREHOUSE_UserTable as WUT
  on WUT.ID = WEST.WAREHOUSEUserID

  Inner Join WAREHOUSE_CentreTable as WCT
  on WCT.ID = WSCET.WAREHOUSECentreID

  Inner Join WAREHOUSE_ExamSessionTable_Shreded as WESTS
  on WESTS.examSessionId = WEST.ID

where wut.CandidateRef = '1402'
	order by wests.ExternalReference;