select * from ReMarkExamSessionTable

select * from WAREHOUSE_ExamSessionTable where ID = 62797

update WAREHOUSE_ExamSessionTable_Shreded
set StructureXML = '<assessmentDetails>
  <assessmentID>2199</assessmentID>
  <qualificationID>132</qualificationID>
  <qualificationName>OCR Functional Skills in English Reading Level 2</qualificationName>
  <qualificationReference>FSEnglishReadingL2</qualificationReference>
  <assessmentGroupName>Level 2 Functional Skills English - Reading Task</assessmentGroupName>
  <assessmentGroupID>66</assessmentGroupID>
  <assessmentGroupReference>09499/4</assessmentGroupReference>
  <assessmentName>Level 2 Functional Skills English Reading - ONDEMAND - BR27</assessmentName>
  <validFromDate>13 Aug 2012</validFromDate>
  <expiryDate>14 Aug 2022</expiryDate>
  <startTime>00:00:00</startTime>
  <endTime>23:59:59</endTime>
  <duration>55</duration>
  <defaultDuration>55</defaultDuration>
  <scheduledDuration>
    <value>55</value>
    <reason></reason>
  </scheduledDuration>
  <sRBonus>0</sRBonus>
  <sRBonusMaximum>14</sRBonusMaximum>
  <externalReference>FSUM_ONDEMAND_0000_m_09499_4_BR27</externalReference>
  <passLevelValue>12</passLevelValue>
  <passLevelType>0</passLevelType>
  <status>2</status>
  <testFeedbackType>
    <passFail>0</passFail>
    <percentageMark>0</percentageMark>
    <allowSummaryFeedback>0</allowSummaryFeedback>
    <summaryFeedbackType>1</summaryFeedbackType>
    <itemSummary>0</itemSummary>
    <itemReview>0</itemReview>
    <itemReviewCorrectAnswer>0</itemReviewCorrectAnswer>
    <itemFeedback>0</itemFeedback>
    <printableSummary>0</printableSummary>
    <candidateDetails>0</candidateDetails>
    <feedbackByReference>0</feedbackByReference>
    <allowFeedbackDuringExam>0</allowFeedbackDuringExam>
    <allowFeedbackDuringSummary>0</allowFeedbackDuringSummary>
  </testFeedbackType>
  <itemFeedback>0</itemFeedback>
  <allowCalculator>0</allowCalculator>
  <lastInUseDate>21 Feb 2014 09:24:48</lastInUseDate>
  <completedScriptReview>0</completedScriptReview>
  <assessment currentSection="1" totalTime="0" currentTime="0">
    <intro id="0" name="Introduction" currentItem="">
      <item id="6002P6863" name="BR27_FS" totalMark="1" version="49" markingType="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="0" />
    </intro>
    <outro id="0" name="Finish" currentItem="" />
    <tools id="0" name="Tools" currentItem="">
      <item id="6002P6872" name="BR27_Doc1" toolName="BR27_Doc1" totalMark="1" version="9" markingType="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="0" />
      <item id="6002P6873" name="BR27_Doc2" toolName="BR27_Doc2" totalMark="1" version="9" markingType="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="0" />
      <item id="6002P6874" name="BR27_Doc3" toolName="BR27_Doc3" totalMark="1" version="5" markingType="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="0" />
    </tools>
    <section id="1" name="Section 1" passLevelValue="0" passLevelType="1" itemsToMark="0" currentItem="6050P6783" fixed="1">
      <item id="6050P6776" name="BR27_Info1" totalMark="0" version="69" markingType="0" userMark="-1" markerUserMark="0" userAttempted="0" viewingTime="37265" LO="Information page only (no marks)" unit="2" />
      <item id="6050P6777" reMarked="1" name="BR27_01" totalMark="3" version="69" markingType="1" userMark="0" markerUserMark="3" userAttempted="1" viewingTime="796237" unit="2" quT="11" MarkedMetadata_4="3" />
      <item id="6050P6778" reMarked="1" name="BR27_02" totalMark="3" version="72" markingType="1" userMark="0" markerUserMark="1" userAttempted="1" viewingTime="405532" unit="2" quT="11" MarkedMetadata_4="3" />
      <item id="6050P6779" reMarked="1" name="BR27_03" totalMark="3" version="72" markingType="1" userMark="0" markerUserMark="2" userAttempted="1" viewingTime="198324" unit="2" quT="11" MarkedMetadata_5="3" />
      <item id="6050P6780" reMarked="1" name="BR27_04" totalMark="4" version="70" markingType="1" userMark="0" markerUserMark="3" userAttempted="1" viewingTime="207812" unit="2" quT="11" MarkedMetadata_6="4" />
      <item id="6050P6781" reMarked="1" name="BR27_05" totalMark="4" version="69" markingType="1" userMark="0" markerUserMark="3" userAttempted="1" viewingTime="603782" unit="2" quT="11" MarkedMetadata_5="4" />
      <item id="6050P6782" reMarked="1" name="BR27_06" totalMark="4" version="71" markingType="1" userMark="0" markerUserMark="3" userAttempted="1" viewingTime="495867" unit="2" quT="11" MarkedMetadata_6="4" />
      <item id="6050P6783" reMarked="1" name="BR27_07" totalMark="4" version="71" markingType="1" userMark="0" markerUserMark="4" userAttempted="1" viewingTime="483336" unit="2" quT="11" MarkedMetadata_6="4" />
    </section>
  </assessment>
  <isValid>1</isValid>
  <isPrintable>0</isPrintable>
  <qualityReview>0</qualityReview>
  <numberOfGenerations>1</numberOfGenerations>
  <requiresInvigilation>0</requiresInvigilation>
  <advanceContentDownloadTimespan>24</advanceContentDownloadTimespan>
  <automaticVerification>0</automaticVerification>
  <offlineMode>2</offlineMode>
  <requiresValidation>0</requiresValidation>
  <requiresSecureClient>1</requiresSecureClient>
  <examType>0</examType>
  <advanceDownload>0</advanceDownload>
  <gradeBoundaryData>
    <gradeBoundaries passLevelType="0" adjustedGradeBoundaries="0">
      <grade modifier="lt" value="12" description="Fail" />
      <grade modifier="gt" value="12" description="Fail n" />
      <grade modifier="gt" value="14" description="Pass" userCreated="1" />
    </gradeBoundaries>
  </gradeBoundaryData>
  <autoViewExam>0</autoViewExam>
  <autoProgressItems>0</autoProgressItems>
  <confirmationText>
    <confirmationText>By ticking this box you confirm that the above details are correct and you will adhere to OCR''s code of conduct.</confirmationText>
  </confirmationText>
  <requiresConfirmationCheck>1</requiresConfirmationCheck>
  <allowCloseWithoutSubmit>0</allowCloseWithoutSubmit>
  <useSecureMarker>0</useSecureMarker>
  <annotationVersion>2</annotationVersion>
  <allowPackagingDelivery>1</allowPackagingDelivery>
  <scoreBoundaryData>
    <scoreBoundaries showScoreBoundaryColumn="0">
      <score modifier="lt" value="40" description="Not met" higherBoundarySet="1" />
      <score modifier="lt" value="40" description="Not met" higherBoundarySet="0" />
      <score modifier="gt" value="40" description="Close to met" higherBoundarySet="1" />
      <score modifier="gt" value="40" description="Met" higherBoundarySet="0" />
      <score modifier="gt" value="45" description="Met" higherBoundarySet="1" />
      <score modifier="gt" value="80" description="Exceeded" higherBoundarySet="1" />
    </scoreBoundaries>
  </scoreBoundaryData>
  <showCandidateReportResultColumn>0</showCandidateReportResultColumn>
  <showCandidateReportPercentageColumn>1</showCandidateReportPercentageColumn>
  <certifiedAccessible>0</certifiedAccessible>
  <markingProgressVisible>1</markingProgressVisible>
  <markingProgressMode>0</markingProgressMode>
  <secureClientOperationMode>1</secureClientOperationMode>
  <examDeliverySystemStyleType>delivery</examDeliverySystemStyleType>
  <markingAutoVoidPeriod>365</markingAutoVoidPeriod>
  <showPageRequiresScrollingAlert>1</showPageRequiresScrollingAlert>
  <project ID="4209" />
  <project ID="6002" />
  <project ID="6050">
    <value display="1R1 Candidate has identified the main points, ideas and how they are presented in a variety of texts">0</value>
    <value display="1R2 Candidate has read and understood texts in detail">1</value>
    <value display="1R3 Candidate has utilised information contained in texts">2</value>
    <value display="1R4 Candidate has identified suitable responses to texts">3</value>
    <value display="2R1 Candidate has obtained and utilised relevant information and identified purposes of texts">4</value>
    <value display="2R2 Candidate has read and summarised info, analysed texts in rel to needs and considered suit resp">5</value>
    <value display="2R3 Candidate has commented on how meaning is conveyed, detected views, implic. meaning and/or bias ">6</value>
  </project>
</assessmentDetails>'

, ResultData = '<exam passMark="12" passType="0" originalPassMark="12" originalPassType="0" totalMark="25" userMark="19" userPercentage="76.000" passValue="0" totalTimeSpent="3228155" closeBoundaryType="0" closeBoundaryValue="0" originalPassValue="0" grade="Pass" originalGrade="Pass">
  <gradeBoundaryData>
    <gradeBoundaries passLevelType="0" adjustedGradeBoundaries="0">
      <grade modifier="lt" value="12" description="Fail" />
      <grade modifier="gt" value="12" description="Fail n" />
      <grade modifier="gt" value="14" description="Pass" userCreated="1" />
    </gradeBoundaries>
  </gradeBoundaryData>
  <section id="1" passMark="0" passType="1" totalMark="25" userMark="19" userPercentage="76.000" passValue="1" totalTimeSpent="3228155" itemsToMark="0">
    <item id="6050P6776" name="BR27_Info1" totalMark="0" userMark="-1" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="37265" userAttempted="0" version="69" />
    <item id="6050P6777" name="BR27_01" totalMark="3" userMark="3" actualUserMark="0" markingType="1" markerUserMark="0" viewingTime="796237" userAttempted="1" version="69">
      <mark mark="3" learningOutcome="4" displayName="2R1 Candidate has obtained and utilised relevant information and identified purposes of texts" maxMark="3" />
    </item>
    <item id="6050P6778" name="BR27_02" totalMark="3" userMark="1" actualUserMark="0" markingType="1" markerUserMark="0" viewingTime="405532" userAttempted="1" version="72">
      <mark mark="1" learningOutcome="4" displayName="2R1 Candidate has obtained and utilised relevant information and identified purposes of texts" maxMark="3" />
    </item>
    <item id="6050P6779" name="BR27_03" totalMark="3" userMark="2" actualUserMark="0" markingType="1" markerUserMark="0" viewingTime="198324" userAttempted="1" version="72">
      <mark mark="2" learningOutcome="5" displayName="2R2 Candidate has read and summarised info, analysed texts in rel to needs and considered suit resp" maxMark="3" />
    </item>
    <item id="6050P6780" name="BR27_04" totalMark="4" userMark="3" actualUserMark="0" markingType="1" markerUserMark="0" viewingTime="207812" userAttempted="1" version="70">
      <mark mark="3" learningOutcome="6" displayName="2R3 Candidate has commented on how meaning is conveyed, detected views, implic. meaning and/or bias " maxMark="4" />
    </item>
    <item id="6050P6781" name="BR27_05" totalMark="4" userMark="3" actualUserMark="0" markingType="1" markerUserMark="0" viewingTime="603782" userAttempted="1" version="69">
      <mark mark="3" learningOutcome="5" displayName="2R2 Candidate has read and summarised info, analysed texts in rel to needs and considered suit resp" maxMark="4" />
    </item>
    <item id="6050P6782" name="BR27_06" totalMark="4" userMark="3" actualUserMark="0" markingType="1" markerUserMark="0" viewingTime="495867" userAttempted="1" version="71">
      <mark mark="3" learningOutcome="6" displayName="2R3 Candidate has commented on how meaning is conveyed, detected views, implic. meaning and/or bias " maxMark="4" />
    </item>
    <item id="6050P6783" name="BR27_07" totalMark="4" userMark="4" actualUserMark="0" markingType="1" markerUserMark="0" viewingTime="483336" userAttempted="1" version="71">
      <mark mark="4" learningOutcome="6" displayName="2R3 Candidate has commented on how meaning is conveyed, detected views, implic. meaning and/or bias " maxMark="4" />
    </item>
  </section>
</exam>'
--, ResultDataFull = '<assessmentDetails xmlns="">
--  <assessmentID>2199</assessmentID>
--  <qualificationID>132</qualificationID>
--  <qualificationName>OCR Functional Skills in English Reading Level 2</qualificationName>
--  <qualificationReference>FSEnglishReadingL2</qualificationReference>
--  <assessmentGroupName>Level 2 Functional Skills English - Reading Task</assessmentGroupName>
--  <assessmentGroupID>66</assessmentGroupID>
--  <assessmentGroupReference>09499/4</assessmentGroupReference>
--  <assessmentName>Level 2 Functional Skills English Reading - ONDEMAND - BR27</assessmentName>
--  <validFromDate>13 Aug 2012</validFromDate>
--  <expiryDate>14 Aug 2022</expiryDate>
--  <startTime>00:00:00</startTime>
--  <endTime>23:59:59</endTime>
--  <duration>55</duration>
--  <defaultDuration>55</defaultDuration>
--  <scheduledDuration>
--    <value>55</value>
--    <reason />
--  </scheduledDuration>
--  <sRBonus>0</sRBonus>
--  <sRBonusMaximum>14</sRBonusMaximum>
--  <externalReference>FSUM_ONDEMAND_0000_m_09499_4_BR27</externalReference>
--  <passLevelValue>12</passLevelValue>
--  <passLevelType>0</passLevelType>
--  <status>2</status>
--  <testFeedbackType>
--    <passFail>0</passFail>
--    <percentageMark>0</percentageMark>
--    <allowSummaryFeedback>0</allowSummaryFeedback>
--    <summaryFeedbackType>1</summaryFeedbackType>
--    <itemSummary>0</itemSummary>
--    <itemReview>0</itemReview>
--    <itemReviewCorrectAnswer>0</itemReviewCorrectAnswer>
--    <itemFeedback>0</itemFeedback>
--    <printableSummary>0</printableSummary>
--    <candidateDetails>0</candidateDetails>
--    <feedbackByReference>0</feedbackByReference>
--    <allowFeedbackDuringExam>0</allowFeedbackDuringExam>
--    <allowFeedbackDuringSummary>0</allowFeedbackDuringSummary>
--  </testFeedbackType>
--  <itemFeedback>0</itemFeedback>
--  <allowCalculator>0</allowCalculator>
--  <lastInUseDate>21 Feb 2014 09:24:48</lastInUseDate>
--  <completedScriptReview>0</completedScriptReview>
--  <assessment currentSection="1" totalTime="0" currentTime="0" originalPassMark="12" originalPassType="0" passMark="12" passType="0" totalMark="25" userMark="19" userPercentage="76.000" passValue="0" originalGrade="Fail">
--    <intro id="0" name="Introduction" currentItem="">
--      <item id="6002P6863" name="BR27_FS" totalMark="1" version="49" markingType="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="0" />
--    </intro>
--    <outro id="0" name="Finish" currentItem="" />
--    <tools id="0" name="Tools" currentItem="">
--      <item id="6002P6872" name="BR27_Doc1" toolName="BR27_Doc1" totalMark="1" version="9" markingType="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="0" />
--      <item id="6002P6873" name="BR27_Doc2" toolName="BR27_Doc2" totalMark="1" version="9" markingType="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="0" />
--      <item id="6002P6874" name="BR27_Doc3" toolName="BR27_Doc3" totalMark="1" version="5" markingType="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="0" />
--    </tools>
--    <section id="1" name="Section 1" passLevelValue="0" passLevelType="1" itemsToMark="0" currentItem="6050P6783" fixed="1" totalMark="25" userMark="19" userPercentage="76.000" passValue="1">
--      <item id="6050P6776" name="BR27_Info1" totalMark="0" version="69" markingType="0" userMark="-1" markerUserMark="" userAttempted="0" viewingTime="37265" LO="Information page only (no marks)" unit="2">
--        <p um="-1" cs="1" ua="0" id="6050P6776">
--          <s ua="0" um="-1" id="1">
--            <c typ="4" id="43">
--              <i cc="0" id="1" />
--            </c>
--            <c typ="4" id="44">
--              <i cc="0" id="1" />
--            </c>
--            <c typ="4" id="45">
--              <i cc="0" id="1" />
--            </c>
--          </s>
--        </p>
--      </item>
--      <item id="6050P6777" reMarked="1" name="BR27_01" totalMark="3" version="69" markingType="1" userMark="0" markerUserMark="3" userAttempted="1" viewingTime="796237" unit="2" quT="11" MarkedMetadata_4="3">
--        <p um="0" cs="1" ua="1" id="6050P6777">
--          <s ua="1" um="0" id="1">
--            <c typ="4" id="28">
--              <i cc="1" id="1" />
--            </c>
--            <c typ="4" id="29">
--              <i cc="2" id="1" />
--            </c>
--            <c wei="3" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="30">
--              <i ca="" id="1">leaflet for music festival giving information about how to get there #!#newspaper report about rip off prices#!#online forum views about the music festival</i>
--            </c>
--            <c typ="4" id="33">
--              <i cc="1" id="1" />
--            </c>
--          </s>
--        </p>
--      </item>
--      <item id="6050P6778" reMarked="1" name="BR27_02" totalMark="3" version="72" markingType="1" userMark="0" markerUserMark="1" userAttempted="1" viewingTime="405532" unit="2" quT="11" MarkedMetadata_4="3">
--        <p um="0" cs="1" ua="1" id="6050P6778">
--          <s ua="1" um="0" id="1">
--            <c typ="4" id="28">
--              <i cc="1" id="1" />
--            </c>
--            <c typ="4" id="29">
--              <i cc="0" id="1" />
--            </c>
--            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="30">
--              <i ca="" id="1">fuss about nothing</i>
--            </c>
--            <c typ="4" id="33">
--              <i cc="0" id="1" />
--            </c>
--            <c wei="2" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="35">
--              <i ca="" id="1">rich people, nothing else better to spend their money on #!##!#There are no home comforts, such as a warm shower, a bed or duvet</i>
--            </c>
--          </s>
--        </p>
--      </item>
--      <item id="6050P6779" reMarked="1" name="BR27_03" totalMark="3" version="72" markingType="1" userMark="0" markerUserMark="2" userAttempted="1" viewingTime="198324" unit="2" quT="11" MarkedMetadata_5="3">
--        <p um="0" cs="1" ua="1" id="6050P6779">
--          <s ua="1" um="0" id="1">
--            <c typ="4" id="28">
--              <i cc="0" id="1" />
--            </c>
--            <c typ="4" id="29">
--              <i cc="1" id="1" />
--            </c>
--            <c wei="3" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="30">
--              <i ca="" id="1">free access to new shower and toilets #!##!#pick up luggage in within 20 miles #!##!#new comedy and poetrystages</i>
--            </c>
--            <c typ="4" id="33">
--              <i cc="0" id="1" />
--            </c>
--          </s>
--        </p>
--      </item>
--      <item id="6050P6780" reMarked="1" name="BR27_04" totalMark="4" version="70" markingType="1" userMark="0" markerUserMark="3" userAttempted="1" viewingTime="207812" unit="2" quT="11" MarkedMetadata_6="4">
--        <p um="0" cs="1" ua="1" id="6050P6780">
--          <s ua="1" um="0" id="1">
--            <c typ="4" id="28">
--              <i cc="1" id="1" />
--            </c>
--            <c typ="4" id="29">
--              <i cc="1" id="1" />
--            </c>
--            <c wei="2" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="30">
--              <i ca="" id="1">the big title to stand out#!#use of pictures for transport and accomodation for visual effect</i>
--            </c>
--            <c typ="4" id="33">
--              <i cc="0" id="1" />
--            </c>
--            <c wei="2" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="35">
--              <i ca="" id="1">smaller paragraphs easier to read#!#Big bold title to stand out</i>
--            </c>
--          </s>
--        </p>
--      </item>
--      <item id="6050P6781" reMarked="1" name="BR27_05" totalMark="4" version="69" markingType="1" userMark="0" markerUserMark="3" userAttempted="1" viewingTime="603782" unit="2" quT="11" MarkedMetadata_5="4">
--        <p um="0" cs="1" ua="1" id="6050P6781">
--          <s ua="1" um="0" id="1">
--            <c typ="4" id="28">
--              <i cc="1" id="1" />
--            </c>
--            <c typ="4" id="29">
--              <i cc="0" id="1" />
--            </c>
--            <c wei="4" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="30">
--              <i ca="" id="1">Jay says you would be better off staying at home due to the high prices and lack of home comforts. The prices are too high and you would be better off watching it on your own tv with your home comforts. the weather conditions would be awful up to your knees in mud due to the rain. #!#Mark says that Jay is misleading people because hes not giving them the right information. Mark is a member of the organising committee so he gives actual facts. he says there  are new showers and toilets with disabled access to that the facililties are really good. the price he thinks is good value for 3 days entertainment because there is a llot of thinga going on.</i>
--            </c>
--            <c typ="4" id="33">
--              <i cc="1" id="1" />
--            </c>
--          </s>
--        </p>
--      </item>
--      <item id="6050P6782" reMarked="1" name="BR27_06" totalMark="4" version="71" markingType="1" userMark="0" markerUserMark="3" userAttempted="1" viewingTime="495867" unit="2" quT="11" MarkedMetadata_6="4">
--        <p um="0" cs="1" ua="1" id="6050P6782">
--          <s ua="1" um="0" id="1">
--            <c typ="4" id="28">
--              <i cc="1" id="1" />
--            </c>
--            <c typ="4" id="29">
--              <i cc="1" id="1" />
--            </c>
--            <c wei="3" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="30">
--              <i ca="" id="1">document 2 is more biased because Jay is saying his opinion and not using actual facts with evidence to back up.#!#examples are only the rich can afford the prices  and  it stops real music lovers being able to afford it -how does he know what the can afford.#!#the comforts of your own home hes also saying why stay in a tent for that price -how does he know that people dont like being in a tent</i>
--            </c>
--            <c typ="4" id="33">
--              <i cc="0" id="1" />
--            </c>
--            <c wei="1" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="35">
--              <i ca="" id="1">you need all the actual facts to make a decision to whether it is value for money for you</i>
--            </c>
--          </s>
--        </p>
--      </item>
--      <item id="6050P6783" reMarked="1" name="BR27_07" totalMark="4" version="71" markingType="1" userMark="0" markerUserMark="4" userAttempted="1" viewingTime="483336" unit="2" quT="11" MarkedMetadata_6="4">
--        <p um="0" cs="1" ua="1" id="6050P6783">
--          <s ua="1" um="0" id="1">
--            <c typ="4" id="28">
--              <i cc="0" id="1" />
--            </c>
--            <c typ="4" id="29">
--              <i cc="1" id="1" />
--            </c>
--            <c wei="4" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="30">
--              <i ca="" id="1">i think i would recomend that my friend should go because there are new improved showers and toilets so this is good.because  of personal hygiene is important.#!#I think its good value for money because of all the acts and entertainment availble because it gives more value and there is lots more choice than just one act to see.#!#They offer a luggage collection service within 20 miles so this is good because you dont have to worrry about carting it about .#!#There is disabled access to all the 12 stages so this is good because its  giving everybody a chance to go and not discrimminating against no-one.</i>
--            </c>
--            <c typ="4" id="33">
--              <i cc="0" id="1" />
--            </c>
--          </s>
--        </p>
--      </item>
--    </section>
--  </assessment>
--  <isValid>1</isValid>
--  <isPrintable>0</isPrintable>
--  <qualityReview>0</qualityReview>
--  <numberOfGenerations>1</numberOfGenerations>
--  <requiresInvigilation>0</requiresInvigilation>
--  <advanceContentDownloadTimespan>24</advanceContentDownloadTimespan>
--  <automaticVerification>0</automaticVerification>
--  <offlineMode>2</offlineMode>
--  <requiresValidation>0</requiresValidation>
--  <requiresSecureClient>1</requiresSecureClient>
--  <examType>0</examType>
--  <advanceDownload>0</advanceDownload>
--  <gradeBoundaryData>
--    <gradeBoundaries passLevelType="0" adjustedGradeBoundaries="0">
--      <grade modifier="lt" value="12" description="Fail" />
--      <grade modifier="gt" value="12" description="Fail n" />
--      <grade modifier="gt" value="14" description="Pass" userCreated="1" />
--    </gradeBoundaries>
--  </gradeBoundaryData>
--  <autoViewExam>0</autoViewExam>
--  <autoProgressItems>0</autoProgressItems>
--  <confirmationText>
--    <confirmationText>By ticking this box you confirm that the above details are correct and you will adhere to OCR''s code of conduct.</confirmationText>
--  </confirmationText>
--  <requiresConfirmationCheck>1</requiresConfirmationCheck>
--  <allowCloseWithoutSubmit>0</allowCloseWithoutSubmit>
--  <useSecureMarker>0</useSecureMarker>
--  <annotationVersion>2</annotationVersion>
--  <allowPackagingDelivery>1</allowPackagingDelivery>
--  <scoreBoundaryData>
--    <scoreBoundaries showScoreBoundaryColumn="0">
--      <score modifier="lt" value="40" description="Not met" higherBoundarySet="1" />
--      <score modifier="lt" value="40" description="Not met" higherBoundarySet="0" />
--      <score modifier="gt" value="40" description="Close to met" higherBoundarySet="1" />
--      <score modifier="gt" value="40" description="Met" higherBoundarySet="0" />
--      <score modifier="gt" value="45" description="Met" higherBoundarySet="1" />
--      <score modifier="gt" value="80" description="Exceeded" higherBoundarySet="1" />
--    </scoreBoundaries>
--  </scoreBoundaryData>
--  <showCandidateReportResultColumn>0</showCandidateReportResultColumn>
--  <showCandidateReportPercentageColumn>1</showCandidateReportPercentageColumn>
--  <certifiedAccessible>0</certifiedAccessible>
--  <markingProgressVisible>1</markingProgressVisible>
--  <markingProgressMode>0</markingProgressMode>
--  <secureClientOperationMode>1</secureClientOperationMode>
--  <examDeliverySystemStyleType>delivery</examDeliverySystemStyleType>
--  <markingAutoVoidPeriod>365</markingAutoVoidPeriod>
--  <showPageRequiresScrollingAlert>1</showPageRequiresScrollingAlert>
--  <project ID="4209" />
--  <project ID="6002" />
--  <project ID="6050">
--    <value display="1R1 Candidate has identified the main points, ideas and how they are presented in a variety of texts">0</value>
--    <value display="1R2 Candidate has read and understood texts in detail">1</value>
--    <value display="1R3 Candidate has utilised information contained in texts">2</value>
--    <value display="1R4 Candidate has identified suitable responses to texts">3</value>
--    <value display="2R1 Candidate has obtained and utilised relevant information and identified purposes of texts">4</value>
--    <value display="2R2 Candidate has read and summarised info, analysed texts in rel to needs and considered suit resp">5</value>
--    <value display="2R3 Candidate has commented on how meaning is conveyed, detected views, implic. meaning and/or bias ">6</value>
--  </project>
--</assessmentDetails>'
where examSessionId = 62797