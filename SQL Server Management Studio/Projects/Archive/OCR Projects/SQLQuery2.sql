DECLARE @ExamVersionReferencePattern NVARCHAR = 'FSUM_ONDEMAND_0000_%';

WITH temp AS (
	SELECT 
		row_number() over (partition by [ExamSessionTable].[ID] ORDER BY [ExamSessionTable].[ID]) as rownum
		,[ExamSessionTable].[ID] AS 'ExamSessionId'
		--,[CentreTable].CentreCode AS 'CentreCode'
		--,[CentreTable].CentreName AS 'CentreName'
		,[ScheduledExamsTable].examVersionRef AS 'ExamVersionRef'
		,[ScheduledExamsTable].examVersionName AS 'ExamVersionName'
		--,[ExamSessionItemResponseTable].MarkerResponseData.value('(//userId)[last()]','int') AS ExaminerUserId
		,REPLACE(CONVERT(varchar(10),DOB,103), '/', '') AS CandidateDOB
		,[ExamSessionTable].ResultData.query('data(/exam/@userMark)').value('.', 'varchar(3)') as TotalMark
		,[ExamSessionTable].ResultData
		,[UserTable].Forename
		,[UserTable].Surname
		,[Usertable].Middlename AS Initials
		,[UserTable].Gender
		--,REPLACE(CONVERT(varchar(10),[ExamStateChangeAuditTable].StateChangeDate,103), '/', '') AS TestDate
		,[UserTable].ULN AS ULN
		,[UserTable].ID as UserId

		FROM [ExamSessionTable]
			INNER JOIN [ScheduledExamsTable]
				ON ScheduledExamsTable.ID = [ExamSessionTable].ScheduledExamID
			--INNER JOIN CentreTable
			--	ON CentreTable.ID = [ScheduledExamsTable].CentreID
			 -- LEFT JOIN ExamSessionItemResponseTable
				--ON ExamSessionItemResponseTable.ExamSessionID = [ExamSessionTable].ID
				--AND ExamSessionItemResponseTable.ItemID = [ExamSessionTable].resultData.value('data(//item[@userAttempted = "1" and @markingType != "0"]/@id)[1]', 'varchar(15)')			
			INNER JOIN UserTable
				ON UserTable.ID = ExamSessionTable.UserID
			--INNER JOIN [ExamStateChangeAuditTable]
			--	ON ExamStateChangeAuditTable.ExamSessionId = ExamSessionTable.Id
			--	AND ExamStateChangeAuditTable.NewState = 9
		WHERE
			--[examState] = 16
			--AND
			[ScheduledExamsTable].examVersionRef LIKE  'FSUM_ONDEMAND_0000%'
	)

SELECT

temp.ExamSessionId,
--temp.CentreCode,
--temp.CentreName,
temp.ExamVersionRef,
temp.ExamVersionName,
--ISNULL(SUBSTRING(UserTable.Username,3,4), '0000') AS ExaminerNo,
temp.CandidateDOB,
temp.TotalMark,
temp.resultData,
temp.Forename, 
temp.Surname,
temp.Initials,
temp.Gender,
--temp.TestDate, 
temp.ULN,
temp.UserId
 
FROM temp
--	LEFT JOIN UserTable
--ON UserTable.id = temp.ExaminerUserId 
WHERE rownum = 1
ORDER BY ExamVersionRef
