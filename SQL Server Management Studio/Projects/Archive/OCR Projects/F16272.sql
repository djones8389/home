SELECT WEST.ID, WEST.KeyCode, WUT.Forename, WUT.Surname, WUT.CandidateRef, WCT.CentreName, WSCET.examName, WEST.completionDate
	, WESTS.ExternalReference, west.StructureXML
 FROM WAREHOUSE_ExamSessionTable as WEST

  Inner Join WAREHOUSE_ScheduledExamsTable as WSCET
  on WSCET.ID = WEST.WAREHOUSEScheduledExamID
  
  Inner Join WAREHOUSE_UserTable as WUT
  on WUT.ID = WEST.WAREHOUSEUserID

  Inner Join WAREHOUSE_CentreTable as WCT
  on WCT.ID = WSCET.WAREHOUSECentreID

  Inner Join WAREHOUSE_ExamSessionTable_Shreded as WESTS
  on WESTS.examSessionId = WEST.ID

where west.KeyCode in ('9DGHPJB8 ','B5HPRUB8 ', 'WNR7MHB8')

--46101	9DGHPJB8 - FAILS
--46081	B5HPRUB8 - WORKS

select * from WAREHOUSE_ExamSessionItemResponseTable where WAREHOUSEExamSessionID in (46101,46081, 44856) and ItemID = '6049P6385'
select * from WAREHOUSE_ExamSessionTable_ShrededItems where examSessionId in (46101,46081)