/*select top 100

	ID
	, ExamStateChangeAuditXml
	
 from WAREHOUSE_ExamSessionTable where PreviousExamState = 10 order by id desc
 */
 
 
 select ID, Keycode, ExamStateChangeAuditXml  
	from Warehouse_examsessiontable 
		where keycode in 
 ('ZDZLAYB8','VGS8PFB8','MVS9VXB8','7K8URAB8','G8CW3MB8','LJJU7NB8','JEK5B7B8','SMU5FNB8','3ESSRHB8','5EF5DGB8','PCDU9TB8','XVFX48B8','Y8NAGTB8','HDKULCB8','8JE6ULB8','Z9X9G9B8','4E4MGWB8')
 

 
 
/*
delete from ReMarkExamSessionTable where CentreID = 416 

update WAREHOUSE_ExamSessionTable
set ExamStateChangeAuditXml = '<exam>
  <stateChange>
    <newStateID>1</newStateID>
    <newState>Exam Scheduled (but not created)</newState>
    <changeDate>20 Nov 2013 15:37:26:56</changeDate>
  </stateChange>
  <stateChange>
    <newStateID>2</newStateID>
    <newState>Exam Scheduled (but not downloaded)</newState>
    <changeDate>20 Nov 2013 15:37:35:19</changeDate>
  </stateChange>
  <stateChange>
    <newStateID>3</newStateID>
    <newState>Exam Scheduled (but locked)</newState>
    <changeDate>20 Nov 2013 15:37:50:27</changeDate>
    <information />
  </stateChange>
  <stateChange>
    <newStateID>4</newStateID>
    <newState>Exam Ready</newState>
    <changeDate>21 Nov 2013 00:00:00:96</changeDate>
    <information />
  </stateChange>
  <stateChange>
    <newStateID>3</newStateID>
    <newState>Exam Scheduled (but locked)</newState>
    <changeDate>22 Nov 2013 00:00:06:93</changeDate>
    <information />
  </stateChange>
  <stateChange>
    <newStateID>18</newStateID>
    <newState>Awaiting Upload</newState>
    <changeDate>22 Nov 2013 00:00:08:16</changeDate>
    <information />
  </stateChange>
  <stateChange>
    <newStateID>10</newStateID>
    <newState>Exam Voided</newState>
    <changeDate>29 Nov 2013 00:00:14:92</changeDate>
    <information />
  </stateChange>
  <stateChange>
    <newStateID>4</newStateID>
    <newState>Exam Ready</newState>
    <changeDate>09 May 2014 09:03:01:44</changeDate>
  </stateChange>
  <stateChange>
    <newStateID>6</newStateID>
    <newState>Exam Delivered To Candidate</newState>
    <changeDate>09 May 2014 09:03:01:44</changeDate>
  </stateChange>
  <stateChange>
    <newStateID>9</newStateID>
    <newState>Exam Submitted</newState>
    <changeDate>09 May 2014 09:03:01:44</changeDate>
  </stateChange>
    <stateChange>
    <newStateID>10</newStateID>
    <newState>Exam Voided</newState>
    <changeDate>23 Jun 2014 15:30:00:00</changeDate>
    <information>
      <stateChangeInformation xmlns="">
        <type>void</type>
        <reason>4</reason>
        <description>Voided by OCR after discussion with the centre.</description>
      </stateChangeInformation>
    </information>
  </stateChange>
</exam>'
where id =  31636

update WAREHOUSE_ExamSessionTable
set ExamStateChangeAuditXml = '<exam>
  <stateChange>
    <newStateID>1</newStateID>
    <newState>Exam Scheduled (but not created)</newState>
    <changeDate>27 Nov 2013 21:22:45:00</changeDate>
  </stateChange>
  <stateChange>
    <newStateID>2</newStateID>
    <newState>Exam Scheduled (but not downloaded)</newState>
    <changeDate>27 Nov 2013 21:22:52:87</changeDate>
  </stateChange>
  <stateChange>
    <newStateID>3</newStateID>
    <newState>Exam Scheduled (but locked)</newState>
    <changeDate>27 Nov 2013 21:23:07:95</changeDate>
    <information />
  </stateChange>
  <stateChange>
    <newStateID>4</newStateID>
    <newState>Exam Ready</newState>
    <changeDate>28 Nov 2013 00:00:01:59</changeDate>
    <information />
  </stateChange>
  <stateChange>
    <newStateID>3</newStateID>
    <newState>Exam Scheduled (but locked)</newState>
    <changeDate>29 Nov 2013 00:00:13:58</changeDate>
    <information />
  </stateChange>
  <stateChange>
    <newStateID>18</newStateID>
    <newState>Awaiting Upload</newState>
    <changeDate>29 Nov 2013 00:00:14:88</changeDate>
    <information />
  </stateChange>
  <stateChange>
    <newStateID>10</newStateID>
    <newState>Exam Voided</newState>
    <changeDate>06 Dec 2013 00:00:21:86</changeDate>
    <information />
  </stateChange>
  <stateChange>
    <newStateID>4</newStateID>
    <newState>Exam Ready</newState>
    <changeDate>09 May 2014 09:05:17:30</changeDate>
  </stateChange>
  <stateChange>
    <newStateID>6</newStateID>
    <newState>Exam Delivered To Candidate</newState>
    <changeDate>09 May 2014 09:05:17:30</changeDate>
  </stateChange>
  <stateChange>
    <newStateID>9</newStateID>
    <newState>Exam Submitted</newState>
    <changeDate>09 May 2014 09:05:17:30</changeDate>
  </stateChange>
    <stateChange>
    <newStateID>10</newStateID>
    <newState>Exam Voided</newState>
    <changeDate>23 Jun 2014 15:30:00:00</changeDate>
    <information>
      <stateChangeInformation xmlns="">
        <type>void</type>
        <reason>4</reason>
        <description>Voided by OCR after discussion with the centre.</description>
      </stateChangeInformation>
    </information>
  </stateChange>
</exam>'
where id = 32641

update WAREHOUSE_ExamSessionTable
set ExamStateChangeAuditXml = '<exam>
  <stateChange>
    <newStateID>1</newStateID>
    <newState>Exam Scheduled (but not created)</newState>
    <changeDate>27 Nov 2013 21:23:44:93</changeDate>
  </stateChange>
  <stateChange>
    <newStateID>2</newStateID>
    <newState>Exam Scheduled (but not downloaded)</newState>
    <changeDate>27 Nov 2013 21:23:53:11</changeDate>
  </stateChange>
  <stateChange>
    <newStateID>3</newStateID>
    <newState>Exam Scheduled (but locked)</newState>
    <changeDate>27 Nov 2013 21:24:08:16</changeDate>
    <information />
  </stateChange>
  <stateChange>
    <newStateID>4</newStateID>
    <newState>Exam Ready</newState>
    <changeDate>28 Nov 2013 00:00:01:59</changeDate>
    <information />
  </stateChange>
  <stateChange>
    <newStateID>3</newStateID>
    <newState>Exam Scheduled (but locked)</newState>
    <changeDate>29 Nov 2013 00:00:13:58</changeDate>
    <information />
  </stateChange>
  <stateChange>
    <newStateID>18</newStateID>
    <newState>Awaiting Upload</newState>
    <changeDate>29 Nov 2013 00:00:14:88</changeDate>
    <information />
  </stateChange>
  <stateChange>
    <newStateID>10</newStateID>
    <newState>Exam Voided</newState>
    <changeDate>06 Dec 2013 00:00:21:86</changeDate>
    <information />
  </stateChange>
  <stateChange>
    <newStateID>4</newStateID>
    <newState>Exam Ready</newState>
    <changeDate>09 May 2014 09:07:35:21</changeDate>
  </stateChange>
  <stateChange>
    <newStateID>6</newStateID>
    <newState>Exam Delivered To Candidate</newState>
    <changeDate>09 May 2014 09:07:35:21</changeDate>
  </stateChange>
  <stateChange>
    <newStateID>9</newStateID>
    <newState>Exam Submitted</newState>
    <changeDate>09 May 2014 09:07:35:21</changeDate>
  </stateChange>
   <stateChange>
    <newStateID>10</newStateID>
    <newState>Exam Voided</newState>
    <changeDate>23 Jun 2014 15:30:00:00</changeDate>
    <information>
      <stateChangeInformation xmlns="">
        <type>void</type>
        <reason>4</reason>
        <description>Voided by OCR after discussion with the centre.</description>
      </stateChangeInformation>
    </information>
  </stateChange>
</exam>'
where id = 32642

update WAREHOUSE_ExamSessionTable
set ExamStateChangeAuditXml = '<exam>
  <stateChange>
    <newStateID>1</newStateID>
    <newState>Exam Scheduled (but not created)</newState>
    <changeDate>27 Nov 2013 21:24:33:49</changeDate>
  </stateChange>
  <stateChange>
    <newStateID>2</newStateID>
    <newState>Exam Scheduled (but not downloaded)</newState>
    <changeDate>27 Nov 2013 21:24:38:66</changeDate>
  </stateChange>
  <stateChange>
    <newStateID>3</newStateID>
    <newState>Exam Scheduled (but locked)</newState>
    <changeDate>27 Nov 2013 21:24:53:76</changeDate>
    <information />
  </stateChange>
  <stateChange>
    <newStateID>4</newStateID>
    <newState>Exam Ready</newState>
    <changeDate>28 Nov 2013 00:00:01:59</changeDate>
    <information />
  </stateChange>
  <stateChange>
    <newStateID>3</newStateID>
    <newState>Exam Scheduled (but locked)</newState>
    <changeDate>29 Nov 2013 00:00:13:58</changeDate>
    <information />
  </stateChange>
  <stateChange>
    <newStateID>18</newStateID>
    <newState>Awaiting Upload</newState>
    <changeDate>29 Nov 2013 00:00:14:88</changeDate>
    <information />
  </stateChange>
  <stateChange>
    <newStateID>10</newStateID>
    <newState>Exam Voided</newState>
    <changeDate>06 Dec 2013 00:00:21:86</changeDate>
    <information />
  </stateChange>
  <stateChange>
    <newStateID>4</newStateID>
    <newState>Exam Ready</newState>
    <changeDate>09 May 2014 09:10:03:32</changeDate>
  </stateChange>
  <stateChange>
    <newStateID>6</newStateID>
    <newState>Exam Delivered To Candidate</newState>
    <changeDate>09 May 2014 09:10:03:32</changeDate>
  </stateChange>
  <stateChange>
    <newStateID>9</newStateID>
    <newState>Exam Submitted</newState>
    <changeDate>09 May 2014 09:10:03:32</changeDate>
  </stateChange>
  <stateChange>
    <newStateID>10</newStateID>
    <newState>Exam Voided</newState>
    <changeDate>23 Jun 2014 15:30:00:00</changeDate>
    <information>
      <stateChangeInformation xmlns="">
        <type>void</type>
        <reason>4</reason>
        <description>Voided by OCR after discussion with the centre.</description>
      </stateChangeInformation>
    </information>
  </stateChange>
</exam>'
where id = 32643

update WAREHOUSE_ExamSessionTable
set ExamStateChangeAuditXml = '<exam>
  <stateChange>
    <newStateID>1</newStateID>
    <newState>Exam Scheduled (but not created)</newState>
    <changeDate>04 Dec 2013 14:39:45:12</changeDate>
  </stateChange>
  <stateChange>
    <newStateID>2</newStateID>
    <newState>Exam Scheduled (but not downloaded)</newState>
    <changeDate>04 Dec 2013 14:39:46:86</changeDate>
  </stateChange>
  <stateChange>
    <newStateID>3</newStateID>
    <newState>Exam Scheduled (but locked)</newState>
    <changeDate>04 Dec 2013 14:40:01:99</changeDate>
    <information />
  </stateChange>
  <stateChange>
    <newStateID>4</newStateID>
    <newState>Exam Ready</newState>
    <changeDate>05 Dec 2013 00:00:14:66</changeDate>
    <information />
  </stateChange>
  <stateChange>
    <newStateID>3</newStateID>
    <newState>Exam Scheduled (but locked)</newState>
    <changeDate>06 Dec 2013 00:00:03:50</changeDate>
    <information />
  </stateChange>
  <stateChange>
    <newStateID>18</newStateID>
    <newState>Awaiting Upload</newState>
    <changeDate>06 Dec 2013 00:00:04:67</changeDate>
    <information />
  </stateChange>
  <stateChange>
    <newStateID>10</newStateID>
    <newState>Exam Voided</newState>
    <changeDate>13 Dec 2013 00:00:15:35</changeDate>
    <information />
  </stateChange>
  <stateChange>
    <newStateID>4</newStateID>
    <newState>Exam Ready</newState>
    <changeDate>09 May 2014 09:14:02:74</changeDate>
  </stateChange>
  <stateChange>
    <newStateID>6</newStateID>
    <newState>Exam Delivered To Candidate</newState>
    <changeDate>09 May 2014 09:14:02:74</changeDate>
  </stateChange>
  <stateChange>
    <newStateID>9</newStateID>
    <newState>Exam Submitted</newState>
    <changeDate>09 May 2014 09:14:02:74</changeDate>
  </stateChange>
  <stateChange>
    <newStateID>10</newStateID>
    <newState>Exam Voided</newState>
    <changeDate>23 Jun 2014 15:30:00:00</changeDate>
    <information>
      <stateChangeInformation xmlns="">
        <type>void</type>
        <reason>4</reason>
        <description>Voided by OCR after discussion with the centre.</description>
      </stateChangeInformation>
    </information>
  </stateChange>
</exam>'
where id = 33578

update WAREHOUSE_ExamSessionTable
set ExamStateChangeAuditXml = '<exam>
  <stateChange>
    <newStateID>1</newStateID>
    <newState>Exam Scheduled (but not created)</newState>
    <changeDate>04 Dec 2013 14:40:36:90</changeDate>
  </stateChange>
  <stateChange>
    <newStateID>2</newStateID>
    <newState>Exam Scheduled (but not downloaded)</newState>
    <changeDate>04 Dec 2013 14:40:48:25</changeDate>
  </stateChange>
  <stateChange>
    <newStateID>3</newStateID>
    <newState>Exam Scheduled (but locked)</newState>
    <changeDate>04 Dec 2013 14:41:03:73</changeDate>
    <information />
  </stateChange>
  <stateChange>
    <newStateID>4</newStateID>
    <newState>Exam Ready</newState>
    <changeDate>05 Dec 2013 00:00:14:66</changeDate>
    <information />
  </stateChange>
  <stateChange>
    <newStateID>3</newStateID>
    <newState>Exam Scheduled (but locked)</newState>
    <changeDate>06 Dec 2013 00:00:03:50</changeDate>
    <information />
  </stateChange>
  <stateChange>
    <newStateID>18</newStateID>
    <newState>Awaiting Upload</newState>
    <changeDate>06 Dec 2013 00:00:04:67</changeDate>
    <information />
  </stateChange>
  <stateChange>
    <newStateID>10</newStateID>
    <newState>Exam Voided</newState>
    <changeDate>13 Dec 2013 00:00:15:35</changeDate>
    <information />
  </stateChange>
  <stateChange>
    <newStateID>4</newStateID>
    <newState>Exam Ready</newState>
    <changeDate>09 May 2014 09:15:34:80</changeDate>
  </stateChange>
  <stateChange>
    <newStateID>6</newStateID>
    <newState>Exam Delivered To Candidate</newState>
    <changeDate>09 May 2014 09:15:34:80</changeDate>
  </stateChange>
  <stateChange>
    <newStateID>9</newStateID>
    <newState>Exam Submitted</newState>
    <changeDate>09 May 2014 09:15:34:80</changeDate>
  </stateChange>
  <stateChange>
    <newStateID>10</newStateID>
    <newState>Exam Voided</newState>
    <changeDate>23 Jun 2014 15:30:00:00</changeDate>
    <information>
      <stateChangeInformation xmlns="">
        <type>void</type>
        <reason>4</reason>
        <description>Voided by OCR after discussion with the centre.</description>
      </stateChangeInformation>
    </information>
  </stateChange>
</exam>'
where id = 33580

update WAREHOUSE_ExamSessionTable
set ExamStateChangeAuditXml = '<exam>
  <stateChange>
    <newStateID>1</newStateID>
    <newState>Exam Scheduled (but not created)</newState>
    <changeDate>04 Dec 2013 14:51:06:18</changeDate>
  </stateChange>
  <stateChange>
    <newStateID>2</newStateID>
    <newState>Exam Scheduled (but not downloaded)</newState>
    <changeDate>05 Dec 2013 00:03:29:84</changeDate>
  </stateChange>
  <stateChange>
    <newStateID>3</newStateID>
    <newState>Exam Scheduled (but locked)</newState>
    <changeDate>05 Dec 2013 00:03:45:02</changeDate>
    <information />
  </stateChange>
  <stateChange>
    <newStateID>4</newStateID>
    <newState>Exam Ready</newState>
    <changeDate>06 Dec 2013 00:00:03:46</changeDate>
    <information />
  </stateChange>
  <stateChange>
    <newStateID>3</newStateID>
    <newState>Exam Scheduled (but locked)</newState>
    <changeDate>07 Dec 2013 00:00:10:71</changeDate>
    <information />
  </stateChange>
  <stateChange>
    <newStateID>18</newStateID>
    <newState>Awaiting Upload</newState>
    <changeDate>07 Dec 2013 00:00:11:93</changeDate>
    <information />
  </stateChange>
  <stateChange>
    <newStateID>10</newStateID>
    <newState>Exam Voided</newState>
    <changeDate>14 Dec 2013 00:00:30:54</changeDate>
    <information />
  </stateChange>
  <stateChange>
    <newStateID>4</newStateID>
    <newState>Exam Ready</newState>
    <changeDate>09 May 2014 09:19:18:57</changeDate>
  </stateChange>
  <stateChange>
    <newStateID>6</newStateID>
    <newState>Exam Delivered To Candidate</newState>
    <changeDate>09 May 2014 09:19:18:57</changeDate>
  </stateChange>
  <stateChange>
    <newStateID>9</newStateID>
    <newState>Exam Submitted</newState>
    <changeDate>09 May 2014 09:19:18:57</changeDate>
  </stateChange>
  <stateChange>
    <newStateID>10</newStateID>
    <newState>Exam Voided</newState>
    <changeDate>23 Jun 2014 15:30:00:00</changeDate>
    <information>
      <stateChangeInformation xmlns="">
        <type>void</type>
        <reason>4</reason>
        <description>Voided by OCR after discussion with the centre.</description>
      </stateChangeInformation>
    </information>
  </stateChange>
</exam>'
where id = 33744

update WAREHOUSE_ExamSessionTable
set ExamStateChangeAuditXml = '<exam>
  <stateChange>
    <newStateID>1</newStateID>
    <newState>Exam Scheduled (but not created)</newState>
    <changeDate>04 Dec 2013 14:54:43:43</changeDate>
  </stateChange>
  <stateChange>
    <newStateID>2</newStateID>
    <newState>Exam Scheduled (but not downloaded)</newState>
    <changeDate>05 Dec 2013 00:03:29:88</changeDate>
  </stateChange>
  <stateChange>
    <newStateID>3</newStateID>
    <newState>Exam Scheduled (but locked)</newState>
    <changeDate>05 Dec 2013 00:04:15:91</changeDate>
    <information />
  </stateChange>
  <stateChange>
    <newStateID>4</newStateID>
    <newState>Exam Ready</newState>
    <changeDate>06 Dec 2013 00:00:03:46</changeDate>
    <information />
  </stateChange>
  <stateChange>
    <newStateID>3</newStateID>
    <newState>Exam Scheduled (but locked)</newState>
    <changeDate>07 Dec 2013 00:00:10:71</changeDate>
    <information />
  </stateChange>
  <stateChange>
    <newStateID>18</newStateID>
    <newState>Awaiting Upload</newState>
    <changeDate>07 Dec 2013 00:00:11:93</changeDate>
    <information />
  </stateChange>
  <stateChange>
    <newStateID>10</newStateID>
    <newState>Exam Voided</newState>
    <changeDate>14 Dec 2013 00:00:30:54</changeDate>
    <information />
  </stateChange>
  <stateChange>
    <newStateID>4</newStateID>
    <newState>Exam Ready</newState>
    <changeDate>09 May 2014 09:21:02:10</changeDate>
  </stateChange>
  <stateChange>
    <newStateID>6</newStateID>
    <newState>Exam Delivered To Candidate</newState>
    <changeDate>09 May 2014 09:21:02:10</changeDate>
  </stateChange>
  <stateChange>
    <newStateID>9</newStateID>
    <newState>Exam Submitted</newState>
    <changeDate>09 May 2014 09:21:02:10</changeDate>
  </stateChange>
  <stateChange>
    <newStateID>10</newStateID>
    <newState>Exam Voided</newState>
    <changeDate>23 Jun 2014 15:30:00:00</changeDate>
    <information>
      <stateChangeInformation xmlns="">
        <type>void</type>
        <reason>4</reason>
        <description>Voided by OCR after discussion with the centre.</description>
      </stateChangeInformation>
    </information>
  </stateChange>
</exam>'
where id = 33745

update WAREHOUSE_ExamSessionTable
set ExamStateChangeAuditXml = '<exam>
  <stateChange>
    <newStateID>1</newStateID>
    <newState>Exam Scheduled (but not created)</newState>
    <changeDate>04 Dec 2013 14:54:43:43</changeDate>
  </stateChange>
  <stateChange>
    <newStateID>2</newStateID>
    <newState>Exam Scheduled (but not downloaded)</newState>
    <changeDate>05 Dec 2013 00:03:29:88</changeDate>
  </stateChange>
  <stateChange>
    <newStateID>3</newStateID>
    <newState>Exam Scheduled (but locked)</newState>
    <changeDate>05 Dec 2013 00:03:45:08</changeDate>
    <information />
  </stateChange>
  <stateChange>
    <newStateID>4</newStateID>
    <newState>Exam Ready</newState>
    <changeDate>06 Dec 2013 00:00:03:46</changeDate>
    <information />
  </stateChange>
  <stateChange>
    <newStateID>3</newStateID>
    <newState>Exam Scheduled (but locked)</newState>
    <changeDate>07 Dec 2013 00:00:10:71</changeDate>
    <information />
  </stateChange>
  <stateChange>
    <newStateID>18</newStateID>
    <newState>Awaiting Upload</newState>
    <changeDate>07 Dec 2013 00:00:11:93</changeDate>
    <information />
  </stateChange>
  <stateChange>
    <newStateID>10</newStateID>
    <newState>Exam Voided</newState>
    <changeDate>14 Dec 2013 00:00:30:54</changeDate>
    <information />
  </stateChange>
  <stateChange>
    <newStateID>4</newStateID>
    <newState>Exam Ready</newState>
    <changeDate>09 May 2014 09:22:50:46</changeDate>
  </stateChange>
  <stateChange>
    <newStateID>6</newStateID>
    <newState>Exam Delivered To Candidate</newState>
    <changeDate>09 May 2014 09:22:50:46</changeDate>
  </stateChange>
  <stateChange>
    <newStateID>9</newStateID>
    <newState>Exam Submitted</newState>
    <changeDate>09 May 2014 09:22:50:46</changeDate>
  </stateChange>
  <stateChange>
    <newStateID>10</newStateID>
    <newState>Exam Voided</newState>
    <changeDate>23 Jun 2014 15:30:00:00</changeDate>
    <information>
      <stateChangeInformation xmlns="">
        <type>void</type>
        <reason>4</reason>
        <description>Voided by OCR after discussion with the centre.</description>
      </stateChangeInformation>
    </information>
  </stateChange>
</exam>'
where id = 33746

update WAREHOUSE_ExamSessionTable
set ExamStateChangeAuditXml = '<exam>
  <stateChange>
    <newStateID>1</newStateID>
    <newState>Exam Scheduled (but not created)</newState>
    <changeDate>04 Dec 2013 14:54:43:43</changeDate>
  </stateChange>
  <stateChange>
    <newStateID>2</newStateID>
    <newState>Exam Scheduled (but not downloaded)</newState>
    <changeDate>05 Dec 2013 00:03:29:88</changeDate>
  </stateChange>
  <stateChange>
    <newStateID>3</newStateID>
    <newState>Exam Scheduled (but locked)</newState>
    <changeDate>05 Dec 2013 00:04:15:91</changeDate>
    <information />
  </stateChange>
  <stateChange>
    <newStateID>4</newStateID>
    <newState>Exam Ready</newState>
    <changeDate>06 Dec 2013 00:00:03:46</changeDate>
    <information />
  </stateChange>
  <stateChange>
    <newStateID>3</newStateID>
    <newState>Exam Scheduled (but locked)</newState>
    <changeDate>07 Dec 2013 00:00:10:71</changeDate>
    <information />
  </stateChange>
  <stateChange>
    <newStateID>18</newStateID>
    <newState>Awaiting Upload</newState>
    <changeDate>07 Dec 2013 00:00:11:93</changeDate>
    <information />
  </stateChange>
  <stateChange>
    <newStateID>10</newStateID>
    <newState>Exam Voided</newState>
    <changeDate>14 Dec 2013 00:00:30:54</changeDate>
    <information />
  </stateChange>
  <stateChange>
    <newStateID>4</newStateID>
    <newState>Exam Ready</newState>
    <changeDate>09 May 2014 09:24:29:32</changeDate>
  </stateChange>
  <stateChange>
    <newStateID>6</newStateID>
    <newState>Exam Delivered To Candidate</newState>
    <changeDate>09 May 2014 09:24:29:32</changeDate>
  </stateChange>
  <stateChange>
    <newStateID>9</newStateID>
    <newState>Exam Submitted</newState>
    <changeDate>09 May 2014 09:24:29:32</changeDate>
  </stateChange>
  <stateChange>
    <newStateID>10</newStateID>
    <newState>Exam Voided</newState>
    <changeDate>23 Jun 2014 15:30:00:00</changeDate>
    <information>
      <stateChangeInformation xmlns="">
        <type>void</type>
        <reason>4</reason>
        <description>Voided by OCR after discussion with the centre.</description>
      </stateChangeInformation>
    </information>
  </stateChange>
</exam>'
where id = 33747

update WAREHOUSE_ExamSessionTable
set ExamStateChangeAuditXml = '<exam>
  <stateChange>
    <newStateID>1</newStateID>
    <newState>Exam Scheduled (but not created)</newState>
    <changeDate>04 Dec 2013 14:54:43:43</changeDate>
  </stateChange>
  <stateChange>
    <newStateID>2</newStateID>
    <newState>Exam Scheduled (but not downloaded)</newState>
    <changeDate>05 Dec 2013 00:03:29:88</changeDate>
  </stateChange>
  <stateChange>
    <newStateID>3</newStateID>
    <newState>Exam Scheduled (but locked)</newState>
    <changeDate>05 Dec 2013 00:04:15:90</changeDate>
    <information />
  </stateChange>
  <stateChange>
    <newStateID>4</newStateID>
    <newState>Exam Ready</newState>
    <changeDate>06 Dec 2013 00:00:03:46</changeDate>
    <information />
  </stateChange>
  <stateChange>
    <newStateID>3</newStateID>
    <newState>Exam Scheduled (but locked)</newState>
    <changeDate>07 Dec 2013 00:00:10:71</changeDate>
    <information />
  </stateChange>
  <stateChange>
    <newStateID>18</newStateID>
    <newState>Awaiting Upload</newState>
    <changeDate>07 Dec 2013 00:00:11:93</changeDate>
    <information />
  </stateChange>
  <stateChange>
    <newStateID>10</newStateID>
    <newState>Exam Voided</newState>
    <changeDate>14 Dec 2013 00:00:30:54</changeDate>
    <information />
  </stateChange>
  <stateChange>
    <newStateID>4</newStateID>
    <newState>Exam Ready</newState>
    <changeDate>09 May 2014 09:25:50:77</changeDate>
  </stateChange>
  <stateChange>
    <newStateID>6</newStateID>
    <newState>Exam Delivered To Candidate</newState>
    <changeDate>09 May 2014 09:25:50:77</changeDate>
  </stateChange>
  <stateChange>
    <newStateID>9</newStateID>
    <newState>Exam Submitted</newState>
    <changeDate>09 May 2014 09:25:50:77</changeDate>
  </stateChange>
  <stateChange>
    <newStateID>10</newStateID>
    <newState>Exam Voided</newState>
    <changeDate>23 Jun 2014 15:30:00:00</changeDate>
    <information>
      <stateChangeInformation xmlns="">
        <type>void</type>
        <reason>4</reason>
        <description>Voided by OCR after discussion with the centre.</description>
      </stateChangeInformation>
    </information>
  </stateChange>
</exam>'
where id = 33748

update WAREHOUSE_ExamSessionTable
set ExamStateChangeAuditXml = '<exam>
  <stateChange>
    <newStateID>1</newStateID>
    <newState>Exam Scheduled (but not created)</newState>
    <changeDate>04 Dec 2013 14:54:43:43</changeDate>
  </stateChange>
  <stateChange>
    <newStateID>2</newStateID>
    <newState>Exam Scheduled (but not downloaded)</newState>
    <changeDate>05 Dec 2013 00:03:29:88</changeDate>
  </stateChange>
  <stateChange>
    <newStateID>3</newStateID>
    <newState>Exam Scheduled (but locked)</newState>
    <changeDate>05 Dec 2013 00:04:15:89</changeDate>
    <information />
  </stateChange>
  <stateChange>
    <newStateID>4</newStateID>
    <newState>Exam Ready</newState>
    <changeDate>06 Dec 2013 00:00:03:46</changeDate>
    <information />
  </stateChange>
  <stateChange>
    <newStateID>3</newStateID>
    <newState>Exam Scheduled (but locked)</newState>
    <changeDate>07 Dec 2013 00:00:10:71</changeDate>
    <information />
  </stateChange>
  <stateChange>
    <newStateID>18</newStateID>
    <newState>Awaiting Upload</newState>
    <changeDate>07 Dec 2013 00:00:11:93</changeDate>
    <information />
  </stateChange>
  <stateChange>
    <newStateID>10</newStateID>
    <newState>Exam Voided</newState>
    <changeDate>14 Dec 2013 00:00:30:54</changeDate>
    <information />
  </stateChange>
  <stateChange>
    <newStateID>4</newStateID>
    <newState>Exam Ready</newState>
    <changeDate>09 May 2014 09:27:14:01</changeDate>
  </stateChange>
  <stateChange>
    <newStateID>6</newStateID>
    <newState>Exam Delivered To Candidate</newState>
    <changeDate>09 May 2014 09:27:14:01</changeDate>
  </stateChange>
  <stateChange>
    <newStateID>9</newStateID>
    <newState>Exam Submitted</newState>
    <changeDate>09 May 2014 09:27:14:01</changeDate>
  </stateChange>
  <stateChange>
    <newStateID>10</newStateID>
    <newState>Exam Voided</newState>
    <changeDate>23 Jun 2014 15:30:00:00</changeDate>
    <information>
      <stateChangeInformation xmlns="">
        <type>void</type>
        <reason>4</reason>
        <description>Voided by OCR after discussion with the centre.</description>
      </stateChangeInformation>
    </information>
  </stateChange>
</exam>'
where id = 33749

update WAREHOUSE_ExamSessionTable
set ExamStateChangeAuditXml = '<exam>
  <stateChange>
    <newStateID>1</newStateID>
    <newState>Exam Scheduled (but not created)</newState>
    <changeDate>04 Dec 2013 14:56:30:15</changeDate>
  </stateChange>
  <stateChange>
    <newStateID>2</newStateID>
    <newState>Exam Scheduled (but not downloaded)</newState>
    <changeDate>05 Dec 2013 00:02:58:67</changeDate>
  </stateChange>
  <stateChange>
    <newStateID>3</newStateID>
    <newState>Exam Scheduled (but locked)</newState>
    <changeDate>05 Dec 2013 00:03:14:43</changeDate>
    <information />
  </stateChange>
  <stateChange>
    <newStateID>4</newStateID>
    <newState>Exam Ready</newState>
    <changeDate>06 Dec 2013 00:00:03:46</changeDate>
    <information />
  </stateChange>
  <stateChange>
    <newStateID>3</newStateID>
    <newState>Exam Scheduled (but locked)</newState>
    <changeDate>07 Dec 2013 00:00:10:71</changeDate>
    <information />
  </stateChange>
  <stateChange>
    <newStateID>18</newStateID>
    <newState>Awaiting Upload</newState>
    <changeDate>07 Dec 2013 00:00:11:93</changeDate>
    <information />
  </stateChange>
  <stateChange>
    <newStateID>10</newStateID>
    <newState>Exam Voided</newState>
    <changeDate>14 Dec 2013 00:00:30:54</changeDate>
    <information />
  </stateChange>
  <stateChange>
    <newStateID>4</newStateID>
    <newState>Exam Ready</newState>
    <changeDate>09 May 2014 09:28:34:25</changeDate>
  </stateChange>
  <stateChange>
    <newStateID>6</newStateID>
    <newState>Exam Delivered To Candidate</newState>
    <changeDate>09 May 2014 09:28:34:25</changeDate>
  </stateChange>
  <stateChange>
    <newStateID>9</newStateID>
    <newState>Exam Submitted</newState>
    <changeDate>09 May 2014 09:28:34:25</changeDate>
  </stateChange>
  <stateChange>
    <newStateID>10</newStateID>
    <newState>Exam Voided</newState>
    <changeDate>23 Jun 2014 15:30:00:00</changeDate>
    <information>
      <stateChangeInformation xmlns="">
        <type>void</type>
        <reason>4</reason>
        <description>Voided by OCR after discussion with the centre.</description>
      </stateChangeInformation>
    </information>
  </stateChange>
</exam>'
where id = 33750

update WAREHOUSE_ExamSessionTable
set ExamStateChangeAuditXml = '<exam>
  <stateChange>
    <newStateID>1</newStateID>
    <newState>Exam Scheduled (but not created)</newState>
    <changeDate>04 Dec 2013 14:56:30:15</changeDate>
  </stateChange>
  <stateChange>
    <newStateID>2</newStateID>
    <newState>Exam Scheduled (but not downloaded)</newState>
    <changeDate>05 Dec 2013 00:02:58:67</changeDate>
  </stateChange>
  <stateChange>
    <newStateID>3</newStateID>
    <newState>Exam Scheduled (but locked)</newState>
    <changeDate>05 Dec 2013 00:03:14:45</changeDate>
    <information />
  </stateChange>
  <stateChange>
    <newStateID>4</newStateID>
    <newState>Exam Ready</newState>
    <changeDate>06 Dec 2013 00:00:03:46</changeDate>
    <information />
  </stateChange>
  <stateChange>
    <newStateID>3</newStateID>
    <newState>Exam Scheduled (but locked)</newState>
    <changeDate>07 Dec 2013 00:00:10:71</changeDate>
    <information />
  </stateChange>
  <stateChange>
    <newStateID>18</newStateID>
    <newState>Awaiting Upload</newState>
    <changeDate>07 Dec 2013 00:00:11:93</changeDate>
    <information />
  </stateChange>
  <stateChange>
    <newStateID>10</newStateID>
    <newState>Exam Voided</newState>
    <changeDate>14 Dec 2013 00:00:30:54</changeDate>
    <information />
  </stateChange>
  <stateChange>
    <newStateID>4</newStateID>
    <newState>Exam Ready</newState>
    <changeDate>09 May 2014 09:31:42:12</changeDate>
  </stateChange>
  <stateChange>
    <newStateID>6</newStateID>
    <newState>Exam Delivered To Candidate</newState>
    <changeDate>09 May 2014 09:31:42:12</changeDate>
  </stateChange>
  <stateChange>
    <newStateID>9</newStateID>
    <newState>Exam Submitted</newState>
    <changeDate>09 May 2014 09:31:42:12</changeDate>
  </stateChange>
  <stateChange>
    <newStateID>10</newStateID>
    <newState>Exam Voided</newState>
    <changeDate>23 Jun 2014 15:30:00:00</changeDate>
    <information>
      <stateChangeInformation xmlns="">
        <type>void</type>
        <reason>4</reason>
        <description>Voided by OCR after discussion with the centre.</description>
      </stateChangeInformation>
    </information>
  </stateChange>
</exam>'
where id = 33751

update WAREHOUSE_ExamSessionTable
set ExamStateChangeAuditXml = '<exam>
  <stateChange>
    <newStateID>1</newStateID>
    <newState>Exam Scheduled (but not created)</newState>
    <changeDate>04 Dec 2013 14:56:30:15</changeDate>
  </stateChange>
  <stateChange>
    <newStateID>2</newStateID>
    <newState>Exam Scheduled (but not downloaded)</newState>
    <changeDate>05 Dec 2013 00:02:58:67</changeDate>
  </stateChange>
  <stateChange>
    <newStateID>3</newStateID>
    <newState>Exam Scheduled (but locked)</newState>
    <changeDate>05 Dec 2013 00:03:14:50</changeDate>
    <information />
  </stateChange>
  <stateChange>
    <newStateID>4</newStateID>
    <newState>Exam Ready</newState>
    <changeDate>06 Dec 2013 00:00:03:46</changeDate>
    <information />
  </stateChange>
  <stateChange>
    <newStateID>3</newStateID>
    <newState>Exam Scheduled (but locked)</newState>
    <changeDate>07 Dec 2013 00:00:10:71</changeDate>
    <information />
  </stateChange>
  <stateChange>
    <newStateID>18</newStateID>
    <newState>Awaiting Upload</newState>
    <changeDate>07 Dec 2013 00:00:11:93</changeDate>
    <information />
  </stateChange>
  <stateChange>
    <newStateID>10</newStateID>
    <newState>Exam Voided</newState>
    <changeDate>14 Dec 2013 00:00:30:54</changeDate>
    <information />
  </stateChange>
  <stateChange>
    <newStateID>4</newStateID>
    <newState>Exam Ready</newState>
    <changeDate>09 May 2014 09:33:14:82</changeDate>
  </stateChange>
  <stateChange>
    <newStateID>6</newStateID>
    <newState>Exam Delivered To Candidate</newState>
    <changeDate>09 May 2014 09:33:14:82</changeDate>
  </stateChange>
  <stateChange>
    <newStateID>9</newStateID>
    <newState>Exam Submitted</newState>
    <changeDate>09 May 2014 09:33:14:82</changeDate>
  </stateChange>
  <stateChange>
    <newStateID>10</newStateID>
    <newState>Exam Voided</newState>
    <changeDate>23 Jun 2014 15:30:00:00</changeDate>
    <information>
      <stateChangeInformation xmlns="">
        <type>void</type>
        <reason>4</reason>
        <description>Voided by OCR after discussion with the centre.</description>
      </stateChangeInformation>
    </information>
  </stateChange>
</exam>'
where id = 33752

update WAREHOUSE_ExamSessionTable
set ExamStateChangeAuditXml = '<exam>
  <stateChange>
    <newStateID>1</newStateID>
    <newState>Exam Scheduled (but not created)</newState>
    <changeDate>04 Dec 2013 14:56:30:15</changeDate>
  </stateChange>
  <stateChange>
    <newStateID>2</newStateID>
    <newState>Exam Scheduled (but not downloaded)</newState>
    <changeDate>05 Dec 2013 00:02:58:67</changeDate>
  </stateChange>
  <stateChange>
    <newStateID>3</newStateID>
    <newState>Exam Scheduled (but locked)</newState>
    <changeDate>05 Dec 2013 00:03:14:51</changeDate>
    <information />
  </stateChange>
  <stateChange>
    <newStateID>4</newStateID>
    <newState>Exam Ready</newState>
    <changeDate>06 Dec 2013 00:00:03:46</changeDate>
    <information />
  </stateChange>
  <stateChange>
    <newStateID>3</newStateID>
    <newState>Exam Scheduled (but locked)</newState>
    <changeDate>07 Dec 2013 00:00:10:71</changeDate>
    <information />
  </stateChange>
  <stateChange>
    <newStateID>18</newStateID>
    <newState>Awaiting Upload</newState>
    <changeDate>07 Dec 2013 00:00:11:93</changeDate>
    <information />
  </stateChange>
  <stateChange>
    <newStateID>10</newStateID>
    <newState>Exam Voided</newState>
    <changeDate>14 Dec 2013 00:00:30:54</changeDate>
    <information />
  </stateChange>
  <stateChange>
    <newStateID>4</newStateID>
    <newState>Exam Ready</newState>
    <changeDate>09 May 2014 09:34:55:57</changeDate>
  </stateChange>
  <stateChange>
    <newStateID>6</newStateID>
    <newState>Exam Delivered To Candidate</newState>
    <changeDate>09 May 2014 09:34:55:57</changeDate>
  </stateChange>
  <stateChange>
    <newStateID>9</newStateID>
    <newState>Exam Submitted</newState>
    <changeDate>09 May 2014 09:34:55:57</changeDate>
  </stateChange>
  <stateChange>
    <newStateID>10</newStateID>
    <newState>Exam Voided</newState>
    <changeDate>23 Jun 2014 15:30:00:00</changeDate>
    <information>
      <stateChangeInformation xmlns="">
        <type>void</type>
        <reason>4</reason>
        <description>Voided by OCR after discussion with the centre.</description>
      </stateChangeInformation>
    </information>
  </stateChange>
</exam>'
where id = 33753

update WAREHOUSE_ExamSessionTable
set ExamStateChangeAuditXml = '<exam>
  <stateChange>
    <newStateID>1</newStateID>
    <newState>Exam Scheduled (but not created)</newState>
    <changeDate>04 Dec 2013 14:56:30:15</changeDate>
  </stateChange>
  <stateChange>
    <newStateID>2</newStateID>
    <newState>Exam Scheduled (but not downloaded)</newState>
    <changeDate>05 Dec 2013 00:02:58:67</changeDate>
  </stateChange>
  <stateChange>
    <newStateID>3</newStateID>
    <newState>Exam Scheduled (but locked)</newState>
    <changeDate>05 Dec 2013 00:03:14:52</changeDate>
    <information />
  </stateChange>
  <stateChange>
    <newStateID>4</newStateID>
    <newState>Exam Ready</newState>
    <changeDate>06 Dec 2013 00:00:03:46</changeDate>
    <information />
  </stateChange>
  <stateChange>
    <newStateID>3</newStateID>
    <newState>Exam Scheduled (but locked)</newState>
    <changeDate>07 Dec 2013 00:00:10:71</changeDate>
    <information />
  </stateChange>
  <stateChange>
    <newStateID>18</newStateID>
    <newState>Awaiting Upload</newState>
    <changeDate>07 Dec 2013 00:00:11:93</changeDate>
    <information />
  </stateChange>
  <stateChange>
    <newStateID>10</newStateID>
    <newState>Exam Voided</newState>
    <changeDate>14 Dec 2013 00:00:30:54</changeDate>
    <information />
  </stateChange>
  <stateChange>
    <newStateID>4</newStateID>
    <newState>Exam Ready</newState>
    <changeDate>09 May 2014 09:36:33:96</changeDate>
  </stateChange>
  <stateChange>
    <newStateID>6</newStateID>
    <newState>Exam Delivered To Candidate</newState>
    <changeDate>09 May 2014 09:36:33:96</changeDate>
  </stateChange>
  <stateChange>
    <newStateID>9</newStateID>
    <newState>Exam Submitted</newState>
    <changeDate>09 May 2014 09:36:33:96</changeDate>
  </stateChange>
  <stateChange>
    <newStateID>10</newStateID>
    <newState>Exam Voided</newState>
    <changeDate>23 Jun 2014 15:30:00:00</changeDate>
    <information>
      <stateChangeInformation xmlns="">
        <type>void</type>
        <reason>4</reason>
        <description>Voided by OCR after discussion with the centre.</description>
      </stateChangeInformation>
    </information>
  </stateChange>
</exam>'
where id = 33754

update Warehouse_examsessiontable
set remarkstatus = 0
where keycode in ('ZDZLAYB8','VGS8PFB8','MVS9VXB8','7K8URAB8','G8CW3MB8','LJJU7NB8','JEK5B7B8','SMU5FNB8','3ESSRHB8','5EF5DGB8','PCDU9TB8','XVFX48B8','Y8NAGTB8','HDKULCB8','8JE6ULB8','Z9X9G9B8','4E4MGWB8')

update Warehouse_examsessiontable_shreded
set remarkstatus = 0
where keycode in ('ZDZLAYB8','VGS8PFB8','MVS9VXB8','7K8URAB8','G8CW3MB8','LJJU7NB8','JEK5B7B8','SMU5FNB8','3ESSRHB8','5EF5DGB8','PCDU9TB8','XVFX48B8','Y8NAGTB8','HDKULCB8','8JE6ULB8','Z9X9G9B8','4E4MGWB8')

*/