SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

SELECT
	 WEST.ID
	 ,Track.versionID
	, WSCET.examVersionID
FROM WAREHOUSE_ExamSessionTable as WEST with (NOLOCK)

  inner join UserExamHistoryVersionTrackerTable as Track
  on Track.examSessionID = WEST.ExamSessionID
  
  inner join WAREHOUSE_ScheduledExamsTable as WSCET
  on WSCET.ID = WEST.WAREHOUSEScheduledExamID
  
  
 where WSCET.examVersionID <> Track.versionID
  order by WEST.ID DESC;
  
--where west.StructureXML.value('(/assessmentDetails/assessmentID)[1]', 'int') <> Track.versionID

	--west.StructureXML.value('(/assessmentDetails/assessmentID)[1]', 'int') <> WSCET.examVersionId