USE SQA_ItemBank
	
SET NOCOUNT ON;

CREATE TABLE #BadMarkingType (ProjectID int, PageID NVARCHAR(50), MarkingType nvarchar(100), OriginalPageXML XML, NewPageXML XML, NewMarkingType INT);
INSERT INTO #BadMarkingType
	SELECT ProjectID
		,Project.Items.value('@ID', 'NVARCHAR(50)')
		,Project.Items.value('@MarkingType', 'nvarchar(100)')
		,Project.Items.query('.')
		,Project.Items.query('.')
		,0
	FROM [ProjectTable]
	CROSS APPLY ProjectStructure.nodes('//I') Project(Items)
	WHERE Project.Items.value('@MarkingType', 'NVARCHAR(100)') NOT LIKE N'[0-9]'


DECLARE @MarkingTypes TABLE (ProjectID int, MarkingType nvarchar(100), MarkingTypeValue int);

INSERT INTO @MarkingTypes
	SELECT DISTINCT
		 ProjectTable.ProjectID
		,Def.MarkingType.value('@display', 'nvarchar(100)')
		,Def.MarkingType.value('.', 'int')
	FROM #BadMarkingType
	INNER JOIN ProjectTable
	ON #BadMarkingType.ProjectID = ProjectTable.ProjectID
	CROSS APPLY ProjectMetadata.nodes('/Defaults/StructureAttributes/Attribute[@Name = "MarkingType"]/Value') Def(MarkingType);


UPDATE #BadMarkingType
SET NewMarkingType = MarkingType.MarkingTypeValue
FROM #BadMarkingType
LEFT JOIN @MarkingTypes MarkingType
ON #BadMarkingType.ProjectID = MarkingType.ProjectID
WHERE (#BadMarkingType.MarkingType = MarkingType.MarkingType COLLATE Latin1_General_CI_AS
	OR #BadMarkingType.MarkingType = MarkingType.MarkingType+N'ed' COLLATE Latin1_General_CI_AS)
AND MarkingType.MarkingType IS NOT NULL;

UPDATE #BadMarkingType
SET NewPageXML.modify('replace value of (/I/@MarkingType)[1] with sql:column("NewMarkingType")');

SELECT ProjectID AS ProjectID
	,CAST(ProjectStructure AS NVARCHAR(MAX)) ProjectStructure
INTO #ProjectStructure
FROM ProjectTable
WHERE ProjectID IN (SELECT ProjectID FROM #BadMarkingType);


/*SELECT * FROM #BadMarkingType
	inner join ProjectTable as plt
	on plt.ProjectID = #BadMarkingType.ProjectID
order by #BadMarkingType.ProjectID */
/*RUN TO HERE*/

DECLARE PageReplacement CURSOR FOR
	SELECT ProjectID, CAST(OriginalPageXML AS NVARCHAR(MAX)), CAST(NewPageXML AS NVARCHAR(MAX)) FROM #BadMarkingType;

OPEN PageReplacement;

DECLARE @ProjectID INT, @OriginalPageXML NVARCHAR(MAX), @NewPageXML NVARCHAR(MAX);

FETCH NEXT FROM PageReplacement INTO @ProjectID, @OriginalPageXML, @NewPageXML;

WHILE @@FETCH_STATUS = 0
BEGIN
	UPDATE #ProjectStructure
	SET ProjectStructure = REPLACE(ProjectStructure, @OriginalPageXML, @NewPageXML)
	WHERE ProjectID = @ProjectID;
	
	FETCH NEXT FROM PageReplacement INTO @ProjectID, @OriginalPageXML, @NewPageXML;
END

CLOSE PageReplacement;
DEALLOCATE PageReplacement;

/*
SELECT ProjectID AS ID, 
CAST(ProjectStructure AS XML) AS ProjectStructure 
FROM #ProjectStructure;
*/

update ProjectTable
set ProjectStructure = PS.ProjectStructure 
from ProjectTable

	inner join #ProjectStructure as PS
	on PS.ProjectID = ProjectTable.projectid
	
where PS.ProjectID = ProjectTable.projectid


DROP TABLE #BadMarkingType;
DROP TABLE #ProjectStructure;


