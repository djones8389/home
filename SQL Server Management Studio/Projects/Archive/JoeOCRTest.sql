select west.ID,  west.ExamSessionID, west.KeyCode, userMark, userPercentage, west.resultData, wests.structureXml, west.resultDataFull

	from WAREHOUSE_ExamSessionTable_Shreded as wests
	
	inner join WAREHOUSE_ExamSessionTable as west
	on west.ID = wests.examSessionId
	
		Where  west.ID = 82151
		
	
select * from WAREHOUSE_ExamSessionTable_ShreddedItems_Mark where ExamSessionID = 82151
select * from WAREHOUSE_ExamSessionItemResponseTable where WAREHOUSEExamSessionID = 82151
--select * from ExamSessionItemResponseTable where ExamSessionID = 88202

update WAREHOUSE_ExamSessionTable
set StructureXML = '<assessmentDetails>
  <assessmentID>2138</assessmentID>
  <qualificationID>152</qualificationID>
  <qualificationName>OCR Functional Skills in English Writing Level 2</qualificationName>
  <qualificationReference>FSEnglishWritingL2</qualificationReference>
  <assessmentGroupName>Level 2 Functional Skills English - Writing Task</assessmentGroupName>
  <assessmentGroupID>109</assessmentGroupID>
  <assessmentGroupReference>09499/5</assessmentGroupReference>
  <assessmentName>Level 2 Functional Skills English Writing - ONDEMAND - BW17</assessmentName>
  <validFromDate>12 Aug 2012</validFromDate>
  <expiryDate>17 Nov 2022</expiryDate>
  <startTime>00:00:00</startTime>
  <endTime>23:59:59</endTime>
  <duration>55</duration>
  <defaultDuration>55</defaultDuration>
  <scheduledDuration>
    <value>55</value>
    <reason></reason>
  </scheduledDuration>
  <sRBonus>0</sRBonus>
  <sRBonusMaximum>14</sRBonusMaximum>
  <externalReference>FSUM_ONDEMAND_0000_m_09499_5_BW17</externalReference>
  <passLevelValue>18</passLevelValue>
  <passLevelType>0</passLevelType>
  <status>2</status>
  <testFeedbackType>
    <passFail>0</passFail>
    <percentageMark>0</percentageMark>
    <allowSummaryFeedback>0</allowSummaryFeedback>
    <summaryFeedbackType>1</summaryFeedbackType>
    <itemSummary>0</itemSummary>
    <itemReview>0</itemReview>
    <itemReviewCorrectAnswer>0</itemReviewCorrectAnswer>
    <itemFeedback>0</itemFeedback>
    <printableSummary>0</printableSummary>
    <candidateDetails>0</candidateDetails>
    <feedbackByReference>0</feedbackByReference>
    <allowFeedbackDuringExam>0</allowFeedbackDuringExam>
    <allowFeedbackDuringSummary>0</allowFeedbackDuringSummary>
  </testFeedbackType>
  <itemFeedback>0</itemFeedback>
  <allowCalculator>0</allowCalculator>
  <lastInUseDate>05 Feb 2014 11:12:42</lastInUseDate>
  <completedScriptReview>0</completedScriptReview>
  <assessment currentSection="0" totalTime="0" currentTime="0">
    <intro id="0" name="Introduction" currentItem="">
      <item id="4209P4490" name="2ENW12017_FS" totalMark="1" version="14" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="1" viewingTime="" flagged="0" />
    </intro>
    <outro id="0" name="Finish" currentItem="" />
    <tools id="0" name="Tools" currentItem="">
      <item id="4209P4488" name="2ENW12017_DOC1" toolName="2ENW12017_DOC1" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="4209P4489" name="2ENW12017_DOC2" toolName="2ENW12017_DOC2" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
    </tools>
    <section id="1" name="Section 1" passLevelValue="0" passLevelType="1" itemsToMark="0" currentItem="" fixed="1">
      <item id="6049P6463" name="Copy of 2ENW12017_INFO1" totalMark="0" version="25" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="0" flagged="0" LO="Information page only (no marks)" unit="3" />
      <item id="6049P6377" name="2ENW12017_Q01" totalMark="21" version="20" markingType="1" markingState="0" userMark="0" markerUserMark="" userAttempted="1" viewingTime="0" flagged="0" unit="3" quT="11" MarkedMetadata_0="4" MarkedMetadata_1="3" MarkedMetadata_2="3" MarkedMetadata_3="3" MarkedMetadata_4="4" MarkedMetadata_5="4" />
      <item id="6049P6378" name="2ENW12017_Q02" totalMark="13" version="19" markingType="1" markingState="0" userMark="0" markerUserMark="" userAttempted="1" viewingTime="0" flagged="0" unit="3" quT="11" MarkedMetadata_0="3" MarkedMetadata_1="2" MarkedMetadata_2="2" MarkedMetadata_3="2" MarkedMetadata_4="2" MarkedMetadata_5="2" />
    </section>
  </assessment>
  <isValid>1</isValid>
  <isPrintable>0</isPrintable>
  <qualityReview>0</qualityReview>
  <numberOfGenerations>1</numberOfGenerations>
  <requiresInvigilation>0</requiresInvigilation>
  <advanceContentDownloadTimespan>24</advanceContentDownloadTimespan>
  <automaticVerification>0</automaticVerification>
  <offlineMode>2</offlineMode>
  <requiresValidation>0</requiresValidation>
  <requiresSecureClient>1</requiresSecureClient>
  <examType>0</examType>
  <advanceDownload>0</advanceDownload>
  <gradeBoundaryData>
    <gradeBoundaries passLevelType="0" adjustedGradeBoundaries="0">
      <grade modifier="lt" value="18" description="Fail" />
      <grade modifier="gt" value="18" description="Fail n" />
      <grade modifier="gt" value="22" description="Pass" userCreated="1" />
    </gradeBoundaries>
  </gradeBoundaryData>
  <autoViewExam>0</autoViewExam>
  <autoProgressItems>0</autoProgressItems>
  <confirmationText>
    <confirmationText>By ticking this box you confirm that the above details are correct and you will adhere to OCR''s code of conduct.</confirmationText>
  </confirmationText>
  <requiresConfirmationCheck>1</requiresConfirmationCheck>
  <allowCloseWithoutSubmit>0</allowCloseWithoutSubmit>
  <useSecureMarker>0</useSecureMarker>
  <annotationVersion>3</annotationVersion>
  <allowPackagingDelivery>1</allowPackagingDelivery>
  <scoreBoundaryData>
    <scoreBoundaries showScoreBoundaryColumn="0">
      <score modifier="lt" value="40" description="Not met" higherBoundarySet="1" />
      <score modifier="lt" value="40" description="Not met" higherBoundarySet="0" />
      <score modifier="gt" value="40" description="Close to met" higherBoundarySet="1" />
      <score modifier="gt" value="40" description="Met" higherBoundarySet="0" />
      <score modifier="gt" value="45" description="Met" higherBoundarySet="1" />
      <score modifier="gt" value="80" description="Exceeded" higherBoundarySet="1" />
    </scoreBoundaries>
  </scoreBoundaryData>
  <showCandidateReportResultColumn>0</showCandidateReportResultColumn>
  <showCandidateReportPercentageColumn>1</showCandidateReportPercentageColumn>
  <certifiedAccessible>0</certifiedAccessible>
  <markingProgressVisible>1</markingProgressVisible>
  <markingProgressMode>0</markingProgressMode>
  <secureClientOperationMode>1</secureClientOperationMode>
  <examDeliverySystemStyleType>delivery</examDeliverySystemStyleType>
  <markingAutoVoidPeriod>365</markingAutoVoidPeriod>
  <showPageRequiresScrollingAlert>1</showPageRequiresScrollingAlert>
  <project ID="4209" />
  <project ID="6002" />
  <project ID="6049">
    <value display="2W1 Candidate has presented relevant ideas and information effectively">0</value>
    <value display="2W2 Candidate has used spelling accurately">1</value>
    <value display="2W3 Candidate has used punctuation accurately">2</value>
    <value display="2W4 Candidate has used grammar accurately">3</value>
    <value display="2W5 Candidate has used an appropriate document format">4</value>
    <value display="2W6 Candidate has used an appropriate style and tone">5</value>
  </project>
</assessmentDetails>', resultData = '<exam passMark="18" passType="0" originalPassMark="18" originalPassType="0" totalMark="34" userMark="0" userPercentage="0" passValue="0" originalPassValue="0" totalTimeSpent="0" closeBoundaryType="0" closeBoundaryValue="0" grade="Fail" originalGrade="Fail">
  <gradeBoundaryData>
    <gradeBoundaries passLevelType="0" adjustedGradeBoundaries="0">
      <grade modifier="lt" value="18" description="Fail" />
      <grade modifier="gt" value="18" description="Fail n" />
      <grade modifier="gt" value="22" description="Pass" userCreated="1" />
    </gradeBoundaries>
  </gradeBoundaryData>
  <section id="1" passMark="0" passType="1" name="Section 1" totalMark="34" userMark="0" userPercentage="0" passValue="1" totalTimeSpent="0" itemsToMark="0">
    <item id="6049P6463" name="Copy of 2ENW12017_INFO1" totalMark="0" userMark="0" actualUserMark="0" markingType="0" markerUserMark="" viewingTime="0" userAttempted="1" version="25" />
    <item id="6049P6377" name="2ENW12017_Q01" totalMark="21" userMark="0" actualUserMark="0" markingType="1" markerUserMark="" viewingTime="0" userAttempted="1" version="20">
      <mark mark="0" learningOutcome="0" displayName="2W1 Candidate has presented relevant ideas and information effectively" maxMark="4" />
      <mark mark="0" learningOutcome="1" displayName="2W2 Candidate has used spelling accurately" maxMark="3" />
      <mark mark="0" learningOutcome="2" displayName="2W3 Candidate has used punctuation accurately" maxMark="3" />
      <mark mark="0" learningOutcome="3" displayName="2W4 Candidate has used grammar accurately" maxMark="3" />
      <mark mark="0" learningOutcome="4" displayName="2W5 Candidate has used an appropriate document format" maxMark="4" />
      <mark mark="0" learningOutcome="5" displayName="2W6 Candidate has used an appropriate style and tone" maxMark="4" />
    </item>
    <item id="6049P6378" name="2ENW12017_Q02" totalMark="13" userMark="0" actualUserMark="0" markingType="1" markerUserMark="" viewingTime="0" userAttempted="1" version="19">
      <mark mark="0" learningOutcome="0" displayName="2W1 Candidate has presented relevant ideas and information effectively" maxMark="3" />
      <mark mark="0" learningOutcome="1" displayName="2W2 Candidate has used spelling accurately" maxMark="2" />
      <mark mark="0" learningOutcome="2" displayName="2W3 Candidate has used punctuation accurately" maxMark="2" />
      <mark mark="0" learningOutcome="3" displayName="2W4 Candidate has used grammar accurately" maxMark="2" />
      <mark mark="0" learningOutcome="4" displayName="2W5 Candidate has used an appropriate document format" maxMark="2" />
      <mark mark="0" learningOutcome="5" displayName="2W6 Candidate has used an appropriate style and tone" maxMark="2" />
    </item>
  </section>
</exam>', resultDataFull = '<assessmentDetails>
  <assessmentID>2138</assessmentID>
  <qualificationID>152</qualificationID>
  <qualificationName>OCR Functional Skills in English Writing Level 2</qualificationName>
  <qualificationReference>FSEnglishWritingL2</qualificationReference>
  <assessmentGroupName>Level 2 Functional Skills English - Writing Task</assessmentGroupName>
  <assessmentGroupID>109</assessmentGroupID>
  <assessmentGroupReference>09499/5</assessmentGroupReference>
  <assessmentName>Level 2 Functional Skills English Writing - ONDEMAND - BW17</assessmentName>
  <validFromDate>12 Aug 2012</validFromDate>
  <expiryDate>17 Nov 2022</expiryDate>
  <startTime>00:00:00</startTime>
  <endTime>23:59:59</endTime>
  <duration>55</duration>
  <defaultDuration>55</defaultDuration>
  <scheduledDuration>
    <value>55</value>
    <reason />
  </scheduledDuration>
  <sRBonus>0</sRBonus>
  <sRBonusMaximum>14</sRBonusMaximum>
  <externalReference>FSUM_ONDEMAND_0000_m_09499_5_BW17</externalReference>
  <passLevelValue>18</passLevelValue>
  <passLevelType>0</passLevelType>
  <status>2</status>
  <testFeedbackType>
    <passFail>0</passFail>
    <percentageMark>0</percentageMark>
    <allowSummaryFeedback>0</allowSummaryFeedback>
    <summaryFeedbackType>1</summaryFeedbackType>
    <itemSummary>0</itemSummary>
    <itemReview>0</itemReview>
    <itemReviewCorrectAnswer>0</itemReviewCorrectAnswer>
    <itemFeedback>0</itemFeedback>
    <printableSummary>0</printableSummary>
    <candidateDetails>0</candidateDetails>
    <feedbackByReference>0</feedbackByReference>
    <allowFeedbackDuringExam>0</allowFeedbackDuringExam>
    <allowFeedbackDuringSummary>0</allowFeedbackDuringSummary>
  </testFeedbackType>
  <itemFeedback>0</itemFeedback>
  <allowCalculator>0</allowCalculator>
  <lastInUseDate>Nov 27 2014  9:27AM</lastInUseDate>
  <completedScriptReview>0</completedScriptReview>
  <assessment currentSection="0" totalTime="0" currentTime="0" originalPassMark="18" originalPassType="0" passMark="18" passType="0" totalMark="34" userMark="0" userPercentage="0" passValue="0" originalGrade="Fail">
    <intro id="0" name="Introduction" currentItem="">
      <item id="4209P4490" name="2ENW12017_FS" totalMark="1" version="14" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
    </intro>
    <outro id="0" name="Finish" currentItem="" />
    <tools id="0" name="Tools" currentItem="">
      <item id="4209P4488" name="2ENW12017_DOC1" toolName="2ENW12017_DOC1" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="4209P4489" name="2ENW12017_DOC2" toolName="2ENW12017_DOC2" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
    </tools>
    <section id="1" name="Section 1" passLevelValue="0" passLevelType="1" itemsToMark="0" currentItem="" fixed="1" totalMark="34" userMark="0" userPercentage="0" passValue="1">
      <item id="6049P6463" name="Copy of 2ENW12017_INFO1" totalMark="0" version="25" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="1" viewingTime="0" flagged="0" LO="Information page only (no marks)" unit="3" />
      <item id="6049P6377" name="2ENW12017_Q01" totalMark="21" version="20" markingType="1" markingState="0" userMark="0" markerUserMark="" userAttempted="1" viewingTime="0" flagged="0" unit="3" quT="11" MarkedMetadata_0="4" MarkedMetadata_1="3" MarkedMetadata_2="3" MarkedMetadata_3="3" MarkedMetadata_4="4" MarkedMetadata_5="4" />
      <item id="6049P6378" name="2ENW12017_Q02" totalMark="13" version="19" markingType="1" markingState="0" userMark="0" markerUserMark="" userAttempted="1" viewingTime="0" flagged="0" unit="3" quT="11" MarkedMetadata_0="3" MarkedMetadata_1="2" MarkedMetadata_2="2" MarkedMetadata_3="2" MarkedMetadata_4="2" MarkedMetadata_5="2" />
    </section>
  </assessment>
  <isValid>1</isValid>
  <isPrintable>0</isPrintable>
  <qualityReview>0</qualityReview>
  <numberOfGenerations>1</numberOfGenerations>
  <requiresInvigilation>0</requiresInvigilation>
  <advanceContentDownloadTimespan>24</advanceContentDownloadTimespan>
  <automaticVerification>0</automaticVerification>
  <offlineMode>2</offlineMode>
  <requiresValidation>0</requiresValidation>
  <requiresSecureClient>1</requiresSecureClient>
  <examType>0</examType>
  <advanceDownload>0</advanceDownload>
  <gradeBoundaryData>
    <gradeBoundaries passLevelType="0" adjustedGradeBoundaries="0">
      <grade modifier="lt" value="18" description="Fail" />
      <grade modifier="gt" value="18" description="Fail n" />
      <grade modifier="gt" value="22" description="Pass" userCreated="1" />
    </gradeBoundaries>
  </gradeBoundaryData>
  <autoViewExam>0</autoViewExam>
  <autoProgressItems>0</autoProgressItems>
  <confirmationText>
    <confirmationText>By ticking this box you confirm that the above details are correct and you will adhere to OCR''s code of conduct.</confirmationText>
  </confirmationText>
  <requiresConfirmationCheck>1</requiresConfirmationCheck>
  <allowCloseWithoutSubmit>0</allowCloseWithoutSubmit>
  <useSecureMarker>0</useSecureMarker>
  <annotationVersion>3</annotationVersion>
  <allowPackagingDelivery>1</allowPackagingDelivery>
  <scoreBoundaryData>
    <scoreBoundaries showScoreBoundaryColumn="0">
      <score modifier="lt" value="40" description="Not met" higherBoundarySet="1" />
      <score modifier="lt" value="40" description="Not met" higherBoundarySet="0" />
      <score modifier="gt" value="40" description="Close to met" higherBoundarySet="1" />
      <score modifier="gt" value="40" description="Met" higherBoundarySet="0" />
      <score modifier="gt" value="45" description="Met" higherBoundarySet="1" />
      <score modifier="gt" value="80" description="Exceeded" higherBoundarySet="1" />
    </scoreBoundaries>
  </scoreBoundaryData>
  <showCandidateReportResultColumn>0</showCandidateReportResultColumn>
  <showCandidateReportPercentageColumn>1</showCandidateReportPercentageColumn>
  <certifiedAccessible>0</certifiedAccessible>
  <markingProgressVisible>1</markingProgressVisible>
  <markingProgressMode>0</markingProgressMode>
  <secureClientOperationMode>1</secureClientOperationMode>
  <examDeliverySystemStyleType>delivery</examDeliverySystemStyleType>
  <markingAutoVoidPeriod>365</markingAutoVoidPeriod>
  <showPageRequiresScrollingAlert>1</showPageRequiresScrollingAlert>
  <project ID="4209" />
  <project ID="6002" />
  <project ID="6049">
    <value display="2W1 Candidate has presented relevant ideas and information effectively">0</value>
    <value display="2W2 Candidate has used spelling accurately">1</value>
    <value display="2W3 Candidate has used punctuation accurately">2</value>
    <value display="2W4 Candidate has used grammar accurately">3</value>
    <value display="2W5 Candidate has used an appropriate document format">4</value>
    <value display="2W6 Candidate has used an appropriate style and tone">5</value>
  </project>
</assessmentDetails>'
where ID = 82151

      BEGIN TRANSACTION;
            INSERT INTO WAREHOUSE_ExamSessionTable_ShreddedItems_Mark
            (     ExamSessionID
               ,ItemID
               ,Mark
               ,LearningOutcome
               ,DisplayText
               ,MaxMark)
                  SELECT  WAREHOUSE_ExamSessionTable.ID AS [ExamSessionID]
                           ,Result.Item.value('@id', 'varchar(15)') AS [ItemID]
                           ,Item.Mark.value('@mark', 'decimal(6, 3)') AS [Mark]
                           ,Item.Mark.value('@learningOutcome', 'int') AS [LearningOutcome]
                           ,Item.Mark.value('@displayName', 'nvarchar(200)') AS [DisplayName]
                           ,Item.Mark.value('@maxMark', 'decimal(6, 3)') AS [MaxMark]
                  FROM   WAREHOUSE_ExamSessionTable
                  CROSS APPLY WAREHOUSE_ExamSessionTable.ResultData.nodes('exam/section/item') Result(Item)
                  CROSS APPLY Result.Item.nodes('mark') Item(Mark)
                  WHERE   WAREHOUSE_ExamSessionTable.ID = 82151;
      COMMIT TRANSACTION;
