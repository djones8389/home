--CREATE TABLE ZeroMarkBackup
--	(
--		ESID INT
--		, StructureXML XML
--		, Date datetime
--	)
DECLARE @DynamicSQL NVARCHAR(MAX) = '';

DECLARE @ExamSessions table
(
	ESID INT
)

insert @ExamSessions(ESID)
select DISTINCT est.ID
	
	from ExamSessionTable as est WITH (NOLOCK)
	
	cross apply structurexml.nodes('assessmentDetails/assessment/section/item') s(i)
	
	INNER join ExamSessionItemResponseTable as esirt WITH (NOLOCK)
	on esirt.ExamSessionID = est.ID
		and itemResponseData.value('(/p/@id)[1]', 'nvarchar(14)') = s.i.value('@id[1]', 'nvarchar(14)')
		
	inner join UserTable as ut WITH (NOLOCK)
	on ut.id = est.UserID
	
	LEFT join CumulativeMarkingTable as CMT (NOLOCK)
	on CMT.ExamSessionId = est.ID 
	
	where		--Find cumulative marking = 0 scripts (computer marked)--
		 (
		 examState in (15, 16)
		 and esirt.ItemResponseData.exist('p[@um > 0]') = 1
		 and MarkerResponseData.exist('entries/entry[assignedMark=0.000]') = 1
		 and CMT.CumulativeUserMarks = 0 and CMT.MarkingProgress IS NULL
		 )
			
		 or		--Find userAttempted = 0 scripts (human marked)--
		 (
		 examState in (16, 15)
		 and itemResponseData.value('(/p/@ua)[1]', 'bit') !=  s.i.value('@userAttempted[1]', 'bit')
		 )
		 
		and est.id > 108693 --Filter out old exams
 
--Select * from @ExamSessions

--Keep as a backup--
INSERT INTO ZeroMarkBackup 
SELECT ID, StructureXML, GETDATE()
FROM ExamSessionTable (NOLOCK)
WHERE ID IN (Select ESID from @ExamSessions);


		SELECT	 ID
				,CAST(StructureXML AS XML) AS [StructureXML]
		INTO	 #ExamXML
		FROM	 ExamSessionTable (NOLOCK)
		WHERE	 ID IN (Select ESID from @ExamSessions);


		DECLARE STRUCTUREXML CURSOR FOR

		select
			 ExamSessionID
			,ItemID
			,ItemResponseData.value('(/p/@um)[1]', 'decimal(6,4)') as userMark
			,ItemResponseData.value('(/p/@ua)[1]', 'tinyint') as userAttempted
			From ExamSessionItemResponseTable
			
		WHERE	 ExamSessionID IN (Select ESID from @ExamSessions);


		DECLARE @ESID int, @ItemID nvarchar(12), @userMark decimal(6,4), @userAttempted tinyint;

		OPEN STRUCTUREXML;

		FETCH NEXT FROM STRUCTUREXML INTO @ESID, @ItemID, @userMark, @userAttempted;

		WHILE @@FETCH_STATUS = 0

		BEGIN

		update #ExamXML
		set StructureXML.modify('replace value of (/assessmentDetails/assessment/section/item[@id = sql:variable("@ItemID")]/@userMark)[1] with sql:variable("@userMark")')
		where ID = @ESID

		update #ExamXML
		set StructureXML.modify('replace value of (/assessmentDetails/assessment/section/item[@id = sql:variable("@ItemID")]/@userAttempted)[1] with sql:variable("@userAttempted")')
		where ID = @ESID


		FETCH NEXT FROM STRUCTUREXML INTO @ESID, @ItemID, @userMark, @userAttempted

		END

		CLOSE STRUCTUREXML;
		DEALLOCATE STRUCTUREXML;



select
	 ESIRT.ExamSessionID
	, ItemID
	, MarkerResponseData
INTO #MarkerResponseXML
from ExamSessionItemResponseTable as ESIRT (NOLOCK)

where ExamSessionID IN (Select ESID from @ExamSessions);


DECLARE MarkerResponse CURSOR FOR
select
	 EST.id 
	, a.b.value('@id[1]','nvarchar(12)') as ItemID
    , round(a.b.value('@totalMark[1]','tinyint') * ItemResponseData.value('(/p/@um)[1]', 'decimal(6,3)'), 0) as 'ActualUserMark'
from ExamSessionTable as EST (NOLOCK)

cross apply StructureXML.nodes('/assessmentDetails/assessment/section/item') a(b)

INNER JOIN ExamSessionItemResponseTable as ESIRT (NOLOCK)
on ESIRT.ExamSessionID = est.id	

	and ItemID = a.b.value('@id[1]','nvarchar(12)')

where ExamSessionID IN (Select ESID from @ExamSessions);

DECLARE @ItemESID int, @ItemItemID nvarchar(12), @ActualUserMark decimal(6,3)

OPEN MarkerResponse

FETCH NEXT FROM MarkerResponse INTO @ItemItemID, @ItemID, @ActualUserMark

WHILE @@FETCH_STATUS = 0

BEGIN

UPDATE #MarkerResponseXML
SET MarkerResponseData.modify('replace value of (/entries/entry[userId = -1]/assignedMark/text())[1] with sql:variable("@ActualUserMark")')
WHERE ExamSessionID = @ItemESID
	AND ItemID = @ItemItemID
	
FETCH NEXT FROM MarkerResponse INTO @ItemItemID, @ItemID, @ActualUserMark

END
CLOSE MarkerResponse
DEALLOCATE  MarkerResponse



update ExamSessionTable
set StructureXML = B.StructureXML
from ExamSessionTable as A
inner join #ExamXML as B
on A.ID = B.ID
where A.ID = B.ID
	and A.ID IN (Select ESID from @ExamSessions);


update ExamSessionItemResponseTable
set MarkerResponseData = B.MarkerResponseData
FROM ExamSessionItemResponseTable A
inner join #MarkerResponseXML as B
on A.ExamSessionID = B.ExamSessionID
	and A.ItemID = B.ItemID
	and A.ExamSessionID IN (Select ESID from @ExamSessions);


SELECT @DynamicSQL += CHAR(13) +
	'
DECLARE	@return_value int

EXEC	@return_value = [dbo].[sa_CANDIDATEEXAMSTATEMANAGEMENTSERVICE_UpdateCumulativeMarkingProgress_sp]
		@ExamSessionId =' + CONVERT(NVARCHAR(10), ESID) + ' 

SELECT	''Return Value'' = @return_value

GO
'
from @ExamSessions

PRINT(@DynamicSQL)
--EXEC(@DynamicSQL)


DROP TABLE #ExamXML;
DROP TABLE #MarkerResponseXML;