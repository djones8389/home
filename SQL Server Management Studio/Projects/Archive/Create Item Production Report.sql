--select 
	
--	XMLSTORE.value('(/report/item/@QualificationName)[1]', 'nvarchar(max)') as Subject
--    ,XMLSTORE.query('data(/report/item[1]/@QualificationName)') 
--    ,XMLSTORE
-- from DJXML


----cross apply XMLSTORE.nodes()

--where UniqueIdentifier = 2



select 
	'Centre: All Centres' as 'Centre'
	,r.i.value('@QualificationName','nvarchar(max)')  as 'Subject'
	,r.i.value('@QuestionName','nvarchar(max)') as 'Item Name'
	,r.i.value('@Id','nvarchar(max)')  as 'Item ID'
	,r.i.value('@QuestionStem','nvarchar(max)') as 'Question Text'
	,r.i.value('@QuestionTypeId','nvarchar(max)') as 'Question Type'
	,r.i.value('@Status','nvarchar(max)') as 'Status'
	,r.i.value('@CreatorFirstName','nvarchar(max)') + ' ' +  r.i.value('@CreatorFirstName','nvarchar(max)') as 'Created by'
	,r.i.value('@CreationDate','nvarchar(max)') as 'Created at'
	,r.i.value('@ModifierFirstName','nvarchar(max)') + ' ' +  r.i.value('@ModifierLastName','nvarchar(max)') as 'Last Edited by'
	,r.i.value('@LastModifiedDate','nvarchar(max)') as 'Last Edited at'
	,r.i.value('@LastModifiedDate','nvarchar(max)') as 'Last Edited at'
	,i.c.query(('.')) as Comments
 from DJXML


cross apply XMLSTORE.nodes('report/item') r(i)
cross apply r.i.nodes('comments') i(c)

where UniqueIdentifier = 2