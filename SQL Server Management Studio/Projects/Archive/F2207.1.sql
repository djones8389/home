SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

DECLARE @MyTable TABLE (projectID smallint, FolderCount SMALLINT, ItemCount smallint)
INSERT @MyTable(projectID, FolderCount, ItemCount)


SELECT A.ProjectID
,	 B.NumberofPages
,    A.NumberOfFolders
FROM
(

SELECT  
	ProjectListTable.id as ProjectID
	, COUNT(pro.fol.value('@ID','int')) AS NumberOfFolders
from ProjectListTable (NOLOCK)

	cross apply ProjectStructureXml.nodes('Pro//Fol') pro(fol)

	GROUP BY ProjectListTable.id

EXCEPT

select  
	ProjectListTable.id as ProjectID
	, COUNT(pro.fol.value('@ID','int')) AS NumberOfFolders
from ProjectListTable (NOLOCK)

	cross apply ProjectStructureXml.nodes('Pro/Rec//Fol') pro(fol)

	GROUP BY ProjectListTable.id
) A
	
LEFT JOIN (

select  
	ProjectListTable.id as ProjectID
	, COUNT(pro.pag.value('@ID','int')) AS NumberofPages
from ProjectListTable (NOLOCK)

cross apply ProjectStructureXml.nodes('Pro//Pag') pro(pag)
		GROUP BY ProjectListTable.id

EXCEPT

select  
	ProjectListTable.id as ProjectID
	, COUNT(pro.pag.value('@ID','int')) AS NumberofPages
from ProjectListTable (NOLOCK)

cross apply ProjectStructureXml.nodes('Pro/Rec//Pag') pro(pag)

		GROUP BY ProjectListTable.id
) B

ON A.ProjectID = B.ProjectID



SELECT * 
FROM @MyTable

SELECT projectID, CAST(	CAST (FolderCount AS float)/CAST(ItemCount AS float) AS DECIMAL(6,3)) AS AvgItemsPerFolder 
FROM @MyTable

--SELECT * 
--FROM @MyTable


--DECLARE @a SMALLINT = 7512
--DECLARE @b SMALLINT = 759

--SELECT @a/@b

--9.897233201581028