SELECT D.Name, F.Name, F.physical_name, *
FROM sys.master_files as F
inner join sys.databases as D
on D.database_id = F.database_id 
where D.name in ('ItemBank','SecureAssess')
	and type_desc = 'LOG'


--C:\Program Files\Microsoft SQL Server\MSSQL10_50.SADB\MSSQL\DATA\ItemBank_log.ldf
--C:\Program Files\Microsoft SQL Server\MSSQL10_50.SADB\MSSQL\DATA\SecureAssess_log.ldf
--NAME = tempdev

DECLARE @NewLogLocation nvarchar(20) = 'D:\'

DECLARE @AlterLocation nvarchar(MAX) = '';
DECLARE @setOffline nvarchar(MAX) = '';
DECLARE @setOnline nvarchar(MAX) = '';


DECLARE @Tables TABLE(DBName nvarchar(100), LogFileName nvarchar(100))
INSERT @Tables
SELECT D.Name, F.Name--, F.physical_name
FROM sys.master_files as F
inner join sys.databases as D
on D.database_id = F.database_id 
where D.name in ('ItemBank','SecureAssess')
	and type_desc = 'LOG'

SELECT @AlterLocation += char(13) + 'use master;  ALTER DATABASE [' + DBName + '] MODIFY FILE (NAME = ' + LogFileName  + ', Filename=''' + @NewLogLocation + LogFileName + '.ldf'');'
FROM @Tables 

SELECT @setOffline += char(13) +  'use master;  ALTER DATABASE [' + DBName + '] SET OFFLINE;'
FROM @Tables 

SELECT @setOnline += char(13) +  'use master;  ALTER DATABASE [' + DBName + '] SET ONLINE;'
FROM @Tables 


PRINT(@AlterLocation)
PRINT(@setOffline)
--EXEC(@AlterLocation);
--EXEC(@setOffline);

PRINT(@setOnline)


--Change LOG default location on the server?

--USE master;
--GO
--ALTER DATABASE tempdb 
--MODIFY FILE (NAME = tempdev, FILENAME = 'E:\SQLData\tempdb.mdf');
--GO
--ALTER DATABASE tempdb 
--MODIFY FILE (NAME = templog, FILENAME = 'F:\SQLLog\templog.ldf');
--GO

use master;  ALTER DATABASE [ItemBank] SET ONLINE;use master;  ALTER DATABASE [SecureAssess] SET ONLINE;



use master;  ALTER DATABASE [ItemBank] MODIFY FILE (NAME = ItemBank_log, Filename='D:\ItemBank_log.ldf');use master;  ALTER DATABASE [SecureAssess] MODIFY FILE (NAME = SecureAssess_log, Filename='D:\SecureAssess_log.ldf');