USE UAT_EAL_SecureAssess

SELECT EST.ID, EST.KeyCode, EST.examState, UT.Forename, UT.Surname, CT.CentreName, SCET.examName
	, ESIRT.*
  FROM ExamSessionTable as EST

  Inner Join ScheduledExamsTable as SCET
  on SCET.ID = EST.ScheduledExamID
  
  Inner Join UserTable as UT
  on UT.ID = EST.UserID

  Inner Join CentreTable as CT
  on CT.ID = SCET.CentreID
  
  Inner Join ExamSessionItemResponseTable as ESIRT
  on ESIRT.ExamSessionID = EST.ID
  
where CandidateRef = '70793550'