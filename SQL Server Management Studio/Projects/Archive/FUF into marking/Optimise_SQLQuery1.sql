USE AAT_SecureAssess_LIVE_28_01_2015
--select top 1 * from ExamSessionTable where examState = 16
--Populate @ID and @ItemESID accordingly once exam is in Warehouse--

declare @Keycode nvarchar(10) = 'Y8T2WZA6';
declare @resultData xml;
declare @resultDataFull xml;
declare @structureXML xml;

declare @ID int;
declare @ItemESID int;

--Find Exam in Live--


select ExamSessionTable.id, keycode, examstate, CandidateRef, Forename, Surname, ExamSessionItemResponseTable.ItemID 
	  from  ExamSessionTable 

inner join UserTable on usertable.id = ExamSessionTable.UserID
left Join ExamSessionItemResponseTable on ExamSessionItemResponseTable.ExamSessionID = ExamSessionTable.ID

where KeyCode =  @Keycode;


--Void exam for FUF--

update ExamSessionTable
set previousExamState = examState, examState = 10
where KeyCode =  @Keycode;


--Find Exam in Warehouse--

--select id, ExamSessionID, keycode, StructureXML, resultData, resultDataFull  
--      from  Warehouse_ExamSessionTable 
--           where KeyCode =  @Keycode;


SET @ResultDataFull = (Select resultDataFull from Warehouse_ExamSessionTable WITH (NOLOCK) where KeyCode = @Keycode);
SET @resultData = (Select resultData from Warehouse_ExamSessionTable WITH (NOLOCK)  where KeyCode = @Keycode);
SET @structureXML = (Select structureXML from Warehouse_ExamSessionTable WITH (NOLOCK) where KeyCode = @Keycode);

--Run FUF,  then replace XML's

update ExamSessionTable
set StructureXML = @structureXML
      , resultData = @resultData
      , resultDataFull = @resultDataFull
where KeyCode =  @Keycode;

--Put exam to State 9 > will go to State 15

update ExamSessionTable
set previousExamState = examState, examState = 9
where KeyCode =  @Keycode;














--declare @autoID int
declare @ExamSessionID int;
declare @ItemID nvarchar(15);
declare @ItemVersion tinyint;
declare @ItemResponseData xml;
declare @MarkerResponseData xml;
declare @ItemMark tinyint;
declare @MarkingIgnored tinyint;
declare @MarkedForReview bit;

declare @ItemResponses table (ID int, ExamSessionID int, ItemID  nvarchar(15), ItemVersion tinyint, ItemResponseData xml, MarkerResponseData xml, ItemMark tinyint, MarkingIgnored bit, MarkedForReview bit)
INSERT @ItemResponses(ID, ExamSessionID, ItemID, ItemVersion, ItemResponseData, MarkerResponseData, ItemMark, MarkingIgnored, MarkedForReview)
	
SELECT	
	 ID, WAREHOUSEExamSessionID, ItemID, ItemVersion, ItemResponseData, MarkerResponseData, ItemMark, MarkingIgnored
from WAREHOUSE_ExamSessionItemResponseTable	
	where WAREHOUSEExamSessionID = 635389










/*
--Update ItemResponses

set identity_insert ExamSessionItemResponseTable ON

Insert into ExamSessionItemResponseTable(ID, ExamSessionID, ItemID, ItemVersion, ItemResponseData, MarkerResponseData, ItemMark, MarkingIgnored)
	SELECT	
		CASE
			WHEN MIN(ID) <= 0
			THEN MIN(ID) - 1
			ELSE -1
		END  ID,  ItemID, ItemVersion, cast(ItemResponseData as nvarchar(max)), MarkerResponseData, ItemMark, MarkingIgnored 
from WAREHOUSE_ExamSessionItemResponseTable  group by   ItemID, ItemVersion, cast(ItemResponseData as nvarchar(max)), MarkerResponseData, ItemMark, MarkingIgnored
where WAREHOUSEExamSessionID = @ItemESID
      
set identity_insert ExamSessionItemResponseTable OFF        
            
select top 10 * from ExamSessionItemResponseTable where ExamSessionID = @ID
select top 10 * from  WAREHOUSE_ExamSessionItemResponseTable
*/