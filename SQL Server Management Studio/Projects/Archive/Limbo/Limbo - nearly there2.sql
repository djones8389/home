-------Testing  #3--------


create function CheckItemResponsesForLimboExams2()
RETURNS @myTable TABLE (NumberOfQuestionsAnswered int,  ID int,  
						KeyCode nvarchar(100), Forename nvarchar(100), Surname nvarchar(100), CandidateRef nvarchar(100))
AS
BEGIN 


DECLARE @tempTable table (ID2 varchar(20), TotalNumberOfQuestions int)

insert @tempTable
	select
			ID,
			StructureXML.value('count(/assessmentDetails/assessment/section/item)[1]', 'int')
	from ExamSessionTable
	where examState = 6


insert @myTable

select 
		
	   COUNT (ExamSessionID) as NumberOfQuestionsAnswered
	   --, @myStorage as TotalNumberOfQuestions
	   --, StructureXML.value('count(/assessmentDetails/assessment/section/item)[1]', 'int') as TotalNumberOfQuestions
	   , EST.ID as ID
	   , EST.KeyCode as Keycode
	   , UT.Forename as Forename
	   , UT.Surname as Surname
	   , UT.CandidateRef as CandidateRef
from ExamSessionItemResponseTable as ESIRT

inner join ExamSessionTable as EST
on EST.ID = ESIRT.ExamSessionID

inner join UserTable as UT
on UT.ID = EST.UserID

WHERE examState = 6

	--and ExamSessionID in (
	--select ID
	--	from ExamSessionTable
	--	)
	
GROUP BY ExamSessionID, EST.ID, EST.KeyCode, UT.Forename, UT.Surname, CandidateRef 
--)

--insert into @myTable(NumberOfQuestionsAnswered, TotalNumberOfQuestions, ID, KeyCode, Forename, Surname, CandidateRef)  values(@myRow)
--HAVING COUNT (ExamSessionID) <> 0
	--order by NumberOfQuestionsAnswered desc
	RETURN --@myTable
END
	
GO



select * from CheckItemResponsesForLimboExams2()


/*
declare @myStorage int = (

select 
StructureXML.value('count(/assessmentDetails/assessment/section/item)[1]', 'int')
from ExamSessionTable
where ID = 1059236

)
*/