-------Testing  #3--------


alter function CheckItemResponsesForLimboExamsTest()
RETURNS @myTable TABLE (NumberOfQuestionsAnswered int,  ID int,  
						KeyCode nvarchar(100), Forename nvarchar(100), Surname nvarchar(100), CandidateRef nvarchar(100))
AS
BEGIN 

DECLARE @tempTable table (ID2 varchar(20), TotalNumberOfQuestions int)

	insert @tempTable
	
	select
			ID as ID2,
			StructureXML.value('count(/assessmentDetails/assessment/section/item)[1]', 'int') as TotalNumberOfQuestions 
	from ExamSessionTable
	where examState = 6


		insert @myTable

		select 
				
			   COUNT (ExamSessionID) as NumberOfQuestionsAnswered
			   , EST.ID as ID
			   , EST.KeyCode as Keycode
			   , UT.Forename as Forename
			   , UT.Surname as Surname
			   , UT.CandidateRef as CandidateRef
		from ExamSessionItemResponseTable as ESIRT

		inner join ExamSessionTable as EST
		on EST.ID = ESIRT.ExamSessionID

		inner join UserTable as UT
		on UT.ID = EST.UserID

		WHERE examState = 6

	
GROUP BY ExamSessionID, EST.ID, EST.KeyCode, UT.Forename, UT.Surname, CandidateRef 

		--insert @myTable
		--select ID2, TotalNumberOfQuestions
		--from @tempTable

RETURN  
END
	
GO



select * from CheckItemResponsesForLimboExamsTest();
