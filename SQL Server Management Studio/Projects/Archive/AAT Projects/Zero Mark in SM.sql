SELECT UniqueResponses.ID, CandidateExamVersions.*
FROM UniqueResponses
INNER JOIN AssignedItemMarks
ON AssignedItemMarks.UniqueResponseId = UniqueResponses.id
INNER JOIN AssignedGroupMarks
ON AssignedGroupMarks.ID = AssignedItemMarks.GroupMarkId
INNER JOIN CandidateResponses
ON CandidateResponses.UniqueResponseID = UniqueResponses.id
INNER JOIN CandidateExamVersions
ON CandidateExamVersions.ID = CandidateResponses.CandidateExamVersionID
WHERE MarkingMethodId = 12
AND AssignedItemMarks.AssignedMark = 0
AND responseData.exist('p[not(@um=0)]') = 1
AND UniqueResponses.id > 1
