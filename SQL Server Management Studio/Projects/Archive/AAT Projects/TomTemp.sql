/*

USE [TempDatabase] 

DECLARE @myExamSessionIDs TABLE (ID INT);

INSERT INTO @myExamSessionIDs
SELECT ID
FROM AAT_SecureAssess..WAREHOUSE_ExamSessionTable
WHERE KeyCode IN ('AULZ6KA6',
'NLEMM8A6',
'8C4YGGA6',
'KAWLCBA6',
'7KD7UHA6',
'H2CTHHA6',
'4CA9BYA6',
'87KQBYA6',
'GSM6HMA6',
'PGTZQXA6',
'S2WUN9A6',
'6AMU8EA6',
'TUSP6KA6',
'CQXKVJA6',
'4YK7ENA6',
'C424C3A6',
'5WPM7EA6',
'PBN8U7A6',
'U35XPPA6',
'YKWWXCA6',
'866EZ7A6',
'GDTAR8A6',
'TYTU4DA6',
'WS2GTQA6',
'H7UAZ9A6',
'K599XDA6',
'LZAATAA6',
'8CJARUA6',
'C3HGXPA6',
'R6VH4CA6',
'D84FSDA6',
'VEG9DQA6',
'7Q9SL9A6',
'WZTZYUA6',
'2JX5URA6',
'D58LXDA6',
'4JPEXSA6',
'QHYWPZA6',
'BKX8LKA6',
'DLC5T7A6',
'SUGTEMA6',
'VXDYLZA6',
'RUWTJGA6',
'K92DFCA6',
'2782KEA6',
'AMSMBFA6',
'DGJ9SZA6',
'JVADV9A6',
'TKPMCZA6',
'KE9M8JA6',
'KP9GKJA6',
'C5QXYVA6',
'48LDAMA6',
'97AUXJA6',
'53FNTHA6',
'V34MMTA6',
'KRELYXA6',
'67AHFRA6',
'E67LTGA6',
'VKS4VHA6',
'83M9J5A6',
'VDPEA6A6',
'NRK5F7A6',
'H93TCKA6',
'NXCHD3A6',
'F2PK5MA6',
'Z9S92SA6',
'K57967A6',
'AQN78QA6',
'PN7PD5A6',
'M5RC89A6',
'6BTJSJA6',
'ZRTCTMA6',
'YBVDGZA6');

CREATE TABLE TempDatabase.[dbo].[WAREHOUSE_ExamSessionTable](
	[ID] [int] NOT NULL,
	[ExamSessionID] [int] NOT NULL,
	[WAREHOUSEScheduledExamID] [int] NOT NULL,
	[WAREHOUSEUserID] [int] NOT NULL,
	[StructureXML] [xml] NULL,
	[MarkerData] [xml] NULL,
	[KeyCode] [varchar](12) NOT NULL,
	[PreviousExamState] [int] NOT NULL,
	[pinNumber] [nvarchar](15) NULL,
	[ExamStateChangeAuditXml] [xml] NOT NULL,
	[resultData] [xml] NULL,
	[resultDataFull] [xml] NULL,
	[warehouseTime] [datetime] NULL,
	[completionDate] [datetime] NULL,
	[CQN] [nvarchar](20) NULL,
	[examIPAuditData] [xml] NULL,
	[appeal] [bit] NULL,
	[reMarkStatus] [int] NOT NULL,
	[reMarkUser] [int] NULL,
	[downloadInformation] [xml] NULL,
	[clientInformation] [xml] NULL,
	[availableForCentreReview] [bit] NULL,
	[ExportToSecureMarker] [bit] NOT NULL,
	[WarehouseExamState] [int] NOT NULL,
	[AllowPackageDelivery] [bit] NOT NULL,
	[ContainsBTLOffice] [bit] NULL,
	[ExportedToIntegration] [bit] NULL,
	[copiedFromExamSessionID] [int] NULL,
	[submissionExported] [bit] NOT NULL,
	[EnableOverrideMarking] [bit] NOT NULL,
	[TargetedForVoid] [xml] NULL,
	[itemMarksUploaded] [bit] NOT NULL,
	[TakenThroughLocalScan] [bit] NOT NULL,
	[LocalScanDownloadDate] [datetime] NULL,
	[LocalScanUploadDate] [datetime] NULL,
	[LocalScanNumPages] [int] NULL,
	[CertifiedForTablet] [bit] NOT NULL,
	[IsProjectBased] [bit] NOT NULL,
	[AssignedMarkerUserID] [int] NULL,
	[AssignedModeratorUserID] [int] NULL
);

INSERT INTO TempDatabase.[dbo].WAREHOUSE_ExamSessionTable
SELECT *
FROM AAT_SecureAssess..WAREHOUSE_ExamSessionTable
WHERE ID IN (1389306,1397361,1397436,1397540,1397573,1384719,1391147,1391689,1391720,1397770,1397849,1398018,1398084,1398086,1398132,1398362,1385597,1385636,1391797,1392141,1392193,1392278,1392294,1399051,1399577,1399695,1392577,1392633,1392876,1393016,1393378,1393383,1393498,1393533,1393622,1393628,1393666,1393705,1399781,1383918,1390485,1396536,1396560,1396647,1396724,1388521,1388570,1388913,1388993,1388994,1389030,1395895,1395954,1388124,1388266,1395350,1395567,1395569,1395591,1395595,1386059,1386102,1386125,1394039,1394110,1394182,1387209,1387233,1387321,1387577,1394982,1382691,1390114,1390123)
GO

CREATE FUNCTION [dbo].[fn_getValueFromResultData]
(
	@resultData XML,
	@requiredField  nvarchar(20)
)
RETURNS nvarchar(50)
WITH SCHEMABINDING
AS
BEGIN

	DECLARE @returnValue nvarchar(50)
	
	SELECT @returnValue = 
	CASE
		WHEN @requiredField = 'GRADE'
			THEN @resultData.value('(/exam/@grade)[1]', 'nvarchar(50)')
		WHEN @requiredField = 'PASSVALUE'
			THEN @resultData.value('(/exam/@passValue)[1]', 'nvarchar(50)')
		WHEN @requiredField = 'CLOSEVALUE'
			THEN @resultData.value('(/exam/@closeValue)[1]', 'nvarchar(50)')
		WHEN @requiredField = 'USERMARK'
			THEN @resultData.value('(/exam/@userMark)[1]', 'nvarchar(50)')
		WHEN @requiredField = 'USERPERCENTAGE'
			THEN @resultData.value('(/exam/@userPercentage)[1]', 'nvarchar(50)')
		WHEN @requiredField = 'ORIGINALGRADE'
			THEN @resultData.value('(/exam/@originalGrade)[1]', 'nvarchar(50)')	
	END
	
	RETURN @returnValue
	
END

GO

CREATE TABLE TempDatabase.[dbo].[WAREHOUSE_ExamSessionTable_Shreded](
	[examSessionId] [int] NOT NULL,
	[structureXml] [xml] NULL,
	[examVersionName] [nvarchar](max) NULL,
	[examVersionRef] [nvarchar](max) NULL,
	[examVersionId] [int] NULL,
	[examName] [nvarchar](max) NULL,
	[examRef] [nvarchar](max) NULL,
	[qualificationid] [int] NULL,
	[qualificationName] [nvarchar](max) NULL,
	[qualificationRef] [nvarchar](max) NULL,
	[resultData] [xml] NULL,
	[submittedDate] [datetime] NULL,
	[originatorId] [int] NULL,
	[centreName] [nvarchar](max) NULL,
	[centreCode] [nvarchar](max) NULL,
	[foreName] [nvarchar](max) NULL,
	[dateOfBirth] [smalldatetime] NULL,
	[gender] [char](1) NULL,
	[candidateRef] [nvarchar](max) NULL,
	[surName] [nvarchar](max) NULL,
	[scheduledDurationValue] [int] NULL,
	[previousExamState] [int] NULL,
	[examStateInformation] [xml] NULL,
	[examResult] [float] NULL,
	[passValue] [bit] NULL,
	[closeValue] [bit] NULL,
	[ExternalReference] [nvarchar](100) NULL,
	[examType] [int] NULL,
	[warehouseTime] [datetime] NULL,
	[CQN] [nvarchar](20) NULL,
	[actualDuration] [int] NULL,
	[appeal] [bit] NULL,
	[reMarkStatus] [int] NULL,
	[qualificationLevel] [int] NULL,
	[centreId] [int] NULL,
	[ULN] [nvarchar](10) NULL,
	[AllowPackageDelivery] [bit] NOT NULL,
	[ExportToSecureMarker] [bit] NOT NULL,
	[ContainsBTLOffice] [bit] NULL,
	[ExportedToIntegration] [bit] NULL,
	[WarehouseExamState] [int] NOT NULL,
	[grade]  AS (case when [previousExamState]=(10) then 'Voided' when NOT isnull([dbo].[fn_getValueFromResultData]([resultData],'GRADE'),'')='' then [dbo].[fn_getValueFromResultData]([resultData],'GRADE') when CONVERT([int],[dbo].[fn_getValueFromResultData]([resultData],'PASSVALUE'),(0))=(1) then 'Pass' when CONVERT([int],[dbo].[fn_getValueFromResultData]([resultData],'PASSVALUE'),(0))=(0) AND CONVERT([int],[dbo].[fn_getValueFromResultData]([resultData],'CLOSEVALUE'),(0))=(1) then 'Close' else 'Fail' end) PERSISTED,
	[userMark]  AS (CONVERT([float],[dbo].[fn_getValueFromResultData]([resultData],'USERMARK'),(0))) PERSISTED,
	[userPercentage]  AS (CONVERT([float],[dbo].[fn_getValueFromResultData]([resultData],'USERPERCENTAGE'),(0))) PERSISTED,
	[adjustedGrade]  AS (case when NOT isnull([dbo].[fn_getValueFromResultData]([resultData],'ORIGINALGRADE'),[dbo].[fn_getValueFromResultData]([resultData],'GRADE'))=[dbo].[fn_getValueFromResultData]([resultData],'GRADE') then case when NOT isnull([dbo].[fn_getValueFromResultData]([resultData],'GRADE'),'')='' then [dbo].[fn_getValueFromResultData]([resultData],'GRADE') when CONVERT([int],[dbo].[fn_getValueFromResultData]([resultData],'PASSVALUE'),(0))=(1) then 'Pass' when CONVERT([int],[dbo].[fn_getValueFromResultData]([resultData],'PASSVALUE'),(0))=(0) AND CONVERT([int],[dbo].[fn_getValueFromResultData]([resultData],'CLOSEVALUE'),(0))=(1) then 'Close' else 'Fail' end else '' end) PERSISTED,
	[language] [nchar](10) NULL,
	[KeyCode] [varchar](12) NOT NULL,
	[TargetedForVoid] [xml] NULL,
	[EnableOverrideMarking] [bit] NOT NULL,
	[originalGrade]  AS (case when NOT isnull([dbo].[fn_getValueFromResultData]([resultData],'ORIGINALGRADE'),[dbo].[fn_getValueFromResultData]([resultData],'GRADE'))=[dbo].[fn_getValueFromResultData]([resultData],'GRADE') then [dbo].[fn_getValueFromResultData]([resultData],'ORIGINALGRADE') else case when [previousExamState]=(10) then 'Voided' when NOT isnull([dbo].[fn_getValueFromResultData]([resultData],'GRADE'),'')='' then [dbo].[fn_getValueFromResultData]([resultData],'GRADE') when CONVERT([int],[dbo].[fn_getValueFromResultData]([resultData],'PASSVALUE'),(0))=(1) then 'Pass' when CONVERT([int],[dbo].[fn_getValueFromResultData]([resultData],'PASSVALUE'),(0))=(0) AND CONVERT([int],[dbo].[fn_getValueFromResultData]([resultData],'CLOSEVALUE'),(0))=(1) then 'Close' else 'Fail' end end) PERSISTED,
	[IsExternal] [bit] NOT NULL,
	[AddressLine1] [nvarchar](100) NULL,
	[AddressLine2] [nvarchar](100) NULL,
	[Town] [nvarchar](100) NULL,
	[County] [nvarchar](70) NULL,
	[Country] [nvarchar](50) NULL,
	[Postcode] [nvarchar](12) NULL,
	[ScoreBoundaryData] [xml] NULL,
	[itemMarksUploaded] [bit] NOT NULL,
	[passMark] [decimal](6, 3) NULL,
	[totalMark] [decimal](6, 3) NULL,
	[passType] [int] NULL,
	[voidJustificationLookupTableId] [int] NULL,
	[automaticVerification] [bit] NULL,
	[resultSampled] [bit] NULL,
	[started] [datetime] NULL,
	[submitted] [datetime] NULL,
	[validFromDate] [datetime] NULL,
	[expiryDate] [datetime] NULL,
	[totalTimeSpent] [int] NULL,
	[defaultDuration] [int] NULL,
	[scheduledDurationReason] [nvarchar](max) NULL,
	[WAREHOUSEScheduledExamID] [int] NULL,
	[WAREHOUSEUserID] [int] NULL,
	[itemDataFull] [xml] NULL,
	[examId] [int] NULL,
	[userId] [int] NULL,
	[TakenThroughLocalScan] [bit] NOT NULL,
	[LocalScanDownloadDate] [datetime] NULL,
	[LocalScanUploadDate] [datetime] NULL,
	[LocalScanNumPages] [int] NULL,
	[CertifiedForTablet] [bit] NOT NULL,
	[IsProjectBased] [bit] NOT NULL,
	[AssignedMarkerUserID] [int] NULL,
	[AssignedModeratorUserID] [int] NULL
);

INSERT INTO TempDatabase.[dbo].WAREHOUSE_ExamSessionTable_Shreded
([examSessionId]
           ,[structureXml]
           ,[examVersionName]
           ,[examVersionRef]
           ,[examVersionId]
           ,[examName]
           ,[examRef]
           ,[qualificationid]
           ,[qualificationName]
           ,[qualificationRef]
           ,[resultData]
           ,[submittedDate]
           ,[originatorId]
           ,[centreName]
           ,[centreCode]
           ,[foreName]
           ,[dateOfBirth]
           ,[gender]
           ,[candidateRef]
           ,[surName]
           ,[scheduledDurationValue]
           ,[previousExamState]
           ,[examStateInformation]
           ,[examResult]
           ,[passValue]
           ,[closeValue]
           ,[ExternalReference]
           ,[examType]
           ,[warehouseTime]
           ,[CQN]
           ,[actualDuration]
           ,[appeal]
           ,[reMarkStatus]
           ,[qualificationLevel]
           ,[centreId]
           ,[ULN]
           ,[AllowPackageDelivery]
           ,[ExportToSecureMarker]
           ,[ContainsBTLOffice]
           ,[ExportedToIntegration]
           ,[WarehouseExamState]
           ,[language]
           ,[KeyCode]
           ,[TargetedForVoid]
           ,[EnableOverrideMarking]
           ,[IsExternal]
           ,[AddressLine1]
           ,[AddressLine2]
           ,[Town]
           ,[County]
           ,[Country]
           ,[Postcode]
           ,[ScoreBoundaryData]
           ,[itemMarksUploaded]
           ,[passMark]
           ,[totalMark]
           ,[passType]
           ,[voidJustificationLookupTableId]
           ,[automaticVerification]
           ,[resultSampled]
           ,[started]
           ,[submitted]
           ,[validFromDate]
           ,[expiryDate]
           ,[totalTimeSpent]
           ,[defaultDuration]
           ,[scheduledDurationReason]
           ,[WAREHOUSEScheduledExamID]
           ,[WAREHOUSEUserID]
           ,[itemDataFull]
           ,[examId]
           ,[userId]
           ,[TakenThroughLocalScan]
           ,[LocalScanDownloadDate]
           ,[LocalScanUploadDate]
           ,[LocalScanNumPages]
           ,[CertifiedForTablet]
           ,[IsProjectBased]
           ,[AssignedMarkerUserID]
           ,[AssignedModeratorUserID])
SELECT [examSessionId]
      ,[structureXml]
      ,[examVersionName]
      ,[examVersionRef]
      ,[examVersionId]
      ,[examName]
      ,[examRef]
      ,[qualificationid]
      ,[qualificationName]
      ,[qualificationRef]
      ,[resultData]
      ,[submittedDate]
      ,[originatorId]
      ,[centreName]
      ,[centreCode]
      ,[foreName]
      ,[dateOfBirth]
      ,[gender]
      ,[candidateRef]
      ,[surName]
      ,[scheduledDurationValue]
      ,[previousExamState]
      ,[examStateInformation]
      ,[examResult]
      ,[passValue]
      ,[closeValue]
      ,[ExternalReference]
      ,[examType]
      ,[warehouseTime]
      ,[CQN]
      ,[actualDuration]
      ,[appeal]
      ,[reMarkStatus]
      ,[qualificationLevel]
      ,[centreId]
      ,[ULN]
      ,[AllowPackageDelivery]
      ,[ExportToSecureMarker]
      ,[ContainsBTLOffice]
      ,[ExportedToIntegration]
      ,[WarehouseExamState]
      ,[language]
      ,[KeyCode]
      ,[TargetedForVoid]
      ,[EnableOverrideMarking]
      ,[IsExternal]
      ,[AddressLine1]
      ,[AddressLine2]
      ,[Town]
      ,[County]
      ,[Country]
      ,[Postcode]
      ,[ScoreBoundaryData]
      ,[itemMarksUploaded]
      ,[passMark]
      ,[totalMark]
      ,[passType]
      ,[voidJustificationLookupTableId]
      ,[automaticVerification]
      ,[resultSampled]
      ,[started]
      ,[submitted]
      ,[validFromDate]
      ,[expiryDate]
      ,[totalTimeSpent]
      ,[defaultDuration]
      ,[scheduledDurationReason]
      ,[WAREHOUSEScheduledExamID]
      ,[WAREHOUSEUserID]
      ,[itemDataFull]
      ,[examId]
      ,[userId]
      ,[TakenThroughLocalScan]
      ,[LocalScanDownloadDate]
      ,[LocalScanUploadDate]
      ,[LocalScanNumPages]
      ,[CertifiedForTablet]
      ,[IsProjectBased]
      ,[AssignedMarkerUserID]
      ,[AssignedModeratorUserID]
FROM AAT_SecureAssess..WAREHOUSE_ExamSessionTable_Shreded
WHERE examSessionId IN (1389306,1397361,1397436,1397540,1397573,1384719,1391147,1391689,1391720,1397770,1397849,1398018,1398084,1398086,1398132,1398362,1385597,1385636,1391797,1392141,1392193,1392278,1392294,1399051,1399577,1399695,1392577,1392633,1392876,1393016,1393378,1393383,1393498,1393533,1393622,1393628,1393666,1393705,1399781,1383918,1390485,1396536,1396560,1396647,1396724,1388521,1388570,1388913,1388993,1388994,1389030,1395895,1395954,1388124,1388266,1395350,1395567,1395569,1395591,1395595,1386059,1386102,1386125,1394039,1394110,1394182,1387209,1387233,1387321,1387577,1394982,1382691,1390114,1390123);


CREATE TABLE TempDatabase.[dbo].[WAREHOUSE_ExamSessionItemResponseTable](
	[ID] [int] NOT NULL,
	[WAREHOUSEExamSessionID] [int] NOT NULL,
	[ItemID] [nvarchar](15) NOT NULL,
	[ItemVersion] [int] NOT NULL,
	[ItemResponseData] [xml] NULL,
	[MarkerResponseData] [xml] NULL,
	[ItemMark] [int] NULL,
	[MarkingIgnored] [tinyint] NULL,
	[DerivedResponse] [nvarchar](max) NULL,
	[ShortDerivedResponse] [nvarchar](500) NULL
)

INSERT INTO TempDatabase.[dbo].WAREHOUSE_ExamSessionItemResponseTable
SELECT *
FROM AAT_SecureAssess..WAREHOUSE_ExamSessionItemResponseTable
WHERE WAREHOUSEExamSessionID IN (1389306,1397361,1397436,1397540,1397573,1384719,1391147,1391689,1391720,1397770,1397849,1398018,1398084,1398086,1398132,1398362,1385597,1385636,1391797,1392141,1392193,1392278,1392294,1399051,1399577,1399695,1392577,1392633,1392876,1393016,1393378,1393383,1393498,1393533,1393622,1393628,1393666,1393705,1399781,1383918,1390485,1396536,1396560,1396647,1396724,1388521,1388570,1388913,1388993,1388994,1389030,1395895,1395954,1388124,1388266,1395350,1395567,1395569,1395591,1395595,1386059,1386102,1386125,1394039,1394110,1394182,1387209,1387233,1387321,1387577,1394982,1382691,1390114,1390123);

CREATE TABLE TempDatabase.[dbo].[WAREHOUSE_ExamSessionTable_ShrededItems](
	[examSessionId] [int] NOT NULL,
	[ItemRef] [varchar](15) NULL,
	[ItemName] [nvarchar](200) NULL,
	[userMark] [decimal](6, 3) NULL,
	[markerUserMark] [nvarchar](max) NULL,
	[examPercentage] [decimal](6, 3) NULL,
	[candidateId] [int] IDENTITY(1,1) NOT NULL,
	[ItemVersion] [int] NULL,
	[responsexml] [xml] NULL,
	[OptionsChosen] [varchar](200) NULL,
	[selectedCount] [int] NULL,
	[correctAnswerCount] [int] NULL,
	[TotalMark] [decimal](6, 3) NOT NULL,
	[userAttempted] [bit] NULL,
	[markingIgnored] [tinyint] NULL,
	[markedMetadataCount] [int] NULL,
	[ItemType] [int] NULL,
	[FriendItems] [nvarchar](max) NULL
);

INSERT INTO TempDatabase.[dbo].WAREHOUSE_ExamSessionItemResponseTable
SELECT *
FROM AAT_SecureAssess..WAREHOUSE_ExamSessionItemResponseTable
WHERE WAREHOUSEExamSessionID IN (1389306,1397361,1397436,1397540,1397573,1384719,1391147,1391689,1391720,1397770,1397849,1398018,1398084,1398086,1398132,1398362,1385597,1385636,1391797,1392141,1392193,1392278,1392294,1399051,1399577,1399695,1392577,1392633,1392876,1393016,1393378,1393383,1393498,1393533,1393622,1393628,1393666,1393705,1399781,1383918,1390485,1396536,1396560,1396647,1396724,1388521,1388570,1388913,1388993,1388994,1389030,1395895,1395954,1388124,1388266,1395350,1395567,1395569,1395591,1395595,1386059,1386102,1386125,1394039,1394110,1394182,1387209,1387233,1387321,1387577,1394982,1382691,1390114,1390123);


CREATE TABLE TempDatabase.[dbo].[WAREHOUSE_ExamSessionTable_ShreddedItems_Mark](
	[ExamSessionID] [int] NOT NULL,
	[ItemID] [varchar](15) NOT NULL,
	[Mark] [decimal](6, 3) NOT NULL,
	[LearningOutcome] [int] NOT NULL,
	[DisplayText] [nvarchar](max) NOT NULL,
	[MaxMark] [decimal](6, 3) NOT NULL
);

INSERT INTO TempDatabase.[dbo].[WAREHOUSE_ExamSessionTable_ShreddedItems_Mark]
SELECT *
FROM AAT_SecureAssess..[WAREHOUSE_ExamSessionTable_ShreddedItems_Mark]
WHERE [ExamSessionID] IN (1389306,1397361,1397436,1397540,1397573,1384719,1391147,1391689,1391720,1397770,1397849,1398018,1398084,1398086,1398132,1398362,1385597,1385636,1391797,1392141,1392193,1392278,1392294,1399051,1399577,1399695,1392577,1392633,1392876,1393016,1393378,1393383,1393498,1393533,1393622,1393628,1393666,1393705,1399781,1383918,1390485,1396536,1396560,1396647,1396724,1388521,1388570,1388913,1388993,1388994,1389030,1395895,1395954,1388124,1388266,1395350,1395567,1395569,1395591,1395595,1386059,1386102,1386125,1394039,1394110,1394182,1387209,1387233,1387321,1387577,1394982,1382691,1390114,1390123);



/*

drop table TempDatabase.[dbo].WAREHOUSE_ExamSessionItemResponseTable
drop table TempDatabase.[dbo].WAREHOUSE_ExamSessionTable_ShreddedItems_Mark
drop table TempDatabase.[dbo].WAREHOUSE_ExamSessionTable_Shreded
drop table TempDatabase.[dbo].WAREHOUSE_ExamSessionTable_ShrededItems
drop table TempDatabase.[dbo].WAREHOUSE_ExamSessionTable
drop function [dbo].[fn_getValueFromResultData]

*/

*/