begin tran 
update Warehouse_ExamSessionTable
set resultData = 
'<exam passMark="70" passType="1" originalPassMark="70" originalPassType="1" totalMark="150" userMark="131.000" userPercentage="87.333" passValue="1" originalPassValue="0" totalTimeSpent="5378563" closeBoundaryType="0" closeBoundaryValue="0" grade="Pass" originalGrade="Pass">
  <gradeBoundaryData>
    <gradeBoundaries passLevelType="1" adjustedGradeBoundaries="0">
      <grade modifier="lt" value="70" description="Fail" />
      <grade modifier="gt" value="70" description="Pass" />
    </gradeBoundaries>
  </gradeBoundaryData>
  <section id="1" passMark="60" passType="1" name="1" totalMark="90" userMark="80.000" userPercentage="88.889" passValue="0" totalTimeSpent="3151096" itemsToMark="0">
    <item id="879P1324" name="P 1.1 FNST AC003" totalMark="19" userMark="0" actualUserMark="0.0000000000" markingType="0" markerUserMark="19.0000000000" viewingTime="639522" userAttempted="1" version="14" />
    <item id="879P1316" name="P 1.2 FNST AC003" totalMark="17" userMark="0" actualUserMark="0.0000000000" markingType="0" markerUserMark="17.0000000000" viewingTime="529870" userAttempted="1" version="9" />
    <item id="879P1698" name="1.3 FNST AC 010" totalMark="12" userMark="0" actualUserMark="10.0000000000" markingType="1" markerUserMark="10.0000000000" viewingTime="403885" userAttempted="1" version="22" />
    <item id="879P1828" name="1.4 FNST DO 012" totalMark="12" userMark="0" actualUserMark="0.0000000000" markingType="0" markerUserMark="8.0000000000" viewingTime="441294" userAttempted="1" version="24" />
    <item id="879P1997" name="1.5 FNST AC 013" totalMark="30" userMark="0" actualUserMark="0.0000000000" markingType="0" markerUserMark="26.0000000000" viewingTime="1136525" userAttempted="1" version="12" />
  </section>
  <section id="2" passMark="60" passType="1" name="2" totalMark="60" userMark="51.000" userPercentage="85.000" passValue="0" totalTimeSpent="2227467" itemsToMark="0">
    <item id="879P1734" name="2.1 FNST DO 010" totalMark="32" userMark="0" actualUserMark="0.0000000000" markingType="0" markerUserMark="32.0000000000" viewingTime="562007" userAttempted="1" version="21" />
    <item id="879P2089" name="2.2 FNST AC 002 2011" totalMark="18" userMark="0" actualUserMark="15.0000000000" markingType="1" markerUserMark="15.0000000000" viewingTime="1097837" userAttempted="1" version="26" />
    <item id="879P1523" name="2.3 FNST AC 005" totalMark="10" userMark="0" actualUserMark="4.0000000000" markingType="1" markerUserMark="4.0000000000" viewingTime="567623" userAttempted="1" version="15" />
  </section>
</exam>', resultDataFull = '<assessmentDetails>
  <assessmentID>801</assessmentID>
  <qualificationID>43</qualificationID>
  <qualificationName>Financial Statements (AQ2010)</qualificationName>
  <qualificationReference>AATQCF</qualificationReference>
  <assessmentGroupName>FNST</assessmentGroupName>
  <assessmentGroupID>349</assessmentGroupID>
  <assessmentGroupReference>CAFNST</assessmentGroupReference>
  <assessmentName>FNST pop up variant 2</assessmentName>
  <validFromDate>01 Jan 2012</validFromDate>
  <expiryDate>31 Dec 2014</expiryDate>
  <startTime>00:00:00</startTime>
  <endTime>23:59:59</endTime>
  <duration>150</duration>
  <defaultDuration>150</defaultDuration>
  <scheduledDuration>
    <value>150</value>
    <reason />
  </scheduledDuration>
  <sRBonus>0</sRBonus>
  <sRBonusMaximum>50</sRBonusMaximum>
  <externalReference>CBTAAT</externalReference>
  <passLevelValue>70</passLevelValue>
  <passLevelType>1</passLevelType>
  <status>2</status>
  <testFeedbackType>
    <passFail>0</passFail>
    <percentageMark>0</percentageMark>
    <allowSummaryFeedback>0</allowSummaryFeedback>
    <summaryFeedbackType>1</summaryFeedbackType>
    <itemSummary>0</itemSummary>
    <itemReview>0</itemReview>
    <itemReviewCorrectAnswer>0</itemReviewCorrectAnswer>
    <itemFeedback>0</itemFeedback>
    <printableSummary>0</printableSummary>
    <candidateDetails>0</candidateDetails>
    <feedbackByReference>0</feedbackByReference>
    <allowFeedbackDuringExam>0</allowFeedbackDuringExam>
    <allowFeedbackDuringSummary>0</allowFeedbackDuringSummary>
  </testFeedbackType>
  <itemFeedback>0</itemFeedback>
  <allowCalculator>0</allowCalculator>
  <lastInUseDate>10 Nov 2011 17:14:02</lastInUseDate>
  <completedScriptReview>0</completedScriptReview>
  <assessment currentSection="2" totalTime="0" currentTime="0" originalPassMark="70" originalPassType="1" passMark="70" passType="1" totalMark="150" userMark="131.000" userPercentage="87.333" passValue="1" originalGrade="Pass">
    <intro id="0" name="Introduction" currentItem="">
      <item id="879P2208" name="Copy of Introduction" totalMark="1" version="10" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
    </intro>
    <outro id="0" name="Finish" currentItem="" />
    <tools id="0" name="Tools" currentItem="">
      <item id="879P1629" name="1.1 FNST AC 001 Asker Ltd popup A" toolName="1.1 FNST AC 001 Asker Ltd popup A" totalMark="1" version="2" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1628" name="1.1 FNST AC 001 Asker Ltd popup B" toolName="1.1 FNST AC 001 Asker Ltd popup B" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1620" name="1.1 FNST DO 001 Polo Ltd popup A" toolName="1.1 FNST DO 001 Polo Ltd popup A" totalMark="1" version="2" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1619" name="1.1 FNST DO 001 Polo Ltd popup B" toolName="1.1 FNST DO 001 Polo Ltd popup B" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1368" name="1.1 FNST AC002 Cole Ltd popup A" toolName="1.1 FNST AC002 Cole Ltd popup A" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1369" name="1.1 FNST AC002 Cole Ltd popup B" toolName="1.1 FNST AC002 Cole Ltd popup B" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1370" name="1.1 FNST AC003 Allen Ltd popup A" toolName="1.1 FNST AC003 Allen Ltd popup A" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1371" name="1.1 FNST AC003 Allen Ltd popup B" toolName="1.1 FNST AC003 Allen Ltd popup B" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1372" name="1.1 FNST DO002 Hughes Ltd popup A" toolName="1.1 FNST DO002 Hughes Ltd popup A" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1373" name="1.1 FNST DO002 Hughes Ltd popup B" toolName="1.1 FNST DO002 Hughes Ltd popup B" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1374" name="1.1 FNST DO003 Burgess Ltd popup A" toolName="1.1 FNST DO003 Burgess Ltd popup A" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1375" name="1.1 FNST DO003 Burgess Ltd popup B" toolName="1.1 FNST DO003 Burgess Ltd popup B" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1376" name="1.5 FNST AC002 Austell popup A" toolName="1.5 FNST AC002 Austell popup A" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1377" name="1.5 FNST AC002 Austell popup B" toolName="1.5 FNST AC002 Austell popup B" totalMark="1" version="2" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1378" name="1.5 FNST AC003 Avon popup A" toolName="1.5 FNST AC003 Avon popup A" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1379" name="1.5 FNST AC003 Avon popup B" toolName="1.5 FNST AC003 Avon popup B" totalMark="1" version="2" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1380" name="1.5 FNST DO003 Finch popup A" toolName="1.5 FNST DO003 Finch popup A" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1381" name="1.5 FNST DO003 Finch popup B" toolName="1.5 FNST DO003 Finch popup B" totalMark="1" version="2" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1382" name="2.1 FNST AC003 popup A" toolName="2.1 FNST AC003 popup A" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1383" name="2.1 FNST AC003 popup B" toolName="2.1 FNST AC003 popup B" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1384" name="2.1 FNST AC002 popup A" toolName="2.1 FNST AC002 popup A" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1385" name="2.1 FNST AC002 popup B" toolName="2.1 FNST AC002 popup B" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1386" name="2.1 FNST AC001 popup A" toolName="2.1 FNST AC001 popup A" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1387" name="2.1 FNST AC001 popup B" toolName="2.1 FNST AC001 popup B" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1388" name="2.1 FNST DO002 popup A" toolName="2.1 FNST DO002 popup A" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1389" name="2.1 FNST DO002 popup B" toolName="2.1 FNST DO002 popup B" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1390" name="2.1 FNST DO003 popup A" toolName="2.1 FNST DO003 popup A" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1391" name="2.1 FNST DO003 popup B" toolName="2.1 FNST DO003 popup B" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1633" name="1.1 FNST AC 001 Iron Ltd popup A" toolName="1.1 FNST AC 001 Iron Ltd popup A" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1634" name="1.1 FNST AC 001 Iron Ltd popup B" toolName="1.1 FNST AC 001 Iron Ltd popup B" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1646" name="1.1 FNST AC 002 Camel A" toolName="1.1 FNST AC 002 Camel A" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1647" name="1.1 FNST AC 002 Camel B" toolName="1.1 FNST AC 002 Camel B" totalMark="1" version="2" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1653" name="1.1 FNST AC 003 Trent A" toolName="1.1 FNST AC 003 Trent A" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1654" name="1.1 FNST AC 003 Trent B" toolName="1.1 FNST AC 003 Trent B" totalMark="1" version="2" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1641" name="1.1 FNST DO 001 Shelford Ltd popup A" toolName="1.1 FNST DO 001 Shelford Ltd popup A" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1642" name="1.1 FNST DO 001 Shelford Ltd popup B" toolName="1.1 FNST DO 001 Shelford Ltd popup B" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1637" name="1.5 FNST AC 001 Mole Ltd popup A" toolName="1.5 FNST AC 001 Mole Ltd popup A" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1638" name="1.5 FNST AC 001 Madford Plc popup A" toolName="1.5 FNST AC 001 Madford Plc popup A" totalMark="1" version="2" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1649" name="1.5 FNST AC 002 Chelt A" toolName="1.5 FNST AC 002 Chelt A" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1650" name="1.5 FNST AC 002 Chelt B" toolName="1.5 FNST AC 002 Chelt B" totalMark="1" version="2" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1655" name="1.5 FNST AC 003 Kinder A" toolName="1.5 FNST AC 003 Kinder A" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1643" name="1.5 FNST DO 001 Rickling Ltd popup A" toolName="1.5 FNST DO 001 Rickling Ltd popup A" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1644" name="1.5 FNST DO 001 Stebbing Ltd popup A" toolName="1.5 FNST DO 001 Stebbing Ltd popup A" totalMark="1" version="2" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1661" name="2.1 FNST AC 001 Durham Ltd popup A" toolName="2.1 FNST AC 001 Durham Ltd popup A" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1662" name="2.1 FNST AC 001 Durham Ltd popup B" toolName="2.1 FNST AC 001 Durham Ltd popup B" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1665" name="2.1 FNST AC 002 Tweed A" toolName="2.1 FNST AC 002 Tweed A" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1666" name="2.1 FNST AC 002 Tweed B" toolName="2.1 FNST AC 002 Tweed B" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1667" name="2.1 FNST AC 003 Arun A" toolName="2.1 FNST AC 003 Arun A" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1668" name="2.1 FNST AC 003 Arun B" toolName="2.1 FNST AC 003 Arun B" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1663" name="2.1 FNST DO 001 Wembling A" toolName="2.1 FNST DO 001 Wembling A" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1664" name="2.1 FNST DO 001 Wembling B" toolName="2.1 FNST DO 001 Wembling B" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2356" name="1.5 FNST DO 010 pop up A" toolName="1.5 FNST DO 010 pop up A" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2358" name="1.5 FNST DO 010 pop up B" toolName="1.5 FNST DO 010 pop up B" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2359" name="1.5 FNST DO 010 pop up C" toolName="1.5 FNST DO 010 pop up C" totalMark="1" version="2" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2305" name="1.5 FNST DO 013 Platt Popup B" toolName="1.5 FNST DO 013 Platt Popup B" totalMark="1" version="2" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2303" name="1.5 FNST DO 013 Platt Popup A" toolName="1.5 FNST DO 013 Platt Popup A" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2249" name="1.5 FNST AC 010 Bywell Popup A" toolName="1.5 FNST AC 010 Bywell Popup A" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2250" name="1.5 FNST AC 010 Bywell Popup B" toolName="1.5 FNST AC 010 Bywell Popup B" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2322" name="1.5 FNST AC 011 Barton Popup B" toolName="1.5 FNST AC 011 Barton Popup B" totalMark="1" version="2" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2320" name="1.5 FNST AC 011 Barton Popup A" toolName="1.5 FNST AC 011 Barton Popup A" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2258" name="1.5 FNST AC 014 Lowick Popup A" toolName="1.5 FNST AC 014 Lowick Popup A" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2257" name="1.5 FNST AC 014 Birtley Popup A" toolName="1.5 FNST AC 014 Birtley Popup A" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2334" name="1.5 FNST AC 012 Pop up B" toolName="1.5 FNST AC 012 Pop up B" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2332" name="1.5 FNST AC 012 Pop up A" toolName="1.5 FNST AC 012 Pop up A" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2288" name="1.5 FNST AC 013 Earle Popup A" toolName="1.5 FNST AC 013 Earle Popup A" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2290" name="1.5 FNST AC 013 Earle Popup B" toolName="1.5 FNST AC 013 Earle Popup B" totalMark="1" version="2" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2346" name="1.5 FNST DO 011 Hills Pop up A" toolName="1.5 FNST DO 011 Hills Pop up A" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2348" name="1.5 FNST DO 011 Grant Pop up B" toolName="1.5 FNST DO 011 Grant Pop up B" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2372" name="1.5 FNST DO 012 Pop up a" toolName="1.5 FNST DO 012 Pop up a" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2374" name="1.5 FNST DO 012 Pop up B" toolName="1.5 FNST DO 012 Pop up B" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2369" name="2.1 FNST DO 010 pop up A" toolName="2.1 FNST DO 010 pop up A" totalMark="1" version="2" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2370" name="2.1 FNST DO 010 pop up B" toolName="2.1 FNST DO 010 pop up B" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2251" name="2.1 FNST AC 010 Abberwick Popup A" toolName="2.1 FNST AC 010 Abberwick Popup A" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2252" name="2.1 FNST AC 010 Abberwick Popup B" toolName="2.1 FNST AC 010 Abberwick Popup B" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2307" name="2.1 FNST DO 013 Court Popup A" toolName="2.1 FNST DO 013 Court Popup A" totalMark="1" version="2" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2309" name="2.1 FNST DO 013 Court Popup B" toolName="2.1 FNST DO 013 Court Popup B" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2260" name="2.1 FNST AC 014 Belsay Popup B" toolName="2.1 FNST AC 014 Belsay Popup B" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2259" name="2.1 FNST AC 014 Belsay Popup A" toolName="2.1 FNST AC 014 Belsay Popup A" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2326" name="2.1 FNST AC 011 Anick Popup B" toolName="2.1 FNST AC 011 Anick Popup B" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2324" name="2.1 FNST AC 011 Anick Popup A" toolName="2.1 FNST AC 011 Anick Popup A" totalMark="1" version="2" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2381" name="2.1 FNST DO 012 Pop up B" toolName="2.1 FNST DO 012 Pop up B" totalMark="1" version="2" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2380" name="2.1 FNST DO 012 Pop up A" toolName="2.1 FNST DO 012 Pop up A" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2352" name="2.1 FNST DO 011 Leigh Pop up B" toolName="2.1 FNST DO 011 Leigh Pop up B" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2351" name="2.1 FNST DO 011 Leigh Pop up A" toolName="2.1 FNST DO 011 Leigh Pop up A" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2294" name="2.1 FNST AC 013 Lucker Popup B" toolName="2.1 FNST AC 013 Lucker Popup B" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2292" name="2.1 FNST AC 013 Lucker Popup A" toolName="2.1 FNST AC 013 Lucker Popup A" totalMark="1" version="2" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2330" name="2.1 FNST AC 012 Glanton Pop up B" toolName="2.1 FNST AC 012 Glanton Pop up B" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2329" name="2.1 FNST AC 012 Glanton Pop up A" toolName="2.1 FNST AC 012 Glanton Pop up A" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2394" name="1.5 FNST AC 003 Kinder B v2" toolName="1.5 FNST AC 003 Kinder B v2" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
    </tools>
    <section id="1" name="1" passLevelValue="60" passLevelType="1" itemsToMark="0" currentItem="879P1828" fixed="0" totalMark="90" userMark="80.000" userPercentage="88.889" passValue="0">
      <item id="879P1324" name="P 1.1 FNST AC003" totalMark="19" version="14" markingType="0" markingState="0" userMark="1" markerUserMark="19.0000000000" userAttempted="1" viewingTime="639522" flagged="0" LO="1.1 Prepare a statement of comprehensive income/financial position/change in equity/cash flow" unit="Financial Statements" quT="20">
        <p um="1" cs="1" ua="1" id="879P1324">
          <s ua="0" um="-1" id="1">
            <c typ="4" id="23">
              <i cc="5" id="1" />
            </c>
            <c typ="4" id="24">
              <i cc="3" id="1" />
            </c>
          </s>
          <s ua="1" um="1" id="2">
            <c wei="14" maS="0" ie="1" typ="20" rv="0" mv="0" ua="1" um="1" id="1">
              <i um="1" ia="1" id="1">&lt;table extensionName="linkedproforma"&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="72824"&gt;72824&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="3" cor="-[s2c3i1c2r1]-[s2c3i1c2r2]-[s2c3i1c2r3]-[s2c3i1c2r4]�-33509"&gt;-33509&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="2" cor="-[s2c4i1c2r1]-[s2c4i1c2r2]�-12393"&gt;-12393&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="2" cor="-[s2c5i1c2r1]-[s2c5i1c2r2]-[s2c5i1c2r3]"&gt;-9472&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="2" cor="-900"&gt;-900&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="2" cor="-[s2c15i1c2r1]-[s2c15i1c2r2]�-3538"&gt;-3538&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="2" cor="5000"&gt;5000&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;/table&gt;</i>
            </c>
            <c wei="2" maS="0" ie="1" typ="20" rv="0" mv="0" ua="1" um="1" id="4">
              <i um="1" ia="1" id="1">&lt;table extensionName="linkedproforma"&gt;&lt;r&gt;&lt;c h="0" tp="3" cor="Distribution costs "&gt;Distribution costs &lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="10937"&gt;10937&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="3" cor="Wages and salaries"&gt;Wages and salaries&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="1456"&gt;1456&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;/table&gt;</i>
            </c>
            <c wei="3" maS="0" ie="1" typ="20" rv="0" mv="0" ua="1" um="1" id="3">
              <i um="1" ia="1" id="1">&lt;table extensionName="linkedproforma"&gt;&lt;r&gt;&lt;c h="0" tp="3" cor="Opening inventories"&gt;Opening inventories&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="12583"&gt;12583&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="3" cor="Purchases"&gt;Purchases&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="35118"&gt;35118&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="3" cor="Closing inventories"&gt;Closing inventories&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="-14192"&gt;-14192&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;/table&gt;</i>
            </c>
            <c wei="2" maS="0" ie="1" typ="20" rv="0" mv="0" ua="1" um="1" id="15">
              <i um="1" ia="1" id="1">&lt;table extensionName="linkedproforma"&gt;&lt;r&gt;&lt;c h="0" tp="3" cor="Current year"&gt;Current year&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="3546"&gt;3546&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="3" cor="Previous year"&gt;Previous year&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="-8"&gt;-8&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;/table&gt;</i>
            </c>
            <c wei="2" maS="0" ie="1" typ="20" rv="0" mv="0" ua="1" um="1" id="5">
              <i um="1" ia="1" id="1">&lt;table extensionName="linkedproforma"&gt;&lt;r&gt;&lt;c h="0" tp="3" cor="Administrative expenses"&gt;Administrative expenses&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="7288"&gt;7288&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="3" cor="Wages and salaries"&gt;Wages and salaries&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="2184"&gt;2184&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;/table&gt;</i>
            </c>
          </s>
          <s ua="1" um="1" id="4">
            <c wei="5" maS="0" ie="1" typ="20" rv="0" mv="0" ua="1" um="1" id="1">
              <i um="1" ia="1" id="1">&lt;table&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="18000"&gt;18000&lt;/c&gt;&lt;c h="0" tp="1" mark="0" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="16482"&gt;16482&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="0" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="5000�[s2c1i1c2r11]~b"&gt;5000&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="13012�[s2c1i1c2r1]+[s2c1i1c2r2]+[s2c1i1c2r4]+[s2c1i1c2r5]+[s2c1i1c2r7]+[s2c1i1c2r9]~b"&gt;13012&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="0" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="0" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="-1600"&gt;-1600&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="0" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="0" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="0" cor=""&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;/table&gt;</i>
            </c>
          </s>
        </p>
      </item>
      <item id="879P1316" name="P 1.2 FNST AC003" totalMark="17" version="9" markingType="0" markingState="0" userMark="1" markerUserMark="17.0000000000" userAttempted="1" viewingTime="529870" flagged="0" LO="1.2 Prepare a statement of comprehensive income/financial position/change in equity/cash flow" unit="Financial Statements" quT="20">
        <p um="1" cs="1" ua="1" id="879P1316">
          <s ua="0" um="-1" id="1">
            <c typ="4" id="23">
              <i cc="1" id="1" />
            </c>
            <c typ="4" id="24">
              <i cc="1" id="1" />
            </c>
          </s>
          <s ua="1" um="1" id="3">
            <c wei="10" maS="0" ie="1" typ="20" rv="0" mv="0" ua="1" um="1" id="5">
              <i um="1" ia="1" id="1">&lt;table extensionName="linkedproforma"&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="3" cor="Share capital"&gt;Retained earnings&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="18000"&gt;27894&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="3" cor="Retained earnings"&gt;Revaluation reserve&lt;/c&gt;&lt;c h="0" tp="1" mark="3" cor="27894�[s3c9i1c2r1]+[s3c9i1c2r2]+[s3c9i1c2r3]"&gt;5000&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="3" cor="Revaluation reserve"&gt;Share capital&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="5000"&gt;18000&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="3" cor="Bank loans"&gt;Bank loans&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="20000"&gt;20000&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="3" cor="Trade and other payables"&gt;Trade and other payables&lt;/c&gt;&lt;c h="0" tp="1" mark="3" cor="3651�[s3c10i1c2r1]+[s3c10i1c2r2]+[s3c10i1c2r3]"&gt;3651&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="3" cor="Tax liability"&gt;Tax liability&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="3546"&gt;3546&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;/table&gt;</i>
            </c>
            <c wei="3" maS="0" ie="1" typ="20" rv="0" mv="0" ua="1" um="1" id="10">
              <i um="1" ia="1" id="1">&lt;table extensionName="linkedproforma"&gt;&lt;r&gt;&lt;c h="0" tp="3" cor="Trade and other payables"&gt;Trade and other payables&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="2937"&gt;2937&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="3" cor="Accruals - trial balance"&gt;Accruals - trial balance&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="414"&gt;414&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="3" cor="Interest accrual"&gt;Interest accrual&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="300"&gt;300&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;/table&gt;</i>
            </c>
            <c wei="3" maS="0" ie="1" typ="20" rv="0" mv="0" ua="1" um="1" id="9">
              <i um="1" ia="1" id="1">&lt;table extensionName="linkedproforma"&gt;&lt;r&gt;&lt;c h="0" tp="3" cor="Retained earnings at 1 January 20X0"&gt;Retained earnings at 1 January 20X0&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="16482"&gt;16482&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="3" cor="Total profit for the year"&gt;Dividends paid&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="13012"&gt;-1600&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="3" cor="Dividends paid"&gt;Total profit for the year&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="-1600"&gt;13012&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;/table&gt;</i>
            </c>
            <c wei="2" maS="0" ie="1" typ="20" rv="0" mv="0" ua="1" um="1" id="7">
              <i um="1" ia="1" id="1">&lt;table extensionName="linkedproforma"&gt;&lt;r&gt;&lt;c h="0" tp="3" cor="Trade and other receivables"&gt;Trade and other receivables&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="8846"&gt;8846&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="3" cor="Prepayments - trial balance"&gt;Prepayments - trial balance&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="387"&gt;387&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;/table&gt;</i>
            </c>
            <c wei="3" maS="0" ie="1" typ="20" rv="0" mv="0" ua="1" um="1" id="6">
              <i um="1" ia="1" id="1">&lt;table extensionName="linkedproforma"&gt;&lt;r&gt;&lt;c h="0" tp="3" cor="Property, plant and equipment - cost"&gt;Property, plant and equipment - cost&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="70649"&gt;70649&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="3" cor="Accumulated depreciation"&gt;Revaluation&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="-21267"&gt;5000&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="3" cor="Revaluation"&gt;Accumulated depreciation&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="5000"&gt;-21267&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;/table&gt;</i>
            </c>
            <c wei="7" maS="0" ie="1" typ="20" rv="0" mv="0" ua="1" um="1" id="24">
              <i um="1" ia="1" id="1">&lt;table extensionName="linkedproforma"&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="0" cor=""&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="0" cor=""&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="3" cor="Property, plant and equipment"&gt;Property, plant and equipment&lt;/c&gt;&lt;c h="0" tp="1" mark="3" cor="54382�[s3c6i1c2r1]+[s3c6i1c2r2]+[s3c6i1c2r3]"&gt;54382&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="0" cor=""&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="3" cor="Inventories"&gt;Inventories&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="14192"&gt;14192&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="3" cor="Trade and other receivables"&gt;Trade and other receivables&lt;/c&gt;&lt;c h="0" tp="1" mark="2" cor="9233�[s3c7i1c2r1]+[s3c7i1c2r2]"&gt;9233&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="3" cor="Cash and cash equivalents"&gt;Cash and cash equivalents&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="284"&gt;284&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;/table&gt;</i>
            </c>
          </s>
        </p>
      </item>
      <item id="879P1698" name="1.3 FNST AC 010" totalMark="12" version="22" markingType="1" markingState="0" userMark="0" markerUserMark="10.0000000000" userAttempted="1" viewingTime="403885" flagged="0" LO="1.3 Knowledge and understanding of International Financial Reporting Standards (written)" unit="Financial Statements" quT="11">
        <p um="0" cs="1" ua="1" id="879P1698">
          <s ua="1" um="0" id="2">
            <c wei="12" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="1">
              <i ca="" id="1">a) Property, Plant &amp; Equipment is defined as assets which can give future economic benefits to the business and ensure more efficient workings, such as vans, industrial machinery and computers.#!##!#b) The cost of an item of property, plant &amp; equipment should be recognised as an asset when:#!##!#i) the cost of the asset can be measured reliably#!#ii) it is probable that future economic benefits will flow to the entity.#!##!#c) The total cost that must be recognised is �32,010. This includes the purchase price of the machine and all incidental costs associated with the purchase except for the cost of the maintenance contract.#!#</i>
            </c>
          </s>
        </p>
      </item>
      <item id="879P1828" name="1.4 FNST DO 012" totalMark="12" version="24" markingType="0" markingState="0" userMark="1" markerUserMark="8.0000000000" userAttempted="1" viewingTime="441294" flagged="0" LO="1.4 Application of reporting standards" unit="Financial Statements" quT="10">
        <p um="0.666666666666667" cs="1" ua="1" id="879P1828">
          <s ua="1" um="1" id="1">
            <c wei="2" maS="1" ie="1" typ="10" rv="0" mv="0" ua="1" um="1" id="15">
              <i ac="A" sl="0" id="1" />
              <i ca="1" ac="B" sl="1" id="2" />
              <i ac="C" sl="0" id="3" />
              <i ac="D" sl="0" id="4" />
            </c>
          </s>
          <s ua="1" um="1" id="2">
            <c wei="2" maS="1" ie="1" typ="10" rv="0" mv="0" ua="1" um="1" id="5">
              <i ac="A" sl="0" id="1" />
              <i ac="B" sl="0" id="2" />
              <i ca="1" ac="C" sl="1" id="3" />
              <i ac="D" sl="0" id="4" />
            </c>
          </s>
          <s ua="1" um="0" id="3">
            <c wei="2" maS="1" ie="1" typ="10" rv="0" mv="0" ua="1" um="0" id="31">
              <i ac="A" sl="1" id="1" />
              <i ac="B" sl="0" id="2" />
              <i ac="C" sl="0" id="3" />
              <i ca="1" ac="D" sl="0" id="4" />
            </c>
          </s>
          <s ua="1" um="1" id="4">
            <c wei="2" maS="1" ie="1" typ="10" rv="0" mv="0" ua="1" um="1" id="14">
              <i ac="A" sl="0" id="1" />
              <i ca="1" ac="B" sl="1" id="2" />
              <i ac="C" sl="0" id="3" />
              <i ac="D" sl="0" id="4" />
            </c>
          </s>
          <s ua="1" um="0" id="5">
            <c wei="2" maS="1" ie="1" typ="10" rv="0" mv="0" ua="1" um="0" id="5">
              <i ac="A" sl="1" id="1" />
              <i ca="1" ac="B" sl="0" id="2" />
              <i ac="C" sl="0" id="3" />
              <i ac="D" sl="0" id="4" />
            </c>
          </s>
          <s ua="1" um="1" id="6">
            <c wei="2" maS="1" ie="1" typ="10" rv="0" mv="0" ua="1" um="1" id="5">
              <i ca="1" ac="A" sl="1" id="1" />
              <i ac="B" sl="0" id="2" />
            </c>
          </s>
        </p>
      </item>
      <item id="879P1997" name="1.5 FNST AC 013" totalMark="30" version="12" markingType="0" markingState="0" userMark="1" markerUserMark="26.0000000000" userAttempted="1" viewingTime="1136525" flagged="0" LO="1.5 Drafting consolidated financial statements" unit="Financial Statements" quT="20">
        <p um="0.866666666666667" cs="1" ua="1" id="879P1997">
          <s ua="0" um="-1" id="1">
            <c typ="4" id="17">
              <i cc="5" id="1" />
            </c>
            <c typ="4" id="19">
              <i cc="5" id="1" />
            </c>
          </s>
          <s ua="1" um="0.87" id="2">
            <c wei="17" maS="0" ie="1" typ="20" rv="0" mv="0" ua="1" um="0.82" id="2">
              <i um="0.823529411764706" ia="1" id="1">&lt;table extensionName="linkedproforma"&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="0" cor=""&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="0" cor=""&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="1400"&gt;1400&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="300"&gt;300&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="5" cor="3976�[s2c5i1c2r1]+[s2c5i1c2r2]+[s2c5i1c2r3]"&gt;4241&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="7" cor="[s2c4i1c2r1]+[s2c4i1c2r2]+[s2c4i1c2r3]+[s2c4i1c2r4]+[s2c4i1c2r5]�592"&gt;592&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="768"&gt;768&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="2" cor="1676"&gt;1676&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;/table&gt;</i>
            </c>
            <c wei="7" maS="0" ie="1" typ="20" rv="0" mv="0" ua="1" um="1" id="4">
              <i um="1" ia="1" id="1">&lt;table extensionName="linkedproforma"&gt;&lt;r&gt;&lt;c h="0" tp="3" cor="Share capital - attributable to NCI"&gt;Retained earnings  - attributable to NCI&lt;/c&gt;&lt;c h="0" tp="1" mark="2" cor="240"&gt;140&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="3" cor="Share premium - attributable to NCI"&gt;Revaluation reserve - attributable to NCI&lt;/c&gt;&lt;c h="0" tp="1" mark="2" cor="180"&gt;32&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="3" cor="Revaluation reserve - attributable to NCI"&gt;Share capital - attributable to NCI&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="32"&gt;240&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="3" cor="Retained earnings  - attributable to NCI"&gt;Share premium - attributable to NCI&lt;/c&gt;&lt;c h="0" tp="1" mark="2" cor="140"&gt;180&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;/table&gt;</i>
            </c>
            <c wei="13" maS="0" ie="1" typ="20" rv="0" mv="0" ua="1" um="0.92" id="13">
              <i um="0.923076923076923" ia="1" id="1">&lt;table extensionName="linkedproforma"&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="0" cor=""&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="0" cor=""&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="9" cor="[s2c3i1c2r1]+[s2c3i1c2r2]+[s2c3i1c2r3]+[s2c3i1c2r4]+[s2c3i1c2r5]+[s2c3i1c2r6]�367"&gt;623&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="2" cor="6071"&gt;6071&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="2" cor="2274"&gt;2274&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;/table&gt;</i>
            </c>
            <c wei="5" maS="0" ie="1" typ="20" rv="0" mv="0" ua="1" um="0.4" id="5">
              <i um="0.4" ia="1" id="1">&lt;table extensionName="linkedproforma"&gt;&lt;r&gt;&lt;c h="0" tp="3" cor="Earle Plc"&gt;Earle Plc&lt;/c&gt;&lt;c h="0" tp="1" mark="2" cor="3941"&gt;3941&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="3" cor="Brandon Ltd - attributable to Earle Plc"&gt;Brandon Ltd - attributable to Earle Plc&lt;/c&gt;&lt;c h="0" tp="1" mark="2" cor="112"&gt;140&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="3" cor="Impairment of goodwill"&gt;Revaluation &lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="-77"&gt;160&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;/table&gt;</i>
            </c>
            <c wei="9" maS="0" ie="1" typ="20" rv="0" mv="0" ua="1" um="0.89" id="3">
              <i um="0.888888888888889" ia="1" id="1">&lt;table extensionName="linkedproforma"&gt;&lt;r&gt;&lt;c h="0" tp="3" cor="Share capital - attributable to Earle Plc"&gt;Price paid&lt;/c&gt;&lt;c h="0" tp="1" mark="2" cor="-960"&gt;2700&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="3" cor="Share premium - attributable to Earle Plc"&gt;Retained earnings - attributable to Earle Plc&lt;/c&gt;&lt;c h="0" tp="1" mark="2" cor="-720"&gt;-448&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="3" cor="Revaluation reserve - attributable to Earle Plc"&gt;Revaluation reserve - attributable to Earle Plc&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="-128"&gt;128&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="3" cor="Retained earnings - attributable to Earle Plc"&gt;Share capital - attributable to Earle Plc&lt;/c&gt;&lt;c h="0" tp="1" mark="2" cor="-448"&gt;-960&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="3" cor="Price paid"&gt;Share premium - attributable to Earle Plc&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="2700"&gt;-720&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="3" cor="Impairment of goodwill"&gt;Impairment of goodwill&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="-77"&gt;-77&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;/table&gt;</i>
            </c>
          </s>
        </p>
      </item>
    </section>
    <section id="2" name="2" passLevelValue="60" passLevelType="1" itemsToMark="0" currentItem="879P1523" fixed="0" totalMark="60" userMark="51.000" userPercentage="85.000" passValue="0">
      <item id="879P1734" name="2.1 FNST DO 010" totalMark="32" version="21" markingType="0" markingState="0" userMark="1" markerUserMark="32.0000000000" userAttempted="1" viewingTime="562007" flagged="0" LO="2.1 Analysis of financial statements using ratios" unit="Financial Statements" quT="11,12">
        <p um="1" cs="1" ua="1" id="879P1734">
          <s ua="0" um="-1" id="1">
            <c typ="4" id="10">
              <i cc="1" id="1" />
            </c>
            <c typ="4" id="11">
              <i cc="1" id="1" />
            </c>
          </s>
          <s ua="1" um="1" id="3">
            <c wei="2" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="46">
              <i ca="56.4�56.5" id="1">56.5</i>
            </c>
            <c wei="2" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="47">
              <i ca="12.3�12.4" id="1">12.3</i>
            </c>
            <c wei="2" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="48">
              <i ca="0.7�0.8" id="1">0.7</i>
            </c>
            <c wei="2" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="49">
              <i ca="0.6�0.7" id="1">0.7</i>
            </c>
            <c wei="2" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="50">
              <i ca="137.9�138" id="1">137.9</i>
            </c>
            <c wei="2" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="51">
              <i ca="36.1�36.2" id="1">36.1</i>
            </c>
            <c wei="2" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="52">
              <i ca="31.8�31.7" id="1">31.7</i>
            </c>
            <c wei="2" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="53">
              <i ca="14.4�14.5" id="1">14.4</i>
            </c>
            <c wei="2" maS="0" ie="1" typ="12" rv="0" mv="0" ua="1" um="1" id="26">
              <i sl="5" id="1" />
            </c>
            <c wei="2" maS="0" ie="1" typ="12" rv="0" mv="0" ua="1" um="1" id="29">
              <i sl="5" id="1" />
            </c>
            <c wei="2" maS="0" ie="1" typ="12" rv="0" mv="0" ua="1" um="1" id="30">
              <i sl="4" id="1" />
            </c>
            <c wei="2" maS="0" ie="1" typ="12" rv="0" mv="0" ua="1" um="1" id="31">
              <i sl="5" id="1" />
            </c>
            <c wei="2" maS="0" ie="1" typ="12" rv="0" mv="0" ua="1" um="1" id="27">
              <i sl="4" id="1" />
            </c>
            <c wei="2" maS="0" ie="1" typ="12" rv="0" mv="0" ua="1" um="1" id="33">
              <i sl="3" id="1" />
            </c>
            <c wei="2" maS="0" ie="1" typ="12" rv="0" mv="0" ua="1" um="1" id="34">
              <i sl="7" id="1" />
            </c>
            <c wei="2" maS="0" ie="1" typ="12" rv="0" mv="0" ua="1" um="1" id="35">
              <i sl="4" id="1" />
            </c>
          </s>
        </p>
      </item>
      <item id="879P2089" name="2.2 FNST AC 002 2011" totalMark="18" version="26" markingType="1" markingState="0" userMark="1" markerUserMark="15.0000000000" userAttempted="1" viewingTime="1097837" flagged="0" LO="2.2 Interpretation of financial statements using ratios (written)" unit="Financial Statements" quT="11">
        <p um="0" cs="1" ua="1" id="879P2089">
          <s ua="1" um="0" id="2">
            <c wei="18" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="1">
              <i ca="" id="1">a) #!#(i) Gearing#!#The gearing ratio has good down since 20X0 which could be a positive sign.#!#This could mean the company is borrowing less money.#!#Shows signs that the company has paid money back to get their borrowing ratio down.#!#Their cash flow shows signs that their cash has actually increased, so although they seem to have paid money off possible loans, they have still managed to have a positive cash flow and decreased their negative cash at the end of the year.#!##!#Acid Test Ratio#!#This has also improved since 20X0.#!#A good acid test ratio would ideally be 1:1, which shows you have enough current assets, less inventories, to pay off all liabilites at short notice. #!#This could be that they have increased there trade receivables, in which case business is improving.#!#This could also be that they have decreased their liabilities, such as trade payables. Or it could be both. #!#The cash flow shows certainly an increase of operating activities, which could suggest trade receivables have increased.#!##!#Working Capital Cycle#!#This has improved, and shows that it now takes 15 days less than in 20X0 to buy inventories, receive payments, and then make payments to payables. This means the business is working more efficiently.#!#This could be that they have agreed shorter payment terms with receiveables, or that they have offered discounts for quicker payments. #!##!#(ii) I think the bank should keep the overdraft facility because from the information above, the business has become a lot more solvent in the last year, proved by the acid test ratio and the cash flow statement. #!##!#b) If working capital cycle is reduced too much, the dangers could be:#!##!#(i) Not enough inventories are stocked, meaning loss of sales and ultimately the business becoming insolvent.#!#(ii) Paying trade payables too quickly means no money is being kept within the business to invest in other activities, such as making improvements to the business to become more efficient, or replacing old equipment within the business.</i>
            </c>
          </s>
        </p>
      </item>
      <item id="879P1523" name="2.3 FNST AC 005" totalMark="10" version="15" markingType="1" markingState="0" userMark="1" markerUserMark="4.0000000000" userAttempted="1" viewingTime="567623" flagged="0" LO="2.3 Legal and regulatory framework (written)" unit="Financial Statements" quT="11,20">
        <p um="0" cs="1" ua="1" id="879P1523">
          <s ua="1" um="0" id="2">
            <c wei="10" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="1">
              <i ca="" id="1">a) Some of the primary users of financial reports and why they might be interested are;#!#- Shareholders - they will want to see the profitability of the business and ultimately, how much investment they are likely to receive.#!#- Potential investors - they will want to also know the profitability of the business and compare with other companies to decide where best to invest their money. #!#- Trade unions - they may want to see what the investing activities are, whether restructures are likely to occurr.#!#- Managers - They may want to see how efficient the company is, whether improvements need to be made to areas such as cost of sales (i.e obtaining better supplier deals) or whether revenue needs to be boosted (i.e advertising more and creating better quality products that customers want) etc.#!##!##!#b) A general purpose finanical report should include - #!#A statement of financial position#!#A statement of comprehnesive income#!#A statment of cash flow#!#Notes to the accounts should also be made, detailing any important details that may affect any of the primary users of these reports, such as any possible restructures in the future, or adjusting events.#!##!#</i>
            </c>
            <c wei="1" maS="0" ie="1" typ="20" rv="0" mv="0" ua="0" um="0" id="2">
              <i um="0" id="1" />
            </c>
          </s>
        </p>
      </item>
    </section>
  </assessment>
  <isValid>1</isValid>
  <isPrintable>0</isPrintable>
  <qualityReview>0</qualityReview>
  <numberOfGenerations>1</numberOfGenerations>
  <requiresInvigilation>1</requiresInvigilation>
  <advanceContentDownloadTimespan>60</advanceContentDownloadTimespan>
  <automaticVerification>1</automaticVerification>
  <offlineMode>2</offlineMode>
  <requiresValidation>0</requiresValidation>
  <requiresSecureClient>1</requiresSecureClient>
  <examType>0</examType>
  <advanceDownload>0</advanceDownload>
  <gradeBoundaryData>
    <gradeBoundaries passLevelType="1" adjustedGradeBoundaries="0">
      <grade modifier="lt" value="70" description="Fail" />
      <grade modifier="gt" value="70" description="Pass" />
    </gradeBoundaries>
  </gradeBoundaryData>
  <autoViewExam>0</autoViewExam>
  <autoProgressItems>0</autoProgressItems>
  <confirmationText>
    <confirmationText />
  </confirmationText>
  <requiresConfirmationCheck>0</requiresConfirmationCheck>
  <allowCloseWithoutSubmit>0</allowCloseWithoutSubmit>
  <useSecureMarker>1</useSecureMarker>
  <annotationVersion>1</annotationVersion>
  <allowPackagingDelivery>1</allowPackagingDelivery>
  <scoreBoundaryData>
    <scoreBoundaries showScoreBoundaryColumn="1">
      <score modifier="lt" value="36" description="Significantly below" higherBoundarySet="1" />
      <score modifier="lt" value="36" description="Significantly below" higherBoundarySet="0" />
      <score modifier="gt" value="36" description="Below requirement" higherBoundarySet="1" />
      <score modifier="gt" value="60" description="Borderline" higherBoundarySet="1" />
      <score modifier="gt" value="70" description="Met" higherBoundarySet="0" />
      <score modifier="gt" value="70" description="Met" higherBoundarySet="1" />
      <score modifier="gt" value="36" description="Below requirement" higherBoundarySet="0" userCreated="1" />
      <score modifier="gt" value="60" description="Borderline" higherBoundarySet="0" userCreated="1" />
      <score modifier="gt" value="85" description="Exceeded" higherBoundarySet="1" userCreated="1" />
    </scoreBoundaries>
  </scoreBoundaryData>
  <showCandidateReportResultColumn>1</showCandidateReportResultColumn>
  <showCandidateReportPercentageColumn>1</showCandidateReportPercentageColumn>
  <certifiedAccessible>0</certifiedAccessible>
  <markingProgressVisible>1</markingProgressVisible>
  <markingProgressMode>0</markingProgressMode>
  <secureClientOperationMode>1</secureClientOperationMode>
  <examDeliverySystemStyleType>delivery</examDeliverySystemStyleType>
  <markingAutoVoidPeriod>365</markingAutoVoidPeriod>
  <showPageRequiresScrollingAlert>0</showPageRequiresScrollingAlert>
  <project ID="879" />
</assessmentDetails>
'
where ID = 1383918;
rollback

/*
<assessmentDetails>
  <assessmentID>801</assessmentID>
  <qualificationID>43</qualificationID>
  <qualificationName>Financial Statements (AQ2010)</qualificationName>
  <qualificationReference>AATQCF</qualificationReference>
  <assessmentGroupName>FNST</assessmentGroupName>
  <assessmentGroupID>349</assessmentGroupID>
  <assessmentGroupReference>CAFNST</assessmentGroupReference>
  <assessmentName>FNST pop up variant 2</assessmentName>
  <validFromDate>01 Jan 2012</validFromDate>
  <expiryDate>31 Dec 2014</expiryDate>
  <startTime>00:00:00</startTime>
  <endTime>23:59:59</endTime>
  <duration>150</duration>
  <defaultDuration>150</defaultDuration>
  <scheduledDuration>
    <value>150</value>
    <reason />
  </scheduledDuration>
  <sRBonus>0</sRBonus>
  <sRBonusMaximum>50</sRBonusMaximum>
  <externalReference>CBTAAT</externalReference>
  <passLevelValue>70</passLevelValue>
  <passLevelType>1</passLevelType>
  <status>2</status>
  <testFeedbackType>
    <passFail>0</passFail>
    <percentageMark>0</percentageMark>
    <allowSummaryFeedback>0</allowSummaryFeedback>
    <summaryFeedbackType>1</summaryFeedbackType>
    <itemSummary>0</itemSummary>
    <itemReview>0</itemReview>
    <itemReviewCorrectAnswer>0</itemReviewCorrectAnswer>
    <itemFeedback>0</itemFeedback>
    <printableSummary>0</printableSummary>
    <candidateDetails>0</candidateDetails>
    <feedbackByReference>0</feedbackByReference>
    <allowFeedbackDuringExam>0</allowFeedbackDuringExam>
    <allowFeedbackDuringSummary>0</allowFeedbackDuringSummary>
  </testFeedbackType>
  <itemFeedback>0</itemFeedback>
  <allowCalculator>0</allowCalculator>
  <lastInUseDate>10 Nov 2011 17:14:02</lastInUseDate>
  <completedScriptReview>0</completedScriptReview>
  <assessment currentSection="2" totalTime="0" currentTime="0" originalPassMark="70" originalPassType="1" passMark="70" passType="1" totalMark="150" userMark="29.000" userPercentage="19.333" passValue="0" originalGrade="Fail">
    <intro id="0" name="Introduction" currentItem="">
      <item id="879P2208" name="Copy of Introduction" totalMark="1" version="10" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
    </intro>
    <outro id="0" name="Finish" currentItem="" />
    <tools id="0" name="Tools" currentItem="">
      <item id="879P1629" name="1.1 FNST AC 001 Asker Ltd popup A" toolName="1.1 FNST AC 001 Asker Ltd popup A" totalMark="1" version="2" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1628" name="1.1 FNST AC 001 Asker Ltd popup B" toolName="1.1 FNST AC 001 Asker Ltd popup B" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1620" name="1.1 FNST DO 001 Polo Ltd popup A" toolName="1.1 FNST DO 001 Polo Ltd popup A" totalMark="1" version="2" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1619" name="1.1 FNST DO 001 Polo Ltd popup B" toolName="1.1 FNST DO 001 Polo Ltd popup B" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1368" name="1.1 FNST AC002 Cole Ltd popup A" toolName="1.1 FNST AC002 Cole Ltd popup A" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1369" name="1.1 FNST AC002 Cole Ltd popup B" toolName="1.1 FNST AC002 Cole Ltd popup B" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1370" name="1.1 FNST AC003 Allen Ltd popup A" toolName="1.1 FNST AC003 Allen Ltd popup A" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1371" name="1.1 FNST AC003 Allen Ltd popup B" toolName="1.1 FNST AC003 Allen Ltd popup B" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1372" name="1.1 FNST DO002 Hughes Ltd popup A" toolName="1.1 FNST DO002 Hughes Ltd popup A" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1373" name="1.1 FNST DO002 Hughes Ltd popup B" toolName="1.1 FNST DO002 Hughes Ltd popup B" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1374" name="1.1 FNST DO003 Burgess Ltd popup A" toolName="1.1 FNST DO003 Burgess Ltd popup A" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1375" name="1.1 FNST DO003 Burgess Ltd popup B" toolName="1.1 FNST DO003 Burgess Ltd popup B" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1376" name="1.5 FNST AC002 Austell popup A" toolName="1.5 FNST AC002 Austell popup A" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1377" name="1.5 FNST AC002 Austell popup B" toolName="1.5 FNST AC002 Austell popup B" totalMark="1" version="2" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1378" name="1.5 FNST AC003 Avon popup A" toolName="1.5 FNST AC003 Avon popup A" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1379" name="1.5 FNST AC003 Avon popup B" toolName="1.5 FNST AC003 Avon popup B" totalMark="1" version="2" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1380" name="1.5 FNST DO003 Finch popup A" toolName="1.5 FNST DO003 Finch popup A" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1381" name="1.5 FNST DO003 Finch popup B" toolName="1.5 FNST DO003 Finch popup B" totalMark="1" version="2" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1382" name="2.1 FNST AC003 popup A" toolName="2.1 FNST AC003 popup A" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1383" name="2.1 FNST AC003 popup B" toolName="2.1 FNST AC003 popup B" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1384" name="2.1 FNST AC002 popup A" toolName="2.1 FNST AC002 popup A" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1385" name="2.1 FNST AC002 popup B" toolName="2.1 FNST AC002 popup B" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1386" name="2.1 FNST AC001 popup A" toolName="2.1 FNST AC001 popup A" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1387" name="2.1 FNST AC001 popup B" toolName="2.1 FNST AC001 popup B" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1388" name="2.1 FNST DO002 popup A" toolName="2.1 FNST DO002 popup A" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1389" name="2.1 FNST DO002 popup B" toolName="2.1 FNST DO002 popup B" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1390" name="2.1 FNST DO003 popup A" toolName="2.1 FNST DO003 popup A" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1391" name="2.1 FNST DO003 popup B" toolName="2.1 FNST DO003 popup B" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1633" name="1.1 FNST AC 001 Iron Ltd popup A" toolName="1.1 FNST AC 001 Iron Ltd popup A" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1634" name="1.1 FNST AC 001 Iron Ltd popup B" toolName="1.1 FNST AC 001 Iron Ltd popup B" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1646" name="1.1 FNST AC 002 Camel A" toolName="1.1 FNST AC 002 Camel A" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1647" name="1.1 FNST AC 002 Camel B" toolName="1.1 FNST AC 002 Camel B" totalMark="1" version="2" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1653" name="1.1 FNST AC 003 Trent A" toolName="1.1 FNST AC 003 Trent A" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1654" name="1.1 FNST AC 003 Trent B" toolName="1.1 FNST AC 003 Trent B" totalMark="1" version="2" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1641" name="1.1 FNST DO 001 Shelford Ltd popup A" toolName="1.1 FNST DO 001 Shelford Ltd popup A" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1642" name="1.1 FNST DO 001 Shelford Ltd popup B" toolName="1.1 FNST DO 001 Shelford Ltd popup B" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1637" name="1.5 FNST AC 001 Mole Ltd popup A" toolName="1.5 FNST AC 001 Mole Ltd popup A" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1638" name="1.5 FNST AC 001 Madford Plc popup A" toolName="1.5 FNST AC 001 Madford Plc popup A" totalMark="1" version="2" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1649" name="1.5 FNST AC 002 Chelt A" toolName="1.5 FNST AC 002 Chelt A" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1650" name="1.5 FNST AC 002 Chelt B" toolName="1.5 FNST AC 002 Chelt B" totalMark="1" version="2" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1655" name="1.5 FNST AC 003 Kinder A" toolName="1.5 FNST AC 003 Kinder A" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1643" name="1.5 FNST DO 001 Rickling Ltd popup A" toolName="1.5 FNST DO 001 Rickling Ltd popup A" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1644" name="1.5 FNST DO 001 Stebbing Ltd popup A" toolName="1.5 FNST DO 001 Stebbing Ltd popup A" totalMark="1" version="2" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1661" name="2.1 FNST AC 001 Durham Ltd popup A" toolName="2.1 FNST AC 001 Durham Ltd popup A" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1662" name="2.1 FNST AC 001 Durham Ltd popup B" toolName="2.1 FNST AC 001 Durham Ltd popup B" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1665" name="2.1 FNST AC 002 Tweed A" toolName="2.1 FNST AC 002 Tweed A" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1666" name="2.1 FNST AC 002 Tweed B" toolName="2.1 FNST AC 002 Tweed B" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1667" name="2.1 FNST AC 003 Arun A" toolName="2.1 FNST AC 003 Arun A" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1668" name="2.1 FNST AC 003 Arun B" toolName="2.1 FNST AC 003 Arun B" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1663" name="2.1 FNST DO 001 Wembling A" toolName="2.1 FNST DO 001 Wembling A" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P1664" name="2.1 FNST DO 001 Wembling B" toolName="2.1 FNST DO 001 Wembling B" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2356" name="1.5 FNST DO 010 pop up A" toolName="1.5 FNST DO 010 pop up A" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2358" name="1.5 FNST DO 010 pop up B" toolName="1.5 FNST DO 010 pop up B" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2359" name="1.5 FNST DO 010 pop up C" toolName="1.5 FNST DO 010 pop up C" totalMark="1" version="2" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2305" name="1.5 FNST DO 013 Platt Popup B" toolName="1.5 FNST DO 013 Platt Popup B" totalMark="1" version="2" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2303" name="1.5 FNST DO 013 Platt Popup A" toolName="1.5 FNST DO 013 Platt Popup A" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2249" name="1.5 FNST AC 010 Bywell Popup A" toolName="1.5 FNST AC 010 Bywell Popup A" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2250" name="1.5 FNST AC 010 Bywell Popup B" toolName="1.5 FNST AC 010 Bywell Popup B" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2322" name="1.5 FNST AC 011 Barton Popup B" toolName="1.5 FNST AC 011 Barton Popup B" totalMark="1" version="2" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2320" name="1.5 FNST AC 011 Barton Popup A" toolName="1.5 FNST AC 011 Barton Popup A" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2258" name="1.5 FNST AC 014 Lowick Popup A" toolName="1.5 FNST AC 014 Lowick Popup A" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2257" name="1.5 FNST AC 014 Birtley Popup A" toolName="1.5 FNST AC 014 Birtley Popup A" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2334" name="1.5 FNST AC 012 Pop up B" toolName="1.5 FNST AC 012 Pop up B" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2332" name="1.5 FNST AC 012 Pop up A" toolName="1.5 FNST AC 012 Pop up A" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2288" name="1.5 FNST AC 013 Earle Popup A" toolName="1.5 FNST AC 013 Earle Popup A" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2290" name="1.5 FNST AC 013 Earle Popup B" toolName="1.5 FNST AC 013 Earle Popup B" totalMark="1" version="2" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2346" name="1.5 FNST DO 011 Hills Pop up A" toolName="1.5 FNST DO 011 Hills Pop up A" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2348" name="1.5 FNST DO 011 Grant Pop up B" toolName="1.5 FNST DO 011 Grant Pop up B" totalMark="1" version="5" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2372" name="1.5 FNST DO 012 Pop up a" toolName="1.5 FNST DO 012 Pop up a" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2374" name="1.5 FNST DO 012 Pop up B" toolName="1.5 FNST DO 012 Pop up B" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2369" name="2.1 FNST DO 010 pop up A" toolName="2.1 FNST DO 010 pop up A" totalMark="1" version="2" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2370" name="2.1 FNST DO 010 pop up B" toolName="2.1 FNST DO 010 pop up B" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2251" name="2.1 FNST AC 010 Abberwick Popup A" toolName="2.1 FNST AC 010 Abberwick Popup A" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2252" name="2.1 FNST AC 010 Abberwick Popup B" toolName="2.1 FNST AC 010 Abberwick Popup B" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2307" name="2.1 FNST DO 013 Court Popup A" toolName="2.1 FNST DO 013 Court Popup A" totalMark="1" version="2" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2309" name="2.1 FNST DO 013 Court Popup B" toolName="2.1 FNST DO 013 Court Popup B" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2260" name="2.1 FNST AC 014 Belsay Popup B" toolName="2.1 FNST AC 014 Belsay Popup B" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2259" name="2.1 FNST AC 014 Belsay Popup A" toolName="2.1 FNST AC 014 Belsay Popup A" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2326" name="2.1 FNST AC 011 Anick Popup B" toolName="2.1 FNST AC 011 Anick Popup B" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2324" name="2.1 FNST AC 011 Anick Popup A" toolName="2.1 FNST AC 011 Anick Popup A" totalMark="1" version="2" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2381" name="2.1 FNST DO 012 Pop up B" toolName="2.1 FNST DO 012 Pop up B" totalMark="1" version="2" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2380" name="2.1 FNST DO 012 Pop up A" toolName="2.1 FNST DO 012 Pop up A" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2352" name="2.1 FNST DO 011 Leigh Pop up B" toolName="2.1 FNST DO 011 Leigh Pop up B" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2351" name="2.1 FNST DO 011 Leigh Pop up A" toolName="2.1 FNST DO 011 Leigh Pop up A" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2294" name="2.1 FNST AC 013 Lucker Popup B" toolName="2.1 FNST AC 013 Lucker Popup B" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2292" name="2.1 FNST AC 013 Lucker Popup A" toolName="2.1 FNST AC 013 Lucker Popup A" totalMark="1" version="2" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2330" name="2.1 FNST AC 012 Glanton Pop up B" toolName="2.1 FNST AC 012 Glanton Pop up B" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2329" name="2.1 FNST AC 012 Glanton Pop up A" toolName="2.1 FNST AC 012 Glanton Pop up A" totalMark="1" version="3" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
      <item id="879P2394" name="1.5 FNST AC 003 Kinder B v2" toolName="1.5 FNST AC 003 Kinder B v2" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
    </tools>
    <section id="1" name="1" passLevelValue="60" passLevelType="1" itemsToMark="0" currentItem="879P1828" fixed="0" totalMark="90" userMark="80.000" userPercentage="88.889" passValue="0">
      <item id="879P1324" name="P 1.1 FNST AC003" totalMark="19" version="14" markingType="0" markingState="0" userMark="1" markerUserMark="19.0000000000" userAttempted="1" viewingTime="639522" flagged="0" LO="1.1 Prepare a statement of comprehensive income/financial position/change in equity/cash flow" unit="Financial Statements" quT="20">
        <p um="1" cs="1" ua="1" id="879P1324">
          <s ua="0" um="-1" id="1">
            <c typ="4" id="23">
              <i cc="5" id="1" />
            </c>
            <c typ="4" id="24">
              <i cc="3" id="1" />
            </c>
          </s>
          <s ua="1" um="1" id="2">
            <c wei="14" maS="0" ie="1" typ="20" rv="0" mv="0" ua="1" um="1" id="1">
              <i um="1" ia="1" id="1">&lt;table extensionName="linkedproforma"&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="72824"&gt;72824&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="3" cor="-[s2c3i1c2r1]-[s2c3i1c2r2]-[s2c3i1c2r3]-[s2c3i1c2r4]�-33509"&gt;-33509&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="2" cor="-[s2c4i1c2r1]-[s2c4i1c2r2]�-12393"&gt;-12393&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="2" cor="-[s2c5i1c2r1]-[s2c5i1c2r2]-[s2c5i1c2r3]"&gt;-9472&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="2" cor="-900"&gt;-900&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="2" cor="-[s2c15i1c2r1]-[s2c15i1c2r2]�-3538"&gt;-3538&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="2" cor="5000"&gt;5000&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;/table&gt;</i>
            </c>
            <c wei="2" maS="0" ie="1" typ="20" rv="0" mv="0" ua="1" um="1" id="4">
              <i um="1" ia="1" id="1">&lt;table extensionName="linkedproforma"&gt;&lt;r&gt;&lt;c h="0" tp="3" cor="Distribution costs "&gt;Distribution costs &lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="10937"&gt;10937&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="3" cor="Wages and salaries"&gt;Wages and salaries&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="1456"&gt;1456&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;/table&gt;</i>
            </c>
            <c wei="3" maS="0" ie="1" typ="20" rv="0" mv="0" ua="1" um="1" id="3">
              <i um="1" ia="1" id="1">&lt;table extensionName="linkedproforma"&gt;&lt;r&gt;&lt;c h="0" tp="3" cor="Opening inventories"&gt;Opening inventories&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="12583"&gt;12583&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="3" cor="Purchases"&gt;Purchases&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="35118"&gt;35118&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="3" cor="Closing inventories"&gt;Closing inventories&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="-14192"&gt;-14192&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;/table&gt;</i>
            </c>
            <c wei="2" maS="0" ie="1" typ="20" rv="0" mv="0" ua="1" um="1" id="15">
              <i um="1" ia="1" id="1">&lt;table extensionName="linkedproforma"&gt;&lt;r&gt;&lt;c h="0" tp="3" cor="Current year"&gt;Current year&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="3546"&gt;3546&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="3" cor="Previous year"&gt;Previous year&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="-8"&gt;-8&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;/table&gt;</i>
            </c>
            <c wei="2" maS="0" ie="1" typ="20" rv="0" mv="0" ua="1" um="1" id="5">
              <i um="1" ia="1" id="1">&lt;table extensionName="linkedproforma"&gt;&lt;r&gt;&lt;c h="0" tp="3" cor="Administrative expenses"&gt;Administrative expenses&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="7288"&gt;7288&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="3" cor="Wages and salaries"&gt;Wages and salaries&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="2184"&gt;2184&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;/table&gt;</i>
            </c>
          </s>
          <s ua="1" um="1" id="4">
            <c wei="5" maS="0" ie="1" typ="20" rv="0" mv="0" ua="1" um="1" id="1">
              <i um="1" ia="1" id="1">&lt;table&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="18000"&gt;18000&lt;/c&gt;&lt;c h="0" tp="1" mark="0" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="16482"&gt;16482&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="0" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="5000�[s2c1i1c2r11]~b"&gt;5000&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="13012�[s2c1i1c2r1]+[s2c1i1c2r2]+[s2c1i1c2r4]+[s2c1i1c2r5]+[s2c1i1c2r7]+[s2c1i1c2r9]~b"&gt;13012&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="0" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="0" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="-1600"&gt;-1600&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="0" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="0" cor=""&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="0" cor=""&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;/table&gt;</i>
            </c>
          </s>
        </p>
      </item>
      <item id="879P1316" name="P 1.2 FNST AC003" totalMark="17" version="9" markingType="0" markingState="0" userMark="1" markerUserMark="17.0000000000" userAttempted="1" viewingTime="529870" flagged="0" LO="1.2 Prepare a statement of comprehensive income/financial position/change in equity/cash flow" unit="Financial Statements" quT="20">
        <p um="1" cs="1" ua="1" id="879P1316">
          <s ua="0" um="-1" id="1">
            <c typ="4" id="23">
              <i cc="1" id="1" />
            </c>
            <c typ="4" id="24">
              <i cc="1" id="1" />
            </c>
          </s>
          <s ua="1" um="1" id="3">
            <c wei="10" maS="0" ie="1" typ="20" rv="0" mv="0" ua="1" um="1" id="5">
              <i um="1" ia="1" id="1">&lt;table extensionName="linkedproforma"&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="3" cor="Share capital"&gt;Retained earnings&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="18000"&gt;27894&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="3" cor="Retained earnings"&gt;Revaluation reserve&lt;/c&gt;&lt;c h="0" tp="1" mark="3" cor="27894�[s3c9i1c2r1]+[s3c9i1c2r2]+[s3c9i1c2r3]"&gt;5000&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="3" cor="Revaluation reserve"&gt;Share capital&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="5000"&gt;18000&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="3" cor="Bank loans"&gt;Bank loans&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="20000"&gt;20000&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor=""&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="3" cor="Trade and other payables"&gt;Trade and other payables&lt;/c&gt;&lt;c h="0" tp="1" mark="3" cor="3651�[s3c10i1c2r1]+[s3c10i1c2r2]+[s3c10i1c2r3]"&gt;3651&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="3" cor="Tax liability"&gt;Tax liability&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="3546"&gt;3546&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;/table&gt;</i>
            </c>
            <c wei="3" maS="0" ie="1" typ="20" rv="0" mv="0" ua="1" um="1" id="10">
              <i um="1" ia="1" id="1">&lt;table extensionName="linkedproforma"&gt;&lt;r&gt;&lt;c h="0" tp="3" cor="Trade and other payables"&gt;Trade and other payables&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="2937"&gt;2937&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="3" cor="Accruals - trial balance"&gt;Accruals - trial balance&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="414"&gt;414&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="3" cor="Interest accrual"&gt;Interest accrual&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="300"&gt;300&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;/table&gt;</i>
            </c>
            <c wei="3" maS="0" ie="1" typ="20" rv="0" mv="0" ua="1" um="1" id="9">
              <i um="1" ia="1" id="1">&lt;table extensionName="linkedproforma"&gt;&lt;r&gt;&lt;c h="0" tp="3" cor="Retained earnings at 1 January 20X0"&gt;Retained earnings at 1 January 20X0&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="16482"&gt;16482&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="3" cor="Total profit for the year"&gt;Dividends paid&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="13012"&gt;-1600&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="3" cor="Dividends paid"&gt;Total profit for the year&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="-1600"&gt;13012&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;/table&gt;</i>
            </c>
            <c wei="2" maS="0" ie="1" typ="20" rv="0" mv="0" ua="1" um="1" id="7">
              <i um="1" ia="1" id="1">&lt;table extensionName="linkedproforma"&gt;&lt;r&gt;&lt;c h="0" tp="3" cor="Trade and other receivables"&gt;Trade and other receivables&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="8846"&gt;8846&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="3" cor="Prepayments - trial balance"&gt;Prepayments - trial balance&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="387"&gt;387&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;/table&gt;</i>
            </c>
            <c wei="3" maS="0" ie="1" typ="20" rv="0" mv="0" ua="1" um="1" id="6">
              <i um="1" ia="1" id="1">&lt;table extensionName="linkedproforma"&gt;&lt;r&gt;&lt;c h="0" tp="3" cor="Property, plant and equipment - cost"&gt;Property, plant and equipment - cost&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="70649"&gt;70649&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="3" cor="Accumulated depreciation"&gt;Revaluation&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="-21267"&gt;5000&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="3" cor="Revaluation"&gt;Accumulated depreciation&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="5000"&gt;-21267&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;/table&gt;</i>
            </c>
            <c wei="7" maS="0" ie="1" typ="20" rv="0" mv="0" ua="1" um="1" id="24">
              <i um="1" ia="1" id="1">&lt;table extensionName="linkedproforma"&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="0" cor=""&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="0" cor=""&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="3" cor="Property, plant and equipment"&gt;Property, plant and equipment&lt;/c&gt;&lt;c h="0" tp="1" mark="3" cor="54382�[s3c6i1c2r1]+[s3c6i1c2r2]+[s3c6i1c2r3]"&gt;54382&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="0" cor=""&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="3" cor="Inventories"&gt;Inventories&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="14192"&gt;14192&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="3" cor="Trade and other receivables"&gt;Trade and other receivables&lt;/c&gt;&lt;c h="0" tp="1" mark="2" cor="9233�[s3c7i1c2r1]+[s3c7i1c2r2]"&gt;9233&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="3" cor="Cash and cash equivalents"&gt;Cash and cash equivalents&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="284"&gt;284&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;/table&gt;</i>
            </c>
          </s>
        </p>
      </item>
      <item id="879P1698" name="1.3 FNST AC 010" totalMark="12" version="22" markingType="1" markingState="0" userMark="0" markerUserMark="10.0000000000" userAttempted="1" viewingTime="403885" flagged="0" LO="1.3 Knowledge and understanding of International Financial Reporting Standards (written)" unit="Financial Statements" quT="11">
        <p um="0" cs="1" ua="1" id="879P1698">
          <s ua="1" um="0" id="2">
            <c wei="12" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="1">
              <i ca="" id="1">a) Property, Plant &amp; Equipment is defined as assets which can give future economic benefits to the business and ensure more efficient workings, such as vans, industrial machinery and computers.#!##!#b) The cost of an item of property, plant &amp; equipment should be recognised as an asset when:#!##!#i) the cost of the asset can be measured reliably#!#ii) it is probable that future economic benefits will flow to the entity.#!##!#c) The total cost that must be recognised is �32,010. This includes the purchase price of the machine and all incidental costs associated with the purchase except for the cost of the maintenance contract.#!#</i>
            </c>
          </s>
        </p>
      </item>
      <item id="879P1828" name="1.4 FNST DO 012" totalMark="12" version="24" markingType="0" markingState="0" userMark="1" markerUserMark="8.0000000000" userAttempted="1" viewingTime="441294" flagged="0" LO="1.4 Application of reporting standards" unit="Financial Statements" quT="10">
        <p um="0.666666666666667" cs="1" ua="1" id="879P1828">
          <s ua="1" um="1" id="1">
            <c wei="2" maS="1" ie="1" typ="10" rv="0" mv="0" ua="1" um="1" id="15">
              <i ac="A" sl="0" id="1" />
              <i ca="1" ac="B" sl="1" id="2" />
              <i ac="C" sl="0" id="3" />
              <i ac="D" sl="0" id="4" />
            </c>
          </s>
          <s ua="1" um="1" id="2">
            <c wei="2" maS="1" ie="1" typ="10" rv="0" mv="0" ua="1" um="1" id="5">
              <i ac="A" sl="0" id="1" />
              <i ac="B" sl="0" id="2" />
              <i ca="1" ac="C" sl="1" id="3" />
              <i ac="D" sl="0" id="4" />
            </c>
          </s>
          <s ua="1" um="0" id="3">
            <c wei="2" maS="1" ie="1" typ="10" rv="0" mv="0" ua="1" um="0" id="31">
              <i ac="A" sl="1" id="1" />
              <i ac="B" sl="0" id="2" />
              <i ac="C" sl="0" id="3" />
              <i ca="1" ac="D" sl="0" id="4" />
            </c>
          </s>
          <s ua="1" um="1" id="4">
            <c wei="2" maS="1" ie="1" typ="10" rv="0" mv="0" ua="1" um="1" id="14">
              <i ac="A" sl="0" id="1" />
              <i ca="1" ac="B" sl="1" id="2" />
              <i ac="C" sl="0" id="3" />
              <i ac="D" sl="0" id="4" />
            </c>
          </s>
          <s ua="1" um="0" id="5">
            <c wei="2" maS="1" ie="1" typ="10" rv="0" mv="0" ua="1" um="0" id="5">
              <i ac="A" sl="1" id="1" />
              <i ca="1" ac="B" sl="0" id="2" />
              <i ac="C" sl="0" id="3" />
              <i ac="D" sl="0" id="4" />
            </c>
          </s>
          <s ua="1" um="1" id="6">
            <c wei="2" maS="1" ie="1" typ="10" rv="0" mv="0" ua="1" um="1" id="5">
              <i ca="1" ac="A" sl="1" id="1" />
              <i ac="B" sl="0" id="2" />
            </c>
          </s>
        </p>
      </item>
      <item id="879P1997" name="1.5 FNST AC 013" totalMark="30" version="12" markingType="0" markingState="0" userMark="1" markerUserMark="26.0000000000" userAttempted="1" viewingTime="1136525" flagged="0" LO="1.5 Drafting consolidated financial statements" unit="Financial Statements" quT="20">
        <p um="0.866666666666667" cs="1" ua="1" id="879P1997">
          <s ua="0" um="-1" id="1">
            <c typ="4" id="17">
              <i cc="5" id="1" />
            </c>
            <c typ="4" id="19">
              <i cc="5" id="1" />
            </c>
          </s>
          <s ua="1" um="0.87" id="2">
            <c wei="17" maS="0" ie="1" typ="20" rv="0" mv="0" ua="1" um="0.82" id="2">
              <i um="0.823529411764706" ia="1" id="1">&lt;table extensionName="linkedproforma"&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="0" cor=""&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="0" cor=""&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="1400"&gt;1400&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="300"&gt;300&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="5" cor="3976�[s2c5i1c2r1]+[s2c5i1c2r2]+[s2c5i1c2r3]"&gt;4241&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="7" cor="[s2c4i1c2r1]+[s2c4i1c2r2]+[s2c4i1c2r3]+[s2c4i1c2r4]+[s2c4i1c2r5]�592"&gt;592&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="768"&gt;768&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="2" cor="1676"&gt;1676&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;/table&gt;</i>
            </c>
            <c wei="7" maS="0" ie="1" typ="20" rv="0" mv="0" ua="1" um="1" id="4">
              <i um="1" ia="1" id="1">&lt;table extensionName="linkedproforma"&gt;&lt;r&gt;&lt;c h="0" tp="3" cor="Share capital - attributable to NCI"&gt;Retained earnings  - attributable to NCI&lt;/c&gt;&lt;c h="0" tp="1" mark="2" cor="240"&gt;140&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="3" cor="Share premium - attributable to NCI"&gt;Revaluation reserve - attributable to NCI&lt;/c&gt;&lt;c h="0" tp="1" mark="2" cor="180"&gt;32&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="3" cor="Revaluation reserve - attributable to NCI"&gt;Share capital - attributable to NCI&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="32"&gt;240&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="3" cor="Retained earnings  - attributable to NCI"&gt;Share premium - attributable to NCI&lt;/c&gt;&lt;c h="0" tp="1" mark="2" cor="140"&gt;180&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;/table&gt;</i>
            </c>
            <c wei="13" maS="0" ie="1" typ="20" rv="0" mv="0" ua="1" um="0.92" id="13">
              <i um="0.923076923076923" ia="1" id="1">&lt;table extensionName="linkedproforma"&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="0" cor=""&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="0" cor=""&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="9" cor="[s2c3i1c2r1]+[s2c3i1c2r2]+[s2c3i1c2r3]+[s2c3i1c2r4]+[s2c3i1c2r5]+[s2c3i1c2r6]�367"&gt;623&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="2" cor="6071"&gt;6071&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0" tp="1" mark="2" cor="2274"&gt;2274&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;/table&gt;</i>
            </c>
            <c wei="5" maS="0" ie="1" typ="20" rv="0" mv="0" ua="1" um="0.4" id="5">
              <i um="0.4" ia="1" id="1">&lt;table extensionName="linkedproforma"&gt;&lt;r&gt;&lt;c h="0" tp="3" cor="Earle Plc"&gt;Earle Plc&lt;/c&gt;&lt;c h="0" tp="1" mark="2" cor="3941"&gt;3941&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="3" cor="Brandon Ltd - attributable to Earle Plc"&gt;Brandon Ltd - attributable to Earle Plc&lt;/c&gt;&lt;c h="0" tp="1" mark="2" cor="112"&gt;140&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="3" cor="Impairment of goodwill"&gt;Revaluation &lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="-77"&gt;160&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;/table&gt;</i>
            </c>
            <c wei="9" maS="0" ie="1" typ="20" rv="0" mv="0" ua="1" um="0.89" id="3">
              <i um="0.888888888888889" ia="1" id="1">&lt;table extensionName="linkedproforma"&gt;&lt;r&gt;&lt;c h="0" tp="3" cor="Share capital - attributable to Earle Plc"&gt;Price paid&lt;/c&gt;&lt;c h="0" tp="1" mark="2" cor="-960"&gt;2700&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="3" cor="Share premium - attributable to Earle Plc"&gt;Retained earnings - attributable to Earle Plc&lt;/c&gt;&lt;c h="0" tp="1" mark="2" cor="-720"&gt;-448&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="3" cor="Revaluation reserve - attributable to Earle Plc"&gt;Revaluation reserve - attributable to Earle Plc&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="-128"&gt;128&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="3" cor="Retained earnings - attributable to Earle Plc"&gt;Share capital - attributable to Earle Plc&lt;/c&gt;&lt;c h="0" tp="1" mark="2" cor="-448"&gt;-960&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="3" cor="Price paid"&gt;Share premium - attributable to Earle Plc&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="2700"&gt;-720&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0" tp="3" cor="Impairment of goodwill"&gt;Impairment of goodwill&lt;/c&gt;&lt;c h="0" tp="1" mark="1" cor="-77"&gt;-77&lt;/c&gt;&lt;/r&gt;&lt;r&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;c h="0"&gt;&lt;/c&gt;&lt;/r&gt;&lt;/table&gt;</i>
            </c>
          </s>
        </p>
      </item>
    </section>
    <section id="2" name="2" passLevelValue="60" passLevelType="1" itemsToMark="0" currentItem="879P1523" fixed="0" totalMark="60" userMark="51.000" userPercentage="85.000" passValue="0">
      <item id="879P1734" name="2.1 FNST DO 010" totalMark="32" version="21" markingType="0" markingState="0" userMark="1" markerUserMark="32.0000000000" userAttempted="1" viewingTime="562007" flagged="0" LO="2.1 Analysis of financial statements using ratios" unit="Financial Statements" quT="11,12">
        <p um="1" cs="1" ua="1" id="879P1734">
          <s ua="0" um="-1" id="1">
            <c typ="4" id="10">
              <i cc="1" id="1" />
            </c>
            <c typ="4" id="11">
              <i cc="1" id="1" />
            </c>
          </s>
          <s ua="1" um="1" id="3">
            <c wei="2" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="46">
              <i ca="56.4�56.5" id="1">56.5</i>
            </c>
            <c wei="2" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="47">
              <i ca="12.3�12.4" id="1">12.3</i>
            </c>
            <c wei="2" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="48">
              <i ca="0.7�0.8" id="1">0.7</i>
            </c>
            <c wei="2" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="49">
              <i ca="0.6�0.7" id="1">0.7</i>
            </c>
            <c wei="2" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="50">
              <i ca="137.9�138" id="1">137.9</i>
            </c>
            <c wei="2" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="51">
              <i ca="36.1�36.2" id="1">36.1</i>
            </c>
            <c wei="2" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="52">
              <i ca="31.8�31.7" id="1">31.7</i>
            </c>
            <c wei="2" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="1" id="53">
              <i ca="14.4�14.5" id="1">14.4</i>
            </c>
            <c wei="2" maS="0" ie="1" typ="12" rv="0" mv="0" ua="1" um="1" id="26">
              <i sl="5" id="1" />
            </c>
            <c wei="2" maS="0" ie="1" typ="12" rv="0" mv="0" ua="1" um="1" id="29">
              <i sl="5" id="1" />
            </c>
            <c wei="2" maS="0" ie="1" typ="12" rv="0" mv="0" ua="1" um="1" id="30">
              <i sl="4" id="1" />
            </c>
            <c wei="2" maS="0" ie="1" typ="12" rv="0" mv="0" ua="1" um="1" id="31">
              <i sl="5" id="1" />
            </c>
            <c wei="2" maS="0" ie="1" typ="12" rv="0" mv="0" ua="1" um="1" id="27">
              <i sl="4" id="1" />
            </c>
            <c wei="2" maS="0" ie="1" typ="12" rv="0" mv="0" ua="1" um="1" id="33">
              <i sl="3" id="1" />
            </c>
            <c wei="2" maS="0" ie="1" typ="12" rv="0" mv="0" ua="1" um="1" id="34">
              <i sl="7" id="1" />
            </c>
            <c wei="2" maS="0" ie="1" typ="12" rv="0" mv="0" ua="1" um="1" id="35">
              <i sl="4" id="1" />
            </c>
          </s>
        </p>
      </item>
      <item id="879P2089" name="2.2 FNST AC 002 2011" totalMark="18" version="26" markingType="1" markingState="0" userMark="1" markerUserMark="15.0000000000" userAttempted="1" viewingTime="1097837" flagged="0" LO="2.2 Interpretation of financial statements using ratios (written)" unit="Financial Statements" quT="11">
        <p um="0" cs="1" ua="1" id="879P2089">
          <s ua="1" um="0" id="2">
            <c wei="18" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="1">
              <i ca="" id="1">a) #!#(i) Gearing#!#The gearing ratio has good down since 20X0 which could be a positive sign.#!#This could mean the company is borrowing less money.#!#Shows signs that the company has paid money back to get their borrowing ratio down.#!#Their cash flow shows signs that their cash has actually increased, so although they seem to have paid money off possible loans, they have still managed to have a positive cash flow and decreased their negative cash at the end of the year.#!##!#Acid Test Ratio#!#This has also improved since 20X0.#!#A good acid test ratio would ideally be 1:1, which shows you have enough current assets, less inventories, to pay off all liabilites at short notice. #!#This could be that they have increased there trade receivables, in which case business is improving.#!#This could also be that they have decreased their liabilities, such as trade payables. Or it could be both. #!#The cash flow shows certainly an increase of operating activities, which could suggest trade receivables have increased.#!##!#Working Capital Cycle#!#This has improved, and shows that it now takes 15 days less than in 20X0 to buy inventories, receive payments, and then make payments to payables. This means the business is working more efficiently.#!#This could be that they have agreed shorter payment terms with receiveables, or that they have offered discounts for quicker payments. #!##!#(ii) I think the bank should keep the overdraft facility because from the information above, the business has become a lot more solvent in the last year, proved by the acid test ratio and the cash flow statement. #!##!#b) If working capital cycle is reduced too much, the dangers could be:#!##!#(i) Not enough inventories are stocked, meaning loss of sales and ultimately the business becoming insolvent.#!#(ii) Paying trade payables too quickly means no money is being kept within the business to invest in other activities, such as making improvements to the business to become more efficient, or replacing old equipment within the business.</i>
            </c>
          </s>
        </p>
      </item>
      <item id="879P1523" name="2.3 FNST AC 005" totalMark="10" version="15" markingType="1" markingState="0" userMark="1" markerUserMark="4.0000000000" userAttempted="1" viewingTime="567623" flagged="0" LO="2.3 Legal and regulatory framework (written)" unit="Financial Statements" quT="11,20">
        <p um="0" cs="1" ua="1" id="879P1523">
          <s ua="1" um="0" id="2">
            <c wei="10" ca="" maS="0" ie="1" typ="11" rv="0" mv="0" ua="1" um="0" id="1">
              <i ca="" id="1">a) Some of the primary users of financial reports and why they might be interested are;#!#- Shareholders - they will want to see the profitability of the business and ultimately, how much investment they are likely to receive.#!#- Potential investors - they will want to also know the profitability of the business and compare with other companies to decide where best to invest their money. #!#- Trade unions - they may want to see what the investing activities are, whether restructures are likely to occurr.#!#- Managers - They may want to see how efficient the company is, whether improvements need to be made to areas such as cost of sales (i.e obtaining better supplier deals) or whether revenue needs to be boosted (i.e advertising more and creating better quality products that customers want) etc.#!##!##!#b) A general purpose finanical report should include - #!#A statement of financial position#!#A statement of comprehnesive income#!#A statment of cash flow#!#Notes to the accounts should also be made, detailing any important details that may affect any of the primary users of these reports, such as any possible restructures in the future, or adjusting events.#!##!#</i>
            </c>
            <c wei="1" maS="0" ie="1" typ="20" rv="0" mv="0" ua="0" um="0" id="2">
              <i um="0" id="1" />
            </c>
          </s>
        </p>
      </item>
    </section>
  </assessment>
  <isValid>1</isValid>
  <isPrintable>0</isPrintable>
  <qualityReview>0</qualityReview>
  <numberOfGenerations>1</numberOfGenerations>
  <requiresInvigilation>1</requiresInvigilation>
  <advanceContentDownloadTimespan>60</advanceContentDownloadTimespan>
  <automaticVerification>1</automaticVerification>
  <offlineMode>2</offlineMode>
  <requiresValidation>0</requiresValidation>
  <requiresSecureClient>1</requiresSecureClient>
  <examType>0</examType>
  <advanceDownload>0</advanceDownload>
  <gradeBoundaryData>
    <gradeBoundaries passLevelType="1" adjustedGradeBoundaries="0">
      <grade modifier="lt" value="70" description="Fail" />
      <grade modifier="gt" value="70" description="Pass" />
    </gradeBoundaries>
  </gradeBoundaryData>
  <autoViewExam>0</autoViewExam>
  <autoProgressItems>0</autoProgressItems>
  <confirmationText>
    <confirmationText />
  </confirmationText>
  <requiresConfirmationCheck>0</requiresConfirmationCheck>
  <allowCloseWithoutSubmit>0</allowCloseWithoutSubmit>
  <useSecureMarker>1</useSecureMarker>
  <annotationVersion>1</annotationVersion>
  <allowPackagingDelivery>1</allowPackagingDelivery>
  <scoreBoundaryData>
    <scoreBoundaries showScoreBoundaryColumn="1">
      <score modifier="lt" value="36" description="Significantly below" higherBoundarySet="1" />
      <score modifier="lt" value="36" description="Significantly below" higherBoundarySet="0" />
      <score modifier="gt" value="36" description="Below requirement" higherBoundarySet="1" />
      <score modifier="gt" value="60" description="Borderline" higherBoundarySet="1" />
      <score modifier="gt" value="70" description="Met" higherBoundarySet="0" />
      <score modifier="gt" value="70" description="Met" higherBoundarySet="1" />
      <score modifier="gt" value="36" description="Below requirement" higherBoundarySet="0" userCreated="1" />
      <score modifier="gt" value="60" description="Borderline" higherBoundarySet="0" userCreated="1" />
      <score modifier="gt" value="85" description="Exceeded" higherBoundarySet="1" userCreated="1" />
    </scoreBoundaries>
  </scoreBoundaryData>
  <showCandidateReportResultColumn>1</showCandidateReportResultColumn>
  <showCandidateReportPercentageColumn>1</showCandidateReportPercentageColumn>
  <certifiedAccessible>0</certifiedAccessible>
  <markingProgressVisible>1</markingProgressVisible>
  <markingProgressMode>0</markingProgressMode>
  <secureClientOperationMode>1</secureClientOperationMode>
  <examDeliverySystemStyleType>delivery</examDeliverySystemStyleType>
  <markingAutoVoidPeriod>365</markingAutoVoidPeriod>
  <showPageRequiresScrollingAlert>0</showPageRequiresScrollingAlert>
  <project ID="879" />
</assessmentDetails>

