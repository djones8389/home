SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

select top 5 examSessionId
	, examStateInformation 
	, s.r.query('(.)[1]')
	, s.r.query('(..)[1]')
	, KeyCode
	
  from WAREHOUSE_ExamSessionTable_Shreded 
	
	cross apply examStateInformation.nodes('stateChangeInformation/reason') s (r)
	
where previousExamState = 10
	and cast(s.r.query('(.)[1]') as nvarchar(max)) = '<reason>4</reason>';

--------------------------------------------------------------------------------------------------------------

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

select top 5 examSessionId
	, examStateInformation 
	, s.r.query('(.)[1]')
	, s.r.query('(..)[1]')
	, KeyCode
	
  from WAREHOUSE_ExamSessionTable_Shreded 
	
	cross apply examStateInformation.nodes('stateChangeInformation/reason') s (r)
	
where previousExamState = 10
	and cast(s.r.query('(.)[1]') as nvarchar(max)) = '<reason>16</reason>';