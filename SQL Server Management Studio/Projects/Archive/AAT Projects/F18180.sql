select distinct

--    UT.ID
      Forename
      ,Surname
      ,ut.Retired
      ,ct.CentreName
      ,ct.CentreCode
      ,RT.Name as RoleName
--    ,RT.ID as RoleID
      ,IB.QualificationName
      --,PT.Name as PermissionName
      ,ut.Email
from UserTable as UT

inner join AssignedUserRolesTable as AURT
on AURT.UserID = UT.ID

inner join CentreTable as CT
on CT.ID = AURT.CentreID

inner join RolesTable as RT
on RT.ID = AURT.RoleID

--inner join RolePermissionsTable as RPT
--on RPT.RoleID = RT.ID

inner join UserQualificationsTable as UQT
on UQT.UserID = UT.ID

inner join IB3QualificationLookup as IB
on IB.ID = UQT.QualificationID

--inner join PermissionsTable as PT
--on PT.ID = RPT.PermissionID

where --CT.CentreName like '%Kaplan%'
       rt.Name != 'Base'
     -- and pt.Name != 'Base'
      and CandidateRef = ''
      and Email not like '%@btl.com'
      and CentreName != 'Global Centre'
      --and UT.Retired = 0
      --and Surname = 'Essop'
order by CentreName, Forename, Surname, RoleName, QualificationName asc
	