

SELECT EST.ID, EST.KeyCode, EST.examState, UT.Forename, UT.Surname, CT.CentreName, SCET.examName
  FROM ExamSessionTable as EST

  Inner Join ScheduledExamsTable as SCET
  on SCET.ID = EST.ScheduledExamID
  
  Inner Join UserTable as UT
  on UT.ID = EST.UserID

  Inner Join CentreTable as CT
  on CT.ID = SCET.CentreID




select UT.* from AssignedUserRolesTable

	inner join CentreTable as CT
	on CT.ID = AssignedUserRolesTable.CentreID
	
	inner join UserTable as UT
	on UT.ID = AssignedUserRolesTable.UserID
	
where CentreName = 'Botswana Accountancy College'
	order by DOB asc
	
	
	
	
SELECT *
FROM (

	SELECT COUNT(CandidateRef) AS NumberOfCandidates
		   , CentreName as CentreName
		   , CentreID
		FROM UserTable

		INNER JOIN AssignedUserRolesTable
		ON AssignedUserRolesTable.UserID = UserTable.ID

		INNER JOIN CentreTable
		ON CentreTable.ID = AssignedUserRolesTable.CentreID

	where LEN(candidateRef) > 0
	GROUP BY CentreName, CentreID
	
	
	) AS Main

WHERE NumberOfCandidates > 1 
order by NumberOfCandidates desc