select CAST(structureXML as xml), ProjectId from SharedLibraryTable where ProjectId in (875, 886) order by ProjectId asc;

select * from ItemGraphicTable where ID like '%875p2377%'

--875 = table_recstatement.swf 
--886 = table_journal.swf 

select * from ProjectManifestTable where ProjectId in (875) and name = 'table_recstatement.swf';
--<item id="56" name="table_recstatement.swf" visible="1" type="swf" source="55" />
--<item original="table_recstatement" Type="swf" id="56" />

select * from ProjectManifestTable where ProjectId in (886) and name = 'table_journal.swf';
--<item id="56" name="table_journal.swf" visible="1" type="swf" source="55" />
--<item original="table_journal" Type="swf" id="56" />



update SharedLibraryTable
set structureXML = '<sharedLibrary>
  <item id="3" name="red_dot_divider.fla" visible="0" type="fla" source="" />
  <item id="4" name="red_dot_divider.swf" visible="1" type="swf" source="3" />
  <item id="5" name="black_line.fla" visible="0" type="fla" source="" />
  <item id="6" name="black_line.swf" visible="1" type="swf" source="5" />
  <item id="7" name="button_viewcalculator.fla" visible="0" type="fla" source="" />
  <item id="8" name="button_viewcalculator.swf" visible="1" type="swf" source="7" />
  <item id="11" name="table_highlight_table.fla" visible="0" type="fla" source="" />
  <item id="12" name="table_highlight_table.swf" visible="1" type="swf" source="11" />
  <item id="21" name="table_apportionment.fla" visible="0" type="fla" source="" />
  <item id="22" name="table_apportionment.swf" visible="1" type="swf" source="21" />
  <item id="23" name="table_cashledger.fla" visible="0" type="fla" source="" />
  <item id="24" name="table_cashledger.swf" visible="1" type="swf" source="23" />
  <item id="25" name="table_contribution.fla" visible="0" type="fla" source="" />
  <item id="26" name="table_contribution.swf" visible="1" type="swf" source="25" />
  <item id="27" name="table_extendedtrialbalance.fla" visible="0" type="fla" source="" />
  <item id="28" name="table_extendedtrialbalance.swf" visible="1" type="swf" source="27" />
  <item id="29" name="table_extendedtrialbalanceprofit.fla" visible="0" type="fla" source="" />
  <item id="30" name="table_extendedtrialbalanceprofit.swf" visible="1" type="swf" source="29" />
  <item id="31" name="table_ledgerincomplete.fla" visible="0" type="fla" source="" />
  <item id="32" name="table_ledgerincomplete.swf" visible="1" type="swf" source="31" />
  <item id="33" name="table_ledgermc.fla" visible="0" type="fla" source="" />
  <item id="34" name="table_ledgermc.swf" visible="1" type="swf" source="33" />
  <item id="35" name="table_ledgertotals.fla" visible="0" type="fla" source="" />
  <item id="36" name="table_ledgertotals.swf" visible="1" type="swf" source="35" />
  <item id="37" name="table_limitingfactorstable.fla" visible="0" type="fla" source="" />
  <item id="38" name="table_limitingfactorstable.swf" visible="1" type="swf" source="37" />
  <item id="39" name="table_netpresentvalue.fla" visible="0" type="fla" source="" />
  <item id="40" name="table_netpresentvalue.swf" visible="1" type="swf" source="39" />
  <item id="41" name="table_netpresentvaluepaybackperiod.fla" visible="0" type="fla" source="" />
  <item id="42" name="table_netpresentvaluepaybackperiod.swf" visible="1" type="swf" source="41" />
  <item id="43" name="table_netpresentvalueposneg.fla" visible="0" type="fla" source="" />
  <item id="44" name="table_netpresentvalueposneg.swf" visible="1" type="swf" source="43" />
  <item id="45" name="table_partnerscurrentaccount.fla" visible="0" type="fla" source="" />
  <item id="46" name="table_partnerscurrentaccount.swf" visible="1" type="swf" source="45" />
  <item id="47" name="table_paybackperiod.fla" visible="0" type="fla" source="" />
  <item id="49" name="table_payinginslip.fla" visible="0" type="fla" source="" />
  <item id="50" name="table_payinginslip.swf" visible="1" type="swf" source="49" />
  <item id="51" name="table_processaccount.fla" visible="0" type="fla" source="" />
  <item id="52" name="table_processaccount.swf" visible="1" type="swf" source="51" />
  <item id="53" name="table_profitandloss.fla" visible="0" type="fla" source="" />
  <item id="54" name="table_profitandloss.swf" visible="1" type="swf" source="53" />
  <item id="55" name="table_recstatement.fla" visible="0" type="fla" source="" />
  <item id="56" name="table_recstatement.swf" visible="1" type="swf" source="55" />
  <item id="57" name="table_tbextract.fla" visible="0" type="fla" source="" />
  <item id="58" name="table_tbextract.swf" visible="1" type="swf" source="57" />
  <item id="59" name="table_timesheet.fla" visible="0" type="fla" source="" />
  <item id="60" name="table_timesheet.swf" visible="1" type="swf" source="59" />
  <item id="61" name="table_variableforecast.fla" visible="0" type="fla" source="" />
  <item id="62" name="table_variableforecast.swf" visible="1" type="swf" source="61" />
  <item id="65" name="table_appropriation.fla" visible="0" type="fla" source="" />
  <item id="66" name="table_appropriation.swf" visible="1" type="swf" source="65" />
  <item id="67" name="table_balancesheet.fla" visible="0" type="fla" source="" />
  <item id="68" name="table_balancesheet.swf" visible="1" type="swf" source="67" />
  <item id="69" name="table_journal.fla" visible="0" type="fla" source="" />
  <item id="70" name="table_journal.swf" visible="1" type="swf" source="69" />
  <item id="71" name="test.fla" visible="0" type="fla" source="" />
  <item id="72" name="test.swf" visible="1" type="swf" source="71" />
  <item id="73" name="table_cashbook.fla" visible="0" type="fla" source="" />
  <item id="74" name="table_cashbook.swf" visible="1" type="swf" source="73" />
  <item id="75" name="table_linkedgapfill.fla" visible="0" type="fla" source="" />
  <item id="76" name="table_linkedgapfill.swf" visible="1" type="swf" source="75" />
  <item id="77" name="table_protean.fla" visible="0" type="fla" source="" />
  <item id="78" name="table_protean.swf" visible="1" type="swf" source="77" />
  <updateHistory>
    <item original="red_dot_divider" Type="fla" id="3" />
    <item original="red_dot_divider" Type="swf" id="4" />
    <item original="black_line" Type="fla" id="5" />
    <item original="black_line" Type="swf" id="6" />
    <item original="button_viewcalculator" Type="fla" id="7" />
    <item original="button_viewcalculator" Type="swf" id="8" />
    <item original="table_highlight_table" Type="fla" id="11" />
    <item original="table_highlight_table" Type="swf" id="12" />
    <item original="table_apportionment" Type="fla" id="21" />
    <item original="table_apportionment" Type="swf" id="22" />
    <item original="table_cashledger" Type="fla" id="23" />
    <item original="table_cashledger" Type="swf" id="24" />
    <item original="table_contribution" Type="fla" id="25" />
    <item original="table_contribution" Type="swf" id="26" />
    <item original="table_extendedtrialbalance" Type="fla" id="27" />
    <item original="table_extendedtrialbalance" Type="swf" id="28" />
    <item original="table_extendedtrialbalanceprofit" Type="fla" id="29" />
    <item original="table_extendedtrialbalanceprofit" Type="swf" id="30" />
    <item original="table_ledgerincomplete" Type="fla" id="31" />
    <item original="table_ledgerincomplete" Type="swf" id="32" />
    <item original="table_ledgermc" Type="fla" id="33" />
    <item original="table_ledgermc" Type="swf" id="34" />
    <item original="table_ledgertotals" Type="fla" id="35" />
    <item original="table_ledgertotals" Type="swf" id="36" />
    <item original="table_limitingfactorstable" Type="fla" id="37" />
    <item original="table_limitingfactorstable" Type="swf" id="38" />
    <item original="table_netpresentvalue" Type="fla" id="39" />
    <item original="table_netpresentvalue" Type="swf" id="40" />
    <item original="table_netpresentvaluepaybackperiod" Type="fla" id="41" />
    <item original="table_netpresentvaluepaybackperiod" Type="swf" id="42" />
    <item original="table_netpresentvalueposneg" Type="fla" id="43" />
    <item original="table_netpresentvalueposneg" Type="swf" id="44" />
    <item original="table_partnerscurrentaccount" Type="fla" id="45" />
    <item original="table_partnerscurrentaccount" Type="swf" id="46" />
    <item original="table_paybackperiod" Type="fla" id="47" />
    <item original="table_payinginslip" Type="fla" id="49" />
    <item original="table_payinginslip" Type="swf" id="50" />
    <item original="table_processaccount" Type="fla" id="51" />
    <item original="table_processaccount" Type="swf" id="52" />
    <item original="table_profitandloss" Type="fla" id="53" />
    <item original="table_profitandloss" Type="swf" id="54" />
    <item original="table_recstatement" Type="fla" id="55" />
    <item original="table_recstatement" Type="swf" id="56" />
    <item original="table_tbextract" Type="fla" id="57" />
    <item original="table_tbextract" Type="swf" id="58" />
    <item original="table_timesheet" Type="fla" id="59" />
    <item original="table_timesheet" Type="swf" id="60" />
    <item original="table_variableforecast" Type="fla" id="61" />
    <item original="table_variableforecast" Type="swf" id="62" />
    <item original="table_appropriation" Type="fla" id="65" />
    <item original="table_appropriation" Type="swf" id="66" />
    <item original="table_balancesheet" Type="fla" id="67" />
    <item original="table_balancesheet" Type="swf" id="68" />
    <item original="table_journal" Type="fla" id="69" />
    <item original="table_journal" Type="swf" id="70" />
    <item original="test" Type="fla" id="71" />
    <item original="test" Type="swf" id="72" />
    <item original="table_cashbook" Type="fla" id="73" />
    <item original="table_cashbook" Type="swf" id="74" />
    <item original="table_linkedgapfill" Type="fla" id="75" />
    <item original="table_linkedgapfill" Type="swf" id="76" />
    <item original="table_protean" Type="fla" id="77" />
    <item original="table_protean" Type="swf" id="78" />
  </updateHistory>
</sharedLibrary>'
where ProjectId = 875;
/*
update SharedLibraryTable
set structureXML = '<sharedLibrary>
  <item id="9" name="red_dot_divider.fla" visible="0" type="fla" source="" />
  <item id="10" name="red_dot_divider.swf" visible="1" type="swf" source="9" />
  <item id="11" name="table_apportionment.fla" visible="0" type="fla" source="" />
  <item id="12" name="table_apportionment.swf" visible="1" type="swf" source="11" />
  <item id="13" name="table_cashledger.fla" visible="0" type="fla" source="" />
  <item id="14" name="table_cashledger.swf" visible="1" type="swf" source="13" />
  <item id="15" name="table_contribution.fla" visible="0" type="fla" source="" />
  <item id="16" name="table_contribution.swf" visible="1" type="swf" source="15" />
  <item id="17" name="table_extendedtrialbalance.fla" visible="0" type="fla" source="" />
  <item id="18" name="table_extendedtrialbalance.swf" visible="1" type="swf" source="17" />
  <item id="19" name="table_extendedtrialbalanceprofit.fla" visible="0" type="fla" source="" />
  <item id="20" name="table_extendedtrialbalanceprofit.swf" visible="1" type="swf" source="19" />
  <item id="21" name="table_ledgerincomplete.fla" visible="0" type="fla" source="" />
  <item id="22" name="table_ledgerincomplete.swf" visible="1" type="swf" source="21" />
  <item id="23" name="table_ledgermc.fla" visible="0" type="fla" source="" />
  <item id="24" name="table_ledgermc.swf" visible="1" type="swf" source="23" />
  <item id="25" name="table_ledgertotals.fla" visible="0" type="fla" source="" />
  <item id="26" name="table_ledgertotals.swf" visible="1" type="swf" source="25" />
  <item id="27" name="table_limitingfactorstable.fla" visible="0" type="fla" source="" />
  <item id="28" name="table_limitingfactorstable.swf" visible="1" type="swf" source="27" />
  <item id="29" name="table_partnerscurrentaccount.fla" visible="0" type="fla" source="" />
  <item id="30" name="table_partnerscurrentaccount.swf" visible="1" type="swf" source="29" />
  <item id="31" name="table_paybackperiod.fla" visible="0" type="fla" source="" />
  <item id="32" name="table_paybackperiod.swf" visible="1" type="swf" source="31" />
  <item id="33" name="table_payinginslip.fla" visible="0" type="fla" source="" />
  <item id="34" name="table_payinginslip.swf" visible="1" type="swf" source="33" />
  <item id="35" name="table_processaccount.fla" visible="0" type="fla" source="" />
  <item id="36" name="table_processaccount.swf" visible="1" type="swf" source="35" />
  <item id="37" name="table_profitandloss.fla" visible="0" type="fla" source="" />
  <item id="38" name="table_profitandloss.swf" visible="1" type="swf" source="37" />
  <item id="39" name="table_recstatement.fla" visible="0" type="fla" source="" />
  <item id="40" name="table_recstatement.swf" visible="1" type="swf" source="39" />
  <item id="41" name="table_stockrecordcard.fla" visible="0" type="fla" source="" />
  <item id="42" name="table_stockrecordcard.swf" visible="1" type="swf" source="41" />
  <item id="43" name="table_tbextract.fla" visible="0" type="fla" source="" />
  <item id="44" name="table_tbextract.swf" visible="1" type="swf" source="43" />
  <item id="45" name="black_line.fla" visible="0" type="fla" source="" />
  <item id="46" name="black_line.swf" visible="1" type="swf" source="45" />
  <item id="49" name="table_highlight_table.fla" visible="0" type="fla" source="" />
  <item id="50" name="table_highlight_table.swf" visible="1" type="swf" source="49" />
  <item id="55" name="table_journal.fla" visible="0" type="fla" source="" />
  <item id="56" name="table_journal.swf" visible="1" type="swf" source="55" />
  <item id="57" name="button_viewcalculator.fla" visible="0" type="fla" source="" />
  <item id="58" name="button_viewcalculator.swf" visible="1" type="swf" source="57" />
  <item id="59" name="table_appropriation.fla" visible="0" type="fla" source="" />
  <item id="60" name="table_appropriation.swf" visible="1" type="swf" source="59" />
  <item id="61" name="table_balancesheet.fla" visible="0" type="fla" source="" />
  <item id="62" name="table_balancesheet.swf" visible="1" type="swf" source="61" />
  <item id="63" name="table_netpresentvalue.fla" visible="0" type="fla" source="" />
  <item id="64" name="table_netpresentvalue.swf" visible="1" type="swf" source="63" />
  <item id="65" name="table_netpresentvaluepaybackperiod.fla" visible="0" type="fla" source="" />
  <item id="66" name="table_netpresentvaluepaybackperiod.swf" visible="1" type="swf" source="65" />
  <item id="67" name="table_netpresentvalueposneg.fla" visible="0" type="fla" source="" />
  <item id="68" name="table_netpresentvalueposneg.swf" visible="1" type="swf" source="67" />
  <item id="69" name="table_timesheet.fla" visible="0" type="fla" source="" />
  <item id="70" name="table_timesheet.swf" visible="1" type="swf" source="69" />
  <item id="71" name="table_variableforecast.fla" visible="0" type="fla" source="" />
  <item id="72" name="table_variableforecast.swf" visible="1" type="swf" source="71" />
  <item id="73" name="test.fla" visible="0" type="fla" source="" />
  <item id="74" name="test.swf" visible="1" type="swf" source="73" />
  <item id="75" name="table_extendedtrialbalancetotals.swf" visible="1" type="swf" source="76" />
  <item id="76" name="table_extendedtrialbalancetotals.fla" visible="0" type="fla" source="" />
  <item id="77" name="table_protean.fla" visible="0" type="fla" source="" />
  <item id="78" name="table_protean.swf" visible="1" type="swf" source="77" />
  <updateHistory>
    <item original="red_dot_divider" Type="fla" id="9" />
    <item original="red_dot_divider" Type="swf" id="10" />
    <item original="table_apportionment" Type="fla" id="11" />
    <item original="table_apportionment" Type="swf" id="12" />
    <item original="table_cashledger" Type="fla" id="13" />
    <item original="table_cashledger" Type="swf" id="14" />
    <item original="table_contribution" Type="fla" id="15" />
    <item original="table_contribution" Type="swf" id="16" />
    <item original="table_extendedtrialbalance" Type="fla" id="17" />
    <item original="table_extendedtrialbalance" Type="swf" id="18" />
    <item original="table_extendedtrialbalanceprofit" Type="fla" id="19" />
    <item original="table_extendedtrialbalanceprofit" Type="swf" id="20" />
    <item original="table_ledgerincomplete" Type="fla" id="21" />
    <item original="table_ledgerincomplete" Type="swf" id="22" />
    <item original="table_ledgermc" Type="fla" id="23" />
    <item original="table_ledgermc" Type="swf" id="24" />
    <item original="table_ledgertotals" Type="fla" id="25" />
    <item original="table_ledgertotals" Type="swf" id="26" />
    <item original="table_limitingfactorstable" Type="fla" id="27" />
    <item original="table_limitingfactorstable" Type="swf" id="28" />
    <item original="table_partnerscurrentaccount" Type="fla" id="29" />
    <item original="table_partnerscurrentaccount" Type="swf" id="30" />
    <item original="table_paybackperiod" Type="fla" id="31" />
    <item original="table_paybackperiod" Type="swf" id="32" />
    <item original="table_payinginslip" Type="fla" id="33" />
    <item original="table_payinginslip" Type="swf" id="34" />
    <item original="table_processaccount" Type="fla" id="35" />
    <item original="table_processaccount" Type="swf" id="36" />
    <item original="table_profitandloss" Type="fla" id="37" />
    <item original="table_profitandloss" Type="swf" id="38" />
    <item original="table_recstatement" Type="fla" id="39" />
    <item original="table_recstatement" Type="swf" id="40" />
    <item original="table_stockrecordcard" Type="fla" id="41" />
    <item original="table_stockrecordcard" Type="swf" id="42" />
    <item original="table_tbextract" Type="fla" id="43" />
    <item original="table_tbextract" Type="swf" id="44" />
    <item original="black_line" Type="fla" id="45" />
    <item original="black_line" Type="swf" id="46" />
    <item original="table_highlight_table" Type="fla" id="49" />
    <item original="table_highlight_table" Type="swf" id="50" />
    <item original="table_journal" Type="fla" id="55" />
    <item original="table_journal" Type="swf" id="56" />
    <item original="button_viewcalculator" Type="fla" id="57" />
    <item original="button_viewcalculator" Type="swf" id="58" />
    <item original="table_appropriation" Type="fla" id="59" />
    <item original="table_appropriation" Type="swf" id="60" />
    <item original="table_balancesheet" Type="fla" id="61" />
    <item original="table_balancesheet" Type="swf" id="62" />
    <item original="table_netpresentvalue" Type="fla" id="63" />
    <item original="table_netpresentvalue" Type="swf" id="64" />
    <item original="table_netpresentvaluepaybackperiod" Type="fla" id="65" />
    <item original="table_netpresentvaluepaybackperiod" Type="swf" id="66" />
    <item original="table_netpresentvalueposneg" Type="fla" id="67" />
    <item original="table_netpresentvalueposneg" Type="swf" id="68" />
    <item original="table_timesheet" Type="fla" id="69" />
    <item original="table_timesheet" Type="swf" id="70" />
    <item original="table_variableforecast" Type="fla" id="71" />
    <item original="table_variableforecast" Type="swf" id="72" />
    <item original="test" Type="fla" id="73" />
    <item original="test" Type="swf" id="74" />
    <item original="table_extendedtrialbalancetotals" Type="swf" id="75" />
    <item original="table_extendedtrialbalancetotals" Type="fla" id="76" />
    <item original="table_protean" Type="fla" id="77" />
    <item original="table_protean" Type="swf" id="78" />
  </updateHistory>
</sharedLibrary>'
where ProjectId = 886;


*/