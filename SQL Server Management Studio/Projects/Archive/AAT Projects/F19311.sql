SELECT EST.ID, EST.KeyCode, EST.examState, UT.Forename, UT.Surname, CT.CentreName, SCET.examName
  FROM ExamSessionTable as EST

  Inner Join ScheduledExamsTable as SCET
  on SCET.ID = EST.ScheduledExamID
  
  Inner Join UserTable as UT
  on UT.ID = EST.UserID

  Inner Join CentreTable as CT
  on CT.ID = SCET.CentreID

where CandidateRef = '10327092'


update ExamSessionTable
set previousExamState= examState, examState = 15
where ID = 1473986

delete from ExamSessionTable_Shredded
where ExamSessionID = 1473986

