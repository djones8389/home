select 
 CentreName
, CentreCode
, InstallKey
, lastSuccessfullUpdate
, lastAuthenticationTime	
from CentreTable where lastAuthenticationTime is not null
	and offlineEnabled = 1
	order by lastSuccessfullUpdate