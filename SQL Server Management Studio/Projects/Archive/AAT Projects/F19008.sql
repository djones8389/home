SELECT EST.ID, EST.KeyCode, EST.examState, UT.Forename, UT.Surname, CT.CentreName, SCET.examName
  FROM ExamSessionTable as EST

  Inner Join ScheduledExamsTable as SCET
  on SCET.ID = EST.ScheduledExamID
  
  Inner Join UserTable as UT
  on UT.ID = EST.UserID

  Inner Join CentreTable as CT
  on CT.ID = SCET.CentreID

where est.ID in (1410183,1410189)


update ExamSessionTable
set previousExamState = examState, examState = 15
where ID in (1410183,1410189)


delete from ExamSessionTable_Shredded where examSessionId in (1410183,1410189)