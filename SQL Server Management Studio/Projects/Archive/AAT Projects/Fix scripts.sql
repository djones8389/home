--**Only consideration I haven�t checked is when there�s > 1 section number**
--**Script set to work where there is <= 10 items in there,  which is true in this scenario for all 7 scripts**
--**SET	STRUCTUREXML MANUALLY**

--begin tran			**RUN THIS**

declare @totalMark int = '180'
declare @NewValue int = 0
declare @esid int = 1401159

--93 87--

--1399695,1399781,1400056,1400225,1400574,1401025,1401134,1401159--
/*
1)  Set the TotalMark using the Cross Apply calculator
2)  Set the StructureXML manually,  see line 530
*/

select StructureXML from WAREHOUSE_ExamSessionTable where id = @esid;

--Get the totalMark of the exam--
select
      sum(s.i.value('@totalMark','int')) as sumTotalMark
from WAREHOUSE_ExamSessionTable 

cross apply resultData.nodes('/exam/section/item') as s(i)

where id = @esid;

      --update WAREHOUSE_ExamSessionTable
      --set ResultData = '', ResultDataFull = ''  
      --where id = @esid;


select StructureXML, resultData, resultDataFull from WAREHOUSE_ExamSessionTable where id = @esid;

--WEST.ResultData--

      --SectionLevel--

      update WAREHOUSE_ExamSessionTable
      set ResultData.modify('replace value of (/exam/@userMark)[1] with sql:variable("@NewValue") ')    
      where id = @esid;

      delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid

      update WAREHOUSE_ExamSessionTable
      set ResultData.modify('replace value of (/exam/@totalMark)[1] with sql:variable("@totalMark") ')    
      where id = @esid;

      delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
      
      update WAREHOUSE_ExamSessionTable
      set ResultData.modify('replace value of (/exam/@userPercentage)[1] with sql:variable("@NewValue") ')    
      where id = @esid;
      
      delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid

      update WAREHOUSE_ExamSessionTable
      set ResultData.modify('replace value of (/exam/section/@userMark)[1] with sql:variable("@NewValue") ')    
      where id = @esid;

      delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid

      update WAREHOUSE_ExamSessionTable
      set ResultData.modify('replace value of (/exam/section/@userPercentage)[1] with sql:variable("@NewValue") ')    
      where id = @esid;
      
      delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid

      update WAREHOUSE_ExamSessionTable
      set ResultData.modify('replace value of (/exam/section/@totalMark)[1] with sql:variable("@totalMark") ')    
      where id = @esid;
      --ItemLevel--
      
      delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
      
      update WAREHOUSE_ExamSessionTable
      set ResultData.modify('replace value of (/exam/section/item/@markerUserMark)[1] with sql:variable("@NewValue") ')    
      where id = @esid;
      
      delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
      
      update WAREHOUSE_ExamSessionTable
      set ResultData.modify('replace value of (/exam/section/item/@markerUserMark)[2] with sql:variable("@NewValue") ')    
      where id = @esid;
            
      delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
      
      update WAREHOUSE_ExamSessionTable
      set ResultData.modify('replace value of (/exam/section/item/@markerUserMark)[3] with sql:variable("@NewValue") ')    
      where id = @esid;
      
      delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
      
      update WAREHOUSE_ExamSessionTable
      set ResultData.modify('replace value of (/exam/section/item/@markerUserMark)[4] with sql:variable("@NewValue") ')    
      where id = @esid; 
      
      delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
      
      update WAREHOUSE_ExamSessionTable
      set ResultData.modify('replace value of (/exam/section/item/@markerUserMark)[5] with sql:variable("@NewValue") ')    
      where id = @esid;
      
      delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
      
      update WAREHOUSE_ExamSessionTable
      set ResultData.modify('replace value of (/exam/section/item/@markerUserMark)[6] with sql:variable("@NewValue") ')    
      where id = @esid;
            
      delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
      
      update WAREHOUSE_ExamSessionTable
      set ResultData.modify('replace value of (/exam/section/item/@markerUserMark)[7] with sql:variable("@NewValue") ')    
      where id = @esid;
      
      delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
      
      update WAREHOUSE_ExamSessionTable
      set ResultData.modify('replace value of (/exam/section/item/@markerUserMark)[8] with sql:variable("@NewValue") ')    
      where id = @esid;
            
      delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
      
      update WAREHOUSE_ExamSessionTable
      set ResultData.modify('replace value of (/exam/section/item/@markerUserMark)[9] with sql:variable("@NewValue") ')    
      where id = @esid;
      
      delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
      
      update WAREHOUSE_ExamSessionTable
      set ResultData.modify('replace value of (/exam/section/item/@markerUserMark)[10] with sql:variable("@NewValue") ')    
      where id = @esid; 
      
      delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
      
      update WAREHOUSE_ExamSessionTable
      set ResultData.modify('replace value of (/exam/section/item/@actualUserMark)[1] with sql:variable("@NewValue") ')    
      where id = @esid;
            
      delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
      
      update WAREHOUSE_ExamSessionTable
      set ResultData.modify('replace value of (/exam/section/item/@actualUserMark)[2] with sql:variable("@NewValue") ')    
      where id = @esid;
      
      delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
      
      update WAREHOUSE_ExamSessionTable
      set ResultData.modify('replace value of (/exam/section/item/@actualUserMark)[3] with sql:variable("@NewValue") ')    
      where id = @esid;
            
      delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
      
      update WAREHOUSE_ExamSessionTable
      set ResultData.modify('replace value of (/exam/section/item/@actualUserMark)[4] with sql:variable("@NewValue") ')    
      where id = @esid;
            
      delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
      
      update WAREHOUSE_ExamSessionTable
      set ResultData.modify('replace value of (/exam/section/item/@actualUserMark)[5] with sql:variable("@NewValue") ')    
      where id = @esid;
            
      delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
      
      update WAREHOUSE_ExamSessionTable
      set ResultData.modify('replace value of (/exam/section/item/@actualUserMark)[6] with sql:variable("@NewValue") ')    
      where id = @esid;
            
      delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
      
      update WAREHOUSE_ExamSessionTable
      set ResultData.modify('replace value of (/exam/section/item/@actualUserMark)[7] with sql:variable("@NewValue") ')    
      where id = @esid;
            
      delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
      
      update WAREHOUSE_ExamSessionTable
      set ResultData.modify('replace value of (/exam/section/item/@actualUserMark)[8] with sql:variable("@NewValue") ')    
      where id = @esid;
            
      delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
            
      update WAREHOUSE_ExamSessionTable
      set ResultData.modify('replace value of (/exam/section/item/@actualUserMark)[9] with sql:variable("@NewValue") ')    
      where id = @esid;
            
      delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
      
      update WAREHOUSE_ExamSessionTable
      set ResultData.modify('replace value of (/exam/section/item/@actualUserMark)[10] with sql:variable("@NewValue") ')    
      where id = @esid;
      
      delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
      
      update WAREHOUSE_ExamSessionTable
      set ResultData.modify('replace value of (/exam/section/item/@userMark)[1] with sql:variable("@NewValue") ')    
      where id = @esid;
            
      delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
      
      update WAREHOUSE_ExamSessionTable
      set ResultData.modify('replace value of (/exam/section/item/@userMark)[9] with sql:variable("@NewValue") ')    
      where id = @esid;
            
      delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
      
      update WAREHOUSE_ExamSessionTable
      set ResultData.modify('replace value of (/exam/section/item/@userMark)[10] with sql:variable("@NewValue") ')    
      where id = @esid; 
      
      delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid

      
      update WAREHOUSE_ExamSessionTable
      set ResultData.modify('replace value of (/exam/section/item/@userMark)[2] with sql:variable("@NewValue") ')    
      where id = @esid;
            
      delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
      
      update WAREHOUSE_ExamSessionTable
      set ResultData.modify('replace value of (/exam/section/item/@userMark)[3] with sql:variable("@NewValue") ')    
      where id = @esid;
            
      delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
      
      update WAREHOUSE_ExamSessionTable
      set ResultData.modify('replace value of (/exam/section/item/@userMark)[4] with sql:variable("@NewValue") ')    
      where id = @esid;
            
      delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
      
      update WAREHOUSE_ExamSessionTable
      set ResultData.modify('replace value of (/exam/section/item/@userMark)[5] with sql:variable("@NewValue") ')    
      where id = @esid;
            
      delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
      
      update WAREHOUSE_ExamSessionTable
      set ResultData.modify('replace value of (/exam/section/item/@userMark)[6] with sql:variable("@NewValue") ')    
      where id = @esid;
            
      delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
      
      update WAREHOUSE_ExamSessionTable
      set ResultData.modify('replace value of (/exam/section/item/@userMark)[7] with sql:variable("@NewValue") ')    
      where id = @esid;
            
      delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
      
      update WAREHOUSE_ExamSessionTable
      set ResultData.modify('replace value of (/exam/section/item/@userMark)[8] with sql:variable("@NewValue") ')    
      where id = @esid;

      delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
      
      update WAREHOUSE_ExamSessionTable
      set ResultData.modify('replace value of (/exam/section/item/@viewingTime)[1] with sql:variable("@NewValue") ')    
      where id = @esid;
            
      delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
      
      update WAREHOUSE_ExamSessionTable
      set ResultData.modify('replace value of (/exam/section/item/@viewingTime)[2] with sql:variable("@NewValue") ')    
      where id = @esid;
      
      delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
      
      update WAREHOUSE_ExamSessionTable
      set ResultData.modify('replace value of (/exam/section/item/@viewingTime)[3] with sql:variable("@NewValue") ')    
      where id = @esid;
            
      delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
      
      update WAREHOUSE_ExamSessionTable
      set ResultData.modify('replace value of (/exam/section/item/@viewingTime)[4] with sql:variable("@NewValue") ')    
      where id = @esid;
            
      delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
      
      update WAREHOUSE_ExamSessionTable
      set ResultData.modify('replace value of (/exam/section/item/@viewingTime)[5] with sql:variable("@NewValue") ')    
      where id = @esid;
            
      delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
      
      update WAREHOUSE_ExamSessionTable
      set ResultData.modify('replace value of (/exam/section/item/@viewingTime)[6] with sql:variable("@NewValue") ')    
      where id = @esid;
            
      delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
      
      update WAREHOUSE_ExamSessionTable
      set ResultData.modify('replace value of (/exam/section/item/@viewingTime)[7] with sql:variable("@NewValue") ')    
      where id = @esid;
            
      delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
      
      update WAREHOUSE_ExamSessionTable
      set ResultData.modify('replace value of (/exam/section/item/@viewingTime)[8] with sql:variable("@NewValue") ')    
      where id = @esid;
            
      delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
      
      update WAREHOUSE_ExamSessionTable
      set ResultData.modify('replace value of (/exam/section/item/@viewingTime)[9] with sql:variable("@NewValue") ')    
      where id = @esid;
            
      delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
      
      update WAREHOUSE_ExamSessionTable
      set ResultData.modify('replace value of (/exam/section/item/@viewingTime)[10] with sql:variable("@NewValue") ')    
      where id = @esid;
      
      delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
      
      update WAREHOUSE_ExamSessionTable
      set ResultData.modify('replace value of (/exam/section/item/@userAttempted)[1] with sql:variable("@NewValue") ')    
      where id = @esid;
      
      delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
      
      update WAREHOUSE_ExamSessionTable
      set ResultData.modify('replace value of (/exam/section/item/@userAttempted)[2] with sql:variable("@NewValue") ')    
      where id = @esid;
            
      delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
      
      update WAREHOUSE_ExamSessionTable
      set ResultData.modify('replace value of (/exam/section/item/@userAttempted)[3] with sql:variable("@NewValue") ')    
      where id = @esid;
      
      delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
      
      update WAREHOUSE_ExamSessionTable
      set ResultData.modify('replace value of (/exam/section/item/@userAttempted)[4] with sql:variable("@NewValue") ')    
      where id = @esid;
            
      delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
      
      update WAREHOUSE_ExamSessionTable
      set ResultData.modify('replace value of (/exam/section/item/@userAttempted)[5] with sql:variable("@NewValue") ')    
      where id = @esid;
      
      delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
      
      update WAREHOUSE_ExamSessionTable
      set ResultData.modify('replace value of (/exam/section/item/@userAttempted)[6] with sql:variable("@NewValue") ')    
      where id = @esid;
            
      delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
      
      update WAREHOUSE_ExamSessionTable
      set ResultData.modify('replace value of (/exam/section/item/@userAttempted)[7] with sql:variable("@NewValue") ')    
      where id = @esid;
      
      delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
      
      update WAREHOUSE_ExamSessionTable
      set ResultData.modify('replace value of (/exam/section/item/@userAttempted)[8] with sql:variable("@NewValue") ')    
      where id = @esid;
            
      delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
      
      update WAREHOUSE_ExamSessionTable
      set ResultData.modify('replace value of (/exam/section/item/@userAttempted)[9] with sql:variable("@NewValue") ')    
      where id = @esid;
      
      delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
      
      update WAREHOUSE_ExamSessionTable
      set ResultData.modify('replace value of (/exam/section/item/@userAttempted)[10] with sql:variable("@NewValue") ')    
      where id = @esid;

--WEST.ResultDataFull--

      --SectionLevel--
      
      delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid

      update WAREHOUSE_ExamSessionTable
      set resultDataFull.modify('replace value of (/assessmentDetails/assessment/@totalMark)[1] with sql:variable("@totalMark") ')    
      where id = @esid;
      
      delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid

      update WAREHOUSE_ExamSessionTable
      set resultDataFull.modify('replace value of (/assessmentDetails/assessment/section/@totalMark)[1] with sql:variable("@totalMark") ')    
      where id = @esid;
      
      delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
      
      update WAREHOUSE_ExamSessionTable
      set resultDataFull.modify('replace value of (/assessmentDetails/assessment/@userMark)[1] with sql:variable("@NewValue") ')    
      where id = @esid;
      
      delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
      
      update WAREHOUSE_ExamSessionTable
      set resultDataFull.modify('replace value of (/assessmentDetails/assessment/@userPercentage)[1] with sql:variable("@NewValue") ')    
      where id = @esid;
      
      delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
      
      update WAREHOUSE_ExamSessionTable
      set resultDataFull.modify('replace value of (/assessmentDetails/assessment/section/@userMark)[1] with sql:variable("@NewValue") ')    
      where id = @esid;
      
      delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid

      update WAREHOUSE_ExamSessionTable
      set resultDataFull.modify('replace value of (/assessmentDetails/assessment/section/@userPercentage)[1] with sql:variable("@NewValue") ')    
      where id = @esid;

	  delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
      --ItemLevel--
      --UserMark--
      update WAREHOUSE_ExamSessionTable
      set resultDataFull.modify('replace value of (/assessmentDetails/assessment/section/item/@userMark)[1] with sql:variable("@NewValue") ')    
      where id = @esid;
      
      delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
      
      update WAREHOUSE_ExamSessionTable
      set resultDataFull.modify('replace value of (/assessmentDetails/assessment/section/item/@userMark)[2] with sql:variable("@NewValue") ')    
      where id = @esid;
	  
	  delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
	  
      update WAREHOUSE_ExamSessionTable
      set resultDataFull.modify('replace value of (/assessmentDetails/assessment/section/item/@userMark)[3] with sql:variable("@NewValue") ')    
      where id = @esid;
	  
	  delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
      
      update WAREHOUSE_ExamSessionTable
      set resultDataFull.modify('replace value of (/assessmentDetails/assessment/section/item/@userMark)[4] with sql:variable("@NewValue") ')    
      where id = @esid;
	  
	  delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid

      update WAREHOUSE_ExamSessionTable
      set resultDataFull.modify('replace value of (/assessmentDetails/assessment/section/item/@userMark)[5] with sql:variable("@NewValue") ')    
      where id = @esid;
	  
	  delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
      
      update WAREHOUSE_ExamSessionTable
      set resultDataFull.modify('replace value of (/assessmentDetails/assessment/section/item/@userMark)[6] with sql:variable("@NewValue") ')    
      where id = @esid;
	  
	  delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid

      update WAREHOUSE_ExamSessionTable
      set resultDataFull.modify('replace value of (/assessmentDetails/assessment/section/item/@userMark)[7] with sql:variable("@NewValue") ')    
      where id = @esid;
	  
	  delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
      
      update WAREHOUSE_ExamSessionTable
      set resultDataFull.modify('replace value of (/assessmentDetails/assessment/section/item/@userMark)[8] with sql:variable("@NewValue") ')    
      where id = @esid;
	  
	  delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid

      update WAREHOUSE_ExamSessionTable
      set resultDataFull.modify('replace value of (/assessmentDetails/assessment/section/item/@userMark)[9] with sql:variable("@NewValue") ')    
      where id = @esid;
	  
	  delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
      
      update WAREHOUSE_ExamSessionTable
      set resultDataFull.modify('replace value of (/assessmentDetails/assessment/section/item/@userMark)[10] with sql:variable("@NewValue") ')    
      where id = @esid;
	  
	  delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid

      update WAREHOUSE_ExamSessionTable
      set resultDataFull.modify('replace value of (/assessmentDetails/assessment/section/item/@userAttempted)[1] with sql:variable("@NewValue") ')    
      where id = @esid;
	  
	  delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
      
      update WAREHOUSE_ExamSessionTable
      set resultDataFull.modify('replace value of (/assessmentDetails/assessment/section/item/@userAttempted)[2] with sql:variable("@NewValue") ')    
      where id = @esid;
	  
	  delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
      
      update WAREHOUSE_ExamSessionTable
      set resultDataFull.modify('replace value of (/assessmentDetails/assessment/section/item/@userAttempted)[3] with sql:variable("@NewValue") ')    
      where id = @esid;
	  
	  delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
      
      update WAREHOUSE_ExamSessionTable
      set resultDataFull.modify('replace value of (/assessmentDetails/assessment/section/item/@userAttempted)[4] with sql:variable("@NewValue") ')    
      where id = @esid;
	  
	  delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
      
      update WAREHOUSE_ExamSessionTable
      set resultDataFull.modify('replace value of (/assessmentDetails/assessment/section/item/@userAttempted)[5] with sql:variable("@NewValue") ')    
      where id = @esid;
	  
	  delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
      
      update WAREHOUSE_ExamSessionTable
      set resultDataFull.modify('replace value of (/assessmentDetails/assessment/section/item/@userAttempted)[6] with sql:variable("@NewValue") ')    
      where id = @esid;
	  
	  delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
      
      update WAREHOUSE_ExamSessionTable
      set resultDataFull.modify('replace value of (/assessmentDetails/assessment/section/item/@userAttempted)[7] with sql:variable("@NewValue") ')    
      where id = @esid;
	  
	  delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
      
      update WAREHOUSE_ExamSessionTable
      set resultDataFull.modify('replace value of (/assessmentDetails/assessment/section/item/@userAttempted)[8] with sql:variable("@NewValue") ')    
      where id = @esid;
	  
	  delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
      
      update WAREHOUSE_ExamSessionTable
      set resultDataFull.modify('replace value of (/assessmentDetails/assessment/section/item/@userAttempted)[9] with sql:variable("@NewValue") ')    
      where id = @esid;
	  
	  delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
      
      update WAREHOUSE_ExamSessionTable
      set resultDataFull.modify('replace value of (/assessmentDetails/assessment/section/item/@userAttempted)[10] with sql:variable("@NewValue") ')    
      where id = @esid;
	  
	  delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid

      update WAREHOUSE_ExamSessionTable
      set resultDataFull.modify('replace value of (/assessmentDetails/assessment/section/item/@viewingTime)[1] with sql:variable("@NewValue") ')    
      where id = @esid;
	  
	  delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
      
      update WAREHOUSE_ExamSessionTable
      set resultDataFull.modify('replace value of (/assessmentDetails/assessment/section/item/@viewingTime)[2] with sql:variable("@NewValue") ')    
      where id = @esid;
	  
	  delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
      
      update WAREHOUSE_ExamSessionTable
      set resultDataFull.modify('replace value of (/assessmentDetails/assessment/section/item/@viewingTime)[3] with sql:variable("@NewValue") ')    
      where id = @esid;
	  
	  delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid

      update WAREHOUSE_ExamSessionTable
      set resultDataFull.modify('replace value of (/assessmentDetails/assessment/section/item/@viewingTime)[4] with sql:variable("@NewValue") ')    
      where id = @esid;
	  
	  delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
      
      update WAREHOUSE_ExamSessionTable
      set resultDataFull.modify('replace value of (/assessmentDetails/assessment/section/item/@viewingTime)[5] with sql:variable("@NewValue") ')    
      where id = @esid;
	  
	  delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
      
      update WAREHOUSE_ExamSessionTable
      set resultDataFull.modify('replace value of (/assessmentDetails/assessment/section/item/@viewingTime)[6] with sql:variable("@NewValue") ')    
      where id = @esid;
	  
	  delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
      
      update WAREHOUSE_ExamSessionTable
      set resultDataFull.modify('replace value of (/assessmentDetails/assessment/section/item/@viewingTime)[7] with sql:variable("@NewValue") ')    
      where id = @esid;
	  
	  delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
      
      update WAREHOUSE_ExamSessionTable
      set resultDataFull.modify('replace value of (/assessmentDetails/assessment/section/item/@viewingTime)[8] with sql:variable("@NewValue") ')    
      where id = @esid;
	  
	  delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
      
      update WAREHOUSE_ExamSessionTable
      set resultDataFull.modify('replace value of (/assessmentDetails/assessment/section/item/@viewingTime)[9] with sql:variable("@NewValue") ')    
      where id = @esid;
	  
	  delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid
      
      update WAREHOUSE_ExamSessionTable
      set resultDataFull.modify('replace value of (/assessmentDetails/assessment/section/item/@viewingTime)[10] with sql:variable("@NewValue") ')    
      where id = @esid;
	  
	  delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = @esid

      update WAREHOUSE_ExamSessionTable
      set StructureXML = '<assessmentDetails xmlns="">
  <assessmentID>788</assessmentID>
  <qualificationID>28</qualificationID>
  <qualificationName>Financial Performance (AQ2010)</qualificationName>
  <qualificationReference>AATQCF</qualificationReference>
  <assessmentGroupName>FNPF</assessmentGroupName>
  <assessmentGroupID>344</assessmentGroupID>
  <assessmentGroupReference>CAFNPF</assessmentGroupReference>
  <assessmentName>FNPF</assessmentName>
  <validFromDate>03 Nov 2010</validFromDate>
  <expiryDate>31 Jul 2014</expiryDate>
  <startTime>00:00:00</startTime>
  <endTime>23:59:59</endTime>
  <duration>150</duration>
  <defaultDuration>150</defaultDuration>
  <scheduledDuration>
    <value>150</value>
    <reason></reason>
  </scheduledDuration>
  <sRBonus>0</sRBonus>
  <sRBonusMaximum>50</sRBonusMaximum>
  <externalReference>CAFNPF</externalReference>
  <passLevelValue>70</passLevelValue>
  <passLevelType>1</passLevelType>
  <status>2</status>
  <testFeedbackType>
    <passFail>0</passFail>
    <percentageMark>0</percentageMark>
    <allowSummaryFeedback>0</allowSummaryFeedback>
    <summaryFeedbackType>1</summaryFeedbackType>
    <itemSummary>0</itemSummary>
    <itemReview>0</itemReview>
    <itemReviewCorrectAnswer>0</itemReviewCorrectAnswer>
    <itemFeedback>0</itemFeedback>
    <printableSummary>0</printableSummary>
    <candidateDetails>0</candidateDetails>
    <feedbackByReference>0</feedbackByReference>
    <allowFeedbackDuringExam>0</allowFeedbackDuringExam>
    <allowFeedbackDuringSummary>0</allowFeedbackDuringSummary>
  </testFeedbackType>
  <itemFeedback>0</itemFeedback>
  <allowCalculator>0</allowCalculator>
  <lastInUseDate>07 Nov 2011 16:59:41</lastInUseDate>
  <completedScriptReview>0</completedScriptReview>
  <assessment currentSection="0" totalTime="0" currentTime="0" originalPassMark="70" originalPassType="1" passMark="70" passType="1" totalMark="360" userMark="230" userPercentage="63.889" passValue="0" originalGrade="Fail">
    <intro id="0" name="Introduction" currentItem="">
      <item id="877P1517" name="Nov11 FNPF introduction" totalMark="1" version="4" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" />
    </intro>
    <outro id="0" name="Finish" currentItem="" />
    <tools id="0" name="Tools" currentItem="" />
    <section id="1" name="1" passLevelValue="60" passLevelType="1" itemsToMark="0" currentItem="" fixed="0" totalMark="93" userMark="0" userPercentage="0" passValue="0">
      <item id="877P1547" name="Copy of 1.1 FNPF CF002" totalMark="12" version="7" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" LO="1.1 Basic costing information" unit="Financial Performance" quT="11" />
      <item id="877P1539" name="Copy of 1.2 FNPF CF001" totalMark="16" version="6" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" LO="1.2 Raw materials and labour variances" unit="Financial Performance" quT="10,11,12" />
      <item id="877P1891" name="1.3 FNPF DW011" totalMark="16" version="9" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" LO="1.3 Fixed overhead variances&amp;#xD;&amp;#xA;" unit="Financial Performance" quT="11,12" />
      <item id="877P1892" name="1.4 FNPF DW011" totalMark="12" version="8" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" LO="1.4 Standard cost reporting using an operating statement" unit="Financial Performance" quT="20" />
      <item id="877P1521" name="Copy of 1.5 FNPF DW001" totalMark="12" version="7" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" LO="1.5 Statistical techniques" unit="Financial Performance" quT="11,12" />
      <item id="877P1659" name="1.6 FNPF CF008" totalMark="25" version="18" markingType="1" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" LO="1.6 Drafting reports on variance analysis (written)" unit="Financial Performance" quT="11" />
    </section>
    <section id="2" name="2" passLevelValue="60" passLevelType="1" itemsToMark="0" currentItem="" fixed="0" totalMark="87" userMark="0" userPercentage="0" passValue="1">
      <item id="877P1378" name="2.1 FNPF DW004" totalMark="32" version="17" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" LO="2.1 Calculation of Performance indicators" unit="Financial Performance" quT="11" />
      <item id="877P1591" name="2.2 FNPF DW007" totalMark="18" version="17" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" LO="2.2 What if analysis calculations" unit="Financial Performance" quT="11" />
      <item id="877P1499" name="2.3 Copy of 1.8 FNPF DW001" totalMark="12" version="6" markingType="0" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" LO="2.3 Advanced costing techniques (e.g. lifecycle, target costing)" unit="Financial Performance" quT="11,12" />
      <item id="877P1384" name="2.4 FNPF CF005" totalMark="25" version="21" markingType="1" markingState="0" userMark="0" markerUserMark="" userAttempted="0" viewingTime="" flagged="0" LO="2.4 Drafting reports on key performance indicators (written)" unit="Financial Performance" quT="11" />
    </section>
  </assessment>
  <isValid>1</isValid>
  <isPrintable>0</isPrintable>
  <qualityReview>0</qualityReview>
  <numberOfGenerations>1</numberOfGenerations>
  <requiresInvigilation>1</requiresInvigilation>
  <advanceContentDownloadTimespan>60</advanceContentDownloadTimespan>
  <automaticVerification>0</automaticVerification>
  <offlineMode>2</offlineMode>
  <requiresValidation>0</requiresValidation>
  <requiresSecureClient>1</requiresSecureClient>
  <examType>0</examType>
  <advanceDownload>0</advanceDownload>
  <gradeBoundaryData>
    <gradeBoundaries passLevelType="1" adjustedGradeBoundaries="0">
      <grade modifier="lt" value="70" description="Fail" />
      <grade modifier="gt" value="70" description="Pass" />
    </gradeBoundaries>
  </gradeBoundaryData>
  <autoViewExam>0</autoViewExam>
  <autoProgressItems>0</autoProgressItems>
  <confirmationText>
    <confirmationText></confirmationText>
  </confirmationText>
  <requiresConfirmationCheck>0</requiresConfirmationCheck>
  <allowCloseWithoutSubmit>0</allowCloseWithoutSubmit>
  <useSecureMarker>0</useSecureMarker>
  <annotationVersion>0</annotationVersion>
  <allowPackagingDelivery>1</allowPackagingDelivery>
  <scoreBoundaryData>
    <scoreBoundaries showScoreBoundaryColumn="1">
      <score modifier="lt" value="36" description="Significantly below" higherBoundarySet="1" />
      <score modifier="lt" value="36" description="Significantly below" higherBoundarySet="0" />
      <score modifier="gt" value="36" description="Below requirement" higherBoundarySet="1" />
      <score modifier="gt" value="60" description="Borderline" higherBoundarySet="1" />
      <score modifier="gt" value="70" description="Met" higherBoundarySet="0" />
      <score modifier="gt" value="70" description="Met" higherBoundarySet="1" />
      <score modifier="gt" value="36" description="Below requirement" higherBoundarySet="0" userCreated="1" />
      <score modifier="gt" value="60" description="Borderline" higherBoundarySet="0" userCreated="1" />
      <score modifier="gt" value="85" description="Exceeded" higherBoundarySet="1" userCreated="1" />
    </scoreBoundaries>
  </scoreBoundaryData>
  <showCandidateReportResultColumn>1</showCandidateReportResultColumn>
  <showCandidateReportPercentageColumn>1</showCandidateReportPercentageColumn>
  <certifiedAccessible>0</certifiedAccessible>
  <markingProgressVisible>1</markingProgressVisible>
  <markingProgressMode>0</markingProgressMode>
  <secureClientOperationMode>1</secureClientOperationMode>
  <examDeliverySystemStyleType>delivery</examDeliverySystemStyleType>
  <markingAutoVoidPeriod>365</markingAutoVoidPeriod>
  <showPageRequiresScrollingAlert>0</showPageRequiresScrollingAlert>
  <project ID="877" />
</assessmentDetails>'
      where id = @esid;
            

      insert into WAREHOUSE_ExamSessionTable_Shreded  
     (examSessionId,      
      structureXml,      
      examVersionName,      
      examVersionRef,      
      examVersionId,      
      examName,      
      examRef,      
      qualificationid,      
      qualificationName,      
      qualificationRef,      
      resultData,      
      submittedDate,      
      originatorId,      
      centreName,      
      centreCode,      
      foreName,      
      dateOfBirth,      
      gender,      
      candidateRef,      
      surName,      
      scheduledDurationValue,      
      previousExamState,      
      examStateInformation,      
      examResult,      
      passValue,      
      closeValue,      
      --0,      
      externalReference,      
      examType,      
      warehouseTime,      
      CQN,      
      actualDuration,      
      appeal,      
      reMarkStatus,        
      qualificationLevel,   
      centreId,     
      uln,  
      AllowPackageDelivery,  
      ExportToSecureMarker,  
      ContainsBTLOffice,
      ExportedToIntegration,
      WarehouseExamState,
      [language],
      KeyCode,
      TargetedForVoid,
      EnableOverrideMarking,
      AddressLine1,
      AddressLine2,
      Town,
      County,
      Country,
      Postcode,
      ScoreBoundaryData,
      itemMarksUploaded,
      passMark,
      totalMark,
      passType,
      voidJustificationLookupTableId,
      automaticVerification,
      resultSampled,
      [started],
      submitted,
      validFromDate,
      expiryDate,
      totalTimeSpent,
      defaultDuration,
      scheduledDurationReason,
      WAREHOUSEScheduledExamID,
      WAREHOUSEUserID,
      itemDataFull,
      examId,
      userId,
      TakenThroughLocalScan,
        LocalScanDownloadDate,
        LocalScanUploadDate,
        LocalScanNumPages,
        CertifiedForTablet,
        IsProjectBased)    
     SELECT      
      examSessionId,      
      structureXml,      
      examVersionName,      
      examVersionRef,      
      examVersionId,      
      examName,      
      examRef,      
      qualificationid,      
      qualificationName,      
      qualificationRef,      
      resultData,      
      submittedDate,      
      originatorId,      
      centreName,      
      centreCode,      
      foreName,      
      dateOfBirth,      
      gender,      
      candidateRef,      
      surName,      
      scheduledDurationValue,      
      previousExamState,      
      examStateInformation,      
      examResult,      
      passValue,      
      closeValue,      
      --0,      
      externalReference,      
      examType,      
      warehouseTime,      
      CQN,      
      actualDuration,      
      appeal,      
      reMarkStatus,        
      qualificationLevel,   
      centreId,     
      uln,  
      AllowPackageDelivery,  
      ExportToSecureMarker,  
      ContainsBTLOffice,
      ExportedToIntegration,
      WarehouseExamState,
      [language],
      KeyCode,
      TargetedForVoid,
      EnableOverrideMarking,
      AddressLine1,
      AddressLine2,
      Town,
      County,
      Country,
      Postcode,
      ScoreBoundaryData,
      itemMarksUploaded,
      passMark,
      totalMark,
      passType,
      voidJustificationLookupTableId,
      automaticVerification,
      resultSampled,
      [started],
      submitted,
      validFromDate,
      expiryDate,
      totalTimeSpent,
      defaultDuration,
      scheduledDurationReason,
      WAREHOUSEScheduledExamID,
      WAREHOUSEUserID,
      itemDataFull,
      examId,
      userId,
      TakenThroughLocalScan,
        LocalScanDownloadDate,
        LocalScanUploadDate,
        LocalScanNumPages,
        CertifiedForTablet,
        IsProjectBased
     FROM sa_CandidateExamAudit_View      
     WHERE examSessionId = @esid;         
            
      BEGIN TRANSACTION;

            INSERT INTO WAREHOUSE_ExamSessionTable_ShrededItems
            (    ExamSessionID
                  ,ItemRef
                  ,ItemName
                  ,UserMark
                  ,MarkerUserMark
                  ,UserAttempted
                  ,ItemVersion
                  ,MarkingIgnored
                  ,MarkedMetadataCount
                  ,ExamPercentage
                  ,TotalMark
                  ,ResponseXML
                  ,OptionsChosen
                  ,SelectedCount
                  ,CorrectAnswerCount
                  ,itemType
                  ,FriendItems)
                  SELECT      @esid AS [examSessionId]
                              ,Result.Item.value('@id', 'nvarchar(15)') AS [itemRef]
                              ,Result.Item.value('@name', 'nvarchar(200)') AS [itemName]
                              ,Result.Item.value('@userMark', 'decimal(6, 3)') AS [userMark]
                              ,Result.Item.value('@markerUserMark', 'nvarchar(max)') AS [markerUserMark]
                              ,Result.Item.value('@userAttempted', 'bit') AS [userAttempted]
                              ,Result.Item.value('@version', 'int') AS [itemVersion]
                              ,Result.Item.value('@markingIgnored', 'tinyint') AS [markingIgnored]
                              ,Result.Item.value('count(mark)', 'int') AS [markedMetadataCount]
                              ,WAREHOUSE_ExamSessionTable.ResultData.value('(exam/@userPercentage)[1]', 'decimal(6, 3)') AS [examPercentage]
                              ,Result.Item.value('@totalMark', 'decimal(6, 3)') AS [totalMark]
                              ,WAREHOUSE_ExamSessionItemResponseTable.ItemResponseData AS [responseXml]
                              ,CAST(ISNULL(WAREHOUSE_ExamSessionItemResponseTable.ItemResponseData.query('data(p/s/c[@typ = "10"]/i[@sl = "1"]/@ac)'), ' UA ') AS nvarchar(200)) AS [optionsChosen] --Why?
                              ,WAREHOUSE_ExamSessionItemResponseTable.ItemResponseData.value('count(p/s/c/i[@sl = "1"])', 'int') AS [selectedCount]
                              ,WAREHOUSE_ExamSessionItemResponseTable.ItemResponseData.value('count(p/s/c/i[@ca = "1"])', 'int') AS [correctAnswerCount]
                              ,Result.Item.value('@type', 'int') AS ItemType
                              ,Result.Item.value('@SurpassFriendItems', 'NVARCHAR(MAX)') AS FriendItems
                  FROM WAREHOUSE_ExamSessionTable
                  CROSS APPLY WAREHOUSE_ExamSessionTable.ResultData.nodes('exam/section/item') Result(Item)
                  LEFT JOIN WAREHOUSE_ExamSessionItemResponseTable -- we want to include nulls in the the item response table so that non attempted items are included
                              ON WAREHOUSE_ExamSessionItemResponseTable.WAREHOUSEExamSessionID = @esid
                              AND WAREHOUSE_ExamSessionItemResponseTable.ItemID = Result.Item.value('@id', 'nvarchar(15)')
                  WHERE WAREHOUSE_ExamSessionTable.ID = @esid;
      COMMIT TRANSACTION;     
            
select StructureXML, resultData, resultDataFull from WAREHOUSE_ExamSessionTable where id = @esid;

--Rollback
