--select 
--	s.e.value('@id', 'nvarchar(50)') as ItemID
--	,s.e.value('sum(mark/@mark)', 'float') as TotalMark
--	,s.e.value('sum(//@mark)', 'float') as TotalMark
--	, XMLStore
--from DJXML

--cross apply XMLStore.nodes('exam/section/item') s(e)

--where UniqueIdentifier = 3



DECLARE MarkedMetadata CURSOR FOR 
select 
	s.e.value('@id', 'nvarchar(50)') as ItemID
	,s.e.value('sum(mark/@mark)', 'float') as TotalMarkPerItem
	,s.e.value('sum(//@mark)', 'float') as TotalMarkPerExam
from DJXML

cross apply XMLStore.nodes('exam/section/item') s(e)

where UniqueIdentifier = 3


DECLARE @ItemID nvarchar(12), @totalMarkItem int, @totalMarkExam int

Open MarkedMetadata;

FETCH NEXT FROM MarkedMetadata into  @ItemID, @totalMarkItem, @totalMarkExam;

WHILE @@FETCH_STATUS = 0

BEGIN

Update DJXML
set XMLStore.modify('replace value of(/exam/section/item[@id = sql:variable("@ItemID")]/@userMark)[1] with sql:variable("@totalMarkItem")')
where UniqueIdentifier = 3;
													
Update DJXML
set XMLStore.modify('replace value of(/exam/section/@userMark)[1] with sql:variable("@totalMarkExam")')
where UniqueIdentifier = 3;

Update DJXML
set XMLStore.modify('replace value of(/exam/@userMark)[1] with sql:variable("@totalMarkExam")')
where UniqueIdentifier = 3;

FETCH NEXT FROM MarkedMetadata into  @ItemID, @totalMarkItem, @totalMarkExam;


END

CLOSE  MarkedMetadata;
DEALLOCATE  MarkedMetadata;

select * from DJXML where UniqueIdentifier = 3;

