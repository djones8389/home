--Find corrupt Mark Schemes in Computer + Paper-based projects--

SELECT		
		ProjectListTable.ID AS ProjectID
		,NAME AS ProjectName
		,page.value('@ID', 'int') AS PageID
		--,CAST(ProjectListTable.ID as nvarchar(100)) + 'P' + page.value('@ID', 'nvarchar(1000)') AS TEST
		,page.value('@markingSchemeFor', 'NVARCHAR(1000)') AS markingSchemeFor  
		--,ProjectStructureXml
		,datalength(PageTable.SupportingFiles) as 'DataLength'
		,case OutputMedium when '0' then 'Computer-Based' else 'Paper-Based' END AS 'ProjectType'
		,page.value('(@chO2)[1]', 'NVARCHAR(12)') as CheckedOut
	FROM dbo.ProjectListTable
      
  CROSS APPLY ProjectStructureXml.nodes('//Pag') pages(page)

		inner join PageTable
		on PageTable.ID = CAST(ProjectListTable.ID as nvarchar(100)) + 'P' + page.value('@ID', 'nvarchar(1000)')
        
  WHERE page.value('local-name((..)[1])', 'nvarchar(1000)') <> 'Rec'
     AND page.value('@markingSchemeFor', 'NVARCHAR(1000)') != '-1'
     AND datalength(PageTable.SupportingFiles) <= 22
     AND PageTable.ver > 1;        


--Find corrupt pages in Paper-based projects--

SELECT		
		ProjectListTable.ID AS ProjectID
		,NAME AS ProjectName
		,page.value('@ID', 'int') AS PageID
		--,CAST(ProjectListTable.ID as nvarchar(100)) + 'P' + page.value('@ID', 'nvarchar(1000)') AS TEST
		,page.value('@markingSchemeFor', 'NVARCHAR(1000)') AS markingSchemeFor  
		--,ProjectStructureXml
		,datalength(PageTable.SupportingFiles) as 'DataLength'
		,case OutputMedium when '0' then 'Computer-Based' else 'Paper-Based' END AS 'ProjectType'
		,page.value('(@chO2)[1]', 'NVARCHAR(12)') as CheckedOut
	FROM dbo.ProjectListTable
      
  CROSS APPLY ProjectStructureXml.nodes('//Pag') pages(page)

		inner join PageTable
		on PageTable.ID = CAST(ProjectListTable.ID as nvarchar(100)) + 'P' + page.value('@ID', 'nvarchar(1000)')
        
  WHERE page.value('local-name((..)[1])', 'nvarchar(1000)') <> 'Rec'
     --AND page.value('@markingSchemeFor', 'NVARCHAR(1000)') != '-1'
     AND datalength(PageTable.SupportingFiles) <= 22
     AND PageTable.ver > 1
     and OutputMedium = 1;         
