--select
--   distinct d.a.value('@Name[1]', 'nvarchar(max)') as Metadata
--	--, pro.pag.value('@Topic[1]', 'nvarchar(max)') as Topic
--from ProjectListTable

--cross apply ProjectDefaultXml.nodes('Defaults/StructureAttributes/Attribute') d(a)
--cross apply ProjectStructureXML.nodes('Pro//Pag') Pro(pag)

--where ID = 177;
--where d.a.value('@Name[1]', 'nvarchar(max)') in ('Metadata','2Dor3D','AccessibilityApplied','Age_Range','AGrade','aKUmarks','aKUordKUorPS','AssessedDate','Assessment_Version','AssessmentCriteria','AssessmentVersion','Assessor','Author','AuthorDate','boo','Calculator','challenge','Coding','cognitiveDomain','CommandWord','Content','ContentArea1','ContentArea2','ContentArea3','context','Cows','CS_Communications','CS_IT','CS_Numeracy','currArea1','currArea2','currArea3','Date','Decimal','Desc','Describe','Describe_Folder','Description','DesignCategory','DiscriminationValue','dKUmarks','e2dwa','EnabledForPhone','End_User','eo1','eo2','FacilityValue','FiveChar_Integer','Folder_Reference','format','FourCharInteger','FreeText2','IfIntegrativeMarksPerTopic','ImageDescription','ImageKeyword','ImageTitle','Integration','Integrative','InternalAssessedDate','InternalAssessor','IsEscalated','Issues','Item_Spec_No','itemID','ItemSpec','ItemSpecNo','ItemSpecNo.','ItemSpecNo2','ItemYearUsed','Key','Key_Area','KeyArea','Keyword2','Keywords','KnowledgeUnderstanding','Language','level','LO','MarkAllocation','MarkBreakdown','MarkedMetadata','markingCriteria','MarkingType','Marks','MediaUsed','mode','Model','ModernLanguageScrutComplete','ModernLanguageScrutineerCheckComplete','Multiple_Value_List','N5ItemSpecNo','N5Keywords','N5TotalMark','N5YearItemUsed','NameOfArtistDesigner','No_A_Marks','No_aKU_Marks','No_dKU_Marks','No_Marks','No_Marks_DrawingConclusionsGivingExplanations','No_Marks_IdentifyingErrorSuggestingImprovements','No_Marks_MakingPredictionsGeneralisations','No_Marks_PlanningDesigningExperimentsInvestigations','No_Marks_PresentingInformation','No_Marks_ProcessingInformation','No_Marks_SelectingInformation','NoaKUMarks','NoAMarks','NodKUMarks','NoMarksDrawingConclusionsGivingExplanations','NoMarksDrawingValidConclusions','NoMarksDrawingValidConclusionsAndGivingExplanationsSupportedByEvidenceJustification','NoMarksForSelectingInformationAppropriately','NoMarksIdentifyingaSourceOfeErrorAndSuggestingImprovementsToExperiment','NoMarksIdentifyingASourceOfErrorAndSuggestingImprovementsToExperiments','NoMarksIdentifyingErrorSuggestingImprovements','NoMarksIdentifyingSourceOfError','NoMarksMakingPredictions','NoMarksMakingPredictionsAndGeneralisationsBasedOnEvidenceInformation','NoMarksMakingPredictionsGeneralisations','NoMarksPlanningDesigningExperimentInvestigations','NoMarksPlanningOrDesigningExperimentsIinvestigationsToTestGivenHypotheses','NoMarksPlanningOrDesigningExperimentsInvestigationsToTestGivenHypothesesOrToIllustrateParticularEffectsApplyingSafetyMeasures','NoMarksPresentingInformation','NoMarksPresentingInformationAppropriately','NoMarksPresentingInformationAppropriatelyInaVarietyOfForms','NoMarksPresentingnformationAppropriatelyInaVarietyOfForms','NoMarksProcessingInformation','NoMarksProcessingInformationUsingCalculationsAndUnitsWhereAppropriate','NoMarksSelectingInformation','NoMarksSelectingInformationAppropriatelyInaVarietyOfForms','NoOfCMarks','NoOfMarksBeyondC','NoOfOperationalMarks','NoOfReasoningMarks','OperationalDescrimination','OperationalDiscrimination','OperationalFacilityValue','organiser1','organiser2','OriginalDateOfEntry','OriginalItemID','OriginalItemWriterName','OriginalKUMarks','OriginalLevel','OriginalPSMarks','OriginalQuestionID','OrthographicScrutineerCheckComplete','OrthographicScrutineerComplete','OtherOtherOtherOtherSkillCode','OtherOtherOtherSkillCode','OtherOtherOtherSkillSet','OtherOtherSkillCode','OtherOtherSkillSet','OtherSkillCode','OtherSkillSet','Outcome','Outcome_Number','OutcomeNumber','parentID','Part','pFV','PretestDiscrimination','PretestFacilityValue','PretestYear','pretestYr','PreviousID','PSvalue','Purpose','QCFUnitNumber','QPSection','Qualification_Suite','Question_Author','QuestionAuthor','QuestionOrder','QuestionPage','QuestionPaper','QuestionPeerReviewer','QuestionType','RecordingComplete','Resource_Type','ScoreType','ScottishArtistDesigner','ScrollBarEnabled','Section','SexOfArtistDesigner','Skill','SkillCode','SkillSet','sourceChal','Specific_Integration','SQAUnitNumber','SQCF_Level','Status','Steps','Subject','SubSection','SubTopic','taskName','test','TestMultpleValue','Title','Topic','TopicForSectionC','Total_Mark','TotalMark','TotalMarks','TypeOfVisualArt','unit','Unit_Number','Unit_Title','UnitAdditional','UnitMain','UnitNumber','UnitNumberQCF','UnitTitle','UnitTitleQCF','Use','Writer','y','YearItemLastUsed','YearItemUsed')
--order by ID


DECLARE Metadata CURSOR 
FOR

select
   distinct d.a.value('@Name[1]', 'nvarchar(max)') as Metadata
	
from ProjectListTable

cross apply ProjectDefaultXml.nodes('Defaults/StructureAttributes/Attribute') d(a);

Declare @Metadata nvarchar(max);

Open Metadata;

FETCH NEXT FROM Metadata INTO @Metadata;

WHILE @@FETCH_STATUS = 0

BEGIN

 PRINT N'
 select
   ProjectListTable.ID as ProjectID
  ,ProjectListTable.Name as ProjectName
  
from ProjectListTable

	inner join PageTable on PageTable.ParentID = ProjectListTable.ID

  cross apply ProjectStructureXML.nodes(''Pro//Pag'') Pro(pag)
  
 where ProjectStructureXML.exist(''Pro//Pag[@' +  @Metadata  +  ']'') = 1
 
 '
 
	
FETCH NEXT FROM Metadata INTO @Metadata

END
CLOSE Metadata;
DEALLOCATE Metadata;
   --select     ProjectListTable.ID as ProjectID    ,ProjectListTable.Name as ProjectName      from ProjectLitTable     inner join PageTable on PageTable.ParentID = ProjectListTable.ID      cross apply ProjectStructureXML.nodes('Pro//Pag') Pro(pag)       where ProjectStructureXML.exist('Pro//Pag[@2Dor3D]') = 1      



select top 10 
    d.a.value('@Name[1]', 'nvarchar(max)') as Metadata
	
from ProjectListTable

inner join PageTable on PageTable.ParentID = ProjectListTable.ID

cross apply ProjectDefaultXml.nodes('Defaults/StructureAttributes/Attribute') d(a)
where d.a.value('@Name[1]', 'nvarchar(max)') like '%''%'

;







select distinct
   pagetable.id
  ,ProjectListTable.ID as ProjectID
  ,ProjectListTable.Name as ProjectName
  ,pro.pag.value('(@Topic)','nvarchar(max)') as Topic
  ---,ProjectStructureXML
from ProjectListTable

	inner join PageTable on PageTable.ParentID = ProjectListTable.ID

  cross apply ProjectStructureXML.nodes('//Pag') Pro(pag)
  
where ProjectStructureXML.exist('//Pag[@Topic]') = 1
	 and ProjectListTable.ID = 201
	 --AND pagetable.id = '201P1000'
	 and pro.pag.value('(@Topic)','nvarchar(max)') like '%''%'


select  ProjectListTable.ID as ProjectID, ProjectListTable.Name as ProjectName, PageTable.ID 
from ProjectListTable
 inner join PageTable on PageTable.ParentID = ProjectListTable.ID   
 cross apply ProjectStructureXML.nodes('Pro//Pag') Pro(pag) 
 where  pro.pag.value('(@AccessibilityApplied)','nvarchar(max)') like '%''%';


--Test.Conf.exist('data(/Conf/UserData/data[@value=''''])') FROM Test;






/*
select
   ProjectListTable.ID as ProjectID
  ,ProjectListTable.Name as ProjectName
  
from ProjectListTable

	inner join PageTable on PageTable.ParentID = ProjectListTable.ID

  cross apply ProjectStructureXML.nodes('Pro//Pag') Pro(pag)
  
 where ProjectStructureXML.exist('Pro//Pag[@Topic]') = 1
*/
  
    
    
/*
select
   ProjectListTable.ID as ProjectID
  ,ProjectListTable.Name as ProjectName
  
from ProjectListTable

	inner join PageTable on PageTable.ParentID = ProjectListTable.ID

  cross apply ProjectStructureXML.nodes('Pro//Pag') Pro(pag)
  
 where ProjectStructureXML.exist('Pro//Pag[@Topic]') = 1
*/



--select cast(ProjectStructureXml as nvarchar(MAX)) from ProjectListTable where id = 11

--select ProjectStructureXml from ProjectListTable where id = 11

/*

select top 5
	ID
	,ProjectDefaultXml
	,ProjectStructureXML
	from ProjectListTable

where ID = 177;


*/