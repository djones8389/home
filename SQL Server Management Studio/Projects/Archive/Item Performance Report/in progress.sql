UPDATE #Items
SET
	 Inx = B.Inx
	,Inx5 = B.Inx5
	,QuestionMarks = ISNULL(A.MarkerMark, B.AssM2)
	,QuestionTotalMarks = B.QuestionTotalMarks
	,QuestionName = B.QuestionName
	,QuestionTypeKey = B.QuestionTypeKey
	,CAQuestionTypeKey = B.CAQuestionTypeKey
	,CAId = B.CAId
	,ModifiedBy = B.ModifiedByUserKey
	,IsDichotomous = B.IsDichotomous
	,SessionCount = C.SessionCount
FROM #Items A 

INNER JOIN   (
	SELECT 
		I.ExamSessionKey
		,I.CPID
		,ROW_NUMBER() OVER(PARTITION BY ExamVersionKey, I.CPID, I.CPVersion ORDER BY Percentage DESC, ExamSessionKey ASC) Inx
		,NTILE(5) OVER(PARTITION BY ExamVersionKey, I.CPID, I.CPVersion ORDER BY UserMarks DESC, ExamSessionKey ASC) Inx5
		,Q.TotalMark * I.FQRMark AssM2
		,Q.TotalMark QuestionTotalMarks
		,Q.QuestionName
		,Q.QuestionTypeKey
		,Q.CAQuestionTypeKey
		,CASE WHEN Q.CAQuestionTypeKey IS NOT NULL THEN Q.ExternalId ELSE NULL END CAId
		,Q.ModifiedByUserKey

		,CASE 
			WHEN 
				Q.CAQuestionTypeKey IN (2,4,5,7,9,18)	--MCQ, Either/Or, Numerical Entry, Short Answer, Select from a list, Hotspot
				AND Q.MarkingTypeKey=0				--ComputerMarked
				AND I.MarkerMark IS NULL			--Hadn't been overriden
				THEN 1 
			ELSE 0 
		 END IsDichotomous
		 FROM #Items I
		 INNER JOIN DimQuestions Q 
		 ON I.CPID = Q.CPID
			AND I.CPVersion = Q.CPVersion
) B

ON A.CPID = B.CPID
	AND A.ExamSessionKey = B.ExamSessionKey
	
INNER JOIN  (
	SELECT 
		ExamVersionKey
		, CPID
		, CPVersion
		, COUNT(DISTINCT ExamSessionKey) SessionCount 
	FROM #Items
	GROUP BY ExamVersionKey, CPID, CPVersion

) C
ON A.ExamVersionKey = C.ExamVersionKey
	AND A.CPID = C.CPID
	AND A.CPVersion = C.CPVersion
