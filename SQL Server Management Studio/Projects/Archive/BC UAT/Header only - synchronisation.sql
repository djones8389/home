--SELECT MAX(SurpassCandidateId) AS TPM
--FROm[dbo].[ScheduledPackageCandidates]

SELECT *
FROM UAT_BC2_TestPackageManager..[ScheduledPackageCandidates]
WHERE SurpassCandidateId > 283090

--SELECT MAX(ID) AS SA
--FROM UAT_BC2_SecureAssess..UserTable


SELECT *
FROM UAT_BC2_SecureAssess..UserTable
WHERE CandidateRef COLLATE SQL_Latin1_General_CP1_CI_AS IN (
SELECT SurpassCandidateRef
FROM UAT_BC2_TestPackageManager..[ScheduledPackageCandidates]
WHERE SurpassCandidateId > 283090
)


--TPM 283456
--SA 283090
/*
DECLARE @BackupFile NVARCHAR(255)         

SET @BackupFile = N'G:\UAT_DATABASE_BACKUPS\UAT.BC.Pre.Migration.bak';


DECLARE @Header TABLE (
      BackupName NVARCHAR(128)
      ,BackupDescription NVARCHAR(255)
      ,BackupType SMALLINT
      ,ExpirationDate DATETIME
      ,Compressed BIT
      ,Position SMALLINT
      ,DeviceType TINYINT
      ,UserName NVARCHAR(128)
      ,ServerName NVARCHAR(128)
      ,DatabaseName NVARCHAR(128)
      ,DatabaseVersion INT
      ,DatabaseCreationDate DATETIME
      ,BackupSize NUMERIC(20, 0)
      ,FirstLSN NUMERIC(25, 0)
      ,LastLSN NUMERIC(25, 0)
      ,CheckpointLSN NUMERIC(25, 0)
      ,DatabaseBackupLSN NUMERIC(25, 0)
      ,BackupStartDate DATETIME
      ,BackupFinishDate DATETIME
      ,SortOrder SMALLINT
      ,CodePage SMALLINT
      ,UnicodeLocaleId INT
      ,UnicodeComparisonStyle INT
      ,CompatibilityLevel TINYINT
      ,SoftwareVendorId INT
      ,SoftwareVersionMajor INT
      ,SoftwareVersionMinor INT
      ,SoftwareVersionBuild INT
      ,MachineName NVARCHAR(128)
      ,Flags INT
      ,BindingID UNIQUEIDENTIFIER
      ,RecoveryForkID UNIQUEIDENTIFIER
      ,Collation NVARCHAR(128)
      ,FamilyGUID UNIQUEIDENTIFIER
      ,HasBulkLoggedData BIT
      ,IsSnapshot BIT
      ,IsReadOnly BIT
      ,IsSingleUser BIT
      ,HasBackupChecksums BIT
      ,IsDamaged BIT
      ,BeginsLogChain BIT
      ,HasIncompleteMetaData BIT
      ,IsForceOffline BIT
      ,IsCopyOnly BIT
      ,FirstRecoveryForkID UNIQUEIDENTIFIER
      ,ForkPointLSN NUMERIC(25, 0) NULL
      ,RecoveryModel NVARCHAR(60)
      ,DifferentialBaseLSN NUMERIC(25, 0) NULL
      ,DifferentialBaseGUID UNIQUEIDENTIFIER
      ,BackupTypeDescription NVARCHAR(60)
      ,BackupSetGUID UNIQUEIDENTIFIER NULL
      ,CompressedBackupSize NUMERIC(20, 0)
      );

INSERT INTO @Header
EXECUTE (N'RESTORE HEADERONLY FROM DISK = N''' + @BackupFile + ''';');

SELECT BackupName, BackupStartDate, BackupFinishDate FROM @Header*/