SELECT MAX(SurpassCandidateId) AS TPM
FROM UAT_BC2_TestPackageManager..[ScheduledPackageCandidates]

SELECT MAX(ID) AS SA
FROM UAT_BC2_SecureAssess..UserTable



SET IDENTITY_INSERT UAT_BC2_SecureAssess..UserTable ON

MERGE UAT_BC2_SecureAssess..UserTable AS TGT
USING
(
	SELECT distinct
		SurpassCandidateId
		,SurpassCandidateRef
		,FirstName
		,LastName
	FROM UAT_BC2_TestPackageManager..ScheduledPackageCandidates
	WHERE SurpassCandidateId NOT IN (SELECT id FROM UAT_BC2_SecureAssess..UserTable)
		AND SurpassCandidateId > 283090
	
) AS SRC
ON TGT.ID = SRC.SurpassCandidateId	
WHEN NOT MATCHED THEN
INSERT (ID, CandidateRef, Forename, Surname, DOB, Gender, SpecialRequirements, County, Country, EthnicOriginID, AccountCreationDate, AccountExpiryDate, Username, Password, Retired, version, Seeded, IsExternal, IsExternalUser)
VALUES(SRC.SurpassCandidateId, SRC.SurpassCandidateRef, SRC.FirstName, SRC.LastName, '01/01/1915 00:00','M', '<specialRequirements />','-1','-1', 0, '05/08/2015 09:39','05/08/2015 09:39', 'DJTEST', 'DJTEST',0, 1, 0, 0, 0)
;

SET IDENTITY_INSERT UAT_BC2_SecureAssess..UserTable OFF
