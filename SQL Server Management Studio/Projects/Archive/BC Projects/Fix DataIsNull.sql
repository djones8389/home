 WITH T AS ( SELECT ROW_NUMBER() OVER( ORDER BY DateEscalated DESC, ID ASC )
  AS RowNum, ID, Qualification, QualificationId, Exam, ExamId, ExamVersion, ExamVersionId, ItemName, ItemId, Examiner, ExaminerId, DateEscalated, Issues, ReasonIds, Centres, CentresCodes, CountOfScripts, ScriptId, CandidateName, CandidateRef, CentreName, CentreCode, IsAllowViewWholeScript 
  
  FROM sm_EscalatedUGRs_fn(118) WHERE (ID > 0) ) SELECT *  FROM T where CountOfScripts = 0  ORDER BY DateEscalated DESC, ID ASC
  
  
  select CandidateExamVersions.* -- Users.UserName, Keycode, DateSubmitted

	FROM CandidateExamVersions
	INNER JOIN CandidateResponses
	ON CandidateResponses.CandidateExamVersionID = CandidateExamVersions.ID
	INNER JOIN AssignedItemMarks
	ON AssignedItemMarks.UniqueResponseId = CandidateResponses.UniqueResponseID
	INNER JOIN AssignedGroupMarks
	ON AssignedGroupMarks.ID = AssignedItemMarks.GroupMarkId AND AssignedGroupMarks.IsConfirmedMark = 1
	INNER JOIN Users
	ON Users.ID = AssignedGroupMarks.UserId
	INNER JOIN UniqueResponses
	ON UniqueResponses.ID = CandidateResponses.UniqueResponseID
	INNER JOIN Items
	ON Items.ID = UniqueResponses.itemId
	INNER JOIN MarkingMethodLookup
	ON MarkingMethodLookup.id = MarkingMethodId
	inner join ExamVersions
	on ExamVersions.ID = Items.ExamVersionID

where CandidateExamVersions.ExternalSessionID = 713142

--select * from CandidateExamVersions where ExternalSessionID = 713142


SELECT UGR.ID, Keycode, CandidateRef, CentreName
FROM CandidateExamVersions as CEV
 
INNER JOIN CandidateResponses as CR
ON CR.CandidateExamVersionID = CEV.ID
 
INNER JOIN UniqueResponses as UR
ON UR.ID = CR.UniqueResponseID 
 
INNER JOIN UniqueGroupResponseLinks as UGRL
on UGRL.UniqueResponseId = UR.id
 
INNER JOIN UniqueGroupResponses as UGR
on UGR.ID = UGRL.UniqueGroupResponseID
 
--INNER JOIN Items as I
--on I.ID = UR.itemId
 
where ugr.ID in (721288)
      order by ugr.ID asc


--select * from UniqueGroupResponses where ID = 721288


begin tran

 ;WITH T AS ( SELECT ROW_NUMBER() OVER( ORDER BY DateEscalated DESC, ID ASC )
  AS RowNum, ID, Qualification, QualificationId, Exam, ExamId, ExamVersion, ExamVersionId, ItemName, ItemId, Examiner, ExaminerId, DateEscalated, Issues, ReasonIds, Centres, CentresCodes, CountOfScripts, ScriptId, CandidateName, CandidateRef, CentreName, CentreCode, IsAllowViewWholeScript 
  
  FROM sm_EscalatedUGRs_fn(118) WHERE (ID > 0) ) SELECT *  FROM T where CountOfScripts = 0  ORDER BY DateEscalated DESC, ID ASC
  

--select UGR.* from UniqueGroupResponses as UGR
	
--	inner join UniqueGroupResponseLinks as UGRL
--	on UGRL.UniqueGroupResponseID = UGR.ID
	
--	inner join UniqueResponses as UR
--	on UR.id = UGRL.UniqueResponseId
	
--	left join CandidateResponses as CR
--	on CR.UniqueResponseID = UR.id
	
--	left join CandidateExamVersions as CEV
--	on CEV.ID = CR.CandidateExamVersionID
	
--where ugr.ID = 721288

delete from UniqueGroupResponseParkedReasonCrossRef where UniqueGroupResponseId = 721288
delete from UniqueGroupResponseLinks where UniqueResponseId = 641663
delete from AssignedItemMarks where UniqueResponseId = 641663
delete from UniqueResponses where id = 641663
delete from UniqueGroupResponses where ID = 721288
delete from AssignedGroupMarks where UniqueGroupResponseId = 721288
--select * from AssignedGroupMarksParkedReasonCrossRef where AssignedGroupMarkId = 641663

 ;WITH T AS ( SELECT ROW_NUMBER() OVER( ORDER BY DateEscalated DESC, ID ASC )
  AS RowNum, ID, Qualification, QualificationId, Exam, ExamId, ExamVersion, ExamVersionId, ItemName, ItemId, Examiner, ExaminerId, DateEscalated, Issues, ReasonIds, Centres, CentresCodes, CountOfScripts, ScriptId, CandidateName, CandidateRef, CentreName, CentreCode, IsAllowViewWholeScript 
  
  FROM sm_EscalatedUGRs_fn(118) WHERE (ID > 0) ) SELECT *  FROM T where CountOfScripts = 0  ORDER BY DateEscalated DESC, ID ASC
  
  
--select CEV.* from UniqueGroupResponses as UGR
	
--	inner join UniqueGroupResponseLinks as UGRL
--	on UGRL.UniqueGroupResponseID = UGR.ID
	
--	inner join UniqueResponses as UR
--	on UR.id = UGRL.UniqueResponseId
	
--	left join CandidateResponses as CR
--	on CR.UniqueResponseID = UR.id
	
--	--left JOIN AssignedItemMarks
--	--ON AssignedItemMarks.UniqueResponseId = CR.UniqueResponseID
	
--	--left JOIN AssignedGroupMarks
--	--ON AssignedGroupMarks.ID = AssignedItemMarks.GroupMarkId AND AssignedGroupMarks.IsConfirmedMark = 1

--	left join CandidateExamVersions as CEV
--	on CEV.ID = CR.CandidateExamVersionID
	
--where ugr.ID = 721288

rollback