declare @groupId int = 256
declare @userId int = 35

SELECT CGR.CandidateExamVersionID
  FROM CandidateGroupResponses CGR
  INNER JOIN UniqueGroupResponses UGR  ON UGR.ID = CGR.UniqueGroupResponseID
  INNER JOIN 
   (
    select CGR.CandidateExamVersionID AS ScriptId
  from CandidateGroupResponses CGR
  INNER JOIN UniqueGroupResponses UGR    ON UGR.ID = CGR.UniqueGroupResponseID
  where UGR.GroupDefinitionID != @groupId
   AND exists 
   (
     select 1
     from 
     (
       select TOP (1) 
        AGM.MarkingMethodId AS MM, 
        AGM.UserId AS UI,
        AGM.IsEscalated AS IE
       from AssignedGroupMarks AGM
       where AGM.UniqueGroupResponseId = CGR.UniqueGroupResponseID
       order by AGM.Timestamp desc
     ) AS T
    where ((T.MM != 14 AND T.MM != 11) OR (T.MM = 11 AND T.IE = 1 )) AND T.UI = @userId
    ) 
  ) AS AffectedScriptIds ON CGR.CandidateExamVersionID = AffectedScriptIds.ScriptId
 WHERE UGR.GroupDefinitionID = @groupId