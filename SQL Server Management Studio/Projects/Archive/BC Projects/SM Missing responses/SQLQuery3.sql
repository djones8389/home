SELECT distinct Keycode
FROM CandidateExamVersions
INNER JOIN CandidateResponses
ON CandidateResponses.CandidateExamVersionID = CandidateExamVersions.ID
INNER JOIN UniqueResponses
ON UniqueResponses.id = CandidateResponses.UniqueResponseID
LEFT JOIN AssignedItemMarks
ON AssignedItemMarks.UniqueResponseId = UniqueResponses.ID
INNER JOIN AssignedGroupMarks
ON AssignedGroupMarks.ID = AssignedItemMarks.GroupMarkId
WHERE ExternalSessionID IN ('430689','430690','450245','456635','456734','465028','465066','465081','465119','465121','468040','470059','477894','477916','479558','480955','480969','480997','481009','481064','481496','483484','488160','488166','488199','493936','512015','525625','549723','549743','549799','549843','549854','549869','549908','560086','565460','565577','565693','565990','568235','569543','569914','586236','589947','590994','661805','663114','678838','679402','682855','693420')
--AND MarkingMethodId = 12
	and ExamSessionState = 2
ORDER BY DateSubmitted desc
 