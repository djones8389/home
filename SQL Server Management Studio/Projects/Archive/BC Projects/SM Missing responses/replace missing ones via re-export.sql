select * from CandidateExamVersions
	Where Keycode in ('DVZZMV99','EYG8J399','TRZFTV99','TZXYEY99')
	
	
--update CandidateExamVersions 
--set ExamSessionState = 1, PercentageMarkingComplete = '0'
--Where Keycode in ('DVZZMV99','EYG8J399','TRZFTV99','TZXYEY99')

update CandidateExamVersions 
set ExamSessionState = 1, PercentageMarkingComplete = '0', ExternalSessionID = '-682855'
Where Keycode in ('TZXYEY99')

update CandidateExamVersions 
set ExamSessionState = 1, PercentageMarkingComplete = '0', ExternalSessionID = '-679402'
Where Keycode in ('TRZFTV99')

update CandidateExamVersions 
set ExamSessionState = 1, PercentageMarkingComplete = '0', ExternalSessionID = '-661805'
Where Keycode in ('EYG8J399')



delete from CandidateResponses where CandidateExamVersionID in (148387,152972,153877)
delete from CandidateGroupResponses where CandidateExamVersionID in (148387,152972,153877)
delete from ExamVersionItems where CandidateExamVersionID in (148387,152972,153877)
delete FROM CandidateExamVersions where ExternalSessionID in ('-661805','-679402','-682855')



SELECT responseData, Keycode
FROM CandidateExamVersions as CEV

INNER JOIN CandidateResponses as CR
ON CR.CandidateExamVersionID = CEV.ID

INNER JOIN UniqueResponses as UR
ON UR.ID = UniqueResponseID 

left join UniqueResponseDocuments as URD
ON URD.UniqueResponseID = UR.id 
	
Inner Join Items as I
on I.ID = ur.itemId

inner join ExamVersions as EV
on EV.ID = I.ExamVersionID

INNER JOIN Exams as E
ON E.ID = EV.ExamID

INNER JOIN Qualifications
ON E.QualificationID = Qualifications.ID


Where Keycode in ('DVZZMV99','EYG8J399','TRZFTV99','TZXYEY99')
	order by Keycode