select * from ScheduledPackageCandidateExams as SPCE

	inner join ScheduledPackageCandidates as SPC
	on SPC.ScheduledPackageCandidateId =  SPCE.Candidate_ScheduledPackageCandidateId

where ScheduledExamRef in   ('HSPKQQ99','TRD5AE99','TTEPHU99')

begin tran

update ScheduledPackageCandidateExams
set IsCompleted = 0
where ScheduledExamRef in   ('HSPKQQ99','TRD5AE99','TTEPHU99')

update ScheduledPackageCandidates
set IsCompleted = 0
where ScheduledPackageCandidateId = '199943'

rollback