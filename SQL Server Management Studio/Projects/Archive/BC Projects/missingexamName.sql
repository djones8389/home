use BRITISHCOUNCIL_SecureAssess
select examSessionId, qualificationName, examName, qualificationName, examRef, examVersionId, examVersionName, examVersionRef, KeyCode, examName, warehousetime
	 from WAREHOUSE_ExamSessionTable_Shreded 
	 where qualificationName = 'AustriaAMS'
	 and examVersionRef = 'Version 3 AMS'
	 and examName != ''
	 and examVersionId = 645
		order by warehousetime
	 
	 

begin tran
update WAREHOUSE_ExamSessionTable_Shreded 
set examName = 'Speaking AMS'
where examName = '' and examVersionId = 645
Rollback
	 
	 
select examSessionId, qualificationName, examName, qualificationName, examRef, examVersionId, examVersionName, examVersionRef, KeyCode, warehousetime
 from WAREHOUSE_ExamSessionTable_Shreded where 
qualificationName = 'Aptis General'
and examVersionId = 86

select * from BRITISHCOUNCIL_ItemBank..AssessmentGroupTable where ID = 86

use BRITISHCOUNCIL_SecureAssess

select examSessionId, qualificationName, examName, qualificationName, examRef, examVersionId, examVersionName, examVersionRef, KeyCode, warehousetime
	 from WAREHOUSE_ExamSessionTable_Shreded where examName != '' and qualificationName = 'Aptis General' and KeyCode = '266VJU01'
	 
	
--select * from BRITISHCOUNCIL_ItemBank..AssessmentTable  where ID = 636	 
use BRITISHCOUNCIL_ItemBank
	 
select AGT.Name, AT.*	
		from BRITISHCOUNCIL_ItemBank..AssessmentGroupTable as AGT
			
			inner join QualificationTable as QT
			on QT.ID = AGT.QualificationID
			
			
			inner join AssessmentTable as AT
			on AT.AssessmentGroupID = AGT.ID
		
			where qualificationName = 'Aptis General'
				and AT.ID = 81

	 
select * from BRITISHCOUNCIL_ItemBank..AssessmentGroupTable where Name = 'Familiarisation Test Speaking'
	 
	 
	 
	 
	 
	 
	 
	 
use BRITISHCOUNCIL_ItemBank

declare @examversion int = 86

select QT.QualificationName, QT.QualificationRef, AGT.Name, Ref, AssessmentName,  AT.ExternalReference, AT.ID, wests.examName, examVersionRef
	 from AssessmentTable as AT

	inner join AssessmentGroupTable as AGT
	on AGT.ID = AT.AssessmentGroupID
	
	inner join QualificationTable as QT
	on QT.ID = AGT.QualificationID
	
	inner join BRITISHCOUNCIL_SecureAssess..WAREHOUSE_ExamSessionTable_Shreded as wests
	on wests.examVersionId = AT.ID
	
--where wests.examName != '' and 	qt.QualificationName = 'AustriaAMS'
where  wests.examName = ''
	and examVersionId = @examversion
			
begin tran

update BRITISHCOUNCIL_SecureAssess..WAREHOUSE_ExamSessionTable_Shreded
set examName = (
	select name	
		from BRITISHCOUNCIL_ItemBank..AssessmentGroupTable
			
			inner join QualificationTable as QT
			on QT.ID = AssessmentGroupTable.QualificationID
		
			where qualificationName = 'AustriaAMS'
				
		)
where BRITISHCOUNCIL_SecureAssess..WAREHOUSE_ExamSessionTable_Shreded.examName = 'AustriaAMS'

rollback			



use BRITISHCOUNCIL_SecureAssess

select count(qualificationName), qualificationName, examName, examVersionRef, examVersionId
	 from WAREHOUSE_ExamSessionTable_Shreded
	where examname = '' 
	 group by qualificationName, examName, examVersionRef, examVersionId
		
	order by qualificationName asc
	
	
use BRITISHCOUNCIL_ItemBank
begin tran

update BRITISHCOUNCIL_SecureAssess..WAREHOUSE_ExamSessionTable_Shreded
set examName = (
	select Name, *	
		from BRITISHCOUNCIL_ItemBank..AssessmentGroupTable
			
			inner join QualificationTable as QT
			on QT.ID = AssessmentGroupTable.QualificationID
		
			where qualificationName = 'Aptis For Teachers'
				and AssessmentGroupTable.ID = 783
		)
where BRITISHCOUNCIL_SecureAssess..WAREHOUSE_ExamSessionTable_Shreded.examName = 'AustriaAMS'

rollback	