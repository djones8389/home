SELECT 

AssignedGroupMarks.ID	
,	AssignedGroupMarks.UserId
,   UniqueGroupResponses.ParkedUserID
, U.Forename
, U.Surname
, U.ID
FROM AssignedGroupMarks
INNER JOIN UniqueGroupResponses
ON UniqueGroupResponseId = UniqueGroupResponses.ID
INNER JOIN UniqueGroupResponseLinks
ON UniqueGroupResponseLinks.UniqueGroupResponseID = UniqueGroupResponses.ID
INNER JOIN UniqueResponses
ON UniqueGroupResponseLinks.UniqueResponseId = UniqueResponses.ID
INNER JOIN Items
ON UniqueResponses.itemId = Items.ID
INNER JOIN ExamVersions
ON ExamVersions.ID = Items.ExamVersionID
INNER JOIN Exams
ON Exams.ID = ExamVersions.ExamID
Inner Join Users as U
on U.ID = UniqueGroupResponses.ParkedUserID

WHERE 
 --[Timestamp] >= '2014-03-17 00:00:00' 
 ParkedDate >= '2014-03-17 00:00:00' 
	--AND IsEscalated	 = 1
	and ParkedDate is not null
	