SELECT CEV.ExternalSessionID, ExternalItemID, Keycode, ugr.ID
FROM CandidateExamVersions as CEV

INNER JOIN CandidateResponses as CR
ON CR.CandidateExamVersionID = CEV.ID

INNER JOIN UniqueResponses as UR
ON UR.ID = UniqueResponseID 

INNER JOIN UniqueGroupResponseLinks as UGRL
on UGRL.UniqueResponseId = UR.id

INNER JOIN UniqueGroupResponses as UGR
on UGR.ID = UGRL.UniqueGroupResponseID

left join UniqueResponseDocuments as URD
ON URD.UniqueResponseID = UR.id 
	
Inner Join Items as I
on I.ID = ur.itemId

inner join ExamVersions as EV
on EV.ID = I.ExamVersionID

INNER JOIN Exams as E
ON E.ID = EV.ExamID

INNER JOIN Qualifications as Q
ON E.QualificationID = Q.ID


where ugr.ID in (538500	,720520	,709063	,719296	,699841	,707213	,698859	,663729	,292911	,720549	,47366	,747625	,721946	,709614	,691713	,700111	,711245	,692401	,156235	,659770	,526483	,710112	,705572	,725689	,707321	,710346	,714657	,720212	,661429	,661397	,664495	,664391	)
      order by Keycode asc

