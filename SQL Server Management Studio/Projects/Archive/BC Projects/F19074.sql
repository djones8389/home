DECLARE @myKeycode NVARCHAR(12)
SET @myKeycode = 'KVMCBS99'

SELECT Items.ExternalItemName, Users.Forename, Users.Surname, AssignedGroupMarks.Timestamp, MarkingMethodId, MarkingMethodLookup.markingMethod
	 , IsMarkOneItemPerScript
FROM CandidateExamVersions
INNER JOIN CandidateResponses
ON CandidateResponses.CandidateExamVersionID = CandidateExamVersions.ID
INNER JOIN AssignedItemMarks
ON AssignedItemMarks.UniqueResponseId = CandidateResponses.UniqueResponseID
INNER JOIN AssignedGroupMarks
ON AssignedGroupMarks.ID = AssignedItemMarks.GroupMarkId AND AssignedGroupMarks.IsConfirmedMark = 1
INNER JOIN Users
ON Users.ID = AssignedGroupMarks.UserId
INNER JOIN UniqueResponses
ON UniqueResponses.ID = CandidateResponses.UniqueResponseID
INNER JOIN Items
ON Items.ID = UniqueResponses.itemId
INNER JOIN MarkingMethodLookup
ON MarkingMethodLookup.id = MarkingMethodId
inner join ExamVersions
on ExamVersions.ID = Items.ExamVersionID

WHERE Keycode = @myKeycode
