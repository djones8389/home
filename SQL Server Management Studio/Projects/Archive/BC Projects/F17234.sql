/*create table #August2012V3W
(
	forename nvarchar(max),
	surname nvarchar(max),
	candidateRef nvarchar(max),
	centreName nvarchar(max)

)

create table #August2012V3S
(
	forename nvarchar(max),
	surname nvarchar(max),
	candidateRef nvarchar(max),
	centreName nvarchar(max)

)

*/



select 
	foreName
	,surName
	,candidateRef
	,centreName
into #August2012V3W	
	from WAREHOUSE_ExamSessionTable_Shreded	
	where ExternalReference = 'August 2012_V3W'
	order by candidateRef desc
	
--drop table #August2012V3W



select 
	foreName
	,surName
	,candidateRef
	,centreName
into #August2012V3S
	from WAREHOUSE_ExamSessionTable_Shreded	
	where ExternalReference = 'August 2012_V3S'
	order by candidateRef desc
	
--drop table #August2012V3S


select * from #August2012V3S
	where candidateRef in (
		select candidateRef 
		from #August2012V3W
		)