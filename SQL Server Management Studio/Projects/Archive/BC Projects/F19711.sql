SELECT EST.ID, EST.KeyCode, EST.examState, UT.Forename, UT.Surname, CT.CentreName, SCET.examName
	 , CreatedDateTime
  FROM ExamSessionTable as EST

  Inner Join ScheduledExamsTable as SCET
  on SCET.ID = EST.ScheduledExamID
  
  Inner Join UserTable as UT
  on UT.ID = EST.UserID

  Inner Join CentreTable as CT
  on CT.ID = SCET.CentreID

where CentreName like '%malaysia%'
	and examState = 18
	and CreatedDateTime > '03 Nov 2014'
	and CreatedDateTime < '28 Nov 2014'
	order by CreatedDateTime asc;