SELECT *
FROM UniqueGroupResponses
INNER JOIN AssignedGroupMarks
ON AssignedGroupMarks.UniqueGroupResponseId = UniqueGroupResponses.ID
INNER JOIN Users
ON USers.ID = UserID
WHERE UniqueGroupResponses.ID = 59645 AND Users.Forename = 'meha'

SELECT *
FROM UniqueGroupResponses
INNER JOIN AssignedGroupMarks
ON AssignedGroupMarks.UniqueGroupResponseId = UniqueGroupResponses.ID
INNER JOIN GroupDefinitions
ON UniqueGroupResponses.GroupDefinitionID = GroupDefinitions.ID
WHERE 
AssignedGroupMarks.MarkingMethodId = 9 
AND
ConfirmedMark <> AssignedGroupMarks.AssignedMark
AND EXISTS(
	SELECT ID
	FROM AssignedGroupMarks sub
	WHERE AssignedGroupMarks.UniqueGroupResponseId = sub.UniqueGroupResponseId
	AND sub.MarkingMethodId IN (2,3)
	--AND sub.MarkingDeviation > GroupDefinitions.CIMarkingTolerance
)
ORDER BY Timestamp ASC


SELECT *
FROM MarkingMethodLookup