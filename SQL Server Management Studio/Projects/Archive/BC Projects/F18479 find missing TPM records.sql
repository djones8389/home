select WAREHOUSE_ExamSessionTable.KeyCode, WAREHOUSE_ExamSessionTable.warehouseTime, wests.candidateRef  --COUNT( WAREHOUSE_ExamSessionTable.ID)--, 
	from WAREHOUSE_ExamSessionTable

  Inner Join WAREHOUSE_ScheduledExamsTable as WSCET
  on WSCET.ID = WAREHOUSE_ExamSessionTable.WAREHOUSEScheduledExamID

  Inner Join WAREHOUSE_UserTable as WUT
  on WUT.ID = WSCET.WAREHOUSECreatedBy
  
  inner join WAREHOUSE_ExamSessionTable_Shreded as wests
  on wests.examSessionId = WAREHOUSE_ExamSessionTable.id

where WAREHOUSE_ExamSessionTable.KeyCode collate Latin1_General_CI_AS not in (
	
	select ScheduledExamRef from BritishCouncil_TestPackage..ScheduledPackageCandidateExams
)
	and wut.Username = 'ServiceUser_BritishCouncil'
	and WAREHOUSE_ExamSessionTable.PreviousExamState != 10
	--and KeyCode = 'U89YYH99'
	ORDER BY warehouseTime;
	
--select * from ExamSessionTable where Keycode = 'ATAKCW99'
--select PreviousExamState, * from WAREHOUSE_ExamSessionTable where Keycode = 'ATAKCW99'

select ID, KeyCode, ExamStateChangeAuditXml from WAREHOUSE_ExamSessionTable where KeyCode in ('YRDP8L99','KQYLJX99')