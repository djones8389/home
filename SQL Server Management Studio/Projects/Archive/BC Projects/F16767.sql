SELECT *
FROM CandidateExamVersions
INNER JOIN CandidateGroupResponses
ON CandidateGroupResponses.CandidateExamVersionID = CandidateExamVersions.ID
INNER JOIN UniqueGroupResponses
ON UniqueGroupResponses.ID = CandidateGroupResponses.UniqueGroupResponseID
INNER JOIN UniqueGroupResponseLinks
ON UniqueGroupResponseLinks.UniqueGroupResponseID = UniqueGroupResponses.ID
INNER JOIN UniqueResponses
ON UniqueResponses.id = UniqueGroupResponseLinks.UniqueResponseId
INNER JOIN Items
ON Items.ID = UniqueResponses.itemId
INNER JOIN AssignedGroupMarks
ON AssignedGroupMarks.UniqueGroupResponseId = UniqueGroupResponses.ID
WHERE Keycode = '6CSGEJ01' AND ExternalItemName = 'SPVer_4_Task_4'

SELECT *
FROM AssignedGroupMarks
WHERE UserId = 50 AND Timestamp < '2013-09-04 07:40:50' AND Timestamp > '2013-09-04 07:40:49'

SELECT *
FROM CandidateGroupResponses
WHERE CandidateGroupResponses.UniqueGroupResponseID = 246363

SELECT *
FROM CandidateExamVersions
WHERE ID = 40744