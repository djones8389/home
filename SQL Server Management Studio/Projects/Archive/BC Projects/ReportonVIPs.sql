select count(ID), ExamSessionState
	from CandidateExamVersions
		where VIP = 1 and ExamSessionState <> 7
			group by  ExamSessionState;
					
			
--select * from UniqueGroupResponses where VIP = 1


/*
238	2
6	4
*/

SELECT UniqueGroupResponses.VIP, CandidateExamVersions.VIP, UniqueGroupResponses.ID
	, IsEscalated
	,Q.Name, E.Name, E.Reference, EV.ExternalExamReference, EV.Name, DateSubmitted, Keycode
	, I.ExternalItemName, CentreName, CandidateRef
FROM UniqueResponses
inner join UniqueGroupResponseLinks
on UniqueGroupResponseLinks.UniqueResponseId = UniqueResponses.id
INNER JOIN UniqueGroupResponses
on UniqueGroupResponses.ID = UniqueGroupResponseLinks.UniqueGroupResponseID
INNER JOIN CandidateResponses
ON CandidateResponses.UniqueResponseID = UniqueResponses.id
INNER JOIN CandidateExamVersions
ON CandidateExamVersions.ID = CandidateResponses.CandidateExamVersionID

Inner Join Items as I
on I.ID = UniqueResponses.itemId

inner join ExamVersions as EV
on EV.ID = I.ExamVersionID

INNER JOIN Exams as E
ON E.ID = EV.ExamID

INNER JOIN Qualifications as Q
ON E.QualificationID = Q.ID


--where itemId = 800 and UniqueGroupResponses.confirmedMark IS NULL
where ExamSessionState <> 7
--and CandidateExamVersions.VIP = 0
and UniqueGroupResponses.VIP = 1
and UniqueGroupResponses.ConfirmedMark is null
--and itemId = 800
	order by Q.Name, E.Name, E.Reference, EV.ExternalExamReference, EV.Name
--order by UniqueGroupResponses.VIP desc, UniqueGroupResponses.insertionDate ASC