SELECT *
FROM UniqueResponses
INNER JOIN UniqueGroupResponseLinks
ON UniqueResponseId = UniqueResponses.id
INNER JOIN UniqueGroupResponses
ON UniqueGroupResponses.ID = UniqueGroupResponseLinks.UniqueGroupResponseID
INNER JOIN CandidateGroupResponses
ON CandidateGroupResponses.UniqueGroupResponseID = UniqueGroupResponses.ID
INNER JOIN CandidateExamVersions
ON CandidateExamVersionID = CandidateExamVersions.ID
WHERE UniqueResponses.id = 402933