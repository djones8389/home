create table #tempTable

(
      ID2 int,
      TotalNumberOfQuestions int
)

insert into #tempTable

      select
                  ID,
                  StructureXML.value('count(/assessmentDetails/assessment/section/item)[1]', 'int'),
                  attemptAutoSubmitForAwaitingUpload,
                  AwaitingUploadGracePeriodInDays
      from ExamSessionTable
     where KeyCode in ('3F9GAB99','3MBHRC99','3SBDG799','45Q8VN99','589MSD99','6K8VL299','7XSL9999','859BAA99','85CBKT99','8JKQVN99','97M46599','9N4NVF99','AABRR599','BCZWUM99','BRJBBC99','DPMAS599','EAZEZY99','FGDQRB99','FKT8ZB99','GAZH7E99','GRVY4S99','GWKKU999','HL3JBZ99','HS2P7399','HS83LX99','JJRN9L99','JKWS9R99','JNNSXL99','JSKRCA99','MXX6U599','N8VXXX99','NCQ5NV99','NYTSFH99','Q3L8T399','Q725M799','QPKU6U99','QSLM9Q99','TKD2XR99','TSSMSW99','VLF3ER99','WN3A2T99','WUHXJD99','XE735A99','Y96BXR99','Z33PXA99')
      order by KeyCode
      
	select 				
		   COUNT (ExamSessionID) as NumberOfQuestionsAnswered
		   ,  TT.TotalNumberOfQuestions      
		   , KeyCode
		from ExamSessionItemResponseTable as ESIRT

		inner join ExamSessionTable as EST
		on EST.ID = ESIRT.ExamSessionID
		
		inner join #tempTable as TT
        on TT.ID2 = ESIRT.ExamSessionID
		
		where KeyCode in ('3F9GAB99','3MBHRC99','3SBDG799','45Q8VN99','589MSD99','6K8VL299','7XSL9999','859BAA99','85CBKT99','8JKQVN99','97M46599','9N4NVF99','AABRR599','BCZWUM99','BRJBBC99','DPMAS599','EAZEZY99','FGDQRB99','FKT8ZB99','GAZH7E99','GRVY4S99','GWKKU999','HL3JBZ99','HS2P7399','HS83LX99','JJRN9L99','JKWS9R99','JNNSXL99','JSKRCA99','MXX6U599','N8VXXX99','NCQ5NV99','NYTSFH99','Q3L8T399','Q725M799','QPKU6U99','QSLM9Q99','TKD2XR99','TSSMSW99','VLF3ER99','WN3A2T99','WUHXJD99','XE735A99','Y96BXR99','Z33PXA99')
		
	group by ExamSessionID, TT.TotalNumberOfQuestions, KeyCode
		order by KeyCode

drop table #tempTable




SELECT EST.ID, EST.KeyCode, EST.examState, UT.Forename, UT.Surname, CT.CentreName, SCET.examName, CandidateRef, StructureXML, ExportToSecureMarker
	 , resultData.query('data(/exam/@userPercentage)')
  FROM ExamSessionTable as EST

  Inner Join ScheduledExamsTable as SCET
  on SCET.ID = EST.ScheduledExamID
  
  Inner Join UserTable as UT
  on UT.ID = EST.UserID

  Inner Join CentreTable as CT
  on CT.ID = SCET.CentreID
  
 where KeyCode in ('3F9GAB99','3MBHRC99','3SBDG799','45Q8VN99','589MSD99','6K8VL299','7XSL9999','859BAA99','85CBKT99','8JKQVN99','97M46599','9N4NVF99','AABRR599','BCZWUM99','BRJBBC99','DPMAS599','EAZEZY99','FGDQRB99','FKT8ZB99','GAZH7E99','GRVY4S99','GWKKU999','HL3JBZ99','HS2P7399','HS83LX99','JJRN9L99','JKWS9R99','JNNSXL99','JSKRCA99','MXX6U599','N8VXXX99','NCQ5NV99','NYTSFH99','Q3L8T399','Q725M799','QPKU6U99','QSLM9Q99','TKD2XR99','TSSMSW99','VLF3ER99','WN3A2T99','WUHXJD99','XE735A99','Y96BXR99','Z33PXA99')
	and examState <> 13
	order by examName
	
	
	
select attemptAutoSubmitForAwaitingUpload, AwaitingUploadGracePeriodInDays, AwaitingUploadStateChangingTime, examstate, resultData 
, resultData.query('data(/exam/@userPercentage)')
from 
ExamSessionTable
--update ExamSessionTable 
--set attemptAutoSubmitForAwaitingUpload = 1
--where ID IN (724132,724129,724133,724124,724142,724151,724126)

--update ExamSessionTable 
--set AwaitingUploadGracePeriodInDays = 10
where ID IN (724132,724129,724133,724124,724142,724151,724126)