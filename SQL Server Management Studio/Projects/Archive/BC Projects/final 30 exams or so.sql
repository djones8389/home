--Final 30 exams--

USE BRITISHCOUNCIL_SecureAssess
GO

BEGIN TRAN
      -- [TODO1] Specify test package ID 
      DECLARE @testPackageId int = 203

      -- [TODO2] Specify list of exam sessions that have been scheduled in SecureAssess, but not in TestPackage
      DECLARE @examSessions table(id int, candidateId int)
      INSERT @examSessions(id, candidateId)
            SELECT WEST.ID, WEST.WAREHOUSEUserID
                  FROM WAREHOUSE_ExamSessionTable WEST
                  WHERE ID in ('398422','398479','398481','398482','398498','398505','398510','398527','398534','398539','398570','398614','398619','398636','398639','398653','398662','398663','398664','398682','398711','398712','398737','398760','398780','398785','398800','398803','398806','398818','398841','398848','398862','398869','398872','398931','398988','398990','398991','399007','399014','399019','399036','399043','399048','399079','399123','399128','399145','399148','399162','399171','399172','399173','399191','399220','399221','399246','399269','399289','399294','399309','399312','399315','399327','399350','399357','399371','399378','399381','399440','399497','399499','399500','399516','399523','399528','399545','399552','399557','399588','399632','399637','399654','399657','399671','399680','399681','399682','399700','399729','399730','399755','399778','399798','399803','399818','399821','399824','399836','399859','399866','399879','399886','399889')
	             
	             --Omitted 399901,399940,
	                          
      -- How many exams the test package contains
      DECLARE @testPackageExamCount int
      SELECT @testPackageExamCount = COUNT(*)
         FROM BritishCouncil_TestPackage..PackageExams PE
            WHERE PE.PackageId = @testPackageId
                  AND StatusValue = 0 -- Live

      DECLARE @candidates TABLE(candidateId int)
      INSERT @candidates(candidateId)
            SELECT candidateId
				FROM @examSessions
            GROUP BY candidateId

     -- Find candidate exams that have been scheduled in SecureAssess, but not in TestPackage
      DECLARE @candidateId int
      DECLARE @intersectedCount int

      WHILE EXISTS (SELECT TOP 1 * FROM @candidates)
      BEGIN
            SELECT TOP 1 @candidateId = candidateId FROM @candidates
            DELETE FROM @candidates WHERE candidateId = @candidateId

            -- Check if all exams from the package have been scheduled in SecureAssess
            SELECT @intersectedCount = COUNT(*)
                  FROM 
                  (
                        SELECT PE.SurpassExamId
                              FROM BritishCouncil_TestPackage..PackageExams PE
                              WHERE PE.PackageId = @testPackageId
                        INTERSECT
                        SELECT SEXT.ExamID
                              FROM @examSessions ES
                                    JOIN WAREHOUSE_ExamSessionTable EST ON EST.ID = ES.id
                                    JOIN WAREHOUSE_ScheduledExamsTable SEXT ON SEXT.ID = EST.WAREHOUSEScheduledExamID
                              WHERE ES.candidateId = @candidateId
                  ) T

            IF (@intersectedCount <> @testPackageExamCount)
            BEGIN
                  PRINT CAST(@candidateId AS nvarchar(100)) + ' DO NOT PROCEED'
                  CONTINUE
            END


            -- Add one row to ScheduledPackages
            DECLARE @scheduledPackageId int
            INSERT INTO BritishCouncil_TestPackage.[dbo].[ScheduledPackages]
                           ([CenterId]
                           ,[CenterRef]
                           ,[CenterName]
                           ,[QualificationId]
                           ,[QualificationRef]
                           ,[QualificationName]
                           ,[StartDate]
                           ,[EndDate]
                           ,[StartTime]
                           ,[EndTime]
                           ,[DateCreated]
                           ,[CreatedBy]
                           ,[StatusValue]
                           ,[PackageId]
                           ,[PackageDate])
                  SELECT TOP 1
                              CT.ID
                              ,CT.CentreID
                              ,CT.CentreName
                              ,Q.ID
                              ,Q.QualificationRef
                              ,Q.QualificationName
                              ,SEXT.ScheduledStartDateTime
                              ,SEXT.ScheduledEndDateTime
                              ,'00:01' AS [StartTime] -- TODO: check 
                              ,'23:59' AS [EndTime] -- TODO: check 
                              ,SEXT.CreatedDateTime
                              ,'aptismanagermorocco' AS [CreatedBy] -- TODO: check
                              ,0 AS [StatusValue] -- Editable 
                              ,@testPackageId AS [PackageId]
                              ,GETDATE() AS [PackageDate]
                        FROM @examSessions ES
                              JOIN WAREHOUSE_ExamSessionTable EST ON EST.ID = ES.id
                              JOIN WAREHOUSE_ScheduledExamsTable SEXT ON SEXT.ID = EST.WAREHOUSEScheduledExamID
                              JOIN WAREHOUSE_CentreTable CT ON CT.ID = SEXT.WAREHOUSECentreID
                              JOIN IB3QualificationLookup Q ON Q.ID = SEXT.qualificationId
                        WHERE ES.candidateId = @candidateId

            SELECT @scheduledPackageId = SCOPE_IDENTITY();

            -- Add one row to ScheduledPackageCandidates
            DECLARE @scheduledPackageCandidateId int
            INSERT INTO BritishCouncil_TestPackage.[dbo].[ScheduledPackageCandidates]
                           ([SurpassCandidateId]
                           ,[SurpassCandidateRef]
                           ,[FirstName]
                           ,[LastName]
                           ,[MiddleName]
                           ,[BirthDate]
                           ,[IsCompleted]
                           ,[DateCompleted]
                           ,[PackageScore]
                           ,[ScheduledPackage_ScheduledPackageId]
                           ,[IsVoided])
                  SELECT 
                               U.id
                              ,U.CandidateRef
                              ,U.Forename
                              ,U.Surname
                              ,U.Middlename
                              ,U.DOB
                              ,0 AS [IsCompleted]
                              ,NULL AS [DateCompleted]
                              ,NULL AS [PackageScore]
                              ,@scheduledPackageId AS [ScheduledPackage_ScheduledPackageId]
                              ,0 AS [IsVoided]
                        FROM WAREHOUSE_UserTable U
                        WHERE U.ID = @candidateId
            
            SELECT @scheduledPackageCandidateId = SCOPE_IDENTITY();

            -- ScheduledPackageCandidateExams - add as many rows as exams in the package
            
            INSERT INTO BritishCouncil_TestPackage.[dbo].[ScheduledPackageCandidateExams]
                           (
                           [ScheduledExamRef]
                           ,[PackageExamId]
                           ,[IsCompleted]
                           ,[DateCompleted]
                           ,[Score]
                           ,[Candidate_ScheduledPackageCandidateId]
                           ,[IsVoided]
                           ,[ShouldIncludeScale]
                           ,[ShouldIncludeCEFR]
                           ,[ShouldIncludeFinalScore]
                           ,[IsRescheduled]
                           )
                                                      
                      SELECT 
							EST.KeyCode
							   -- SEXT.ExamRef
							  ,/*PE.PackageExamId*/ (SELECT PackageExamId FROM BritishCouncil_TestPackage.[dbo].PackageExams PE WHERE PE.SurpassExamId = SEXT.ExamID) AS PackageExamId
							  ,0 AS [IsCompleted]
							  ,NULL AS [DateCompleted]
							  ,NULL AS [Score]
							  ,@scheduledPackageCandidateId AS [Candidate_ScheduledPackageCandidateId]
							  ,0 AS [IsVoided]
							  ,/*PE.ShouldIncludeScale*/ (SELECT ShouldIncludeScale FROM BritishCouncil_TestPackage.[dbo].PackageExams PE WHERE PE.SurpassExamId = SEXT.ExamID) AS ShouldIncludeScale
							  ,/*PE.ShouldIncludeCEFR*/ (SELECT ShouldIncludeCEFR FROM BritishCouncil_TestPackage.[dbo].PackageExams PE WHERE PE.SurpassExamId = SEXT.ExamID) AS ShouldIncludeCEFR
							  ,/*PE.ShouldIncludeFinalScore*/ (SELECT ShouldIncludeFinalScore FROM BritishCouncil_TestPackage.[dbo].PackageExams PE WHERE PE.SurpassExamId = SEXT.ExamID) AS ShouldIncludeFinalScore
							  ,0 AS [IsRescheduled]
						FROM @examSessions ES
                              JOIN WAREHOUSE_ExamSessionTable EST ON EST.ID = ES.id
                              JOIN WAREHOUSE_ScheduledExamsTable SEXT ON SEXT.ID = EST.WAREHOUSEScheduledExamID
                              --JOIN BritishCouncil_TestPackage.[dbo].PackageExams PE ON PE.SurpassExamId = SEXT.ExamID
                        WHERE ES.candidateId = @candidateId
      END
ROLLBACK TRAN

select ID, ExamID from WAREHOUSE_ScheduledExamsTable  