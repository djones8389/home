USE [BRITISHCOUNCIL_SecureMarker]
GO
/****** Object:  StoredProcedure [dbo].[sm_MARKINGSERVICE_GetAllGroupsForExamVersion_sp]    Script Date: 05/08/2014 13:57:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- ============================================= 
-- Author:    Roman Tseiko   
-- Create date: 14/03/2011 
-- Description:  sproc to get all groups for a given Examversion 

-- date: 2013-06-04
-- Description:  Don't show Single Item Groups with Automatic marking type in Marking tab
-- Author:    Vitaly Bevzik   
-- Create date: 1/07/2013 
-- Description:  fix bugs with structures with several released groups
-- ============================================= 
--ALTER PROCEDURE [dbo].[sm_MARKINGSERVICE_GetAllGroupsForExamVersion_sp] 
  -- Add the parameters for the stored procedure here 
   declare
   
    @UserID INT = 73,
    @ExamVersionID INT = 88 ,
    @TokenStoreID NVARCHAR(30) = NULL
--AS 
    BEGIN 
      -- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements. 
        SET NOCOUNT ON; 
      
        SELECT  G.id AS ID ,
                G.name AS Name ,
                SUM(QM.[assignedquota]) AS AssignedQuota ,
                SUM(QM.[numbermarked]) AS NumberMarked ,
                QM.MarkingSuspended AS MarkingSuspended ,
                ( SELECT    COUNT(UGR.[id])
                  FROM      UniqueGroupResponses UGR
                            INNER JOIN GroupDefinitions GD ON UGR.GroupDefinitionID = GD.ID
                  WHERE     ( UGR.[tokenid] IS NULL
                              OR UGR.[tokenid] = @TokenStoreID
                            )
					-- unecessary check as we are passing in item id which determines this 
                            AND UGR.[parkeduserid] IS NULL
                            AND UGR.[confirmedmark] IS NULL
                            AND UGR.CI = 0
                            AND GD.ID = G.ID
                ) AS UniqueResponsesAvailableToMark
        FROM    dbo.GroupDefinitions G
                INNER JOIN [dbo].[QuotaManagement] QM ON QM.[groupid] = G.[id]
                INNER JOIN [dbo].[ExamVersions] EV ON G.examversionid = EV.[id]
        WHERE   --structures with status released 
                EXISTS ( SELECT *
                         FROM   GroupDefinitionStructureCrossRef CR
                                INNER JOIN ExamVersionStructures EVS ON EVS.ID = CR.ExamVersionStructureID
                         WHERE  CR.Status = 0
                                AND CR.GroupDefinitionID = G.ID
                                AND EVS.StatusID = 0 )
                AND EV.id = @ExamVersionID
                AND QM.UserId = @UserID
                --Remove automatic groups with Marking Type = automatic
                AND NOT EXISTS ( SELECT GD1.ID
                                 FROM   dbo.Items I1
                                        INNER JOIN dbo.GroupDefinitionItems GDI1 ON I1.ID = GDI1.ItemID
                                        INNER JOIN dbo.GroupDefinitions GD1 ON GDI1.GroupID = GD1.ID
                                 WHERE  I1.MarkingType = 0
                                        AND GD1.IsAutomaticGroup = 1 AND GD1.ID = g.ID)			
			
				---------------------------------------------------------------------------------
        GROUP BY G.id ,
                G.name ,
                QM.MarkingSuspended
    END 



SELECT *
FROM AssignedGroupMarks AGM
INNER JOIN GroupDefinitionItems GDI ON GDI.GroupID = AGM.GroupDefinitionID
INNER JOIN UniqueGroupResponses UGR ON AGM.UniqueGroupResponseId = UGR.ID
INNER JOIN UniqueGroupResponseLinks UGRL ON UGRL.UniqueGroupResponseID = UGR.ID
INNER JOIN UniqueResponses UR ON UGRL.UniqueResponseId = UR.ID
INNER JOIN Items I ON UR.itemId = I.ID
	AND GDI.ItemID = I.ID
INNER JOIN ExamVersions EV ON EV.ID = I.ExamVersionID
INNER JOIN Exams E ON E.ID = EV.ExamID
WHERE UserId = 73
	AND ExternalItemName = 'W_Ver_7_Task_1'
	--AND [Timestamp] >= '2014-03-01 00:00:00' 
	--AND [Timestamp] < '2014-04-30 00:00:00'
	AND EV.ID = 88
	AND (
		(AGM.MarkingMethodId IN (4))
                        OR AGM.MarkingMethodId NOT IN (1,4,5,6,7,8,9,11,12,13,14))
AND AGM.IsEscalated = 0
AND GDI.ViewOnly = 0
AND EXISTS (
                     SELECT TOP 1 *
                     FROM   dbo.GroupDefinitionStructureCrossRef CR
                                INNER JOIN dbo.ExamVersionStructures EVS
                                       ON  CR.ExamVersionStructureID = EVS.ID
                     WHERE  CR.GroupDefinitionID = GDI.GroupID
                                AND CR.Status IN (0 ,2)
                                AND EVS.StatusID = 0
)