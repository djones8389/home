DECLARE @NewOperatorName nvarchar(20) = 'DBAdmin';
DECLARE @OldOperatorName nvarchar(20) = (select name from msdb.dbo.sysoperators where name like 'Andy%');
DECLARE @EmailAddress  nvarchar(25) = 'DBAdmin@btl.com';
DECLARE @CreateNewOperator nvarchar(MAX) = '';
DECLARE @AssignOperatorNotifications nvarchar(MAX) = '';
DECLARE @RemoveOperatorNotifications  nvarchar(MAX) = '';
DECLARE @ChangeJobNotification nvarchar(MAX) = '';
DECLARE @ChangeSQLServerAgent nvarchar(MAX) = '';

--Create a new Operator

SELECT  @CreateNewOperator +=CHAR(13) + '

IF (select name from msdb.dbo.sysoperators where name =N'+ '''' + @NewOperatorName + ''') IS NOT NULL

      BEGIN       
            PRINT ''Operator already exists''
      END
      ELSE
      BEGIN

         EXEC msdb.dbo.sp_add_operator @name=N'+ '''' + @NewOperatorName + '''' + ', 
                  @enabled=1, 
                  @pager_days=0, 
                  @email_address=N'+ '''' + @EmailAddress + '''' + '
            
      END   
'


--Assign all notifications to this new Operator and drop the old Operator

select  @AssignOperatorNotifications +=CHAR(13) +  '
BEGIN

      EXEC msdb.dbo.sp_add_notification @alert_name=''' +  name + ''', @operator_name =N'+ '''' + @NewOperatorName + ''', @notification_method = 1; 

END

'
from msdb.dbo.sysalerts 


select  @RemoveOperatorNotifications +=CHAR(13) +  '

      BEGIN
      
      EXEC msdb.dbo.sp_delete_notification @alert_name=N'+ ''''+ name + ''', @operator_name=N'+ '''' + @OldOperatorName + '''; 
      
      PRINT ''Notification removed for '+ name + '''
      END

'
from msdb.dbo.sysalerts 


--Change Jobs's Email + NetSend Operators'--

SELECT @ChangeJobNotification +=CHAR(13) + '

IF (SELECT top 1 MSDB.dbo.sysjobs.name--, notify_email_operator_id
FROM MSDB.dbo.sysjobs
inner join MSDB.dbo.sysoperators
on MSDB.dbo.sysoperators.id = MSDB.dbo.sysjobs.notify_email_operator_id
where MSDB.dbo.sysoperators.name=N'+ ''''+ @OldOperatorName + '''and MSDB.dbo.sysjobs.name=N''' +  name + ''') 
	
IS NOT NULL

      BEGIN
      
      EXEC msdb.dbo.sp_update_job @job_name =N''' + name + ''',
            @notify_level_netsend=2, 
            @notify_level_page=2,
            @notify_email_operator_name =N'+ '''' + @NewOperatorName + ''', 
            @notify_netsend_operator_name =N'+ '''' + @NewOperatorName + '''
      
      PRINT ''Notifications have been updated''
      
      END
      ELSE
      BEGIN
         
         PRINT ''Notifications do not need changing''
      
      END
'
from MSDB.dbo.sysjobs

      
Select @ChangeSQLServerAgent += char(13) + '

      EXEC master.dbo.sp_MSsetalertinfo @failsafeoperator=N'+ ''''+ @NewOperatorName + ''',
            @notificationmethod=1
'

--PRINT (@CreateNewOperator)
--PRINT (@AssignOperatorNotifications);
--PRINT (@RemoveOperatorNotifications);
--PRINT (@ChangeJobNotification);
--PRINT (@ChangeSQLServerAgent);

BEGIN TRY
   BEGIN TRANSACTION 
            --EXEC (@CreateNewOperator)
            --EXEC (@AssignOperatorNotifications);
            --EXEC (@RemoveOperatorNotifications);
            EXEC (@ChangeJobNotification);
            --EXEC (@ChangeSQLServerAgent);
    COMMIT TRANSACTION
END TRY
BEGIN CATCH
    SELECT 
        ERROR_NUMBER() AS ErrorNumber
        ,ERROR_SEVERITY() AS ErrorSeverity
        ,ERROR_STATE() AS ErrorState
        ,ERROR_PROCEDURE() AS ErrorProcedure
        ,ERROR_LINE() AS ErrorLine
        ,ERROR_MESSAGE() AS ErrorMessage;

    IF @@TRANCOUNT > 0
        ROLLBACK TRANSACTION;
END CATCH;

IF @@TRANCOUNT > 0
    COMMIT TRANSACTION;
GO


--select J.name ,n.operator_id, o.name from msdb.dbo.sysnotifications as N
--inner join msdb.dbo.sysoperators as O
--on O.id = N.operator_id
--inner join msdb.dbo.sysjobs as J
--on J.job_id = O.id


--select j.name, o.id, o.name from msdb.dbo.sysjobs as J
--inner join msdb.dbo.sysoperators as O
--on O.id = J.notify_email_operator_id


--select j.name, o.id, o.name from msdb.dbo.sysjobs as J
--inner join msdb.dbo.sysoperators as O
--on O.id = J.notify_netsend_operator_id