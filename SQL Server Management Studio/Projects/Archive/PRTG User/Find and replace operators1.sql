USE [MSDB];

DECLARE @NewOperatorName nvarchar(20) = 'DBAdmin123'
DECLARE @OldOperatorName nvarchar(20) = (select name from msdb.dbo.sysoperators where name like 'DJ_DB%')	--Needs to be Dynamic--
DECLARE @EmailAddress  nvarchar(25) = 'DBAdmin@btl.com'
DECLARE @CreateNewOperator nvarchar(MAX) = ''
DECLARE @AssignOperatorNotifications nvarchar(MAX) = ''
DECLARE @DropAndyAsAlert nvarchar(MAX) = ''
DECLARE @ChangeJobNotification nvarchar(MAX) = ''
DECLARE @ChangeSQLServerAgent nvarchar(MAX) = ''


--Create a new Operator

SELECT  @CreateNewOperator +=CHAR(13) + '
   EXEC msdb.dbo.sp_add_operator @name=N'+ '''' + @NewOperatorName + '''' + ', 
		@enabled=1, 
		@pager_days=0, 
		@email_address=N'+ '''' + @EmailAddress + '''' + '
'

--Assign all notifications to this new Operator

select  @AssignOperatorNotifications +=CHAR(13) +  '
	EXEC msdb.dbo.sp_add_notification @alert_name=N'+ ''''+ name + ''', @operator_name=N'+ '''' + @NewOperatorName + ''', @notification_method = 1' + ';'
	FROM msdb.dbo.sysalerts

--Drop all notifications from the old Operator

select @DropAndyAsAlert  +=CHAR(13) +  '
	EXEC msdb.dbo.sp_delete_notification @alert_name=N'+ ''''+ name + ''', @operator_name=N'+ '''' + @OldOperatorName + ''' '
	FROM msdb.dbo.sysalerts
	
--Change Jobs's Operators'--


SELECT @ChangeJobNotification +=CHAR(13) + '
	EXEC msdb.dbo.sp_update_job @job_name=N'+ ''''+ name + ''',
		@notify_level_email=2, 
		@notify_level_netsend=2, 
		@notify_level_page=2, 
		@notify_email_operator_name=N'+ ''''+ @NewOperatorName + '''
'
FROM MSDB.dbo.sysjobs
WHERE SUSER_SNAME(owner_sid) != 'sa'

Select @ChangeSQLServerAgent += char(13) + '

EXEC master.dbo.sp_MSsetalertinfo @failsafeoperator=N'+ ''''+ @NewOperatorName + ''',
		@notificationmethod=1
'

PRINT @CreateNewOperator + @AssignOperatorNotifications + @DropAndyAsAlert + @ChangeJobNotification + @ChangeSQLServerAgent


BEGIN TRY
    BEGIN TRANSACTION 
		EXEC (@CreateNewOperator);
		EXEC (@AssignOperatorNotifications);
		EXEC (@DropAndyAsAlert);
		EXEC (@ChangeJobNotification);
    COMMIT TRANSACTION
END TRY
BEGIN CATCH
    SELECT 
        ERROR_NUMBER() AS ErrorNumber
        ,ERROR_SEVERITY() AS ErrorSeverity
        ,ERROR_STATE() AS ErrorState
        ,ERROR_PROCEDURE() AS ErrorProcedure
        ,ERROR_LINE() AS ErrorLine
        ,ERROR_MESSAGE() AS ErrorMessage;

    IF @@TRANCOUNT > 0
        ROLLBACK TRANSACTION;
END CATCH;

IF @@TRANCOUNT > 0
    COMMIT TRANSACTION;
GO

