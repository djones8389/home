select  
	ProjectListTable.id as ProjectID
	, pro.pag.value('@ID','nvarchar(100)') as PageID
	, Name
from ProjectListTable 

	cross apply ProjectStructureXml.nodes('//Pag') pro(pag)
	
		left join PageTable
		on PageTable.ID = cast(ProjectListTable.ID as nvarchar(100)) + N'P' + pro.pag.value('@ID','nvarchar(100)')
					AND ProjectListTable.ID = PageTable.ParentID
					
WHERE pagetable.id is null
	and pag.value('local-name((..)[1])', 'nvarchar(1000)') <> 'Rec';
	
	
select ID, Name, ProjectStructureXml from ProjectListTable where ID = 872;
select * from PageTable where ID in ('872p1402',  '872p1403','872p1404','872p1305','872p1358', '872p1353');