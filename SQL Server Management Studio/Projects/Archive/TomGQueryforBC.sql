SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

select A.ExamSessionId, A.ItemId, numAttempts
from (
	select 
		  WAREHOUSE_ExamSessionItemResponseTable.WAREHOUSEExamSessionID AS ExamSessionId,
		  WAREHOUSE_ExamSessionItemResponseTable.ItemID + 'S' +
		  x.value('../@id', 'VARCHAR(2)') + 'C' +
		  x.value('../../@id', 'VARCHAR(2)') + 'T' +
		  x.value('../../../@id', 'VARCHAR(2)') AS ItemId,
		  x.value('../@att', 'int') AS numAttempts
	from WAREHOUSE_ExamSessionItemResponseTable 
	cross apply WAREHOUSE_ExamSessionItemResponseTable.ItemResponseData.nodes('p/s/c/i/data') AS p(x)

	) A

	left join 
	(
		select WAREHOUSEExamSessionID, itemId
		from WAREHOUSE_ExamSessionDocumentTable
		where documentName IS NULL
	) B

	on 
		B.WAREHOUSEExamSessionID = A.ExamSessionId
		AND
		B.itemId = A.ItemId



