DECLARE @Username nvarchar(100) = 'davejtesting1'
DECLARE @DatabaseName nvarchar(100) = 'DataWarehouse'				--Leave Blank for ALL		'AQA_SALocal'
DECLARE @AccessRequired nvarchar(100) = 'Read'			
DECLARE @ReadRole nvarchar(100) = '[btl_developer_access]'
DECLARE @WriteRole nvarchar(100) = '[btl_developer_full_access]'
DECLARE @AssignLogin nvarchar(MAX)= ''
DECLARE @GrantRole nvarchar(MAX)= ''
DECLARE @GrantExecSPAcccess nvarchar(MAX) = ''
DECLARE @AssociateXMLSchemaCollection nvarchar(MAX) = ''

DECLARE @@BigTable TABLE (
		Schema_DatabaseName nvarchar(MAX)
	  , SchemaName nvarchar(50)
	  , XMLSchemaCollection_DatabaseName nvarchar(MAX)
	  , XMLSchemaName nvarchar(50)

)

/*Write DB names and DB Schema into a Table Variable*/

INSERT @@BigTable(Schema_DatabaseName, SchemaName)
exec sp_MSforeachdb '

use [?];

select ''?''
	, Name 
from sys.schemas 
where 
	''?'' not in (''msdb'',''master'', ''SurpassReportServer'', ''SurpassReportServerTempDB'')
and name not in 
	(select name 
		from sysusers 
			where issqlrole = 1
			)
and name not in (''dbo'',''guest'',''INFORMATION_SCHEMA'',''sys'')
and ''?'' in (select name from sys.databases where state_desc = ''ONLINE'')
'  

/*Write DB names and DB XMLSchemaCollections into a Table Variable*/

INSERT @@BigTable(XMLSchemaCollection_DatabaseName, XMLSchemaName)
EXEC sp_MSforeachdb '

	use [?];
	
	select ''?'', name 	
	from sys.xml_schema_collections
	
	where ''?'' not in (''master'', ''msdb'', ''tempdb'', ''model'')
		and name != ''sys''
		and ''?'' in (select name from sys.databases where state_desc = ''ONLINE'')

'
SELECT * FROM @@BigTable



IF LEN(@DatabaseName) > 1
BEGIN							
	(
	SELECT @AssignLogin += CHAR(13) + 'use [' + name + ']; ' + 'CREATE USER ' + @Username + ' FOR LOGIN [' + @Username + ']; '
	from sys.databases
	where name  = @DatabaseName
		and state_desc = 'ONLINE'
		
	)
	 
	( 	
		
	SELECT @GrantRole += CHAR(13) 
	
	+ 'use [' + A.name + ']; ' 	+
	
	Case When @AccessRequired = 'Read' 
		then 'CREATE ROLE '  + @ReadRole +  ' AUTHORIZATION [' + SchemaName +'];' 
				
				+ ' GRANT EXECUTE, SELECT, VIEW DEFINITION ON SCHEMA::' + SchemaName + ' TO ' + @ReadRole +';' 				
		
				+ ' EXEC sp_addrolemember '''  + @ReadRole + ''', ''' + @Username + '''; '
				
		Else 'CREATE ROLE '  + @WriteRole +  ' AUTHORIZATION [' + SchemaName +'];' 
	
				+ ' GRANT ALTER, DELETE, EXECUTE, INSERT, SELECT, UPDATE, VIEW DEFINITION ON SCHEMA::' + SchemaName + ' TO ' + @WriteRole +';' 		   
						
				+ ' EXEC sp_addrolemember ''' +	 @WriteRole + ''', ''' + @Username + '''; '
									   
		End

	from sys.databases A
	INNER join @@BigTable  B
	on B.Schema_DatabaseName COLLATE Latin1_General_CI_AS = a.name

	where name COLLATE Latin1_General_CI_AS  = @DatabaseName
		and state_desc = 'ONLINE'
	
	)
	
	(
	
	SELECT @AssociateXMLSchemaCollection +=CHAR(13) + 'use ' + A.XMLSchemaCollection_DatabaseName + ';'
		+ 'GRANT EXECUTE ON XML SCHEMA COLLECTION::' + A.XMLSchemaName + ' To ' + @Username
	from @@BigTable A
	
	
	)
	
	--(
	
	--SELECT @GrantExecSPAcccess = +=CHAR(13) 
	
	
	--)
	
END

PRINT(@AssignLogin)
PRINT(@GrantRole)
PRINT(@AssociateXMLSchemaCollection) --IF NULL, it won't create a query, which is good--
	
	
	--(
		

	--SELECT @AssociateRoleAccessAndSchema +=CHAR(13) + 'use [' + name +']; GRANT EXECUTE, SELECT, VIEW DEFINITION ON SCHEMA::' + SchemaName + ' To ' + +

	--	Case When @AccessRequired = 'Read' then @ReadRole  
	--							   Else  @WriteRole 
	--	End 
	--from sys.databases
	--inner join @@AssignSchema A
	--on A.DatabaseName collate Latin1_General_CI_AS = sys.databases.name
	
	--where name = @DatabaseName
	--	and state_desc = 'ONLINE'
	
	--)