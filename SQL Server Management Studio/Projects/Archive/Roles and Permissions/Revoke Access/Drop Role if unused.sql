DECLARE @DropRoles nvarchar(max) = ''
DECLARE @FindRoles TABLE (DBName nvarchar(max), RoleName nvarchar(max))

INSERT @FindRoles(DBName, RoleName)
EXEC sp_MSforeachdb  '
USE [?];

--insert into #DatabaseRoleMemberShip
SELECT  ''?'', roleprinc.[name]
FROM sys.database_role_members members

RIGHT JOIN sys.database_principals roleprinc
ON roleprinc.[principal_id] = members.[role_principal_id]

where is_fixed_role  = 0
	and type = ''R''
	and name != ''public''
	and name in (''btl_developer_access'',''btl_developer_full_access'')
	and roleprinc.[name] not in (
		
		--Find user Associations

		SELECT  roleprinc.[name]
		FROM sys.database_role_members members

		INNER JOIN sys.database_principals roleprinc
		ON roleprinc.[principal_id] = members.[role_principal_id]

		where is_fixed_role  = 0

	)
'


SELECT @DropRoles +=CHAR(13) + 'use ' + DBName + '; ' + 'DROP ROLE ' + RoleName
from @FindRoles


PRINT(@DropRoles);
EXEC(@DropRoles);