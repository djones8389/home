DECLARE @Username nvarchar(MAX) = 'kevinh'

IF OBJECT_ID('tempdb..##SchemaEntries') IS NOT NULL DROP TABLE tempdb..##SchemaEntries

CREATE TABLE ##SchemaEntries (

	 DatabaseName nvarchar(75)
   , SchemaName nvarchar(75)
)


EXEC sp_MSforeachdb '

	use [?];
	
	insert ##SchemaEntries
	select ''?'', name 	
	from sys.xml_schema_collections
	
	where ''?'' not in (''master'', ''msdb'', ''tempdb'', ''model'')
	and name != ''sys''
	'

DECLARE @AssignSchema nvarchar(MAX)= ''

SELECT @AssignSchema += CHAR(13) + N'use [' + DatabaseName + '];' + 'GRANT EXECUTE ON XML SCHEMA COLLECTION::' + ##SchemaEntries.SchemaName + N' TO '+@Username+';'
			from ##SchemaEntries
DROP TABLE ##SchemaEntries;

PRINT(@AssignSchema)
