DECLARE @Username nvarchar(MAX) = 'kevinh'

DECLARE @AssignSchema nvarchar(MAX) = ''

IF OBJECT_ID('tempdb..##AssignSchema') IS NOT NULL DROP TABLE tempdb..##AssignSchema

CREATE TABLE  ##AssignSchema  (

	  DatabaseName nvarchar(MAX)
	, SchemaName nvarchar(50) 

)


exec sp_MSforeachdb '

use [?];

insert into ##AssignSchema(DatabaseName, SchemaName)
select ''?''
	, Name 
from sys.schemas 
where 
	''?'' not in (''msdb'',''master'', ''SurpassReportServer'', ''SurpassReportServerTempDB'')
and name not in 
	(select name 
		from sysusers 
			where issqlrole = 1
			)
and name not in (''dbo'',''guest'',''INFORMATION_SCHEMA'',''sys'')

'  

SELECT @AssignSchema += CHAR(13) + 'use ' + DatabaseName + '; GRANT EXECUTE, SELECT, VIEW DEFINITION ON SCHEMA::' + SchemaName + ' to [' + @UserName + '] '
from ##AssignSchema

PRINT(@AssignSchema)

