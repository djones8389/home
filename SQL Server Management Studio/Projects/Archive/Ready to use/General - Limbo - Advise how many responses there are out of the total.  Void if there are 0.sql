create table #tempTable

(
      ID2 int,
      TotalNumberOfQuestions int
)

insert into #tempTable

      select
                  ID,
                  StructureXML.value('count(/assessmentDetails/assessment/section/item)[1]', 'int')
      from ExamSessionTable
      where examState = 99
      order by ID

      
      select 
                EST.ID as ID
             , COUNT (ExamSessionID) as NumberOfQuestionsAnswered
             , TT.TotalNumberOfQuestions      
             , EST.KeyCode as Keycode
             , UT.Forename as Forename
             , UT.Surname as Surname
             , UT.CandidateRef as CandidateRef
         from ExamSessionItemResponseTable as ESIRT

            inner join ExamSessionTable as EST
            on EST.ID = ESIRT.ExamSessionID

            inner join UserTable as UT
            on UT.ID = EST.UserID
            
            inner join #tempTable as TT
            on TT.ID2 = EST.ID

            WHERE examState = 99
            
      
GROUP BY TT.ID2, TT.TotalNumberOfQuestions, ExamSessionID, EST.ID, EST.KeyCode, UT.Forename, UT.Surname, CandidateRef      
order by EST.ID

select ID, Keycode as ALL_Limbos from ExamSessionTable where examState = 99
--begin tran
update ExamSessionTable 
            set previousExamState = examState,
                  examState = 10
                        where examState = 99
      
      and ID not IN
            ( 
            select ExamSessionID from
                        ExamSessionItemResponseTable
                        )
select ID, Keycode as VOIDED_Limbos, examState, previousExamState from ExamSessionTable where examState = 10
--rollback
drop table #tempTable


