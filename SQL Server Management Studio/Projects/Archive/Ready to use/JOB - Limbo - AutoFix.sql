DECLARE  @subject VARCHAR(30),
		 @query NVARCHAR(MAX);

SELECT	 @subject = CAST(COUNT(ExamSessionTable.ID) AS NVARCHAR(11)) + ' Exams in Limbo.'
 FROM	 ExamSessionTable
WHERE ExamSessionTable.ExamState = 99;

SET @query = 

'
SET QUOTED_IDENTIFIER ON


select ID, Keycode as Keycode_For_All_Limbo_Exams from ExamSessionTable where examState = 99

update ExamSessionTable
	set previousExamState = examState,
		examState = 10
	where examState = 99

	and ID not in
	
	(
	select ExamSessionID
		from ExamSessionItemResponseTable
		)
		
		
	update ExamSessionTable
	set previousExamState = examState,
		examState = 9
	where examState = 99

	and ID in
	
	(
	select ExamSessionID
		from ExamSessionItemResponseTable
		)
		
select ID, Keycode as Keycode_For_All_Voided_Or_Submitted_Limbos, examState, previousExamState from ExamSessionTable where examState in (9,10) and previousexamState = 99
		
            ';


--execute as login = 'Administrators'
EXECUTE	 msdb.dbo.sp_send_dbmail
		 @profile_name = N'Local SMTP Virtual Server',
		 @recipients = N'liveservices@btl.com; support@btl.com',
		 @subject = @subject,
		 @body = 'See attached for "Auto-Fixed" Limbo exams',
		 @execute_query_database = N'SKILLSFIRST_SecureAssess_10.0',
		 @query = @query,
		 @attach_query_result_as_file = 1,
		 @query_attachment_filename = N'Skillsfirst Limbo Exams.csv',
		 @query_result_separator = N',',
		 @query_result_no_padding = 1;		 