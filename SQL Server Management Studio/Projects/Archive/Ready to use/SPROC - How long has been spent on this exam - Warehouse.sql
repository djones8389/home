use SADB
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure BTL_DaveJ_HowLongWasSpentOnThisExam_Warehouse

@examSessionID int

AS
BEGIN

select 

 ID
, resultData.value('(exam/@totalTimeSpent)[1]', 'decimal(10,2)') as TotalTimeSpentOnExam_InMilliseconds
, resultData.value('(exam/@totalTimeSpent)[1]', 'decimal(10,2)')/1000 as TotalTimeSpentOnExam_InSeconds
, resultData.value('(exam/@totalTimeSpent)[1]', 'decimal(10,2)')/60000 as TotalTimeSpentOnExam_InMinutes


from WAREHOUSE_ExamSessionTable 
where ID = @examSessionID


--drop procedure BTL_DaveJ_HowLongWasSpentOnThisExam_Warehouse

END
