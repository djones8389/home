/*

SELECT top 500 UT.Forename, UT.Surname, EST.Keycode, EST.examState, EST.previousExamState, EST.downloadInformation, EST.clientInformation
FROM UserTable as UT
INNER JOIN ExamSessionTable as EST
ON UT.ID=EST.UserID
where examState <> 10
and downloadInformation is not null
ORDER BY UT.Surname


*/


SELECT top 500 Forename, Surname, EST.Keycode, EST.examState, EST.previousExamState, EST.downloadInformation, EST.clientInformation
FROM UserTable
INNER JOIN ExamSessionTable as EST
ON UserTable.ID=EST.UserID
where examState <> 10
and downloadInformation is not null
ORDER BY Surname

--Declare @Exam1 int
--set @Exam1 = 1681

