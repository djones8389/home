--exec sp_MSforeachdb N'
--SET QUOTED_IDENTIFIER ON;
--USE [?];

--if (''?'' like ''%BritishCouncil_SecureAssess_LIVE'')
--	Select ''?'' as ClientName, ID, examState, Keycode, previousexamState, pinnumber
--		from ExamSessionTable
--			where examState = 99


--	update ?.dbo.ExamSessionTable
--	set pinnumber = ''1234''
--	where ID = 617615
				
--'


declare @query varchar(max) = ''
select  @query = 

@query + 'UPDATE ' + name + 
            '.dbo.ExamSessionTable 
				set previousExamState = examState, examState = 10 
                 where examState = 99
					and ID not in					
					(
					select ExamSessionID
						from ExamSessionItemResponseTable
						where ItemResponseData.exist(''p[@ua=1]'') = 1
						)
		
		
				update ExamSessionTable
				set previousExamState = examState,	examState = 9
				where examState = 99
				and ID in				
				(
				select ExamSessionID
					from ExamSessionItemResponseTable
					where ItemResponseData.exist(''p[@ua=1]'') = 1
					)

								
				'
from    master.sys.databases
where   name like '%SecureAssess%'
	and state_desc = 'ONLINE'
--select @query
exec (@query)
--select @query





SELECT 
	d.state_desc
FROM sys.databases as d

inner join sys.master_files as m
on (m.database_id = d.database_id) 

where recovery_model_desc = 'FULL'

order by size desc;
GO
