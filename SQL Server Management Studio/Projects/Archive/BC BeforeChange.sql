--BEFORE CHANGE-- THis is the original one

USE [BritishCouncil_SecureAssess_LIVE]
GO

/****** Object:  Table [dbo].[WAREHOUSE_ExamSessionDocumentTable]    Script Date: 09/11/2014 14:13:22 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[WAREHOUSE_ExamSessionDocumentTable](
	[ID] [int] NOT NULL,
	[warehouseExamSessionID] [int] NOT NULL,
	[itemId] [nvarchar](20) NOT NULL,
	[documentName] [nvarchar](200) NOT NULL,
	[Document] [image] NOT NULL,
	[uploadDate] [datetime] NOT NULL,
 CONSTRAINT [PK_WAREHOUSE_ExamSessionDocumentTable] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

ALTER TABLE [dbo].[WAREHOUSE_ExamSessionDocumentTable]  WITH CHECK ADD  CONSTRAINT [FK_WAREHOUSE_ExamSessionDocumentTable_WAREHOUSE_ExamSessionTable] FOREIGN KEY([warehouseExamSessionID])
REFERENCES [dbo].[WAREHOUSE_ExamSessionTable] ([ID])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[WAREHOUSE_ExamSessionDocumentTable] CHECK CONSTRAINT [FK_WAREHOUSE_ExamSessionDocumentTable_WAREHOUSE_ExamSessionTable]
GO

ALTER TABLE [WAREHOUSE_ExamSessionDocumentTable]
DROP CONSTRAINT FK_WAREHOUSE_ExamSessionDocumentTable_WAREHOUSE_ExamSessionTable;
GO