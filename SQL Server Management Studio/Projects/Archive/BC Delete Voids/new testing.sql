USE SANDBOX_BC_SecureAssess

select  west.ID--, warehouseTime
	--, wesdt.*
	--, wesirt.*
	,wscet.ID
from WAREHOUSE_ExamSessionTable as west

inner join WAREHOUSE_ExamSessionDocumentTable as wesdt
on wesdt.WarehouseExamSessionID = west.ID

inner join WAREHOUSE_ScheduledExamsTable as wscet
on wscet.ID = west.WAREHOUSEScheduledExamID

inner join WAREHOUSE_ExamSessionItemResponseTable as wesirt
on wesirt.WAREHOUSEExamSessionID = west.ID

where west.id = 5558

--DELETE FROM WAREHOUSE_ScheduledExamsTable where ID = 5839

--DELETE FROM WAREHOUSE_ExamSessionTable WHERE ID = 15387

--5558	5839
/*
918219
898207
4388
5192
5558
*/

--DROP TABLE ##ExamsToDelete

select west.ID as ESID, wscet.ID as SCETID, wscet.WAREHOUSECentreID as CentreID				--320713 Exams
into ##ExamsToDelete
from WAREHOUSE_ExamSessionTable as west
inner join WAREHOUSE_ScheduledExamsTable as wscet
on west.WAREHOUSEScheduledExamID = wscet.ID
where warehouseTime <= DATEADD(Month, -6, GETDATE())
	and PreviousExamState = 10


select count(*) from WAREHOUSE_ExamSessionAvailableItemsTable where ExamSessionID in (SELECT ESID from ##ExamsToDelete);
select count(*) from WAREHOUSE_ExamSessionDocumentTable where warehouseExamSessionID in (SELECT ESID from ##ExamsToDelete);
select count(*) from WAREHOUSE_ExamSessionItemResponseTable where WAREHOUSEExamSessionID in (SELECT ESID from ##ExamsToDelete);
select count(*) from WAREHOUSE_ExamSessionTable where ID in (SELECT ESID from ##ExamsToDelete);
select count(*) from WAREHOUSE_ExamSessionTable_Shreded where examSessionID in (SELECT ESID from ##ExamsToDelete);
select count(*) from WAREHOUSE_ExamSessionTable_ShrededItems where examSessionID in (SELECT ESID from ##ExamsToDelete);
select count(*) from WAREHOUSE_ExamStateAuditTable where WAREHOUSEExamSessionID in (SELECT ESID from ##ExamsToDelete);
select count(*) from WAREHOUSE_ScheduledExamsTable where ID IN (SELECT SCETID from ##ExamsToDelete);
select count(*) from WAREHOUSE_ExamSessionDurationAuditTable where Warehoused_ExamSessionID in (SELECT ESID from ##ExamsToDelete);
select count(*) from WAREHOUSE_ExamSessionResultHistoryTable where  WareHouseExamSessionTableID in (SELECT ESID from ##ExamsToDelete);
select count(*) from WAREHOUSE_CentreTable where ID in (SELECT CentreID from ##ExamsToDelete);
