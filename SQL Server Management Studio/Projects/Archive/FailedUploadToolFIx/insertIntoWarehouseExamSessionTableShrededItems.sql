-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
SET NOCOUNT ON;
BEGIN TRY

	--insert the records
	insert into [WAREHOUSE_ExamSessionTable_ShrededItems](examSessionId, ItemRef, ItemName, userMark, markerUserMark, examPercentage, ItemVersion, responsexml, OptionsChosen, selectedCount, correctAnswerCount, userAttempted)
		SELECT 
			@myExamSessionId2, -- exam session id
			ParamValues.Item.query('data(@id)').value('.','nvarchar(max)') as itemref, -- Item Reference
			ParamValues.Item.query('data(@name)').value('.','nvarchar(max)'), -- Item Name
			CONVERT(decimal(6,3), ParamValues.Item.query('data(@userMark)').value('.','nvarchar(max)')), -- User mark
			ParamValues.Item.query('data(@markerUserMark)').value('.','nvarchar(max)'), -- markerUserMark
			CONVERT(decimal(6,3), @myResultData.query('data(/exam/@userPercentage)').value('.','nvarchar(max)')), -- exam percentage
			(SELECT [ItemVersion]
			FROM [WAREHOUSE_ExamSessionItemResponseTable]
			WHERE [WAREHOUSE_ExamSessionItemResponseTable].WAREHOUSEExamSessionId = @myExamSessionId2
			AND [ItemID] = ParamValues.Item.query('data(@id)').value('.','nvarchar(max)')), -- item version
			(SELECT [ItemResponseData]
			FROM [WAREHOUSE_ExamSessionItemResponseTable]
			WHERE [WAREHOUSE_ExamSessionItemResponseTable].WAREHOUSEExamSessionId = @myExamSessionId2
			AND [ItemID] = ParamValues.Item.query('data(@id)').value('.','nvarchar(max)')), -- response xml
			ISNULL((
			SELECT ' ' + CONVERT(nvarchar(max),[ItemResponseData].query('data(//c[@typ=''10'']/i[@sl=''1'']/@ac)')) + ' '
			FROM [WAREHOUSE_ExamSessionItemResponseTable]
			WHERE [WAREHOUSE_ExamSessionItemResponseTable].WAREHOUSEExamSessionId = @myExamSessionId2 
			AND [ItemID] = ParamValues.Item.query('data(@id)').value('.','nvarchar(max)')),' UA '), -- options chosen
			ISNULL((
			SELECT CONVERT(nvarchar(max),[ItemResponseData].query('count(//i[@sl=''1''])'))
			FROM [WAREHOUSE_ExamSessionItemResponseTable]
			WHERE [WAREHOUSE_ExamSessionItemResponseTable].WAREHOUSEExamSessionId = @myExamSessionId2 
			AND [ItemID] = ParamValues.Item.query('data(@id)').value('.','nvarchar(max)')),0), -- selected count 
			ISNULL((
			SELECT CONVERT(nvarchar(max),[ItemResponseData].query('count(//i[@ca=''1''])'))
			FROM [WAREHOUSE_ExamSessionItemResponseTable]
			WHERE [WAREHOUSE_ExamSessionItemResponseTable].WAREHOUSEExamSessionId = @myExamSessionId2 
			AND [ItemID] = ParamValues.Item.query('data(@id)').value('.','nvarchar(max)')),1), -- correct answer count
			ParamValues.Item.query('data(@userAttempted)').value('.','bit') -- userAttempted
		FROM @myStructureXml.nodes('/assessmentDetails/assessment/section/item') as ParamValues(Item)

	
	--Return success errorCode string
	DECLARE @errorReturnString 	nvarchar(max)
	SET @errorReturnString = '<result errorCode="0"><return>true</return></result>'
	SELECT @errorReturnString

END TRY
BEGIN CATCH
	SELECT '1' as '@errorCode', --Attribute to our root (btl standard 'result') node
	(
		SELECT ERROR_MESSAGE() AS "*"
		FOR XML PATH('') ,TYPE
	)
	FOR XML PATH('return'), ROOT('result')--Our outermost/root node
END CATCH;