SELECT EST.ID, EST.KeyCode, EST.examState, UT.Forename, UT.Surname, CT.CentreName, SCET.examName, est.StructureXML, examVersionRef
	,est.AutoVoidDate, est.AwaitingUploadGracePeriodInDays, est.AwaitingUploadStateChangingTime, attemptAutoSubmitForAwaitingUpload
  FROM ExamSessionTable as EST

  Inner Join ScheduledExamsTable as SCET
  on SCET.ID = EST.ScheduledExamID
  
  Inner Join UserTable as UT
  on UT.ID = EST.UserID

  Inner Join CentreTable as CT
  on CT.ID = SCET.CentreID

where ut.candidateref  in ('455667806', 'AUTO6437c7f2e70c4b91b55f8034fd941530')
	and examVersionRef = 'August 2012_V1R'
	and examState = 4
	
	order by KeyCode desc

