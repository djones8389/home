USE SQA_CPProjectAdmin_DI

--Step #1 - Run AndyB script to write in the missing entries
--Need to set DelCol = 0 - Only for items that need it
--Need to set visible = 0 - Only for items that need it
--Need to set ItemGraphicTable.ali to be = to ProjectManifest.ID

--1 hr 8 mins to run...
	   
    Select 
		DISTINCT
		  A.Name
		 , A.ProjectID
		 , A.PageID
		 , A.ID
		 , A.Ali as 'ItemGraphic ref'
		 , B.ID as 'PMT ali ref'
	into #FROM_ItemGraphicTable
	FROM (
	SELECT
           PLT.Name
		   , SUBSTRING(IT.ID, 0, CHARINDEX('P', IT.ID)) as ProjectID
		   , SUBSTRING(IT.ID, CHARINDEX('P', IT.ID) + 1, CHARINDEX('S',IT.ID) - CHARINDEX('P', IT.ID) - 2 + Len('S')) as PageID
		   , IT.ID
		   , IT.ALI
      from ItemGraphicTable it 
            INNER JOIN ComponentTable c ON c.ID = it.ParentID 
            INNER JOIN SceneTable s ON s.ID = c.ParentID 
            INNER JOIN PageTable i ON i.id = S.ParentID
			INNER JOIN ProjectListTable PLT on PLT.ID = I.ParentID
	   where ali not like '%[0-9]%' and ali != '' and ali != 'placeholder'

	   )  A

	INNER JOIN		--Find where the Ali is in the ProjectManifestTable, and obtain the ID reference
	(
		select  ID
				, ProjectID
				, SUBSTRING(Name,0,CHARINDEX('.', Name)) as Name
				, sharedLib
		from ProjectManifestTable 
		where name not like '%.fla'
				and SharedLib = 1
				and DelCol = 1
	)   B

On A.ProjectID = B.ProjectId	
	and A.ali = B.name

	INNER JOIN			--Has to exist in SharedLibrary AND ProjectManifestTable
	(
		SELECT X.ProjectID
			  ,sl.i.value('@id', 'int') ItemID
			  ,sl.i.value('@name', 'nvarchar(max)') ItemName
		FROM (
			SELECT  SharedLibraryTable.ProjectID		
				,  CAST(SharedLibraryTable.structureXML AS XML) AS [StructureXML]
			FROM dbo.SharedLibraryTable
			) X
		CROSS APPLY X.StructureXML.nodes('//item') sl(i)
	)  X 

ON B.ProjectId = X.ProjectID
	AND B.ID = X.ItemID;


--Store this,  these are your affected pages--
SELECT * FROM #FROM_ItemGraphicTable


update ProjectManifestTable
set DelCol = 0
From ProjectManifestTable a

inner join #FROM_ItemGraphicTable b
on b.projectid = a.ProjectId
	and a.id = b.[PMT ali ref];




DROP TABLE #FROM_ItemGraphicTable
