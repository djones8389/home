USE UAT_OCR_ContentProducer

DECLARE @ProjectStructureXml XML;
DECLARE @ID INT;
DECLARE @ItemID NVARCHAR(50);

DECLARE ProjectXML CURSOR
FOR
SELECT ID
	,ProjectStructureXml
FROM ProjectListTable
WHERE ID IN (6011)
		
		--WHERE ID IN (
		--	SELECT PT.ParentID
		--	FROM dbo.ItemGraphicTable IGT
		--	INNER JOIN dbo.ComponentTable CT ON IGT.ParentID = CT.ID
		--	INNER JOIN dbo.SceneTable ST ON ST.ID = CT.ParentID
		--	INNER JOIN dbo.PageTable PT ON PT.ID = ST.ParentID
		--	WHERE IGT.loM IS NULL
		--)

OPEN ProjectXML;

FETCH NEXT
FROM ProjectXML
INTO @ID
	,@ProjectStructureXml;

WHILE @@FETCH_STATUS = 0
BEGIN
	DECLARE PageXML CURSOR
	FOR
	SELECT ProjectStrcutureXML.Page.value('(@ID)', 'NVARCHAR(50)')
	FROM @ProjectStructureXml.nodes('//Pag') AS ProjectStrcutureXML(Page);

	OPEN PageXML;

	FETCH NEXT
	FROM PageXML
	INTO @ItemID;

	WHILE @@FETCH_STATUS = 0
	BEGIN
		IF @ProjectStructureXml.exist('(//Pag[@ID = sql:variable("@ItemID")]/@alF)[1]') = 0
			SET @ProjectStructureXml.modify('insert attribute alF {"1"} into (//Pag[@ID = sql:variable("@ItemID")])[1]');
		ELSE
			SET @ProjectStructureXml.modify('replace value of (//Pag[@ID = sql:variable("@ItemID")][1]/@alF)[1] with ("1") ');

		FETCH NEXT
		FROM PageXML
		INTO @ItemID
	END

	CLOSE PageXML;

	DEALLOCATE PageXML;

	UPDATE ProjectListTable
	SET ProjectStructureXml = @ProjectStructureXml
	WHERE ID = @ID

	FETCH NEXT
	FROM ProjectXML
	INTO @ID
		,@ProjectStructureXml;
END

CLOSE ProjectXML;

DEALLOCATE ProjectXML;

GO

UPDATE PageTable
SET ver = ver + 1 
where ParentID = 6011;

--UPDATE dbo.ItemGraphicTable 
--SET loM = 1
--WHERE loM IS NULL-- OR loM = 0




