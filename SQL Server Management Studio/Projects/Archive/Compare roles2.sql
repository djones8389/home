----USE UAT_BC2_SecureAssess

--SELECT RPT.*--, RT.*
--FROM  UAT_BC2_SecureAssess.dbo.RolePermissionsTable AS RPT

--INNER JOIN UAT_BC2_SecureAssess.dbo.RolesTable AS RT
--ON RT.id = RPT.RoleID

--WHERE RT.Name IN ('Org IT Admin', 'superuser', 'Standard Integration', 'IntegrationRole')

--EXCEPT

----USE UAT_BCGuilds_SecureAssess_11_7_223_0_IP


--SELECT RPT.*--, RT.*
--FROM UAT_BCGuilds_SecureAssess_11_7_223_0_IP.dbo.RolePermissionsTable AS RPT

--INNER JOIN UAT_BCGuilds_SecureAssess_11_7_223_0_IP.dbo.RolesTable AS RT
--ON RT.id = RPT.RoleID

--WHERE RT.Name IN ('Org IT Admin', 'superuser', 'Standard Integration', 'IntegrationRole')



USE UAT_BC2_SecureAssess

SELECT distinct

    UT.ID
	, ut.Username
      ,Forename
      ,Surname
      ,ut.Retired
      ,ct.CentreName
      ,ct.CentreCode
      ,RT.Name as RoleName
	  , PT.*
from UserTable as UT

inner join AssignedUserRolesTable as AURT
on AURT.UserID = UT.ID

inner join CentreTable as CT
on CT.ID = AURT.CentreID

inner join RolesTable as RT
on RT.ID = AURT.RoleID

inner join RolePermissionsTable as RPT
on RPT.RoleID = RT.ID

--inner join UserQualificationsTable as UQT
--on UQT.UserID = UT.ID

--inner join IB3QualificationLookup as IB
--on IB.ID = UQT.QualificationID

inner join PermissionsTable as PT
on PT.ID = RPT.PermissionID

where  forename ='Service' AND UT.Surname ='user'
	AND CT.CentreName = 'Global Centre'
	AND ut.id = 1081
		ORDER BY RT.Name 















USE UAT_BCGuilds_SecureAssess_11_7_223_0_IP

SELECT distinct

    UT.ID
	, ut.Username
      ,Forename
      ,Surname
      ,ut.Retired
      ,ct.CentreName
      ,ct.CentreCode
      ,RT.Name as RoleName
		, pt.*
from UserTable as UT

inner join AssignedUserRolesTable as AURT
on AURT.UserID = UT.ID

inner join CentreTable as CT
on CT.ID = AURT.CentreID

inner join RolesTable as RT
on RT.ID = AURT.RoleID

inner join RolePermissionsTable as RPT
on RPT.RoleID = RT.ID

--inner join UserQualificationsTable as UQT
--on UQT.UserID = UT.ID

--inner join IB3QualificationLookup as IB
--on IB.ID = UQT.QualificationID

inner join PermissionsTable as PT
on PT.ID = RPT.PermissionID

where  UT.id = 23
	AND CT.CentreName = 'Global Centre'
	AND RT.Name NOT IN ('BTLTestRole', 'Centre Exams Officer')
	ORDER BY RT.Name 