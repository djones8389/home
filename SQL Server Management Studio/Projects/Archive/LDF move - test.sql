/*Create DBs*/

/*
RESTORE DATABASE [STG_BCGuilds_SecureAssess] FROM DISK = N'E:\BCGUILDS.ALL2015.09.25.bak' WITH FILE = 1,  NOUNLOAD, NOREWIND,  STATS = 10, MOVE 'UAT_BCGuilds_SecureAssess_11_7_223_0_IP' TO N'E:\UAT_BCGuilds_SecureAssess_11_7_223_0_IP.mdf', MOVE 'UAT_BCGuilds_SecureAssess_11_7_223_0_IP_log' TO N'E:\UAT_BCGuilds_SecureAssess_11_7_223_0_IP.ldf';
RESTORE DATABASE [STG_BCGuilds_SecureMarker] FROM DISK = N'E:\BCGUILDS.ALL2015.09.25.bak' WITH FILE = 2,  NOUNLOAD, NOREWIND,  STATS = 10, MOVE 'UAT_BCGuilds_SecureMarker_2_7_0_124_IP' TO N'E:\UAT_BCGuilds_SecureMarker_2_7_0_124_IP.mdf', MOVE 'UAT_BCGuilds_SecureMarker_2_7_0_124_IP_log' TO N'E:\UAT_BCGuilds_SecureMarker_2_7_0_124_IP.ldf';
RESTORE DATABASE [STG_BCGuilds_TestPackage] FROM DISK = N'E:\BCGUILDS.ALL2015.09.25.bak' WITH FILE = 3,  NOUNLOAD, NOREWIND,  STATS = 10, MOVE 'UAT_BCGuilds_TestPackage_11_6_0_241_IP' TO N'E:\UAT_BCGuilds_TestPackage_11_6_0_241_IP.mdf', MOVE 'UAT_BCGuilds_TestPackage_11_6_0_241_IP_log' TO N'E:\UAT_BCGuilds_TestPackage_11_6_0_241_IP.ldf';
*/

/*

DROP DATABASE [STG_BCGuilds_SecureAssess]
DROP DATABASE [STG_BCGuilds_SecureMarker]
DROP DATABASE [STG_BCGuilds_TestPackage]

*/


/*Alter LDF Locations*/

use master;

DECLARE @NewLogLocation nvarchar(20) = 'E:\Log'

DECLARE @AlterLocation nvarchar(MAX) = '';
DECLARE @moveLog nvarchar(MAX) = '';
DECLARE @setOnline nvarchar(MAX) = '';
DECLARE @TakeOffline nvarchar(MAX) = '';

DECLARE @Tables TABLE(DBName nvarchar(100), LogFileName nvarchar(100), OriginalLocation nvarchar(250))
INSERT @Tables
SELECT D.Name, F.Name, F.physical_name
FROM sys.master_files as F
inner join sys.databases as D
on D.database_id = F.database_id 
where D.name LIKE 'STG_BCGuilds_%'
	and type_desc = 'LOG'

SELECT @TakeOffline += char(13) + 
	'ALTER DATABASE [' + DBName + '] SET OFFLINE;
'
FROM @Tables 

SELECT @AlterLocation += char(13) + 
	'ALTER DATABASE [' + DBName + '] MODIFY FILE (NAME = ' + LogFileName  + ', Filename=''' + @NewLogLocation + '\'+ LogFileName + '.ldf'');
'
FROM @Tables


SELECT @setOnline += char(13) +  
	'use master;  ALTER DATABASE [' + DBName + '] SET ONLINE;'
FROM @Tables 


SELECT @moveLog += char(13) +  
	 'EXECUTE xp_cmdshell ''copy "'+  OriginalLocation + '" "'+ @NewLogLocation + '\' + LogFileName + '.ldf' + '"'''
FROM @Tables 

PRINT(@TakeOffline);
PRINT(@AlterLocation);
/*PRINT(@moveLog);*/
PRINT(@setOnline);
