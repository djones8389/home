select RT.Name as 'Role Name'
from RolesTable as RT (NOLOCK)
order by 1 asc;

select RT.Name as 'Role Name',  PT.Name as 'Permission Name'
from RolesTable as RT (NOLOCK)

INNER JOIN RolePermissionsTable as RPT (NOLOCK)
on RPT.RoleID = RT.ID

INNER JOIN PermissionsTable as PT (NOLOCK)
on PT.ID = RPT.PermissionID

order by 1, 2;


Select RT.Name as 'Role Name', count(userid) as 'Number Of Users'
from AssignedUserRolesTable (NOLOCK)
INNER JOIN RolesTable as RT (NOLOCK)
on RT.id = AssignedUserRolesTable.RoleID
group by RT.Name
order by RT.Name;


--SELECT *  from AssignedUserRolesTable where RoleID = 9