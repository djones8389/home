SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
--, cast(document as varbinary(MAX))

--38982 without the distinct..

select DISTINCT A.ItemID, A.warehouseExamSessionID, B.DL
FROM 
(
SELECT
	ItemID
	, warehouseexamsessionid
from Warehouse_ExamSessioNDocumentTable

--where warehouseexamsessionid = 535904
group by ItemID, warehouseexamsessionid
having count(ItemID) > 1

) A

INNER JOIN (

SELECT
	ItemID
	, warehouseexamsessionid
	, DATALENGTH(document) as DL
from Warehouse_ExamSessioNDocumentTable

--where warehouseexamsessionid = 535904

)  B

On A.warehouseExamSessionID = B.warehouseExamSessionID
	and A.itemId = B.itemId
	order by 2, 1