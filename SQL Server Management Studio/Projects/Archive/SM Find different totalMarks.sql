--use BritishCouncil_SecureAssess_LIVE

--select top 10 * from WAREHOUSE_ExamSessionTable_ShrededItems order by ExamSessionID desc

--select top 10 WAREHOUSE_ExamSessionTable.ID, WAREHOUSE_ExamSessionTable.Keycode
--	 , CandidateExamVersions.*
--	from WAREHOUSE_ExamSessionTable 

--	inner join BritishCouncil_SecureMarker_LIVE..CandidateExamVersions
--	on CandidateExamVersions.Keycode = WAREHOUSE_ExamSessionTable.KeyCode

--where ExportToSecureMarker = 1
--	order by WAREHOUSE_ExamSessionTable.ID DESC
	
	
	--create table DJUniqueResponse as (
	

	--drop table #DJUniqueResponse
	--)
	
select ID, ItemID 
  into #DJUniqueResponse
from BritishCouncil_SecureMarker_LIVE..UniqueResponses;

select UniqueResponseId
  into #DJAssignedItemMarks
from BritishCouncil_SecureMarker_LIVE..AssignedItemMarks;

	
select top 10 WEST.ID, WEST.Keycode, WESTSI.ItemRef, WESTSI.TotalMark, I.TotalMark
	from WAREHOUSE_ExamSessionTable as WEST
	
	inner join WAREHOUSE_ExamSessionTable_ShrededItems as WESTSI
	on WESTSI.examSessionId = WEST.ID

	inner join BritishCouncil_SecureMarker_LIVE..CandidateExamVersions as CEV
	on CEV.Keycode = WEST.KeyCode

	INNER JOIN BritishCouncil_SecureMarker_LIVE..CandidateResponses as CR
	ON CR.CandidateExamVersionID = CEV.ID

	INNER JOIN BritishCouncil_SecureMarker_LIVE..#DJUniqueResponse as UR
	ON UR.ID = CR.UniqueResponseID 

	--inner join BritishCouncil_SecureMarker_LIVE..UniqueResponseDocuments as URD
	--ON URD.UniqueResponseID = UR.id 
		
	Inner Join BritishCouncil_SecureMarker_LIVE..Items as I
	on I.ID = UR.itemid
		and I.ExternalItemID = WESTSI.ItemRef

	inner join BritishCouncil_SecureMarker_LIVE..#DJAssignedItemMarks as AIM
	on AIM.UniqueResponseId = UR.id


where ExportToSecureMarker = 1
	and WESTSI.TotalMark <> I.TotalMark
	and WESTSI.TotalMark <> 0
	order by WEST.ID DESC;	
	
	--select * from BritishCouncil_SecureMarker_LIVE..UniqueResponses
	
	
	
	
	
	/*
	use BritishCouncil_SecureMarker_LIVE
	
	create table DJUniqueResponses
	(
		id bigint
		, itemID int
	)
	
	insert into DJUniqueResponses(id, itemID) values()
	OPENQUERY
	
	*/