select 

PLT.Name
,PLT.ID
,Forename + ' ' + Surname
,UT.Username
,C.ContractID
,CP.*
,CTL.*
,ISL.SpecificationName

 from Contract as C

inner join ProjectListTable as PLT
on PLT.ID = C.ProjectID

inner join UserTable as UT
on UT.UserID = C.UserID

inner join ContractPage as CP
on CP.ContractID = C.ID

inner join ContractTypeLookup as CTL
on CTL.ID = C.Type

inner join ItemSpecificationLookup as ISL
on ISL.ID = C.ItemSpecificationID	and ISL.SpecificationName in (1, 10, 11, 7, 8, 9)

 --where UT.UserName in ('134346','107908','136995','122229','135353','122678','4468','123302','17875','22977','131996','126736','136434','126740','126735','6650','1876','122186','1011','495','123309','123648','126662','123313','123315','117151','7561','20479','6942','134114','134176','119497','132513','111836','22595','107770','2983','5593','114532','4647','128007','22355','115625','22575','9157','131846','9277','7690','109107','106543','139976','121565','117703','2972','114641','9308','18821','111321','130757','131263','126351','22463','122699','7449','112289','122370','122749','128690','135341','100563','126055','126775','1982','122765','2142','113819','103360','122872','2630','112023','121694','126863','127904','107517','113565')
 where PLT.ID = 1107
	and CTL.ID = 2
	order by Username, ISL.SpecificationName
 
 
 begin tran
 
 declare @ProjectID int = 1107;
 declare @ContractID int = 361;
 declare @UserName nvarchar(20) = '122678';
 
 --  1 is Writer,  2 is Checker
 declare @ContractType int = 1;

 select * from Contract
  where ProjectID = @ProjectID
	and Type = @ContractType
	and UserID =  (select UserID from UserTable where Username = @UserName)
	and ContractID = @ContractID
    and ItemSpecificationID in (select ID from ItemSpecificationLookup where SpecificationName IN (1, 2, 3, 4, 5, 35));



 update Contract with (NOLOCK)
 set ContractItems = 
			(select COUNT(PageID) 
				FROM ContractPage with (NOLOCK) 
					where ProjectID = @ProjectID
					and ContractPage.ContractID = @ContractID
					)
 
  where ProjectID = @ProjectID
	and Type = @ContractType
	and UserID =  (select UserID from UserTable where Username = @UserName)
	and ContractID = @ContractID
    and ItemSpecificationID in (select ID from ItemSpecificationLookup where SpecificationName IN (1, 10, 11, 7, 8, 9));
 
  select * from Contract
  where ProjectID = @ProjectID
	and Type = @ContractType
	and UserID =  (select UserID from UserTable where Username = @UserName)
	and ContractID = @ContractID
    and ItemSpecificationID in (select ID from ItemSpecificationLookup where SpecificationName IN (1, 10, 11, 7, 8, 9));
 
 rollback
 
 
 --SELECT * FROM Contract WHERE ContractID = 325
 
 SELECT * FROM ItemSpecificationLookup 
 --1944--
 SELECT * FROM Contract WHERE ContractID = '325' AND ItemSpecificationID = 4
 
 SELECT * FROM ContractPage WHERE ContractID = 1821
 
 
 
 
 
 select * 
 
 from Contract as C
 
 inner join ContractPage as CP
 on CP.ContractID = C.ContractID
 
 inner join ContractTypeLookup as CTL
 on CTL.ID = C.Type

 inner join ItemSpecificationLookup as ISL
 on ISL.ID = C.ItemSpecificationID	and ISL.SpecificationName in (1, 10, 11, 7, 8, 9)
 WHERE C.ContractID = '325';
 
  
SELECT * FROM ItemSpecificationLookup where SpecificationName in (1, 10, 11, 7, 8, 9) ;
 
SELECT * FROM Contract WHERE ContractID = '325' AND ItemSpecificationID in (4,6,12,18,19,20);


SELECT * FROM Contract WHERE ContractID = 325

SELECT * FROM ContractPage WHERE ContractID = 1819
SELECT * FROM PageTable WHERE ID = '1107P1302'

--Update Contract
--set ContractItems = 0, CompletionDate = GETDATE()
--where ID = 1819



--update Contract
--set ContractItems = 1
--where ID in (1823,1836, 1841)

--update Contract
--set ContractItems = 2
--where ID in (1837,1839,1842)