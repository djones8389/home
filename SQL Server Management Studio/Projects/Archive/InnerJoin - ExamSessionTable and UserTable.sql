/****** Script for SelectTopNRows command from SSMS  ******/
SELECT top 100 UserTable.ID
      [CandidateRef]
      ,ES.KeyCode
      ,[Forename]
      ,[Surname]
      ,[Username]
      ,[ULN]
  FROM [SA_TEST_R10SP1].[dbo].[UserTable]
  
  FULL JOIN ExamSessionTable as ES
  ON ES.KeyCode = KeyCode
  Where Surname <> 'Jones'