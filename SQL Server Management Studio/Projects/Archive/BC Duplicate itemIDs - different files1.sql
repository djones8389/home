SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

IF OBJECT_ID('tempdb..#TEMP') IS NOT NULL DROP TABLE #TEMP

SELECT
	ROW_NUMBER() OVER(PARTITION BY a.warehouseExamSessionID , a.itemId , a.DL ORDER BY a.warehouseExamSessionID , a.itemId) as [N]
	, A.itemId
	, A.warehouseExamSessionID
	, A.DL
INTO #TEMP
FROM
(
SELECT
	ItemID
	, warehouseexamsessionid
	, DATALENGTH(document) as DL
from Warehouse_ExamSessioNDocumentTable (NOLOCK)
) A
INNER JOIN
(
SELECT
	ItemID
	, warehouseexamsessionid
from Warehouse_ExamSessioNDocumentTable

group by ItemID, warehouseexamsessionid
having count(ItemID) > 1
) B

ON A.warehouseexamsessionid = B.warehouseexamsessionid
	and A.ItemID = B.ItemID;



--SELECT COUNT(N) as 'CountOfDifferentFiles', warehouseExamSessionID, itemId 
--FROM #Temp
--group by N,  warehouseExamSessionID , itemId 
--having count(*) > 1
--order by warehouseExamSessionID;


SELECT C.*
FROM
(
SELECT ID
	,ItemID
	, warehouseexamsessionid
	, DATALENGTH(document) as DL
from Warehouse_ExamSessioNDocumentTable (NOLOCK)
) C
INNER JOIN
(

SELECT COUNT(N) as 'CountOfDifferentFiles', warehouseExamSessionID, itemId 
FROM #Temp
group by N,  warehouseExamSessionID , itemId 
having count(*) > 1
) D

ON C.warehouseexamsessionid = D.warehouseexamsessionid
	and C.ItemID = D.ItemID
	ORDER BY C.warehouseexamsessionid, C.ItemID

/*

SELECT ID
	,ItemID
	, warehouseexamsessionid
	, DATALENGTH(document) as DL
from Warehouse_ExamSessioNDocumentTable (NOLOCK)
where itemid = '3507P6469S11C1T1' and warehouseexamsessionid= 16687

*/