
create table #Temp
(
	metadata nvarchar(max)
)

insert into #Temp
values('ella_tillett@hotmail.comdrhammadkhan@gmail.comanil.garg@wash.nhs.uk')


select * from #Temp

begin tran
select * from #Temp

update #Temp
set metadata = REPLACE(metadata, '',CHAR(10))
where metadata like '%%'

select * from #Temp
rollback

	USE DataWarehouse

	update DimQuestionMetadata
	set attribval = REPLACE(attribval, '',CHAR(10))
	where attribval like '%%'

	USE ContentAuthor

	update SubjectTagValues
	set Text = REPLACE(text, '',CHAR(10))
	where SubjectTagValues.Text like '%%';

	update SubjectTagTypes
	set UpdateDate = '2015-03-05 12:20:00.001'
	where ID in (111,118,191,215,227);
