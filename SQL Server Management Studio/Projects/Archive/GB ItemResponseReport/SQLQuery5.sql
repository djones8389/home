select
	ItemName
	,  KeyCode
	,	c.i.value('.[1]', 'nvarchar(max)') as userData
	, structureXml
	, WAREHOUSE_ExamSessionTable_ShrededItems.*
from WAREHOUSE_ExamSessionTable_Shreded 


left join WAREHOUSE_ExamSessionItemResponseTable 
on WAREHOUSE_ExamSessionItemResponseTable.WAREHOUSEExamSessionID = WAREHOUSE_ExamSessionTable_Shreded.examSessionId

left join WAREHOUSE_ExamSessionTable_ShrededItems
on WAREHOUSE_ExamSessionTable_ShrededItems.examSessionId = WAREHOUSE_ExamSessionTable_Shreded.examSessionId

cross apply ItemResponseData.nodes('/p') ItemResponseData(p)
cross apply ItemResponseData.nodes('/p/s/c/i') c(i)

where WAREHOUSE_ExamSessionTable_Shreded.KeyCode in ('UANNKY01','5N86RD01','7SGKLF01','4WJDP901','GVN7UN01','EE5EV401','6XPWYS01','3N7L8H01','R96KLW01')
	and ItemName = 'Teens_W_Ver_3_Task_4_WIP'
	and WAREHOUSE_ExamSessionTable_ShrededItems.ItemRef = ItemResponseData.p.value('@id','nvarchar(max)')

;