begin tran	

select StructureXML, resultData from WAREHOUSE_ExamSessionTable where ID = 107839
	
UPDATE WAREHOUSE_ExamSessionTable 
SET StructureXML = REPLACE(cast (structureXML AS nvarchar(Max)), N'userAttempted="1"', N'userAttempted="0"')
where ID = 107839

UPDATE WAREHOUSE_ExamSessionTable 
SET StructureXML = REPLACE(cast (structureXML AS nvarchar(Max)), N'userMark="1"', N'userMark="0"')
where ID = 107839

delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = 107839

UPDATE WAREHOUSE_ExamSessionTable 
SET resultData = REPLACE(cast (resultData AS nvarchar(Max)), N'userMark="1"', N'userMark="0"')
where ID = 107839

delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = 107839

UPDATE WAREHOUSE_ExamSessionTable 
SET resultData = REPLACE(cast (resultData AS nvarchar(Max)), N'userPercentage="1"', N'userPercentage="0"')
where ID = 107839

delete from WAREHOUSE_ExamStateAuditTable where WarehouseExamSessionID = 107839

select StructureXML, resultData from WAREHOUSE_ExamSessionTable where ID = 107839

rollback