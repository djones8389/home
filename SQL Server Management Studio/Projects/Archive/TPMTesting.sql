select distinct	
	SPC.IsVoided
	, P.Name
	, FirstName
	, LastName
	, SurpassCandidateRef
	, PackageScore
	, SP.CenterName
	, SPC.DateCompleted
	--,*
from ScheduledPackageCandidates as SPC

	inner join ScheduledPackageCandidateExams as SPCE
	on SPCE.Candidate_ScheduledPackageCandidateId = SPC.ScheduledPackageCandidateId

	inner join PackageExams as PE
	on PE.PackageExamId = SPCE.PackageExamId
	
	inner join Packages as P
	on P.PackageId = PE.PackageId	

	inner join ScheduledPackages as SP
	on SP.ScheduledPackageId = SPC.ScheduledPackage_ScheduledPackageId

where p.Name = '9a_Reading and Writing GM'
	and SPC.IsVoided = 0
	order by SPC.DateCompleted desc