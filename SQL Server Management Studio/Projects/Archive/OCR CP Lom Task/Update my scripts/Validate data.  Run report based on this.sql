USE UAT_OCR_ContentProducer_IP

--Find pages that have images, and that are not in the recycle bin
with cte as (

	SELECT 
		 A.ProjectName
		,  A.ProjectID
		,  A.PageID
		,  A.Version
		--, A.Cast
	FROM 
	(
	select distinct
		  PLT.ID as [ProjectID]
		 , Name as [ProjectName]
		 , p.r.value('@ID','nvarchar(12)') as PageID
		 , PT.Ver as Version
		-- , cast(PLT.ID as nvarchar(10)) + 'P' + cast(p.r.value('@ID','nvarchar(12)') as nvarchar(10)) as 'CAST'
		from ProjectListTable as PLT with (NOLOCK)
	
	cross apply ProjectStructureXml.nodes('Pro//Pag') p(r)  
	
	INNER JOIN dbo.ItemGraphicTable IGT with (NOLOCK) on SUBSTRING(IGT.ID, CHARINDEX('P', IGT.ID) + 1, CHARINDEX('S',IGT.ID) - CHARINDEX('P', IGT.ID) - 2 + Len('S')) =  p.r.value('@ID','nvarchar(12)')
	INNER JOIN dbo.ComponentTable CT with (NOLOCK) ON IGT.ParentID = CT.ID
	INNER JOIN dbo.SceneTable ST with (NOLOCK) ON  ST.ID = CT.ParentID
	INNER JOIN dbo.PageTable PT with (NOLOCK) ON PT.ParentID = PLT.ID AND PT.ID = ST.ParentID AND PT.ID = cast(PLT.ID as nvarchar(10)) + 'P' + cast(p.r.value('@ID','nvarchar(12)') as nvarchar(10))
	) A

	LEFT JOIN (
		
	select distinct
		  PLT.ID as [ProjectID]
		 , Name as [ProjectName]
		 , p.r.value('@ID','nvarchar(12)') as PageID
		 , PT.Ver as Version
		 --, cast(PLT.ID as nvarchar(10)) + 'P' + cast(p.r.value('@ID','nvarchar(12)') as nvarchar(10)) as 'CAST'
		from ProjectListTable as PLT with (NOLOCK) 
	
	cross apply ProjectStructureXml.nodes('Pro/Rec//Pag') p(r) 
	
	INNER JOIN dbo.ItemGraphicTable IGT with (NOLOCK) on SUBSTRING(IGT.ID, CHARINDEX('P', IGT.ID) + 1, CHARINDEX('S',IGT.ID) - CHARINDEX('P', IGT.ID) - 2 + Len('S')) =  p.r.value('@ID','nvarchar(12)')
	INNER JOIN dbo.ComponentTable CT with (NOLOCK) ON IGT.ParentID = CT.ID
	INNER JOIN dbo.SceneTable ST with (NOLOCK) ON  ST.ID = CT.ParentID
	INNER JOIN dbo.PageTable PT with (NOLOCK) ON PT.ParentID = PLT.ID AND PT.ID = ST.ParentID AND PT.ID = cast(PLT.ID as nvarchar(10)) + 'P' + cast(p.r.value('@ID','nvarchar(12)') as nvarchar(10))

	) B


	ON A.ProjectID = B.ProjectID
		and A.PageID = B.PageID

	where B.PageID is null
)











/*
USE UAT_OCR_ContentProducer_IP
select top 5 ParentID from SceneTable
SELECT IGT.*
FROM dbo.ItemGraphicTable IGT
INNER JOIN dbo.ComponentTable CT ON IGT.ParentID = CT.ID
INNER JOIN dbo.SceneTable ST ON ST.ID = CT.ParentID
INNER JOIN dbo.PageTable PT ON PT.ID = ST.ParentID

Where  IGT.id like '%6014P%'


select top 5 * from ItemGraphicTable where id like '6014P%'


*/