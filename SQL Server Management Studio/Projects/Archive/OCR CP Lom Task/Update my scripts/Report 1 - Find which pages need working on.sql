USE UAT_OCR_ContentProducer_IP

--Find pages that have images, and that are not in the recycle bin
--with cte as (

	SELECT 
		 A.ProjectName
		,  A.ProjectID
		,  A.PageID
		,  A.Version
		--, A.Cast
	FROM 
	(
	select distinct
		  PLT.ID as [ProjectID]
		 , Name as [ProjectName]
		 , p.r.value('@ID','nvarchar(12)') as PageID
		 , PT.Ver as Version
		-- , cast(PLT.ID as nvarchar(10)) + 'P' + cast(p.r.value('@ID','nvarchar(12)') as nvarchar(10)) as 'CAST'
		from ProjectListTable as PLT with (NOLOCK)
	
	cross apply ProjectStructureXml.nodes('Pro//Pag') p(r)  
	
	INNER JOIN dbo.ItemGraphicTable IGT with (NOLOCK) on SUBSTRING(IGT.ID, CHARINDEX('P', IGT.ID) + 1, CHARINDEX('S',IGT.ID) - CHARINDEX('P', IGT.ID) - 2 + Len('S')) =  p.r.value('@ID','nvarchar(12)')
	INNER JOIN dbo.ComponentTable CT with (NOLOCK) ON IGT.ParentID = CT.ID
	INNER JOIN dbo.SceneTable ST with (NOLOCK) ON  ST.ID = CT.ParentID
	INNER JOIN dbo.PageTable PT with (NOLOCK) ON PT.ParentID = PLT.ID AND PT.ID = ST.ParentID AND PT.ID = cast(PLT.ID as nvarchar(10)) + 'P' + cast(p.r.value('@ID','nvarchar(12)') as nvarchar(10))

	) A

	LEFT JOIN (
		
	select distinct
		  PLT.ID as [ProjectID]
		 , Name as [ProjectName]
		 , p.r.value('@ID','nvarchar(12)') as PageID
		 , PT.Ver as Version
		 --, cast(PLT.ID as nvarchar(10)) + 'P' + cast(p.r.value('@ID','nvarchar(12)') as nvarchar(10)) as 'CAST'
		from ProjectListTable as PLT with (NOLOCK) 
	
	cross apply ProjectStructureXml.nodes('Pro/Rec//Pag') p(r) 
	
	INNER JOIN dbo.ItemGraphicTable IGT with (NOLOCK) on SUBSTRING(IGT.ID, CHARINDEX('P', IGT.ID) + 1, CHARINDEX('S',IGT.ID) - CHARINDEX('P', IGT.ID) - 2 + Len('S')) =  p.r.value('@ID','nvarchar(12)')
	INNER JOIN dbo.ComponentTable CT with (NOLOCK) ON IGT.ParentID = CT.ID
	INNER JOIN dbo.SceneTable ST with (NOLOCK) ON  ST.ID = CT.ParentID
	INNER JOIN dbo.PageTable PT with (NOLOCK) ON PT.ParentID = PLT.ID AND PT.ID = ST.ParentID AND PT.ID = cast(PLT.ID as nvarchar(10)) + 'P' + cast(p.r.value('@ID','nvarchar(12)') as nvarchar(10))

	) B


	ON A.ProjectID = B.ProjectID
		and A.PageID = B.PageID
		
	where B.PageID is null --and A.ProjectID = 6014





USE UAT_OCR_CONTENTPRODUCER_IP

select 
	--cast(A.ProjectID as nvarchar(10)) + 'P' + cast(A.ItemRef as nvarchar(10)) as ProjectAndPage
    A.ProjectID
   , A.ItemRef
   , A.Version as CPVersion
   , B.Version as IBVersion
From 
(
SELECT
	PLT.ID as ProjectID
	, a.b.value('@ID','nvarchar(20)') as ItemRef
	, PT.Ver as Version
from ProjectListTable as PLT with (NOLOCK)

cross apply ProjectStructureXML.nodes('Pro//Pag') a(b)

inner join PageTable as PT with (NOLOCK)
on PT.ParentID = PLT.ID and PT.ID = cast(PLT.ID as nvarchar(10)) + 'P' + cast(a.b.value('@ID','nvarchar(20)') as nvarchar(10))

where 
	--Check Status of the page is within the publishable range
	 a.b.value('@sta','int') Between PublishSettingsXML.value('(/PublishConfig/StatusLevelFrom)[1]','int') and PublishSettingsXML.value('(/PublishConfig/StatusLevelTo)[1]','int')
) A

INNER JOIN (

	select  ProjectID, ItemRef, max(Version) as Version 
		from UAT_OCR_ItemBank_IP..ItemTable  with (NOLOCK)
			group by ProjectID, ItemRef 
) B

ON A.ProjectID = B.ProjectID
	and A.ItemRef = B.ItemRef
	and A.Version <> B.Version