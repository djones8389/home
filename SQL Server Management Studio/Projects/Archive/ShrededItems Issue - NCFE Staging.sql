USE NCFE_SecureAssess_Live

SELECT  --count(E.ID )
	 distinct ID, e.KeyCode
	 --, e.KeyCode, e.warehouseTime
  --,I.ItemRef AS [itemID]  
  --,I.itemVersion  
  --,CASE LEN(I.MarkerUserMark)  
  -- WHEN 0 THEN I.UserMark * I.TotalMark  
  -- ELSE I.MarkerUserMark  
  -- END AS [userMark]  
  --,I.userAttempted AS [userAttempt]  
  --,I.totalMark  
  --, e.reMarkStatus
  --,s.userMark
 FROM  WAREHOUSE_ExamSessionTable E  
 INNER JOIN WAREHOUSE_ExamSessionTable_ShrededItems I  
 ON E.ID = I.ExamSessionID  
 Inner Join WAREHOUSE_ExamSessionTable_Shreded S
 On S.examSessionId = E.ID 
  
 where --e.KeyCode in ('WSF884A5','U3L4N5A5','BRCLTAA5','ZA96SFA5')
 
	I.userAttempted is null
	and I.totalMark  = '0.000'
	--and e.reMarkStatus = 2
	and e.warehouseTime > '01 Jun 2014'
	and e.PreviousExamState <> 10

SELECT TOP 100 ItemResponseData.value('(p/@ua)[1]', 'FLOAT') AS ua, WAREHOUSE_ExamSessionTable_ShrededItems.userAttempted, *
FROM WAREHOUSE_ExamSessionItemResponseTable
INNER JOIN WAREHOUSE_ExamSessionTable_ShrededItems
ON WAREHOUSE_ExamSessionItemResponseTable.ItemID = WAREHOUSE_ExamSessionTable_ShrededItems.ItemRef
AND WAREHOUSE_ExamSessionItemResponseTable.WAREHOUSEExamSessionID = WAREHOUSE_ExamSessionTable_ShrededItems.examSessionId
WHERE WAREHOUSEExamSessionID IN (151586,
151887,
152934)

SELECT *
FROM WAREHOUSE_ExamSessionTable
WHERE KeyCode IN (
SELECT WAREHOUSE_ExamSessionTable.KeyCode
FROM WAREHOUSE_ExamSessionTable_ShrededItems
INNER JOIN WAREHOUSE_ExamSessionTable
ON WAREHOUSE_ExamSessionTable.ID = WAREHOUSE_ExamSessionTable_ShrededItems.examSessionId
WHERE userAttempted IS NULL
GROUP BY KeyCode)