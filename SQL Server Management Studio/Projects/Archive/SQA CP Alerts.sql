USE UAT_SQA_ContentProducer
 
SELECT  AlertDefinitionTable.[ID]
      ,AlertDefinitionTable.[Name]
      ,[Type]
      ,[Frequency]
      ,[ProjectId]
      ,[AlertTimeInSeconds]
      ,[ItemTypeID]
      ,[ItemStates]
      ,[ItemReferences]
      ,[ItemFlags]
      ,[ItemAuthor]
      ,ProjectDefaultXml
  FROM UAT_SQA_ContentProducer.[dbo].[AlertDefinitionTable]
  
  inner join ProjectListTable 
  on ProjectListTable.ID = ProjectId
  
where ProjectId = 1141; --AND AlertDefinitionTable.Name in ('GraphicProduced', 'GraphicRequired')
	 
	 
	 
	
--select ProjectDefaultXml 
--	from ProjectListTable
--	 where id = 1141; 