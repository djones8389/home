select 
	Forename
	,Surname
	,CandidateRef
	,DOB
	,Gender
	,Email
	,case IsCandidate when 0 then 'User' else 'Candidate' end as 'Candidate/User'
	,AccountExpiryDate

 from UserTable
 
 where Retired = 0
 order by AccountExpiryDate ASC