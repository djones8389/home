restore database [docsopen] with recovery;
restore database [docsopen] with recovery;
restore database [docsopen] with recovery;

select 'restore database '+ quotename(name)+' with recovery;'
from sys.databases
where state_desc = 'RESTORING'

restore database [docsopen] with recovery;
restore database [eDOCS_WRK] with recovery;
restore database [HIS_SQL_Admin] with recovery;
restore database [INTEGRATION_ADMIN] with recovery;
restore database [MIDB] with recovery;
restore database [STDDB] with recovery;
restore database [WMDI] with recovery;