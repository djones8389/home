select @@servername

--pr030361a520-00
--pr030361a520-01
--pr0303c844ed-01

--alter availability group [availabilitygroup] remove database IDIT 

declare  @dynamic nvarchar(max) = '';

select @dynamic += CHAR(13) +
        'kill ' + cast(spid as char(10))
       from sys.sysprocesses
where db_name(dbid) = 'IDIT'

exec(@dynamic);


alter database IDIT set offline WITH ROLLBACK IMMEDIATE