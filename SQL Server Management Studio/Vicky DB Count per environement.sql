SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

use SQL_Inventory

; with Instances as ( 
SELECT 
	h.hostName AS SQLNetworkName
	, s.instanceName
	,db.databaseName
	,s.STATUS 
FROM dbo.Databases db 
INNER JOIN dbo.Servers s
ON db.serverID = s.serverID 
INNER JOIN dbo.Hosts h
ON h.hostID = s.hostID
WHERE s.serverState <> 'Decommissioned'
	and db.databaseName not in ('msdb','model','master','tempdb', 'cdr_local','distribution')

UNION

SELECT 
	c.SQLClusterName AS SQLNetworkName
	, s.instanceName
	,db.databaseName
	,s.STATUS 
FROM dbo.Databases db 
INNER JOIN dbo.Servers s
ON db.serverID = s.serverID 
INNER JOIN dbo.Clusters c
ON c.clusterID = s.clusterID 
WHERE s.serverState <> 'Decommissioned'
	and db.databaseName not in ('msdb','model','master','tempdb', 'cdr_local','distribution')
)

select a.Instance
	, Environment
	, count(databasename)	[Count]
from (
select  
	case when instanceName = 'MSSQLSERVER' then I.SQLNetworkName
		else I.SQLNetworkName + '\' + i.instancename
		end as [Instance]
	, i.databaseName
	, e.Environment
from Instances I
inner join Environments E
on E.ShortCode = I.status
) a
group by a.Instance
	, Environment
order by Environment