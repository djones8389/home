USE [ODS_RAQ]
GO

/****** Object:  StoredProcedure [dbo].[Refresh_tblUsers]    Script Date: 13/12/2018 15:45:45 ******/
DROP PROCEDURE [dbo].[Refresh_tblUsers]
GO

/****** Object:  StoredProcedure [dbo].[Refresh_tblUsers]    Script Date: 13/12/2018 15:45:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


	
		create procedure [dbo].[Refresh_tblUsers] (
			@Date date,
			@updatecount int output,
			@InsertCount int output
		) AS
	
		DECLARE @temp table(
			[UserName] varchar (50) COLLATE SQL_Latin1_General_CP1_CI_AS)
			
		insert into @temp (
			[UserName]
		)
		select A.[UserName] 
			from [RAQ].[dbo].[tblUsers] A
			where not exists (
				select *
				from [ODS_RAQ].[dbo].[tblUsers] B 
				where ods_DateTo = '31 dec 2999' and A.[UserName] = B.[UserName]
				and (((A.[UserId] IS NULL AND B.[UserId] IS NULL) OR (A.[UserId] = B.[UserId])) and ((A.[UserName] IS NULL AND B.[UserName] IS NULL) OR (A.[UserName] = B.[UserName])) and ((A.[Password] IS NULL AND B.[Password] IS NULL) OR (A.[Password] = B.[Password])) and ((A.[EmailAddress] IS NULL AND B.[EmailAddress] IS NULL) OR (A.[EmailAddress] = B.[EmailAddress])) and ((A.[Rowguid] IS NULL AND B.[Rowguid] IS NULL) OR (A.[Rowguid] = B.[Rowguid])) and ((A.[AddNewPolicies] IS NULL AND B.[AddNewPolicies] IS NULL) OR (A.[AddNewPolicies] = B.[AddNewPolicies])) and ((A.[ViewExistingPolicies] IS NULL AND B.[ViewExistingPolicies] IS NULL) OR (A.[ViewExistingPolicies] = B.[ViewExistingPolicies])) and ((A.[ViewStatistics] IS NULL AND B.[ViewStatistics] IS NULL) OR (A.[ViewStatistics] = B.[ViewStatistics])) and ((A.[AgentID] IS NULL AND B.[AgentID] IS NULL) OR (A.[AgentID] = B.[AgentID])) and ((A.[ViewManagementInfo] IS NULL AND B.[ViewManagementInfo] IS NULL) OR (A.[ViewManagementInfo] = B.[ViewManagementInfo])) and ((A.[FullName] IS NULL AND B.[FullName] IS NULL) OR (A.[FullName] = B.[FullName])) and ((A.[DepartmentID] IS NULL AND B.[DepartmentID] IS NULL) OR (A.[DepartmentID] = B.[DepartmentID])) and ((A.[JobTitle] IS NULL AND B.[JobTitle] IS NULL) OR (A.[JobTitle] = B.[JobTitle])) and ((A.[PhoneNumber] IS NULL AND B.[PhoneNumber] IS NULL) OR (A.[PhoneNumber] = B.[PhoneNumber])) and ((A.[Blocked] IS NULL AND B.[Blocked] IS NULL) OR (A.[Blocked] = B.[Blocked])) and ((A.[UserGroup] IS NULL AND B.[UserGroup] IS NULL) OR (A.[UserGroup] = B.[UserGroup])) and ((A.[PasswordLastChanged] IS NULL AND B.[PasswordLastChanged] IS NULL) OR (A.[PasswordLastChanged] = B.[PasswordLastChanged])) and ((A.[LanguageID] IS NULL AND B.[LanguageID] IS NULL) OR (A.[LanguageID] = B.[LanguageID])) and ((A.[AuthorityLevel] IS NULL AND B.[AuthorityLevel] IS NULL) OR (A.[AuthorityLevel] = B.[AuthorityLevel])) and ((A.[DocFolder] IS NULL AND B.[DocFolder] IS NULL) OR (A.[DocFolder] = B.[DocFolder])) and ((A.[DocFolderLocation] IS NULL AND B.[DocFolderLocation] IS NULL) OR (A.[DocFolderLocation] = B.[DocFolderLocation])))
			)
			
		insert into @temp (
			[UserName]
		)
		select A.[UserName] 
			from [ODS_RAQ].[dbo].[tblUsers] A
			where ods_DateTo = '31 dec 2999'
			and not exists (
				select *
				from [RAQ].[dbo].[tblUsers] B 
				where A.[UserName] = B.[UserName]
			)
			
		if not exists (
			select *
			from @temp
		) begin
			set @updatecount = 0
			set @InsertCount = 0
			return
		end	
		
		if exists (
			select *
			from [ODS_RAQ].[dbo].[tblUsers]
			where ods_DateFrom > @Date) begin
						
				RAISERROR ('Future data exists in tblUsers',11,1)
				return
		end
		
		update A
		set A.ods_DateTo = @Date
		from [ODS_RAQ].[dbo].[tblUsers] A
		join @temp B on A.[UserName] = B.[UserName]
		where A.ods_DateTo = '31 dec 2999'
		
		set @updatecount = @@ROWCOUNT
		
		insert into [ODS_RAQ].[dbo].[tblUsers] (
			ods_DateFrom, ods_DateTo, [UserId],[UserName],[Password],[EmailAddress],[Rowguid],[AddNewPolicies],[ViewExistingPolicies],[ViewStatistics],[AgentID],[ViewManagementInfo],[FullName],[DepartmentID],[JobTitle],[PhoneNumber],[Blocked],[UserGroup],[PasswordLastChanged],[LanguageID],[AuthorityLevel],[DocFolder],[DocFolderLocation]
		)
		select @Date , '31 dec 2999', A.[UserId],A.[UserName],A.[Password],A.[EmailAddress],A.[Rowguid],A.[AddNewPolicies],A.[ViewExistingPolicies],A.[ViewStatistics],A.[AgentID],A.[ViewManagementInfo],A.[FullName],A.[DepartmentID],A.[JobTitle],A.[PhoneNumber],A.[Blocked],A.[UserGroup],A.[PasswordLastChanged],A.[LanguageID],A.[AuthorityLevel],A.[DocFolder],A.[DocFolderLocation]
		from [RAQ].[dbo].[tblUsers] A	
		join @temp B on A.[UserName] = B.[UserName]
		
		set @InsertCount = @@ROWCOUNT

	
GO


