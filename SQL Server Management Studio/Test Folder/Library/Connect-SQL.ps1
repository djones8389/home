Function Connect-SQL {

[Cmdletbinding()]
    Param(
       # [Parameter(Mandatory=$true, Position=1)] [String]$dataSource
        [Parameter(Mandatory=$true, Position=1)] [String]$instance
        
        ,[Parameter(Mandatory=$false, Position=5)] [boolean]$windowsAuthentication
    )



    Begin {    
        [Reflection.Assembly]::LoadWithPartialName("Microsoft.SqlServer.Smo") | Out-Null;
    }


    Process {

        $srv = New-Object Microsoft.SqlServer.Management.Smo.Server $instance;
        
        if($windowsAuthentication) {
            $srv.ConnectionContext.LoginSecure = $True;
        }
        else {
             $srv.ConnectionContext.LoginSecure = $False;
             $srv.ConnectionContext.Login = $login;
             $srv.ConnectionContext.SecurePassword = $password;
        }
        
        $srv.ConnectionContext.StatementTimeout = 0;
        
        Try {
            $srv.ConnectionContext.Connect();    
            if($name) {Write-Host "Connected to: $name";}
            else {Write-Host "Connected to: $instance";}
        
        }
        Catch {
                Throw New-Object System.Exception("Failed to connect to: {0}" -f $srv.DomainInstanceName);
        }
            
           return $srv;
     }      

}
