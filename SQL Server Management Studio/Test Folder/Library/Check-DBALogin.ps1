Function Check-DBALogin {

[Cmdletbinding()]
    Param(
        [Parameter(Mandatory=$true, Position=1)] $connect
		,[Parameter(Mandatory=$true, Position=2)] $DBALogin
	)
	
    Begin {    
		[Reflection.Assembly]::LoadWithPartialName("Microsoft.SqlServer.Smo") | Out-Null;
		$InstanceName = ($connect.ComputerNamePhysicalNetBIOS + '\' + $connect.InstanceName);
    }


    Process {
	
		if(!$connect.Logins[$DBALogin]){
			$message = "SQL login: $DBALogin does not exist on $InstanceName";                 
		}	
		else {
			$message = "SQL login: $DBALogin does exist on $InstanceName";  
		}
		return $message;
	}
	
	

}
