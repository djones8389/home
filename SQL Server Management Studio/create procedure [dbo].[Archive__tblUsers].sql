USE [ODS_RAQ]
GO

/****** Object:  StoredProcedure [dbo].[Archive__tblUsers]    Script Date: 13/12/2018 17:13:12 ******/
DROP PROCEDURE [dbo].[Archive__tblUsers]
GO

/****** Object:  StoredProcedure [dbo].[Archive__tblUsers]    Script Date: 13/12/2018 17:13:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


	
		create procedure [dbo].[Archive__tblUsers] (
			@Date date,
			@updatecount int output,
			@NoOfDailyMonths int,
			@NoOfMonthlyQuarters int,
			@NoOfQuarterlyYears int
		) AS
	
		set @updatecount = 0
		
		insert into [ODS_RAQ].[dbo].[Archived__tblUsers] (
			ods_DateFrom, ods_DateTo, 
			[UserId],[UserName],[EmailAddress],[Rowguid],[AddNewPolicies],[ViewExistingPolicies],[ViewStatistics],[AgentID],[ViewManagementInfo],[FullName],[DepartmentID],[JobTitle],[PhoneNumber],[Blocked],[UserGroup],[PasswordLastChanged],[LanguageID],[AuthorityLevel],[DocFolder],[DocFolderLocation]
		)
		select ods_DateFrom, ods_DateTo, [UserId],[UserName],[EmailAddress],[Rowguid],[AddNewPolicies],[ViewExistingPolicies],[ViewStatistics],[AgentID],[ViewManagementInfo],[FullName],[DepartmentID],[JobTitle],[PhoneNumber],[Blocked],[UserGroup],[PasswordLastChanged],[LanguageID],[AuthorityLevel],[DocFolder],[DocFolderLocation]
		from [ODS_RAQ].[dbo].[tblUsers]
		where datediff(year, ods_DateFrom, ods_DateTo) = 0
			and datediff(year, ods_DateFrom, @Date) > @NoOfQuarterlyYears
			and ods_DateTo <> '31 Dec 2999'

		delete 
		from [ODS_RAQ].[dbo].[tblUsers]
		where datediff(year, ods_DateFrom, ods_DateTo) = 0
			and datediff(year, ods_DateFrom, @Date) > @NoOfQuarterlyYears
			and ods_DateTo <> '31 Dec 2999'
		
		set @updatecount = @updatecount + @@ROWCOUNT
		
		insert into [ODS_RAQ].[dbo].[Archived__tblUsers] (
			ods_DateFrom, ods_DateTo, 
			[UserId],[UserName],[EmailAddress],[Rowguid],[AddNewPolicies],[ViewExistingPolicies],[ViewStatistics],[AgentID],[ViewManagementInfo],[FullName],[DepartmentID],[JobTitle],[PhoneNumber],[Blocked],[UserGroup],[PasswordLastChanged],[LanguageID],[AuthorityLevel],[DocFolder],[DocFolderLocation]
		)
		select ods_DateFrom, ods_DateTo, [UserId],[UserName],[EmailAddress],[Rowguid],[AddNewPolicies],[ViewExistingPolicies],[ViewStatistics],[AgentID],[ViewManagementInfo],[FullName],[DepartmentID],[JobTitle],[PhoneNumber],[Blocked],[UserGroup],[PasswordLastChanged],[LanguageID],[AuthorityLevel],[DocFolder],[DocFolderLocation]
		from [ODS_RAQ].[dbo].[tblUsers]
		where datediff(quarter, ods_DateFrom, ods_DateTo) = 0
			and datediff(quarter, ods_DateFrom, @Date) > @NoOfMonthlyQuarters
			and ods_DateTo <> '31 Dec 2999'
	
		delete 
		from [ODS_RAQ].[dbo].[tblUsers]
		where datediff(quarter, ods_DateFrom, ods_DateTo) = 0
			and datediff(quarter, ods_DateFrom, @Date) > @NoOfMonthlyQuarters
			and ods_DateTo <> '31 Dec 2999'
		
		set @updatecount = @updatecount + @@ROWCOUNT
		
		insert into [ODS_RAQ].[dbo].[Archived__tblUsers] (
			ods_DateFrom, ods_DateTo, 
			[UserId],[UserName],[EmailAddress],[Rowguid],[AddNewPolicies],[ViewExistingPolicies],[ViewStatistics],[AgentID],[ViewManagementInfo],[FullName],[DepartmentID],[JobTitle],[PhoneNumber],[Blocked],[UserGroup],[PasswordLastChanged],[LanguageID],[AuthorityLevel],[DocFolder],[DocFolderLocation]
		)
		select ods_DateFrom, ods_DateTo, [UserId],[UserName],[EmailAddress],[Rowguid],[AddNewPolicies],[ViewExistingPolicies],[ViewStatistics],[AgentID],[ViewManagementInfo],[FullName],[DepartmentID],[JobTitle],[PhoneNumber],[Blocked],[UserGroup],[PasswordLastChanged],[LanguageID],[AuthorityLevel],[DocFolder],[DocFolderLocation]
		from [ODS_RAQ].[dbo].[tblUsers]
		where datediff(month, ods_DateFrom, ods_DateTo) = 0
			and datediff(month, ods_DateFrom, @Date) > @NoOfDailyMonths
			and ods_DateTo <> '31 Dec 2999'
				
		delete
		from [ODS_RAQ].[dbo].[tblUsers]
		where datediff(month, ods_DateFrom, ods_DateTo) = 0
			and datediff(month, ods_DateFrom, @Date) > @NoOfDailyMonths
			and ods_DateTo <> '31 Dec 2999'
		
		set @updatecount = @updatecount + @@ROWCOUNT
		
	
GO


