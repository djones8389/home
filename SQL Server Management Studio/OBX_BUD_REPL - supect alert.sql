if exists (
select 1
from sys.databases
where name = 'OBX_BUD_REPL'
	and state_desc != 'ONLINE'
)
BEGIN

--Alert DBA's

	EXECUTE [msdb].[dbo].[sp_send_dbmail]
		@profile_name = 'DefaultMailProfile'
		,@recipients  = 'dave.jones@hiscox.com; paul.holden@hiscox.com'
		,@body        = 'OBX_BUD_REPL is in suspect mode.  T-SQL fix deployed'
		,@subject     = 'OBX_BUD_REPL - Suspect Mode'

--Fix the issue

	ALTER AVAILABILITY GROUP [AGHXE76S-BUD] REMOVE DATABASE OBX_BUD_REPL;

	ALTER DATABASE OBX_BUD_REPL SET EMERGENCY;

	ALTER DATABASE OBX_BUD_REPL SET SINGLE_USER WITH ROLLBACK IMMEDIATE;

	DBCC checkdb(OBX_BUD_REPL,'REPAIR_ALLOW_DATA_LOSS');

	ALTER DATABASE OBX_BUD_REPL SET MULTI_USER;

	ALTER AVAILABILITY GROUP [AGHXE76S-BUD] ADD DATABASE OBX_BUD_REPL;

END