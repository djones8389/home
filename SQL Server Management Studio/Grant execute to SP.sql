use Aggregate_Hiscox

GRANT EXECUTE ON OBJECT::dbo.User_SetUp TO [HISCOX\SQL_Sequel_RW];
GRANT EXECUTE ON OBJECT::dbo.User_delete TO [HISCOX\SQL_Sequel_RW];

select 'GRANT EXECUTE ON OBJECT::'+quotename(SCHEMA_NAME(schema_id))+'.'+quotename(name)+' TO [HISCOX\SQL_Sequel_RW];'
from sys.procedures
where type = 'P'
order by name asc;