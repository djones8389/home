use SQL_Inventory

SELECT s.serverID  
	, a.hostname
	, a.instanceName,d.databaseName
	, d.application
FROM [SQL_Inventory].[dbo].[vw_CommissionedSQLInstances] a
inner join Servers s
on s.serverID = a.serverID
inner join [databases] d
on d.serverID = s.serverID
where   d.databaseName != 'model'

--exec [SQL_Inventory].[dbo].usp_CDRPortal_InstanceDetails @serverID=1298
--exec [SQL_Inventory].[dbo].usp_CDRPortal_InstanceDetails @serverID=1355