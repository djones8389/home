--Drop triggers if they exist

	DECLARE @DROPTRIGGER NVARCHAR(MAX)='';

	SELECT @DROPTRIGGER +=CHAR(13) +'DROP TRIGGER ' + isnull( s.name, '' ) + '.' + t.name
	FROM  sys.triggers t
	LEFT JOIN sys.all_objects o
	ON t.parent_id = o.object_id
	LEFT JOIN sys.schemas s
	ON s.schema_id = o.schema_id
	where t.name LIKE 'tr_audit_%'

	PRINT(@DROPTRIGGER);

----Create AuditTable

--	IF NOT EXISTS (SELECT * FROM sysobjects WHERE id = OBJECT_ID(N'[dbo].[Audit]'))
--	BEGIN
--		PRINT 'Adding Audit table in the database'
--		CREATE TABLE Audit
--		   (Type CHAR(1), 
--		   TableName VARCHAR(128), 
--		   PK VARCHAR(1000), 
--		   FieldName VARCHAR(128), 
--		   OldValue VARCHAR(MAX), 
--		   NewValue VARCHAR(MAX), 
--		   UpdateDate datetime,
--		   UserId NVARCHAR(50))
--	END
--	GO

DECLARE @AddTriggers NVARCHAR(max)='';

SELECT @AddTriggers+=CHAR(13) +  
	'CREATE TRIGGER tr_audit_'+TABLE_NAME+'
	ON '+QUOTENAME(TABLE_SCHEMA)+'.'+QUOTENAME(TABLE_NAME)+' FOR INSERT, UPDATE, DELETE
	AS
	DECLARE @field INT,
		   @maxfield INT,
		   @char INT,
		   @mask INT,
		   @fieldname VARCHAR(128),
		   @TableName VARCHAR(128),
		   @PKCols VARCHAR(1000),
		   @sql VARCHAR(8000), 
		   @UpdateDate VARCHAR(21),
		   @UserName VARCHAR(128),
		   @Type CHAR(1),
		   @PKSelect VARCHAR(1000),
		   @Schema VARCHAR(20)

	SET NOCOUNT ON

	--You will need to change @TableName to match the table to be audited
	SELECT @TableName = ''$$TableName$$''
	SELECT @Schema = ''$$Schema$$''

	-- date and user
	SELECT @UserName = SYSTEM_USER,
		   @UpdateDate = CONVERT(VARCHAR(8), GETDATE(), 112) 
				   + '' '' + CONVERT(VARCHAR(12), GETDATE(), 114)

	-- Action
	IF EXISTS (SELECT * FROM inserted)
		   IF EXISTS (SELECT * FROM deleted)
				   SELECT @Type = ''U''
		   ELSE
				   SELECT @Type = ''I''
	ELSE
		   SELECT @Type = ''D''

	-- get list of columns
	SELECT * INTO #ins FROM inserted
	SELECT * INTO #del FROM deleted

	-- Get primary key columns for full outer join
	SELECT @PKCols = COALESCE(@PKCols + '' and'', '' on'') 
				   + '' i.'' + c.COLUMN_NAME + '' = d.'' + c.COLUMN_NAME
		   FROM    INFORMATION_SCHEMA.TABLE_CONSTRAINTS pk ,

				  INFORMATION_SCHEMA.KEY_COLUMN_USAGE c
		   WHERE   pk.TABLE_NAME = @TableName
		   AND     CONSTRAINT_TYPE = ''PRIMARY KEY''
		   AND     c.TABLE_NAME = pk.TABLE_NAME
		   AND     c.CONSTRAINT_NAME = pk.CONSTRAINT_NAME

	-- Get primary key select for insert
	SELECT @PKSelect = COALESCE(@PKSelect+''+'','''') 
		   + ''''''<'' + COLUMN_NAME 
		   + ''=''''+convert(varchar(100),
	coalesce(i.'' + COLUMN_NAME +'',d.'' + COLUMN_NAME + ''))+''''>'''''' 
		   FROM    INFORMATION_SCHEMA.TABLE_CONSTRAINTS pk ,
				   INFORMATION_SCHEMA.KEY_COLUMN_USAGE c
		   WHERE   pk.TABLE_NAME = @TableName
		   AND     CONSTRAINT_TYPE = ''PRIMARY KEY''
		   AND     c.TABLE_NAME = pk.TABLE_NAME
		   AND     c.CONSTRAINT_NAME = pk.CONSTRAINT_NAME

	IF @PKCols IS NULL
	BEGIN
		   RAISERROR(''no PK on table %s'', 16, -1, @TableName)
		   RETURN
	END

	SELECT @field = 0, 
		   @maxfield = MAX(ORDINAL_POSITION) 
		FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = @TableName
	WHILE @field < @maxfield
	BEGIN
		SELECT @field = MIN(ORDINAL_POSITION) 
			   FROM INFORMATION_SCHEMA.COLUMNS 
			   WHERE TABLE_NAME = @TableName 
			   AND ORDINAL_POSITION > @field
        IF @field IS NOT NULL
        BEGIN
		    SELECT
				@field = MIN(ORDINAL_POSITION),
				@char = (column_id - 1) / 8 + 1,
				@mask = POWER(2, (column_id - 1) % 8),
				@fieldname = name
			FROM SYS.COLUMNS SC
			INNER JOIN INFORMATION_SCHEMA.COLUMNS ISC
			ON SC.name = ISC.COLUMN_NAME
			WHERE object_id = OBJECT_ID(@Schema +''.'' + @TableName)
			AND TABLE_NAME = @TableName
			AND ORDINAL_POSITION = @field
			GROUP BY column_id, name
		   
		   IF ((SUBSTRING(COLUMNS_UPDATED(), @char, 1) & @mask) > 0
										   OR @Type IN (''I'',''D'')) 
				AND (@fieldname not in (''DateCreated'',''UpdatedBy'',''DateModified'',''DateClosed'',''DateOpened''))
		   BEGIN
			   SELECT @sql = ''
					INSERT Audit ( Type, 
								   TableName, 
								   PK, 
								   FieldName, 
								   OldValue, 
								   NewValue, 
								   UpdateDate,
								   UserId)
					SELECT '''''' + @Type + '''''','''''' 
						   + @TableName + '''''','' + @PKSelect
						   + '','''''' + @fieldname + ''''''''
						   + '',convert(varchar(MAX),d.'' + @fieldname + '')''
						   + '',convert(varchar(MAX),i.'' + @fieldname + '')''
						   + '','''''' + @UpdateDate + ''''''''
						   + '',convert(varchar(MAX),i.UpdatedBy)''
						   + '' from #ins i full outer join #del d''
						   + @PKCols
						   + '' where convert(varchar(MAX),i.'' + @fieldname + '') <> convert(varchar(MAX),d.'' + @fieldname + '')''
						   + '' or (i.'' + @fieldname + '' is null and  d.''
													+ @fieldname
													+ '' is not null)'' 
						   + '' or (i.'' + @fieldname + '' is not null and  d.'' 
													+ @fieldname
													+ '' is null)'' 
			   EXEC (@sql)
			END
		END
	END'
FROM information_schema.columns   
WHERE OBJECTPROPERTY(OBJECT_ID(TABLE_CATALOG + '.' + TABLE_SCHEMA + '.' + TABLE_NAME), 'IsView') = 0
	and TABLE_NAME in ('Claim','Payment','Reserve','ConductComplaint','ConductDeclinature','ConductSanction','Lookup','Narrative','PaymentDescription','Strategy','SanctionType');

PRINT(@AddTriggers);
