SELECT hostName +
		case instanceName
		when 'MSSQLSERVER'
		then ''
		else '\' + instanceName
		end as [connection]
             ,*
  FROM [SQL_Inventory].[dbo].[vw_CommissionedSQLInstances]