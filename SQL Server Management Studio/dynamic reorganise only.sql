exec sp_MSforeachdb '

use [?];

IF ''?'' in (select name FROM SYS.databases where database_id > 4 and is_read_only = 0)

DECLARE @dynamic nvarchar(MAX) = '''';

SELECT @dynamic += CHAR(13) +
	''ALTER INDEX '' + quotename(C.name) + '' ON '' + quotename(SCHEMA_NAME(b.schema_id)) + ''.'' + quotename(B.name) + '' REORGANIZE;''
FROM sys.dm_db_index_physical_stats(DB_ID(),NULL,NULL,NULL,NULL) A
INNER JOIN sys.objects B
ON A.object_id = B.object_id
INNER JOIN sys.indexes C
ON B.object_id = C.object_id AND A.index_id = C.index_id
INNER JOIN sys.partitions D
ON B.object_id = D.object_id AND A.index_id = D.index_id
WHERE C.index_id > 0
	and A.avg_fragmentation_in_percent > 30

PRINT(@dynamic);'

--exec sp_MSforeachdb '

--use [?];

--IF ''?'' in (select name FROM SYS.databases where database_id > 4 and is_read_only = 0)

--begin
--exec sp_updatestats;
--end

--'

