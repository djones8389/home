USE [master];

	declare  @dynamic nvarchar(max) = '';

	select @dynamic += CHAR(13) + 
		 'kill ' + cast(spid as char(10))
		from sys.sysprocesses
	where db_name(dbid) = 'integration'

	exec(@dynamic);

use [integration];

	ALTER INDEX [idx_Session_8] ON [integration].[dbo].[Session] REBUILD;
	ALTER INDEX [PK__Session__3213E83F71382573] ON [integration].[dbo].[Session] REBUILD; 

	UPDATE STATISTICS [dbo].[Session];