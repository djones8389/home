backup database CDR_Local to disk = 'C:\Replication\CDR.bak'
	with compression, stats = 1;

backup log CDR_Local to disk = 'C:\Replication\CDR1.trn'
	with compression, stats = 1;

backup log CDR_Local to disk = 'C:\Replication\CDR2.trn'
	with compression, stats = 1;

backup database CDR_Local to disk = 'C:\Replication\CDR_diff.bak'
	with differential, compression, stats = 1;

