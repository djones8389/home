select backup_start_date
	, database_name
	, type
	 , user_name
	 , name	
	 , DATEDIFF(minute, backup_start_date,backup_finish_date) [Duration (min)]
	 , @@SERVERNAME
	 , is_copy_only
from msdb.dbo.backupset  a
where type not  in ('l','i', 'l')
	--and database_name in ('Zurich_US_Q2_EQ_0119_35793_RDM','Farmers_AllLines_FF_0120_36926_RDM')
	--and backup_start_date > cast(getdate() as date)
order by  backup_start_date desc;
 