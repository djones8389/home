USE [CDR_Local]
GO

/****** Object:  StoredProcedure [dbo].[usp_CDRUploadBackupStatus]    Script Date: 06/12/2018 09:06:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[usp_CDRUploadBackupStatus]

AS

SET NOCOUNT ON

BEGIN TRY

DELETE FROM [DBA_CDR].[SQL_Inventory].[dbo].[SQLBackupStatus]
WHERE ServerInstance= @@SERVERNAME

INSERT INTO [DBA_CDR].[SQL_Inventory].[dbo].[SQLBackupStatus]
	([ServerInstance]
           ,[Database]
           ,[LastCommvaultFull]
           ,[LastTran]
           ,[LastDiff]
           ,[PopulatedDate]
           ,[RecoveryMode]
           ,[DBCreationTime]
           ,[DBStatus])
SELECT
@@SERVERNAME [ServerInstance], 
  [Database]=d.name,LastCommvaultFull,LastTran,LastDiff,CapturedDate=GETDATE(),
  RecoveryMode=DATABASEPROPERTYEX(d.name, 'Recovery'),
  CreationTime=d.crdate, Status=DATABASEPROPERTYEX(d.name, 'Status')
from master.dbo.sysdatabases d
left outer join
 (select database_name, LastCommvaultFull=max(backup_finish_date)
        from msdb.dbo.backupset
        where type = 'D' and backup_finish_date <= getdate() and name = 'CommVault Galaxy Backup'
        group by database_name
 ) b
on d.name = b.database_name
left outer join
 (select database_name, LastTran=max(backup_finish_date)
        from msdb.dbo.backupset
        where type ='L' and backup_finish_date <= getdate()
        group by database_name
 ) c
on d.name = c.database_name
left outer join
 (select database_name, LastDiff=max(backup_finish_date)
        from msdb.dbo.backupset
        where type ='I' and backup_finish_date <= getdate()
        group by database_name
 ) e
on d.name = e.database_name

 where d.name <> 'tempdb' and d.name <> 'model'and DATABASEPROPERTYEX(d.name, 'Status') = 'ONLINE'
order by [LastCommvaultFull]


END TRY

BEGIN CATCH
	-- Execute error retrieval routine.
	SELECT 
    ERROR_NUMBER() AS ErrorNumber,
    ERROR_SEVERITY() AS ErrorSeverity,
    ERROR_STATE() as ErrorState,
    ERROR_LINE () as ErrorLine,
    ERROR_PROCEDURE() as ErrorProcedure,
    ERROR_MESSAGE() as ErrorMessage;
    
	RETURN @@Error
END CATCH

GO


