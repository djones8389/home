select 'alter availability group [availabilitygroup] remove database ' + quotename(d.name) + 'drop database ' + quotename(d.name)
from sys.databases d
LEFT JOIN sys.dm_hadr_availability_replica_states h ON d.replica_id = h.replica_id
where database_id > 4
	and name <> 'cdr_local'
	and h.connected_state is null
order by 1;


alter availability group [availabilitygroup] add database [cloudci-contentservices]
alter availability group [availabilitygroup] add database [cloudci-integration]
alter availability group [availabilitygroup] add database [cloudci-orchestrator]
alter availability group [availabilitygroup] add database [cloudci-portal]
alter availability group [availabilitygroup] add database [production-contentservices]
alter availability group [availabilitygroup] add database [production-integration]
alter availability group [availabilitygroup] add database [production-orchestrator]
alter availability group [availabilitygroup] add database [production-portal]
alter availability group [availabilitygroup] add database [staging-contentservices]
alter availability group [availabilitygroup] add database [staging-integration]
alter availability group [availabilitygroup] add database [staging-orchestrator]
alter availability group [availabilitygroup] add database [staging-portal]

/*

select * from sys.dm_hadr_automatic_seeding
select * from sys.dm_hadr_physical_seeding_stats

*/