select case when instanceName = 'MSSQLSERVER'
 THEN host else host + '\' + instanceName end as InstanceName
from (
SELECT 
	CASE WHEN hostname = '' then sqlclustername else hostname end as host
	, instanceName
FROM [SQL_Inventory].[dbo].[vw_NonDecommissionedSQLServerInstances]
where environment ='p'
) a

--select SQLServerID, case when [SQL Instance] = 'MSSQLSERVER' THEN [SQL Cluster Name] else [SQL Cluster Name] + '\' + [SQL Instance] end as InstanceName, Version, Edition from vw_CDRPortal_InstanceList where Environment = 'Production' and commissionedState = 'Commissioned' and [SQL Cluster Name] not like '%.hsx.%' and [SQL Cluster Name] not like '%nonprod%' order by [SQL Cluster Name], [SQL Instance]