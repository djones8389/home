use master;

IF OBJECT_ID('tempdb..##ClientTableMetrics') IS NOT NULL DROP TABLE ##ClientTableMetrics;

CREATE TABLE ##ClientTableMetrics (
	server_name nvarchar(100)
	, database_name nvarchar(100)
	, database_id int
	, table_name nvarchar(100)
	, rows int
	, reserved_kb nvarchar(20)
	, data_kb nvarchar(20)
	, index_size nvarchar(20)
	, unused_kb nvarchar(20)
);

DECLARE @RunForAll nvarchar(MAX)='';

select @RunForAll+=CHAR(13) +
	'use '+quotename(name)+';
	declare @'+name+' nvarchar(MAX) = '''';

	select @'+name+' +=CHAR(13) +
		''exec sp_spaceused @objname = ''''''+quotename(s.name)+''.''+quotename(t.name)+ '''''' ''
	from sys.tables T
	inner join sys.schemas S
	on s.schema_id = t.schema_id
	where type = ''u''
	order by t.name;

	INSERT ##ClientTableMetrics (table_name, [rows], reserved_kb, data_kb, index_size, unused_kb)
	EXEC(@'+name+');

	UPDATE ##ClientTableMetrics
	SET database_id = db_id()
	where database_id IS NULL;

	UPDATE ##ClientTableMetrics
	SET server_name = @@SERVERNAME
		, database_name = DB_Name()
	where db_id() = database_id;
	'
from sys.databases
where database_id > 4
	and name not in ('GenericRatingEngine', 'distribution', 'CDR_Local');

EXEC(@RunForAll);

select server_name
	, database_name
	--, database_id
	, table_name
	, rows
	, replace(reserved_kb, 'KB','') reserved_kb
	, replace(data_kb, 'KB','') data_kb
	, replace(index_size, 'KB','') index_size
	, replace(unused_kb, 'KB','') unused_kb
from ##ClientTableMetrics
order by server_name, database_name;
