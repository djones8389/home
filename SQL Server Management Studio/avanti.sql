use Protect
SELECT
	machine.[mmKey] AS Id,
	machine.[dnsName] AS DnsName,
	machine.[domain] AS Domain,
	machine.[language] AS [Language],
	machine.[name] AS Name,
	machine.[mmOSID] AS ProductId,
	[Reporting].[IPAddressToString](machine.[lastKnownIPAddress]) AS LastKnownIP,
	lastPatchAssessment.[smachScanDate] AS LastPatchAssessedOn,
	lastPatchAssessment.[smachID] AS LastAssessedMachineStateId,
	lastPatchAssessment.[groupName] AS LastPatchMachineGroupName,
	p.prodID 
FROM [dbo].[ManagedMachines] AS machine
LEFT JOIN [dbo].[Scans] AS scan 
ON scan.[ScanID] = machine.[lastPatchScanId]
LEFT  JOIN [dbo].[ScanMachines] AS lastPatchAssessment ON
		lastPatchAssessment.[smachmmKey] = machine.[mmKey] AND
		lastPatchAssessment.smachScanID = scan.[ScanID]
LEFT JOIN LinkSPProduct LSP
on LSP.spplID = smachOSID
LEFT JOIN Products P
on P.prodID = LSP.spplprodID
--where p.prodName like '%SQL%'