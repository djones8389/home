--sp_whoisactive

select * from msdb.dbo.sysjobs order by 3;

use msdb

exec sp_update_job @job_name = 'IDIT - IDIT_PROD_MI Restore', @enabled = 0
exec sp_stop_job @job_name = 'IDIT - IDIT_PROD_MI Restore'
