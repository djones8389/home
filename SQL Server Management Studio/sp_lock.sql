IF object_id('tempdb..#lock') IS NOT NULL
	DROP TABLE #lock;

CREATE TABLE #lock (
	[spid] SMALLINT
	,[dbid] SMALLINT
	,[objID] INT
	,[indid] TINYINT
	,[type] VARCHAR(100)
	,[Resource] VARCHAR(200)
	,[Mode] VARCHAR(10)
	,[Status] VARCHAR(100)
	)

INSERT #Lock
EXEC ('sp_lock')

SELECT *
	,db_name(dbid) [DatabaseName]
	,'USE ' + quotename(db_name(dbid)) + '; select name, type from sys.objects where object_id = ' + cast(objid AS NVARCHAR(100)) + ''
FROM #Lock A
WHERE [Status] <> 'GRANT'

USE cdr_local

EXEC sp_whoisactive