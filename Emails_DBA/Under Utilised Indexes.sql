SELECT	 TOP 10
		 DB_NAME() AS [database],
		 OBJECT_NAME(s.OBJECT_ID) AS [table],
		 i.name AS [index],
		 i.index_id,
         s.user_updates,
         s.user_seeks + s.user_scans + s.user_lookups AS [user_selects],
         [Difference] = user_updates - (user_seeks + user_scans + user_lookups)
 FROM		sys.dm_db_index_usage_stats AS s WITH (NOLOCK)
 INNER JOIN sys.indexes AS i WITH (NOLOCK)
		 ON s.[object_id] = i.[object_id]
		AND i.index_id = s.index_id
WHERE	 OBJECTPROPERTY(s.[OBJECT_ID],'IsUserTable') = 1
 AND	 i.index_id > 1
ORDER BY [DIFFERENCE] DESC
OPTION	 (RECOMPILE);